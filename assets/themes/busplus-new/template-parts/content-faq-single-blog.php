<?php
$posts = get_field( 'preguntas_frecuentes' );
if ( $posts ) : ?>
<?php $count = 1; ?>
<div class="faq">
    <div class="container">
        <ul>
            <?php foreach( $posts as $post) : ?>
                <?php setup_postdata( $post ); ?>

                    <li class="faq-according">
                        <div class="faq-according-title">
                                <h2><?php echo $count++; ?>. <?php the_title();?></h2>
                                <div class="circle">
                                    <i class="fa-solid fa-chevron-down"></i>
                                </div>
                        </div>
                        <div class="faq-according-info single-body">
                            <div class="image">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <?php the_content(); ?>
                        </div>
                    </li>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        </ul>
    </div>
</div>
<?php endif; ?>