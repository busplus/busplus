<?php 
    $currentDate =  date('d/m/Y');
    $nextDate =  date('d/m/Y', strtotime("tomorrow"));
    $cupon = get_option('buscador_codigo_descuento');
    $origen = get_field( 'origen' );
    $destino = get_field( 'destino' );
    /* ESTE ES EL BUSCADOR QUE ESTA EN STAND BY */
?>


<div class="block-buscador">
    <input type="hidden" value="<?php echo $cupon?>" id="cupon-verify">
    <div class="container">
        <div class="buscador p-relative">
            <div class="row">
                <?php get_template_part('template-parts/buscador-parts/column-origen-destino');?>
                <div class="row-column col-fechas" >
                    <div class="column">
                        <div class="input-container border-right">
                            <input type="text" readonly placeholder="" value=""  id="fechaIngreso" class="date-start">
                            <label for="fechaIngreso">IDA</label>
                            <svg class="icon-date" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.92638 1.61279V5.06542M12.8316 1.61279V5.06542M1.6106 8.51806H17.1474M5.92638 11.9707H5.93502M9.37902 11.9707H9.38765M12.8316 11.9707H12.8403M5.92638 15.4233H5.93502M9.37902 15.4233H9.38765M12.8316 15.4233H12.8403M3.33691 3.33911H15.4211C16.3745 3.33911 17.1474 4.11201 17.1474 5.06542V17.1496C17.1474 18.1031 16.3745 18.876 15.4211 18.876H3.33691C2.38349 18.876 1.6106 18.1031 1.6106 17.1496V5.06542C1.6106 4.11201 2.38349 3.33911 3.33691 3.33911Z" stroke="#6D6D6D" stroke-width="1.72632" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            
                        </div>
                        <div class="input-container">
                       
                            <input type="text" readonly  placeholder="Opcional" value="" id="fechaSalida" class="date-end">
                            <label for="fechaSalida" class="label-up">VUELTA</label>
                            <svg class="icon-date" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.92638 1.61279V5.06542M12.8316 1.61279V5.06542M1.6106 8.51806H17.1474M5.92638 11.9707H5.93502M9.37902 11.9707H9.38765M12.8316 11.9707H12.8403M5.92638 15.4233H5.93502M9.37902 15.4233H9.38765M12.8316 15.4233H12.8403M3.33691 3.33911H15.4211C16.3745 3.33911 17.1474 4.11201 17.1474 5.06542V17.1496C17.1474 18.1031 16.3745 18.876 15.4211 18.876H3.33691C2.38349 18.876 1.6106 18.1031 1.6106 17.1496V5.06542C1.6106 4.11201 2.38349 3.33911 3.33691 3.33911Z" stroke="#6D6D6D" stroke-width="1.72632" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="row-column col-pasajeros">
                    <div class="column">
                        <div class="input-container">
                            <button type="button" class="btn-pasajeros btn-minus">
                                <svg width="15" height="3" viewBox="0 0 15 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.67358 1.39172H13.7578" stroke="#6D6D6D" stroke-width="1.72632" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <input readonly type="number" pattern="\d*" placeholder="" id="pasajeros" value="1" min="1">
                            <label for="pasajeros">PASAJEROS</label>
                            <button type="button" class="btn-pasajeros btn-plus">
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.24219 7.39178H13.3264M7.28429 1.34967V13.4339" stroke="#6D6D6D" stroke-width="1.72632" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                
                <?php get_template_part('template-parts/buscador-parts/column-codigo-descuento');?>
                <div class="row-column col-descuento">
                    <div class="descuento-container">
                        <div class="content d-flex">
                            <p id="codigo-descuento">Tengo un código de descuento</p>
                            <div class="switch-content">
                                <label class="switch">
                                    <input type="checkbox" id="checkCupon">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="popup p-relative">
                                <button type="button">
                                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M9.01562 15.1631H11.0156V9.16309H9.01562V15.1631ZM10.0156 7.16309C10.299 7.16309 10.5365 7.06725 10.7281 6.87559C10.9198 6.68392 11.0156 6.44642 11.0156 6.16309C11.0156 5.87975 10.9198 5.64225 10.7281 5.45059C10.5365 5.25892 10.299 5.16309 10.0156 5.16309C9.73229 5.16309 9.49479 5.25892 9.30313 5.45059C9.11146 5.64225 9.01562 5.87975 9.01562 6.16309C9.01562 6.44642 9.11146 6.68392 9.30313 6.87559C9.49479 7.06725 9.73229 7.16309 10.0156 7.16309ZM10.0156 20.1631C8.63229 20.1631 7.33229 19.9006 6.11562 19.3756C4.89896 18.8506 3.84063 18.1381 2.94062 17.2381C2.04063 16.3381 1.32812 15.2798 0.803125 14.0631C0.278125 12.8464 0.015625 11.5464 0.015625 10.1631C0.015625 8.77975 0.278125 7.47975 0.803125 6.26309C1.32812 5.04642 2.04063 3.98809 2.94062 3.08809C3.84063 2.18809 4.89896 1.47559 6.11562 0.950586C7.33229 0.425586 8.63229 0.163086 10.0156 0.163086C11.399 0.163086 12.699 0.425586 13.9156 0.950586C15.1323 1.47559 16.1906 2.18809 17.0906 3.08809C17.9906 3.98809 18.7031 5.04642 19.2281 6.26309C19.7531 7.47975 20.0156 8.77975 20.0156 10.1631C20.0156 11.5464 19.7531 12.8464 19.2281 14.0631C18.7031 15.2798 17.9906 16.3381 17.0906 17.2381C16.1906 18.1381 15.1323 18.8506 13.9156 19.3756C12.699 19.9006 11.399 20.1631 10.0156 20.1631ZM10.0156 18.1631C12.249 18.1631 14.1406 17.3881 15.6906 15.8381C17.2406 14.2881 18.0156 12.3964 18.0156 10.1631C18.0156 7.92975 17.2406 6.03809 15.6906 4.48809C14.1406 2.93809 12.249 2.16309 10.0156 2.16309C7.78229 2.16309 5.89062 2.93809 4.34063 4.48809C2.79063 6.03809 2.01562 7.92975 2.01562 10.1631C2.01562 12.3964 2.79063 14.2881 4.34063 15.8381C5.89062 17.3881 7.78229 18.1631 10.0156 18.1631Z"
                                            fill="white" />
                                    </svg>
                                </button>
                                <div class="popup-card">
                                    <p>
                                        Completá los datos del viaje, validá tu código y por último buscá los servicios
                                    </p>
                                </div>
                            </div>
                            <div class="modal-codigo-descuento">
                                <div class="content">
                                    <div class="content-header">
                                        <button type="button" class="close-modal">
                                            <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M30 18L18 30M18 18L30 30" stroke="#383838" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                            </svg>
                                        </button>
                                    </div>
                                    <h2>AGREGÁ EL CÓDIGO DE DESCUENTO</h2>
                                    <p>Ingresá el código alfanumérico</p>
                                    <div class="input-container">
                                        <input type="text" id="cd-input" placeholder="CÓDIGO">
                                        <p class="error-msg">Código incorrecto</p>
                                    </div>
                                    <div class="button__container">
                                        <button type="button" class="btn btn-orange" id="applyCode">
                                            APLICAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-column row-column-submit">
                    <div class="button__container">
                        <button type="button" class="btn btn-orange btn-icon" id="searchSubmit">
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.75 15.75L12.525 12.525M14.25 8.25C14.25 11.5637 11.5637 14.25 8.25 14.25C4.93629 14.25 2.25 11.5637 2.25 8.25C2.25 4.93629 4.93629 2.25 8.25 2.25C11.5637 2.25 14.25 4.93629 14.25 8.25Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            BUSCAR
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template-parts/buscador-parts/modal-terminal');?>
</div>
