<?php 
    $currentDate =  date('d/m/Y');
    $nextDate =  date('d/m/Y', strtotime("tomorrow"));
    $cupon = get_option('buscador_codigo_descuento');
    $origen = get_field( 'origen' );
    $destino = get_field( 'destino' );
?>


<div class="block-buscador">
    <input type="hidden" value="<?php echo $cupon?>" id="cupon-verify">
    <div class="container">
        <div class="buscador p-relative">
            <div class="row">
                <div class="col-origen" style="order:1;">
                    <div class="column column-origen-salida">
                        <div class="input-container">
                            <div class="input-location">
                                <svg width="18" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.54 20.351L8.61 20.391L8.638 20.407C8.74903 20.467 8.87327 20.4985 8.9995 20.4985C9.12573 20.4985 9.24997 20.467 9.361 20.407L9.389 20.392L9.46 20.351C9.85112 20.1191 10.2328 19.8716 10.604 19.609C11.5651 18.9305 12.463 18.1667 13.287 17.327C15.231 15.337 17.25 12.347 17.25 8.5C17.25 6.31196 16.3808 4.21354 14.8336 2.66637C13.2865 1.11919 11.188 0.25 9 0.25C6.81196 0.25 4.71354 1.11919 3.16637 2.66637C1.61919 4.21354 0.75 6.31196 0.75 8.5C0.75 12.346 2.77 15.337 4.713 17.327C5.53664 18.1667 6.43427 18.9304 7.395 19.609C7.76657 19.8716 8.14854 20.1191 8.54 20.351ZM9 11.5C9.79565 11.5 10.5587 11.1839 11.1213 10.6213C11.6839 10.0587 12 9.29565 12 8.5C12 7.70435 11.6839 6.94129 11.1213 6.37868C10.5587 5.81607 9.79565 5.5 9 5.5C8.20435 5.5 7.44129 5.81607 6.87868 6.37868C6.31607 6.94129 6 7.70435 6 8.5C6 9.29565 6.31607 10.0587 6.87868 10.6213C7.44129 11.1839 8.20435 11.5 9 11.5Z" fill="#e2a615"/>
                                </svg>
                            </div>
                            <label for="origen">ORIGEN</label>
                            <input class="input-terminal" type="text" id="origen" autocomplete="off" data-id='<?php echo $origen;?>' placeholder="Ciudad de origen...">
                            <div class="input-delete">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <div class="arrow-double" id="cambiarInputs">
                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_3025_17476)">
                                <rect width="40" height="40" rx="20" fill="#E2A615"/>
                                <path d="M11 24L15 28M15 28L19 24M15 28V12M29 16L25 12M25 12L21 16M25 12V28" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </g>
                                <defs>
                                <clipPath id="clip0_3025_17476">
                                <rect width="40" height="40" rx="20" fill="white"/>
                                </clipPath>
                                </defs>
                            </svg>
                        </div>
                        <div class="input-container">
                            <div class="input-location">
                                <svg width="18" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.54 20.351L8.61 20.391L8.638 20.407C8.74903 20.467 8.87327 20.4985 8.9995 20.4985C9.12573 20.4985 9.24997 20.467 9.361 20.407L9.389 20.392L9.46 20.351C9.85112 20.1191 10.2328 19.8716 10.604 19.609C11.5651 18.9305 12.463 18.1667 13.287 17.327C15.231 15.337 17.25 12.347 17.25 8.5C17.25 6.31196 16.3808 4.21354 14.8336 2.66637C13.2865 1.11919 11.188 0.25 9 0.25C6.81196 0.25 4.71354 1.11919 3.16637 2.66637C1.61919 4.21354 0.75 6.31196 0.75 8.5C0.75 12.346 2.77 15.337 4.713 17.327C5.53664 18.1667 6.43427 18.9304 7.395 19.609C7.76657 19.8716 8.14854 20.1191 8.54 20.351ZM9 11.5C9.79565 11.5 10.5587 11.1839 11.1213 10.6213C11.6839 10.0587 12 9.29565 12 8.5C12 7.70435 11.6839 6.94129 11.1213 6.37868C10.5587 5.81607 9.79565 5.5 9 5.5C8.20435 5.5 7.44129 5.81607 6.87868 6.37868C6.31607 6.94129 6 7.70435 6 8.5C6 9.29565 6.31607 10.0587 6.87868 10.6213C7.44129 11.1839 8.20435 11.5 9 11.5Z" fill="#e2a615"/>
                                </svg>
                            </div>
                            <label for="destino">DESTINO</label>
                            <input class="input-terminal" type="text" id="salida" autocomplete="off" data-id='<?php echo $destino;?>' placeholder="Ciudad de salida...">
                            <div class="input-delete">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-fechas" style="order:2;">
                    <div class="column">
                        <div class="input-container border-right">
                            <label for="fechaIngreso">IDA</label>
                            <!-- <input class="date-start" type="text" readonly value="<?php echo $currentDate;?>" id="fechaIngreso"> -->
                            <input class="date-start" type="text" readonly id="fechaIngreso" placeholder="DD/MM/AAAA" required>
                        </div>
                        <div class="input-container">
                            <label for="fechaSalida">VUELTA</label>
                            <input class="date-end" type="text" readonly value="" placeholder="Opcional..." value="" id="fechaSalida">
                        </div>
                    </div>
                </div>
                <div class="col-pasajeros" style="order:3;">
                    <div class="column column-icon">
                        <div class="input-container border-right p-relative">
                            <svg class="input-icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M7.5001 6C7.5001 4.80653 7.9742 3.66193 8.81812 2.81802C9.66203 1.97411 10.8066 1.5 12.0001 1.5C13.1936 1.5 14.3382 1.97411 15.1821 2.81802C16.026 3.66193 16.5001 4.80653 16.5001 6C16.5001 7.19347 16.026 8.33807 15.1821 9.18198C14.3382 10.0259 13.1936 10.5 12.0001 10.5C10.8066 10.5 9.66203 10.0259 8.81812 9.18198C7.9742 8.33807 7.5001 7.19347 7.5001 6ZM3.7511 20.105C3.78482 17.9395 4.66874 15.8741 6.21206 14.3546C7.75538 12.8351 9.83431 11.9834 12.0001 11.9834C14.1659 11.9834 16.2448 12.8351 17.7881 14.3546C19.3315 15.8741 20.2154 17.9395 20.2491 20.105C20.2517 20.2508 20.2117 20.3942 20.1341 20.5176C20.0565 20.641 19.9446 20.7392 19.8121 20.8C17.3613 21.9237 14.6963 22.5037 12.0001 22.5C9.2141 22.5 6.5671 21.892 4.1881 20.8C4.05558 20.7392 3.94367 20.641 3.86606 20.5176C3.78844 20.3942 3.74849 20.2508 3.7511 20.105Z" fill="#e2a615"/>
                            </svg>
                            <label for="pasajeros">PASAJEROS</label>
                            <input type="number"  value="1" id="pasajeros">
                        </div>
                    </div>
                </div>
                <div class="col-cupon" style="order:4;">
                    <div class="column column-descuento">
                        <div class="input-container">
                            <label for="ida">COD. DESCUENTO <span class="msg-label"></span></label>
                            <input type="text" placeholder="AJ032IAJ032I" id="cupon" autocomplete="off">
                        </div>
                        <div class="input-container">
                            <button type="button" class="btn btn-orange btn-descuento">
                                VALIDAR
                            </button>
                            <div class="input-icon delete cupon-delete">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" style="order:5;">
                    <div class="button__container">
                        <button type="button" class="btn btn-orange" id="searchSubmit">
                            <svg class="d-none" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.6 21L13.3 14.7C12.8 15.1 12.225 15.4167 11.575 15.65C10.925 15.8833 10.2333 16 9.5 16C7.68333 16 6.14583 15.3708 4.8875 14.1125C3.62917 12.8542 3 11.3167 3 9.5C3 7.68333 3.62917 6.14583 4.8875 4.8875C6.14583 3.62917 7.68333 3 9.5 3C11.3167 3 12.8542 3.62917 14.1125 4.8875C15.3708 6.14583 16 7.68333 16 9.5C16 10.2333 15.8833 10.925 15.65 11.575C15.4167 12.225 15.1 12.8 14.7 13.3L21 19.6L19.6 21ZM9.5 14C10.75 14 11.8125 13.5625 12.6875 12.6875C13.5625 11.8125 14 10.75 14 9.5C14 8.25 13.5625 7.1875 12.6875 6.3125C11.8125 5.4375 10.75 5 9.5 5C8.25 5 7.1875 5.4375 6.3125 6.3125C5.4375 7.1875 5 8.25 5 9.5C5 10.75 5.4375 11.8125 6.3125 12.6875C7.1875 13.5625 8.25 14 9.5 14Z" fill="white"/>
                            </svg>
                            <span>BUSCAR</span>
                        </button>
                    </div>
                </div>
                <div class="col-descuento" style="order:6">
                    <div class="descuento-container">
                        <div class="content d-flex">
                            <p>Tengo un código de descuento</p>
                            <div class="switch-content">
                                <label class="switch">
                                    <input type="checkbox" id="checkCupon">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="popup p-relative">
                                <button type="button">
                                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M9.01562 15.1631H11.0156V9.16309H9.01562V15.1631ZM10.0156 7.16309C10.299 7.16309 10.5365 7.06725 10.7281 6.87559C10.9198 6.68392 11.0156 6.44642 11.0156 6.16309C11.0156 5.87975 10.9198 5.64225 10.7281 5.45059C10.5365 5.25892 10.299 5.16309 10.0156 5.16309C9.73229 5.16309 9.49479 5.25892 9.30313 5.45059C9.11146 5.64225 9.01562 5.87975 9.01562 6.16309C9.01562 6.44642 9.11146 6.68392 9.30313 6.87559C9.49479 7.06725 9.73229 7.16309 10.0156 7.16309ZM10.0156 20.1631C8.63229 20.1631 7.33229 19.9006 6.11562 19.3756C4.89896 18.8506 3.84063 18.1381 2.94062 17.2381C2.04063 16.3381 1.32812 15.2798 0.803125 14.0631C0.278125 12.8464 0.015625 11.5464 0.015625 10.1631C0.015625 8.77975 0.278125 7.47975 0.803125 6.26309C1.32812 5.04642 2.04063 3.98809 2.94062 3.08809C3.84063 2.18809 4.89896 1.47559 6.11562 0.950586C7.33229 0.425586 8.63229 0.163086 10.0156 0.163086C11.399 0.163086 12.699 0.425586 13.9156 0.950586C15.1323 1.47559 16.1906 2.18809 17.0906 3.08809C17.9906 3.98809 18.7031 5.04642 19.2281 6.26309C19.7531 7.47975 20.0156 8.77975 20.0156 10.1631C20.0156 11.5464 19.7531 12.8464 19.2281 14.0631C18.7031 15.2798 17.9906 16.3381 17.0906 17.2381C16.1906 18.1381 15.1323 18.8506 13.9156 19.3756C12.699 19.9006 11.399 20.1631 10.0156 20.1631ZM10.0156 18.1631C12.249 18.1631 14.1406 17.3881 15.6906 15.8381C17.2406 14.2881 18.0156 12.3964 18.0156 10.1631C18.0156 7.92975 17.2406 6.03809 15.6906 4.48809C14.1406 2.93809 12.249 2.16309 10.0156 2.16309C7.78229 2.16309 5.89062 2.93809 4.34063 4.48809C2.79063 6.03809 2.01562 7.92975 2.01562 10.1631C2.01562 12.3964 2.79063 14.2881 4.34063 15.8381C5.89062 17.3881 7.78229 18.1631 10.0156 18.1631Z"
                                            fill="white" />
                                    </svg>
                                </button>
                                <div class="popup-card">
                                    <p>
                                        Completá los datos del viaje, validá tu código y por último buscá los servicios
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template-parts/buscador-parts/modal-terminal');?>
</div>
<?php get_template_part('template-parts/content-calendar');?>