
<?php 
  $titulo = get_field( 'titulo', 'options' );
  $placeholder = get_field( 'placeholder', 'options' );
  $dest = get_field( 'destinatario', 'options' );

?>

<section class="main__newsletter">
    <img width="100%" height="100%" alt="En la imagen se muestra una carta roja abierto con una hoja blanca con rojo adentro." src="<?php echo IMAGE;?>/newsletter-vector1.svg"  class="vector1" />
    <img width="100%" height="100%" alt="En la imagen se muestra una carta roja abierto con una hoja blanca con rojo adentro." src="<?php echo IMAGE;?>/newsletter-vector2.svg"  class="vector2" />
    <div class="heading-newsletter">
            <h2 class="article-title-newsletter"><?php echo $titulo;?></h2>
    </div>
    <div class="container">

    
    <div class="form" data-destinatario="<?php echo $dest;?>">

                <input type="text" placeholder="<?php echo $placeholder;?>" />

                <button class="btn btn-orange" id="send-newsletter"> 
                    <i class="fa-regular fa-paper-plane d-none-xs"></i>
                    <span class="d-flex-xs d-none">Suscribite</span>
                </button>

    </div>
    </div>
    <img src="<?php echo IMAGE;?>/newsletter-img-desktop.svg" alt="newsletter-img" class="newsletter-img-desktop" />
</section>