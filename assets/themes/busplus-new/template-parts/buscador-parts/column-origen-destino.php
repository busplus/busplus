<div class="row-column col-origen">
    <div class="column column-origen-salida">
        <div class="input-container input-container-origen">
            <div class="input-icon search origen">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M21 21L16.7 16.7M19 11C19 15.4183 15.4183 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11Z"
                        stroke="#6D6D6D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <input type="text" autocomplete="off" id="origen" data-id='<?php echo $origen;?>' placeholder="" data-lat="" data-long="">
            <label for="origen" id="origenLabel">CIUDAD DE ORIGEN</label>
            <div class="input-icon delete delete-origen">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </div>
        </div>
        <div class="arrow-double" id="cambiarInputs">
            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_3025_17476)">
                    <rect width="40" height="40" rx="20" fill="#E2A615" />
                    <path d="M11 24L15 28M15 28L19 24M15 28V12M29 16L25 12M25 12L21 16M25 12V28" stroke="#EEEEEE"
                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </g>
                <defs>
                    <clipPath id="clip0_3025_17476">
                        <rect width="40" height="40" rx="20" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        </div>
        <div class="input-container input-container-salida">
            <div class="input-icon search destino">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M21 21L16.7 16.7M19 11C19 15.4183 15.4183 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11Z"
                        stroke="#6D6D6D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            </div>
            <input type="text" autocomplete="off" id="salida" data-id='<?php echo $destino;?>' placeholder="" data-lat="" data-long="">
            <label for="origen" id="salidaLabel">CIUDAD DE DESTINO</label>
            <div class="input-icon delete delete-salida">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </div>
        </div>
    </div>
</div>