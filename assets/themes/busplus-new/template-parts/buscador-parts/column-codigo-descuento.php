<div class="row-column col-descuento-mobile">
    <div class="column">
        <div class="input-container">
            <input type="text" placeholder="" id="cd-mobile-input">
            <label for="pasajeros">CÓDIGO DE DESCUENTO</label>
            <div class="input-icon delete delete-cupon">
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13 1L1 13M1 1L13 13" stroke="#6D6D6D" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </div>
        </div>
    </div>
</div>