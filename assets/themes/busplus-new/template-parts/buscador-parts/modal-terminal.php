<div class="modal-terminal">
    <div class="content content-terminal">
        <button type="button" class="close-modal-terminal">
            <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M30 18L18 30M18 18L30 30" stroke="#383838" stroke-width="2" stroke-linecap="round"
                    stroke-linejoin="round" />
            </svg>
        </button>
        <div id="mapaTerminal"></div>
    </div>
</div>