<div id="datepicker-container">
    <div class="datepicker-content">
        <div id="header">
            <h3>Seleccionar Fechas</h3>
            <button type="button" class="btn close-calendar">
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M30 18L18 30M18 18L30 30" stroke="#383838" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </button>
        </div>
        <div id="weekdays">
            <span>Lun</span><span>Mar</span><span>Mié</span>
            <span>Jue</span><span>Vie</span><span>Sáb</span><span>Dom</span>
        </div>
        <div id="calendar-scroll">
            <div id="month-list"></div>
        </div>
        <div id="selected-range">
            <span id="selected-dates">Seleccione un rango</span>
        </div>
        <div id="calendar-actions">
            <button type="button" id="apply-dates" class="btn btn-orange mb-1">APLICAR</button>
            <button type="button" id="clear-dates" class="btn only-text green-solid mb-xs-2 mt-xs-0 mb-4 mt-1">LIMPIAR</button>
        </div>
    </div>
</div>