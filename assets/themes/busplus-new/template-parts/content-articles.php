<?php 
    $titulo = get_sub_field('titulo');
    $link = get_sub_field('link');
?>
<?php if ( have_rows( 'relacionados' ) ) : ?>
 <?php while ( have_rows( 'relacionados' ) ) :
  the_row(); ?>
  <?php     $titulo = get_sub_field('titulo');
    $link = get_sub_field('link'); ?>
    <section class="block-related_blog">
        <div class="container">
            <div class="block-title heading-arrow">
                <h2 class="f-24-18 f-bold p-relative">
                    <?php echo $titulo; ?>
                    <?php insert_button($link, 0, 'only-text orange'); ?>
                </h2>
            </div>
            <div class="row">
                <?php 
                    $args = array(
                        'post_type' => 'blog',
                        'posts_per_page' => 3,
                        'orderby' => 'date',
                        'order' => 'ASC',
                    );
                    $query = new WP_Query($args);
                ?>
                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="col-sm-4 col-xs-6 col-12">
                            <a href=" <?php echo get_the_permalink(); ?>" aria-label="<?php the_title(); ?>" class="card">
                                <div class="block-image">
                                    <?php echo the_post_thumbnail(); ?>
                                </div>
                                <h3 class="title-blog"> <?php the_title(); ?></h3>
                            </a>
                        </div>
                    <?php endwhile; ?>
                    <?php  wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="btn-down d-none-xs">
                <?php insert_button($link, 4, 'only-text orange'); ?>
            </div>
        </div>
    </section>
 <?php endwhile; ?>
<?php endif; ?>
