
<?php get_header(); ?>
    <div class="single single-default single-blog">
        <div class="container">
            <div class="single-body single-body-blog">
                <div class="title">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="image">
                    <?php the_post_thumbnail(); ?>
                </div>
                <?php the_content(); ?>
            </div>
        </div>
        <?php get_template_part('template-parts/content', 'faq-single-blog'); ?>
        <div clas="block-last">
            <div class="container single-body">
                <?php if ( $texto = get_field( 'texto' ) ) : ?>
                    <?php echo $texto; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php show_related_posts_blog(); ?>
<?php get_footer(); ?>