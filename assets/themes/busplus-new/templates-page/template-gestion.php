<?php 
/*
 * Template Name: Plantilla Gestion
 * 
 */
?>
<?php get_header(); ?>
<?php $titulo = get_field('titulo'); ?>
<main class="single layout-default single-gestion">
    <div class="container">
        <div class="title mb-80">
            <?php insert_acf($titulo, 'h1') ?>
        </div>
        <?php if ( have_rows( 'tarjetas' ) ) : ?>
            <?php while ( have_rows( 'tarjetas' ) ) :
            the_row(); ?>
                <?php 
                    // CARD 1
                    $titulo = get_sub_field('titulo');
                    $link = get_sub_field('link');
                    $link_url = $link['url'];
                    $imagen = get_sub_field('imagen');
                    // CARD 2
                    $titulo_2 = get_sub_field('titulo_2');
                    $link_2 = get_sub_field('link_2');
                    $link_url_2 = $link_2['url'];
                    $imagen_2 = get_sub_field('imagen_2');
                    // CARD 3
                    $titulo_3 = get_sub_field('titulo_3');
                    $link_3 = get_sub_field('link_3');
                    $link_url_3 = $link_3['url'];
                    $imagen_3 = get_sub_field('imagen_3');
                    // CARD 4
                    $titulo_4 = get_sub_field('titulo_4');
                    $link_4 = get_sub_field( 'link_4');
                    $link_url_4 = $link_4['url'];
                    $imagen_4 = get_sub_field('imagen_4');
                    // CARD 5
                    $titulo_5 = get_sub_field('titulo_5');
                    $link_5 = get_sub_field( 'link_5' );
                    $link_url_5 = $link_5['url'];
                    $imagen_5 = get_sub_field( 'imagen_5');
                ?>
                <div class="grid">
                    <?php if($link): ?>
                        <div class="col-grid">
                            <a href="<?php echo $link_url;?>" title="<?php echo $titulo;?>">
                                <div class="card">
                                    <div class="card__main-content">
                                        <?php if($imagen): ?>
                                            <div class="icon-wrapper">
                                                <?php insert_image($imagen, '400') ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                          insert_acf($titulo, 'h2');
                                        ?>
                                    </div>
                                    <div class="plus-icon-wrapper">
                                                    <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                    <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                    <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                    </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if($link_2): ?>
                        <div class="col-grid">
                            <a href="<?php echo $link_url_2;?>" title="<?php echo $titulo_2;?>">
                                <div class="card">
                                    <div class="card__main-content">
                                        <?php if($imagen_2): ?>
                                            <div class="icon-wrapper">
                                                <?php insert_image($imagen_2, '400') ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                          insert_acf($titulo_2, 'h2');
                                        ?>
                                    </div>
                                    <div class="plus-icon-wrapper">
                                                    <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                    <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                    <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                    </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if($link_3): ?>
                        <div class="col-grid">
                            <a href="<?php echo $link_url_3;?>" title="<?php echo $titulo_3;?>">
                                <div class="card">
                                    <div class="card__main-content">
                                        <?php if($imagen_3): ?>
                                            <div class="icon-wrapper">
                                                <?php insert_image($imagen_3, '400') ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                          insert_acf($titulo_3, 'h2');
                                        ?>
                                    </div>
                                    <div class="plus-icon-wrapper">
                                                    <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                    <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                    <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                    </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if($link_4): ?>
                        <div class="col-grid">
                            <a href="<?php echo $link_url_4;?>" title="<?php echo $titulo_4;?>">
                                <div class="card">
                                    <div class="card__main-content">
                                        <?php if($imagen_4): ?>
                                            <div class="icon-wrapper">
                                                <?php insert_image($imagen_4, '400') ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                          insert_acf($titulo_4, 'h2');
                                        ?>
                                    </div>
                                    <div class="plus-icon-wrapper">
                                                    <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                    <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                    <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                    </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if($link_5): ?>
                        <div class="col-grid">
                            <a href="<?php echo $link_url_5;?>" title="<?php echo $titulo_5;?>">
                                <div class="card">
                                    <div class="card__main-content">
                                        <?php if($imagen_5): ?>
                                            <div class="icon-wrapper">
                                                <?php insert_image($imagen_5, '400') ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                          insert_acf($titulo_5, 'h2');
                                        ?>
                                    </div>
                                    <div class="plus-icon-wrapper">
                                                    <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                    <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                    <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                    </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>