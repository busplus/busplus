<?php 
/*
 * Template Name: Plantilla Via Club
 * 
 */
?>

<?php get_header(); ?>
<!-- Fin de la tarea GDM-2539 -->
<style>
  input[type=text],
  input[type=password],
  select {
    -webkit-border-radius: 16px;
    -moz-border-radius: 16px;
    border-radius: 16px;
    border: 1px solid #e6e6e6;
    color: black;
    width: 250px;
    height: 45px;
    padding-left: 16px;
    padding-right: 16px;

  }
  
  input[type=text]:focus,
  input[type=password]:focus,
  select:focus {
    outline: none;
    border: 1px solid #a0d18c;
    color: black;
  }
  .title-via-desktop {
    display:block;
  }
  .title-via-mobile {
    display:none;
  }
  /* Estilos para el modo mobile */
	@media only screen and (max-width: 700px) {
		.title-via-desktop {
      display:none;
    }
    .title-via-mobile {
      display:block;
    }
	}
</style>
<!-- Fin de la tarea GDM-2539 -->

<main class="single single-viaclub">
    <section class="hero">

        <div class="container">
          <div class="row">
              <div class="col-sm-6 col-12"></div>
                <div class="col-sm-6 col-12 sectionQueEsViaClub">
                    <?php if (have_rows('que_es')): ?>
                        <?php while (have_rows('que_es')) : the_row(); ?>
                            <?php if (get_row_layout() == 'que_es_via_club') : ?>
                                <div class="content">   
                                        <div class="title">
                                            <h1 class="f-28-20 f-extrabold"><?php the_sub_field('titulo'); ?></h1>
                                        </div>
                                        <?php the_sub_field('texto'); ?>
                                        <div class="buttons">
                                                <a href="https://socios.busplus.com.ar/socios/adhesion/titular" class="btn btn-orange" target="_blank">
                                                Registrate
                                                </a>
                                                <button type="button" class="btn btn-orange btnIngresar" href="#">
                                                    Ingresar
                                                </button>
                                        </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                    <?php // no layouts found?>
                    <?php endif; ?>
                </div>
                <!-- Inicio de la tarea GDM-2539 -->
                <div class="col-sm-6 col-12 sectionLoginViaClub d-none">
                  <form id="frmLoginSocio" name="frmLoginSocio" action="https://socios.busplus.com.ar/socios/login" method="post" accept-charset="utf-8">
                    <div class="title-via-desktop">
                        <div div class="title">
                           <h2 class="f-28-20 f-extrabold mb-4">Ingresa a Vía Club</h2>
                        </div>
                    </div>
                    <div class="title-via-mobile">
                        <div div class="title">
                           <h2 class="f-28-20 f-extrabold mb-4">Ingresa a <br> Vía Club</h2>
                        </div>
                    </div>

                    <div class="form-wrapper">
                      <select name="tipo" class="form-select mb-1" style="cursor:pointer;">
                        <option value="1">DNI</option>
                        <option value="2">CI</option>
                        <option value="3">Pasaporte</option>
                        <option value="4">LE</option>
                        <option value="5">LC</option>
                        <option value="6">Otro</option>
                        <option value="8">Cuit/Cuil</option>
                        <option value="9">R.U.N.</option>
                      </select>
                    </div>
                    <div class="form-wrapper">
                      <input type="text" id="nro" name="nro" placeholder="Numero" class="form-control mt-2 mb-1" required>
                      <h5 class="msgEmptyNro d-none" style="color:red;font-size: .8em;">Debe completar este campo.</h5>
                    </div>
                    <div class="form-wrapper">
                      <input type="password" id="password" name="password" placeholder="Contraseña" class="form-control mt-2 mb-1" required>
                      <h5 class="msgEmptyPassword d-none" style="color:red;font-size: .8em;">Debe completar este campo.</h5>
                    </div>
                    <div class="button__container btnSubmitLogin mt-2">
                      <a class="btn btn-orange" href="#">Ingresar</a>
                    </div>
                    <div class="form-wrapper mt-3">
                      <a class="f-bold text-orange" href="https://socios.busplus.com.ar/socios/nueva_contrasena">&iquest;Olvidaste tu contrase&ntilde;a?</a>
                    </div>
                  </form>
                </div>
                <!-- Fin de la tarea GDM-2539 -->
          </div>
        </div>
    </section>
    <section class="points">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php if (have_rows('consigo_puntos')): ?>
              <?php while (have_rows('consigo_puntos')) : the_row(); ?>
                <?php if (get_row_layout() == 'logos_puntos') : ?>
                  <div class="block-title">
                    <h2 class="f-24-18 f-bold"><?php the_sub_field('titulo'); ?></h2>
                  </div>
         
                  <p><?php the_sub_field('subtitulo'); ?></p>
                  <?php $logos_images = get_sub_field('logos'); ?>
                  <div class="logo-gal">
                    <?php if ($logos_images) :  ?>
                      <?php foreach ($logos_images as $logos_image): ?>
                        <div class="cont-logo-gal ">
                        <img src="<?php echo esc_url($logos_image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($logos_image['alt']); ?>" />
                      </div>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else: ?>
              <?php // no layouts found?>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-end">
            <?php the_field('texto_medio'); ?>
          </div>
        </div>
      </div>
    </section>
    <section class="my-points">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php if (have_rows('como_uso_mis_puntos')): ?>
              <?php while (have_rows('como_uso_mis_puntos')) : the_row(); ?>
                <?php if (get_row_layout() == 'bloque_uso_mis_puntos') : ?>
                  <div class="block-title">
                    <h2 class="f-24-18 f-bold"><?php the_sub_field('titulo'); ?></h2>
                  </div>
                  <?php if (have_rows('box_boton')) : ?>
                    <div class="box-block row">
                      <?php while (have_rows('box_boton')) : the_row(); ?>
                        <div class="box-wrapper col-sm-6 col-12">
                          <p><?php the_sub_field('texto'); ?></p>
                          <div class="button__container">
                            <a class="btn btn-orange" href="https://socios.busplus.com.ar/pasajes" target="_blank">Ver puntos</a>
                          </div>
                        </div>
                      <?php endwhile; ?>
                    </div>
                  <?php else : ?>
                    <?php // no rows found?>
                  <?php endif; ?>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else: ?>
              <?php // no layouts found?>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <p><?php the_field('texto_abajo'); ?></p>
          </div>
        </div>
      </div>
    </section>

</main>
<script>
document.addEventListener("DOMContentLoaded", function() {
    //-- Inicio de la tarea GDM-2539 --//

    // Evento que al hacer click al boton de "Ingresar" muestra el login y oculta lo anterior.
    document.querySelector('.btnIngresar').addEventListener('click', function() {
        document.querySelector('.sectionQueEsViaClub').classList.add('d-none');
        const sectionLogin = document.querySelector('.sectionLoginViaClub');
        sectionLogin.classList.remove('d-none');
        sectionLogin.classList.add('d-flex', 'justify-content-center');
    });

    document.querySelector('.btnSubmitLogin').addEventListener('click', function() {
        const nroInput = document.getElementById('nro');
        const passwordInput = document.getElementById('password');
        const frmLoginSocio = document.getElementById("frmLoginSocio");

        // Si los campos nrodocumento y password no están vacíos, hace el submit del formulario.
        if (nroInput.value.length > 1 && passwordInput.value.length > 0) {
            frmLoginSocio.submit();
        } 
        
        // Si el campo nrodocumento está vacío, arroja un error.
        if (nroInput.value.length < 1) {
            nroInput.style.border = "1px solid red";
            document.querySelector('.msgEmptyNro').classList.remove('d-none');
        } else {
            nroInput.style.border = "1px solid #2d9fd9";
            document.querySelector('.msgEmptyNro').classList.add('d-none');
        }

        // Si el campo password está vacío, arroja un error.
        if (passwordInput.value.length < 1) {
            passwordInput.style.border = "1px solid red";
            document.querySelector('.msgEmptyPassword').classList.remove('d-none');
        } else {
            passwordInput.style.border = "1px solid #2d9fd9";
            document.querySelector('.msgEmptyPassword').classList.add('d-none');
        }
    });
    //-- Fin de la tarea GDM-2539 --//
});
</script>

<?php get_footer(); ?>