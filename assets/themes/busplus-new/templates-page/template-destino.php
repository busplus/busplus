<?php 

/*
 * Template Name: Plantilla Destino
 * 
 */
?>
<?php 
    $titulo = get_field('titulo');
    $titulo_negrita = get_field('titulo_en_negrita');

    // Obtener términos de la taxonomía 'ubicaciones'
    $terms = get_terms(array(
        'taxonomy' => 'ubicaciones',
        'hide_empty' => false, // Cambia a true si solo quieres términos que tienen posts asociados
    ));



?>

<?php get_header(); ?>

<main class="single single-terminal">
    <section class="hero-simple" style="background-image:url(<?php echo get_the_post_thumbnail_url()?>);">
        <div class="container">
            <div class="hero-content">
                <h1 class="f-28-20 f-extrabold">
                    <?php echo $titulo;?>
                    <?php if($titulo_negrita): ?>
                    <br><strong><?php echo $titulo_negrita;?></strong>
                    <?php endif; ?>
                </h1>
            </div>
        </div>
    </section>
    <section class="lists-terminales">
        <div class="container">
            <div class="row row-destino">
                <?php if (!empty($terms) && !is_wp_error($terms)) : ?>
                    <?php foreach ($terms as $term) : ?>
                        <?php 
                                $term_id = $term->term_id;
                                // Consulta para obtener el post padre relacionado con el término actual
                                $parent_post = get_posts(array(
                                    'post_type' => 'destino',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'ubicaciones',
                                            'field' => 'term_id',
                                            'terms' => $term_id,
                                        ),
                                    ),
                                    'post_parent' => 0, // Solo obtener posts padres
                                    'posts_per_page' => 1, // Solo necesitamos un post padre
                                ));
                            
                                // Verificar si se encontró un post padre
                                if (!empty($parent_post)) {
                                    $parent_post_id = $parent_post[0]->ID; // ID del post padre
                                    $parent_post_link = get_permalink($parent_post_id);

                                    // Obtener la miniatura del post padre
                                    $parent_post_thumbnail = get_the_post_thumbnail($parent_post_id, 'large'); // Puedes cambiar 'thumbnail' por otro tamaño si lo deseas
                
                                } else {
                                    $parent_post_link = '#'; // Fallback si no se encuentra un post padre
                                }
                            
                            
                            
                            ?>
                        <?php if($parent_post_link !== '#'): ?>
                            <div class="col-sm-4 col-xs-6 col-12 card-destino">
                                <a href="<?php echo esc_url($parent_post_link); ?>" class="card" aria-label="Hace click a <?php echo esc_attr($term->name); ?>">
                                    <?php 
                                        // Obtener el ID del término actual
                                        $term_id = $term->term_id;
                                       
                                    ?>
                                    <div class="block-image">
                                        <?php if($parent_post_thumbnail ): ?>
                                            <?php echo $parent_post_thumbnail; ?>                            
                                            <?php else: ?>
                                                <?php 
                                                    $terminal_titulo="Destino $term->name";
                                                    $terminal_alt = "En la imágen se muestra la foto de $term->name";
                                                 ?>
                                                <?php echo image_custom($terminal_alt, $terminal_titulo, ""); ?>
                                        <?php endif; ?>

                                    </div>
                                 
                                    <div class="block-content">
                                        <h3><?php echo esc_html($term->name); ?></h3>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p class="error-msg">No se encontraron destinos.</p>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <?php show_block_buscador(true); ?>
</main>

<?php get_footer(); ?>