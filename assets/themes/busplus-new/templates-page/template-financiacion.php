<?php 

/*
 * Template Name: Plantilla Financiacion
 * 
 */
?>
<?php get_header();?>
<?php 
   
    $args = array(
        'post_type' => 'financiacion-slug',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $query = new WP_Query($args);
?>
<?php if ( have_rows( 'financiacion' ) ) : ?>
    <?php while ( have_rows( 'financiacion' ) ) :
    the_row(); ?>
        <?php  $titulo = get_sub_field('titulo'); ?>
        <main class="single single-promocion single-financiacion">
            <div class="container container-grid">
                <div class="title">
                    <?php insert_acf($titulo, 'h1') ?>
                </div>
                <div class="row row-cards">
                    <?php if ($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php 
                                            $titulo = get_field('titulo');
                                            $texto = get_field('texto');
                                            $fecha = get_field('fecha');
                                            $link = get_field('link')['url'];
                                        ?>
                                <?php if($titulo): ?>
                                    <div class="col-sm-4 col-6 col-12">
                                            <a href="<?php echo $link; ?>" class="card" aria-label="<?php echo $titulo;?>">
                                                <div class="content">
                                                    <div class="image">
                                                        <?php the_post_thumbnail(); ?>
                                                    </div>
                                                    <span><?php echo $titulo ;?></span>
                                                    <h2><?php echo $texto ;?></h2>
                                                    <p>
                                                        <?php echo $fecha ;?>
                                                    </p>

                                                    <div class="button__container d-flex justify-center align-items-center">
                                                        <button type="button" class="btn btn-orange">
                                                            Condiciones
                                                        </button>
                                                    </div>
                                                </div>
                                            </a>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            <?php  wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php show_block_buscador(true); ?>
        </main>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>