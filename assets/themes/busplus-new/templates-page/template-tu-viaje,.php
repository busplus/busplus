<?php 
/*
 * Template Name: Plantilla Tu viaje
 * 
 */
?>
<?php get_header(); ?>
<?php  
    $titulo = get_field( 'titulo' );
    $texto = get_field('texto');

?>
<main class="single single-default single-tu-viaje">
    <div class="container">
        <div class="title">
            <?php insert_acf($titulo, 'h1') ?>
        </div>
        <div class="single-body">
            <?php echo $texto; ?>
        </div>
        <form id="tu-viaje">
            <div class="field-input">
                <label for="serie">SERIE. EJEMPLO: VB0 (CERO), W, VTI, ETC</label>
                <input type="text" required id="serie">
            </div>
            <div class="field-input">
                <label for="boleto">NÚMERO DE BOLETO</label>
                <input type="text" required id="boleto">
            </div>
            <div class="field-submit">
                <button type="submit" class="btn btn-orange btn-md">
                    BUSCAR
                </button>
            </div>
        </form>

    </div>
</main>
<?php get_footer(); ?>