<?php 

/*
 * Template Name: Plantilla Blog
 * 
 */
?>

<?php get_header(); ?>
<?php 
    $categoria = isset($_GET['categoria']) ? $_GET['categoria'] : null;

?>

<main class="single layout-default blog pb-0">
    <div class="container">
  
        <?php
        $titulo = get_field('titulo'); // Obtiene el título desde los campos personalizados

        if ($titulo): ?>
            <div class="title">
                <?php insert_acf($titulo, 'h1'); ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-12">
                <?php if($categoria): ?>
                    <?php show_category_blog($categoria); ?>
                    <?php else: ?>
                        <?php show_category_blog($categoria); ?>
                <?php endif; ?>
            </div>

            <div class="col-12 list-posts">
                <?php if($categoria): ?>
                    <?php echo show_posts_blog($categoria);  ?>
                    <?php else: ?>
                        <?php echo show_posts_blog();  ?>    
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php show_block_buscador(true); ?>
</main>



<?php get_footer(); ?>