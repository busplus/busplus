<?php 

/*
 * Template Name: Pagina SEO
 * Template Post Type: destino
 */
?>

<?php get_header(); ?>
<?php 
$post_id = get_the_ID();
$titulo = get_field( 'titulo');
$titulo_negrita = get_field( 'titulo_en_negrita');
$image_feature = get_the_post_thumbnail_url();
$target = get_ubicaciones_post()->name;
$texto_editor = get_field( 'texto_editor' );
$image_url = IMAGE . '/destinos/destinos-hero.jpg'; // Cambia la ruta según tu estructura de carpetas
// Verificar si el post tiene una imagen destacada
if (has_post_thumbnail($post_id)) {
    // Obtener la URL de la imagen destacada
    $image_url = $image_feature;
} else {
    // Fallback a una imagen por defecto
    $image_url =  IMAGE . '/destinos/destinos-hero.jpg'; // Cambia la ruta según tu estructura de carpetas
}

?>


<main class="single single-destino destino-principal">
    <section class="hero-simple" style="background-image:url(<?php echo $image_url;?>);">
        <div class="container">
            <div class="hero-content">
                <h1 class="f-28-20 f-extrabold">
                        <?php echo $titulo;?>
                        <?php if($titulo_negrita): ?>
                            <strong><?php echo $titulo_negrita;?></strong> en oferta
                        <?php endif; ?>
                </h1>
            </div>
        </div>
    </section>
    <?php show_block_buscador(); ?>
    <?php if($texto_editor): ?>
        <section class="single-body single-text-pagina-seo">
            <div class="container">
                <div class="content"><?php echo $texto_editor; ?></div>
            </div>
        </section>
    <?php endif; ?>
    <section class="destinos-relacionados">
            <div class="container">
                <h2 class="f-24-18 f-bold mb-xs-2 mb-1">Planea tu próximo viaje comprando pasajes en oferta a <?php echo $target;?> </h2>
                <?php echo get_landing_seo_nivel_2(); ?>
                <h2 class="f-24-18 f-bold mb-xs-2 mb-1">Pasajes a <?php echo $target;?> más buscados</h2>
                <?php echo get_landing_seo_nivel_3(); ?>
            </div>
    </section>
    <?php  #render_block_destino_actual(); ?>
    <?php echo render_block_destino('Nuestros destinos a la costa más populares', 'bg-gray-secondary'); ?>
</main>

<?php get_footer(); ?>