<?php 

/*
 * Template Name: Plantilla Terminal
 * 
 */
?>
<?php 
    $titulo = get_field('titulo');
    $titulo_negrita = get_field('titulo_en_negrita');
    $args = array(
        'post_type' => 'terminal',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC'
    );
    $query = new WP_Query($args);

?>
<?php get_header(); ?>

<main class="single single-terminal pantalla-principal-terminal">
    <section class="hero-simple" style="background-image:url(<?php echo get_the_post_thumbnail_url()?>);">
        <div class="container">
            <div class="hero-content">
                <h1 class="f-28-20 f-extrabold">
                    <?php echo $titulo;?>
                    <?php if($titulo_negrita): ?>
                    <strong><?php echo $titulo_negrita;?></strong>
                    <?php endif; ?>
                </h1>
            </div>
        </div>
    </section>
    <section class="lists-terminales">
        <div class="container">
            <div class="row row-destino">
                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-sm-4 col-xs-6 col-12 card-destino">
                        <?php 
                            $ubicaciones = get_the_terms(get_the_ID(), 'ubicacion');
                        ?>
                        <a href="<?php echo get_the_permalink();?>" class="card"
                            aria-label="Hace click <?php the_title(); ?>">
                            <div class="block-image">
                                <?php if(get_the_post_thumbnail()): ?>
                                    <?php the_post_thumbnail(); ?>
                                <?php else: ?>
                                    <?php 
                                        $image_url = IMAGE . '/destinos/terminales-card.jpg';
                                        $terminal_titulo=get_the_title();
                                        $terminal_alt = "En la imágen se muestra la foto de la $term->name" . $terminal_titulo;
                                    ?>
                                    <?php echo image_custom($terminal_alt, $terminal_titulo, "", $image_url); ?>
                                <?php endif; ?>
                            </div>
                            <div class="block-content">
                                <h3><?php the_title(); ?></h3>
                                <?php if($ubicaciones && !is_wp_error($ubicaciones)): ?>
                                    <p>
                                    <?php foreach ($ubicaciones as $ubicacion): ?>
                                        <?php echo $ubicacion->name; ?>
                                    <?php endforeach; ?>
                                    </p>
                                <?php endif; ?>

                            </div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                <?php else: ?>

                    <p class="error-msg">No se encontraron terminales.</p>
           
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </div>
        </div>
    </section>
    <?php show_block_buscador(true); ?>
</main>


<?php get_footer(); ?>