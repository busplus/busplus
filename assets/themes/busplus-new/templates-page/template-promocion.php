<?php 

/*
 * Template Name: Plantilla Promocion
 * 
 */
?>
<?php get_header();?>
<?php 
    $titulo = get_field('titulo');
    $args = array(
        'post_type' => 'promociones',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
    );
    $query = new WP_Query($args);
?>
<main class="single single-promocion">
    <div class="container container-grid">
        <div class="title">
            <?php insert_acf($titulo, 'h1') ?>
        </div>
        <div class="row">
            <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <?php 
                                    $titulo = get_field('titulo');
                                    $oferta = get_field('oferta');
                                    $texto = get_field('texto');
                                ?>
                        <?php if($titulo): ?>
                            <div class="col-sm-4 col-6 col-12">

                    
                                    <a href="<?php echo get_the_permalink()?>" class="card" aria-label="<?php echo $titulo;?>">
                                        <div class="content">
                                            <span><?php echo $oferta ;?></span>
                                            <h2><?php echo $titulo ;?></h2>
                                            <p>
                                                <?php echo $texto ;?>

                                            </p>
                                            <div class="button__container d-flex justify-center align-items-center">
                                                <button type="button" class="btn btn-orange">
                                                    Ver más
                                                </button>
                                            </div>
                                        </div>
                                    </a>
                            
                            
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    <?php  wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
    <?php show_block_buscador(true); ?>
 
</main>

<?php get_footer(); ?>