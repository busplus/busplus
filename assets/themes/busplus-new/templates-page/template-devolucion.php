<?php 

/*
 * Template Name: Plantilla Contacto
 * 
 */
?>

<?php get_header(); ?>
<?php $titulo = get_field('titulo') ?>
<?php $texto = get_field('texto') ?>
<main class="single single-default single-contacto">
    <div class="container">
        <div class="title">
            <?php insert_acf($titulo, 'h1') ?>
        </div>
        <div class="single-body">
            <?php echo $texto; ?>
        </div>
        <div class="card">
            <?php if ( have_rows( 'tarjeta' ) ) : ?>
                <?php while ( have_rows( 'tarjeta' ) ) :
                the_row(); ?>

                    <?php 
                    $link = get_sub_field( 'link' );

                    $imagen = get_sub_field( 'imagen' );
                    ?>
                    <div class="info">
                        <ul>
                        <?php if ( have_rows( 'info' ) ) : ?>
                            <?php while ( have_rows( 'info' ) ) :
                            the_row(); ?>
                                <?php  
                                    $texto = get_sub_field('texto');
                                    $icono = get_sub_field('icono');
                                ?>
                                 <li>
                                    <?php
                                          insert_image($icono, 1024);
                                          echo $texto;
                                    ?>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                 

                        <li>
                            <?php insert_button( $link , 0 , 'btn-orange') ?>
                        </li>

                  
                        </ul>
                    </div>


                <div class="image">
                    <?php insert_image($imagen, 1024); ?>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="form-widget">
            <style type="text/css" media="screen, projection">
                 @import url(https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css);
            </style>
            <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
            <iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://wcentrix.net/app/form_web.html?accountID=Va4488&wcboxID=996d7eaebc954d46a79af8e7017ebd19" scrolling="no" height="750px" width="100%" frameborder="0" ></iframe>
        </div>
    </div>
</main>

<?php get_footer(); ?>