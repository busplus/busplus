<?php 
/*
 * Template Name: Plantilla Quienes Somos
 * 
 */
?>
<?php get_header(); ?>

<main class="single single-nosotros">
        <?php if ( have_rows( 'portada' ) ) : ?>
            <?php while ( have_rows( 'portada' ) ) :
            the_row(); ?>
                <?php 
                $title = get_sub_field('titulo');
                $image = get_sub_field( 'image' )['url'];
                $image_2 = get_sub_field( 'image_2' )['url'];
                ?>

                <section 
                    class="hero"
                    data-image-desktop="<?php echo esc_url( $image ); ?>" 
                    data-image-mobile="<?php echo esc_url( $image_2 ); ?>"
                    style="--background-image-desktop: url(<?php echo esc_url( $image ); ?>);--background-image-mobile: url(<?php echo esc_url( $image_2 ); ?>);"
                    >
                    <div class="container">
                        <div class="hero-content">
                            <?php insert_acf($title, 'h1', 'f-28-20 f-extrabold')?>
                        </div>
                    </div>

                </section>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'introduccion' ) ) : ?>
            <?php while ( have_rows( 'introduccion' ) ) :
            the_row(); ?>
                <section class="block-intro">
                    <div class="container-sm">
                        <div class="block-text-content">
                            <?php if ( $texto = get_sub_field( 'texto' ) ) : ?>
                                <?php echo $texto; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'dos_columnas' ) ) : ?>
            <?php while ( have_rows( 'dos_columnas' ) ) :
            the_row(); ?>
                <section class="block-two-columns">
                    <div class="container">     
                        <div class="row">
                                <div class="col-xs-6 col-12">
                                    <div class="image">
                                        <?php  
                                            $imagen_1 = get_sub_field( 'imagen_1' ); 
                                            insert_image($imagen_1, 1024);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-12">
                                    <div class="content">
                                        <?php if ( $texto = get_sub_field( 'texto' ) ) : ?>
                                            <?php echo $texto; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'dos_columnas_2' ) ) : ?>
            <?php while ( have_rows( 'dos_columnas_2' ) ) :
            the_row(); ?>
                <section class="block-two-columns">
                    <div class="container">     
                        <div class="row">
                                <div class="col-xs-6 col-12">
                                    <div class="content">
                                        <?php if ( $texto = get_sub_field( 'texto' ) ) : ?>
                                            <?php echo $texto; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-12">
                                    <div class="image">
                                        <?php 
                                            $imagen = get_sub_field( 'imagen' ); 
                                            insert_image($imagen, 1024);
                                        ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'tarjetas' ) ) : ?>
            <?php while ( have_rows( 'tarjetas' ) ) :
            the_row(); ?>
                <section class="block-cards">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-12">
                                <div class="card">
                                    <div class="image">
                                        <?php
                                        $imagen = get_sub_field( 'imagen' );
                                        insert_image($imagen, 1024)
                                        ?>
                                    </div>
                                    <div class="content">
                                        <?php if ( $texto = get_sub_field( 'texto' ) ) : ?>
                                            <?php echo $texto; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-12">
                                <div class="card">
                                    <div class="image">
                                        <?php
                                        $imagen_2 = get_sub_field( 'imagen_2' );
                                        insert_image($imagen_2, 1024)
                                        ?>
                                    </div>
                                    <div class="content">
                                        <?php if ( $texto_2 = get_sub_field( 'texto_2' ) ) : ?>
                                            <?php echo $texto_2; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'dos_columnas_3' ) ) : ?>
            <?php while ( have_rows( 'dos_columnas_3' ) ) :
            the_row(); ?>
                <section class="block-two-columns bg-white">
                    <div class="container">     
                        <div class="row">
                                <div class="col-xs-6 col-12">
                                    <div class="image">
                                        <?php  
                                            $imagen_2 = get_sub_field( 'imagen_2' ); 
                                            insert_image($imagen_2, 1024);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-12">
                                    <div class="content">
                                        <?php if ( $texto = get_sub_field( 'texto' ) ) : ?>
                                            <?php echo $texto; ?>
                                        <?php endif; ?>
                                        <?php  
                                            $link = get_sub_field( 'link' );
                                            insert_button($link, 0, 'btn-orange' )
                                        ?>
                                       
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>
</main>
<?php get_footer(); ?>