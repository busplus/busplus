<?php 
/*
 * Template Name: Plantilla Como Comprar
 * 
 */
?>


<?php get_header();?>
<?php $titulo = get_field('titulo'); ?>
<main class="single single-default single-como-comprar">
    <div class="container">
        <div class="title mb-80">
            <?php insert_acf($titulo, 'h1') ?>
        </div>
        <div class="cards-wrapper">

            <?php $count = 0; ?>
            <?php if ( have_rows( 'tarjeta' ) ) : ?>
                <?php while ( have_rows( 'tarjeta' ) ) :
                the_row(); ?>
                    <?php $count++ ?>
                    <?php $texto = get_sub_field( 'texto' ); ?>
                    <?php $imagen = get_sub_field( 'imagen' ); ?>
                    <div class="card">
                        <div class="img-wrapper">
                            <?php insert_image($imagen, 1024); ?>
                        </div>
                        <div class="card-number">
                            <?php echo $count ?>
                        </div>
                        <div class="card-content">
                            <?php echo $texto; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

    </div>                

    
</main>
<?php get_footer(); ?>