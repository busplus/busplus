<?php 

/*
 * Template Name: Plantilla Preguntas Frecuentes
 * 
 */
?>
<?php get_header();?>
<?php 
    $args = array(
        'post_type' => 'faq',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $query = new WP_Query($args);
?>
    <?php  $titulo = get_field('titulo'); ?>
    <main class="single single-default single-faqs">
            <div class="container">
                <div class="title mb-80">
                    <?php insert_acf($titulo, 'h1') ?>
                </div>
                <ul>
                    <?php if ($query->have_posts()) : ?>
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <li class="faq-according">
                                <div class="faq-according-title">
                                    <h2>
                                        <?php the_title();?>
                                        <div class="circle">
                                            <i class="fa-solid fa-chevron-down"></i>
                                        </div>
                                    </h2>
                                </div>
                                <div class="faq-according-info single-body">
                                    <?php the_content(); ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                        <?php  wp_reset_postdata(); ?>
                    <?php endif; ?>
                </ul>
            </div>
    </main>
<?php get_footer(); ?>