<?php 
/*
 * Template Name: Plantilla Tu viaje
 * 
 */
?>
<?php get_header(); ?>
<?php  
    $titulo = get_field( 'titulo' );
    $texto = get_field('texto');
?>

<?php 
    

?>
<main class="single single-default single-tu-viaje">
    <div class="form-tu-viaje">
        <div clasS="container">
            <div class="title">
                <?php insert_acf($titulo, 'h1') ?>
            </div>
            <div class="single-body">
                <?php echo $texto; ?>
            </div>
            <form id="tu-viaje">
                <div id="error-container"></div>
                <div class="field-input">
                    <label for="serie">
                        SERIE. EJEMPLO: VB0 (CERO), W, VTI, ETC
                        <span class="error"></span>
                    </label>
                    <input type="text" id="serie" value="<?php echo $_GET['serie'] ?? '' ?>">
                </div>
                <div class="field-input">
                    <label for="boleto">
                        NÚMERO DE BOLETO
                        <span class="error"></span>
                    </label>
                    <input type="text" id="boleto" value="<?php echo $_GET['boleto'] ?? '' ?>">
                </div>
                <div class="field-submit">
                    <button type="submit" id="send-viaje" class="btn btn-orange btn-md">
                        Buscar
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="details-tu-viaje d-none">
        <div class="container-md">
            <div class="title">
                <h2>Detalles de tu viaje</h2>
            </div>
            <div class="card-container p-relative" data-link-form="<?php echo get_the_permalink();?>"></div>
        </div>
    </div>
</main>

<?php get_footer(); ?>