document.addEventListener("DOMContentLoaded", function() {
    const terminalesContainer = document.getElementById("terminalesContainer");
    const searchInput = document.getElementById("searchInput");
    const pagination = document.getElementById("pagination");
    let terminales = JSON.parse(terminalesContainer.getAttribute("data-terminales"));
    let currentPage = 1;
    const itemsPerPage = 50;

    function displayResults() {
        const filtered = terminales.filter(terminal => 
            terminal.descripcion_parada.toLowerCase().includes(searchInput.value.toLowerCase())
        );

        const start = (currentPage - 1) * itemsPerPage;
        const end = start + itemsPerPage;
        const paginated = filtered.slice(start, end);

        const tbody = document.querySelector("#terminalesContainer table tbody");
        tbody.innerHTML = "";
        paginated.forEach(terminal => {
            const row = document.createElement("tr");
            row.innerHTML = `<td>${terminal.idparada}</td><td>${terminal.descripcion_parada}</td>`;
            tbody.appendChild(row);
        });

        updatePagination(filtered.length);
    }

    function updatePagination(totalItems) {
        const totalPages = Math.ceil(totalItems / itemsPerPage);
        const maxLinks = 5; // Número de enlaces de páginas a mostrar

        pagination.innerHTML = "";



        // Enlace para la página anterior
        if (currentPage > 1) {
            const prevPageLink = document.createElement("a");
            prevPageLink.href = "#";
            prevPageLink.dataset.page = currentPage - 1;
            prevPageLink.className = "page-link btn-prev"; // Añadir clase btn-prev
            prevPageLink.textContent = "<";
            prevPageLink.addEventListener("click", function(event) {
                event.preventDefault();
                currentPage = currentPage - 1;
                displayResults();
            });
            pagination.appendChild(prevPageLink);
        }

        // Páginas alrededor de la actual
        for (let i = Math.max(1, currentPage - Math.floor(maxLinks / 2)); i <= Math.min(totalPages, currentPage + Math.floor(maxLinks / 2)); i++) {
            const pageLink = document.createElement("a");
            pageLink.href = "#";
            pageLink.dataset.page = i;
            pageLink.className = "page-link";
            pageLink.textContent = i;
            if (i === currentPage) {
                pageLink.style.fontWeight = "bold";
            }
            pageLink.addEventListener("click", function(event) {
                event.preventDefault();
                currentPage = parseInt(this.dataset.page);
                displayResults();
            });
            pagination.appendChild(pageLink);
            pagination.appendChild(document.createTextNode(" "));
        }

        // Enlace para la página siguiente
        if (currentPage < totalPages) {
            const nextPageLink = document.createElement("a");
            nextPageLink.href = "#";
            nextPageLink.dataset.page = currentPage + 1;
            nextPageLink.className = "page-link btn-next"; // Añadir clase btn-next
            nextPageLink.textContent = ">";
            nextPageLink.addEventListener("click", function(event) {
                event.preventDefault();
                currentPage = currentPage + 1;
                displayResults();
            });
            pagination.appendChild(nextPageLink);
        }


    }

    searchInput.addEventListener("input", function() {
        currentPage = 1; // Reset to first page on search
        displayResults();
    });

    displayResults(); // Initial display
});