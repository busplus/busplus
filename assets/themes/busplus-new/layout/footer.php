
<footer>
    <div class="container">
        <div class="footer__social-media-content">
            <div class="logo">
                <?php display_custom_logo_black(); ?>
            </div>
            <div class="footer__logos-wrapper">
                <?php if ( $facebook = get_field( 'facebook', 'options' ) ) : ?>
                    <a href="<?php echo esc_url( $facebook ); ?>" aria-label="Facebook" target="_blank">
                        <i class="fa-brands fa-square-facebook"></i>
                    </a>
                <?php endif; ?>
                <?php if ( $instagram = get_field( 'instagram', 'options' ) ) : ?>
                   
                    <a href="<?php echo esc_url( $instagram ); ?>" aria-label="Instagram" target="_blank">
                        <i class="fa-brands fa-instagram"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="content-wrapper">
                <?php 
                    if (has_nav_menu('main-menu')) {
                        wp_nav_menu(array(
                            'theme_location' => 'main-footer',
                            'menu' => '',
                            'menu_class' => 'menu',
                            'menu_id' => 'menu-menu',
                            'container_class' => 'menu__container',
                            'walker' => new Walker_Zetenta_Menu_Footer()
                     
                        ));
                    }
                ?>
            </div>
    </div>
    <?php get_template_part( 'template-parts/content', 'creditos');?></footer>
</footer>

<?php if ( $link = get_field( 'link', 'options' ) ) : ?>
 
    <a href="<?php echo esc_url( $link ); ?>" target="_blank" class="whatsapp-button">
        <i class="fa-brands fa-whatsapp"></i>
    </a>
<?php endif; ?>


