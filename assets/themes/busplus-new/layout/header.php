<header class="header">
    <?php if ( get_field( 'mostrar_alerta', 'options' ) ) : ?>
        <div class="disclaimer">
            <img loading="lazy" width="13" height="13" src="<?php echo IMAGE;?>/disclaimer-icon.png" alt="disclaimer-icon" alt="Icono Amarillo de advertencia."/>
            <?php if ( $topbar = get_field( 'topbar', 'options' ) ) : ?>
                <?php echo $topbar; ?>
            <?php endif; ?>
        </div>
    
    
    <?php endif; ?>

    <nav class="container">
        <div class="header__logo-container">
            <?php display_custom_logo(); ?>
        </div>
        <div class="menu-links-container">
            <div class="heading-wrapper">
                <div class="logo-mobile">
                    <?php display_custom_logo_black(); ?>
                </div>
                <div class="actions-mobile d-none-md hamburger-mobile">
                    <div class="hamburger hamburger--slider">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                    if (has_nav_menu('main-menu')) {
                        wp_nav_menu(array(
                            'theme_location' => 'main-menu',
                            'menu' => '',
                            'menu_class' => 'menu',
                            'menu_id' => 'menu-menu',
                            'container_class' => 'menu__container',
                            'walker' => new Walker_Zetenta_Menu()
                     
                        ));
                    }
                ?>

        </div>
        <div class="actions-mobile d-none-md">
            <div class="hamburger hamburger--slider">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </div>
    </nav>
</header>