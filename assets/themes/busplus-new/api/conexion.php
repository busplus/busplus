<?php

class WebService {
	private $entorno = "prod";
	private $usuario = '';
	private $clave = '';
	private $clave_acceso = '';
	private $url = '';
	var $key = null;
	var $errores = null;

	function __construct() {
		$this->url = API_URL;
		$this->clave_acceso =  "2b3019437fb6880b02a"; // "qu3r1c4qu3s0s"; // "8daf7shf8ahgf8g8";

		$this->sesion();
	}
	
	/**
	 *  funcion para hacer llamadas al WS, ya internamente tiene la api-key
	 *  url = url del webservice que se desea ejecutar, por ejemplo: sesion
	 *  tipo = el tipo de peticion GET o POST
	 *  retorna un objeto de la llamada al web service
	 * */
	function call_curl_sesion($url, $tipo = "GET"){
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->url.$url,
		  CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('clave_acceso' => $this->clave_acceso),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		return json_decode($response);
	}
	
	/**
	 *  funcion para hacer llamadas al WS, ya internamente tiene la api-key
	 *  url = url del webservice que se desea ejecutar, por ejemplo: sesion
	 *  tipo = el tipo de peticion GET o POST
	 *  retorna un objeto de la llamada al web service
	 * */
	function call_curl($url, $tipo = "GET"){
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->url.$url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $tipo,
		  
		  CURLOPT_HTTPHEADER => array(
			'X-API-KEY: '.$this->key,
    		'Content-Type: application/json'
		  ),
		));
		
		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response);
	}
	
	private function sesion(){
				
		$valores = $this->call_curl_sesion("sesion", "POST");		
		
		if(isset($valores) and isset($valores->message)){
			$this->errores = $valores->message;
		}
		else{
			if(isset($valores->key)){ 
				$this->key = $valores->key;
			}
			else{
				$this->errores = "ERROR, no se pudo conectar al WS";
			}			
		}
	}
	
	public function getKey(){
		return $this->key;
	}
	
	public function getUrl(){
		return $this->url;
	}
}


// // Manejo de la solicitud
// if ($_SERVER['REQUEST_METHOD'] === 'GET') {
//     if (!empty($_GET["serie"]) && !empty($_GET["boleto"])) {
//         $webService = new WebService();
//         $url = "viajes/estado_viaje?serie=" . htmlspecialchars($_GET['serie']) . "&boleto=" . htmlspecialchars($_GET['boleto']);
//         $data = $webService->call_curl($url);

//         // Procesa la respuesta y genera el HTML
//         if (isset($data->boleto)) {
//             $fecha_actual = new DateTime();
//             $fecha_salida = new DateTime($data->boleto->fechasalida);
//             $fecha_llegada = new DateTime($data->boleto->fechallegada);
//             $html = '';
//             if ($fecha_actual > $fecha_salida && $fecha_actual < $fecha_llegada) {
//                 $html = "<p>Tu viaje con ORIGEN {$data->boleto->asc_en} y DESTINO {$data->boleto->des_en} a las {$fecha_salida->format('H:i')} en la butaca {$data->pasajero->nrobutaca} se encuentra actualmente en curso. Para ver la ubicación de tu micro, ingresa al siguiente enlace: <a href='/localiza-micro/?serie={$_GET['serie']}&boleto={$_GET['boleto']}'>LOCALIZA TU MICRO</a></p>";
//             } elseif ($fecha_actual > $fecha_llegada) {
//                 $html = "<p>Tu viaje con ORIGEN {$data->boleto->asc_en} y DESTINO {$data->boleto->des_en} a las {$fecha_salida->format('H:i')} en la butaca {$data->pasajero->nrobutaca} YA SE REALIZÓ.</p>";
//             } else {
//                 $html = "<p>Hola, tu pasaje tiene ORIGEN {$data->boleto->asc_en} - DESTINO {$data->boleto->des_en} a las {$fecha_salida->format('H:i')} en la butaca {$data->pasajero->nrobutaca}. Tu fecha de viaje es {$fecha_salida->format('d-m-Y')}. Recordá presentarte una hora antes de la salida del servicio y para poder seguir la ubicación de tu micro ingresá al siguiente enlace: <a href='/localiza-micro/?serie={$_GET['serie']}&boleto={$_GET['boleto']}'>LOCALIZA TU MICRO</a></p>";
//             }
//             echo json_encode(['status' => 'success', 'html' => $html]);
//         } elseif (isset($data->status) && $data->status === 0) {
//             echo json_encode(['status' => 'error', 'message' => $data->message]);
//         }

//     } else {
//         echo json_encode(['status' => 'error', 'message' => 'Parámetros insuficientes']);
//     }
// }







?>