<?php 
add_action('wp_ajax_get_paradas_y_guardar_json', 'get_paradas_y_guardar_json');
add_action('wp_ajax_nopriv_get_paradas_y_guardar_json', 'get_paradas_y_guardar_json');
function get_paradas_y_guardar_json() {
    // Configuración de la API
    $clave_acceso = 'qu3r1c4qu3s0s';
    $env = isset($_SESSION['env']) ? $_SESSION['env'] : 'prod'; // Asegúrate de que la sesión esté iniciada
    $url_api = ($env === 'test') ? "https://ws.viatesting.com.ar/" : "https://ws.busplus.com.ar/";

    // Ruta donde se guardará el archivo JSON
    $ruta_json = get_template_directory() . '/terminales/terminales.json';

    try {
        // Primer llamada a la API para obtener la clave de sesión
        $session_response = wp_remote_post(API_URL . '/sesion', array(
            'headers' => array('Content-Type' => 'application/json'),
            'body'    => json_encode(array('clave_acceso' => $clave_acceso)),
        ));

        if (is_wp_error($session_response)) {
            wp_send_json_error('Error obteniendo la clave de sesión: ' . $session_response->get_error_message());
        }

        $session_body = wp_remote_retrieve_body($session_response);
        $session_data = json_decode($session_body, true);

        if (!isset($session_data['key'])) {
            wp_send_json_error('No se pudo obtener la clave de sesión.');
        }

        $session_api = $session_data['key'];

        // Segunda llamada a la API para obtener las paradas
        $paradas_response = wp_remote_get(API_URL . '/paradas', array(
            'headers' => array(
                'X-API-KEY'   => $session_api,
                'Content-Type' => 'application/json',
            ),
        ));

        if (is_wp_error($paradas_response)) {
            wp_send_json_error('Error obteniendo las paradas: ' . $paradas_response->get_error_message());
        }

        $paradas_body = wp_remote_retrieve_body($paradas_response);
        $external_paradas = json_decode($paradas_body, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            wp_send_json_error('Error decodificando el JSON de paradas: ' . json_last_error_msg());
        }

        // Guardar los datos en el archivo JSON
        if (file_put_contents($ruta_json, json_encode($external_paradas, JSON_PRETTY_PRINT)) === false) {
            wp_send_json_error('Error guardando los datos en el archivo JSON.');
        }

        wp_send_json_success('Datos guardados correctamente en ' . esc_html($ruta_json));

    } catch (Exception $e) {
        // Manejo de errores
        wp_send_json_error('Error: ' . esc_html($e->getMessage()));
    }
}
function agregar_pagina_terminales() {
    add_menu_page(
        'Lista de todas las terminales',       // Título de la página
        'Terminales IDs',       // Nombre del menú
        'edit_posts',   // Capacidad requerida (ajustar si es necesario)
        'terminales-ids',       // Slug de la página
        'mostrar_terminales', // Función que muestra el contenido
        'dashicons-admin-site', // Icono del menú
        40                  // Posición en el menú
    );
}
add_action('admin_menu', 'agregar_pagina_terminales');
function enqueue_terminales_styles() {
    // Verifica si estamos en la página de administración de terminales
    $screen = get_current_screen();
    if ($screen->id === 'toplevel_page_terminales-ids') {
        // Enqueue CSS file
        wp_enqueue_style(
            'terminales-css', // Handle para el archivo CSS
            get_template_directory_uri() . '/panel-wordpress/terminales/terminales.css', // Ruta al archivo CSS
            array(), // Dependencias (vacío si no hay)
            null, // Versión del archivo CSS
            'all' // Media query
        );
        // Enqueue JavaScript file
        wp_enqueue_script(
            'terminales-js', // Handle para el archivo JS
            get_template_directory_uri() . '/panel-wordpress/terminales/terminales.js', // Ruta al archivo JS
            array('jquery'), // Dependencias (aquí se podría agregar jQuery si se usa)
            null, // Versión del archivo JS
            true // Enqueue en el footer
        );
    }
}
add_action('admin_enqueue_scripts', 'enqueue_terminales_styles');
function mostrar_terminales() {
    // Mostrar los resultados iniciales (sin búsqueda)
    $ruta_json = get_template_directory() . '/terminales/terminales.json';
    $json_data = file_get_contents($ruta_json);
    $terminales = json_decode($json_data, true);

    if (json_last_error() !== JSON_ERROR_NONE) {
        echo '<p>Error decodificando el JSON: ' . json_last_error_msg() . '</p>';
        return;
    }

    // Paginación
    $items_per_page = 50; // Número de resultados por página
    $total_items = count($terminales);
    $total_pages = ceil($total_items / $items_per_page);

    // Mostrar el formulario de búsqueda
    echo '<style>

    </style>';
    echo '<h1>Lista de Terminales</h1>';
    echo '<form id="searchForm">';
    echo '<input type="text" id="searchInput" placeholder="Buscar...">';
    echo '<input type="submit" value="Buscar" class="button">';
    echo '</form>';

    // Mostrar los resultados en una tabla
    echo '<div id="terminalesContainer" data-terminales=\'' . esc_attr(json_encode($terminales)) . '\'>';
    echo '<table>';
    echo '<thead>';
    echo '<tr><th>ID</th><th>Descripción</th></tr>';
    echo '</thead>';
    echo '<tbody>';
    foreach ($terminales as $terminal) {
        echo '<tr>';
        echo '<td>' . esc_html($terminal['idparada']) . '</td>';
        echo '<td>' . esc_html($terminal['descripcion_parada']) . '</td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '</div>';

    // Mostrar paginación
    echo '<div id="pagination">';
    for ($i = 1; $i <= $total_pages; $i++) {
        echo '<a href="#" data-page="' . $i . '" class="page-link">' . $i . '</a> ';
    }
    echo '</div>';

}