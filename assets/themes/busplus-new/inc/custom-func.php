<?php 
function insert_textarea($acf, $tag = 'p'){
    $cont = 1;
    $text_area_arr = explode("\n", $acf);
    $text_area_arr = array_map('trim', $text_area_arr);
    foreach ($text_area_arr as &$valor) {
        if($valor != ''){
            echo('<' . $tag . '>' . $valor . '</' . $tag . '>');
        }
    }
}
function insert_custom_image($url, $w = '100%', $h = '100%', $alt = ''){
    if($url){
        $imageHTML = "<img loading='lazy' src='{$url}' width='{$w}' height='{$h} alt='{$alt}' />";
        echo $imageHTML;
    }
}
function insert_custom_image_json($url, $w = '100%', $h = '100%', $alt = ''){
    if($url){
        return $imageHTML = "<img loading='lazy' src='{$url}' width='{$w}' height='{$h} alt='{$alt}' />";
     
    }
}
function insert_default_image(){
    $root = IMAGE_DEFAULT;

    $imageHTML = "<img loading='lazy' src='{$root}' width='750' height='500' alt='En la imagen se muestra el logo de tromen' />";
    return $imageHTML;
}
function insert_button($acf, $mt, $classBtn, $icon = ''){
    if($mt === ''){
        $mt = 3;
    }
    if($classBtn === ''){
        $classBtn = '';
    }

  
    if ( $acf ):
        $link_url = esc_url($acf['url']) ? $acf['url'] : '';
        $link_title = esc_html($acf['title']);
        $link_target = esc_attr($acf['target'] ? $acf['target'] : '_self');
        $buttonHTML = "<div class='button__container mt-sm-{$mt} mt-2'>";
        $buttonHTML .= "<a class='btn {$classBtn}' href='{$link_url}' target='{$link_target}'>{$icon}{$link_title}</a>";
        $buttonHTML .= "</div>";
        echo($buttonHTML);
    endif;

}
function insert_image($acf, $size = false){
    if(!$size || $size === ''){
        $size = 1024;
    }
    if ( $acf ):
    $url = esc_url($acf['url']);
    $alt = esc_attr($acf['alt']);
    $size_type = 'large';
    $width = $acf['sizes'][ $size_type . '-width' ];
    $height = $acf['sizes'][ $size_type . '-height' ];
    $imageHTML = "<img loading='lazy' width='{$width}'" . img_responsive($acf['id'],"thumb-{$size}","{$size}px") . " 'height='{$height}' alt='{$alt}' />";
    echo $imageHTML;
    endif;
}
function insert_slug($id = ''){
    if(empty($id)){
        return get_post_field( 'post_name', get_post());
    }else{
        return get_post_field( 'post_name', get_post($id));
    }

}
function insert_acf($acf, $tag, $class = ''){
    if($class === ''){
        $class = "";
    }else{
        $class = "class='{$class}'";
    }
    if ( $acf ):
        if($tag === ''):
            $HTML = $acf;
            echo $HTML;
        else:
            $HTML = "<{$tag} $class>";
            $HTML .= $acf;
            $HTML .= "</$tag>";
            echo $HTML;
        endif;
    endif;
}
function sanitizeString($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúúûýýþÿ';
        $modificadas = 'AAAAAAACEEEEIIIIDNOOOOOUUUUYbsaaaaaaaceeeeiiiidnoooooouuuuyyby';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = str_replace(" ", "-", $cadena);
        return strtolower(utf8_encode($cadena));
}
function animation($animation = "fade-in-bottom", $delay = 400){
    $attr = "data-transition='$animation' data-delay='$delay' style='opacity: 0'";
    echo $attr;
}
function limit_description_length($description, $length = 80, $link_text = 'Ver más') {
    if (strlen($description) > $length) {
        $description = substr($description, 0, $length);
        $description .= "...<span>{$link_text}</span>";
    }
    return $description;
}

function generate_pagination($current_page, $total_pages, $tax) {
    $pagination_html = "<div id='pagination' data-tax='$tax'>";

    // Mostrar siempre la primera página
    $pagination_html .= '<button  type="button" data-page="1" class="' . ($current_page == 1 ? 'btn-page active' : 'btn-page') . '">1</button>';

    // Determinar si se deben mostrar puntos suspensivos después de la primera página
    if ($current_page > 3) {
        $pagination_html .= '<span>...</span>';
    }

    // Mostrar las páginas cercanas a la página actual
    $start_page = max(2, $current_page - 1);
    $end_page = min($total_pages - 1, $current_page + 1);

    for ($i = $start_page; $i <= $end_page; $i++) {
        $pagination_html .= '<button type="button" data-page="' . $i . '" class="' . ($current_page == $i ? 'btn-page active' : 'btn-page') . '">' . $i . '</button>';
    }

    // Determinar si se deben mostrar puntos suspensivos antes de la última página
    if ($current_page < $total_pages - 2) {
        $pagination_html .= '<span>...</span>';
    }

    // Mostrar siempre la última página si es mayor que 1
    if ($total_pages > 1) {
        $pagination_html .= '<button type="button" data-page="' . $total_pages . '" class="' . ($current_page == $total_pages ? 'btn-page active' : 'btn-page') . '">' . $total_pages . '</button>';
    }

    // Botón para la siguiente página
    if ($current_page < $total_pages) {
        $pagination_html .= '<button type="button" class="btn-page next" data-page="' . ($current_page + 1) . '">';
        $pagination_html .= '<svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">';
        $pagination_html .= '<path d="M1 1L7 7L1 13" stroke="#606060"></path>';
        $pagination_html .= '</svg></button>';
    }

    $pagination_html .= '</div>';

    return $pagination_html;
}
function show_category_blog($selected_category = '') {
    // Obtener todas las categorías del custom post type 'blog'
    $terms = get_terms(array(
        'taxonomy'   => 'categoria',
        'hide_empty' => false,
        'orderby'    => 'meta_value_num',
        'meta_key'   => 'menu_order',
        'order'      => 'ASC'
    ));

    $index = 0;
    $firstTerm = '';

    // Si hay categorías, construir el HTML
    if (!empty($terms) && !is_wp_error($terms)) : ?>
        <div class="filters">
            <div class="select field-container-input p-relative">
                <div class="select-input d-none-md">
                    <input type="text" readonly value="Seleccione">
                    <svg class="icon-select" width="12" height="6" viewBox="0 0 12 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11 1L6 5L1 1" stroke="black" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                <ul class="select-results list-select">
                    <?php foreach ($terms as $term) : 
                        $name = $term->name;
                        $slug = $term->slug;

                        // Determinar si esta categoría debe ser activa
                        if ($selected_category && $selected_category === $slug) {
                            $active = 'active';
                        } elseif ($index === 0 && !$selected_category) {
                            $active = 'active';
                            $firstTerm = $slug;
                        } else {
                            $active = '';
                        }

                        $index++;
                    ?>
                        <li class="d-flex-md d-none line-container"><div class="line"></div></li>
                        <li>
                            <button type="button" class="btn only-text black btn-category <?php echo $active; ?>" data-slug="<?php echo $slug; ?>">
                                <?php echo $name; ?>
                            </button>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif;
}

function show_posts_blog($category = null) {
    // Guardar la consulta global actual para restaurarla más tarde
    global $wp_query;
    $original_query = $wp_query;

    // Variables necesarias para la consulta
    $args = array(
        'post_type' => 'blog',
        'posts_per_page' => 9,
        'orderby' => 'date',
        'order' => 'asc',
        'post_status' => 'publish',
        'suppress_filters' => false, // No suprimir filtros para permitir el control de WP_Query
    );

    // Obtener términos de la taxonomía
    $terms = get_terms(array(
        'taxonomy' => 'categoria',
        'hide_empty' => false,
        'orderby' => 'meta_value_num',
        'meta_key' => 'menu_order',
        'order' => 'ASC'
    ));

    // Ajustar la consulta si se pasa una categoría
    if (!empty($category)) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms' => $category
            )
        );
    } elseif (!empty($terms) && !is_wp_error($terms)) {
        $category = $terms[0]->slug;
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms' => $category
            )
        );
    }

    // Ejecutar consulta personalizada
    $custom_query = new WP_Query($args);
    $total_pages = $custom_query->max_num_pages;
    ob_start(); // Iniciar captura de salida
    if ($custom_query->have_posts()) : ?>
        <div class="block-related_blog bg-white">
            <div class="row">
                <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
                    <div class="col-sm-4 col-xs-6 col-12">
                        <a href="<?php echo get_the_permalink(); ?>" aria-label="<?php the_title(); ?>" class="card">
                            <div class="block-image">
                                <?php echo get_the_post_thumbnail(); ?>
                            </div>
                            <h2 class="title-blog"><?php the_title(); ?></h2>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="pagination col-12">
            <?php echo generate_pagination(1, $total_pages, $category); ?>
        </div>
    <?php else : ?>
        <p class="col-12 error-msg pb-xs-4 pb-2">No se encontraron posts.</p>
    <?php endif;
    $output = ob_get_clean(); // Obtener el contenido capturado
    // Restaurar la consulta global después de WP_Query
    $wp_query = $original_query;
    wp_reset_postdata();
    return $output;
}

function show_related_posts_blog() {
    global $post;

    // Obtén los términos de la taxonomía personalizada 'categorias' del post actual
    $terms = wp_get_post_terms($post->ID, 'categoria', array('fields' => 'ids'));
 
    if ($terms) {
        $args = array(
            'post_type' => 'blog', // Asegúrate de usar el Custom Post Type 'blog'
            'posts_per_page' => 3,
            'post__not_in' => array($post->ID), // Excluir el post actual
            'tax_query' => array(
                array(
                    'taxonomy' => 'categoria', // Tu taxonomía personalizada
                    'field' => 'term_id',
                    'terms' => $terms, // Mostrar posts que tengan estos términos
                ),
            ),
            'orderby' => 'date',
            'order' => 'ASC',
        );
        $blog_page = get_field( 'blog', 'options' );
        $related_posts = new WP_Query($args);

        if ($related_posts->have_posts()) : ?>
            <section class="block-related_blog">
                <div class="container">
                    <div class="block-title heading-arrow">
                        <h2 class="f-24-18 f-bold p-relative">
                            Blog
                          
                            <?php if($blog_page): ?>
                                <div class="button__container mt-sm-0 mt-2">
                                    <a class="btn only-text orange" href="<?php echo esc_url( $blog_page ); ?>" target="_self">Ver más</a>
                                </div>
                            <?php endif; ?>
                        </h2>
                    </div>
                    <div class="row">
                        <?php while ($related_posts->have_posts()) : $related_posts->the_post(); ?>
                            <div class="col-sm-4 col-xs-6 col-12">
                                <a href="<?php echo get_the_permalink(); ?>" aria-label="<?php the_title(); ?>" class="card">
                                    <div class="block-image">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <h3 class="title-blog"><?php the_title(); ?></h3>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="btn-down d-none-xs">
                        <?php if($blog_page): ?>
                            <div class="button__container mt-sm-4 mt-2">
                                <a class="btn only-text orange" href="<?php echo esc_url( $blog_page ); ?>" target="_self">Ver más</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <?php wp_reset_postdata();
        endif;
    }
}

function show_block_buscador($show_title = false) {
    // Obtener el título de la opción del buscador
    $titulo_opcion = get_option('buscador_titulo');

    // Verifica si hay que mostrar el título
    if ($show_title && $titulo_opcion) {
        ?>
        <div class="buscador__container">
            <div class="container">
                <div class="block-title">
                    <h2 class="f-24-18 f-bold'"><?php echo $titulo_opcion;?></h2>
                </div>
            </div>
            <?php get_template_part("template-parts/content", "buscador"); ?>
        </div>
        <?php
    } else {
        // Si no hay título o $show_title es false, muestra solo el contenido del buscador
        get_template_part("template-parts/content", "buscador");
    }
}

function render_block_destino($titulo = 'Nuestras rutas más populares desde', $bg = 'bg-white') {
    // Consulta personalizada para obtener los términos de la taxonomía 'ubicaciones'
    $get_terms_args = array(
        'taxonomy' => 'ubicaciones',
        'hide_empty' => 0,
        'orderby' => 'meta_value_num',
        'meta_key' => 'menu_order',
        'order' => 'ASC'
    );
    $terms = get_terms($get_terms_args);
    
    // Determinar el primer término para la consulta de posts
    $firstTerm = !empty($terms) ? $terms[0]->slug : '';

    // Obtener los IDs de los posts padres
    $parent_args = array(
        'post_type' => 'destino',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'ubicaciones',
                'field' => 'slug',
                'terms' => $firstTerm,
            ),
        ),
        'post_parent' => 0, // Solo obtener los posts padre
        'fields' => 'ids' // Solo obtener los IDs de los padres
    );
    $parent_posts = get_posts($parent_args);

    // Ahora que tenemos los IDs de los padres, obtenemos los hijos
    $args = array(
        'post_type' => 'destino',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_parent__in' => $parent_posts, // Solo obtener posts que son hijos de estos padres
    );
    $query = new WP_Query($args);
    
    // Comienza a generar el HTML
    ob_start(); // Inicia la captura de salida
    ?>
    <section class="block-destino <?php echo $bg;?>">
        <div class="container">     
            <div class="block-title">
                <h2 class="f-24-18 f-bold p-relative"><?php echo $titulo;?></h2>
            </div>
            <div class="filters">
                <div class="select field-container-input p-relative">
                    <div class="select-input d-none-md">
                        <svg class="icon-select" width="12" height="6" viewBox="0 0 12 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11 1L6 5L1 1" stroke="black" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <input type="text" readonly placeholder="Seleccione">
                    </div>
                    <ul class="select-results list-select">
                        <?php foreach ($terms as $index => $term) : ?>
                            <li class="d-flex-md d-none line-container"><div class="line"></div></li>
                            <li>
                                <button type="button" class="btn only-text black button-rutas <?php echo $index === 0 ? 'active' : ''; ?>" data-slug="<?php echo esc_attr($term->slug); ?>"><?php echo esc_html($term->name); ?></button>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="splide" id="destino">
                <div class="splide__track">
                    <div class="loader-container"></div>
                    <ul class="splide__list" id="containerRutas">
                        <?php if ($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php $post_texto = get_field('texto'); ?>
                                <li class="splide__slide card-destino">
                                    <a href="<?php echo get_the_permalink();?>" class="card" aria-label="Hace click a <?php the_title(); ?>">
                                        <div class="block-image">
                                            <?php if(the_post_thumbnail()): ?>
                                                <?php the_post_thumbnail(); ?>
                                                <?php else: ?>
                                                    <?php 
                                                    $terminal_titulo="Destino " . get_the_title();
                                                    $terminal_alt = "En la imágen se muestra la foto de " . get_the_title();
                                                    ?>
                                                    <?php echo image_custom($terminal_alt, $terminal_titulo, ""); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="block-content">
                                            <h3><?php the_title(); ?></h3>
                                            <p><?php echo $post_texto; ?></p>
                                        </div>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        <?php else: ?>
                            <li class="splide__slide">
                                <p class="error-msg">No se encontraron destinos..</p>
                            </li>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php
    return ob_get_clean(); // Retorna el HTML capturado
}

function get_ubicaciones_post() {
    global $post;
    
    // Obtener los términos asociados al post con la taxonomía 'ubicaciones'
    $post_terms = wp_get_post_terms($post->ID, 'ubicaciones');
    
    // Comprobar si hay términos disponibles
    if (!empty($post_terms) && !is_wp_error($post_terms)) {
        // Retornar el primer término
        return $post_terms[0];
    }
    
    // Si no hay términos, retornar null o cualquier valor predeterminado
    return null;
}

function render_block_destino_actual($titulo = 'Destinos más populares de', $bg = 'bg-white') {
    global $post;
    
    // Obtener la taxonomía 'ubicaciones' del post actual
    $post_terms = wp_get_post_terms($post->ID, 'ubicaciones');
    
    // Si no hay términos, no se muestra nada
    if (empty($post_terms) || is_wp_error($post_terms)) {
        return '';
    }

    // Obtener el primer término (suponiendo que el post tiene al menos una categoría de 'ubicaciones')
    $current_term = $post_terms[0];

    // Obtener los hijos de este término
    $child_terms = get_terms(array(
        'taxonomy' => 'ubicaciones',
        'parent' => $current_term->term_id,
        'hide_empty' => 0,
        'orderby' => 'meta_value_num',
        'meta_key' => 'menu_order',
        'order' => 'ASC'
    ));

    // Obtener los IDs de los posts padres relacionados con este término y sus hijos
    $parent_args = array(
        'post_type' => 'destino',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'ubicaciones',
                'field' => 'term_id',
                'terms' => array_merge(array($current_term->term_id), wp_list_pluck($child_terms, 'term_id')),
            ),
        ),
        'post_parent' => 0,
        'fields' => 'ids'
    );
    $parent_posts = get_posts($parent_args);

    // Obtener los posts hijos
    $args = array(
        'post_type' => 'destino',
        'posts_per_page' => 6,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_parent__in' => $parent_posts,
    );
    $query = new WP_Query($args);
    
    // Comienza a generar el HTML
    ob_start(); 
    ?>
    <section class="block-destino block-only-destino <?php echo $bg;?>">
        <div class="container">     
            <div class="block-title">
                <h2 class="f-24-18 f-bold p-relative"><?php echo $titulo . ' ' . esc_html($current_term->name); ?></h2>
            </div>

            <div class="splide" id="destino-only">
                <div class="splide__track">
                    <div class="loader-container"></div>
                    <ul class="splide__list">
                        <?php if ($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php $post_texto = get_field('texto'); ?>
                                <li class="splide__slide card-destino">
                                    <a href="<?php echo get_the_permalink();?>" class="card" aria-label="Hace click a <?php the_title(); ?>">
                                        <div class="block-image">
                                            <?php the_post_thumbnail(); ?>
                                        </div>
                                        <div class="block-content">
                                  
                                            <h3><?php the_title(); ?></h3>
                                            
                                        </div>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        <?php else: ?>
                            <li class="splide__slide">
                                <p class="error-msg">No se encontraron destinos..</p>
                            </li>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php
    return ob_get_clean(); 
}

function get_landing_seo_nivel_2() {
    // Iniciar la captura de salida
    ob_start();

    // Obtener el ID del post actual
    $current_post_id = get_the_ID();
    $current_target = get_ubicaciones_post()->name;
    // Obtener las páginas hijas del post actual
    $args = array(
        'post_parent' => $current_post_id,
        'post_type'   => 'destino', // Asegúrate de que este sea tu CPT
        'posts_per_page' => -1, // Obtener todas las páginas hijas
    );

    $child_pages = get_posts($args);

    // Definir las variables para almacenar los títulos según los criterios
    $titulo_tipo_true = '';
    $titulo_tipo_false = '';

    // Comprobar si hay páginas hijas
    if ($child_pages) {
        // Iterar sobre las páginas hijas
        foreach ($child_pages as $child) {
            $tipo_pagina = get_field('tipo_de_pagina', $child->ID);
            $nivel_pagina = get_field('nivel_de_pagina', $child->ID);
            $permalink = get_permalink($child->ID);
            // Filtrar según los criterios dados y almacenar los títulos
            if ($tipo_pagina && $nivel_pagina) {
                $titulo_tipo_true .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            } elseif (!$tipo_pagina && $nivel_pagina) {
                $titulo_tipo_false .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            }
        }
    } else {
        // Si no hay páginas hijas, mostrar un mensaje
        $titulo_tipo_true = '<li>No hay páginas hijas disponibles.</li>';
        $titulo_tipo_false = '<li>No hay páginas hijas disponibles.</li>';
    }

    // Generar el HTML completo
    ?>
    <div class="row mb-xs-6 mb-4">
        <div class="col-xs-6 col-12">
            <h3>Pasajes a <?php echo $current_target;?></h3>
            <div class="links">
                <?php echo $titulo_tipo_false; ?>
            </div>
        </div>
        <div class="col-xs-6 col-12">
            <h3>Pasajes desde <?php echo $current_target;?></h3>
            <div class="links">
         
                <?php echo $titulo_tipo_true; ?>
            </div>
           
        </div>
    </div>
    <?php

    // Retornar el contenido capturado
    return ob_get_clean();
}

function get_landing_seo_nivel_3() {
    // Iniciar la captura de salida
    ob_start();

    // Obtener el ID del post actual
    $current_post_id = get_the_ID();
    $current_target = get_ubicaciones_post()->name;
    // Obtener las páginas hijas del post actual
    $args = array(
        'post_parent' => $current_post_id,
        'post_type'   => 'destino', // Asegúrate de que este sea tu CPT
        'posts_per_page' => -1, // Obtener todas las páginas hijas
    );

    $child_pages = get_posts($args);

    // Definir las variables para almacenar los títulos según los criterios
    $titulo_tipo_true = '';
    $titulo_tipo_false = '';

    // Comprobar si hay páginas hijas
    if ($child_pages) {
        // Iterar sobre las páginas hijas
        foreach ($child_pages as $child) {
            $tipo_pagina = get_field('tipo_de_pagina', $child->ID);
            $nivel_pagina = get_field('nivel_de_pagina', $child->ID);
            $permalink = get_permalink($child->ID);
            // Filtrar según los criterios dados y almacenar los títulos
            if ($tipo_pagina && !$nivel_pagina) {
                $titulo_tipo_true .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            } elseif (!$tipo_pagina && !$nivel_pagina) {
                $titulo_tipo_false .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            }
        }
    } else {
        // Si no hay páginas hijas, mostrar un mensaje
        $titulo_tipo_true = '<li>No hay páginas hijas disponibles.</li>';
        $titulo_tipo_false = '<li>No hay páginas hijas disponibles.</li>';
    }

    // Generar el HTML completo
    ?>
    <div class="row">
        <div class="col-xs-6 col-12">
            <h3>Viajar desde <?php echo $current_target;?></h3>
            <div class="links">
            <?php echo $titulo_tipo_true; ?>
            </div>
        </div>
        <div class="col-xs-6 col-12">
            <h3>Viajar a <?php echo $current_target;?></h3>
            <div class="links">
                <?php echo $titulo_tipo_false; ?>
            </div>
        </div>
    </div>
    <?php

    // Retornar el contenido capturado
    return ob_get_clean();
}



function image_custom($alt = '', $titulo = '', $clase = '',$url=null) {
    // Obtener la URL de la imagen en el directorio del tema
    $image_url = IMAGE . '/destinos/destino-card.jpg'; // Cambia la ruta según tu estructura de carpetas
    if($url){
        $image_url = $url; // Cambia la ruta según tu estructura de carpetas
    }else{
        $image_url = IMAGE . '/destinos/destino-card.jpg'; // Cambia la ruta según tu estructura de carpetas
    }

    // Generar la etiqueta <img>
    $img_tag = '<img width="400" height="400" loading="lazy" src="' . esc_url($image_url) . '" alt="' . esc_attr($alt) . '" title="' . esc_attr($titulo) . '" class="' . esc_attr($clase) . '" />';

    return $img_tag; // Retorna la etiqueta <img>
}


function get_current_parent(){
    $parent_id = wp_get_post_parent_id(get_the_ID());
    $result = []; // Array donde almacenaremos la información

    if($parent_id){
        // Obtiene los términos de la taxonomía 'ubicaciones' asociados con el padre
        $ubicaciones = get_the_terms($parent_id, 'ubicaciones');

        // Verifica si existen términos y los añade al array
        if ($ubicaciones && !is_wp_error($ubicaciones)) {
            foreach ($ubicaciones as $ubicacion) {
                $result[] = [
                    'ubicacion' => $ubicacion->name,
                    'slug' => $ubicacion->slug,
                    'descripcion' => $ubicacion->description,
                    'enlace' => get_term_link($ubicacion)
                ];
            }
        } else {
            $result['error'] = 'El padre no tiene una ubicación asignada.';
        }
    } else {
        $result['error'] = 'Esta página no tiene un padre.';
    }

    return $result; // Retorna la información en forma de array
}

function get_landing_seo_single_destino() {
    // Iniciar la captura de salida
    ob_start();

    // Obtener el ID del padre de la página actual
    $parent_id = wp_get_post_parent_id(get_the_ID());
    if (!$parent_id) {
        return '<p>Esta página no tiene un padre.</p>';
    }

    // Obtener la ubicación del padre usando la función get_current_parent
    $parent_data = get_current_parent();
    
    $current_target = $parent_data[0]['ubicacion'] ?? '';

    // Obtener las páginas hijas del post padre
    $args = array(
        'post_parent'    => $parent_id,
        'post_type'      => 'destino', // Asegúrate de que este sea tu CPT
        'posts_per_page' => -1, // Obtener todas las páginas hijas del padre
    );

    $child_pages = get_posts($args);

    // Definir las variables para almacenar los títulos según los criterios
    $titulo_tipo_true = '';
    $titulo_tipo_false = '';

    // Comprobar si hay páginas hijas
    if ($child_pages) {
        // Iterar sobre las páginas hijas
        foreach ($child_pages as $child) {
            $tipo_pagina = get_field('tipo_de_pagina', $child->ID);
            $nivel_pagina = get_field('nivel_de_pagina', $child->ID);
            $permalink = get_permalink($child->ID);

            // Filtrar según los criterios dados y almacenar los títulos
            if ($tipo_pagina && !$nivel_pagina) {
                $titulo_tipo_true .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            } elseif (!$tipo_pagina && !$nivel_pagina) {
                $titulo_tipo_false .= '<a href="' . esc_url($permalink) . '">' . esc_html(get_the_title($child->ID)) . '</a>';
            }
        }
    } else {
        // Si no hay páginas hijas, mostrar un mensaje
        $titulo_tipo_true = '<li>No hay páginas hijas disponibles.</li>';
        $titulo_tipo_false = '<li>No hay páginas hijas disponibles.</li>';
    }

    // Generar el HTML completo
    ?>
    <div class="row">
        <div class="col-xs-6 col-12">
            <h3>Viajar desde <?php echo esc_html($current_target); ?></h3>
            <div class="links">
                <?php echo $titulo_tipo_true; ?>
            </div>
        </div>
        <div class="col-xs-6 col-12">
            <h3>Viajar a <?php echo esc_html($current_target); ?></h3>
            <div class="links">
                <?php echo $titulo_tipo_false; ?>
            </div>
        </div>
    </div>
    <?php

    // Retornar el contenido capturado
    return ob_get_clean();
}