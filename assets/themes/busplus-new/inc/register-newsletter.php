<?php 

function buscador_menu() {
    add_menu_page(
        'Buscador', // Título de la página
        'Buscador', // Texto del menú
        'manage_options', // Capacidad requerida para ver el menú
        'buscador-config', // ID único de la página
        'buscador_opciones', // Función que renderiza la página
        'dashicons-search',
        2
    );
}
add_action('admin_menu', 'buscador_menu');

function buscador_opciones() {
    ?>
    <div class="wrap">
        <h2>Configuración del Buscador</h2>
        <?php
        if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] ) {
            ?>
            <div id="message" class="updated notice is-dismissible">
                <p><?php _e( 'Cambios guardados.', 'mi-text-domain' ); ?></p>
            </div>
            <?php
        } elseif ( isset( $_GET['settings-error'] ) && $_GET['settings-error'] ) {
            ?>
            <div id="message" class="error notice is-dismissible">
                <p><?php _e( 'Hubo un error al guardar los cambios. Por favor, inténtalo de nuevo.', 'mi-text-domain' ); ?></p>
            </div>
            <?php
        }
        ?>
 
        <form method="post" action="options.php">
            <?php
            // Agrega los campos de opciones
            settings_fields('buscador-panel');
            // Renderiza los campos de opciones
            do_settings_sections('buscador-panel');
            // Botón de guardado
            submit_button('Guardar cambios');
            ?>
        </form>
    </div>
    <?php
}

function buscador_settings() {
    // Sección de configuración del buscador
    add_settings_section(
        'buscador_seccion_general', // ID único de la sección
        'Configuración del Buscador', // Título de la sección
        'buscador_seccion_general_callback', // Callback para el contenido de la sección
        'buscador-panel' // ID de la página donde se mostrará la sección
    );

    // Campo Título
    add_settings_field(
        'buscador_titulo', // ID único del campo
        'Título', // Título del campo
        'buscador_titulo_callback', // Callback para el contenido del campo
        'buscador-panel', // ID de la página donde se mostrará el campo
        'buscador_seccion_general' // ID de la sección donde se mostrará el campo
    );

    // Campo Código Descuento
    add_settings_field(
        'buscador_codigo_descuento', // ID único del campo
        'Código Descuento', // Título del campo
        'buscador_codigo_descuento_callback', // Callback para el contenido del campo
        'buscador-panel', // ID de la página donde se mostrará el campo
        'buscador_seccion_general' // ID de la sección donde se mostrará el campo
    );

    // Registra las opciones
    register_setting('buscador-panel', 'buscador_titulo');
    register_setting('buscador-panel', 'buscador_codigo_descuento');
}
add_action('admin_init', 'buscador_settings');

function buscador_seccion_general_callback() {
    echo '<p>Configura los parámetros para el buscador.</p>';
}

function buscador_titulo_callback() {
    $titulo = get_option('buscador_titulo');
    echo "<input type='text' name='buscador_titulo' value='" . esc_attr($titulo) . "' style='width: 300px;' />";
}

function buscador_codigo_descuento_callback() {
    $codigo_descuento = get_option('buscador_codigo_descuento');
    echo "<input type='text' name='buscador_codigo_descuento' value='" . esc_attr($codigo_descuento) . "' />";
}