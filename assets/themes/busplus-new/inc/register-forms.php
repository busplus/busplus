<?php  



add_action('wp_ajax_send_form_contact', 'send_form_contact');
add_action('wp_ajax_nopriv_send_form_contact', 'send_form_contact');
function send_form_contact(){
    global $mail; // define the global variable
    if (!is_object($mail) || !is_a($mail, 'PHPMailer')) { // check if $phpmailer object of class PHPMailer exists
        // if not - include the necessary files
        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        require_once ABSPATH . WPINC . '/class-smtp.php';
        $mail = new PHPMailer(true);
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Obtener el objeto enviado
        $dataObject = $_POST;
        $dataObjectFiles = $_FILES;
        $messageHTML = "<html><head><title>Wecircular</title></head><body>";
        $adjunto = false;
        // Mostrar los datos del objeto
        $destinatario = $_POST['destinatario'];
        $file_name = "";
        $file_tmp = "";
        $subject = "";
            foreach ($dataObject as $key => $value) {
           
                if($key === "subject"){
                    $messageHTML .= "<h2><strong>". $value ."</strong></h2>";
                    $subject = 'Wecircular - ' . $value;
                }else{
                    if($key === "action" || $key === "destinatario"){

                    }else{
                        $keyNew =  str_replace('-', ' ', $key);
                        $capitilize = ucwords(strtolower($keyNew));
                        $messageHTML .= "<p>". $capitilize .": <strong>" . $value ."</strong></p>";
                    }
                }

                
            }
       
            foreach ($dataObjectFiles as $key => $value) {
                foreach ($dataObjectFiles[$key] as $index => $valor) {
                    if($index === 'name'){
                        $file_name = $valor;
                    }
                    if($index === 'tmp_name'){
                        $file_tmp = $valor;
                    }
                    $adjunto = true;
                    move_uploaded_file($file_tmp, get_template_directory()  . '/archivo/' . $file_name);        
                }
            }

            $messageHTML .= "</body></html>";
            $mail = new PHPMailer();
            $mail->isMail();
            $mail->SetFrom($destinatario, $subject);
            $mail->AddAddress($destinatario, $subject);

         
            if($adjunto === true){
                $mail->AddAttachment( get_template_directory() . '/archivo/' . $file_name, $file_name);
            }
            $mail->isHTML(true);
            $mail->CharSet = "UTF-8";
            $mail->Subject = $subject;
            $mail->Body= $messageHTML;
            if (!$mail->Send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo 'ok';
            }
    }


}


// Ajax Rutas
add_action('wp_ajax_send_posts_rutas', 'send_posts_rutas');
add_action('wp_ajax_nopriv_send_posts_rutas', 'send_posts_rutas');

function send_posts_rutas() {
    if (!isset($_GET['tax'])) {
        wp_send_json_error('Taxonomy not specified', 400);
        return;
    }

    $tax = sanitize_text_field($_GET['tax']);

    // Paso 1: Obtener IDs de los posts padres
    $parent_args = array(
        'post_type' => 'destino',
        'posts_per_page' => -1, // Obtener todos los posts padres
        'post_parent' => 0, // Solo posts padres
        'tax_query' => array(
            array(
                'taxonomy' => 'ubicaciones',
                'field' => 'slug',
                'terms' => $tax,
            ),
        ),
        'fields' => 'ids' // Solo obtener IDs
    );

    $parent_query = new WP_Query($parent_args);
    $parent_ids = $parent_query->posts;

    // Paso 2: Obtener posts hijos de los posts padres
    $args = array(
        'post_type' => 'destino',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_parent__in' => $parent_ids, // Solo posts hijos de los posts padres encontrados
    );

    $query = new WP_Query($args);
    
    $posts_data = array();



    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // Obtiene la ubicaciones asociada al post
            $ubicacioness = get_the_terms(get_the_ID(), 'ubicaciones');
            $ubicaciones_name = '';
            if ($ubicacioness && !is_wp_error($ubicacioness)) {
                $ubicaciones_name = $ubicacioness[0]->name; // Asumiendo que solo hay una ubicaciones por post
            }
            $image = get_the_post_thumbnail();
            if(get_the_post_thumbnail()){
                $image = get_the_post_thumbnail();
            }else{
                $terminal_titulo="Destino " . get_the_title();
                $terminal_alt = "En la imágen se muestra la foto de " . get_the_title();
                $image = image_custom($terminal_alt, $terminal_titulo, '');
            }
            $posts_data[] = array(
                'permalink' => get_the_permalink(),
                'title' => get_the_title(),
                'text' => $ubicaciones_name,
                'image' => $image,
            );
        }
        wp_reset_postdata();
    }

    wp_send_json($posts_data);
}

// Ajax Posts
add_action('wp_ajax_send_posts_blog', 'send_posts_blog');
add_action('wp_ajax_nopriv_send_posts_blog', 'send_posts_blog');
function send_posts_blog() {
    if (!isset($_GET['tax'])) {
        wp_send_json_error('Taxonomy not specified', 400);
        return;
    }

    $tax = sanitize_text_field($_GET['tax']);
    $paged = isset($_GET['paged']) ? sanitize_text_field($_GET['paged']) : 1;

    $args = array(
        'post_type' => 'blog',
        'posts_per_page' => 9,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms' => $tax,
            ),
        ),
        'paged' => $paged,
    );

    $query = new WP_Query($args);

    $total_pages = $query->max_num_pages;

    // Verificar si la página solicitada es mayor que el número total de páginas
    if ($paged > $total_pages) {
        wp_send_json_error('<p class="col-12 error-msg pb-xs-4 pb-2">No hay posts para mostrar</p>', 404);
        return;
    }

    ob_start(); // Iniciar captura de salida

    if ($query->have_posts()) : ?>
        <div class="block-related_blog bg-white">
            <div class="row">
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                <div class="col-sm-4 col-xs-6 col-12">
                    <a href="<?php echo get_the_permalink(); ?>" aria-label="<?php the_title(); ?>" class="card">
                        <div class="block-image">
                            <?php echo the_post_thumbnail(); ?>
                        </div>
                        <h2 class="title-blog"><?php the_title(); ?></h2>
                    </a>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="pagination col-12">
            <?php echo generate_pagination(1, $total_pages, $tax); ?>
        </div>
    <?php endif;

    $posts_html = ob_get_clean(); // Obtener el contenido generado y limpiar el buffer

    wp_reset_postdata();

    // Enviar la respuesta JSON con el HTML y las páginas totales
    wp_send_json_success(array(
        'html' => $posts_html,
        'total_pages' => $total_pages
    ));
}


// AJAX ESTADO DE TU VIAJE
add_action('wp_ajax_obtener_estado_viaje', 'obtener_estado_viaje');
add_action('wp_ajax_nopriv_obtener_estado_viaje', 'obtener_estado_viaje');
function obtener_estado_viaje() {
    // Verifica que el request sea válido
    if ( isset($_GET['serie']) && isset($_GET['boleto']) ) {
        require_once get_template_directory() . '/api/conexion.php';
        $serie = sanitize_text_field($_GET['serie']);
        $boleto = sanitize_text_field($_GET['boleto']);

        // Inicializa la clase WebService y realiza la llamada
        $result = new WebService();
        $url = "viajes/estado_viaje?serie=" . $serie . "&boleto=" . $boleto;
        $respuesta = $result->call_curl($url);

        // Si hay un error en la respuesta
        if(isset($respuesta->status) && ($respuesta->status == 0 || $respuesta->status == false)) {
            wp_send_json_error(['message' => $respuesta->message]);
        }

        $hora_salida = formato_fecha($respuesta->boleto->fechasalida);
        $hora_llegada = formato_fecha($respuesta->boleto->fechallegada);
        $bus_horario = formato_fecha($respuesta->datos->fecha);

        $data = [
            'origen' => $respuesta->boleto->asc_en,
            'destino' => $respuesta->boleto->des_en,
            'fecha_salida' => $respuesta->boleto->fechasalida,
            'fecha_llegada' => $respuesta->boleto->fechallegada,
            'hora_salida' => $hora_salida,
            'hora_llegada' => $hora_llegada,
            'nro_butaca' => $respuesta->pasajero->nrobutaca,
            'pasajero_nombre' => $respuesta->pasajero->nombre,
            'pasajero_apellido' => $respuesta->pasajero->apellido,
            'pasajero_documento' => $respuesta->pasajero->descripcion,
            'pasajero_n_documento' => $respuesta->pasajero->nrodocumento,
            'categoria_asiento' => $respuesta->boleto->categoria,
            'empresa' => $respuesta->boleto->razonsocial,
            'precio_total' => $respuesta->boleto->precio_total,
            'boleto_serie' => $respuesta->boleto->nroserie,
            'boleto_id' => $respuesta->boleto->idboleto,
            'bus_patente' => $respuesta->datos->patente,
            'bus_interno' => $respuesta->datos->interno,
            'bus_latitud' => $respuesta->datos->LATITUD,
            'bus_longitud' => $respuesta->datos->LONGITUD,
            'bus_direccion' => $respuesta->datos->direccion,
            'bus_horario' => $bus_horario,
            'origen_latitud' => $respuesta->boleto->asc_en_lat,
            'origen_longitud' => $respuesta->boleto->asc_en_lon,
            'destino_latitud' => $respuesta->boleto->des_en_lat,
            'destino_longitud' => $respuesta->boleto->des_en_lon,
        ];

        // Envía los datos como respuesta en formato JSON
        wp_send_json_success($data);
    } else {
        wp_send_json_error(['message' => 'Parámetros faltantes']);
    }
}

function formato_fecha($fecha){
    $fecha = new DateTime($fecha);
    return $fecha->format('d/m/Y H:i:s');
}
    
