<?php 
// Agrega el campo de orden en la pantalla de edición de términos
function add_ubicaciones_order_field($term) {
    $term_id = $term->term_id;
    $term_meta = get_term_meta($term_id, 'menu_order', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="menu_order"><?php _e('Orden'); ?></label>
        </th>
        <td>
            <input type="number" name="term_meta[menu_order]" id="term_meta[menu_order]" value="<?php echo esc_attr($term_meta) ? esc_attr($term_meta) : ''; ?>">
            <p class="description"><?php _e('Enter a number to order this term.'); ?></p>
        </td>
    </tr>
    <?php
}
add_action('ubicaciones_edit_form_fields', 'add_ubicaciones_order_field');
add_action('ubicaciones_add_form_fields', 'add_ubicaciones_order_field');

// Guarda el campo de orden
function save_ubicaciones_order_field($term_id) {
    if (isset($_POST['term_meta'])) {
        $term_meta = $_POST['term_meta'];
        update_term_meta($term_id, 'menu_order', intval($term_meta['menu_order']));
    }
}
add_action('edited_ubicaciones', 'save_ubicaciones_order_field');
add_action('created_ubicaciones', 'save_ubicaciones_order_field');

// Añade una columna "Orden" a la lista de términos
function add_ubicaciones_order_column($columns) {
    $new_columns = array();
    if (isset($columns['cb'])) {
        $new_columns['cb'] = $columns['cb'];
        $new_columns['name'] = $columns['name'];
        unset($columns['cb']);
        unset($columns['name']);
    }
    $new_columns['menu_order'] = 'Orden';
    return array_merge($new_columns, $columns);
}
add_filter('manage_edit-ubicaciones_columns', 'add_ubicaciones_order_column');

// Muestra el valor de la columna de orden
function show_ubicaciones_order_column($content, $column_name, $term_id) {
    if ($column_name == 'menu_order') {
        $content = get_term_meta($term_id, 'menu_order', true);
    }
    return $content;
}
add_filter('manage_ubicaciones_custom_column', 'show_ubicaciones_order_column', 10, 3);

// Haz que la columna sea ordenable
function make_ubicaciones_order_column_sortable($sortable) {
    $sortable['menu_order'] = 'menu_order';
    return $sortable;
}
add_filter('manage_edit-ubicaciones_sortable_columns', 'make_ubicaciones_order_column_sortable');

// Ordena los términos por el campo "menu_order"
function ubicaciones_orderby($query) {
    if (!is_admin()) {
        return;
    }

    $screen = $GLOBALS['current_screen'];

    if ($screen->taxonomy === 'ubicaciones' && isset($query->query_vars['orderby']) && $query->query_vars['orderby'] == 'menu_order') {
        $query->query_vars = array_merge($query->query_vars, array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'menu_order'
        ));
    }
}
add_action('pre_get_terms', 'ubicaciones_orderby');

// Elimina la etiqueta y descripción de la taxonomía "ubicaciones" (opcional)
function eliminar_label_descripcion_ubicaciones() {
    $screen = get_current_screen();
    if ($screen->taxonomy === 'ubicaciones') {
        echo '<style>
            .form-field.term-description-wrap {
                display: none;
            }
        </style>';
    }
}
add_action('admin_head', 'eliminar_label_descripcion_ubicaciones');





// Modificar el título del post en las páginas hijas del CPT 'destino' solo en el área de administración
function agregar_texto_a_hijas($title, $post_id) {
    // Verificar si estamos en el área de administración
    if (is_admin() && !wp_doing_ajax()) {
        $post = get_post($post_id); // Obtener el post actual

        // Verificar si el post es una página hija y es del CPT 'destino'
        if ($post && $post->post_parent != 0 && get_post_type($post_id) == 'destino') {
            $tipo_pagina = get_field('tipo_de_pagina', $post_id) ? '[Destino]' : '[Destinos relacionados]';
            $nivel_pagina = get_field('nivel_de_pagina', $post_id) ? 'Nivel 2' : 'Nivel 3';

            // Modificar el título con el formato deseado
            $span_content = esc_html($nivel_pagina) . ' — ' . esc_html($tipo_pagina);
            $title .= ' — ' . $span_content; // Agregar el contenido HTML al título
        }
    }

    return $title;
}
add_filter('the_title', 'agregar_texto_a_hijas', 10, 2);