<?php 

function libScript(){
    wp_register_script( 'validator', LIB . '/validator/validator.js', array(), '1.0', true);
    wp_register_script( 'splide', 'https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.4/dist/js/splide.min.js', array(), '4.1.4', true);
    wp_register_script( 'splide-grid', 'https://cdn.jsdelivr.net/npm/@splidejs/splide-extension-grid@0.4.1/dist/js/splide-extension-grid.min.js', array(), '4.1.4', true);
    wp_register_script( 'flatpickr-js', LIB . '/flatpickr/flatpickr.min.js', array(), '1.0', true);
    wp_register_script( 'flatpickr-es-js', LIB . '/flatpickr/flatpickr-es.min.js', array(), '1.0', true);
    wp_register_script( 'flatpickr-range-js', LIB . '/flatpickr/rangePlugin.js', array(), '1.0', true);
    wp_register_script( 'autoComplete-js', LIB . '/autoCompletejs/autoComplete.min.js', array(), '1.0', true);
    wp_register_script( 'leaflet-js', LIB . '/leaflet/leaflet.js', array(), '1.0', true);
    wp_register_script( 'calendar-js', LIB . '/calendar/calendar.js', array(), '1.0', true);
    wp_enqueue_style('leaflet-css', LIB . '/leaflet/leaflet.css', array(), '1.0', 'all');
}
add_action('wp_enqueue_scripts', 'libScript');


function zetenta_theme_styles(){
    $url_fontawesome = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css';
    $splide = 'https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.4/dist/css/splide.min.css';
    $air_datepicker_css = 'https://cdn.jsdelivr.net/npm/air-datepicker@3.5.3/air-datepicker.min.css';
    wp_register_style('fontawesome', $url_fontawesome, array(), '6.4.2', 'all');
    wp_register_style('flatpickr-css', LIB . '/flatpickr/flatpickr.min.css', array('fontawesome'), '1.0', 'all');
    wp_register_style('autocomplete', LIB . '/autoCompletejs/autoComplete.min.css', array('flatpickr-css'), '1.0', 'all');
    wp_register_style('splide', $splide, array('autocomplete'), '4.1.4', 'all');
    wp_register_style('zetenta-styles', get_stylesheet_uri(), array('splide', 'leaflet-css'), '1.0', 'all');
    wp_enqueue_style('zetenta-styles');
}

add_action('wp_enqueue_scripts', 'zetenta_theme_styles');
function zetenta_theme_scripts(){
    wp_register_script( 'zetenta-scripts', JS . '/index.min.js', array('jquery', 'splide', 'autoComplete-js', 'validator', 'leaflet-js', 'splide-grid', 'calendar-js'), '1.0', true);
    wp_enqueue_script('zetenta-scripts');
    wp_localize_script( 'zetenta-scripts', 'ajax_var', array(
        'url'    => admin_url( 'admin-ajax.php' ),
        'theme'    => ROOT
    ) );

}

add_action('wp_enqueue_scripts', 'zetenta_theme_scripts');
function add_module_type_to_script($tag, $handle, $src) {
    // Lista de manejadores de scripts que deben tener el atributo type="module"
    $module_scripts = array('');
    
    if (in_array($handle, $module_scripts)) {
        // Modificar la etiqueta del script para agregar type="module"
        $tag = '<script type="module" src="' . esc_url($src) . '"></script>';
    }
    
    return $tag;
}
add_filter('script_loader_tag', 'add_module_type_to_script', 10, 3);


// Template tu viaje
function enqueue_scripts_for_template() {
    if (is_page_template('templates-page/template-tu-viaje.php')) {
        wp_register_script('leaflet-js', 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js', array('zetenta-scripts'), null, true);
        wp_register_script( 'tu-viaje', LIB . '/tu-viaje/tu-viaje.js', array('leaflet-js'), '1.0', true);

        // Encola el script de Leaflet
        wp_enqueue_script('tu-viaje');
        // Encola el estilo de Leaflet
        wp_enqueue_style('leaflet-css', 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.css', array('zetenta-styles'), '1.0', 'all');
    } 
}
add_action('wp_enqueue_scripts', 'enqueue_scripts_for_template');