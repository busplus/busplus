
<?php get_header(); ?>
    
    <div class="single single-default">
        <div class="container">
            <div class="title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="body">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>