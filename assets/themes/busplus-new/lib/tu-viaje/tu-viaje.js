

window.addEventListener("DOMContentLoaded", () => {
  const form = document.getElementById("tu-viaje");
  const AJAX_URL = ajax_var.url;
  let intervalId = null;
  let map = null;
  const TIME_INTERVAL = 30000
  if (form) {
    let MESSAGE_ERROR = "( Este campo es obligatorio. )";
    const btnSend = document.getElementById('send-viaje');
    const serie = document.getElementById("serie");
    const boleto = document.getElementById("boleto");
    let ERROR = false;

    form.addEventListener("submit", (e) => {
      e.preventDefault();
      ERROR = false;
      let spanErrorSerie = serie.parentElement.firstElementChild.firstElementChild;
      let spanErrorBoleto = boleto.parentElement.firstElementChild.firstElementChild;

      if (validator.isEmpty(serie.value)) {
        spanErrorSerie.textContent = MESSAGE_ERROR;
        ERROR = true;
      }else{
        spanErrorSerie.textContent  = ""
      }
      if (validator.isEmpty(boleto.value)) {
        spanErrorBoleto.textContent = MESSAGE_ERROR;
        ERROR = true;
      }else{
        spanErrorBoleto.textContent = "";
      }



      if (!ERROR) {
        buttonLoader("", btnSend);
        fetch(`${AJAX_URL}?action=obtener_estado_viaje&serie=${serie.value}&boleto=${boleto.value}`, {
          method: "GET",
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.success) {
              cardDetailsHTML(data.data);
              buttonLoader("BUSCAR", btnSend);
            } else {
              showErrorHTML(data.data.message);
              buttonLoader("BUSCAR", btnSend);
            }
          })
          .catch((error) => {
            console.error("Error:", error);
            buttonLoader("BUSCAR", btnSend);
          });
      }
    });
    // TARJETA PRINCIPAL DE DATOS DEL PASAJE
    const cardDetailsHTML = (data) => {
      const form = document.querySelector('.form-tu-viaje');
      const container = document.querySelector('.details-tu-viaje');
      const cardContainer = document.querySelector('.card-container');
      const linkForm = cardContainer.getAttribute('data-link-form');

      let cardHTML = `
        <div class="card">
          <h2>Datos del pasajero y viaje</h2>
            <div class="pasajero">
                <h3>${data.pasajero_nombre} ${data.pasajero_apellido} ${data.pasajero_documento}: ${data.pasajero_n_documento}</h3>
            </div>
            <div class="table">
                <p>
                    <strong>Boleto ${data.boleto_serie}${data.boleto_id} | Butaca ${data.nro_butaca} | Categoría: ${data.categoria_asiento}</strong>
                </p>
                <table>
                                <thead>
                                    <tr>
                                        <th scope="col">ORIGEN</th>
                                        <th scope="col">DESTINO</th>
                                        <th scope="col">SALIDA</th>
                                        <th scope="col">LLEGADA</th>
                                        <th scope="col">EMPRESA</th>
                                        <th scope="col">IMPORTE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-label="ORIGEN">${data.origen}</td>
                                        <td data-label="DESTINO">${data.destino}</td>
                                        <td data-label="SALIDA">${data.hora_salida}</td>
                                        <td data-label="LLEGADA">${data.hora_llegada}</td>
                                        <td data-label="EMPRESA">${data.empresa}</td>
                                        <td data-label="IMPORTE">$ ${data.precio_total}</td>
                                    </tr>
                                </tbody>
                </table>
            </div>
            <h2 class="mt-md-6 mt-xs-4 mt-2">Localizá tu micro</h2>
            <p class="mb-2 localizacion-real">Ultima actualización: ${data.bus_horario} </p>
            <div class="map-container" id="map"></div>
            <div class="button__container justify-start-xs justify-center mt-4">
                <a href="${linkForm}" type="button" class="btn only-text orange icon" aria-label="Volver al formulario">
                    <i class="fa-solid fa-arrow-left-long mr-1"></i> Volver al formulario
                </a>
            </div>
        </div>
      `;

      form.classList.add('d-none');
      container.classList.remove('d-none');
      cardContainer.innerHTML = cardHTML;
      showMapBus(data);
    };
    // MENSAJE DE ERROR
    const showErrorHTML = (message) => {
      let container = document.getElementById('error-container');
      let errorHTML = "";

      if (message.length > 0) {
        errorHTML += `
          <p class="p-relative">
            <i class="fa-solid fa-circle-exclamation"></i> ${message}
          </p>
        `;
        container.innerHTML = errorHTML;
      } else {
        container.innerHTML = "";
      }
    };
    // LOADING BUTTON AL MOMENTO DE HACER FETCH
    const buttonLoader = (message, btn) => {
      let loaderHTML = "";
      if (message.length > 0) {
        btn.innerHTML = message;
      } else {
        loaderHTML += '<span class="loader"></span>';
        btn.innerHTML = loaderHTML;
      }
    };
    // MUESTRA EL MAPA
    const showMapBus = (data) => {
      const BASE_THEME = ajax_var.theme;
      const mapContainer = document.querySelector(".map-container");

      if (mapContainer) {
        if (!map) {
          map = new L.Map("map", {
            center: [data.bus_latitud, data.bus_longitud],
            zoom: 14,
            layers: [
              new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
              }),
            ],
          });
        } else {
          map.setView([data.bus_latitud, data.bus_longitud], 14);
        }

        // Clear existing markers
        map.eachLayer((layer) => {
          if (layer instanceof L.Marker) {
            map.removeLayer(layer);
          }
        });

        const icon_bus = L.icon({
          iconUrl: `${BASE_THEME}/assets/img/icons/ubicacion.png`,
          iconSize: [50, 50],
        });

        L.marker([data.bus_latitud, data.bus_longitud], { icon: icon_bus })
          .addTo(map)
          .bindPopup(`
            <div class="update-bus">
              <h4>Interno ${data.bus_interno}</h4>
              <p><strong>Patente:</strong> ${data.bus_patente}</p>
              <p><strong>ÚLTIMA SEÑAL RECIBIDA:</strong> ${data.bus_horario}</p>
              <p><strong>ÚLTIMA UBICACIÓN RECIBIDA:</strong> ${data.bus_direccion}</p>
            </div>
          `);

        // Add origin and destination markers
        const icon_terminal = L.icon({
          iconUrl: `${BASE_THEME}/assets/img/icons/ubicacion-origen.png`,
          iconSize: [50, 50],
        });

        L.marker([data.origen_latitud, data.origen_longitud], { icon: icon_terminal })
          .addTo(map)
          .bindPopup(`
            <div class="update-bus">
              <h4>${data.origen}</h4>
              <p><strong>Fecha salida:</strong> ${data.hora_salida}</p>
            </div>
          `);

        const icon_terminal_destino = L.icon({
          iconUrl: `${BASE_THEME}/assets/img/icons/ubicacion-origen.png`,
          iconSize: [50, 50],
        });

        L.marker([data.destino_latitud, data.destino_longitud], { icon: icon_terminal_destino })
          .addTo(map)
          .bindPopup(`
            <div class="update-bus">
              <h4>${data.destino}</h4>
              <p><strong>Fecha llegada:</strong> ${data.hora_llegada}</p>
            </div>
          `);

        // Clear existing interval if it exists
        if (intervalId) {
          clearInterval(intervalId);
        }
        let localizacion = document.querySelector('.localizacion-real');
        if(localizacion){
          localizacion.textContent = `Ultima actualización: ${data.bus_horario}`
        }
        // Set a new interval to call the API
        intervalId = setInterval(() => {
          callAPI();
        }, TIME_INTERVAL);
      }
    };
    // LLAMADO PARA ACTUALIZAR EL MAPA
    const callAPI = () => {
      fetch(`${AJAX_URL}?action=obtener_estado_viaje&serie=${serie.value}&boleto=${boleto.value}`, {
        method: "GET",
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.success) {
            showMapBus(data.data);
          } else {
            showErrorHTML(data.data.message);
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    };
  }
});