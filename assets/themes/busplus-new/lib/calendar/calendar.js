document.addEventListener("DOMContentLoaded", () => {
  if(document.getElementById("datepicker-container")){
    const monthList = document.getElementById("month-list");
    const selectedDatesText = document.getElementById("selected-dates");
    const applyButton = document.getElementById("apply-dates");
    const clearButton = document.getElementById("clear-dates");
    const closeBtn = document.querySelector(".close-calendar");
  
    let startDate = null;
    let endDate = null;
    const options = {
      timeZone: "America/Argentina/Buenos_Aires",
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZoneName: "long"
    };
    const currentDate = new Date(); // Objeto Date sin formatear
    const currentDay = currentDate.getDate(); // Día del mes actual
    const currentMonthIndex = currentDate.getMonth(); // Índice del mes actual
    const currentYearValue = currentDate.getFullYear(); // Año actual
    const fechaInicio = document.querySelector(".date-start");
    const fechaSalida = document.querySelector(".date-end");
    const meses = {
      enero: 0,
      febrero: 1,
      marzo: 2,
      abril: 3,
      mayo: 4,
      junio: 5,
      julio: 6,
      agosto: 7,
      septiembre: 8,
      octubre: 9,
      noviembre: 10,
      diciembre: 11,
    };

    // Crear meses desde el mes actual hasta diciembre del próximo año
    for (let i = currentDate.getMonth(); i < currentDate.getMonth() + 12; i++) {
   
      const year = currentDate.getFullYear() + Math.floor(i / 12);
      const month = i % 12;
      const date = new Date(year, month);

      const monthContainer = document.createElement("div");
      monthContainer.classList.add("month-container");
  
      const monthHeader = document.createElement("div");
      monthHeader.classList.add("month-header");
      monthHeader.textContent = date
        .toLocaleString("es-ES", { month: "long", year: "numeric" })
        .replace(" de", "");
       
      const daysGrid = createDaysGrid(date);
      monthContainer.appendChild(monthHeader);
      monthContainer.appendChild(daysGrid);
  
      monthList.appendChild(monthContainer);
    }
  
    function createDaysGrid(date) {
      const daysGrid = document.createElement("div");
      daysGrid.classList.add("days-grid");
      const firstDay =
        (new Date(date.getFullYear(), date.getMonth(), 1).getDay() + 6) % 7;
      const daysInMonth = new Date(
        date.getFullYear(),
        date.getMonth() + 1,
        0
      ).getDate();
    
      const currentDate = new Date(); // Fecha actual completa
      const currentDay = currentDate.getDate();
      const currentMonthIndex = currentDate.getMonth();
      const currentYearValue = currentDate.getFullYear();
    
      // Crear celdas vacías al principio
      for (let i = 0; i < firstDay; i++) {
        const emptyCell = document.createElement("div");
        emptyCell.classList.add("empty-day"); // Clase especial para los días vacíos
        daysGrid.appendChild(emptyCell);
      }
    
      // Crear los días del mes actual
      for (let day = 1; day <= daysInMonth; day++) {
        const dayElement = document.createElement("div");
        dayElement.innerHTML = `<span>${day}</span>`;
        dayElement.classList.add("day");
    
        const currentDateCheck = new Date(date.getFullYear(), date.getMonth(), day);
    
        // Agregar clase .day-off a los días anteriores a la fecha actual, excluyendo el día actual
        if (currentDateCheck < currentDate && !(day === currentDay && date.getMonth() === currentMonthIndex && date.getFullYear() === currentYearValue)) {
          dayElement.classList.add("day-off");
        }
    
        // Comparar con el día actual y agregar la clase `currentDay`
        if (
          date.getFullYear() === currentYearValue &&
          date.getMonth() === currentMonthIndex &&
          day === currentDay
        ) {
          dayElement.classList.add("currentDay");
        }
    
        dayElement.onclick = () =>
          handleDayClick(
            new Date(date.getFullYear(), date.getMonth(), day),
            dayElement
          );
        daysGrid.appendChild(dayElement);
      }
    
      return daysGrid;
    }
  
    function handleDayClick(date, element) {
      if(!element.classList.contains('day-off')){
        if (!startDate || (startDate && endDate)) {
            startDate = date;
            endDate = null;
            clearSelected(); // Limpiamos todas las clases seleccionadas y de rango
            element.classList.add("selected-start");
            if (element.nextElementSibling) {
            element.nextElementSibling.classList.add("range-first");
            }
        } else if (date < startDate) {
            startDate = date;
            clearSelected(); // Limpiamos todas las clases seleccionadas y de rango
            element.classList.add("selected-start");
        } else {
            endDate = date;
            element.classList.add("selected-end");
            if (element.previousElementSibling) {
            element.previousElementSibling.classList.add("range-last");
            }
            markRange();
        }
      }
      updateDateText(); // Actualiza el texto de las fechas seleccionadas
    }
  
    let rangeDays = []; // Array para almacenar los días en el rango
  
    function markRange() {
      clearSelected(); // Limpiamos todas las clases antes de marcar el rango
  
      // Si `startDate` es mayor que `endDate`, intercambiamos las fechas
      if (startDate > endDate) [startDate, endDate] = [endDate, startDate];
  
      const allDays = document.querySelectorAll(".day");
      const areSameDate = (date1, date2) => {
        return (
          date1.getDate() === date2.getDate() &&
          date1.getMonth() === date2.getMonth() &&
          date1.getFullYear() === date2.getFullYear()
        );
      };
  
      allDays.forEach((dayDiv) => {
        const day = parseInt(dayDiv.textContent);
        const parent = dayDiv.closest(".month-container");
        const monthHeaderText = parent
          .querySelector(".month-header")
          .textContent.trim()
          .toLowerCase();
  
        const [monthName, yearString] = monthHeaderText.split(" ");
        const year = parseInt(yearString);
        const monthIndex = meses[monthName];
  
        if (isNaN(year) || monthIndex === undefined || isNaN(day)) return;
  
        const date = new Date(year, monthIndex, day);
        if (isNaN(date.getTime())) return;
  
        // Agregar clases para marcar el rango
        if (areSameDate(date, startDate)) {
          dayDiv.classList.add("selected-start");
          if (dayDiv.nextElementSibling) {
            dayDiv.nextElementSibling.classList.add("range-first");
          }
        } else if (areSameDate(date, endDate)) {
          dayDiv.classList.add("selected-end");
          dayDiv.classList.add("range-last");
          if (dayDiv.previousElementSibling) {
            dayDiv.previousElementSibling.classList.add("range-last");
          }
        } else if (date > startDate && date < endDate) {
          dayDiv.classList.add("range");
        }
      });
    }
  
    function clearSelected() {
      // Eliminar todas las clases seleccionadas y las de rango
      document.querySelectorAll(".day").forEach((day) => {
        day.classList.remove(
          "selected-start",
          "selected-end",
          "range",
          "range-first",
          "range-last"
        );
      });
    }
  
    function updateDateText() {
      const formatDate = (date) => {
        const month = date.toLocaleString("es-ES", { month: "long" });
        const capitalizedMonth = month.charAt(0).toUpperCase() + month.slice(1);
        return `${date.getDate()} ${capitalizedMonth}`;
      };
  
      selectedDatesText.textContent =
        startDate && endDate
          ? `${formatDate(startDate)} - ${formatDate(endDate)}`
          : startDate
          ? formatDate(startDate)
          : "Seleccione un rango";
    }


  
    applyButton.onclick = () => {
      const formatDate = (date) => {
        const day = String(date.getDate()).padStart(2, "0");
        const month = String(date.getMonth() + 1).padStart(2, "0");
        const year = date.getFullYear();
        return `${day}/${month}/${year}`;
      };
  
      if (fechaInicio && startDate) {
        fechaInicio.value = formatDate(startDate);
      }
  
      if (fechaSalida && endDate) {
        fechaSalida.value = formatDate(endDate);
      }
      document.getElementById("datepicker-container").classList.remove("show");
    };
  
    closeBtn.addEventListener("click", () => {
      document.getElementById("datepicker-container").classList.remove("show");
    });
  
      fechaInicio.addEventListener("focus", () => {
        document.getElementById("datepicker-container").classList.add("show");
      });
      fechaSalida.addEventListener("focus", () => {
        document.getElementById("datepicker-container").classList.add("show");
      });
   
  
  
    clearButton.onclick = () => {
      startDate = null;
      endDate = null;
      fechaInicio.value = "";
      fechaSalida.value = "";
  
      clearSelected();
      updateDateText();
    };
  }
  });
  