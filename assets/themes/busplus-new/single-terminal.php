<?php get_header(); ?>
<?php 
    $direccion = get_field('direccion');
    $contenido = get_field('contenido');
    $como_llegar = get_field('como_llegar');
    $estaciones_cercanas = get_field('estaciones_cercanas');
    $telefono = get_field('telefono');

    $image_url = IMAGE . '/destinos/terminales-hero.jpg'; // Cambia la ruta según tu estructura de carpetas
    // Verificar si el post tiene una imagen destacada
    if (has_post_thumbnail($post_id)) {
        // Obtener la URL de la imagen destacada
        $image_url = get_the_post_thumbnail_url();
    } else {
        // Fallback a una imagen por defecto
        $image_url =  IMAGE . '/destinos/terminales-hero.jpg'; // Cambia la ruta según tu estructura de carpetas
    }

?>
<main class="single single-destino single-terminales pagina-detalle-terminal">
    <section class="hero" style="background-image:url(<?php echo $image_url;?>);">
         <div class="container">
            <div class="hero-content">
                <span class="title-terminal"><?php the_title();?></span>
                <h1 class="f-28-20 f-extrabold">
                    <?php echo $titulo;?>
                    Pasajes desde la <?php the_title();?>
                    <strong>a todo el país.</strong>
                </h1>
            </div>
        </div>
    </section>
    <?php show_block_buscador(); ?>
    <section class="block-text">
        <div class="container-sm">
            <div class="single-body">
                <h2>Donde está la <?php the_title();?></h2>
                <p><strong>Dirección:</strong> <?php echo $direccion;?></p>
        

                        <?php 
                            $g_direccion = get_field( 'google_maps_direccion' ); 
                            $g_ciudad = get_field( 'g_ciudad' );
                            $latitud_y_longitud = get_field( 'latitud_y_longitud' );
                            if($latitud_y_longitud){
                                $g_direccion =  str_replace(' ', '%20', $latitud_y_longitud);
                            }else{
                                $codificar = rawurlencode(get_field( 'google_maps_direccion' ));
                                $g_direccion = str_replace('%26', '&amp;', $codificar);
                            }
                         
                        ?>
                        <div class="google-maps">
                            <a href='http://mapswebsite.net/es' style="opacity:0; position:absolute;z-index:-1;">mapswebsite.net/es</a>
                            <div class="map">
                                <iframe 
                                    width="100%" 
                                    height="100%" 
                                    frameborder="0" 
                                    scrolling="no" 
                                    marginheight="0" 
                                    marginwidth="0" 
                                    src="https://maps.google.com/maps?width=100%25&amp;height=100%&amp;hl=en&amp;q=<?php echo $g_direccion;?>&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed">
                                </iframe>
                            </div>
                        </div>
                <div style="width: 100%">


                </div>
                <?php if($como_llegar): ?>
                    <h2>
                        Como llegar a la <?php the_title();?>
                    </h2>
                    <?php echo $como_llegar; ?>
                <?php endif; ?>
             
                <?php if($estaciones_cercanas): ?>
                    <h2>
                        Estaciones de tren cercanas a la terminal <?php the_title();?>
                    </h2>
                    <?php echo  $estaciones_cercanas; ?>
                <?php endif; ?>
                <?php if($telefono): ?>
                    <p><strong>Teléfono:</strong> <?php echo $telefono;?></p>
                <?php endif; ?>
            </div>
            <div class="button__container w-100 d-flex justify-center align-items-center btn-archive">
                    <?php
                    $url = get_field( 'terminales', 'options' );
                    if ( $url ) : ?>
                        <a href="<?php echo esc_url( $url ); ?>" class="btn only-text orange icon" aria-label="Ver todas las ofertas">
                            <i class="fa-solid fa-chevron-left mr-1"></i>
                            VOLVER A TERMINALES
                        </a>
                    <?php endif; ?>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>