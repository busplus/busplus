<?php get_header(); ?>
    <main class="single homepage">
        <?php if ( have_rows( 'portada' ) ) : ?>
            <?php while ( have_rows( 'portada' ) ) :
            the_row(); ?>
                <?php 
                $video = get_sub_field( 'video' ); 
                $video_2 = get_sub_field( 'video_2' ); 
                ?>
                <section class="hero">
                    <div class="video-container">             
                        <?php if($video): ?>
                            <video class="hero__video-desktop d-flex-sm d-none" width="100%" height="100%" autoplay="" muted="" loop="">
                                <source src="<?php echo esc_url( $video['url'] ); ?>" type="video/mp4">
                                Tu navegador no soporta el elemento de video.
                            </video>
                        <?php endif; ?>
                        <?php if($video_2): ?>
                            <video class="hero__video-mobile d-none-sm" width="100%" height="100%" autoplay="" muted="" loop="">
                                <source src="<?php echo esc_url( $video_2['url'] ); ?>" type="video/mp4">
                                Tu navegador no soporta el elemento de video.
                            </video>
                        <?php endif; ?>
                    </div>
                    <div class="container">
                        <div class="content">
                            <?php 
                            $titulo = get_sub_field( 'titulo' ); 
                            $sub_titulo = get_sub_field( 'sub_titulo' ); 
                            ?>
                            <?php 
                                insert_acf($titulo, 'h1');
                                insert_acf($sub_titulo, 'h2');
                            ?>

                        </div>
                    </div>
                </section>
                <?php show_block_buscador(); ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'tarjetas' ) ) : ?>
            <?php while ( have_rows( 'tarjetas' ) ) :
            the_row(); ?>
            <?php 
                // CARD 1
                $titulo = get_sub_field( 'titulo' );
                $texto = get_sub_field( 'texto' );
                $link = get_sub_field( 'link' );
                $link_url = $link['url'];
                $imagen = get_sub_field( 'imagen' );
                // CARD 2
                $titulo_2 = get_sub_field( 'titulo_2' );
                $texto_2 = get_sub_field( 'texto_2' );
                $link_2 = get_sub_field( 'link_2' );
                $link_url_2 = $link_2['url'];
                $imagen_2 = get_sub_field( 'imagen_2' );
                // CARD 3
                $titulo_3 = get_sub_field( 'titulo_3' );
                $texto_3 = get_sub_field( 'texto_3' );
                $link_3 = get_sub_field( 'link_3' );
                $link_url_3 = $link_3['url'];
                $imagen_3 = get_sub_field( 'imagen_3' );
            ?>
            <div class="card-block">
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <?php if($link): ?>
                                <a href="<?php echo $link_url;?>" title="<?php echo $titulo;?>">
                                    <div class="card">
                                        <div class="card__main-content">
                                            <?php if($imagen): ?>
                                                <div class="icon-wrapper">
                                                    <?php insert_image($imagen, '400') ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php 
                                                insert_acf($titulo, 'h3');
                                            ?>
                                        </div>
                                        <?php echo insert_textarea($texto); ?>
                                        <div class="plus-icon-wrapper">
                                                <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                </svg>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="col-4">
                            <?php if($link_2): ?>
                                <a href="<?php echo $link_url_2;?>" title="<?php echo $titulo_2;?>">
                                    <div class="card">
                                        <div class="card__main-content">
                                            <?php if($imagen_2): ?>
                                                <div class="icon-wrapper">
                                                    <?php insert_image($imagen_2, '400') ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php 
                                                insert_acf($titulo_2, 'h3');
                                            ?>
                                        </div>
                                        <?php echo insert_textarea($texto_2); ?>
                                        <div class="plus-icon-wrapper">
                                                <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                </svg>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="col-4">
                            <?php if($link_3): ?>
                                <a href="<?php echo $link_url_3;?>" title="<?php echo $titulo_3;?>">
                                    <div class="card">
                                        <div class="card__main-content">
                                            <?php if($imagen_3): ?>
                                                <div class="icon-wrapper">
                                                    <?php insert_image($imagen_3, '400') ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php 
                                                insert_acf($titulo_3, 'h3');
                                            ?>
                                        </div>
                                        <?php echo insert_textarea($texto_3); ?>
                                        <div class="plus-icon-wrapper">
                                                <svg id="plus-icon" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3 9.91318C3 7.67297 3 6.55287 3.43597 5.69722C3.81947 4.94457 4.43139 4.33265 5.18404 3.94916C6.03969 3.51318 7.15979 3.51318 9.4 3.51318H14.6C16.8402 3.51318 17.9603 3.51318 18.816 3.94916C19.5686 4.33265 20.1805 4.94457 20.564 5.69722C21 6.55287 21 7.67297 21 9.91318V15.1132C21 17.3534 21 18.4735 20.564 19.3291C20.1805 20.0818 19.5686 20.6937 18.816 21.0772C17.9603 21.5132 16.8402 21.5132 14.6 21.5132H9.4C7.15979 21.5132 6.03969 21.5132 5.18404 21.0772C4.43139 20.6937 3.81947 20.0818 3.43597 19.3291C3 18.4735 3 17.3534 3 15.1132V9.91318Z" fill="currentColor" />
                                                <path d="M12 8.51318L12 16.5132" stroke="white" stroke-linejoin="round" />
                                                <path d="M16 12.5132L8 12.5132" stroke="white" stroke-linejoin="round" />
                                                </svg>
                                        </div>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'promociones' ) ) : ?>
            <?php while ( have_rows( 'promociones' ) ) :
            the_row(); ?>
            <?php 
                $titulo = get_sub_field('titulo'); 
                $link = get_sub_field('link');
            ?>
            <?php if($titulo): ?>
                <section class="main__promociones-container">
                    <div class="container">
                        <div class="block-title heading-arrow">
                            <h2 class="f-24-18 f-bold p-relative">
                                <?php echo $titulo; ?>
                                <?php insert_button($link, 0, 'only-text orange'); ?>
                            </h2>
                        </div>
                        <div class="images-gallery row">
                            <?php
                                $posts = get_sub_field( 'promo' );
                                $descuentos = get_field( 'promociones', 'options' );
                   
                                if ( $posts ) : ?>
                                <?php $i = 0; ?>
                                <?php foreach( $posts as $post) : ?>
                                <?php $i++; ?>
                                <?php setup_postdata( $post ); ?>
                                <?php
                                    $link_item = get_field( 'link', $post->ID)['url'] ?? $descuentos;
                           
                                    ?>

                                    <div class="col-sm-4 col-6 promo<?php echo $i;?>" style="order: <?php echo $i;?>;">
                                        <a class="card-promo" href="<?php echo esc_url($link_item)?>" title="<?php echo the_title();?>">
                                            <?php the_post_thumbnail('large'); ?>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                        <div class="btn-down d-none-xs">
                            <?php insert_button($link, 0, 'only-text orange'); ?>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'lista' ) ) : ?>
            <?php while ( have_rows( 'lista' ) ) :
            the_row(); ?>
            <?php 
                $titulo = get_sub_field( 'titulo' );
                $posts = get_sub_field( 'experiencia' );
                $link = get_sub_field( 'link' );
 

            ?>
                <?php if($titulo): ?>
                    <section class="block-experiencia">
                        <div class="container">
                            <div class="block-title heading-arrow">
                                <h2 class="f-24-18 f-bold p-relative">
                                    <?php echo $titulo; ?>
                                    <?php insert_button($link, 0, 'only-text orange'); ?>
                                </h2>


                            </div>
                            <div class="splide" id="experiencia">
                                <div class="splide__track">
                                        <ul class="splide__list">
                                            <?php
                                                $posts = get_sub_field( 'experiencia' );
                                                if ( $posts ) : ?>
                                            <?php foreach( $posts as $post) : ?>
                                                <?php setup_postdata( $post ); ?>
                                                    <li class="splide__slide">
                                                            <?php 
                                                                $post_title = get_field('titulo');
                                                                $post_texto = get_field('texto');
                                                                $post_precio  = get_field('precio');
                                                            ?>
                                                        <a class="card" href="<?php echo $link['url']?>" aria-lable="<?php echo $post_title; ?>">
                                                            <div class="block-image">
                                                                <?php the_post_thumbnail();?>
                                                            </div> 
                                                            <div class="block-content">
                                                                <?php 
                                                                    insert_acf($post_title, 'h3');
                                                                    insert_acf($post_texto, 'p');

                                                                    echo('<span>$</span>');insert_acf($post_precio, 'span');
                                                                ?>
                                                            </div>
                                                        </a>
                                                    </li>
                                            <?php endforeach; ?>
                                                <?php wp_reset_postdata(); ?>
                                            <?php endif; ?>


                                        </ul>
                                </div>
                            </div>
                            <div class="btn-down d-none-xs">
                                <?php insert_button($link, 0, 'only-text orange'); ?>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'filtro' ) ) : ?>
            <?php while ( have_rows( 'filtro' ) ) :
            the_row(); ?>
                <?php $titulo = get_sub_field( 'titulo' ); ?>
                <?php if($titulo): ?>
                    <?php echo render_block_destino($titulo, 'bg-white'); ?>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'empresas' ) ) : ?>
            <?php while ( have_rows( 'empresas' ) ) :
            the_row(); ?>
                <?php 
                    $titulo  = get_sub_field( 'titulo' ); 
                    $link_empresas  = get_sub_field( 'link' ); 
                ?>
                <?php if($titulo): ?>
                    <section class="block block-empresas">
                        <div class="container">
                            <div class="block-title">
                                <h2 class="f-18-16 f-bold p-relative">
                                    <?php echo $titulo; ?>
                                </h2>
                            </div>
                            <div class="splide" id="empresas">
                                <div class="splide__track">
                                    <ul class="splide__list">
                                        <?php if ( have_rows( 'logos' ) ) : ?>
                                            <?php while ( have_rows( 'logos' ) ) :
                                            the_row(); ?>
                                                <li class="splide__slide">
                                                <?php
                                                $logos = get_sub_field( 'logos' );
                                                $link = get_sub_field('link') ? get_sub_field('link') : 'javascript:void(0);';
                                                $target = get_sub_field('link') ? '_blank' : '';
                                                ?>
                                                    <a  href="<?php echo $link ;?>" class="card" target="<?php echo $target; ?>">
                                                        <div class="block-image">
                                                            <?php insert_image($logos, 1024 );?>
                                                        </div> 
                                                    </a>
                                                </li>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
    
                                    </ul>
                                </div>
                            </div>
                            <?php insert_button($link_empresas, 4, 'btn-orange'); ?>
                        </div>
                    </section>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if ( have_rows( 'financiacion' ) ) : ?>
            <?php while ( have_rows( 'financiacion' ) ) :
            the_row(); ?>
                <?php 
                    $titulo  = get_sub_field( 'titulo' ); 
                    $link_financiacion  = get_sub_field( 'link' ); 
                ?>
                <?php if($titulo):?>
                    <section class="block block-financiacion">
                        <div class="container">

                            <div class="block-title">
                                <h2 class="f-18-16 f-bold p-relative">
                                    <?php echo $titulo; ?>
                                </h2>
                            </div>
                            <div class="splide" id="financiacion">
                                <div class="splide__track">
                                    <ul class="splide__list">
                                        <?php if ( have_rows( 'logos' ) ) : ?>
                                            <?php while ( have_rows( 'logos' ) ) :
                                            the_row(); ?>
                                                <li class="splide__slide">
                                                <?php
                                                $logo = get_sub_field( 'logo' );
                                                
                                                ?>
                                                    <div class="card" >
                                                        <div class="block-image">
                                                            <?php insert_image($logo, 1024 );?>
                                                        </div> 
                                                    </div>
                                                </li>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
    
                                    </ul>
                                </div>
                            </div>
                            <?php insert_button($link_financiacion, 4, 'btn-orange'); ?>
                        </div>
                    </section>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php get_template_part( 'template-parts/content', 'articles');?>
    </main>
<?php get_footer(); ?>