export const faq = () => {
    const accordings = document.querySelectorAll('.faq-according');
    if (accordings.length > 0) {
        accordings.forEach(acc => {
            const pointer = acc.querySelector('.faq-according-title');
            const answer = acc.querySelector('.faq-according-info');
    
            pointer.addEventListener('click', () => {
                accordings.forEach(otherAcc => {
                    if (otherAcc !== acc) {
                        let otherAnswer = otherAcc.querySelector('.faq-according-info');
                        otherAcc.classList.remove('show');
                        otherAnswer.style.maxHeight = "0px";


                    }
                });
    
                acc.classList.toggle('show');
    
                if (acc.classList.contains('show')) {
                    answer.style.maxHeight = answer.scrollHeight + "px";
                } else {
                    answer.style.maxHeight = "0px";

                }
            });
        });
    }
}