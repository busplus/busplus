export const buscador_2 = () => {
    const buscadorContainer = document.querySelector('.buscador')
    let autoCompleteOrigen;
    let autoCompleteSalida;
    
    if(buscadorContainer){
        
        // setupCalendar('fechaIngreso', 'fechaSalida');
        activeCupon();
        getParadas();
        defaultValueParadas(autoCompleteOrigen, autoCompleteSalida);
        obtenerParadas(autoCompleteOrigen, autoCompleteSalida);
        searchBus();


    }
}

// LITEPICKER 

const dateFormat = date =>  {
    const dia = date.getDate().toString().padStart(2, '0');
    const mes = (date.getMonth() + 1).toString().padStart(2, '0');
    const ano = date.getFullYear();
    return `${dia}/${mes}/${ano}`;
}

const transformarFecha = (fecha) => {
    // Dividir la fecha en día, mes y año
    const [dia, mes, año] = fecha.split('/');

    // Reorganizar y devolver la fecha en formato yyyy-mm-dd
    return `${año}-${mes}-${dia}`;
}
const setupCalendar = (ingreso, salida) => {
    const inputIngreso = document.getElementById(ingreso);
    const inputSalida = document.getElementById(salida);
    const today = new Date();
    // Configura flatpickr para el campo de fecha de ingreso
    const fpIngreso = flatpickr(inputIngreso, {
        locale: "es",
        dateFormat: "d/m/Y",
        position: "below",
        disableMobile: true,
        defaultDate: today,
        minDate: today,
        onChange: function(selectedDates, dateStr, instance) {
            if (selectedDates.length > 0) {
                // Establece la fecha de ingreso
                inputIngreso.value = dateFormat(selectedDates[0]);

                // Actualiza el minDate del calendario de salida
                const minDate = new Date(selectedDates[0]);
                fpSalida.set('minDate', minDate);
                
                // Al seleccionar la fecha, enfoca el campo de salida
                inputSalida.focus();
            }
        }
    });

    // Configura flatpickr para el campo de fecha de salida
    const fpSalida = flatpickr(inputSalida, {
        locale: "es",
        dateFormat: "d/m/Y",
        position: "below",
        disableMobile: true,
        minDate: today,
        onChange: function(selectedDates, dateStr, instance) {
            if (selectedDates.length > 0) {
                inputSalida.value = dateFormat(selectedDates[0]);
                
                // Calcula la diferencia en días si ambos campos tienen fechas seleccionadas
                if (inputIngreso.value && inputSalida.value) {
                    const fechaIngreso = new Date(inputIngreso.value.split('/').reverse().join('/'));
                    const fechaSalida = new Date(inputSalida.value.split('/').reverse().join('/'));
                    const diferencia = Math.abs(fechaSalida - fechaIngreso);
                    const diasDiferencia = Math.ceil(diferencia / (1000 * 60 * 60 * 24));
                    inputSalida.dataset.total = diasDiferencia;
                } else {
                    // Si solo hay una fecha (ingreso o salida), puedes manejarlo aquí si es necesario
                    inputSalida.dataset.total = '';
                }
            }
        }
    });

    // Abre el calendario correspondiente cuando se enfoca en el campo
    inputIngreso.addEventListener('focus', () => {
        fpIngreso.open();
    });

    inputSalida.addEventListener('focus', () => {
        fpSalida.open();
    });
}

//PARADAS
const getParadas = async () => {
    let external_paradas = [];
    let url_api = '';
    let session_api = '';

    try {
        const password = {
            clave_acceso: "qu3r1c4qu3s0s",
        };
        const env = sessionStorage.getItem('env');
        url_api = env === 'test' ? "https://ws.viatesting.com.ar/" : "https://ws.busplus.com.ar/";

        // Primer llamado API para obtener la clave de sesión
        const sessionResponse = await fetch(url_api + "sesion", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(password),
        });

        if (!sessionResponse.ok) {
            throw new Error('Error obteniendo la clave de sesión');
        }

        const key = await sessionResponse.json();
        session_api = key.key;

        // Segundo llamado API para obtener las paradas
        const paradasResponse = await fetch(url_api + "paradas", {
            method: "GET",
            headers: {
                "X-API-KEY": session_api,
                "Content-Type": "application/json",
            },
        });

        if (!paradasResponse.ok) {
            throw new Error('Error obteniendo las paradas');
        }

        const data = await paradasResponse.json();
        external_paradas = data;

    } catch (error) {
        console.error('Error en getParadas:', error);
    }

    return external_paradas;
}

const obtenerParadas = async (origen, salida) => {
    let paradas = await getParadas();
 
    let paradasData = paradas.map(parada => ({
        descripcion: parada.descripcion,
        id: parada.idparada
    }));
    
    origen = new autoComplete({
        selector: "#origen",
        placeHolder: "Ciudad de origen...",
        data: {
            src: paradasData,
            keys: ["descripcion"],
            cache: true
        },
        resultsList: {
            element: (list, data) => {
                if (!data.results.length) {
                    const message = document.createElement("div");
                    message.setAttribute("class", "no-results");
                    message.innerHTML = `<span>No se encontraron resultados</span>`;
                    list.appendChild(message);
                }
            },
            noResults: true,
            maxResults: 15,
            tabSelect: true
        },
        events: {
            input: {
                selection: (event) => {
                    const selection = event.detail.selection;
                    origen.input.value = selection.value.descripcion;
                    origen.input.dataset.id = selection.value.id;
                }
            }
        }
    });

     salida = new autoComplete({
        selector: "#salida",
        placeHolder: "Ciudad de salida...",
        data: {
            src: paradasData,
            keys: ["descripcion"],
            cache: true
        },
        resultsList: {
            element: (list, data) => {
                if (!data.results.length) {
                    const message = document.createElement("div");
                    message.setAttribute("class", "no-results");
                    message.innerHTML = `<span>No se encontraron resultados</span>`;
                    list.appendChild(message);
                } 
            },
            noResults: true,
            maxResults: 15,
            tabSelect: true
        },
        events: {
            input: {
                selection: (event) => {
                    const selection = event.detail.selection;
                    salida.input.value = selection.value.descripcion;
                    salida.input.dataset.id = selection.value.id;
                }
            }

        }
    });


    
    const changeParadas = () => {
        const origenInput = document.getElementById('origen');
        const salidaInput = document.getElementById('salida');
        const button = document.getElementById('cambiarInputs');
        button.addEventListener('click', () => {
            // Guarda los valores y data-ids actuales
            let valueOrigen = origenInput.value;
            let valueSalida = salidaInput.value;
            let dataIdOrigen = origenInput.dataset.id;
            let dataIdSalida = salidaInput.dataset.id;

            // Intercambia los valores y data-ids
            origenInput.value = valueSalida;
            salidaInput.value = valueOrigen;

            origenInput.dataset.id = dataIdSalida;
            salidaInput.dataset.id = dataIdOrigen;

            // Fuerza la actualización del autocompletado
            if (origen) {
                origen.input.value = valueSalida;  // Actualiza el valor del input
                // origen.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
            }
            if (salida) {
                salida.input.value = valueOrigen;  // Actualiza el valor del input
                // salida.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
            }

        });
    }

    changeParadas();
};


const activeCupon = () => {
    const input = document.getElementById('checkCupon');
    const buscador = document.querySelector('.buscador ')

    input.addEventListener('click', () => {
       if(input.checked){
        buscador.classList.add('cupon-active')
       }else{
        buscador.classList.remove('cupon-active')
       }
    })
}



const defaultValueParadas = async () => {
    let paradas = await getParadas();
    const origen = document.getElementById('origen');
    const salida = document.getElementById('salida');
    const origenID = parseInt(origen.getAttribute('data-id'));
    const salidaID = parseInt(salida.getAttribute('data-id'));

    if (origenID) {
        const paradaOrigen = paradas.find(parada => parada.idparada === origenID);
        if (paradaOrigen) {
            origen.value = paradaOrigen.descripcion;
        }
    }

    if (salidaID) {
        const paradaSalida = paradas.find(parada => parada.idparada === salidaID);
        if (paradaSalida) {
            salida.value = paradaSalida.descripcion;
        }
    }
}

const searchBus = () => {
    let submit = document.getElementById('searchSubmit');
    let cupon = document.querySelector('.btn-descuento');
    let cuponVerify = document.getElementById('cupon-verify');
    let CUPON_VALIDO = false;
    cupon.addEventListener('click', (e) => {
        let cuponInput = document.getElementById('cupon')
        e.preventDefault();
 
        if(cuponVerify.value.trim() !== ''){
            if(cuponVerify.value.trim() === cuponInput.value.trim()){
                cuponInput.parentElement.classList.add('input-success');
                cuponInput.parentElement.classList.remove('input-error');
                CUPON_VALIDO = true
            }else{
                cuponInput.parentElement.classList.add('input-error');
                cuponInput.parentElement.classList.remove('input-success');
                cuponInput.value = "";
                cuponInput.placeholder = "No válido";
                CUPON_VALIDO = false;
            }
        }else{
            cuponInput.parentElement.classList.add('input-error');
            cuponInput.parentElement.classList.remove('input-success');
            cuponInput.placeholder = "No válido";
            CUPON_VALIDO = false;
        }

    })

    submit.addEventListener('click', (e) => {
        e.preventDefault();
        
        const origen = document.getElementById('origen');
        const salida = document.getElementById('salida');
        const fechaIngreso = document.getElementById('fechaIngreso');
        const fechaSalida = document.getElementById('fechaSalida');
        const cupon = document.getElementById('cupon');

        if(validator.isEmpty(origen.value)){
            let container = origen.parentElement.parentElement
            container.classList.add('input-error')
        }else{
            let container = origen.parentElement.parentElement
            container.classList.remove('input-error')

        }
        if(validator.isEmpty(salida.value)){
            let container = salida.parentElement.parentElement
            container.classList.add('input-error')
        }else{
            let container = salida.parentElement.parentElement
            container.classList.remove('input-error')

        }
        if(validator.isEmpty(fechaIngreso.value)){
            let container = fechaIngreso.parentElement
            container.classList.add('input-error')
        }else{
            let container = fechaIngreso.parentElement
            container.classList.remove('input-error')

        }


        // Construcción de la URL base
        let BASE_URL = 'https://check.busplus.com.ar';
        let url;

        // Determinar la ruta base según la presencia de fechaSalida
        if (fechaSalida.value.trim() !== '') {
            url = BASE_URL + `/viajes/${origen.dataset.id}/${transformarFecha(fechaIngreso.value)}/1`;
        } else {
            url = BASE_URL + `/viaje/${origen.dataset.id}/${transformarFecha(fechaIngreso.value)}/1`;
        }

        // Agregar salida si está presente
        if (salida.value.trim() !== '') {
            url += `/${salida.dataset.id}`;
        }

        // Agregar fechaSalida si está presente y ya se usa `/viajes`
        if (fechaSalida.value.trim() !== '' && url.includes('/viajes')) {
            url += `/${transformarFecha(fechaSalida.value)}`;
        }

        // Agregar cupón si es válido
        if (CUPON_VALIDO !== false) {
            url += `/${cupon.value}`;
        } else {
            url += `/0`;
        }
        window.open(url);
  
    })

}