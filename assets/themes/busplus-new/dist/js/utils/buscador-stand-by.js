export const buscador = () => {
  // const buscadorContainer = document.querySelector(".buscador");
  // let autoCompleteOrigen;
  // let autoCompleteSalida;

  // if (buscadorContainer) {
  //   setupCalendar("fechaIngreso", "fechaSalida");
  //   removeSelectionParadas();
  //   activeCupon();
  //   getParadas();
  //   defaultValueParadas(autoCompleteOrigen, autoCompleteSalida);
  //   obtenerParadas(autoCompleteOrigen, autoCompleteSalida);
  //   labelMove();
  //   countPasajeros();
  //   searchBus();
  //   codigoDescuento();
  //   inputCodigoDescuento();
  // }
};

// Transformador de fecha para el calendario
const dateFormat = (date) => {
  const dia = date.getDate().toString().padStart(2, "0");
  const mes = (date.getMonth() + 1).toString().padStart(2, "0");
  const ano = date.getFullYear();
  return `${dia}/${mes}/${ano}`;
};
// Transformar las fechas para la busqueda
const transformarFecha = (fecha) => {
  // Dividir la fecha en día, mes y año
  const [dia, mes, año] = fecha.split("/");

  // Reorganizar y devolver la fecha en formato yyyy-mm-dd
  return `${año}-${mes}-${dia}`;
};

const setupCalendar = (ingreso, salida) => {
  const inputIngreso = document.getElementById(ingreso);
  const inputSalida = document.getElementById(salida);
  // const today = new Date();

  // // Configura el calendario para la fecha de ingreso
  // const fpIngreso = flatpickr(inputIngreso, {
  //   locale: "es",
  //   dateFormat: "d/m/Y",
  //   position: "below",
  //   disableMobile: true,
  //   minDate: today
  // });

  // // Configura el calendario para la fecha de salida
  // const fpSalida = flatpickr(inputSalida, {
  //   locale: "es",
  //   dateFormat: "d/m/Y",
  //   position: "below",
  //   disableMobile: true,
  //   minDate: today
  // });

  // // Abre el calendario correspondiente al enfocar en el campo de ingreso
  // inputIngreso.addEventListener("focus", () => {
  //   fpIngreso.open();
  // });

  // // Abre el mismo calendario al hacer foco en el campo de salida
  // inputSalida.addEventListener("focus", () => {
  //   fpSalida.open();
  // });
};


//Fecth a la API de Busplus
const getParadas = async () => {
  let external_paradas = [];
  let url_api = "";
  let session_api = "";

  try {
    const password = {
      clave_acceso: "qu3r1c4qu3s0s",
    };
    const env = sessionStorage.getItem("env");
    url_api =
      env === "test"
        ? "https://ws.viatesting.com.ar/"
        : "https://ws.busplus.com.ar/";

    // Primer llamado API para obtener la clave de sesión
    const sessionResponse = await fetch(url_api + "sesion", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(password),
    });

    if (!sessionResponse.ok) {
      throw new Error("Error obteniendo la clave de sesión");
    }

    const key = await sessionResponse.json();
    session_api = key.key;

    // Segundo llamado API para obtener las paradas
    const paradasResponse = await fetch(url_api + "paradas", {
      method: "GET",
      headers: {
        "X-API-KEY": session_api,
        "Content-Type": "application/json",
      },
    });

    if (!paradasResponse.ok) {
      throw new Error("Error obteniendo las paradas");
    }

    const data = await paradasResponse.json();
    external_paradas = data;
  } catch (error) {
    console.error("Error en getParadas:", error);
  }

  return external_paradas;
};
// Obtener paradas y autocomplete
const obtenerParadas = async (origen, salida) => {
  let paradas = await getParadas();

  let paradasData = paradas.map((parada) => ({
    descripcion: parada.descripcion,
    descripcion_localidad: parada.descripcion_localidad,
    latitud: parada.latitud,
    longitud: parada.longitud,
    direccion: parada.direccion,
    id: parada.idparada,
  }));

  origen = new autoComplete({
    selector: "#origen",
    placeHolder: "",
    data: {
      src: paradasData,
      keys: ["descripcion"],
      cache: true,
    },
    resultsList: {
      element: (list, data) => {
        if (!data.results.length) {
          const message = document.createElement("div");
          message.setAttribute("class", "no-results");
          message.innerHTML = `<span>No se encontraron resultados</span>`;
          list.appendChild(message);
        }
      },
      noResults: true,
      maxResults: 15,
      tabSelect: true,
    },
    resultItem: {
      highlight: true, // Resalta el elemento seleccionado
      element: (item, data) => {
        // Aquí puedes manipular el HTML de cada resultado

        item.innerHTML = `
                    <div class="result-item">
                        <span>${data.value.descripcion_localidad}</span><br>
                        <span class="terminal">${data.match}</span>
                    </div>
                `;
      },
    },
    events: {
      input: {
        selection: (event) => {
          const selection = event.detail.selection;
          origen.input.value = selection.value.descripcion;
          origen.input.dataset.id = selection.value.id;
          const lat = selection.value.latitud
          const long = selection.value.longitud
          const localidad = selection.value.descripcion_localidad;
          const descripcion = selection.value.descripcion;
          const direccion = selection.value.direccion;

          origen.input.dataset.lat = lat
          origen.input.dataset.long = long
          origen.input.dataset.localidad = localidad
          origen.input.dataset.descripcion = descripcion
          origen.input.dataset.direccion = direccion
          changeSVGLocation('origen', lat, long, localidad, descripcion, direccion );
  
        },
      },
    },
  });

  salida = new autoComplete({
    selector: "#salida",
    placeHolder: "",
    data: {
      src: paradasData,
      keys: ["descripcion"],
      cache: true,
    },
    resultsList: {
      element: (list, data) => {
        if (!data.results.length) {
          const message = document.createElement("div");
          message.setAttribute("class", "no-results");
          message.innerHTML = `<span>No se encontraron resultados</span>`;
          list.appendChild(message);
        }
      },
      noResults: true,
      maxResults: 15,
      tabSelect: true,
    },
    resultItem: {
      highlight: true, // Resalta el elemento seleccionado
      element: (item, data) => {
        // Aquí puedes manipular el HTML de cada resultado

        item.innerHTML = `
                    <div class="result-item">
                 
                        <span>${data.value.descripcion_localidad}</span><br>
                        <span class="terminal">${data.match}</span>
                    </div>
                `;
      },
    },
    events: {
      input: {
        selection: (event) => {
          const selection = event.detail.selection;
          salida.input.value = selection.value.descripcion;
          salida.input.dataset.id = selection.value.id;
          let lat = selection.value.latitud
          let long = selection.value.longitud
          const localidad = selection.value.descripcion_localidad;
          const descripcion = selection.value.descripcion;
          const direccion = selection.value.direccion;
          salida.input.dataset.lat = lat
          salida.input.dataset.long = long
          salida.input.dataset.localidad = localidad
          salida.input.dataset.descripcion = descripcion
          salida.input.dataset.direccion = direccion

          changeSVGLocation('destino', lat, long, localidad, descripcion, direccion);
        },
      },
    },
  });

  const changeParadas = () => {
    const origenInput = document.getElementById("origen");
    const salidaInput = document.getElementById("salida");
    const button = document.getElementById("cambiarInputs");
    button.addEventListener("click", () => {
      // Guarda los valores y data-ids actuales
      let valueOrigen = origenInput.value;
      let valueSalida = salidaInput.value;
      let dataIdOrigen = origenInput.dataset.id;
      let dataIdSalida = salidaInput.dataset.id;
      let dataOrigenLat = origenInput.dataset.lat
      let dataOrigenLong = origenInput.dataset.long

      let dataOrigenLocalidad = origenInput.dataset.localidad
      let dataOrigenDescripcion = origenInput.dataset.descripcion
      let dataOrigenDireccion = origenInput.dataset.direccion

      let dataDestinoLat = salidaInput.dataset.lat
      let dataDestinoLong = salidaInput.dataset.long
      let dataDestinoLocalidad = salidaInput.dataset.localidad
      let dataDestinoDescripcion = salidaInput.dataset.descripcion
      let dataDestinoDireccion = salidaInput.dataset.direccion
      
      // Intercambia los valores y data-ids
      origenInput.value = valueSalida;
      salidaInput.value = valueOrigen;
      origenInput.dataset.lat = dataDestinoLat
      origenInput.dataset.long = dataDestinoLong

      origenInput.dataset.localidad = dataDestinoLocalidad
      origenInput.dataset.descripcion = dataDestinoDescripcion
      origenInput.dataset.direccion = dataDestinoDireccion


      origenInput.dataset.id = dataIdSalida;
      salidaInput.dataset.id = dataIdOrigen;
      salidaInput.dataset.lat = dataOrigenLat
      salidaInput.dataset.long = dataOrigenLong

      salidaInput.dataset.localidad = dataOrigenLocalidad
      salidaInput.dataset.descripcion = dataOrigenDescripcion
      salidaInput.dataset.direccion = dataOrigenDireccion

      // Fuerza la actualización del autocompletado
      if (origen) {
        origen.input.value = valueSalida; // Actualiza el valor del input
        // origen.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
      }
      if (salida) {
        salida.input.value = valueOrigen; // Actualiza el valor del input
        // salida.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
      }
    });
  };

  changeParadas();
};

// Funcion para activar cupon
const activeCupon = () => {
  const input = document.getElementById("checkCupon");
  const buscador = document.querySelector(".buscador ");
  if (input) {
    input.addEventListener("click", () => {
      if (input.checked) {
        buscador.classList.add("cupon-active");
      } else {
        buscador.classList.remove("cupon-active");
      }
    });
  }
};

// Funcion para setear un defaultValueParadas
const defaultValueParadas = async () => {
  let paradas = await getParadas();
  const origen = document.getElementById("origen");
  const salida = document.getElementById("salida");
  const origenID = parseInt(origen.getAttribute("data-id"));
  const salidaID = parseInt(salida.getAttribute("data-id"));

  if (origenID) {
    const paradaOrigen = paradas.find((parada) => parada.idparada === origenID);
    if (paradaOrigen) {
      origen.value = paradaOrigen.descripcion;
    }
  }

  if (salidaID) {
    const paradaSalida = paradas.find((parada) => parada.idparada === salidaID);
    if (paradaSalida) {
      salida.value = paradaSalida.descripcion;
    }
  }
};

// Mover Label de Ciudad de origen y destino
const labelMove = () => {
  const origenInput = document.getElementById("origen");
  const salidaInput = document.getElementById("salida");

  const origenLabel = document.getElementById("origenLabel");
  const salidaLabel = document.getElementById("salidaLabel");

  function checkInputFocus(input, label) {
    // Al recibir el foco, eleva el label del input actual
    label.classList.add("label-up");
  }

  function checkInputBlur(input, label) {
    // Si al perder el foco no hay valor, baja el label
    if (input.value === "") {
      label.classList.remove("label-up");
    }
  }

  // Comprobar el estado inicial de los inputs para ver si ya tienen texto
  function checkInitialInput(input, label) {
    if (input.value !== "") {
      label.classList.add("label-up");
    }
  }

  // Event listeners para el input de origen
  origenInput.addEventListener("focus", () =>
    checkInputFocus(origenInput, origenLabel)
  );
  origenInput.addEventListener("blur", () =>
    checkInputBlur(origenInput, origenLabel)
  );
  origenInput.addEventListener("input", () =>
    checkInputFocus(origenInput, origenLabel)
  );

  // Event listeners para el input de salida
  salidaInput.addEventListener("focus", () =>
    checkInputFocus(salidaInput, salidaLabel)
  );
  salidaInput.addEventListener("blur", () =>
    checkInputBlur(salidaInput, salidaLabel)
  );
  salidaInput.addEventListener("input", () =>
    checkInputFocus(salidaInput, salidaLabel)
  );

  // Comprobación inicial solo si los inputs tienen valor
  checkInitialInput(origenInput, origenLabel);
  checkInitialInput(salidaInput, salidaLabel);
};

// X para borrar la seleccion de Ciudad de origen y destino
const removeSelectionParadas = () => {
  const origen = document.getElementById("origen");
  const salida = document.getElementById("salida");
  if (origen) {
    origen.addEventListener("input", () => {
      let parent = origen.parentElement.parentElement;
      let icon = parent.querySelector(".input-icon.delete-origen");
      let label = parent.querySelector("label");
      if (parent) {
        if (origen.value.length > 0) {
          icon.classList.add("show");
          icon.addEventListener("click", (e) => {
            origen.value = "";
            icon.classList.remove("show");
            label.classList.remove("label-up");
            changeSVGLocation('reset-origen');
          });
        } else {
          icon.classList.remove("show");
          changeSVGLocation('reset-origen');
        }
      }
    });
  }
  if (salida) {
    salida.addEventListener("input", () => {
      let parent = salida.parentElement.parentElement;
      let icon = parent.querySelector(".input-icon.delete-salida");
      let label = parent.querySelector("label");
      if (parent) {
        if (salida.value.length > 0) {
          icon.classList.add("show");
          icon.addEventListener("click", (e) => {
            salida.value = "";
            icon.classList.remove("show");
            label.classList.remove("label-up");
            changeSVGLocation('reset-destino');
          });
        } else {
          icon.classList.remove("show");
          changeSVGLocation('reset-destino');
        }
      }
    });
  }
};

// Contador de pasajeros
const countPasajeros = () => {
  const btnPlus = document.querySelector(".btn-plus");
  const btnMinus = document.querySelector(".btn-minus");
  const input = document.getElementById("pasajeros");

  if (btnPlus) {
    // Incrementar el valor
    btnPlus.addEventListener("click", () => {
      input.value = parseInt(input.value) + 1;
    });
  }
  if (btnMinus) {
    // Decrementar el valor
    btnMinus.addEventListener("click", () => {
      const currentValue = parseInt(input.value);
      if (currentValue > 1) {
        input.value = currentValue - 1;
      }
    });
  }
};

// Función para buscar el bus
const searchBus = () => {
  let submit = document.getElementById("searchSubmit");
  let cupon = document.querySelector(".btn-descuento");
  let cuponVerify = document.getElementById("cupon-verify");
  let CUPON_VALIDO = false;

  // Evento para validar el cupón
  if (cupon) {
    cupon.addEventListener("click", (e) => {
      e.preventDefault();
      let cuponInput = document.getElementById("cupon");

      if (cuponVerify.value.trim() !== "") {
        if (cuponVerify.value.trim() === cuponInput.value.trim()) {
          cuponInput.parentElement.classList.add("input-success");
          cuponInput.parentElement.classList.remove("input-error");
          CUPON_VALIDO = true;
        } else {
          cuponInput.parentElement.classList.add("input-error");
          cuponInput.parentElement.classList.remove("input-success");
          cuponInput.placeholder = "No válido";
          CUPON_VALIDO = false;
        }
      } else {
        cuponInput.parentElement.classList.add("input-error");
        cuponInput.parentElement.classList.remove("input-success");
        cuponInput.placeholder = "No válido";
        CUPON_VALIDO = false;
      }
    });
  }

  // Evento para el botón de buscar
  submit.addEventListener("click", (e) => {
    e.preventDefault();

    // Obtener los campos del formulario
    const origen = document.getElementById("origen");
    const salida = document.getElementById("salida");
    const fechaIngreso = document.getElementById("fechaIngreso");
    const fechaSalida = document.getElementById("fechaSalida");
    const cupon = document.getElementById("cupon");

    let valid = true;

    // Validación de campos vacíos
    if (validator.isEmpty(origen.value)) {
      origen.parentElement.parentElement.classList.add("input-error");
      valid = false;
    } else {
      origen.parentElement.parentElement.classList.remove("input-error");
    }

    if (validator.isEmpty(salida.value)) {
      salida.parentElement.parentElement.classList.add("input-error");
      valid = false;
    } else {
      salida.parentElement.parentElement.classList.remove("input-error");
    }

    if (validator.isEmpty(fechaIngreso.value)) {
      fechaIngreso.parentElement.classList.add("input-error");
      valid = false;
    } else {
      fechaIngreso.parentElement.classList.remove("input-error");
    }

    // Si algún campo es inválido, detener la ejecución
    if (!valid) {
      return;
    }

    // Construcción de la URL base
    let BASE_URL = "https://check.busplus.com.ar";
    let url;

    // Determinar la ruta base según la presencia de fechaSalida
    if (fechaSalida.value.trim() !== "") {
      url =
        BASE_URL +
        `/viajes/${origen.dataset.id}/${transformarFecha(
          fechaIngreso.value
        )}/1`;
    } else {
      url =
        BASE_URL +
        `/viaje/${origen.dataset.id}/${transformarFecha(fechaIngreso.value)}/1`;
    }

    // Agregar salida si está presente
    if (salida.value.trim() !== "") {
      url += `/${salida.dataset.id}`;
    }

    // Agregar fechaSalida si está presente y ya se usa `/viajes`
    if (fechaSalida.value.trim() !== "" && url.includes("/viajes")) {
      url += `/${transformarFecha(fechaSalida.value)}`;
    }

    // Agregar cupón si es válido
    if (CUPON_VALIDO) {
      url += `/${cupon.value}`;
    } else {
      url += `/0`;
    }

    // Abrir la URL generada en una nueva ventana
    window.open(url);
  });
};

const codigoDescuento = () => {
  const closeModal = document.querySelector('.close-modal')
  const codigo = document.getElementById("codigo-descuento");
  const BUSCADOR_CONTAINER = document.querySelector('.buscador');
  const modal = document.querySelector('.modal-codigo-descuento');
  const inputModal  = document.getElementById('cd-input');
  const applyBtn = document.getElementById('applyCode')
  const cuponVerify  = document.getElementById('cupon-verify');
  const error = document.querySelector('.error-msg')
  const inputCD = document.getElementById('cd-mobile-input');
  const icon = document.querySelector('.delete-cupon');

  codigo.addEventListener('click', e => {
    modal.classList.add('show')
    inputModal.addEventListener('input', () => {
      if(inputModal.value.length > 0 && inputModal.value.toUpperCase() === cuponVerify.value.toUpperCase() ){
        inputModal.classList.add('cupon-valido');
      }else{
        inputModal.classList.remove('cupon-valido');
      }
    })
  })
  applyBtn.addEventListener('click', () => {
    if(inputModal.value.toUpperCase() === cuponVerify.value.toUpperCase() ){
      inputModal.classList.add('cupon-valido');
      error.classList.remove('show')
      inputModal.classList.remove('cupon-no-valido');
      BUSCADOR_CONTAINER.classList.add('cd-active');
      modal.classList.remove('show')
      inputCD.value = cuponVerify.value
      inputCD.classList.add('cupon-valido');
      icon.classList.add('show');
    }else{
      inputModal.classList.add('cupon-no-valido');
      inputModal.classList.remove('cupon-valido');
      error.classList.add('show')
    }
  })

  closeModal.addEventListener('click', e => {
    modal.classList.remove('show')
  })
  
};

const inputCodigoDescuento = () => {
  const input = document.getElementById('cd-mobile-input');
  const cuponVerify  = document.getElementById('cupon-verify');
  const icon = document.querySelector('.delete-cupon');
  input.addEventListener('input', () => {
      if(input.value.length > 0){
        icon.classList.add('show');
      }else{
        icon.classList.remove('show');
      }
      if(input.value.length > 0 && input.value.toUpperCase() === cuponVerify.value.toUpperCase() ){
          input.classList.add('cupon-valido');
      }else{
        input.classList.remove('cupon-valido');
      }

  })
  icon.addEventListener('click', () => {
    input.value = ""
    input.classList.remove('cupon-valido');
    icon.classList.remove('show')
  })
}

const changeSVGLocation = (input, lat, long, localidad, descripcion, direcccion ) => {
  const ICON_LOCATION = `
    <svg class="svg-location" width="19" height="25" viewBox="0 0 19 25" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M17.3276 9.56428C17.3037 11.0091 16.6185 12.4213 15.948 13.6757C15.0988 15.2674 14.0857 16.7738 13.0229 18.2332C11.8601 19.8298 10.6281 21.3887 9.32022 22.875C6.73334 19.9625 4.27552 16.7724 2.51064 13.3259C1.26361 10.8913 0.863896 8.45847 2.21408 5.92959C3.80924 2.93845 7.24825 1.22353 10.6486 1.78913C14.5002 2.43087 17.2724 5.75738 17.3276 9.56428ZM17.3276 9.56428V9.5661C17.3276 9.56581 17.3276 9.56553 17.3277 9.56524C17.3276 9.56492 17.3276 9.5646 17.3276 9.56428ZM18.653 9.51286C18.5801 5.89814 16.4274 2.58535 13.0303 1.13833C9.66126 -0.295602 5.67518 0.380577 2.97851 2.82606C0.624437 4.95974 -0.624435 8.41677 0.31314 11.4877C1.18256 14.3374 3.0135 16.9297 4.77629 19.3063C6.05648 21.0339 7.40666 22.7253 8.85078 24.3242C9.07919 24.5762 9.55995 24.5762 9.78836 24.3242C11.3927 22.5476 12.8884 20.6623 14.2883 18.7262C15.3825 17.2125 16.4251 15.6463 17.2761 13.9857C17.9679 12.6329 18.613 11.156 18.6527 9.61994L18.653 9.51286ZM18.653 9.51286C18.6539 9.52994 18.6542 9.54741 18.6539 9.56526L18.653 9.51286Z" fill="#6D6D6D"/>
    <path d="M17.3276 9.56428C17.3037 11.0091 16.6185 12.4213 15.948 13.6757C15.0988 15.2674 14.0857 16.7738 13.0229 18.2332C11.8601 19.8298 10.6281 21.3887 9.32022 22.875C6.73334 19.9625 4.27552 16.7724 2.51064 13.3259C1.26361 10.8913 0.863896 8.45847 2.21408 5.92959C3.80924 2.93845 7.24825 1.22353 10.6486 1.78913C14.5002 2.43087 17.2724 5.75738 17.3276 9.56428ZM17.3276 9.56428V9.5661C17.3276 9.56581 17.3276 9.56553 17.3277 9.56524C17.3276 9.56492 17.3276 9.5646 17.3276 9.56428ZM18.653 9.51286C18.5801 5.89814 16.4274 2.58535 13.0303 1.13833C9.66126 -0.295602 5.67518 0.380577 2.97851 2.82606C0.624437 4.95974 -0.624435 8.41677 0.31314 11.4877C1.18256 14.3374 3.0135 16.9297 4.77629 19.3063C6.05648 21.0339 7.40666 22.7253 8.85078 24.3242C9.07919 24.5762 9.55995 24.5762 9.78836 24.3242C11.3927 22.5476 12.8884 20.6623 14.2883 18.7262C15.3825 17.2125 16.4251 15.6463 17.2761 13.9857C17.9679 12.6329 18.613 11.156 18.6527 9.61994L18.653 9.51286ZM18.653 9.51286C18.6539 9.52994 18.6542 9.54741 18.6539 9.56526L18.653 9.51286Z" stroke="#6D6D6D"/>
    <path d="M12.0968 9.56399C12.0415 11.0541 10.8774 12.2977 9.32087 12.2977C7.76439 12.2977 6.54499 11.0559 6.54499 9.56399C6.54499 8.07204 7.80675 6.83208 9.32087 6.83208C10.835 6.83208 12.0415 8.07386 12.0968 9.56399ZM13.4161 9.43188C13.272 7.29696 11.5662 5.52686 9.32087 5.52686C7.02943 5.52686 5.21875 7.36867 5.21875 9.56399C5.21875 11.7593 7.09022 13.6102 9.32087 13.6029C11.5681 13.5958 13.2723 11.8376 13.4161 9.69619C13.4223 9.65473 13.4247 9.61066 13.423 9.56399C13.4247 9.51736 13.4223 9.47332 13.4161 9.43188Z" fill="#6D6D6D"/>
    <path d="M12.0968 9.56399C12.0415 11.0541 10.8774 12.2977 9.32087 12.2977C7.76439 12.2977 6.54499 11.0559 6.54499 9.56399C6.54499 8.07204 7.80675 6.83208 9.32087 6.83208C10.835 6.83208 12.0415 8.07386 12.0968 9.56399ZM13.4161 9.43188C13.272 7.29696 11.5662 5.52686 9.32087 5.52686C7.02943 5.52686 5.21875 7.36867 5.21875 9.56399C5.21875 11.7593 7.09022 13.6102 9.32087 13.6029C11.5681 13.5958 13.2723 11.8376 13.4161 9.69619C13.4223 9.65473 13.4247 9.61066 13.423 9.56399C13.4247 9.51736 13.4223 9.47332 13.4161 9.43188Z" stroke="#6D6D6D"/>
    </svg>
  `;
  const ICON_SEARCH = `
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path
    d="M21 21L16.7 16.7M19 11C19 15.4183 15.4183 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11Z"
    stroke="#6D6D6D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
  </svg>
  `;
  const modalTerminal = document.querySelector('.modal-terminal');
  if(input === 'origen'){
    const containerOrigen = document.querySelector(`.input-icon.search.origen`);
    containerOrigen.innerHTML = ICON_LOCATION;
    containerOrigen.style.cursor = "pointer"; // Cambia el cursor
    containerOrigen.style.pointerEvents = "auto"; // Desactiva los clics
    containerOrigen.addEventListener("click", () => {
      let input = containerOrigen.nextElementSibling.firstElementChild
      modalTerminal.classList.add('show');
      showModalLocation(input.dataset.lat, input.dataset.long, input.dataset.localidad, input.dataset.descripcion, input.dataset.direccion );
    });

  }
  if(input === 'destino'){
    const containerDestino = document.querySelector(`.input-icon.search.destino`);
    containerDestino.innerHTML = ICON_LOCATION;
    containerDestino.style.cursor = "pointer"; // Cambia el cursor
    containerDestino.style.pointerEvents = "auto"; // Desactiva los clics
    

    containerDestino.addEventListener("click", () => {

      let input = containerDestino.nextElementSibling.firstElementChild
      modalTerminal.classList.add('show');
      showModalLocation(input.dataset.lat, input.dataset.long, input.dataset.localidad, input.dataset.descripcion, input.dataset.direccion );

    });
  }
  if(input === 'origen' || input === 'destino'){
    modalTerminal.addEventListener('click', (event) => {
      // Verifica si el clic fue en el modal (fondo) y no en el contenido interno
      if (event.target === modalTerminal) {
        modalTerminal.classList.remove('show');
      }
    });
  }

  if(input === 'reset-origen'){
    const containerOrigen = document.querySelector(`.input-icon.search.origen`);
    containerOrigen.innerHTML = ICON_SEARCH;
    containerOrigen.style.pointerEvents = "none"; // Desactiva los clics
    containerOrigen.style.cursor = "not-allowed"; // Cambia el cursor
  }
  if(input === 'reset-destino'){
    const containerDestino = document.querySelector(`.input-icon.search.destino`);
    containerDestino.innerHTML = ICON_SEARCH;
    containerDestino.style.pointerEvents = "none"; // Desactiva los clics
    containerDestino.style.cursor = "not-allowed"; // Cambia el cursor
  }
};

const showModalLocation = (lat, long, localidad, descripcion, direcccion ) => {
  const containerMap = document.getElementById('mapaTerminal');
  const modalClose = document.querySelector('.close-modal-terminal');
  const BASE_THEME = ajax_var.theme;
  const modalTerminal = document.querySelector('.modal-terminal');

  // Verifica que las coordenadas existan antes de crear el mapa
  if (lat && long) {
  
    // Inicializa el mapa
    let map = L.map(containerMap, { scrollWheelZoom: false }).setView([lat, long], 18);
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 14 }).addTo(map);
    
    
    const icon_terminal_destino = L.icon({
      iconUrl: `${BASE_THEME}/assets/img/icons/ubicacion-origen.png`,
      iconSize: [50, 50],
    });

    // Añade un marcador al mapa
    L.marker([lat, long], { icon: icon_terminal_destino })
      .addTo(map)
      .bindPopup(`
        <div class="item__terminal-buscador">
          <h2>${localidad}</h2>
          <h3>${descripcion}</h3>
          <p><strong>Dirección: ${direcccion}</strong></p>
        </div>
      `)
      .openPopup();  // Abre el popup automáticamente
    console.log(lat + long);
    // Almacena el mapa en el contenedor para poder acceder a él más tarde
    containerMap.mapInstance = map; 


  }
  // Cerrar el modal al hacer clic en el botón de cierre
  modalClose.addEventListener('click', () => {
    modalTerminal.classList.remove('show');
    // Limpia el mapa y su referencia
    if (containerMap.mapInstance) {
      containerMap.mapInstance.off(); // Desactiva eventos del mapa
      containerMap.mapInstance.remove(); // Elimina el mapa
      containerMap.mapInstance = null; // Limpia la referencia del mapa
    }
    
    // Limpia el contenedor
    containerMap.innerHTML = ''; 
  });

};