export const tinymce = () => {
        // Selecciona todos los párrafos dentro del contenido WYSIWYG
        let paragraphs = document.querySelectorAll('.single-body p');
        if(paragraphs.length > 0){
            paragraphs.forEach(function(p) {
                // Elimina los espacios en blanco y compara con '&nbsp;'
                if (p.innerHTML.trim() === '&nbsp;') {
                    // Si el párrafo solo contiene '&nbsp;', agrega una clase
                    p.classList.add('empty');
                }
       
            });
        }

}