export const destino = () => {
  const blockDestino = document.querySelector('.block-destino');
  if(blockDestino){
    const BASE_URL = ajax_var.url;
    let splideDestino;
    let splideDestinoOnly;
    let loaderContainer = document.querySelector(".loader-container");
    const destinoOnlyContainer = document.getElementById('destino-only')
    let loader = '<span class="loader"></span>';
    splideDestino = new Splide("#destino", {
        type: 'slide',
        perPage: 3,
        arrows: false,
        gap: "32px",
        pagination: false,
        grid: {
            rows: 3,
            cols: 1,
            gap: {
                row: '32px',
                col: '32px',
            },
        },
        breakpoints: {
        1024: {
            perPage: 3,
            gap: "16px",
        },
        878: {
            perPage: 2,
            grid: {
                rows: 1,
                cols: 1,
                gap: {
                    row: '16px',
                    col: '16px',
                },
            },
        },
        600: {
            perPage: 1,
            gap: "10px",
            focus: "center",
        },
        },
    });
    splideDestino.mount(window.splide.Extensions);
    // Built splide_splide
    const rutasHTML = (data) => {
        const container = document.getElementById("containerRutas");
        
        // Limpia los slides existentes en Splide antes de añadir los nuevos
        splideDestino.destroy(); // Desmonta Splide temporalmente
        container.innerHTML = ""; // Limpia el contenedor HTML
    
        // Construye los nuevos slides a partir de la data recibida
        let rutasHTML = "";
        data.forEach((ruta) => {
            const { title, text, image, permalink } = ruta;
            rutasHTML += `
                <li class="splide__slide card-destino">
                    <a href="${permalink}" class="card" aria-label="Haz clic en ${title}">
                        <div class="block-image">
                            ${image}
                        </div>
                        <div class="block-content">
                            <h3>${title}</h3>
                      
                        </div>
                    </a>
                </li>
            `;
        });
    
        // Inserta el HTML generado en el contenedor
        container.innerHTML = rutasHTML;
        
        // Reactiva Splide en el contenedor
        splideDestino.mount(window.splide.Extensions);
        
        // Elimina el loader y muestra el contenedor
        loaderContainer.firstElementChild.remove();
        container.classList.remove("hidden-div");
    };
    // Rutas Ajax
    const callAjax = async (tax) => {
        const URL = `${BASE_URL}?action=send_posts_rutas&tax=${tax}`;
        loaderContainer.innerHTML = loader;
        containerRutas.classList.add("hidden-div");
        try {
        let response = await fetch(URL, {
            method: "GET",
        });

        let resJSON = await response.json();

        if (resJSON.length > 0) {
            // Construir

            rutasHTML(resJSON);
        } else {
            loaderContainer.firstElementChild.remove();
            containerRutas.classList.remove("hidden-div");
            containerRutas.innerHTML =
            '<li class="splide__slide"><p class="error-msg">No se encontraron destinos.</p></li>';
            splideDestino.refresh();
        }
        } catch (error) {
        console.error(error);
        }
    };
    const buttonRutas = document.querySelectorAll(".button-rutas");
    if (buttonRutas.length > 0) {
        buttonRutas.forEach((button) => {
        button.addEventListener("click", (e) => {
            e.preventDefault();
            buttonRutas.forEach((btn) => {
            btn.classList.remove("active");
            });
            button.classList.add("active");
            let tax = button.dataset.slug;
            callAjax(tax);
        });
        });
    }

    // if(destinoOnlyContainer){
    // // Splide for Only Destino
    // splideDestinoOnly = new Splide(destinoOnlyContainer, {
    //     perPage: 2,
    //     arrows: false,
    //     pagination: true,
    //     gap: "32px",
    //     breakpoints: {
    //     1024: {
    //         perPage: 2,
    //         gap: "16px",
    //     },
    //     878: {
    //         perPage: 2,
    //     },
    //     600: {
    //         perPage: 1,
    //         gap: "10px",
    //         focus: "center",
    //     },
    //     },
    // });
    // splideDestinoOnly.mount();
    // }

  }
};
