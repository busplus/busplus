export const home = () => {
    let title = document.getElementById('title-page');
    
    if(title.value === 'Home'){
        
        const BASE_URL = ajax_var.url;
        let splideFinanciacion;
        let splideRutas;
        let loaderContainer = document.querySelector('.loader-container');
        let loader = '<span class="loader"></span>'
        // Carrousel Experiencia
        const experienciaBlock = document.getElementById('experiencia');
        if(experienciaBlock){
            const experiencia = new Splide( experienciaBlock,{     
                perPage: 4,
                arrows: false,
                pagination: false,
                gap: '32px',
                breakpoints: {
                    1024:{
                        perPage: 3,
                        gap: '16px',
                    },
                    878:{
                        perPage: 2,
                    },
                    600: {
                        perPage: 1,
                        gap: '10px',
                        focus  : 'center',
                    }
                }
            } );
            experiencia.mount();
        }

        const empresasBlock = document.getElementById('empresas');
        if(empresasBlock){
            const empresas = new Splide( '#empresas',{
                type       : 'loop',
                perPage    : 6,   
                autoplay   : true,   
                interval   : false,
                speed      : 120000, 
                arrows     : false,
                pagination: false,   
                gap        : '10px', 
                pauseOnHover: true, 
                breakpoints: {
                    1024: {
                        perPage: 6,
        
                    },
                    878: {
                        perPage: 5,
                    },
                    600: {
                        perPage: 2,
                        gap        : '5px', 
                        speed      : 100000, 
        
         
                    }
                }
            } );
            empresas.mount();
        }

        const financiacionBlock = document.getElementById('financiacion');
        if(financiacionBlock){
            splideFinanciacion = new Splide( '#financiacion',{
                type       : 'loop',
                perPage    : 4,   
                autoplay   : true,   
                autoplayInterval: 0,
                interval   : false,
                speed      : 120000, 
                arrows     : false,
                pagination: false,   
                gap        : '10px', 
                pauseOnHover: true, 
                breakpoints: {
                    1024: {
                        perPage: 6,
        
                    },
                    878: {
                        perPage: 5,
                    },
                    600: {
                        perPage: 2,
                        gap        : '10px', 
                        speed      : 100000, 
                    }
                }
            } );
            splideFinanciacion.mount();
        }

    
    
      
    }
}