
<?php get_header(); ?>
    <div class="single single-default">
        <div class="container">
            <div class="single-body">
                <?php the_content(); ?>
            </div>
            <div class="button__container w-100 d-flex justify-center align-items-center btn-archive">
                <?php
                $url = get_field( 'promociones', 'options' );
                if ( $url ) : ?>
                    <a href="<?php echo esc_url( $url ); ?>" class="btn only-text orange icon" aria-label="Ver todas las ofertas">
                        <i class="fa-solid fa-chevron-left mr-1"></i>
                        ver todas las ofertas
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php show_block_buscador(true); ?>
<?php get_footer(); ?>