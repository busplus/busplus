<?php get_header(); ?>

<?php 

$term_id = get_queried_object_id();

$titulo = get_field( 'titulo', 'provincia_' . $term_id );

$titulo_negrita = get_field( 'titulo_en_negrita', 'provincia_' . $term_id );

$tax_provincia_imagen = get_field( 'tax_provincia_imagen', 'provincia_' . $term_id )['url'];

$tax_provincia_texto = get_field( 'tax_provincia_texto' , 'provincia_' . $term_id );



?>

<main class="single single-destino">
    <section class="hero-simple" style="background-image:url(<?php echo $tax_provincia_imagen;?>);">
        <div class="container">
            <div class="hero-content">
                <h1 class="f-28-20 f-extrabold">
                    <?php echo $titulo;?>
                    <?php if($titulo_negrita): ?>
                        <br><strong><?php echo $titulo_negrita;?></strong>

                    <?php endif; ?>

                </h1>

            </div>

        </div>

    </section>

    <?php show_block_buscador(); ?>

    <section class="block-text">

        <div class="container-sm">

            <div class="single-body">

                <?php echo $tax_provincia_texto; ?>

            </div>

        </div>

    </section>

    <?php echo render_block_destino('Nuestros destinos a la costa más populares', 'bg-gray-secondary'); ?>

</main>



<?php get_footer(); ?>