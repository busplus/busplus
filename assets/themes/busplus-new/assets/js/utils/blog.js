export const blog = () => {
    const blogPage = document.querySelector('.blog');
    if (blogPage) {
        const btnCategorys = document.querySelectorAll('.btn-category');
        const loaderContainer = document.querySelector('.list-posts');
        const container = document.querySelector('.container')
        const BASE_URL = ajax_var.url;
        let currentCategory = "";
        let currentPage = 1;
        let totalPages = 1;

        const loader = '<div class="loader-container col-12"><span class="loader"></span></div>';

        if (btnCategorys.length > 0) {
       
            btnCategorys.forEach((button) => {
                button.addEventListener('click', e => {
                    e.preventDefault();
                    btnCategorys.forEach((btn) => {
                        btn.classList.remove('active');
                    });
                    button.classList.add('active');
                    currentCategory = button.dataset.slug;
                    currentPage = 1; // Reset to page 1 on category change
                    callAjax(currentCategory, currentPage);
                   
                });
            });
        }

        const callAjax = async (tax, paged) => {
            const URL = `${BASE_URL}?action=send_posts_blog&tax=${tax}&paged=${paged}`;
            loaderContainer.innerHTML = loader;

            try {
                let response = await fetch(URL, {
                    method: "GET",
                });

                const dataJSON = await response.json();

                if (dataJSON.success) {
                    loaderContainer.innerHTML = dataJSON.data.html;
                    totalPages = dataJSON.data.total_pages;
                    updatePagination(currentPage, totalPages);
                } else {
                    loaderContainer.innerHTML = dataJSON.data;
                }
            } catch (error) {
                console.error('Error:', error);
            }
        }

        function assignPaginationEvents() {
            const paginationButtons = document.querySelectorAll('.btn-page');
            
            paginationButtons.forEach(button => {
                button.addEventListener('click', function (event) {
                    event.preventDefault();
                    
                    const page = parseInt(this.dataset.page, 10);
        
                    if (!isNaN(page)) {
                        currentPage = page;
                        callAjax(currentCategory, currentPage);
                        window.scrollTo({
                            top: container.offsetTop,
                            behavior: 'smooth'
                        });
                    }
                });
            });
        }

        function updatePagination(currentPage, totalPages) {
            let html = '';
            
            // Always show the first page
            html += `<button type="button" data-page="1" class="btn-page ${currentPage === 1 ? 'active' : ''}">1</button>`;
            
            if (currentPage > 3) {
                html += '<span>...</span>';
            }
            
            const startPage = Math.max(2, currentPage - 1);
            const endPage = Math.min(totalPages - 1, currentPage + 1);
            
            for (let i = startPage; i <= endPage; i++) {
                html += `<button type="button" data-page="${i}" class="btn-page ${currentPage === i ? 'active' : ''}">${i}</button>`;
            }
            
            if (currentPage < totalPages - 2) {
                html += '<span>...</span>';
            }
            
            if (totalPages > 1) {
                html += `<button type="button" data-page="${totalPages}" class="btn-page ${currentPage === totalPages ? 'active' : ''}">${totalPages}</button>`;
            }
            
            // Next button
            if (currentPage < totalPages) {
                html += `<button type="button" class="btn-page next" data-page="${currentPage + 1}">
                            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L7 7L1 13" stroke="#606060"></path>
                            </svg>
                        </button>`;
            }

            // Previous button
            if (currentPage > 1) {
                html = `<button type="button" class="btn-page prev" data-page="${currentPage - 1}">
                            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7 1L1 7L7 13" stroke="#606060"></path>
                            </svg>
                        </button>` + html;
            }

            // Update the pagination container
            const paginationContainer = document.getElementById('pagination');
            paginationContainer.innerHTML = html;

            // Reassign events to new pagination buttons
            assignPaginationEvents();
        }

        // Initialize by fetching posts for the first page
      
        assignPaginationEvents();
        // callAjax(currentCategory, currentPage);
    }
}


