export const buscador = () => {
    const buscadorContainer = document.querySelector('.buscador')
    let autoCompleteOrigen;
    let autoCompleteSalida;
    
    if(buscadorContainer){
        // setupCalendar('fechaIngreso', 'fechaSalida');
        getParadas();
        defaultValueParadas(autoCompleteOrigen, autoCompleteSalida);
        obtenerParadas(autoCompleteOrigen, autoCompleteSalida);
        inputDelete();
        searchBus();
        activeCupon();
        clickLocation();
    }
}

// LITEPICKER 

const transformarFecha = (fecha) => {
    // Dividir la fecha en día, mes y año
    const [dia, mes, año] = fecha.split('/');

    // Reorganizar y devolver la fecha en formato yyyy-mm-dd
    return `${año}-${mes}-${dia}`;
}


//PARADAS
const getParadas = async () => {
    let external_paradas = [];
    let url_api = '';
    let session_api = '';

    try {
        const password = {
            clave_acceso: "qu3r1c4qu3s0s",
        };
        const env = sessionStorage.getItem('env');
        url_api = env === 'test' ? "https://ws.viatesting.com.ar/" : "https://ws.busplus.com.ar/";

        // Primer llamado API para obtener la clave de sesión
        const sessionResponse = await fetch(url_api + "sesion", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(password),
        });

        if (!sessionResponse.ok) {
            throw new Error('Error obteniendo la clave de sesión');
        }

        const key = await sessionResponse.json();
        session_api = key.key;

        // Segundo llamado API para obtener las paradas
        const paradasResponse = await fetch(url_api + "paradas", {
            method: "GET",
            headers: {
                "X-API-KEY": session_api,
                "Content-Type": "application/json",
            },
        });

        if (!paradasResponse.ok) {
            throw new Error('Error obteniendo las paradas');
        }

        const data = await paradasResponse.json();
        external_paradas = data;

    } catch (error) {
        console.error('Error en getParadas:', error);
    }

    return external_paradas;
}

const checkCodigoCupon = async (codigo) => {
    let status_cupon = false; // Almacena únicamente el status
    let url_api = '';
    let session_api = '';

    try {
        const password = {
            clave_acceso: "qu3r1c4qu3s0s",
        };

        const env = sessionStorage.getItem('env');
        url_api = env === 'test' 
            ? "https://ws.viatesting.com.ar/" 
            : "https://ws.busplus.com.ar/";

        // Primer llamado: Obtener la clave de sesión
        const sessionResponse = await fetch(`${url_api}sesion`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(password),
        });

        if (!sessionResponse.ok) {
            throw new Error(`Error obteniendo la clave de sesión: ${sessionResponse.status}`);
        }

        const sessionData = await sessionResponse.json();
        session_api = sessionData.key; // Guardar clave de sesión

        // Segundo llamado: Validar el código del cupón
        const cuponResponse = await fetch(`${url_api}pases/codigo_validar?CodigoDescuento=${codigo}&Fecha=2024-12-10&Pasajeros=1`, {
            method: "GET",
            headers: {
                "X-API-KEY": session_api,
                "Content-Type": "application/json",
            },
        });

        if (!cuponResponse.ok) {
            throw new Error(`Error obteniendo el cupón: ${cuponResponse.status}`);
        }

        const cuponData = await cuponResponse.json();
        status_cupon = cuponData.status; // Almacenar solo el valor de "status"

    } catch (error) {
        console.error('Error en checkCodigoCupon:', error.message || error);
    }

    return status_cupon; // Retorna únicamente el valor del "status"
};



const obtenerParadas = async (origen, salida) => {
  
    let paradas = await getParadas();
    let paradasData = paradas.map(parada => ({
        descripcion: parada.descripcion,
        latitud: parada.latitud,
        longitud: parada.longitud,
        direccion: parada.direccion,
        id: parada.idparada
    }));
    origen = new autoComplete({
        selector: "#origen",
        placeHolder: "Ciudad de origen...",
        data: {
            src: paradasData,
            keys: ["descripcion"],
            cache: true
        },
        resultsList: {
            element: (list, data) => {
          
                if (!data.results.length) {
                    const message = document.createElement("div");
                    message.setAttribute("class", "no-results");
                    message.innerHTML = `<span>No se encontraron resultados</span>`;
                    list.appendChild(message);
                }
            },
            noResults: true,
            maxResults: 15,
            tabSelect: true
        },
        events: {
            input: {
                selection: (event) => {
                    const selection = event.detail.selection;
                    origen.input.value = selection.value.descripcion;
                    origen.input.dataset.id = selection.value.id;
                    origen.input.dataset.location = `${selection.value.latitud},${selection.value.longitud}`
                    origen.input.dataset.info = `${selection.value.descripcion},${selection.value.direccion}`
                }
            }
        }
    });
    salida = new autoComplete({
        selector: "#salida",
        placeHolder: "Ciudad de salida...",
        data: {
            src: paradasData,
            keys: ["descripcion"],
            cache: true
        },
        resultsList: {
            element: (list, data) => {
                if (!data.results.length) {
                    const message = document.createElement("div");
                    message.setAttribute("class", "no-results");
                    message.innerHTML = `<span>No se encontraron resultados</span>`;
                    list.appendChild(message);
                } 
            },
            noResults: true,
            maxResults: 15,
            tabSelect: true
        },
        events: {
            input: {
                selection: (event) => {
                    const selection = event.detail.selection;
                    salida.input.value = selection.value.descripcion;
                    salida.input.dataset.id = selection.value.id;
                    salida.input.dataset.location = `${selection.value.latitud},${selection.value.longitud}`
                    salida.input.dataset.info = `${selection.value.descripcion},${selection.value.direccion}`
                }
            }

        }
    });


    
    const changeParadas = () => {
        const origenInput = document.getElementById('origen');
        const salidaInput = document.getElementById('salida');
        const button = document.getElementById('cambiarInputs');
        button.addEventListener('click', () => {
            // Guarda los valores y data-ids actuales
            let valueOrigen = origenInput.value;
            let valueSalida = salidaInput.value;
            let dataIdOrigen = origenInput.dataset.id;
            let dataIdSalida = salidaInput.dataset.id;
            let locationOrigen = origenInput.dataset.location
            let locationSalida = salidaInput.dataset.location
            let infoOrigen = origenInput.dataset.info
            let infoSalida = salidaInput.dataset.info

            // Intercambia los valores y data-ids
            origenInput.value = valueSalida;
            salidaInput.value = valueOrigen;

            salidaInput.dataset.location = locationOrigen
            origenInput.dataset.location = locationSalida

            origenInput.dataset.id = dataIdSalida;
            salidaInput.dataset.id = dataIdOrigen;

            salidaInput.dataset.info = infoOrigen
            origenInput.dataset.info = infoSalida

            // Fuerza la actualización del autocompletado
            if (origen) {
                origen.input.value = valueSalida;  // Actualiza el valor del input
                // origen.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
            }
            if (salida) {
                salida.input.value = valueOrigen;  // Actualiza el valor del input
                // salida.input.dispatchEvent(new Event('input'));  // Dispara el evento de entrada
            }

        });
    }

    changeParadas();
};


const activeCupon = () => {
    const input = document.getElementById('checkCupon');
    const buscador = document.querySelector('.buscador ')
 
    input.addEventListener('click', () => {
       if(input.checked){
        buscador.classList.add('cupon-active');
       }else{
        buscador.classList.remove('cupon-active')

       }
    })
}

const inputDelete = () => {
    const origen = document.getElementById('origen')
    const destino = document.getElementById('salida')
    const deleteButton = document.querySelectorAll('.input-delete')
    origen.addEventListener('input', () => {
        let button = origen.parentElement.nextElementSibling
        if(button){
            button.classList.add('show')
        }


    })
    destino.addEventListener('input', () => {

        let button = destino.parentElement.nextElementSibling
        if(button){
            button.classList.add('show')
        }


    })

    deleteButton.forEach(btn => {
        btn.addEventListener('click', e => {
            let input = btn.previousElementSibling.firstElementChild
            if(input){
                btn.classList.remove('show')
                input.value  = ""
                input.dataset.id = ""
            }

        })
    });
}

const defaultValueParadas = async () => {
    let paradas = await getParadas();
    const origen = document.getElementById('origen');
    const salida = document.getElementById('salida');
    const origenID = parseInt(origen.getAttribute('data-id'));
    const salidaID = parseInt(salida.getAttribute('data-id'));

    if (origenID) {
        const paradaOrigen = paradas.find(parada => parada.idparada === origenID);
        if (paradaOrigen) {
            origen.value = paradaOrigen.descripcion;
        }
    }

    if (salidaID) {
        const paradaSalida = paradas.find(parada => parada.idparada === salidaID);
        if (paradaSalida) {
            salida.value = paradaSalida.descripcion;
        }
    }
}



const searchBus = () => {
    let submit = document.getElementById('searchSubmit');
    let cupon = document.querySelector('.btn-descuento');
    let cuponVerify = document.getElementById('cupon-verify');
    let deleteCupon = document.querySelector('.cupon-delete');
    let cuponInput = document.getElementById('cupon');
    let labelmsg = cuponInput.previousElementSibling.firstElementChild
    const columnDescuento = document.querySelector('.column-descuento')
    let CUPON_VALIDO = false;
    cupon.addEventListener('click', (e) => {
        e.preventDefault();

        checkCodigoCupon(cuponInput.value).then(status => {
            if (status) {
                columnDescuento.classList.add('toggle-validation');
                cuponInput.parentElement.classList.add('input-success');
                cuponInput.parentElement.classList.remove('input-error');
                labelmsg.textContent = "APLICADO"
                CUPON_VALIDO = true
            } else {
                columnDescuento.classList.add('toggle-validation');
                cuponInput.parentElement.classList.add('input-error');
                cuponInput.parentElement.classList.remove('input-success');
                cuponInput.placeholder = "No válido";
                labelmsg.textContent = "INCORRECTO"
                CUPON_VALIDO = false;
            }
        });
    


    })
    deleteCupon.addEventListener('click', () => {
        columnDescuento.classList.remove('toggle-validation');
        cuponInput.value = ""
        cuponInput.parentElement.classList.remove('input-error');
        cuponInput.parentElement.classList.remove('input-success');
        labelmsg.textContent = ""
        cuponInput.placeholder = "AJ032IAJ032I"
        CUPON_VALIDO = false;
    })
    submit.addEventListener("click", (e) => {
        e.preventDefault();
        const origen = document.getElementById("origen");
        const salida = document.getElementById("salida");
        const fechaIngreso = document.getElementById("fechaIngreso");
        const fechaSalida = document.getElementById("fechaSalida");
        const cupon = document.getElementById("cupon");
        const pasajeros = document.getElementById("pasajeros");
      
        // Validación de campos
        if (validator.isEmpty(origen.value)) {
          let container = origen.parentElement.parentElement;
          container.classList.add("input-error");
        } else {
          let container = origen.parentElement.parentElement;
          container.classList.remove("input-error");
        }
      
        if (validator.isEmpty(salida.value)) {
          let container = salida.parentElement.parentElement;
          container.classList.add("input-error");
        } else {
          let container = salida.parentElement.parentElement;
          container.classList.remove("input-error");
        }
      
        if (validator.isEmpty(fechaIngreso.value)) {
          let container = fechaIngreso.parentElement;
          container.classList.add("input-error");
          // Detener la ejecución si fechaIngreso está vacío
          return;
        } else {
          let container = fechaIngreso.parentElement;
          container.classList.remove("input-error");
        }
      
        // Construcción de la URL base
        let BASE_URL = "https://check.busplus.com.ar";
        let url;
      
        if (origen.dataset.id.trim() !== "" && salida.dataset.id.trim() !== "") {
          // Determinar la ruta base según la presencia de fechaSalida
          if (fechaSalida.value.trim() !== "") {
            url =
              BASE_URL +
              `/viajes/${origen.dataset.id}/${transformarFecha(fechaIngreso.value)}`;
            if (pasajeros.value.trim() !== "") {
              url += `/${pasajeros.value}`;
            }
          } else {
            url =
              BASE_URL +
              `/viaje/${origen.dataset.id}/${transformarFecha(fechaIngreso.value)}`;
            if (pasajeros.value.trim() !== "") {
              url += `/${pasajeros.value}`;
            }
          }
      
          // Agregar salida si está presente
          if (salida.value.trim() !== "") {
            url += `/${salida.dataset.id}`;
          }
      
          // Agregar fechaSalida si está presente y ya se usa `/viajes`
          if (fechaSalida.value.trim() !== "" && url.includes("/viajes")) {
            url += `/${transformarFecha(fechaSalida.value)}`;
          }
      
          // Agregar cupón si es válido
          if (CUPON_VALIDO !== false) {
            url += `/${cupon.value}`;
          } else {
            url += `/0`;
          }
      
          window.open(url);
        }
    });

}

const clickLocation = () => {
    const locationInput = document.querySelectorAll('.input-location')
    locationInput.forEach(location => {
        location.addEventListener('click', e => {
            const input = location.parentElement.querySelector('.input-terminal')
            if(input.dataset.id){
                if(input.dataset.location){
                    openModalTerminal(input.dataset.location, input.dataset.info)
                }
            }
        })
    });
}


const openModalTerminal = (data, info) => {
    const containerMap = document.getElementById('mapaTerminal');
    const modalClose = document.querySelector('.close-modal-terminal');
    const BASE_THEME = ajax_var.theme;
    const modalTerminal = document.querySelector('.modal-terminal');
    if (data) {

        // Mostrar el modal
        modalTerminal.classList.add('show');

        // Extraer latitud y longitud del dato recibido
        const [lat, long] = data.split(',');
        const [descripcion, direccion] = info.split(',');
        // Inicializar el mapa en el contenedor con las coordenadas
        const map = L.map(containerMap, { scrollWheelZoom: false }).setView([lat, long], 14);
      
        // Agregar capa base al mapa
        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 14 }).addTo(map);
      
        // Crear un ícono personalizado para el marcador
        const icon_terminal_destino = L.icon({
          iconUrl: `${BASE_THEME}/assets/img/icons/ubicacion-origen.png`,
          iconSize: [50, 50],
        });
      
        // Añadir un marcador al mapa con un popup
        L.marker([lat, long], { icon: icon_terminal_destino })
          .addTo(map)
          .bindPopup(`
            <div class="item__terminal-buscador">
              <h2>${descripcion}</h2>
             <p><strong>Dirección: ${direccion}</strong></p>
            </div>
          `)
          .openPopup(); // Abre el popup automáticamente
      
        // Guardar una referencia al mapa en el contenedor para futuras manipulaciones
        containerMap.mapInstance = map;
      


        setTimeout(() => {
            map.invalidateSize();
        }, 200); // Espera un poco para asegurarte de que el modal ya esté visible
      
        // Manejar el cierre del modal y limpieza del mapa
        modalClose.addEventListener('click', () => {
          modalTerminal.classList.remove('show');
      
          // Limpiar el mapa y eliminar la referencia
          if (containerMap.mapInstance) {
            containerMap.mapInstance.off(); // Desactivar eventos del mapa
            containerMap.mapInstance.remove(); // Eliminar el mapa del DOM
            containerMap.mapInstance = null; // Borrar referencia al mapa
          }
      
          // Limpiar el contenedor del mapa
          containerMap.innerHTML = '';
        });
      }
}