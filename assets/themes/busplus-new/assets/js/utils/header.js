export const header = () => {
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};
    var hamburgers = document.querySelectorAll(".hamburger");
    const menuLinks = document.querySelector('.menu-links-container');
    const header = document.querySelector('header');
    const disclaimer = document.querySelector('.disclaimer');
    const nextSection = document.querySelector('.single')
    const dropBtn = document.querySelectorAll('.drop-btn')
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        nextSection.style.marginTop = `${header.clientHeight - 1}px`
  
        hamburger.addEventListener("click", function() {
          hamburgers[0].classList.add("is-active");
          if(disclaimer.clientHeight){
            menuLinks.style.top = `${disclaimer.clientHeight}px`
          }
          menuLinks.classList.toggle('act')
        }, false);
        hamburgers[0].addEventListener("click", function(){
          hamburgers[0].classList.add("is-active")
        })

      });
    }
    if(dropBtn.length > 0){
      dropBtn.forEach(drop => {
        drop.addEventListener('click', e => {
          drop.parentElement.classList.toggle('dropdown-active');
        })
      });

    }
}