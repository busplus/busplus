

import { select } from './utils/select.js';
import { header } from './utils/header.js';
import { home } from './utils/home.js';
// import { buscador } from './utils/buscador.js';
import { buscador } from './utils/buscador.js';
import { blog } from './utils/blog.js';
import { faq } from './utils/faq.js';
import { destino } from './utils/destino.js';
import { tinymce } from './utils/tinymce.js';
window.addEventListener('DOMContentLoaded', () => {
    try {
        buscador();
        header();
    
        select();

        blog();
        home();
        faq();
        destino();
        tinymce();

    } catch (error) {
        console.error(error)
    }
});