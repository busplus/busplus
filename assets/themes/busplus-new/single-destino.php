<?php get_header(); ?>
<?php 
    $titulo = get_field('titulo');
    $titulo_negrita = get_field('titulo_en_negrita');
    $contenido = get_field('contenido');
    $terminal_origen = get_field( 'origen' );
    $terminal_destino = get_field( 'destino' );
    $target = get_ubicaciones_post()->name;
    $parent_data = get_current_parent();
    $post_id = get_the_ID();
    $image_url = IMAGE . '/destinos/destinos-hero.jpg'; // Cambia la ruta según tu estructura de carpetas

    if (has_post_thumbnail($post_id)) {
        // Obtener la URL de la imagen destacada
        $image_url = get_the_post_thumbnail_url();
    } else {
        // Fallback a una imagen por defecto
        $image_url =  IMAGE . '/destinos/destinos-hero.jpg'; // Cambia la ruta según tu estructura de carpetas
    }
?>
<main class="single single-destino destino-nivel-2-3">
        <section class="hero-simple" style="background-image:url(<?php echo $image_url;?>);">
            <div class="container">
                <div class="hero-content">
                    <h1 class="f-28-20 f-extrabold">
                        <?php echo $titulo;?>
                        <?php if($titulo_negrita): ?>
                            <strong><?php echo $titulo_negrita;?></strong> en oferta
                        <?php endif; ?>
                      
                    </h1>
                </div>
            </div>
        </section>
        <?php show_block_buscador(); ?>

        <section class="destinos-relacionados">
            <div class="container">
                <?php if($parent_data[0]['ubicacion']): ?>
                    <h2 class="f-24-18 f-bold mb-xs-2 mb-1"><?php echo $parent_data[0]['ubicacion'] ?>: pasajes más buscados</h2>
                <?php endif; ?>
                <?php echo get_landing_seo_single_destino(); ?>
            </div>
        </section>

        <?php echo render_block_destino_actual(); ?>
        <?php echo render_block_destino('Nuestros destinos a la costa más populares', 'bg-gray-secondary'); ?>
</main>
<?php get_footer(); ?>