   /*=============================================

    =            Tabs            =

    =============================================*/

    const tabBtn = document.querySelectorAll('.tablinks');

    const tabInfo = document.querySelectorAll('.tab-info');

    tabBtn.forEach((btn) => {

      btn.addEventListener('click', function(e) {

        tabBtn.forEach((btn) => {

          btn.classList.remove('selected');

        });

        e.currentTarget.classList.add('selected');

        tabInfo.forEach((info) => {

          info.classList.remove('show');

        });

        dataset = document.querySelector(e.currentTarget.dataset.content);

        dataset.classList.add('show');

      });

    });

    /*=====  End of Tabs  ======*/



    /*=============================================

    =            Modal            =

    =============================================*/

    const openEls = document.querySelectorAll('[data-open]');

    const closeEls = document.querySelectorAll('[data-close]');

    const isVisible = 'is-visible';



    /* open modal */

    /* for (const el of openEls) {

      el.addEventListener('click', function() {

        const modalId = this.dataset.open;

        document.getElementById(modalId).classList.add(isVisible);

      });

    } */



    /* CLOSE MODAL CLICK BUTTON */

    for (const el of closeEls) {

      el.addEventListener('click', function() {

        this.parentElement.parentElement.parentElement.classList.remove(isVisible);

      });

    }



    /* Click fuera del modal */

    document.addEventListener('click', (e) => {

      if (e.target == document.querySelector('.modal.is-visible')) {

        document.querySelector('.widget-modal.is-visible').classList.remove(isVisible);

      }

    });



    /* boton ESC */

    document.addEventListener('keyup', (e) => {

      if (e.key == 'Escape' && document.querySelector('.modal.is-visible')) {

        document.querySelector('.widget-modal.is-visible').classList.remove(isVisible);

      }

    });





    /*=====  End of Modal  ======*/



// JQUERY

!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(C,e){"use strict";var t=[],r=Object.getPrototypeOf,s=t.slice,g=t.flat?function(e){return t.flat.call(e)}:function(e){return t.concat.apply([],e)},u=t.push,i=t.indexOf,n={},o=n.toString,v=n.hasOwnProperty,a=v.toString,l=a.call(Object),y={},m=function(e){return"function"==typeof e&&"number"!=typeof e.nodeType&&"function"!=typeof e.item},x=function(e){return null!=e&&e===e.window},E=C.document,c={type:!0,src:!0,nonce:!0,noModule:!0};function b(e,t,n){var r,i,o=(n=n||E).createElement("script");if(o.text=e,t)for(r in c)(i=t[r]||t.getAttribute&&t.getAttribute(r))&&o.setAttribute(r,i);n.head.appendChild(o).parentNode.removeChild(o)}function w(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?n[o.call(e)]||"object":typeof e}var f="3.6.0",S=function(e,t){return new S.fn.init(e,t)};function p(e){var t=!!e&&"length"in e&&e.length,n=w(e);return!m(e)&&!x(e)&&("array"===n||0===t||"number"==typeof t&&0<t&&t-1 in e)}S.fn=S.prototype={jquery:f,constructor:S,length:0,toArray:function(){return s.call(this)},get:function(e){return null==e?s.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=S.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return S.each(this,e)},map:function(n){return this.pushStack(S.map(this,function(e,t){return n.call(e,t,e)}))},slice:function(){return this.pushStack(s.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},even:function(){return this.pushStack(S.grep(this,function(e,t){return(t+1)%2}))},odd:function(){return this.pushStack(S.grep(this,function(e,t){return t%2}))},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(0<=n&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:u,sort:t.sort,splice:t.splice},S.extend=S.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||m(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(S.isPlainObject(r)||(i=Array.isArray(r)))?(n=a[t],o=i&&!Array.isArray(n)?[]:i||S.isPlainObject(n)?n:{},i=!1,a[t]=S.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},S.extend({expando:"jQuery"+(f+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==o.call(e))&&(!(t=r(e))||"function"==typeof(n=v.call(t,"constructor")&&t.constructor)&&a.call(n)===l)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e,t,n){b(e,{nonce:t&&t.nonce},n)},each:function(e,t){var n,r=0;if(p(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},makeArray:function(e,t){var n=t||[];return null!=e&&(p(Object(e))?S.merge(n,"string"==typeof e?[e]:e):u.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:i.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var r,i,o=0,a=[];if(p(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&a.push(i);return g(a)},guid:1,support:y}),"function"==typeof Symbol&&(S.fn[Symbol.iterator]=t[Symbol.iterator]),S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){n["[object "+t+"]"]=t.toLowerCase()});var d=function(n){var e,d,b,o,i,h,f,g,w,u,l,T,C,a,E,v,s,c,y,S="sizzle"+1*new Date,p=n.document,k=0,r=0,m=ue(),x=ue(),A=ue(),N=ue(),j=function(e,t){return e===t&&(l=!0),0},D={}.hasOwnProperty,t=[],q=t.pop,L=t.push,H=t.push,O=t.slice,P=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},R="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",I="(?:\\\\[\\da-fA-F]{1,6}"+M+"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",W="\\["+M+"*("+I+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+I+"))|)"+M+"*\\]",F=":("+I+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+W+")*)|.*)\\)|)",B=new RegExp(M+"+","g"),$=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),_=new RegExp("^"+M+"*,"+M+"*"),z=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp(M+"|>"),X=new RegExp(F),V=new RegExp("^"+I+"$"),G={ID:new RegExp("^#("+I+")"),CLASS:new RegExp("^\\.("+I+")"),TAG:new RegExp("^("+I+"|[*])"),ATTR:new RegExp("^"+W),PSEUDO:new RegExp("^"+F),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+R+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/HTML$/i,Q=/^(?:input|select|textarea|button)$/i,J=/^h\d$/i,K=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ee=/[+~]/,te=new RegExp("\\\\[\\da-fA-F]{1,6}"+M+"?|\\\\([^\\r\\n\\f])","g"),ne=function(e,t){var n="0x"+e.slice(1)-65536;return t||(n<0?String.fromCharCode(n+65536):String.fromCharCode(n>>10|55296,1023&n|56320))},re=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ie=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},oe=function(){T()},ae=be(function(e){return!0===e.disabled&&"fieldset"===e.nodeName.toLowerCase()},{dir:"parentNode",next:"legend"});try{H.apply(t=O.call(p.childNodes),p.childNodes),t[p.childNodes.length].nodeType}catch(e){H={apply:t.length?function(e,t){L.apply(e,O.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function se(t,e,n,r){var i,o,a,s,u,l,c,f=e&&e.ownerDocument,p=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==p&&9!==p&&11!==p)return n;if(!r&&(T(e),e=e||C,E)){if(11!==p&&(u=Z.exec(t)))if(i=u[1]){if(9===p){if(!(a=e.getElementById(i)))return n;if(a.id===i)return n.push(a),n}else if(f&&(a=f.getElementById(i))&&y(e,a)&&a.id===i)return n.push(a),n}else{if(u[2])return H.apply(n,e.getElementsByTagName(t)),n;if((i=u[3])&&d.getElementsByClassName&&e.getElementsByClassName)return H.apply(n,e.getElementsByClassName(i)),n}if(d.qsa&&!N[t+" "]&&(!v||!v.test(t))&&(1!==p||"object"!==e.nodeName.toLowerCase())){if(c=t,f=e,1===p&&(U.test(t)||z.test(t))){(f=ee.test(t)&&ye(e.parentNode)||e)===e&&d.scope||((s=e.getAttribute("id"))?s=s.replace(re,ie):e.setAttribute("id",s=S)),o=(l=h(t)).length;while(o--)l[o]=(s?"#"+s:":scope")+" "+xe(l[o]);c=l.join(",")}try{return H.apply(n,f.querySelectorAll(c)),n}catch(e){N(t,!0)}finally{s===S&&e.removeAttribute("id")}}}return g(t.replace($,"$1"),e,n,r)}function ue(){var r=[];return function e(t,n){return r.push(t+" ")>b.cacheLength&&delete e[r.shift()],e[t+" "]=n}}function le(e){return e[S]=!0,e}function ce(e){var t=C.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function fe(e,t){var n=e.split("|"),r=n.length;while(r--)b.attrHandle[n[r]]=t}function pe(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function de(t){return function(e){return"input"===e.nodeName.toLowerCase()&&e.type===t}}function he(n){return function(e){var t=e.nodeName.toLowerCase();return("input"===t||"button"===t)&&e.type===n}}function ge(t){return function(e){return"form"in e?e.parentNode&&!1===e.disabled?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&ae(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function ve(a){return le(function(o){return o=+o,le(function(e,t){var n,r=a([],e.length,o),i=r.length;while(i--)e[n=r[i]]&&(e[n]=!(t[n]=e[n]))})})}function ye(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}for(e in d=se.support={},i=se.isXML=function(e){var t=e&&e.namespaceURI,n=e&&(e.ownerDocument||e).documentElement;return!Y.test(t||n&&n.nodeName||"HTML")},T=se.setDocument=function(e){var t,n,r=e?e.ownerDocument||e:p;return r!=C&&9===r.nodeType&&r.documentElement&&(a=(C=r).documentElement,E=!i(C),p!=C&&(n=C.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",oe,!1):n.attachEvent&&n.attachEvent("onunload",oe)),d.scope=ce(function(e){return a.appendChild(e).appendChild(C.createElement("div")),"undefined"!=typeof e.querySelectorAll&&!e.querySelectorAll(":scope fieldset div").length}),d.attributes=ce(function(e){return e.className="i",!e.getAttribute("className")}),d.getElementsByTagName=ce(function(e){return e.appendChild(C.createComment("")),!e.getElementsByTagName("*").length}),d.getElementsByClassName=K.test(C.getElementsByClassName),d.getById=ce(function(e){return a.appendChild(e).id=S,!C.getElementsByName||!C.getElementsByName(S).length}),d.getById?(b.filter.ID=function(e){var t=e.replace(te,ne);return function(e){return e.getAttribute("id")===t}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n=t.getElementById(e);return n?[n]:[]}}):(b.filter.ID=function(e){var n=e.replace(te,ne);return function(e){var t="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return t&&t.value===n}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),b.find.TAG=d.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):d.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},b.find.CLASS=d.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&E)return t.getElementsByClassName(e)},s=[],v=[],(d.qsa=K.test(C.querySelectorAll))&&(ce(function(e){var t;a.appendChild(e).innerHTML="<a id='"+S+"'></a><select id='"+S+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&v.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||v.push("\\["+M+"*(?:value|"+R+")"),e.querySelectorAll("[id~="+S+"-]").length||v.push("~="),(t=C.createElement("input")).setAttribute("name",""),e.appendChild(t),e.querySelectorAll("[name='']").length||v.push("\\["+M+"*name"+M+"*="+M+"*(?:''|\"\")"),e.querySelectorAll(":checked").length||v.push(":checked"),e.querySelectorAll("a#"+S+"+*").length||v.push(".#.+[+~]"),e.querySelectorAll("\\\f"),v.push("[\\r\\n\\f]")}),ce(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=C.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&v.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&v.push(":enabled",":disabled"),a.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&v.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),v.push(",.*:")})),(d.matchesSelector=K.test(c=a.matches||a.webkitMatchesSelector||a.mozMatchesSelector||a.oMatchesSelector||a.msMatchesSelector))&&ce(function(e){d.disconnectedMatch=c.call(e,"*"),c.call(e,"[s!='']:x"),s.push("!=",F)}),v=v.length&&new RegExp(v.join("|")),s=s.length&&new RegExp(s.join("|")),t=K.test(a.compareDocumentPosition),y=t||K.test(a.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},j=t?function(e,t){if(e===t)return l=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n||(1&(n=(e.ownerDocument||e)==(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!d.sortDetached&&t.compareDocumentPosition(e)===n?e==C||e.ownerDocument==p&&y(p,e)?-1:t==C||t.ownerDocument==p&&y(p,t)?1:u?P(u,e)-P(u,t):0:4&n?-1:1)}:function(e,t){if(e===t)return l=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e==C?-1:t==C?1:i?-1:o?1:u?P(u,e)-P(u,t):0;if(i===o)return pe(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?pe(a[r],s[r]):a[r]==p?-1:s[r]==p?1:0}),C},se.matches=function(e,t){return se(e,null,null,t)},se.matchesSelector=function(e,t){if(T(e),d.matchesSelector&&E&&!N[t+" "]&&(!s||!s.test(t))&&(!v||!v.test(t)))try{var n=c.call(e,t);if(n||d.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(e){N(t,!0)}return 0<se(t,C,null,[e]).length},se.contains=function(e,t){return(e.ownerDocument||e)!=C&&T(e),y(e,t)},se.attr=function(e,t){(e.ownerDocument||e)!=C&&T(e);var n=b.attrHandle[t.toLowerCase()],r=n&&D.call(b.attrHandle,t.toLowerCase())?n(e,t,!E):void 0;return void 0!==r?r:d.attributes||!E?e.getAttribute(t):(r=e.getAttributeNode(t))&&r.specified?r.value:null},se.escape=function(e){return(e+"").replace(re,ie)},se.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},se.uniqueSort=function(e){var t,n=[],r=0,i=0;if(l=!d.detectDuplicates,u=!d.sortStable&&e.slice(0),e.sort(j),l){while(t=e[i++])t===e[i]&&(r=n.push(i));while(r--)e.splice(n[r],1)}return u=null,e},o=se.getText=function(e){var t,n="",r=0,i=e.nodeType;if(i){if(1===i||9===i||11===i){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=o(e)}else if(3===i||4===i)return e.nodeValue}else while(t=e[r++])n+=o(t);return n},(b=se.selectors={cacheLength:50,createPseudo:le,match:G,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(te,ne),e[3]=(e[3]||e[4]||e[5]||"").replace(te,ne),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||se.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&se.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return G.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=h(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(te,ne).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=m[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&m(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(n,r,i){return function(e){var t=se.attr(e,n);return null==t?"!="===r:!r||(t+="","="===r?t===i:"!="===r?t!==i:"^="===r?i&&0===t.indexOf(i):"*="===r?i&&-1<t.indexOf(i):"$="===r?i&&t.slice(-i.length)===i:"~="===r?-1<(" "+t.replace(B," ")+" ").indexOf(i):"|="===r&&(t===i||t.slice(0,i.length+1)===i+"-"))}},CHILD:function(h,e,t,g,v){var y="nth"!==h.slice(0,3),m="last"!==h.slice(-4),x="of-type"===e;return 1===g&&0===v?function(e){return!!e.parentNode}:function(e,t,n){var r,i,o,a,s,u,l=y!==m?"nextSibling":"previousSibling",c=e.parentNode,f=x&&e.nodeName.toLowerCase(),p=!n&&!x,d=!1;if(c){if(y){while(l){a=e;while(a=a[l])if(x?a.nodeName.toLowerCase()===f:1===a.nodeType)return!1;u=l="only"===h&&!u&&"nextSibling"}return!0}if(u=[m?c.firstChild:c.lastChild],m&&p){d=(s=(r=(i=(o=(a=c)[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===k&&r[1])&&r[2],a=s&&c.childNodes[s];while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if(1===a.nodeType&&++d&&a===e){i[h]=[k,s,d];break}}else if(p&&(d=s=(r=(i=(o=(a=e)[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===k&&r[1]),!1===d)while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if((x?a.nodeName.toLowerCase()===f:1===a.nodeType)&&++d&&(p&&((i=(o=a[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]=[k,d]),a===e))break;return(d-=v)===g||d%g==0&&0<=d/g}}},PSEUDO:function(e,o){var t,a=b.pseudos[e]||b.setFilters[e.toLowerCase()]||se.error("unsupported pseudo: "+e);return a[S]?a(o):1<a.length?(t=[e,e,"",o],b.setFilters.hasOwnProperty(e.toLowerCase())?le(function(e,t){var n,r=a(e,o),i=r.length;while(i--)e[n=P(e,r[i])]=!(t[n]=r[i])}):function(e){return a(e,0,t)}):a}},pseudos:{not:le(function(e){var r=[],i=[],s=f(e.replace($,"$1"));return s[S]?le(function(e,t,n,r){var i,o=s(e,null,r,[]),a=e.length;while(a--)(i=o[a])&&(e[a]=!(t[a]=i))}):function(e,t,n){return r[0]=e,s(r,null,n,i),r[0]=null,!i.pop()}}),has:le(function(t){return function(e){return 0<se(t,e).length}}),contains:le(function(t){return t=t.replace(te,ne),function(e){return-1<(e.textContent||o(e)).indexOf(t)}}),lang:le(function(n){return V.test(n||"")||se.error("unsupported lang: "+n),n=n.replace(te,ne).toLowerCase(),function(e){var t;do{if(t=E?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return(t=t.toLowerCase())===n||0===t.indexOf(n+"-")}while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var t=n.location&&n.location.hash;return t&&t.slice(1)===e.id},root:function(e){return e===a},focus:function(e){return e===C.activeElement&&(!C.hasFocus||C.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:ge(!1),disabled:ge(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!b.pseudos.empty(e)},header:function(e){return J.test(e.nodeName)},input:function(e){return Q.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:ve(function(){return[0]}),last:ve(function(e,t){return[t-1]}),eq:ve(function(e,t,n){return[n<0?n+t:n]}),even:ve(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:ve(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:ve(function(e,t,n){for(var r=n<0?n+t:t<n?t:n;0<=--r;)e.push(r);return e}),gt:ve(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=b.pseudos.eq,{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})b.pseudos[e]=de(e);for(e in{submit:!0,reset:!0})b.pseudos[e]=he(e);function me(){}function xe(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function be(s,e,t){var u=e.dir,l=e.next,c=l||u,f=t&&"parentNode"===c,p=r++;return e.first?function(e,t,n){while(e=e[u])if(1===e.nodeType||f)return s(e,t,n);return!1}:function(e,t,n){var r,i,o,a=[k,p];if(n){while(e=e[u])if((1===e.nodeType||f)&&s(e,t,n))return!0}else while(e=e[u])if(1===e.nodeType||f)if(i=(o=e[S]||(e[S]={}))[e.uniqueID]||(o[e.uniqueID]={}),l&&l===e.nodeName.toLowerCase())e=e[u]||e;else{if((r=i[c])&&r[0]===k&&r[1]===p)return a[2]=r[2];if((i[c]=a)[2]=s(e,t,n))return!0}return!1}}function we(i){return 1<i.length?function(e,t,n){var r=i.length;while(r--)if(!i[r](e,t,n))return!1;return!0}:i[0]}function Te(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Ce(d,h,g,v,y,e){return v&&!v[S]&&(v=Ce(v)),y&&!y[S]&&(y=Ce(y,e)),le(function(e,t,n,r){var i,o,a,s=[],u=[],l=t.length,c=e||function(e,t,n){for(var r=0,i=t.length;r<i;r++)se(e,t[r],n);return n}(h||"*",n.nodeType?[n]:n,[]),f=!d||!e&&h?c:Te(c,s,d,n,r),p=g?y||(e?d:l||v)?[]:t:f;if(g&&g(f,p,n,r),v){i=Te(p,u),v(i,[],n,r),o=i.length;while(o--)(a=i[o])&&(p[u[o]]=!(f[u[o]]=a))}if(e){if(y||d){if(y){i=[],o=p.length;while(o--)(a=p[o])&&i.push(f[o]=a);y(null,p=[],i,r)}o=p.length;while(o--)(a=p[o])&&-1<(i=y?P(e,a):s[o])&&(e[i]=!(t[i]=a))}}else p=Te(p===t?p.splice(l,p.length):p),y?y(null,t,p,r):H.apply(t,p)})}function Ee(e){for(var i,t,n,r=e.length,o=b.relative[e[0].type],a=o||b.relative[" "],s=o?1:0,u=be(function(e){return e===i},a,!0),l=be(function(e){return-1<P(i,e)},a,!0),c=[function(e,t,n){var r=!o&&(n||t!==w)||((i=t).nodeType?u(e,t,n):l(e,t,n));return i=null,r}];s<r;s++)if(t=b.relative[e[s].type])c=[be(we(c),t)];else{if((t=b.filter[e[s].type].apply(null,e[s].matches))[S]){for(n=++s;n<r;n++)if(b.relative[e[n].type])break;return Ce(1<s&&we(c),1<s&&xe(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace($,"$1"),t,s<n&&Ee(e.slice(s,n)),n<r&&Ee(e=e.slice(n)),n<r&&xe(e))}c.push(t)}return we(c)}return me.prototype=b.filters=b.pseudos,b.setFilters=new me,h=se.tokenize=function(e,t){var n,r,i,o,a,s,u,l=x[e+" "];if(l)return t?0:l.slice(0);a=e,s=[],u=b.preFilter;while(a){for(o in n&&!(r=_.exec(a))||(r&&(a=a.slice(r[0].length)||a),s.push(i=[])),n=!1,(r=z.exec(a))&&(n=r.shift(),i.push({value:n,type:r[0].replace($," ")}),a=a.slice(n.length)),b.filter)!(r=G[o].exec(a))||u[o]&&!(r=u[o](r))||(n=r.shift(),i.push({value:n,type:o,matches:r}),a=a.slice(n.length));if(!n)break}return t?a.length:a?se.error(e):x(e,s).slice(0)},f=se.compile=function(e,t){var n,v,y,m,x,r,i=[],o=[],a=A[e+" "];if(!a){t||(t=h(e)),n=t.length;while(n--)(a=Ee(t[n]))[S]?i.push(a):o.push(a);(a=A(e,(v=o,m=0<(y=i).length,x=0<v.length,r=function(e,t,n,r,i){var o,a,s,u=0,l="0",c=e&&[],f=[],p=w,d=e||x&&b.find.TAG("*",i),h=k+=null==p?1:Math.random()||.1,g=d.length;for(i&&(w=t==C||t||i);l!==g&&null!=(o=d[l]);l++){if(x&&o){a=0,t||o.ownerDocument==C||(T(o),n=!E);while(s=v[a++])if(s(o,t||C,n)){r.push(o);break}i&&(k=h)}m&&((o=!s&&o)&&u--,e&&c.push(o))}if(u+=l,m&&l!==u){a=0;while(s=y[a++])s(c,f,t,n);if(e){if(0<u)while(l--)c[l]||f[l]||(f[l]=q.call(r));f=Te(f)}H.apply(r,f),i&&!e&&0<f.length&&1<u+y.length&&se.uniqueSort(r)}return i&&(k=h,w=p),c},m?le(r):r))).selector=e}return a},g=se.select=function(e,t,n,r){var i,o,a,s,u,l="function"==typeof e&&e,c=!r&&h(e=l.selector||e);if(n=n||[],1===c.length){if(2<(o=c[0]=c[0].slice(0)).length&&"ID"===(a=o[0]).type&&9===t.nodeType&&E&&b.relative[o[1].type]){if(!(t=(b.find.ID(a.matches[0].replace(te,ne),t)||[])[0]))return n;l&&(t=t.parentNode),e=e.slice(o.shift().value.length)}i=G.needsContext.test(e)?0:o.length;while(i--){if(a=o[i],b.relative[s=a.type])break;if((u=b.find[s])&&(r=u(a.matches[0].replace(te,ne),ee.test(o[0].type)&&ye(t.parentNode)||t))){if(o.splice(i,1),!(e=r.length&&xe(o)))return H.apply(n,r),n;break}}}return(l||f(e,c))(r,t,!E,n,!t||ee.test(e)&&ye(t.parentNode)||t),n},d.sortStable=S.split("").sort(j).join("")===S,d.detectDuplicates=!!l,T(),d.sortDetached=ce(function(e){return 1&e.compareDocumentPosition(C.createElement("fieldset"))}),ce(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||fe("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),d.attributes&&ce(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||fe("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ce(function(e){return null==e.getAttribute("disabled")})||fe(R,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),se}(C);S.find=d,S.expr=d.selectors,S.expr[":"]=S.expr.pseudos,S.uniqueSort=S.unique=d.uniqueSort,S.text=d.getText,S.isXMLDoc=d.isXML,S.contains=d.contains,S.escapeSelector=d.escape;var h=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&S(e).is(n))break;r.push(e)}return r},T=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},k=S.expr.match.needsContext;function A(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var N=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function j(e,n,r){return m(n)?S.grep(e,function(e,t){return!!n.call(e,t,e)!==r}):n.nodeType?S.grep(e,function(e){return e===n!==r}):"string"!=typeof n?S.grep(e,function(e){return-1<i.call(n,e)!==r}):S.filter(n,e,r)}S.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?S.find.matchesSelector(r,e)?[r]:[]:S.find.matches(e,S.grep(t,function(e){return 1===e.nodeType}))},S.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(S(e).filter(function(){for(t=0;t<r;t++)if(S.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)S.find(e,i[t],n);return 1<r?S.uniqueSort(n):n},filter:function(e){return this.pushStack(j(this,e||[],!1))},not:function(e){return this.pushStack(j(this,e||[],!0))},is:function(e){return!!j(this,"string"==typeof e&&k.test(e)?S(e):e||[],!1).length}});var D,q=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(S.fn.init=function(e,t,n){var r,i;if(!e)return this;if(n=n||D,"string"==typeof e){if(!(r="<"===e[0]&&">"===e[e.length-1]&&3<=e.length?[null,e,null]:q.exec(e))||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof S?t[0]:t,S.merge(this,S.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:E,!0)),N.test(r[1])&&S.isPlainObject(t))for(r in t)m(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return(i=E.getElementById(r[2]))&&(this[0]=i,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):m(e)?void 0!==n.ready?n.ready(e):e(S):S.makeArray(e,this)}).prototype=S.fn,D=S(E);var L=/^(?:parents|prev(?:Until|All))/,H={children:!0,contents:!0,next:!0,prev:!0};function O(e,t){while((e=e[t])&&1!==e.nodeType);return e}S.fn.extend({has:function(e){var t=S(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(S.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&S(e);if(!k.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?-1<a.index(n):1===n.nodeType&&S.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(1<o.length?S.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?i.call(S(e),this[0]):i.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(S.uniqueSort(S.merge(this.get(),S(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),S.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return h(e,"parentNode")},parentsUntil:function(e,t,n){return h(e,"parentNode",n)},next:function(e){return O(e,"nextSibling")},prev:function(e){return O(e,"previousSibling")},nextAll:function(e){return h(e,"nextSibling")},prevAll:function(e){return h(e,"previousSibling")},nextUntil:function(e,t,n){return h(e,"nextSibling",n)},prevUntil:function(e,t,n){return h(e,"previousSibling",n)},siblings:function(e){return T((e.parentNode||{}).firstChild,e)},children:function(e){return T(e.firstChild)},contents:function(e){return null!=e.contentDocument&&r(e.contentDocument)?e.contentDocument:(A(e,"template")&&(e=e.content||e),S.merge([],e.childNodes))}},function(r,i){S.fn[r]=function(e,t){var n=S.map(this,i,e);return"Until"!==r.slice(-5)&&(t=e),t&&"string"==typeof t&&(n=S.filter(t,n)),1<this.length&&(H[r]||S.uniqueSort(n),L.test(r)&&n.reverse()),this.pushStack(n)}});var P=/[^\x20\t\r\n\f]+/g;function R(e){return e}function M(e){throw e}function I(e,t,n,r){var i;try{e&&m(i=e.promise)?i.call(e).done(t).fail(n):e&&m(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}S.Callbacks=function(r){var e,n;r="string"==typeof r?(e=r,n={},S.each(e.match(P)||[],function(e,t){n[t]=!0}),n):S.extend({},r);var i,t,o,a,s=[],u=[],l=-1,c=function(){for(a=a||r.once,o=i=!0;u.length;l=-1){t=u.shift();while(++l<s.length)!1===s[l].apply(t[0],t[1])&&r.stopOnFalse&&(l=s.length,t=!1)}r.memory||(t=!1),i=!1,a&&(s=t?[]:"")},f={add:function(){return s&&(t&&!i&&(l=s.length-1,u.push(t)),function n(e){S.each(e,function(e,t){m(t)?r.unique&&f.has(t)||s.push(t):t&&t.length&&"string"!==w(t)&&n(t)})}(arguments),t&&!i&&c()),this},remove:function(){return S.each(arguments,function(e,t){var n;while(-1<(n=S.inArray(t,s,n)))s.splice(n,1),n<=l&&l--}),this},has:function(e){return e?-1<S.inArray(e,s):0<s.length},empty:function(){return s&&(s=[]),this},disable:function(){return a=u=[],s=t="",this},disabled:function(){return!s},lock:function(){return a=u=[],t||i||(s=t=""),this},locked:function(){return!!a},fireWith:function(e,t){return a||(t=[e,(t=t||[]).slice?t.slice():t],u.push(t),i||c()),this},fire:function(){return f.fireWith(this,arguments),this},fired:function(){return!!o}};return f},S.extend({Deferred:function(e){var o=[["notify","progress",S.Callbacks("memory"),S.Callbacks("memory"),2],["resolve","done",S.Callbacks("once memory"),S.Callbacks("once memory"),0,"resolved"],["reject","fail",S.Callbacks("once memory"),S.Callbacks("once memory"),1,"rejected"]],i="pending",a={state:function(){return i},always:function(){return s.done(arguments).fail(arguments),this},"catch":function(e){return a.then(null,e)},pipe:function(){var i=arguments;return S.Deferred(function(r){S.each(o,function(e,t){var n=m(i[t[4]])&&i[t[4]];s[t[1]](function(){var e=n&&n.apply(this,arguments);e&&m(e.promise)?e.promise().progress(r.notify).done(r.resolve).fail(r.reject):r[t[0]+"With"](this,n?[e]:arguments)})}),i=null}).promise()},then:function(t,n,r){var u=0;function l(i,o,a,s){return function(){var n=this,r=arguments,e=function(){var e,t;if(!(i<u)){if((e=a.apply(n,r))===o.promise())throw new TypeError("Thenable self-resolution");t=e&&("object"==typeof e||"function"==typeof e)&&e.then,m(t)?s?t.call(e,l(u,o,R,s),l(u,o,M,s)):(u++,t.call(e,l(u,o,R,s),l(u,o,M,s),l(u,o,R,o.notifyWith))):(a!==R&&(n=void 0,r=[e]),(s||o.resolveWith)(n,r))}},t=s?e:function(){try{e()}catch(e){S.Deferred.exceptionHook&&S.Deferred.exceptionHook(e,t.stackTrace),u<=i+1&&(a!==M&&(n=void 0,r=[e]),o.rejectWith(n,r))}};i?t():(S.Deferred.getStackHook&&(t.stackTrace=S.Deferred.getStackHook()),C.setTimeout(t))}}return S.Deferred(function(e){o[0][3].add(l(0,e,m(r)?r:R,e.notifyWith)),o[1][3].add(l(0,e,m(t)?t:R)),o[2][3].add(l(0,e,m(n)?n:M))}).promise()},promise:function(e){return null!=e?S.extend(e,a):a}},s={};return S.each(o,function(e,t){var n=t[2],r=t[5];a[t[1]]=n.add,r&&n.add(function(){i=r},o[3-e][2].disable,o[3-e][3].disable,o[0][2].lock,o[0][3].lock),n.add(t[3].fire),s[t[0]]=function(){return s[t[0]+"With"](this===s?void 0:this,arguments),this},s[t[0]+"With"]=n.fireWith}),a.promise(s),e&&e.call(s,s),s},when:function(e){var n=arguments.length,t=n,r=Array(t),i=s.call(arguments),o=S.Deferred(),a=function(t){return function(e){r[t]=this,i[t]=1<arguments.length?s.call(arguments):e,--n||o.resolveWith(r,i)}};if(n<=1&&(I(e,o.done(a(t)).resolve,o.reject,!n),"pending"===o.state()||m(i[t]&&i[t].then)))return o.then();while(t--)I(i[t],a(t),o.reject);return o.promise()}});var W=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;S.Deferred.exceptionHook=function(e,t){C.console&&C.console.warn&&e&&W.test(e.name)&&C.console.warn("jQuery.Deferred exception: "+e.message,e.stack,t)},S.readyException=function(e){C.setTimeout(function(){throw e})};var F=S.Deferred();function B(){E.removeEventListener("DOMContentLoaded",B),C.removeEventListener("load",B),S.ready()}S.fn.ready=function(e){return F.then(e)["catch"](function(e){S.readyException(e)}),this},S.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--S.readyWait:S.isReady)||(S.isReady=!0)!==e&&0<--S.readyWait||F.resolveWith(E,[S])}}),S.ready.then=F.then,"complete"===E.readyState||"loading"!==E.readyState&&!E.documentElement.doScroll?C.setTimeout(S.ready):(E.addEventListener("DOMContentLoaded",B),C.addEventListener("load",B));var $=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===w(n))for(s in i=!0,n)$(e,t,s,n[s],!0,o,a);else if(void 0!==r&&(i=!0,m(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(S(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},_=/^-ms-/,z=/-([a-z])/g;function U(e,t){return t.toUpperCase()}function X(e){return e.replace(_,"ms-").replace(z,U)}var V=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function G(){this.expando=S.expando+G.uid++}G.uid=1,G.prototype={cache:function(e){var t=e[this.expando];return t||(t={},V(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[X(t)]=n;else for(r in t)i[X(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][X(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(X):(t=X(t))in r?[t]:t.match(P)||[]).length;while(n--)delete r[t[n]]}(void 0===t||S.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!S.isEmptyObject(t)}};var Y=new G,Q=new G,J=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,K=/[A-Z]/g;function Z(e,t,n){var r,i;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(K,"-$&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n="true"===(i=n)||"false"!==i&&("null"===i?null:i===+i+""?+i:J.test(i)?JSON.parse(i):i)}catch(e){}Q.set(e,t,n)}else n=void 0;return n}S.extend({hasData:function(e){return Q.hasData(e)||Y.hasData(e)},data:function(e,t,n){return Q.access(e,t,n)},removeData:function(e,t){Q.remove(e,t)},_data:function(e,t,n){return Y.access(e,t,n)},_removeData:function(e,t){Y.remove(e,t)}}),S.fn.extend({data:function(n,e){var t,r,i,o=this[0],a=o&&o.attributes;if(void 0===n){if(this.length&&(i=Q.get(o),1===o.nodeType&&!Y.get(o,"hasDataAttrs"))){t=a.length;while(t--)a[t]&&0===(r=a[t].name).indexOf("data-")&&(r=X(r.slice(5)),Z(o,r,i[r]));Y.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof n?this.each(function(){Q.set(this,n)}):$(this,function(e){var t;if(o&&void 0===e)return void 0!==(t=Q.get(o,n))?t:void 0!==(t=Z(o,n))?t:void 0;this.each(function(){Q.set(this,n,e)})},null,e,1<arguments.length,null,!0)},removeData:function(e){return this.each(function(){Q.remove(this,e)})}}),S.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=Y.get(e,t),n&&(!r||Array.isArray(n)?r=Y.access(e,t,S.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=S.queue(e,t),r=n.length,i=n.shift(),o=S._queueHooks(e,t);"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,function(){S.dequeue(e,t)},o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return Y.get(e,n)||Y.access(e,n,{empty:S.Callbacks("once memory").add(function(){Y.remove(e,[t+"queue",n])})})}}),S.fn.extend({queue:function(t,n){var e=2;return"string"!=typeof t&&(n=t,t="fx",e--),arguments.length<e?S.queue(this[0],t):void 0===n?this:this.each(function(){var e=S.queue(this,t,n);S._queueHooks(this,t),"fx"===t&&"inprogress"!==e[0]&&S.dequeue(this,t)})},dequeue:function(e){return this.each(function(){S.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=S.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=Y.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var ee=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,te=new RegExp("^(?:([+-])=|)("+ee+")([a-z%]*)$","i"),ne=["Top","Right","Bottom","Left"],re=E.documentElement,ie=function(e){return S.contains(e.ownerDocument,e)},oe={composed:!0};re.getRootNode&&(ie=function(e){return S.contains(e.ownerDocument,e)||e.getRootNode(oe)===e.ownerDocument});var ae=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&ie(e)&&"none"===S.css(e,"display")};function se(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return S.css(e,t,"")},u=s(),l=n&&n[3]||(S.cssNumber[t]?"":"px"),c=e.nodeType&&(S.cssNumber[t]||"px"!==l&&+u)&&te.exec(S.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)S.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,S.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var ue={};function le(e,t){for(var n,r,i,o,a,s,u,l=[],c=0,f=e.length;c<f;c++)(r=e[c]).style&&(n=r.style.display,t?("none"===n&&(l[c]=Y.get(r,"display")||null,l[c]||(r.style.display="")),""===r.style.display&&ae(r)&&(l[c]=(u=a=o=void 0,a=(i=r).ownerDocument,s=i.nodeName,(u=ue[s])||(o=a.body.appendChild(a.createElement(s)),u=S.css(o,"display"),o.parentNode.removeChild(o),"none"===u&&(u="block"),ue[s]=u)))):"none"!==n&&(l[c]="none",Y.set(r,"display",n)));for(c=0;c<f;c++)null!=l[c]&&(e[c].style.display=l[c]);return e}S.fn.extend({show:function(){return le(this,!0)},hide:function(){return le(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){ae(this)?S(this).show():S(this).hide()})}});var ce,fe,pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,he=/^$|^module$|\/(?:java|ecma)script/i;ce=E.createDocumentFragment().appendChild(E.createElement("div")),(fe=E.createElement("input")).setAttribute("type","radio"),fe.setAttribute("checked","checked"),fe.setAttribute("name","t"),ce.appendChild(fe),y.checkClone=ce.cloneNode(!0).cloneNode(!0).lastChild.checked,ce.innerHTML="<textarea>x</textarea>",y.noCloneChecked=!!ce.cloneNode(!0).lastChild.defaultValue,ce.innerHTML="<option></option>",y.option=!!ce.lastChild;var ge={thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};function ve(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&A(e,t)?S.merge([e],n):n}function ye(e,t){for(var n=0,r=e.length;n<r;n++)Y.set(e[n],"globalEval",!t||Y.get(t[n],"globalEval"))}ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td,y.option||(ge.optgroup=ge.option=[1,"<select multiple='multiple'>","</select>"]);var me=/<|&#?\w+;/;function xe(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===w(o))S.merge(p,o.nodeType?[o]:o);else if(me.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+S.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;S.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&-1<S.inArray(o,r))i&&i.push(o);else if(l=ie(o),a=ve(f.appendChild(o),"script"),l&&ye(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}var be=/^([^.]*)(?:\.(.+)|)/;function we(){return!0}function Te(){return!1}function Ce(e,t){return e===function(){try{return E.activeElement}catch(e){}}()==("focus"===t)}function Ee(e,t,n,r,i,o){var a,s;if("object"==typeof t){for(s in"string"!=typeof n&&(r=r||n,n=void 0),t)Ee(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=Te;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return S().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=S.guid++)),e.each(function(){S.event.add(this,t,i,r,n)})}function Se(e,i,o){o?(Y.set(e,i,!1),S.event.add(e,i,{namespace:!1,handler:function(e){var t,n,r=Y.get(this,i);if(1&e.isTrigger&&this[i]){if(r.length)(S.event.special[i]||{}).delegateType&&e.stopPropagation();else if(r=s.call(arguments),Y.set(this,i,r),t=o(this,i),this[i](),r!==(n=Y.get(this,i))||t?Y.set(this,i,!1):n={},r!==n)return e.stopImmediatePropagation(),e.preventDefault(),n&&n.value}else r.length&&(Y.set(this,i,{value:S.event.trigger(S.extend(r[0],S.Event.prototype),r.slice(1),this)}),e.stopImmediatePropagation())}})):void 0===Y.get(e,i)&&S.event.add(e,i,we)}S.event={global:{},add:function(t,e,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Y.get(t);if(V(t)){n.handler&&(n=(o=n).handler,i=o.selector),i&&S.find.matchesSelector(re,i),n.guid||(n.guid=S.guid++),(u=v.events)||(u=v.events=Object.create(null)),(a=v.handle)||(a=v.handle=function(e){return"undefined"!=typeof S&&S.event.triggered!==e.type?S.event.dispatch.apply(t,arguments):void 0}),l=(e=(e||"").match(P)||[""]).length;while(l--)d=g=(s=be.exec(e[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=S.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=S.event.special[d]||{},c=S.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&S.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(t,r,h,a)||t.addEventListener&&t.addEventListener(d,a)),f.add&&(f.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),S.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Y.hasData(e)&&Y.get(e);if(v&&(u=v.events)){l=(t=(t||"").match(P)||[""]).length;while(l--)if(d=g=(s=be.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d){f=S.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,v.handle)||S.removeEvent(e,d,v.handle),delete u[d])}else for(d in u)S.event.remove(e,d+t[l],n,r,!0);S.isEmptyObject(u)&&Y.remove(e,"handle events")}},dispatch:function(e){var t,n,r,i,o,a,s=new Array(arguments.length),u=S.event.fix(e),l=(Y.get(this,"events")||Object.create(null))[u.type]||[],c=S.event.special[u.type]||{};for(s[0]=u,t=1;t<arguments.length;t++)s[t]=arguments[t];if(u.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,u)){a=S.event.handlers.call(this,u,l),t=0;while((i=a[t++])&&!u.isPropagationStopped()){u.currentTarget=i.elem,n=0;while((o=i.handlers[n++])&&!u.isImmediatePropagationStopped())u.rnamespace&&!1!==o.namespace&&!u.rnamespace.test(o.namespace)||(u.handleObj=o,u.data=o.data,void 0!==(r=((S.event.special[o.origType]||{}).handle||o.handler).apply(i.elem,s))&&!1===(u.result=r)&&(u.preventDefault(),u.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,u),u.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&1<=e.button))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?-1<S(i,this).index(l):S.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(t,e){Object.defineProperty(S.Event.prototype,t,{enumerable:!0,configurable:!0,get:m(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(e){return e[S.expando]?e:new S.Event(e)},special:{load:{noBubble:!0},click:{setup:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&Se(t,"click",we),!1},trigger:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&Se(t,"click"),!0},_default:function(e){var t=e.target;return pe.test(t.type)&&t.click&&A(t,"input")&&Y.get(t,"click")||A(t,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},S.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},S.Event=function(e,t){if(!(this instanceof S.Event))return new S.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?we:Te,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&S.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[S.expando]=!0},S.Event.prototype={constructor:S.Event,isDefaultPrevented:Te,isPropagationStopped:Te,isImmediatePropagationStopped:Te,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=we,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=we,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=we,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},S.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:!0},S.event.addProp),S.each({focus:"focusin",blur:"focusout"},function(e,t){S.event.special[e]={setup:function(){return Se(this,e,Ce),!1},trigger:function(){return Se(this,e),!0},_default:function(){return!0},delegateType:t}}),S.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,i){S.event.special[e]={delegateType:i,bindType:i,handle:function(e){var t,n=e.relatedTarget,r=e.handleObj;return n&&(n===this||S.contains(this,n))||(e.type=r.origType,t=r.handler.apply(this,arguments),e.type=i),t}}}),S.fn.extend({on:function(e,t,n,r){return Ee(this,e,t,n,r)},one:function(e,t,n,r){return Ee(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,S(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=Te),this.each(function(){S.event.remove(this,e,n,t)})}});var ke=/<script|<style|<link/i,Ae=/checked\s*(?:[^=]|=\s*.checked.)/i,Ne=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function je(e,t){return A(e,"table")&&A(11!==t.nodeType?t:t.firstChild,"tr")&&S(e).children("tbody")[0]||e}function De(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function qe(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Le(e,t){var n,r,i,o,a,s;if(1===t.nodeType){if(Y.hasData(e)&&(s=Y.get(e).events))for(i in Y.remove(t,"handle events"),s)for(n=0,r=s[i].length;n<r;n++)S.event.add(t,i,s[i][n]);Q.hasData(e)&&(o=Q.access(e),a=S.extend({},o),Q.set(t,a))}}function He(n,r,i,o){r=g(r);var e,t,a,s,u,l,c=0,f=n.length,p=f-1,d=r[0],h=m(d);if(h||1<f&&"string"==typeof d&&!y.checkClone&&Ae.test(d))return n.each(function(e){var t=n.eq(e);h&&(r[0]=d.call(this,e,t.html())),He(t,r,i,o)});if(f&&(t=(e=xe(r,n[0].ownerDocument,!1,n,o)).firstChild,1===e.childNodes.length&&(e=t),t||o)){for(s=(a=S.map(ve(e,"script"),De)).length;c<f;c++)u=e,c!==p&&(u=S.clone(u,!0,!0),s&&S.merge(a,ve(u,"script"))),i.call(n[c],u,c);if(s)for(l=a[a.length-1].ownerDocument,S.map(a,qe),c=0;c<s;c++)u=a[c],he.test(u.type||"")&&!Y.access(u,"globalEval")&&S.contains(l,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?S._evalUrl&&!u.noModule&&S._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")},l):b(u.textContent.replace(Ne,""),u,l))}return n}function Oe(e,t,n){for(var r,i=t?S.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||S.cleanData(ve(r)),r.parentNode&&(n&&ie(r)&&ye(ve(r,"script")),r.parentNode.removeChild(r));return e}S.extend({htmlPrefilter:function(e){return e},clone:function(e,t,n){var r,i,o,a,s,u,l,c=e.cloneNode(!0),f=ie(e);if(!(y.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||S.isXMLDoc(e)))for(a=ve(c),r=0,i=(o=ve(e)).length;r<i;r++)s=o[r],u=a[r],void 0,"input"===(l=u.nodeName.toLowerCase())&&pe.test(s.type)?u.checked=s.checked:"input"!==l&&"textarea"!==l||(u.defaultValue=s.defaultValue);if(t)if(n)for(o=o||ve(e),a=a||ve(c),r=0,i=o.length;r<i;r++)Le(o[r],a[r]);else Le(e,c);return 0<(a=ve(c,"script")).length&&ye(a,!f&&ve(e,"script")),c},cleanData:function(e){for(var t,n,r,i=S.event.special,o=0;void 0!==(n=e[o]);o++)if(V(n)){if(t=n[Y.expando]){if(t.events)for(r in t.events)i[r]?S.event.remove(n,r):S.removeEvent(n,r,t.handle);n[Y.expando]=void 0}n[Q.expando]&&(n[Q.expando]=void 0)}}}),S.fn.extend({detach:function(e){return Oe(this,e,!0)},remove:function(e){return Oe(this,e)},text:function(e){return $(this,function(e){return void 0===e?S.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return He(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||je(this,e).appendChild(e)})},prepend:function(){return He(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=je(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return He(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return He(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(S.cleanData(ve(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return S.clone(this,e,t)})},html:function(e){return $(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!ke.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=S.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(S.cleanData(ve(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var n=[];return He(this,arguments,function(e){var t=this.parentNode;S.inArray(this,n)<0&&(S.cleanData(ve(this)),t&&t.replaceChild(e,this))},n)}}),S.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,a){S.fn[e]=function(e){for(var t,n=[],r=S(e),i=r.length-1,o=0;o<=i;o++)t=o===i?this:this.clone(!0),S(r[o])[a](t),u.apply(n,t.get());return this.pushStack(n)}});var Pe=new RegExp("^("+ee+")(?!px)[a-z%]+$","i"),Re=function(e){var t=e.ownerDocument.defaultView;return t&&t.opener||(t=C),t.getComputedStyle(e)},Me=function(e,t,n){var r,i,o={};for(i in t)o[i]=e.style[i],e.style[i]=t[i];for(i in r=n.call(e),t)e.style[i]=o[i];return r},Ie=new RegExp(ne.join("|"),"i");function We(e,t,n){var r,i,o,a,s=e.style;return(n=n||Re(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||ie(e)||(a=S.style(e,t)),!y.pixelBoxStyles()&&Pe.test(a)&&Ie.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function Fe(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}!function(){function e(){if(l){u.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",l.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",re.appendChild(u).appendChild(l);var e=C.getComputedStyle(l);n="1%"!==e.top,s=12===t(e.marginLeft),l.style.right="60%",o=36===t(e.right),r=36===t(e.width),l.style.position="absolute",i=12===t(l.offsetWidth/3),re.removeChild(u),l=null}}function t(e){return Math.round(parseFloat(e))}var n,r,i,o,a,s,u=E.createElement("div"),l=E.createElement("div");l.style&&(l.style.backgroundClip="content-box",l.cloneNode(!0).style.backgroundClip="",y.clearCloneStyle="content-box"===l.style.backgroundClip,S.extend(y,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),o},pixelPosition:function(){return e(),n},reliableMarginLeft:function(){return e(),s},scrollboxSize:function(){return e(),i},reliableTrDimensions:function(){var e,t,n,r;return null==a&&(e=E.createElement("table"),t=E.createElement("tr"),n=E.createElement("div"),e.style.cssText="position:absolute;left:-11111px;border-collapse:separate",t.style.cssText="border:1px solid",t.style.height="1px",n.style.height="9px",n.style.display="block",re.appendChild(e).appendChild(t).appendChild(n),r=C.getComputedStyle(t),a=parseInt(r.height,10)+parseInt(r.borderTopWidth,10)+parseInt(r.borderBottomWidth,10)===t.offsetHeight,re.removeChild(e)),a}}))}();var Be=["Webkit","Moz","ms"],$e=E.createElement("div").style,_e={};function ze(e){var t=S.cssProps[e]||_e[e];return t||(e in $e?e:_e[e]=function(e){var t=e[0].toUpperCase()+e.slice(1),n=Be.length;while(n--)if((e=Be[n]+t)in $e)return e}(e)||e)}var Ue=/^(none|table(?!-c[ea]).+)/,Xe=/^--/,Ve={position:"absolute",visibility:"hidden",display:"block"},Ge={letterSpacing:"0",fontWeight:"400"};function Ye(e,t,n){var r=te.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function Qe(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=S.css(e,n+ne[a],!0,i)),r?("content"===n&&(u-=S.css(e,"padding"+ne[a],!0,i)),"margin"!==n&&(u-=S.css(e,"border"+ne[a]+"Width",!0,i))):(u+=S.css(e,"padding"+ne[a],!0,i),"padding"!==n?u+=S.css(e,"border"+ne[a]+"Width",!0,i):s+=S.css(e,"border"+ne[a]+"Width",!0,i));return!r&&0<=o&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))||0),u}function Je(e,t,n){var r=Re(e),i=(!y.boxSizingReliable()||n)&&"border-box"===S.css(e,"boxSizing",!1,r),o=i,a=We(e,t,r),s="offset"+t[0].toUpperCase()+t.slice(1);if(Pe.test(a)){if(!n)return a;a="auto"}return(!y.boxSizingReliable()&&i||!y.reliableTrDimensions()&&A(e,"tr")||"auto"===a||!parseFloat(a)&&"inline"===S.css(e,"display",!1,r))&&e.getClientRects().length&&(i="border-box"===S.css(e,"boxSizing",!1,r),(o=s in e)&&(a=e[s])),(a=parseFloat(a)||0)+Qe(e,t,n||(i?"border":"content"),o,r,a)+"px"}function Ke(e,t,n,r,i){return new Ke.prototype.init(e,t,n,r,i)}S.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=We(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=X(t),u=Xe.test(t),l=e.style;if(u||(t=ze(s)),a=S.cssHooks[t]||S.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"===(o=typeof n)&&(i=te.exec(n))&&i[1]&&(n=se(e,t,i),o="number"),null!=n&&n==n&&("number"!==o||u||(n+=i&&i[3]||(S.cssNumber[s]?"":"px")),y.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=X(t);return Xe.test(t)||(t=ze(s)),(a=S.cssHooks[t]||S.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=We(e,t,r)),"normal"===i&&t in Ge&&(i=Ge[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),S.each(["height","width"],function(e,u){S.cssHooks[u]={get:function(e,t,n){if(t)return!Ue.test(S.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?Je(e,u,n):Me(e,Ve,function(){return Je(e,u,n)})},set:function(e,t,n){var r,i=Re(e),o=!y.scrollboxSize()&&"absolute"===i.position,a=(o||n)&&"border-box"===S.css(e,"boxSizing",!1,i),s=n?Qe(e,u,n,a,i):0;return a&&o&&(s-=Math.ceil(e["offset"+u[0].toUpperCase()+u.slice(1)]-parseFloat(i[u])-Qe(e,u,"border",!1,i)-.5)),s&&(r=te.exec(t))&&"px"!==(r[3]||"px")&&(e.style[u]=t,t=S.css(e,u)),Ye(0,t,s)}}}),S.cssHooks.marginLeft=Fe(y.reliableMarginLeft,function(e,t){if(t)return(parseFloat(We(e,"marginLeft"))||e.getBoundingClientRect().left-Me(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),S.each({margin:"",padding:"",border:"Width"},function(i,o){S.cssHooks[i+o]={expand:function(e){for(var t=0,n={},r="string"==typeof e?e.split(" "):[e];t<4;t++)n[i+ne[t]+o]=r[t]||r[t-2]||r[0];return n}},"margin"!==i&&(S.cssHooks[i+o].set=Ye)}),S.fn.extend({css:function(e,t){return $(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=Re(e),i=t.length;a<i;a++)o[t[a]]=S.css(e,t[a],!1,r);return o}return void 0!==n?S.style(e,t,n):S.css(e,t)},e,t,1<arguments.length)}}),((S.Tween=Ke).prototype={constructor:Ke,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||S.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(S.cssNumber[n]?"":"px")},cur:function(){var e=Ke.propHooks[this.prop];return e&&e.get?e.get(this):Ke.propHooks._default.get(this)},run:function(e){var t,n=Ke.propHooks[this.prop];return this.options.duration?this.pos=t=S.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):Ke.propHooks._default.set(this),this}}).init.prototype=Ke.prototype,(Ke.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=S.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){S.fx.step[e.prop]?S.fx.step[e.prop](e):1!==e.elem.nodeType||!S.cssHooks[e.prop]&&null==e.elem.style[ze(e.prop)]?e.elem[e.prop]=e.now:S.style(e.elem,e.prop,e.now+e.unit)}}}).scrollTop=Ke.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},S.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},S.fx=Ke.prototype.init,S.fx.step={};var Ze,et,tt,nt,rt=/^(?:toggle|show|hide)$/,it=/queueHooks$/;function ot(){et&&(!1===E.hidden&&C.requestAnimationFrame?C.requestAnimationFrame(ot):C.setTimeout(ot,S.fx.interval),S.fx.tick())}function at(){return C.setTimeout(function(){Ze=void 0}),Ze=Date.now()}function st(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=ne[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function ut(e,t,n){for(var r,i=(lt.tweeners[t]||[]).concat(lt.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function lt(o,e,t){var n,a,r=0,i=lt.prefilters.length,s=S.Deferred().always(function(){delete u.elem}),u=function(){if(a)return!1;for(var e=Ze||at(),t=Math.max(0,l.startTime+l.duration-e),n=1-(t/l.duration||0),r=0,i=l.tweens.length;r<i;r++)l.tweens[r].run(n);return s.notifyWith(o,[l,n,t]),n<1&&i?t:(i||s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l]),!1)},l=s.promise({elem:o,props:S.extend({},e),opts:S.extend(!0,{specialEasing:{},easing:S.easing._default},t),originalProperties:e,originalOptions:t,startTime:Ze||at(),duration:t.duration,tweens:[],createTween:function(e,t){var n=S.Tween(o,l.opts,e,t,l.opts.specialEasing[e]||l.opts.easing);return l.tweens.push(n),n},stop:function(e){var t=0,n=e?l.tweens.length:0;if(a)return this;for(a=!0;t<n;t++)l.tweens[t].run(1);return e?(s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l,e])):s.rejectWith(o,[l,e]),this}}),c=l.props;for(!function(e,t){var n,r,i,o,a;for(n in e)if(i=t[r=X(n)],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=S.cssHooks[r])&&"expand"in a)for(n in o=a.expand(o),delete e[r],o)n in e||(e[n]=o[n],t[n]=i);else t[r]=i}(c,l.opts.specialEasing);r<i;r++)if(n=lt.prefilters[r].call(l,o,c,l.opts))return m(n.stop)&&(S._queueHooks(l.elem,l.opts.queue).stop=n.stop.bind(n)),n;return S.map(c,ut,l),m(l.opts.start)&&l.opts.start.call(o,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),S.fx.timer(S.extend(u,{elem:o,anim:l,queue:l.opts.queue})),l}S.Animation=S.extend(lt,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return se(n.elem,e,te.exec(t),n),n}]},tweener:function(e,t){m(e)?(t=e,e=["*"]):e=e.match(P);for(var n,r=0,i=e.length;r<i;r++)n=e[r],lt.tweeners[n]=lt.tweeners[n]||[],lt.tweeners[n].unshift(t)},prefilters:[function(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&ae(e),v=Y.get(e,"fxshow");for(r in n.queue||(null==(a=S._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,S.queue(e,"fx").length||a.empty.fire()})})),t)if(i=t[r],rt.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!v||void 0===v[r])continue;g=!0}d[r]=v&&v[r]||S.style(e,r)}if((u=!S.isEmptyObject(t))||!S.isEmptyObject(d))for(r in f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=v&&v.display)&&(l=Y.get(e,"display")),"none"===(c=S.css(e,"display"))&&(l?c=l:(le([e],!0),l=e.style.display||l,c=S.css(e,"display"),le([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===S.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1,d)u||(v?"hidden"in v&&(g=v.hidden):v=Y.access(e,"fxshow",{display:l}),o&&(v.hidden=!g),g&&le([e],!0),p.done(function(){for(r in g||le([e]),Y.remove(e,"fxshow"),d)S.style(e,r,d[r])})),u=ut(g?v[r]:0,r,p),r in v||(v[r]=u.start,g&&(u.end=u.start,u.start=0))}],prefilter:function(e,t){t?lt.prefilters.unshift(e):lt.prefilters.push(e)}}),S.speed=function(e,t,n){var r=e&&"object"==typeof e?S.extend({},e):{complete:n||!n&&t||m(e)&&e,duration:e,easing:n&&t||t&&!m(t)&&t};return S.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in S.fx.speeds?r.duration=S.fx.speeds[r.duration]:r.duration=S.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){m(r.old)&&r.old.call(this),r.queue&&S.dequeue(this,r.queue)},r},S.fn.extend({fadeTo:function(e,t,n,r){return this.filter(ae).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(t,e,n,r){var i=S.isEmptyObject(t),o=S.speed(e,n,r),a=function(){var e=lt(this,S.extend({},t),o);(i||Y.get(this,"finish"))&&e.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(i,e,o){var a=function(e){var t=e.stop;delete e.stop,t(o)};return"string"!=typeof i&&(o=e,e=i,i=void 0),e&&this.queue(i||"fx",[]),this.each(function(){var e=!0,t=null!=i&&i+"queueHooks",n=S.timers,r=Y.get(this);if(t)r[t]&&r[t].stop&&a(r[t]);else for(t in r)r[t]&&r[t].stop&&it.test(t)&&a(r[t]);for(t=n.length;t--;)n[t].elem!==this||null!=i&&n[t].queue!==i||(n[t].anim.stop(o),e=!1,n.splice(t,1));!e&&o||S.dequeue(this,i)})},finish:function(a){return!1!==a&&(a=a||"fx"),this.each(function(){var e,t=Y.get(this),n=t[a+"queue"],r=t[a+"queueHooks"],i=S.timers,o=n?n.length:0;for(t.finish=!0,S.queue(this,a,[]),r&&r.stop&&r.stop.call(this,!0),e=i.length;e--;)i[e].elem===this&&i[e].queue===a&&(i[e].anim.stop(!0),i.splice(e,1));for(e=0;e<o;e++)n[e]&&n[e].finish&&n[e].finish.call(this);delete t.finish})}}),S.each(["toggle","show","hide"],function(e,r){var i=S.fn[r];S.fn[r]=function(e,t,n){return null==e||"boolean"==typeof e?i.apply(this,arguments):this.animate(st(r,!0),e,t,n)}}),S.each({slideDown:st("show"),slideUp:st("hide"),slideToggle:st("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,r){S.fn[e]=function(e,t,n){return this.animate(r,e,t,n)}}),S.timers=[],S.fx.tick=function(){var e,t=0,n=S.timers;for(Ze=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||S.fx.stop(),Ze=void 0},S.fx.timer=function(e){S.timers.push(e),S.fx.start()},S.fx.interval=13,S.fx.start=function(){et||(et=!0,ot())},S.fx.stop=function(){et=null},S.fx.speeds={slow:600,fast:200,_default:400},S.fn.delay=function(r,e){return r=S.fx&&S.fx.speeds[r]||r,e=e||"fx",this.queue(e,function(e,t){var n=C.setTimeout(e,r);t.stop=function(){C.clearTimeout(n)}})},tt=E.createElement("input"),nt=E.createElement("select").appendChild(E.createElement("option")),tt.type="checkbox",y.checkOn=""!==tt.value,y.optSelected=nt.selected,(tt=E.createElement("input")).value="t",tt.type="radio",y.radioValue="t"===tt.value;var ct,ft=S.expr.attrHandle;S.fn.extend({attr:function(e,t){return $(this,S.attr,e,t,1<arguments.length)},removeAttr:function(e){return this.each(function(){S.removeAttr(this,e)})}}),S.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?S.prop(e,t,n):(1===o&&S.isXMLDoc(e)||(i=S.attrHooks[t.toLowerCase()]||(S.expr.match.bool.test(t)?ct:void 0)),void 0!==n?null===n?void S.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=S.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!y.radioValue&&"radio"===t&&A(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(P);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),ct={set:function(e,t,n){return!1===t?S.removeAttr(e,n):e.setAttribute(n,n),n}},S.each(S.expr.match.bool.source.match(/\w+/g),function(e,t){var a=ft[t]||S.find.attr;ft[t]=function(e,t,n){var r,i,o=t.toLowerCase();return n||(i=ft[o],ft[o]=r,r=null!=a(e,t,n)?o:null,ft[o]=i),r}});var pt=/^(?:input|select|textarea|button)$/i,dt=/^(?:a|area)$/i;function ht(e){return(e.match(P)||[]).join(" ")}function gt(e){return e.getAttribute&&e.getAttribute("class")||""}function vt(e){return Array.isArray(e)?e:"string"==typeof e&&e.match(P)||[]}S.fn.extend({prop:function(e,t){return $(this,S.prop,e,t,1<arguments.length)},removeProp:function(e){return this.each(function(){delete this[S.propFix[e]||e]})}}),S.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&S.isXMLDoc(e)||(t=S.propFix[t]||t,i=S.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=S.find.attr(e,"tabindex");return t?parseInt(t,10):pt.test(e.nodeName)||dt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),y.optSelected||(S.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),S.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){S.propFix[this.toLowerCase()]=this}),S.fn.extend({addClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){S(this).addClass(t.call(this,e,gt(this)))});if((e=vt(t)).length)while(n=this[u++])if(i=gt(n),r=1===n.nodeType&&" "+ht(i)+" "){a=0;while(o=e[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=ht(r))&&n.setAttribute("class",s)}return this},removeClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){S(this).removeClass(t.call(this,e,gt(this)))});if(!arguments.length)return this.attr("class","");if((e=vt(t)).length)while(n=this[u++])if(i=gt(n),r=1===n.nodeType&&" "+ht(i)+" "){a=0;while(o=e[a++])while(-1<r.indexOf(" "+o+" "))r=r.replace(" "+o+" "," ");i!==(s=ht(r))&&n.setAttribute("class",s)}return this},toggleClass:function(i,t){var o=typeof i,a="string"===o||Array.isArray(i);return"boolean"==typeof t&&a?t?this.addClass(i):this.removeClass(i):m(i)?this.each(function(e){S(this).toggleClass(i.call(this,e,gt(this),t),t)}):this.each(function(){var e,t,n,r;if(a){t=0,n=S(this),r=vt(i);while(e=r[t++])n.hasClass(e)?n.removeClass(e):n.addClass(e)}else void 0!==i&&"boolean"!==o||((e=gt(this))&&Y.set(this,"__className__",e),this.setAttribute&&this.setAttribute("class",e||!1===i?"":Y.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&-1<(" "+ht(gt(n))+" ").indexOf(t))return!0;return!1}});var yt=/\r/g;S.fn.extend({val:function(n){var r,e,i,t=this[0];return arguments.length?(i=m(n),this.each(function(e){var t;1===this.nodeType&&(null==(t=i?n.call(this,e,S(this).val()):n)?t="":"number"==typeof t?t+="":Array.isArray(t)&&(t=S.map(t,function(e){return null==e?"":e+""})),(r=S.valHooks[this.type]||S.valHooks[this.nodeName.toLowerCase()])&&"set"in r&&void 0!==r.set(this,t,"value")||(this.value=t))})):t?(r=S.valHooks[t.type]||S.valHooks[t.nodeName.toLowerCase()])&&"get"in r&&void 0!==(e=r.get(t,"value"))?e:"string"==typeof(e=t.value)?e.replace(yt,""):null==e?"":e:void 0}}),S.extend({valHooks:{option:{get:function(e){var t=S.find.attr(e,"value");return null!=t?t:ht(S.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!A(n.parentNode,"optgroup"))){if(t=S(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=S.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=-1<S.inArray(S.valHooks.option.get(r),o))&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),S.each(["radio","checkbox"],function(){S.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=-1<S.inArray(S(e).val(),t)}},y.checkOn||(S.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),y.focusin="onfocusin"in C;var mt=/^(?:focusinfocus|focusoutblur)$/,xt=function(e){e.stopPropagation()};S.extend(S.event,{trigger:function(e,t,n,r){var i,o,a,s,u,l,c,f,p=[n||E],d=v.call(e,"type")?e.type:e,h=v.call(e,"namespace")?e.namespace.split("."):[];if(o=f=a=n=n||E,3!==n.nodeType&&8!==n.nodeType&&!mt.test(d+S.event.triggered)&&(-1<d.indexOf(".")&&(d=(h=d.split(".")).shift(),h.sort()),u=d.indexOf(":")<0&&"on"+d,(e=e[S.expando]?e:new S.Event(d,"object"==typeof e&&e)).isTrigger=r?2:3,e.namespace=h.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,e.result=void 0,e.target||(e.target=n),t=null==t?[e]:S.makeArray(t,[e]),c=S.event.special[d]||{},r||!c.trigger||!1!==c.trigger.apply(n,t))){if(!r&&!c.noBubble&&!x(n)){for(s=c.delegateType||d,mt.test(s+d)||(o=o.parentNode);o;o=o.parentNode)p.push(o),a=o;a===(n.ownerDocument||E)&&p.push(a.defaultView||a.parentWindow||C)}i=0;while((o=p[i++])&&!e.isPropagationStopped())f=o,e.type=1<i?s:c.bindType||d,(l=(Y.get(o,"events")||Object.create(null))[e.type]&&Y.get(o,"handle"))&&l.apply(o,t),(l=u&&o[u])&&l.apply&&V(o)&&(e.result=l.apply(o,t),!1===e.result&&e.preventDefault());return e.type=d,r||e.isDefaultPrevented()||c._default&&!1!==c._default.apply(p.pop(),t)||!V(n)||u&&m(n[d])&&!x(n)&&((a=n[u])&&(n[u]=null),S.event.triggered=d,e.isPropagationStopped()&&f.addEventListener(d,xt),n[d](),e.isPropagationStopped()&&f.removeEventListener(d,xt),S.event.triggered=void 0,a&&(n[u]=a)),e.result}},simulate:function(e,t,n){var r=S.extend(new S.Event,n,{type:e,isSimulated:!0});S.event.trigger(r,null,t)}}),S.fn.extend({trigger:function(e,t){return this.each(function(){S.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return S.event.trigger(e,t,n,!0)}}),y.focusin||S.each({focus:"focusin",blur:"focusout"},function(n,r){var i=function(e){S.event.simulate(r,e.target,S.event.fix(e))};S.event.special[r]={setup:function(){var e=this.ownerDocument||this.document||this,t=Y.access(e,r);t||e.addEventListener(n,i,!0),Y.access(e,r,(t||0)+1)},teardown:function(){var e=this.ownerDocument||this.document||this,t=Y.access(e,r)-1;t?Y.access(e,r,t):(e.removeEventListener(n,i,!0),Y.remove(e,r))}}});var bt=C.location,wt={guid:Date.now()},Tt=/\?/;S.parseXML=function(e){var t,n;if(!e||"string"!=typeof e)return null;try{t=(new C.DOMParser).parseFromString(e,"text/xml")}catch(e){}return n=t&&t.getElementsByTagName("parsererror")[0],t&&!n||S.error("Invalid XML: "+(n?S.map(n.childNodes,function(e){return e.textContent}).join("\n"):e)),t};var Ct=/\[\]$/,Et=/\r?\n/g,St=/^(?:submit|button|image|reset|file)$/i,kt=/^(?:input|select|textarea|keygen)/i;function At(n,e,r,i){var t;if(Array.isArray(e))S.each(e,function(e,t){r||Ct.test(n)?i(n,t):At(n+"["+("object"==typeof t&&null!=t?e:"")+"]",t,r,i)});else if(r||"object"!==w(e))i(n,e);else for(t in e)At(n+"["+t+"]",e[t],r,i)}S.param=function(e,t){var n,r=[],i=function(e,t){var n=m(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(null==e)return"";if(Array.isArray(e)||e.jquery&&!S.isPlainObject(e))S.each(e,function(){i(this.name,this.value)});else for(n in e)At(n,e[n],t,i);return r.join("&")},S.fn.extend({serialize:function(){return S.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=S.prop(this,"elements");return e?S.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!S(this).is(":disabled")&&kt.test(this.nodeName)&&!St.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=S(this).val();return null==n?null:Array.isArray(n)?S.map(n,function(e){return{name:t.name,value:e.replace(Et,"\r\n")}}):{name:t.name,value:n.replace(Et,"\r\n")}}).get()}});var Nt=/%20/g,jt=/#.*$/,Dt=/([?&])_=[^&]*/,qt=/^(.*?):[ \t]*([^\r\n]*)$/gm,Lt=/^(?:GET|HEAD)$/,Ht=/^\/\//,Ot={},Pt={},Rt="*/".concat("*"),Mt=E.createElement("a");function It(o){return function(e,t){"string"!=typeof e&&(t=e,e="*");var n,r=0,i=e.toLowerCase().match(P)||[];if(m(t))while(n=i[r++])"+"===n[0]?(n=n.slice(1)||"*",(o[n]=o[n]||[]).unshift(t)):(o[n]=o[n]||[]).push(t)}}function Wt(t,i,o,a){var s={},u=t===Pt;function l(e){var r;return s[e]=!0,S.each(t[e]||[],function(e,t){var n=t(i,o,a);return"string"!=typeof n||u||s[n]?u?!(r=n):void 0:(i.dataTypes.unshift(n),l(n),!1)}),r}return l(i.dataTypes[0])||!s["*"]&&l("*")}function Ft(e,t){var n,r,i=S.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&S.extend(!0,e,r),e}Mt.href=bt.href,S.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:bt.href,type:"GET",isLocal:/^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(bt.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Rt,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":S.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?Ft(Ft(e,S.ajaxSettings),t):Ft(S.ajaxSettings,e)},ajaxPrefilter:It(Ot),ajaxTransport:It(Pt),ajax:function(e,t){"object"==typeof e&&(t=e,e=void 0),t=t||{};var c,f,p,n,d,r,h,g,i,o,v=S.ajaxSetup({},t),y=v.context||v,m=v.context&&(y.nodeType||y.jquery)?S(y):S.event,x=S.Deferred(),b=S.Callbacks("once memory"),w=v.statusCode||{},a={},s={},u="canceled",T={readyState:0,getResponseHeader:function(e){var t;if(h){if(!n){n={};while(t=qt.exec(p))n[t[1].toLowerCase()+" "]=(n[t[1].toLowerCase()+" "]||[]).concat(t[2])}t=n[e.toLowerCase()+" "]}return null==t?null:t.join(", ")},getAllResponseHeaders:function(){return h?p:null},setRequestHeader:function(e,t){return null==h&&(e=s[e.toLowerCase()]=s[e.toLowerCase()]||e,a[e]=t),this},overrideMimeType:function(e){return null==h&&(v.mimeType=e),this},statusCode:function(e){var t;if(e)if(h)T.always(e[T.status]);else for(t in e)w[t]=[w[t],e[t]];return this},abort:function(e){var t=e||u;return c&&c.abort(t),l(0,t),this}};if(x.promise(T),v.url=((e||v.url||bt.href)+"").replace(Ht,bt.protocol+"//"),v.type=t.method||t.type||v.method||v.type,v.dataTypes=(v.dataType||"*").toLowerCase().match(P)||[""],null==v.crossDomain){r=E.createElement("a");try{r.href=v.url,r.href=r.href,v.crossDomain=Mt.protocol+"//"+Mt.host!=r.protocol+"//"+r.host}catch(e){v.crossDomain=!0}}if(v.data&&v.processData&&"string"!=typeof v.data&&(v.data=S.param(v.data,v.traditional)),Wt(Ot,v,t,T),h)return T;for(i in(g=S.event&&v.global)&&0==S.active++&&S.event.trigger("ajaxStart"),v.type=v.type.toUpperCase(),v.hasContent=!Lt.test(v.type),f=v.url.replace(jt,""),v.hasContent?v.data&&v.processData&&0===(v.contentType||"").indexOf("application/x-www-form-urlencoded")&&(v.data=v.data.replace(Nt,"+")):(o=v.url.slice(f.length),v.data&&(v.processData||"string"==typeof v.data)&&(f+=(Tt.test(f)?"&":"?")+v.data,delete v.data),!1===v.cache&&(f=f.replace(Dt,"$1"),o=(Tt.test(f)?"&":"?")+"_="+wt.guid+++o),v.url=f+o),v.ifModified&&(S.lastModified[f]&&T.setRequestHeader("If-Modified-Since",S.lastModified[f]),S.etag[f]&&T.setRequestHeader("If-None-Match",S.etag[f])),(v.data&&v.hasContent&&!1!==v.contentType||t.contentType)&&T.setRequestHeader("Content-Type",v.contentType),T.setRequestHeader("Accept",v.dataTypes[0]&&v.accepts[v.dataTypes[0]]?v.accepts[v.dataTypes[0]]+("*"!==v.dataTypes[0]?", "+Rt+"; q=0.01":""):v.accepts["*"]),v.headers)T.setRequestHeader(i,v.headers[i]);if(v.beforeSend&&(!1===v.beforeSend.call(y,T,v)||h))return T.abort();if(u="abort",b.add(v.complete),T.done(v.success),T.fail(v.error),c=Wt(Pt,v,t,T)){if(T.readyState=1,g&&m.trigger("ajaxSend",[T,v]),h)return T;v.async&&0<v.timeout&&(d=C.setTimeout(function(){T.abort("timeout")},v.timeout));try{h=!1,c.send(a,l)}catch(e){if(h)throw e;l(-1,e)}}else l(-1,"No Transport");function l(e,t,n,r){var i,o,a,s,u,l=t;h||(h=!0,d&&C.clearTimeout(d),c=void 0,p=r||"",T.readyState=0<e?4:0,i=200<=e&&e<300||304===e,n&&(s=function(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}(v,T,n)),!i&&-1<S.inArray("script",v.dataTypes)&&S.inArray("json",v.dataTypes)<0&&(v.converters["text script"]=function(){}),s=function(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}(v,s,T,i),i?(v.ifModified&&((u=T.getResponseHeader("Last-Modified"))&&(S.lastModified[f]=u),(u=T.getResponseHeader("etag"))&&(S.etag[f]=u)),204===e||"HEAD"===v.type?l="nocontent":304===e?l="notmodified":(l=s.state,o=s.data,i=!(a=s.error))):(a=l,!e&&l||(l="error",e<0&&(e=0))),T.status=e,T.statusText=(t||l)+"",i?x.resolveWith(y,[o,l,T]):x.rejectWith(y,[T,l,a]),T.statusCode(w),w=void 0,g&&m.trigger(i?"ajaxSuccess":"ajaxError",[T,v,i?o:a]),b.fireWith(y,[T,l]),g&&(m.trigger("ajaxComplete",[T,v]),--S.active||S.event.trigger("ajaxStop")))}return T},getJSON:function(e,t,n){return S.get(e,t,n,"json")},getScript:function(e,t){return S.get(e,void 0,t,"script")}}),S.each(["get","post"],function(e,i){S[i]=function(e,t,n,r){return m(t)&&(r=r||n,n=t,t=void 0),S.ajax(S.extend({url:e,type:i,dataType:r,data:t,success:n},S.isPlainObject(e)&&e))}}),S.ajaxPrefilter(function(e){var t;for(t in e.headers)"content-type"===t.toLowerCase()&&(e.contentType=e.headers[t]||"")}),S._evalUrl=function(e,t,n){return S.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,converters:{"text script":function(){}},dataFilter:function(e){S.globalEval(e,t,n)}})},S.fn.extend({wrapAll:function(e){var t;return this[0]&&(m(e)&&(e=e.call(this[0])),t=S(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(n){return m(n)?this.each(function(e){S(this).wrapInner(n.call(this,e))}):this.each(function(){var e=S(this),t=e.contents();t.length?t.wrapAll(n):e.append(n)})},wrap:function(t){var n=m(t);return this.each(function(e){S(this).wrapAll(n?t.call(this,e):t)})},unwrap:function(e){return this.parent(e).not("body").each(function(){S(this).replaceWith(this.childNodes)}),this}}),S.expr.pseudos.hidden=function(e){return!S.expr.pseudos.visible(e)},S.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},S.ajaxSettings.xhr=function(){try{return new C.XMLHttpRequest}catch(e){}};var Bt={0:200,1223:204},$t=S.ajaxSettings.xhr();y.cors=!!$t&&"withCredentials"in $t,y.ajax=$t=!!$t,S.ajaxTransport(function(i){var o,a;if(y.cors||$t&&!i.crossDomain)return{send:function(e,t){var n,r=i.xhr();if(r.open(i.type,i.url,i.async,i.username,i.password),i.xhrFields)for(n in i.xhrFields)r[n]=i.xhrFields[n];for(n in i.mimeType&&r.overrideMimeType&&r.overrideMimeType(i.mimeType),i.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest"),e)r.setRequestHeader(n,e[n]);o=function(e){return function(){o&&(o=a=r.onload=r.onerror=r.onabort=r.ontimeout=r.onreadystatechange=null,"abort"===e?r.abort():"error"===e?"number"!=typeof r.status?t(0,"error"):t(r.status,r.statusText):t(Bt[r.status]||r.status,r.statusText,"text"!==(r.responseType||"text")||"string"!=typeof r.responseText?{binary:r.response}:{text:r.responseText},r.getAllResponseHeaders()))}},r.onload=o(),a=r.onerror=r.ontimeout=o("error"),void 0!==r.onabort?r.onabort=a:r.onreadystatechange=function(){4===r.readyState&&C.setTimeout(function(){o&&a()})},o=o("abort");try{r.send(i.hasContent&&i.data||null)}catch(e){if(o)throw e}},abort:function(){o&&o()}}}),S.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),S.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return S.globalEval(e),e}}}),S.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),S.ajaxTransport("script",function(n){var r,i;if(n.crossDomain||n.scriptAttrs)return{send:function(e,t){r=S("<script>").attr(n.scriptAttrs||{}).prop({charset:n.scriptCharset,src:n.url}).on("load error",i=function(e){r.remove(),i=null,e&&t("error"===e.type?404:200,e.type)}),E.head.appendChild(r[0])},abort:function(){i&&i()}}});var _t,zt=[],Ut=/(=)\?(?=&|$)|\?\?/;S.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=zt.pop()||S.expando+"_"+wt.guid++;return this[e]=!0,e}}),S.ajaxPrefilter("json jsonp",function(e,t,n){var r,i,o,a=!1!==e.jsonp&&(Ut.test(e.url)?"url":"string"==typeof e.data&&0===(e.contentType||"").indexOf("application/x-www-form-urlencoded")&&Ut.test(e.data)&&"data");if(a||"jsonp"===e.dataTypes[0])return r=e.jsonpCallback=m(e.jsonpCallback)?e.jsonpCallback():e.jsonpCallback,a?e[a]=e[a].replace(Ut,"$1"+r):!1!==e.jsonp&&(e.url+=(Tt.test(e.url)?"&":"?")+e.jsonp+"="+r),e.converters["script json"]=function(){return o||S.error(r+" was not called"),o[0]},e.dataTypes[0]="json",i=C[r],C[r]=function(){o=arguments},n.always(function(){void 0===i?S(C).removeProp(r):C[r]=i,e[r]&&(e.jsonpCallback=t.jsonpCallback,zt.push(r)),o&&m(i)&&i(o[0]),o=i=void 0}),"script"}),y.createHTMLDocument=((_t=E.implementation.createHTMLDocument("").body).innerHTML="<form></form><form></form>",2===_t.childNodes.length),S.parseHTML=function(e,t,n){return"string"!=typeof e?[]:("boolean"==typeof t&&(n=t,t=!1),t||(y.createHTMLDocument?((r=(t=E.implementation.createHTMLDocument("")).createElement("base")).href=E.location.href,t.head.appendChild(r)):t=E),o=!n&&[],(i=N.exec(e))?[t.createElement(i[1])]:(i=xe([e],t,o),o&&o.length&&S(o).remove(),S.merge([],i.childNodes)));var r,i,o},S.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return-1<s&&(r=ht(e.slice(s)),e=e.slice(0,s)),m(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),0<a.length&&S.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?S("<div>").append(S.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},S.expr.pseudos.animated=function(t){return S.grep(S.timers,function(e){return t===e.elem}).length},S.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l=S.css(e,"position"),c=S(e),f={};"static"===l&&(e.style.position="relative"),s=c.offset(),o=S.css(e,"top"),u=S.css(e,"left"),("absolute"===l||"fixed"===l)&&-1<(o+u).indexOf("auto")?(a=(r=c.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),m(t)&&(t=t.call(e,n,S.extend({},s))),null!=t.top&&(f.top=t.top-s.top+a),null!=t.left&&(f.left=t.left-s.left+i),"using"in t?t.using.call(e,f):c.css(f)}},S.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){S.offset.setOffset(this,t,e)});var e,n,r=this[0];return r?r.getClientRects().length?(e=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}:void 0},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===S.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===S.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=S(e).offset()).top+=S.css(e,"borderTopWidth",!0),i.left+=S.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-S.css(r,"marginTop",!0),left:t.left-i.left-S.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===S.css(e,"position"))e=e.offsetParent;return e||re})}}),S.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,i){var o="pageYOffset"===i;S.fn[t]=function(e){return $(this,function(e,t,n){var r;if(x(e)?r=e:9===e.nodeType&&(r=e.defaultView),void 0===n)return r?r[i]:e[t];r?r.scrollTo(o?r.pageXOffset:n,o?n:r.pageYOffset):e[t]=n},t,e,arguments.length)}}),S.each(["top","left"],function(e,n){S.cssHooks[n]=Fe(y.pixelPosition,function(e,t){if(t)return t=We(e,n),Pe.test(t)?S(e).position()[n]+"px":t})}),S.each({Height:"height",Width:"width"},function(a,s){S.each({padding:"inner"+a,content:s,"":"outer"+a},function(r,o){S.fn[o]=function(e,t){var n=arguments.length&&(r||"boolean"!=typeof e),i=r||(!0===e||!0===t?"margin":"border");return $(this,function(e,t,n){var r;return x(e)?0===o.indexOf("outer")?e["inner"+a]:e.document.documentElement["client"+a]:9===e.nodeType?(r=e.documentElement,Math.max(e.body["scroll"+a],r["scroll"+a],e.body["offset"+a],r["offset"+a],r["client"+a])):void 0===n?S.css(e,t,i):S.style(e,t,n,i)},s,n?e:void 0,n)}})}),S.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){S.fn[t]=function(e){return this.on(t,e)}}),S.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)},hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,n){S.fn[n]=function(e,t){return 0<arguments.length?this.on(n,null,e,t):this.trigger(n)}});var Xt=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;S.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),m(e))return r=s.call(arguments,2),(i=function(){return e.apply(t||this,r.concat(s.call(arguments)))}).guid=e.guid=e.guid||S.guid++,i},S.holdReady=function(e){e?S.readyWait++:S.ready(!0)},S.isArray=Array.isArray,S.parseJSON=JSON.parse,S.nodeName=A,S.isFunction=m,S.isWindow=x,S.camelCase=X,S.type=w,S.now=Date.now,S.isNumeric=function(e){var t=S.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},S.trim=function(e){return null==e?"":(e+"").replace(Xt,"")},"function"==typeof define&&define.amd&&define("jquery",[],function(){return S});var Vt=C.jQuery,Gt=C.$;return S.noConflict=function(e){return C.$===S&&(C.$=Gt),e&&C.jQuery===S&&(C.jQuery=Vt),S},"undefined"==typeof e&&(C.jQuery=C.$=S),S});

    

    /**

     *

     * LITEPICKER LIB

     *

     */

    var Litepicker=function(t){var e={};function i(n){if(e[n])return e[n].exports;var o=e[n]={i:n,l:!1,exports:{}};return t[n].call(o.exports,o,o.exports,i),o.l=!0,o.exports}return i.m=t,i.c=e,i.d=function(t,e,n){i.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},i.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},i.t=function(t,e){if(1&e&&(t=i(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(i.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var o in t)i.d(n,o,function(e){return t[e]}.bind(null,o));return n},i.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return i.d(e,"a",e),e},i.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},i.p="",i(i.s=4)}([function(t,e,i){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var n=function(){function t(e,i,n){void 0===e&&(e=null),void 0===i&&(i=null),void 0===n&&(n="en-US"),this.dateInstance="object"==typeof i&&null!==i?i.parse(e instanceof t?e.clone().toJSDate():e):"string"==typeof i?t.parseDateTime(e,i,n):e?t.parseDateTime(e):t.parseDateTime(new Date),this.lang=n}return t.parseDateTime=function(e,i,n){if(void 0===i&&(i="YYYY-MM-DD"),void 0===n&&(n="en-US"),!e)return new Date(NaN);if(e instanceof Date)return new Date(e);if(e instanceof t)return e.clone().toJSDate();if(/^-?\d{10,}$/.test(e))return t.getDateZeroTime(new Date(Number(e)));if("string"==typeof e){for(var o=[],s=null;null!=(s=t.regex.exec(i));)"\\"!==s[1]&&o.push(s);if(o.length){var r={year:null,month:null,shortMonth:null,longMonth:null,day:null,value:""};o[0].index>0&&(r.value+=".*?");for(var a=0,l=Object.entries(o);a<l.length;a++){var c=l[a],h=c[0],p=c[1],d=Number(h),u=t.formatPatterns(p[0],n),m=u.group,f=u.pattern;r[m]=d+1,r.value+=f,r.value+=".*?"}var g=new RegExp("^"+r.value+"$");if(g.test(e)){var v=g.exec(e),y=Number(v[r.year]),b=null;r.month?b=Number(v[r.month])-1:r.shortMonth?b=t.shortMonths(n).indexOf(v[r.shortMonth]):r.longMonth&&(b=t.longMonths(n).indexOf(v[r.longMonth]));var k=Number(v[r.day])||1;return new Date(y,b,k,0,0,0,0)}}}return t.getDateZeroTime(new Date(e))},t.convertArray=function(e,i){return e.map((function(e){return e instanceof Array?e.map((function(e){return new t(e,i)})):new t(e,i)}))},t.getDateZeroTime=function(t){return new Date(t.getFullYear(),t.getMonth(),t.getDate(),0,0,0,0)},t.shortMonths=function(e){return t.MONTH_JS.map((function(t){return new Date(2019,t).toLocaleString(e,{month:"short"})}))},t.longMonths=function(e){return t.MONTH_JS.map((function(t){return new Date(2019,t).toLocaleString(e,{month:"long"})}))},t.formatPatterns=function(e,i){switch(e){case"YY":case"YYYY":return{group:"year",pattern:"(\\d{"+e.length+"})"};case"M":return{group:"month",pattern:"(\\d{1,2})"};case"MM":return{group:"month",pattern:"(\\d{2})"};case"MMM":return{group:"shortMonth",pattern:"("+t.shortMonths(i).join("|")+")"};case"MMMM":return{group:"longMonth",pattern:"("+t.longMonths(i).join("|")+")"};case"D":return{group:"day",pattern:"(\\d{1,2})"};case"DD":return{group:"day",pattern:"(\\d{2})"}}},t.prototype.toJSDate=function(){return this.dateInstance},t.prototype.toLocaleString=function(t,e){return this.dateInstance.toLocaleString(t,e)},t.prototype.toDateString=function(){return this.dateInstance.toDateString()},t.prototype.getSeconds=function(){return this.dateInstance.getSeconds()},t.prototype.getDay=function(){return this.dateInstance.getDay()},t.prototype.getTime=function(){return this.dateInstance.getTime()},t.prototype.getDate=function(){return this.dateInstance.getDate()},t.prototype.getMonth=function(){return this.dateInstance.getMonth()},t.prototype.getFullYear=function(){return this.dateInstance.getFullYear()},t.prototype.setMonth=function(t){return this.dateInstance.setMonth(t)},t.prototype.setHours=function(t,e,i,n){void 0===t&&(t=0),void 0===e&&(e=0),void 0===i&&(i=0),void 0===n&&(n=0),this.dateInstance.setHours(t,e,i,n)},t.prototype.setSeconds=function(t){return this.dateInstance.setSeconds(t)},t.prototype.setDate=function(t){return this.dateInstance.setDate(t)},t.prototype.setFullYear=function(t){return this.dateInstance.setFullYear(t)},t.prototype.getWeek=function(t){var e=new Date(this.timestamp()),i=(this.getDay()+(7-t))%7;e.setDate(e.getDate()-i);var n=e.getTime();return e.setMonth(0,1),e.getDay()!==t&&e.setMonth(0,1+(4-e.getDay()+7)%7),1+Math.ceil((n-e.getTime())/6048e5)},t.prototype.clone=function(){return new t(this.toJSDate())},t.prototype.isBetween=function(t,e,i){switch(void 0===i&&(i="()"),i){default:case"()":return this.timestamp()>t.getTime()&&this.timestamp()<e.getTime();case"[)":return this.timestamp()>=t.getTime()&&this.timestamp()<e.getTime();case"(]":return this.timestamp()>t.getTime()&&this.timestamp()<=e.getTime();case"[]":return this.timestamp()>=t.getTime()&&this.timestamp()<=e.getTime()}},t.prototype.isBefore=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":return t.getTime()>this.getTime();case"day":case"days":return new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime()>new Date(this.getFullYear(),this.getMonth(),this.getDate()).getTime();case"month":case"months":return new Date(t.getFullYear(),t.getMonth(),1).getTime()>new Date(this.getFullYear(),this.getMonth(),1).getTime();case"year":case"years":return t.getFullYear()>this.getFullYear()}throw new Error("isBefore: Invalid unit!")},t.prototype.isSameOrBefore=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":return t.getTime()>=this.getTime();case"day":case"days":return new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime()>=new Date(this.getFullYear(),this.getMonth(),this.getDate()).getTime();case"month":case"months":return new Date(t.getFullYear(),t.getMonth(),1).getTime()>=new Date(this.getFullYear(),this.getMonth(),1).getTime()}throw new Error("isSameOrBefore: Invalid unit!")},t.prototype.isAfter=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":return this.getTime()>t.getTime();case"day":case"days":return new Date(this.getFullYear(),this.getMonth(),this.getDate()).getTime()>new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime();case"month":case"months":return new Date(this.getFullYear(),this.getMonth(),1).getTime()>new Date(t.getFullYear(),t.getMonth(),1).getTime();case"year":case"years":return this.getFullYear()>t.getFullYear()}throw new Error("isAfter: Invalid unit!")},t.prototype.isSameOrAfter=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":return this.getTime()>=t.getTime();case"day":case"days":return new Date(this.getFullYear(),this.getMonth(),this.getDate()).getTime()>=new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime();case"month":case"months":return new Date(this.getFullYear(),this.getMonth(),1).getTime()>=new Date(t.getFullYear(),t.getMonth(),1).getTime()}throw new Error("isSameOrAfter: Invalid unit!")},t.prototype.isSame=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":return this.getTime()===t.getTime();case"day":case"days":return new Date(this.getFullYear(),this.getMonth(),this.getDate()).getTime()===new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime();case"month":case"months":return new Date(this.getFullYear(),this.getMonth(),1).getTime()===new Date(t.getFullYear(),t.getMonth(),1).getTime()}throw new Error("isSame: Invalid unit!")},t.prototype.add=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":this.setSeconds(this.getSeconds()+t);break;case"day":case"days":this.setDate(this.getDate()+t);break;case"month":case"months":this.setMonth(this.getMonth()+t)}return this},t.prototype.subtract=function(t,e){switch(void 0===e&&(e="seconds"),e){case"second":case"seconds":this.setSeconds(this.getSeconds()-t);break;case"day":case"days":this.setDate(this.getDate()-t);break;case"month":case"months":this.setMonth(this.getMonth()-t)}return this},t.prototype.diff=function(t,e){void 0===e&&(e="seconds");switch(e){default:case"second":case"seconds":return this.getTime()-t.getTime();case"day":case"days":return Math.round((this.timestamp()-t.getTime())/864e5);case"month":case"months":}},t.prototype.format=function(e,i){if(void 0===i&&(i="en-US"),"object"==typeof e)return e.output(this.clone().toJSDate());for(var n="",o=[],s=null;null!=(s=t.regex.exec(e));)"\\"!==s[1]&&o.push(s);if(o.length){o[0].index>0&&(n+=e.substring(0,o[0].index));for(var r=0,a=Object.entries(o);r<a.length;r++){var l=a[r],c=l[0],h=l[1],p=Number(c);n+=this.formatTokens(h[0],i),o[p+1]&&(n+=e.substring(h.index+h[0].length,o[p+1].index)),p===o.length-1&&(n+=e.substring(h.index+h[0].length))}}return n.replace(/\\/g,"")},t.prototype.timestamp=function(){return new Date(this.getFullYear(),this.getMonth(),this.getDate(),0,0,0,0).getTime()},t.prototype.formatTokens=function(e,i){switch(e){case"YY":return String(this.getFullYear()).slice(-2);case"YYYY":return String(this.getFullYear());case"M":return String(this.getMonth()+1);case"MM":return("0"+(this.getMonth()+1)).slice(-2);case"MMM":return t.shortMonths(i)[this.getMonth()];case"MMMM":return t.longMonths(i)[this.getMonth()];case"D":return String(this.getDate());case"DD":return("0"+this.getDate()).slice(-2);default:return""}},t.regex=/(\\)?(Y{2,4}|M{1,4}|D{1,2}|d{1,4})/g,t.MONTH_JS=[0,1,2,3,4,5,6,7,8,9,10,11],t}();e.DateTime=n},function(t,e,i){"use strict";var n,o=this&&this.__extends||(n=function(t,e){return(n=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(t,e){t.__proto__=e}||function(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i])})(t,e)},function(t,e){function i(){this.constructor=t}n(t,e),t.prototype=null===e?Object.create(e):(i.prototype=e.prototype,new i)}),s=this&&this.__spreadArrays||function(){for(var t=0,e=0,i=arguments.length;e<i;e++)t+=arguments[e].length;var n=Array(t),o=0;for(e=0;e<i;e++)for(var s=arguments[e],r=0,a=s.length;r<a;r++,o++)n[o]=s[r];return n};Object.defineProperty(e,"__esModule",{value:!0});var r=i(5),a=i(0),l=i(3),c=i(2),h=function(t){function e(e){var i=t.call(this,e)||this;return i.preventClick=!1,i.bindEvents(),i}return o(e,t),e.prototype.scrollToDate=function(t){if(this.options.scrollToDate){var e=this.options.startDate instanceof a.DateTime?this.options.startDate.clone():null,i=this.options.endDate instanceof a.DateTime?this.options.endDate.clone():null;!this.options.startDate||t&&t!==this.options.element?t&&this.options.endDate&&t===this.options.elementEnd&&(i.setDate(1),this.options.numberOfMonths>1&&i.isAfter(e)&&i.setMonth(i.getMonth()-(this.options.numberOfMonths-1)),this.calendars[0]=i.clone()):(e.setDate(1),this.calendars[0]=e.clone())}},e.prototype.bindEvents=function(){document.addEventListener("click",this.onClick.bind(this),!0),this.ui=document.createElement("div"),this.ui.className=l.litepicker,this.ui.style.display="none",this.ui.addEventListener("mouseenter",this.onMouseEnter.bind(this),!0),this.ui.addEventListener("mouseleave",this.onMouseLeave.bind(this),!1),this.options.autoRefresh?(this.options.element instanceof HTMLElement&&this.options.element.addEventListener("keyup",this.onInput.bind(this),!0),this.options.elementEnd instanceof HTMLElement&&this.options.elementEnd.addEventListener("keyup",this.onInput.bind(this),!0)):(this.options.element instanceof HTMLElement&&this.options.element.addEventListener("change",this.onInput.bind(this),!0),this.options.elementEnd instanceof HTMLElement&&this.options.elementEnd.addEventListener("change",this.onInput.bind(this),!0)),this.options.parentEl?this.options.parentEl instanceof HTMLElement?this.options.parentEl.appendChild(this.ui):document.querySelector(this.options.parentEl).appendChild(this.ui):this.options.inlineMode?this.options.element instanceof HTMLInputElement?this.options.element.parentNode.appendChild(this.ui):this.options.element.appendChild(this.ui):document.body.appendChild(this.ui),this.updateInput(),this.init(),"function"==typeof this.options.setup&&this.options.setup.call(this,this),this.render(),this.options.inlineMode&&this.show()},e.prototype.updateInput=function(){if(this.options.element instanceof HTMLInputElement){var t=this.options.startDate,e=this.options.endDate;if(this.options.singleMode&&t)this.options.element.value=t.format(this.options.format,this.options.lang);else if(!this.options.singleMode&&t&&e){var i=t.format(this.options.format,this.options.lang),n=e.format(this.options.format,this.options.lang);this.options.elementEnd instanceof HTMLInputElement?(this.options.element.value=i,this.options.elementEnd.value=n):this.options.element.value=""+i+this.options.delimiter+n}t||e||(this.options.element.value="",this.options.elementEnd instanceof HTMLInputElement&&(this.options.elementEnd.value=""))}},e.prototype.isSamePicker=function(t){return t.closest("."+l.litepicker)===this.ui},e.prototype.shouldShown=function(t){return!t.disabled&&(t===this.options.element||this.options.elementEnd&&t===this.options.elementEnd)},e.prototype.shouldResetDatePicked=function(){return this.options.singleMode||2===this.datePicked.length},e.prototype.shouldSwapDatePicked=function(){return 2===this.datePicked.length&&this.datePicked[0].getTime()>this.datePicked[1].getTime()},e.prototype.shouldCheckLockDays=function(){return this.options.disallowLockDaysInRange&&2===this.datePicked.length},e.prototype.onClick=function(t){var e=t.target;if(t.target.shadowRoot&&(e=t.composedPath()[0]),e&&this.ui)if(this.shouldShown(e))this.show(e);else if(e.closest("."+l.litepicker)||!this.isShowning()){if(this.isSamePicker(e))if(this.emit("before:click",e),this.preventClick)this.preventClick=!1;else{if(e.classList.contains(l.dayItem)){if(t.preventDefault(),e.classList.contains(l.isLocked))return;if(this.shouldResetDatePicked()&&(this.datePicked.length=0),this.datePicked[this.datePicked.length]=new a.DateTime(e.dataset.time),this.shouldSwapDatePicked()){var i=this.datePicked[1].clone();this.datePicked[1]=this.datePicked[0].clone(),this.datePicked[0]=i.clone()}if(this.shouldCheckLockDays())c.rangeIsLocked(this.datePicked,this.options)&&(this.emit("error:range",this.datePicked),this.datePicked.length=0);return this.render(),this.emit.apply(this,s(["preselect"],s(this.datePicked).map((function(t){return t.clone()})))),void(this.options.autoApply&&(this.options.singleMode&&this.datePicked.length?(this.setDate(this.datePicked[0]),this.hide()):this.options.singleMode||2!==this.datePicked.length||(this.setDateRange(this.datePicked[0],this.datePicked[1]),this.hide())))}if(e.classList.contains(l.buttonPreviousMonth)){t.preventDefault();var n=0,o=this.options.switchingMonths||this.options.numberOfMonths;if(this.options.splitView){var r=e.closest("."+l.monthItem);n=c.findNestedMonthItem(r),o=1}return this.calendars[n].setMonth(this.calendars[n].getMonth()-o),this.gotoDate(this.calendars[n],n),void this.emit("change:month",this.calendars[n],n)}if(e.classList.contains(l.buttonNextMonth)){t.preventDefault();n=0,o=this.options.switchingMonths||this.options.numberOfMonths;if(this.options.splitView){r=e.closest("."+l.monthItem);n=c.findNestedMonthItem(r),o=1}return this.calendars[n].setMonth(this.calendars[n].getMonth()+o),this.gotoDate(this.calendars[n],n),void this.emit("change:month",this.calendars[n],n)}e.classList.contains(l.buttonCancel)&&(t.preventDefault(),this.hide(),this.emit("button:cancel")),e.classList.contains(l.buttonApply)&&(t.preventDefault(),this.options.singleMode&&this.datePicked.length?this.setDate(this.datePicked[0]):this.options.singleMode||2!==this.datePicked.length||this.setDateRange(this.datePicked[0],this.datePicked[1]),this.hide(),this.emit("button:apply",this.options.startDate,this.options.endDate))}}else this.hide()},e.prototype.showTooltip=function(t,e){var i=this.ui.querySelector("."+l.containerTooltip);i.style.visibility="visible",i.innerHTML=e;var n=this.ui.getBoundingClientRect(),o=i.getBoundingClientRect(),s=t.getBoundingClientRect(),r=s.top,a=s.left;if(this.options.inlineMode&&this.options.parentEl){var c=this.ui.parentNode.getBoundingClientRect();r-=c.top,a-=c.left}else r-=n.top,a-=n.left;r-=o.height,a-=o.width/2,a+=s.width/2,i.style.top=r+"px",i.style.left=a+"px",this.emit("tooltip",i,t)},e.prototype.hideTooltip=function(){this.ui.querySelector("."+l.containerTooltip).style.visibility="hidden"},e.prototype.shouldAllowMouseEnter=function(t){return!this.options.singleMode&&!t.classList.contains(l.isLocked)},e.prototype.shouldAllowRepick=function(){return this.options.elementEnd&&this.options.allowRepick&&this.options.startDate&&this.options.endDate},e.prototype.isDayItem=function(t){return t.classList.contains(l.dayItem)},e.prototype.onMouseEnter=function(t){var e=this,i=t.target;if(this.isDayItem(i)&&this.shouldAllowMouseEnter(i)){if(this.shouldAllowRepick()&&(this.triggerElement===this.options.element?this.datePicked[0]=this.options.endDate.clone():this.triggerElement===this.options.elementEnd&&(this.datePicked[0]=this.options.startDate.clone())),1!==this.datePicked.length)return;var n=this.ui.querySelector("."+l.dayItem+'[data-time="'+this.datePicked[0].getTime()+'"]'),o=this.datePicked[0].clone(),s=new a.DateTime(i.dataset.time),r=!1;if(o.getTime()>s.getTime()){var c=o.clone();o=s.clone(),s=c.clone(),r=!0}if(Array.prototype.slice.call(this.ui.querySelectorAll("."+l.dayItem)).forEach((function(t){var i=new a.DateTime(t.dataset.time),n=e.renderDay(i);i.isBetween(o,s)&&n.classList.add(l.isInRange),t.className=n.className})),i.classList.add(l.isEndDate),r?(n&&n.classList.add(l.isFlipped),i.classList.add(l.isFlipped)):(n&&n.classList.remove(l.isFlipped),i.classList.remove(l.isFlipped)),this.options.showTooltip){var h=s.diff(o,"day")+1;if("function"==typeof this.options.tooltipNumber&&(h=this.options.tooltipNumber.call(this,h)),h>0){var p=this.pluralSelector(h),d=h+" "+(this.options.tooltipText[p]?this.options.tooltipText[p]:"["+p+"]");this.showTooltip(i,d);var u=window.navigator.userAgent,m=/(iphone|ipad)/i.test(u),f=/OS 1([0-2])/i.test(u);m&&f&&i.dispatchEvent(new Event("click"))}else this.hideTooltip()}}},e.prototype.onMouseLeave=function(t){t.target;this.options.allowRepick&&(!this.options.allowRepick||this.options.startDate||this.options.endDate)&&(this.datePicked.length=0,this.render())},e.prototype.onInput=function(t){var e=this.parseInput(),i=e[0],n=e[1],o=this.options.format;if(this.options.elementEnd?i instanceof a.DateTime&&n instanceof a.DateTime&&i.format(o)===this.options.element.value&&n.format(o)===this.options.elementEnd.value:this.options.singleMode?i instanceof a.DateTime&&i.format(o)===this.options.element.value:i instanceof a.DateTime&&n instanceof a.DateTime&&""+i.format(o)+this.options.delimiter+n.format(o)===this.options.element.value){if(n&&i.getTime()>n.getTime()){var s=i.clone();i=n.clone(),n=s.clone()}this.options.startDate=new a.DateTime(i,this.options.format,this.options.lang),n&&(this.options.endDate=new a.DateTime(n,this.options.format,this.options.lang)),this.updateInput(),this.render();var r=i.clone(),l=0;(this.options.elementEnd?i.format(o)===t.target.value:t.target.value.startsWith(i.format(o)))||(r=n.clone(),l=this.options.numberOfMonths-1),this.emit("selected",this.getStartDate(),this.getEndDate()),this.gotoDate(r,l)}},e}(r.Calendar);e.Litepicker=h},function(t,e,i){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.findNestedMonthItem=function(t){for(var e=t.parentNode.childNodes,i=0;i<e.length;i+=1){if(e.item(i)===t)return i}return 0},e.dateIsLocked=function(t,e,i){var n=!1;return e.lockDays.length&&(n=e.lockDays.filter((function(i){return i instanceof Array?t.isBetween(i[0],i[1],e.lockDaysInclusivity):i.isSame(t,"day")})).length),n||"function"!=typeof e.lockDaysFilter||(n=e.lockDaysFilter.call(this,t.clone(),null,i)),n},e.rangeIsLocked=function(t,e){var i=!1;return e.lockDays.length&&(i=e.lockDays.filter((function(i){if(i instanceof Array){var n=t[0].toDateString()===i[0].toDateString()&&t[1].toDateString()===i[1].toDateString();return i[0].isBetween(t[0],t[1],e.lockDaysInclusivity)||i[1].isBetween(t[0],t[1],e.lockDaysInclusivity)||n}return i.isBetween(t[0],t[1],e.lockDaysInclusivity)})).length),i||"function"!=typeof e.lockDaysFilter||(i=e.lockDaysFilter.call(this,t[0].clone(),t[1].clone(),t)),i}},function(t,e,i){var n=i(8);"string"==typeof n&&(n=[[t.i,n,""]]);var o={insert:function(t){var e=document.querySelector("head"),i=window._lastElementInsertedByStyleLoader;window.disableLitepickerStyles||(i?i.nextSibling?e.insertBefore(t,i.nextSibling):e.appendChild(t):e.insertBefore(t,e.firstChild),window._lastElementInsertedByStyleLoader=t)},singleton:!1};i(10)(n,o);n.locals&&(t.exports=n.locals)},function(t,e,i){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var n=i(1);e.Litepicker=n.Litepicker,i(11),window.Litepicker=n.Litepicker,e.default=n.Litepicker},function(t,e,i){"use strict";var n,o=this&&this.__extends||(n=function(t,e){return(n=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(t,e){t.__proto__=e}||function(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i])})(t,e)},function(t,e){function i(){this.constructor=t}n(t,e),t.prototype=null===e?Object.create(e):(i.prototype=e.prototype,new i)});Object.defineProperty(e,"__esModule",{value:!0});var s=i(6),r=i(0),a=i(3),l=i(2),c=function(t){function e(e){return t.call(this,e)||this}return o(e,t),e.prototype.render=function(){var t=this;this.emit("before:render",this.ui);var e=document.createElement("div");e.className=a.containerMain;var i=document.createElement("div");i.className=a.containerMonths,a["columns"+this.options.numberOfColumns]&&(i.classList.remove(a.columns2,a.columns3,a.columns4),i.classList.add(a["columns"+this.options.numberOfColumns])),this.options.splitView&&i.classList.add(a.splitView),this.options.showWeekNumbers&&i.classList.add(a.showWeekNumbers);for(var n=this.calendars[0].clone(),o=n.getMonth(),s=n.getMonth()+this.options.numberOfMonths,r=0,l=o;l<s;l+=1){var c=n.clone();c.setDate(1),c.setHours(0,0,0,0),this.options.splitView?c=this.calendars[r].clone():c.setMonth(l),i.appendChild(this.renderMonth(c,r)),r+=1}if(this.ui.innerHTML="",e.appendChild(i),this.options.resetButton){var h=void 0;"function"==typeof this.options.resetButton?h=this.options.resetButton.call(this):((h=document.createElement("button")).type="button",h.className=a.resetButton,h.innerHTML=this.options.buttonText.reset),h.addEventListener("click",(function(e){e.preventDefault(),t.clearSelection()})),e.querySelector("."+a.monthItem+":last-child").querySelector("."+a.monthItemHeader).appendChild(h)}this.ui.appendChild(e),this.options.autoApply&&!this.options.footerHTML||this.ui.appendChild(this.renderFooter()),this.options.showTooltip&&this.ui.appendChild(this.renderTooltip()),this.ui.dataset.plugins=(this.options.plugins||[]).join("|"),this.emit("render",this.ui)},e.prototype.renderMonth=function(t,e){var i=this,n=t.clone(),o=32-new Date(n.getFullYear(),n.getMonth(),32).getDate(),s=document.createElement("div");s.className=a.monthItem;var c=document.createElement("div");c.className=a.monthItemHeader;var h=document.createElement("div");if(this.options.dropdowns.months){var p=document.createElement("select");p.className=a.monthItemName;for(var d=0;d<12;d+=1){var u=document.createElement("option"),m=new r.DateTime(new Date(t.getFullYear(),d,2,0,0,0)),f=new r.DateTime(new Date(t.getFullYear(),d,1,0,0,0));u.value=String(d),u.text=m.toLocaleString(this.options.lang,{month:"long"}),u.disabled=this.options.minDate&&f.isBefore(new r.DateTime(this.options.minDate),"month")||this.options.maxDate&&f.isAfter(new r.DateTime(this.options.maxDate),"month"),u.selected=f.getMonth()===t.getMonth(),p.appendChild(u)}p.addEventListener("change",(function(t){var e=t.target,n=0;if(i.options.splitView){var o=e.closest("."+a.monthItem);n=l.findNestedMonthItem(o)}i.calendars[n].setMonth(Number(e.value)),i.render(),i.emit("change:month",i.calendars[n],n,t)})),h.appendChild(p)}else{(m=document.createElement("strong")).className=a.monthItemName,m.innerHTML=t.toLocaleString(this.options.lang,{month:"long"}),h.appendChild(m)}if(this.options.dropdowns.years){var g=document.createElement("select");g.className=a.monthItemYear;var v=this.options.dropdowns.minYear,y=this.options.dropdowns.maxYear?this.options.dropdowns.maxYear:(new Date).getFullYear();if(t.getFullYear()>y)(u=document.createElement("option")).value=String(t.getFullYear()),u.text=String(t.getFullYear()),u.selected=!0,u.disabled=!0,g.appendChild(u);for(d=y;d>=v;d-=1){var u=document.createElement("option"),b=new r.DateTime(new Date(d,0,1,0,0,0));u.value=String(d),u.text=String(d),u.disabled=this.options.minDate&&b.isBefore(new r.DateTime(this.options.minDate),"year")||this.options.maxDate&&b.isAfter(new r.DateTime(this.options.maxDate),"year"),u.selected=t.getFullYear()===d,g.appendChild(u)}if(t.getFullYear()<v)(u=document.createElement("option")).value=String(t.getFullYear()),u.text=String(t.getFullYear()),u.selected=!0,u.disabled=!0,g.appendChild(u);if("asc"===this.options.dropdowns.years){var k=Array.prototype.slice.call(g.childNodes).reverse();g.innerHTML="",k.forEach((function(t){t.innerHTML=t.value,g.appendChild(t)}))}g.addEventListener("change",(function(t){var e=t.target,n=0;if(i.options.splitView){var o=e.closest("."+a.monthItem);n=l.findNestedMonthItem(o)}i.calendars[n].setFullYear(Number(e.value)),i.render(),i.emit("change:year",i.calendars[n],n,t)})),h.appendChild(g)}else{var D=document.createElement("span");D.className=a.monthItemYear,D.innerHTML=String(t.getFullYear()),h.appendChild(D)}var w=document.createElement("button");w.type="button",w.className=a.buttonPreviousMonth,w.innerHTML=this.options.buttonText.previousMonth;var x=document.createElement("button");x.type="button",x.className=a.buttonNextMonth,x.innerHTML=this.options.buttonText.nextMonth,c.appendChild(w),c.appendChild(h),c.appendChild(x),this.options.minDate&&n.isSameOrBefore(new r.DateTime(this.options.minDate),"month")&&s.classList.add(a.noPreviousMonth),this.options.maxDate&&n.isSameOrAfter(new r.DateTime(this.options.maxDate),"month")&&s.classList.add(a.noNextMonth);var M=document.createElement("div");M.className=a.monthItemWeekdaysRow,this.options.showWeekNumbers&&(M.innerHTML="<div>W</div>");for(var _=1;_<=7;_+=1){var T=3+this.options.firstDay+_,L=document.createElement("div");L.innerHTML=this.weekdayName(T),L.title=this.weekdayName(T,"long"),M.appendChild(L)}var E=document.createElement("div");E.className=a.containerDays;var S=this.calcSkipDays(n);this.options.showWeekNumbers&&S&&E.appendChild(this.renderWeekNumber(n));for(var I=0;I<S;I+=1){var P=document.createElement("div");E.appendChild(P)}for(I=1;I<=o;I+=1)n.setDate(I),this.options.showWeekNumbers&&n.getDay()===this.options.firstDay&&E.appendChild(this.renderWeekNumber(n)),E.appendChild(this.renderDay(n));return s.appendChild(c),s.appendChild(M),s.appendChild(E),this.emit("render:month",s,t),s},e.prototype.renderDay=function(t){t.setHours();var e=document.createElement("div");if(e.className=a.dayItem,e.innerHTML=String(t.getDate()),e.dataset.time=String(t.getTime()),t.toDateString()===(new Date).toDateString()&&e.classList.add(a.isToday),this.datePicked.length)this.datePicked[0].toDateString()===t.toDateString()&&(e.classList.add(a.isStartDate),this.options.singleMode&&e.classList.add(a.isEndDate)),2===this.datePicked.length&&this.datePicked[1].toDateString()===t.toDateString()&&e.classList.add(a.isEndDate),2===this.datePicked.length&&t.isBetween(this.datePicked[0],this.datePicked[1])&&e.classList.add(a.isInRange);else if(this.options.startDate){var i=this.options.startDate,n=this.options.endDate;i.toDateString()===t.toDateString()&&(e.classList.add(a.isStartDate),this.options.singleMode&&e.classList.add(a.isEndDate)),n&&n.toDateString()===t.toDateString()&&e.classList.add(a.isEndDate),i&&n&&t.isBetween(i,n)&&e.classList.add(a.isInRange)}if(this.options.minDate&&t.isBefore(new r.DateTime(this.options.minDate))&&e.classList.add(a.isLocked),this.options.maxDate&&t.isAfter(new r.DateTime(this.options.maxDate))&&e.classList.add(a.isLocked),this.options.minDays>1&&1===this.datePicked.length){var o=this.options.minDays-1,s=this.datePicked[0].clone().subtract(o,"day"),c=this.datePicked[0].clone().add(o,"day");t.isBetween(s,this.datePicked[0],"(]")&&e.classList.add(a.isLocked),t.isBetween(this.datePicked[0],c,"[)")&&e.classList.add(a.isLocked)}if(this.options.maxDays&&1===this.datePicked.length){var h=this.options.maxDays;s=this.datePicked[0].clone().subtract(h,"day"),c=this.datePicked[0].clone().add(h,"day");t.isSameOrBefore(s)&&e.classList.add(a.isLocked),t.isSameOrAfter(c)&&e.classList.add(a.isLocked)}(this.options.selectForward&&1===this.datePicked.length&&t.isBefore(this.datePicked[0])&&e.classList.add(a.isLocked),this.options.selectBackward&&1===this.datePicked.length&&t.isAfter(this.datePicked[0])&&e.classList.add(a.isLocked),l.dateIsLocked(t,this.options,this.datePicked)&&e.classList.add(a.isLocked),this.options.highlightedDays.length)&&(this.options.highlightedDays.filter((function(e){return e instanceof Array?t.isBetween(e[0],e[1],"[]"):e.isSame(t,"day")})).length&&e.classList.add(a.isHighlighted));return e.tabIndex=e.classList.contains("is-locked")?-1:0,this.emit("render:day",e,t),e},e.prototype.renderFooter=function(){var t=document.createElement("div");if(t.className=a.containerFooter,this.options.footerHTML?t.innerHTML=this.options.footerHTML:t.innerHTML='\n      <span class="'+a.previewDateRange+'"></span>\n      <button type="button" class="'+a.buttonCancel+'">'+this.options.buttonText.cancel+'</button>\n      <button type="button" class="'+a.buttonApply+'">'+this.options.buttonText.apply+"</button>\n      ",this.options.singleMode){if(1===this.datePicked.length){var e=this.datePicked[0].format(this.options.format,this.options.lang);t.querySelector("."+a.previewDateRange).innerHTML=e}}else if(1===this.datePicked.length&&t.querySelector("."+a.buttonApply).setAttribute("disabled",""),2===this.datePicked.length){e=this.datePicked[0].format(this.options.format,this.options.lang);var i=this.datePicked[1].format(this.options.format,this.options.lang);t.querySelector("."+a.previewDateRange).innerHTML=""+e+this.options.delimiter+i}return this.emit("render:footer",t),t},e.prototype.renderWeekNumber=function(t){var e=document.createElement("div"),i=t.getWeek(this.options.firstDay);return e.className=a.weekNumber,e.innerHTML=53===i&&0===t.getMonth()?"53 / 1":i,e},e.prototype.renderTooltip=function(){var t=document.createElement("div");return t.className=a.containerTooltip,t},e.prototype.weekdayName=function(t,e){return void 0===e&&(e="short"),new Date(1970,0,t,12,0,0,0).toLocaleString(this.options.lang,{weekday:e})},e.prototype.calcSkipDays=function(t){var e=t.getDay()-this.options.firstDay;return e<0&&(e+=7),e},e}(s.LPCore);e.Calendar=c},function(t,e,i){"use strict";var n,o=this&&this.__extends||(n=function(t,e){return(n=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(t,e){t.__proto__=e}||function(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i])})(t,e)},function(t,e){function i(){this.constructor=t}n(t,e),t.prototype=null===e?Object.create(e):(i.prototype=e.prototype,new i)}),s=this&&this.__assign||function(){return(s=Object.assign||function(t){for(var e,i=1,n=arguments.length;i<n;i++)for(var o in e=arguments[i])Object.prototype.hasOwnProperty.call(e,o)&&(t[o]=e[o]);return t}).apply(this,arguments)};Object.defineProperty(e,"__esModule",{value:!0});var r=i(7),a=i(0),l=i(1),c=function(t){function e(e){var i=t.call(this)||this;i.datePicked=[],i.calendars=[],i.options={element:null,elementEnd:null,parentEl:null,firstDay:1,format:"YYYY-MM-DD",lang:"en-US",delimiter:" - ",numberOfMonths:1,numberOfColumns:1,startDate:null,endDate:null,zIndex:9999,position:"auto",selectForward:!1,selectBackward:!1,splitView:!1,inlineMode:!1,singleMode:!0,autoApply:!0,allowRepick:!1,showWeekNumbers:!1,showTooltip:!0,scrollToDate:!0,mobileFriendly:!0,resetButton:!1,autoRefresh:!1,lockDaysFormat:"YYYY-MM-DD",lockDays:[],disallowLockDaysInRange:!1,lockDaysInclusivity:"[]",highlightedDaysFormat:"YYYY-MM-DD",highlightedDays:[],dropdowns:{minYear:1990,maxYear:null,months:!1,years:!1},buttonText:{apply:"Apply",cancel:"Cancel",previousMonth:'<svg width="11" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M7.919 0l2.748 2.667L5.333 8l5.334 5.333L7.919 16 0 8z" fill-rule="nonzero"/></svg>',nextMonth:'<svg width="11" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M2.748 16L0 13.333 5.333 8 0 2.667 2.748 0l7.919 8z" fill-rule="nonzero"/></svg>',reset:'<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">\n        <path d="M0 0h24v24H0z" fill="none"/>\n        <path d="M13 3c-4.97 0-9 4.03-9 9H1l3.89 3.89.07.14L9 12H6c0-3.87 3.13-7 7-7s7 3.13 7 7-3.13 7-7 7c-1.93 0-3.68-.79-4.94-2.06l-1.42 1.42C8.27 19.99 10.51 21 13 21c4.97 0 9-4.03 9-9s-4.03-9-9-9zm-1 5v5l4.28 2.54.72-1.21-3.5-2.08V8H12z"/>\n      </svg>'},tooltipText:{one:"day",other:"days"}},i.options=s(s({},i.options),e.element.dataset),Object.keys(i.options).forEach((function(t){"true"!==i.options[t]&&"false"!==i.options[t]||(i.options[t]="true"===i.options[t])}));var n=s(s({},i.options.dropdowns),e.dropdowns),o=s(s({},i.options.buttonText),e.buttonText),r=s(s({},i.options.tooltipText),e.tooltipText);i.options=s(s({},i.options),e),i.options.dropdowns=s({},n),i.options.buttonText=s({},o),i.options.tooltipText=s({},r),i.options.elementEnd||(i.options.allowRepick=!1),i.options.lockDays.length&&(i.options.lockDays=a.DateTime.convertArray(i.options.lockDays,i.options.lockDaysFormat)),i.options.highlightedDays.length&&(i.options.highlightedDays=a.DateTime.convertArray(i.options.highlightedDays,i.options.highlightedDaysFormat));var l=i.parseInput(),c=l[0],h=l[1];i.options.startDate&&(i.options.singleMode||i.options.endDate)&&(c=new a.DateTime(i.options.startDate,i.options.format,i.options.lang)),c&&i.options.endDate&&(h=new a.DateTime(i.options.endDate,i.options.format,i.options.lang)),c instanceof a.DateTime&&!isNaN(c.getTime())&&(i.options.startDate=c),i.options.startDate&&h instanceof a.DateTime&&!isNaN(h.getTime())&&(i.options.endDate=h),!i.options.singleMode||i.options.startDate instanceof a.DateTime||(i.options.startDate=null),i.options.singleMode||i.options.startDate instanceof a.DateTime&&i.options.endDate instanceof a.DateTime||(i.options.startDate=null,i.options.endDate=null);for(var p=0;p<i.options.numberOfMonths;p+=1){var d=i.options.startDate instanceof a.DateTime?i.options.startDate.clone():new a.DateTime;if(!i.options.startDate&&(0===p||i.options.splitView)){var u=i.options.maxDate?new a.DateTime(i.options.maxDate):null,m=i.options.minDate?new a.DateTime(i.options.minDate):null,f=i.options.numberOfMonths-1;m&&u&&d.isAfter(u)?(d=m.clone()).setDate(1):!m&&u&&d.isAfter(u)&&((d=u.clone()).setDate(1),d.setMonth(d.getMonth()-f))}d.setDate(1),d.setMonth(d.getMonth()+p),i.calendars[p]=d}if(i.options.showTooltip)if(i.options.tooltipPluralSelector)i.pluralSelector=i.options.tooltipPluralSelector;else try{var g=new Intl.PluralRules(i.options.lang);i.pluralSelector=g.select.bind(g)}catch(t){i.pluralSelector=function(t){return 0===Math.abs(t)?"one":"other"}}return i}return o(e,t),e.add=function(t,e){l.Litepicker.prototype[t]=e},e.prototype.DateTime=function(t,e){return t?new a.DateTime(t,e):new a.DateTime},e.prototype.init=function(){var t=this;this.options.plugins&&this.options.plugins.length&&this.options.plugins.forEach((function(e){l.Litepicker.prototype.hasOwnProperty(e)?l.Litepicker.prototype[e].init.call(t,t):console.warn("Litepicker: plugin «"+e+"» not found.")}))},e.prototype.parseInput=function(){var t=this.options.delimiter,e=new RegExp(""+t),i=this.options.element instanceof HTMLInputElement?this.options.element.value.split(t):[];if(this.options.elementEnd){if(this.options.element instanceof HTMLInputElement&&this.options.element.value.length&&this.options.elementEnd instanceof HTMLInputElement&&this.options.elementEnd.value.length)return[new a.DateTime(this.options.element.value,this.options.format),new a.DateTime(this.options.elementEnd.value,this.options.format)]}else if(this.options.singleMode){if(this.options.element instanceof HTMLInputElement&&this.options.element.value.length)return[new a.DateTime(this.options.element.value,this.options.format)]}else if(this.options.element instanceof HTMLInputElement&&e.test(this.options.element.value)&&i.length&&i.length%2==0){var n=i.slice(0,i.length/2).join(t),o=i.slice(i.length/2).join(t);return[new a.DateTime(n,this.options.format),new a.DateTime(o,this.options.format)]}return[]},e.prototype.isShowning=function(){return this.ui&&"none"!==this.ui.style.display},e.prototype.findPosition=function(t){var e=t.getBoundingClientRect(),i=this.ui.getBoundingClientRect(),n=this.options.position.split(" "),o=window.scrollX||window.pageXOffset,s=window.scrollY||window.pageYOffset,r=0,a=0;if("auto"!==n[0]&&/top|bottom/.test(n[0]))r=e[n[0]]+s,"top"===n[0]&&(r-=i.height);else{r=e.bottom+s;var l=e.bottom+i.height>window.innerHeight,c=e.top+s-i.height>=i.height;l&&c&&(r=e.top+s-i.height)}if(/left|right/.test(n[0])||n[1]&&"auto"!==n[1]&&/left|right/.test(n[1]))a=/left|right/.test(n[0])?e[n[0]]+o:e[n[1]]+o,"right"!==n[0]&&"right"!==n[1]||(a-=i.width);else{a=e.left+o;l=e.left+i.width>window.innerWidth;var h=e.right+o-i.width>=0;l&&h&&(a=e.right+o-i.width)}return{left:a,top:r}},e}(r.EventEmitter);e.LPCore=c},function(t,e,i){"use strict";var n,o="object"==typeof Reflect?Reflect:null,s=o&&"function"==typeof o.apply?o.apply:function(t,e,i){return Function.prototype.apply.call(t,e,i)};n=o&&"function"==typeof o.ownKeys?o.ownKeys:Object.getOwnPropertySymbols?function(t){return Object.getOwnPropertyNames(t).concat(Object.getOwnPropertySymbols(t))}:function(t){return Object.getOwnPropertyNames(t)};var r=Number.isNaN||function(t){return t!=t};function a(){a.init.call(this)}t.exports=a,a.EventEmitter=a,a.prototype._events=void 0,a.prototype._eventsCount=0,a.prototype._maxListeners=void 0;var l=10;function c(t){return void 0===t._maxListeners?a.defaultMaxListeners:t._maxListeners}function h(t,e,i,n){var o,s,r,a;if("function"!=typeof i)throw new TypeError('The "listener" argument must be of type Function. Received type '+typeof i);if(void 0===(s=t._events)?(s=t._events=Object.create(null),t._eventsCount=0):(void 0!==s.newListener&&(t.emit("newListener",e,i.listener?i.listener:i),s=t._events),r=s[e]),void 0===r)r=s[e]=i,++t._eventsCount;else if("function"==typeof r?r=s[e]=n?[i,r]:[r,i]:n?r.unshift(i):r.push(i),(o=c(t))>0&&r.length>o&&!r.warned){r.warned=!0;var l=new Error("Possible EventEmitter memory leak detected. "+r.length+" "+String(e)+" listeners added. Use emitter.setMaxListeners() to increase limit");l.name="MaxListenersExceededWarning",l.emitter=t,l.type=e,l.count=r.length,a=l,console&&console.warn&&console.warn(a)}return t}function p(){for(var t=[],e=0;e<arguments.length;e++)t.push(arguments[e]);this.fired||(this.target.removeListener(this.type,this.wrapFn),this.fired=!0,s(this.listener,this.target,t))}function d(t,e,i){var n={fired:!1,wrapFn:void 0,target:t,type:e,listener:i},o=p.bind(n);return o.listener=i,n.wrapFn=o,o}function u(t,e,i){var n=t._events;if(void 0===n)return[];var o=n[e];return void 0===o?[]:"function"==typeof o?i?[o.listener||o]:[o]:i?function(t){for(var e=new Array(t.length),i=0;i<e.length;++i)e[i]=t[i].listener||t[i];return e}(o):f(o,o.length)}function m(t){var e=this._events;if(void 0!==e){var i=e[t];if("function"==typeof i)return 1;if(void 0!==i)return i.length}return 0}function f(t,e){for(var i=new Array(e),n=0;n<e;++n)i[n]=t[n];return i}Object.defineProperty(a,"defaultMaxListeners",{enumerable:!0,get:function(){return l},set:function(t){if("number"!=typeof t||t<0||r(t))throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received '+t+".");l=t}}),a.init=function(){void 0!==this._events&&this._events!==Object.getPrototypeOf(this)._events||(this._events=Object.create(null),this._eventsCount=0),this._maxListeners=this._maxListeners||void 0},a.prototype.setMaxListeners=function(t){if("number"!=typeof t||t<0||r(t))throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received '+t+".");return this._maxListeners=t,this},a.prototype.getMaxListeners=function(){return c(this)},a.prototype.emit=function(t){for(var e=[],i=1;i<arguments.length;i++)e.push(arguments[i]);var n="error"===t,o=this._events;if(void 0!==o)n=n&&void 0===o.error;else if(!n)return!1;if(n){var r;if(e.length>0&&(r=e[0]),r instanceof Error)throw r;var a=new Error("Unhandled error."+(r?" ("+r.message+")":""));throw a.context=r,a}var l=o[t];if(void 0===l)return!1;if("function"==typeof l)s(l,this,e);else{var c=l.length,h=f(l,c);for(i=0;i<c;++i)s(h[i],this,e)}return!0},a.prototype.addListener=function(t,e){return h(this,t,e,!1)},a.prototype.on=a.prototype.addListener,a.prototype.prependListener=function(t,e){return h(this,t,e,!0)},a.prototype.once=function(t,e){if("function"!=typeof e)throw new TypeError('The "listener" argument must be of type Function. Received type '+typeof e);return this.on(t,d(this,t,e)),this},a.prototype.prependOnceListener=function(t,e){if("function"!=typeof e)throw new TypeError('The "listener" argument must be of type Function. Received type '+typeof e);return this.prependListener(t,d(this,t,e)),this},a.prototype.removeListener=function(t,e){var i,n,o,s,r;if("function"!=typeof e)throw new TypeError('The "listener" argument must be of type Function. Received type '+typeof e);if(void 0===(n=this._events))return this;if(void 0===(i=n[t]))return this;if(i===e||i.listener===e)0==--this._eventsCount?this._events=Object.create(null):(delete n[t],n.removeListener&&this.emit("removeListener",t,i.listener||e));else if("function"!=typeof i){for(o=-1,s=i.length-1;s>=0;s--)if(i[s]===e||i[s].listener===e){r=i[s].listener,o=s;break}if(o<0)return this;0===o?i.shift():function(t,e){for(;e+1<t.length;e++)t[e]=t[e+1];t.pop()}(i,o),1===i.length&&(n[t]=i[0]),void 0!==n.removeListener&&this.emit("removeListener",t,r||e)}return this},a.prototype.off=a.prototype.removeListener,a.prototype.removeAllListeners=function(t){var e,i,n;if(void 0===(i=this._events))return this;if(void 0===i.removeListener)return 0===arguments.length?(this._events=Object.create(null),this._eventsCount=0):void 0!==i[t]&&(0==--this._eventsCount?this._events=Object.create(null):delete i[t]),this;if(0===arguments.length){var o,s=Object.keys(i);for(n=0;n<s.length;++n)"removeListener"!==(o=s[n])&&this.removeAllListeners(o);return this.removeAllListeners("removeListener"),this._events=Object.create(null),this._eventsCount=0,this}if("function"==typeof(e=i[t]))this.removeListener(t,e);else if(void 0!==e)for(n=e.length-1;n>=0;n--)this.removeListener(t,e[n]);return this},a.prototype.listeners=function(t){return u(this,t,!0)},a.prototype.rawListeners=function(t){return u(this,t,!1)},a.listenerCount=function(t,e){return"function"==typeof t.listenerCount?t.listenerCount(e):m.call(t,e)},a.prototype.listenerCount=m,a.prototype.eventNames=function(){return this._eventsCount>0?n(this._events):[]}},function(t,e,i){(e=i(9)(!1)).push([t.i,':root{--litepicker-container-months-color-bg: #fff;--litepicker-container-months-box-shadow-color: #ddd;--litepicker-footer-color-bg: #fafafa;--litepicker-footer-box-shadow-color: #ddd;--litepicker-tooltip-color-bg: #fff;--litepicker-month-header-color: #333;--litepicker-button-prev-month-color: #9e9e9e;--litepicker-button-next-month-color: #9e9e9e;--litepicker-button-prev-month-color-hover: #2196f3;--litepicker-button-next-month-color-hover: #2196f3;--litepicker-month-width: calc(var(--litepicker-day-width) * 7);--litepicker-month-weekday-color: #9e9e9e;--litepicker-month-week-number-color: #9e9e9e;--litepicker-day-width: 38px;--litepicker-day-color: #333;--litepicker-day-color-hover: #2196f3;--litepicker-is-today-color: #f44336;--litepicker-is-in-range-color: #bbdefb;--litepicker-is-locked-color: #9e9e9e;--litepicker-is-start-color: #fff;--litepicker-is-start-color-bg: #2196f3;--litepicker-is-end-color: #fff;--litepicker-is-end-color-bg: #2196f3;--litepicker-button-cancel-color: #fff;--litepicker-button-cancel-color-bg: #9e9e9e;--litepicker-button-apply-color: #fff;--litepicker-button-apply-color-bg: #2196f3;--litepicker-button-reset-color: #909090;--litepicker-button-reset-color-hover: #2196f3;--litepicker-highlighted-day-color: #333;--litepicker-highlighted-day-color-bg: #ffeb3b}.show-week-numbers{--litepicker-month-width: calc(var(--litepicker-day-width) * 8)}.litepicker{font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;font-size:0.8em;display:none}.litepicker button{border:none;background:none}.litepicker .container__main{display:-webkit-box;display:-ms-flexbox;display:flex}.litepicker .container__months{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;background-color:var(--litepicker-container-months-color-bg);border-radius:5px;-webkit-box-shadow:0 0 5px var(--litepicker-container-months-box-shadow-color);box-shadow:0 0 5px var(--litepicker-container-months-box-shadow-color);width:calc(var(--litepicker-month-width) + 10px);-webkit-box-sizing:content-box;box-sizing:content-box}.litepicker .container__months.columns-2{width:calc((var(--litepicker-month-width) * 2) + 20px)}.litepicker .container__months.columns-3{width:calc((var(--litepicker-month-width) * 3) + 30px)}.litepicker .container__months.columns-4{width:calc((var(--litepicker-month-width) * 4) + 40px)}.litepicker .container__months.split-view .month-item-header .button-previous-month,.litepicker .container__months.split-view .month-item-header .button-next-month{visibility:visible}.litepicker .container__months .month-item{padding:5px;width:var(--litepicker-month-width);-webkit-box-sizing:content-box;box-sizing:content-box}.litepicker .container__months .month-item-header{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;font-weight:500;padding:10px 5px;text-align:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;color:var(--litepicker-month-header-color)}.litepicker .container__months .month-item-header div{-webkit-box-flex:1;-ms-flex:1;flex:1}.litepicker .container__months .month-item-header div>.month-item-name{margin-right:5px}.litepicker .container__months .month-item-header div>.month-item-year{padding:0}.litepicker .container__months .month-item-header .reset-button{color:var(--litepicker-button-reset-color)}.litepicker .container__months .month-item-header .reset-button>svg{fill:var(--litepicker-button-reset-color)}.litepicker .container__months .month-item-header .reset-button *{pointer-events:none}.litepicker .container__months .month-item-header .reset-button:hover{color:var(--litepicker-button-reset-color-hover)}.litepicker .container__months .month-item-header .reset-button:hover>svg{fill:var(--litepicker-button-reset-color-hover)}.litepicker .container__months .month-item-header .button-previous-month,.litepicker .container__months .month-item-header .button-next-month{visibility:hidden;text-decoration:none;padding:3px 5px;border-radius:3px;-webkit-transition:color 0.3s, border 0.3s;transition:color 0.3s, border 0.3s;cursor:default}.litepicker .container__months .month-item-header .button-previous-month *,.litepicker .container__months .month-item-header .button-next-month *{pointer-events:none}.litepicker .container__months .month-item-header .button-previous-month{color:var(--litepicker-button-prev-month-color)}.litepicker .container__months .month-item-header .button-previous-month>svg,.litepicker .container__months .month-item-header .button-previous-month>img{fill:var(--litepicker-button-prev-month-color)}.litepicker .container__months .month-item-header .button-previous-month:hover{color:var(--litepicker-button-prev-month-color-hover)}.litepicker .container__months .month-item-header .button-previous-month:hover>svg{fill:var(--litepicker-button-prev-month-color-hover)}.litepicker .container__months .month-item-header .button-next-month{color:var(--litepicker-button-next-month-color)}.litepicker .container__months .month-item-header .button-next-month>svg,.litepicker .container__months .month-item-header .button-next-month>img{fill:var(--litepicker-button-next-month-color)}.litepicker .container__months .month-item-header .button-next-month:hover{color:var(--litepicker-button-next-month-color-hover)}.litepicker .container__months .month-item-header .button-next-month:hover>svg{fill:var(--litepicker-button-next-month-color-hover)}.litepicker .container__months .month-item-weekdays-row{display:-webkit-box;display:-ms-flexbox;display:flex;justify-self:center;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;color:var(--litepicker-month-weekday-color)}.litepicker .container__months .month-item-weekdays-row>div{padding:5px 0;font-size:85%;-webkit-box-flex:1;-ms-flex:1;flex:1;width:var(--litepicker-day-width);text-align:center}.litepicker .container__months .month-item:first-child .button-previous-month{visibility:visible}.litepicker .container__months .month-item:last-child .button-next-month{visibility:visible}.litepicker .container__months .month-item.no-previous-month .button-previous-month{visibility:hidden}.litepicker .container__months .month-item.no-next-month .button-next-month{visibility:hidden}.litepicker .container__days{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;justify-self:center;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;text-align:center;-webkit-box-sizing:content-box;box-sizing:content-box}.litepicker .container__days>div,.litepicker .container__days>a{padding:5px 0;width:var(--litepicker-day-width)}.litepicker .container__days .day-item{color:var(--litepicker-day-color);text-align:center;text-decoration:none;border-radius:3px;-webkit-transition:color 0.3s, border 0.3s;transition:color 0.3s, border 0.3s;cursor:default}.litepicker .container__days .day-item:hover{color:var(--litepicker-day-color-hover);-webkit-box-shadow:inset 0 0 0 1px var(--litepicker-day-color-hover);box-shadow:inset 0 0 0 1px var(--litepicker-day-color-hover)}.litepicker .container__days .day-item.is-today{color:var(--litepicker-is-today-color)}.litepicker .container__days .day-item.is-locked{color:var(--litepicker-is-locked-color)}.litepicker .container__days .day-item.is-locked:hover{color:var(--litepicker-is-locked-color);-webkit-box-shadow:none;box-shadow:none;cursor:default}.litepicker .container__days .day-item.is-in-range{background-color:var(--litepicker-is-in-range-color);border-radius:0}.litepicker .container__days .day-item.is-start-date{color:var(--litepicker-is-start-color);background-color:var(--litepicker-is-start-color-bg);border-top-left-radius:5px;border-bottom-left-radius:5px;border-top-right-radius:0;border-bottom-right-radius:0}.litepicker .container__days .day-item.is-start-date.is-flipped{border-top-left-radius:0;border-bottom-left-radius:0;border-top-right-radius:5px;border-bottom-right-radius:5px}.litepicker .container__days .day-item.is-end-date{color:var(--litepicker-is-end-color);background-color:var(--litepicker-is-end-color-bg);border-top-left-radius:0;border-bottom-left-radius:0;border-top-right-radius:5px;border-bottom-right-radius:5px}.litepicker .container__days .day-item.is-end-date.is-flipped{border-top-left-radius:5px;border-bottom-left-radius:5px;border-top-right-radius:0;border-bottom-right-radius:0}.litepicker .container__days .day-item.is-start-date.is-end-date{border-top-left-radius:5px;border-bottom-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px}.litepicker .container__days .day-item.is-highlighted{color:var(--litepicker-highlighted-day-color);background-color:var(--litepicker-highlighted-day-color-bg)}.litepicker .container__days .week-number{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;color:var(--litepicker-month-week-number-color);font-size:85%}.litepicker .container__footer{text-align:right;padding:10px 5px;margin:0 5px;background-color:var(--litepicker-footer-color-bg);-webkit-box-shadow:inset 0px 3px 3px 0px var(--litepicker-footer-box-shadow-color);box-shadow:inset 0px 3px 3px 0px var(--litepicker-footer-box-shadow-color);border-bottom-left-radius:5px;border-bottom-right-radius:5px}.litepicker .container__footer .preview-date-range{margin-right:10px;font-size:90%}.litepicker .container__footer .button-cancel{background-color:var(--litepicker-button-cancel-color-bg);color:var(--litepicker-button-cancel-color);border:0;padding:3px 7px 4px;border-radius:3px}.litepicker .container__footer .button-cancel *{pointer-events:none}.litepicker .container__footer .button-apply{background-color:var(--litepicker-button-apply-color-bg);color:var(--litepicker-button-apply-color);border:0;padding:3px 7px 4px;border-radius:3px;margin-left:10px;margin-right:10px}.litepicker .container__footer .button-apply:disabled{opacity:0.7}.litepicker .container__footer .button-apply *{pointer-events:none}.litepicker .container__tooltip{position:absolute;margin-top:-4px;padding:4px 8px;border-radius:4px;background-color:var(--litepicker-tooltip-color-bg);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.25);box-shadow:0 1px 3px rgba(0,0,0,0.25);white-space:nowrap;font-size:11px;pointer-events:none;visibility:hidden}.litepicker .container__tooltip:before{position:absolute;bottom:-5px;left:calc(50% - 5px);border-top:5px solid rgba(0,0,0,0.12);border-right:5px solid transparent;border-left:5px solid transparent;content:""}.litepicker .container__tooltip:after{position:absolute;bottom:-4px;left:calc(50% - 4px);border-top:4px solid var(--litepicker-tooltip-color-bg);border-right:4px solid transparent;border-left:4px solid transparent;content:""}\n',""]),e.locals={showWeekNumbers:"show-week-numbers",litepicker:"litepicker",containerMain:"container__main",containerMonths:"container__months",columns2:"columns-2",columns3:"columns-3",columns4:"columns-4",splitView:"split-view",monthItemHeader:"month-item-header",buttonPreviousMonth:"button-previous-month",buttonNextMonth:"button-next-month",monthItem:"month-item",monthItemName:"month-item-name",monthItemYear:"month-item-year",resetButton:"reset-button",monthItemWeekdaysRow:"month-item-weekdays-row",noPreviousMonth:"no-previous-month",noNextMonth:"no-next-month",containerDays:"container__days",dayItem:"day-item",isToday:"is-today",isLocked:"is-locked",isInRange:"is-in-range",isStartDate:"is-start-date",isFlipped:"is-flipped",isEndDate:"is-end-date",isHighlighted:"is-highlighted",weekNumber:"week-number",containerFooter:"container__footer",previewDateRange:"preview-date-range",buttonCancel:"button-cancel",buttonApply:"button-apply",containerTooltip:"container__tooltip"},t.exports=e},function(t,e,i){"use strict";t.exports=function(t){var e=[];return e.toString=function(){return this.map((function(e){var i=function(t,e){var i=t[1]||"",n=t[3];if(!n)return i;if(e&&"function"==typeof btoa){var o=(r=n,a=btoa(unescape(encodeURIComponent(JSON.stringify(r)))),l="sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(a),"/*# ".concat(l," */")),s=n.sources.map((function(t){return"/*# sourceURL=".concat(n.sourceRoot||"").concat(t," */")}));return[i].concat(s).concat([o]).join("\n")}var r,a,l;return[i].join("\n")}(e,t);return e[2]?"@media ".concat(e[2]," {").concat(i,"}"):i})).join("")},e.i=function(t,i,n){"string"==typeof t&&(t=[[null,t,""]]);var o={};if(n)for(var s=0;s<this.length;s++){var r=this[s][0];null!=r&&(o[r]=!0)}for(var a=0;a<t.length;a++){var l=[].concat(t[a]);n&&o[l[0]]||(i&&(l[2]?l[2]="".concat(i," and ").concat(l[2]):l[2]=i),e.push(l))}},e}},function(t,e,i){"use strict";var n,o={},s=function(){return void 0===n&&(n=Boolean(window&&document&&document.all&&!window.atob)),n},r=function(){var t={};return function(e){if(void 0===t[e]){var i=document.querySelector(e);if(window.HTMLIFrameElement&&i instanceof window.HTMLIFrameElement)try{i=i.contentDocument.head}catch(t){i=null}t[e]=i}return t[e]}}();function a(t,e){for(var i=[],n={},o=0;o<t.length;o++){var s=t[o],r=e.base?s[0]+e.base:s[0],a={css:s[1],media:s[2],sourceMap:s[3]};n[r]?n[r].parts.push(a):i.push(n[r]={id:r,parts:[a]})}return i}function l(t,e){for(var i=0;i<t.length;i++){var n=t[i],s=o[n.id],r=0;if(s){for(s.refs++;r<s.parts.length;r++)s.parts[r](n.parts[r]);for(;r<n.parts.length;r++)s.parts.push(g(n.parts[r],e))}else{for(var a=[];r<n.parts.length;r++)a.push(g(n.parts[r],e));o[n.id]={id:n.id,refs:1,parts:a}}}}function c(t){var e=document.createElement("style");if(void 0===t.attributes.nonce){var n=i.nc;n&&(t.attributes.nonce=n)}if(Object.keys(t.attributes).forEach((function(i){e.setAttribute(i,t.attributes[i])})),"function"==typeof t.insert)t.insert(e);else{var o=r(t.insert||"head");if(!o)throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");o.appendChild(e)}return e}var h,p=(h=[],function(t,e){return h[t]=e,h.filter(Boolean).join("\n")});function d(t,e,i,n){var o=i?"":n.css;if(t.styleSheet)t.styleSheet.cssText=p(e,o);else{var s=document.createTextNode(o),r=t.childNodes;r[e]&&t.removeChild(r[e]),r.length?t.insertBefore(s,r[e]):t.appendChild(s)}}function u(t,e,i){var n=i.css,o=i.media,s=i.sourceMap;if(o&&t.setAttribute("media",o),s&&btoa&&(n+="\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(s))))," */")),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}var m=null,f=0;function g(t,e){var i,n,o;if(e.singleton){var s=f++;i=m||(m=c(e)),n=d.bind(null,i,s,!1),o=d.bind(null,i,s,!0)}else i=c(e),n=u.bind(null,i,e),o=function(){!function(t){if(null===t.parentNode)return!1;t.parentNode.removeChild(t)}(i)};return n(t),function(e){if(e){if(e.css===t.css&&e.media===t.media&&e.sourceMap===t.sourceMap)return;n(t=e)}else o()}}t.exports=function(t,e){(e=e||{}).attributes="object"==typeof e.attributes?e.attributes:{},e.singleton||"boolean"==typeof e.singleton||(e.singleton=s());var i=a(t,e);return l(i,e),function(t){for(var n=[],s=0;s<i.length;s++){var r=i[s],c=o[r.id];c&&(c.refs--,n.push(c))}t&&l(a(t,e),e);for(var h=0;h<n.length;h++){var p=n[h];if(0===p.refs){for(var d=0;d<p.parts.length;d++)p.parts[d]();delete o[p.id]}}}}},function(t,e,i){"use strict";var n=this&&this.__assign||function(){return(n=Object.assign||function(t){for(var e,i=1,n=arguments.length;i<n;i++)for(var o in e=arguments[i])Object.prototype.hasOwnProperty.call(e,o)&&(t[o]=e[o]);return t}).apply(this,arguments)};Object.defineProperty(e,"__esModule",{value:!0});var o=i(0),s=i(1),r=i(2);s.Litepicker.prototype.show=function(t){void 0===t&&(t=null),this.emit("before:show",t);var e=t||this.options.element;if(this.triggerElement=e,!this.isShowning()){if(this.options.inlineMode)return this.ui.style.position="relative",this.ui.style.display="inline-block",this.ui.style.top=null,this.ui.style.left=null,this.ui.style.bottom=null,void(this.ui.style.right=null);this.scrollToDate(t),this.render(),this.ui.style.position="absolute",this.ui.style.display="block",this.ui.style.zIndex=this.options.zIndex;var i=this.findPosition(e);this.ui.style.top=i.top+"px",this.ui.style.left=i.left+"px",this.ui.style.right=null,this.ui.style.bottom=null,this.emit("show",t)}},s.Litepicker.prototype.hide=function(){this.isShowning()&&(this.datePicked.length=0,this.updateInput(),this.options.inlineMode?this.render():(this.ui.style.display="none",this.emit("hide")))},s.Litepicker.prototype.getDate=function(){return this.getStartDate()},s.Litepicker.prototype.getStartDate=function(){return this.options.startDate?this.options.startDate.clone():null},s.Litepicker.prototype.getEndDate=function(){return this.options.endDate?this.options.endDate.clone():null},s.Litepicker.prototype.setDate=function(t,e){void 0===e&&(e=!1);var i=new o.DateTime(t,this.options.format,this.options.lang);r.dateIsLocked(i,this.options,[i])&&!e?this.emit("error:date",i):(this.setStartDate(t),this.options.inlineMode&&this.render(),this.emit("selected",this.getDate()))},s.Litepicker.prototype.setStartDate=function(t){t&&(this.options.startDate=new o.DateTime(t,this.options.format,this.options.lang),this.updateInput())},s.Litepicker.prototype.setEndDate=function(t){t&&(this.options.endDate=new o.DateTime(t,this.options.format,this.options.lang),this.options.startDate.getTime()>this.options.endDate.getTime()&&(this.options.endDate=this.options.startDate.clone(),this.options.startDate=new o.DateTime(t,this.options.format,this.options.lang)),this.updateInput())},s.Litepicker.prototype.setDateRange=function(t,e,i){void 0===i&&(i=!1),this.triggerElement=void 0;var n=new o.DateTime(t,this.options.format,this.options.lang),s=new o.DateTime(e,this.options.format,this.options.lang);(this.options.disallowLockDaysInRange?r.rangeIsLocked([n,s],this.options):r.dateIsLocked(n,this.options,[n,s])||r.dateIsLocked(s,this.options,[n,s]))&&!i?this.emit("error:range",[n,s]):(this.setStartDate(n),this.setEndDate(s),this.options.inlineMode&&this.render(),this.updateInput(),this.emit("selected",this.getStartDate(),this.getEndDate()))},s.Litepicker.prototype.gotoDate=function(t,e){void 0===e&&(e=0);var i=new o.DateTime(t);i.setDate(1),this.calendars[e]=i.clone(),this.render()},s.Litepicker.prototype.setLockDays=function(t){this.options.lockDays=o.DateTime.convertArray(t,this.options.lockDaysFormat),this.render()},s.Litepicker.prototype.setHighlightedDays=function(t){this.options.highlightedDays=o.DateTime.convertArray(t,this.options.highlightedDaysFormat),this.render()},s.Litepicker.prototype.setOptions=function(t){delete t.element,delete t.elementEnd,delete t.parentEl,t.startDate&&(t.startDate=new o.DateTime(t.startDate,this.options.format,this.options.lang)),t.endDate&&(t.endDate=new o.DateTime(t.endDate,this.options.format,this.options.lang));var e=n(n({},this.options.dropdowns),t.dropdowns),i=n(n({},this.options.buttonText),t.buttonText),s=n(n({},this.options.tooltipText),t.tooltipText);this.options=n(n({},this.options),t),this.options.dropdowns=n({},e),this.options.buttonText=n({},i),this.options.tooltipText=n({},s),!this.options.singleMode||this.options.startDate instanceof o.DateTime||(this.options.startDate=null,this.options.endDate=null),this.options.singleMode||this.options.startDate instanceof o.DateTime&&this.options.endDate instanceof o.DateTime||(this.options.startDate=null,this.options.endDate=null);for(var r=0;r<this.options.numberOfMonths;r+=1){var a=this.options.startDate?this.options.startDate.clone():new o.DateTime;a.setDate(1),a.setMonth(a.getMonth()+r),this.calendars[r]=a}this.options.lockDays.length&&(this.options.lockDays=o.DateTime.convertArray(this.options.lockDays,this.options.lockDaysFormat)),this.options.highlightedDays.length&&(this.options.highlightedDays=o.DateTime.convertArray(this.options.highlightedDays,this.options.highlightedDaysFormat)),this.render(),this.options.inlineMode&&this.show(),this.updateInput()},s.Litepicker.prototype.clearSelection=function(){this.options.startDate=null,this.options.endDate=null,this.datePicked.length=0,this.updateInput(),this.isShowning()&&this.render(),this.emit("clear:selection")},s.Litepicker.prototype.destroy=function(){this.ui&&this.ui.parentNode&&(this.ui.parentNode.removeChild(this.ui),this.ui=null),this.emit("destroy")}}]).Litepicker;

    



    /*=============================================

    =            LITEPICKER            =

    =============================================*/

    let current = Date.now();

    const busIda = new Litepicker({

      element: document.getElementById('bus-ida'),

      autoRefresh: false,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: current

    });

    const busIdaId = new Litepicker({

      element: document.getElementById('bus-entrada-id'),

      autoRefresh: false,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: current

    });











    const busVuelta = new Litepicker({

      element: document.getElementById('bus-vuelta'),

      autoRefresh: false,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: current

    });



    const busVueltaId = new Litepicker({

      element: document.getElementById('bus-salida-id'),

      autoRefresh: false,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: current

    });







    busIda.on('selected', (date1, date2) => {

      let stringDate = busIda.getDate().dateInstance

      let newStartDate = stringDate;

      busVueltaId.setOptions({

        minDate: newStartDate,

        startDate: ''

      });

      busVuelta.setOptions({

        minDate: newStartDate,

        startDate: ''

      });

      busIdaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });

      busVuelta.on('show', () => {

        let dateSelected = busIda.getDate();

        if (dateSelected) {

          dateSelected.setMonth(dateSelected.getMonth());

          busVuelta.gotoDate(dateSelected);

          busVueltaId.gotoDate(dateSelected);

        }

      });



    })

    busVuelta.on('selected', (date1, date2) => {

      let stringDate = busVuelta.getDate().dateInstance

      let newStartDate = stringDate;

      busVueltaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate,

      });

    });







    // LITEPICKER HOTELES

    let today = new Date();

    let today2 = new Date();

    let nextDay = today.setDate(today.getDate() + 1);

    let nextThreeDays = today2.setDate(today2.getDate() + 2);



    const hotelEntrada = new Litepicker({

      element: document.getElementById('hotel-entrada'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,



      tooltipText: {

        one: 'día',

        other: 'días'

      },

      startDate: null,

      numberOfColumns: 1,

      minDate: nextThreeDays,

    });

    // ENTRADA ID HOTEL

    const hotelEntradaId = new Litepicker({

      element: document.getElementById('h-entrada-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      startDate: nextThreeDays,

      minDate: nextThreeDays

    });



    const hotelSalida = new Litepicker({

      element: document.getElementById('hotel-salida'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      minDate: nextThreeDays,

      numberOfColumns: 1,

    });



    const hotelSalidaId = new Litepicker({

      element: document.getElementById('h-salida-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: current

    });



    // Para que el calendario muestre la fecha disponible para reservar

    hotelEntrada.on('show', () => {

      let date = hotelEntradaId.getDate();

      console.log(hotelEntradaId)

      if (date) {

        date.setMonth(date.getMonth());

        hotelEntrada.gotoDate(date);

      }

    });

    hotelSalida.on('show', () => {

      let date = hotelEntradaId.getDate();

      // DETECTAR EL ULTIMO DIA DEL MES SELECCIONADO

      date.setDate(date.getDate() + 1);

      if (date) {

        hotelSalida.gotoDate(date);

      }



    });

    hotelEntrada.on('selected', (date1, date2) => {

      let stringDate = hotelEntrada.getDate().dateInstance

      let date = hotelEntrada.getDate();

      let stringDateTwo = hotelEntrada.getDate().dateInstance

      let nextDay = stringDateTwo.setDate(stringDate.getDate() + 1)

      let newStartDate = stringDate;



      hotelSalida.setOptions({

        minDate: nextDay,

        startDate: '',

      });

      hotelSalidaId.setOptions({

        minDate: nextDay,

        startDate: ''

      });

      hotelEntradaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });

    });

    hotelSalida.on('selected', (date1, date2) => {

      let stringDate = hotelSalida.getDate().dateInstance

      let newStartDate = stringDate;

      hotelSalidaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate,

      });

    });



    // LITEPICKER Actividades





    const actividadesEntrada = new Litepicker({

      element: document.getElementById('actividades-entrada'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      switchingMonths: 1,

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: nextDay,

    });

    const actividadesEntradaId = new Litepicker({

      element: document.getElementById('actividades-entrada-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      startDate: nextDay,

      numberOfColumns: 1,

      minDate: nextDay,

    });

    const actividadesSalida = new Litepicker({

      element: document.getElementById('actividades-salida'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: nextDay,

    });

    const actividadesSalidaId = new Litepicker({

      element: document.getElementById('actividades-salida-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

    });

    actividadesEntrada.on('show', () => {

      let date = hotelEntradaId.getDate();

      if (date) {

        date.setMonth(date.getMonth());

        actividadesEntrada.gotoDate(date);

      }

    });

    actividadesSalida.on('show', () => {

      let date = hotelEntradaId.getDate();

      if (date) {

        date.setMonth(date.getMonth());

        actividadesSalida.gotoDate(date);

      }

    });

    actividadesEntrada.on('selected', (date1, date2) => {

      let stringDate = actividadesEntrada.getDate().dateInstance

      let stringDateTwo = actividadesEntrada.getDate().dateInstance

      let newStartDate = stringDate;

      let dayLimit = stringDateTwo.setDate(stringDateTwo.getDate() + 21);

      actividadesSalida.on('show', () => {

        let dateSelected = actividadesEntrada.getDate()

        if (dateSelected) {

          dateSelected.setMonth(dateSelected.getMonth());

          actividadesSalida.gotoDate(dateSelected);

        }

      });

      // FECHA  

      actividadesSalida.setOptions({

        minDate: newStartDate,

        startDate: dayLimit,

        maxDate: dayLimit,

      });

      actividadesSalidaId.setOptions({

        minDate: newStartDate,

        startDate: dayLimit,

        maxDate: dayLimit,

      });

      actividadesEntradaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });

    });

    actividadesSalida.on('selected', (date1, date2) => {

      let stringDate = actividadesSalida.getDate().dateInstance

      let newStartDate = stringDate;

      actividadesSalidaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });

    });

    // LITEPICKER PAQUETES

    const paquetesIda = new Litepicker({

      element: document.getElementById('p-ida'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: nextThreeDays,

    });

    const paquetesIdaId = new Litepicker({

      element: document.getElementById('p-ida-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      startDate: nextThreeDays,

      numberOfColumns: 1,

      minDate: nextThreeDays,

    });

    const paquetesVuelta = new Litepicker({

      element: document.getElementById('p-vuelta'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'DD/MM/YYYY',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

      minDate: nextThreeDays,

    });

    const paquetesVueltaId = new Litepicker({

      element: document.getElementById('p-vuelta-id'),

      autoRefresh: true,

      lang: 'es-ES',

      format: 'YYYY-MM-DD',

      numberOfMonths: 1,

      tooltipText: {

        one: 'día',

        other: 'días'

      },

      numberOfColumns: 1,

    });



    paquetesIda.on('show', () => {

      let date = paquetesIdaId.getDate();

      if (date) {

        date.setMonth(date.getMonth());

        paquetesIda.gotoDate(date);

      }

    });

    paquetesVuelta.on('show', () => {

      let date = paquetesIdaId.getDate();

      date.setDate(date.getDate() + 1);

      if (date) {

        date.setMonth(date.getMonth());

        paquetesVuelta.gotoDate(date);

      }

    });

    paquetesIda.on('selected', (date1, date2) => {

      let stringDate = paquetesIda.getDate().dateInstance

      let stringDateTwo = paquetesIda.getDate().dateInstance

      let nextDay = stringDateTwo.setDate(stringDateTwo.getDate() + 1)

      let newStartDate = stringDate;

      paquetesVuelta.setOptions({

        minDate: nextDay,

        startDate: '',

      });

      paquetesVueltaId.setOptions({

        minDate: nextDay,

        startDate: '',

      });

      paquetesIdaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });

    });

    paquetesVuelta.on('selected', (date1, date2) => {

      let stringDate = paquetesVuelta.getDate().dateInstance

      let newStartDate = stringDate;

      paquetesVueltaId.setOptions({

        minDate: newStartDate,

        startDate: newStartDate

      });



    });

    /*=====  End of LITEPICKER  ======*/





    /*=============================================

    =            CHANGE ORIGEN / DESTION            =

    =============================================*/

    // Invertir Origen/Destino TAB BUS

    $('.d-arrow-img').click(function(e) {

      e.preventDefault();

      var origen = $('#origen').val();

      var origenId = $('#origen-id').val();

      var destino = $('#destino').val();

      var destinoId = $('#destino-id').val();

      $('#origen').val(destino);

      $('#origen-id').val(destinoId);

      $('#destino').val(origen);

      $('#destino-id').val(origenId);

    });



    // Invertir Origen/Destino TAB PAQUETES

    $('#d-arrow-paquetes').click(function(e) {

      e.preventDefault();

      var origen = $('#p-origen').val();

      var origenId = $('#p-origen-id').val();

      var destino = $('#p-destino').val();

      var destinoId = $('#p-destino-id').val();

      $('#p-origen').val(destino);

      $('#p-origen-id').val(destinoId);

      $('#p-destino').val(origen);

      $('#p-destino-id').val(origenId);

    });



    /*=====  End of CHANGE ORIGEN / DESTION  ======*/





    /*=============================================

    =            MODAL PASSENGER           =

    =============================================*/

    // MAX AND MIN BUTTON

    let buttonMin = document.querySelectorAll('.button-min');

    let buttonMax = document.querySelectorAll('.button-max');

    // VALUE INPUT BUS GENERAL

    let totalPass = document.getElementById('pasajeros');

    // let totalAdultos = document.getElementById('bus-adulto');

    // let totalNinos = document.getElementById('bus-ninos');

    // let totalBebes = document.getElementById('bus-bebes');

    // VALUE INPUT HOTEL GENERAL

    let totalHotelPass = document.getElementById('hotel-pasajeros');

    // let totalHotelAdultos = document.getElementById('hotel-adulto');

    // let totalHotelNinos = document.getElementById('hotel-ninos');

    // VALUE INPUT PAQUETES GENERAL

    let totalPaquetePass = document.getElementById('paquetes-pasajeros');

    // let totalPaqueteAdultos = document.getElementById('paquetes-adulto');

    // let totalPaqueteNinos = document.getElementById('paquetes-ninos');

    // FUNCION RESTAR VALOR INPUT 

    const minButton = (btn) => {

      let cantidad = btn.parentNode.nextElementSibling;

      if (cantidad.value > 0) {

        cantidad.value = --cantidad.value;

      } else {}

    }

    // ITERA TODOS LOS BOTONES MIN DEL MODAL

    buttonMin.forEach(minBtn => {

      // CLICK MINBUTTON



      let btn = minBtn.firstElementChild

      btn.addEventListener('click', () => {



        minButton(btn)

      })

    });

    // FUNCION SUMAR VALO

    const plusButton = (btn) => {

      let modal = btn.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode

      let modalId = modal.id

      let cantidad = btn.parentNode.previousElementSibling

      // SI EL MODAL ES DE HOTEL

      if (modalId === 'modal-hotel-passenger' && cantidad.value >= 0 && cantidad.value <= 3 || modalId === 'modal-paquetes-passenger' && cantidad.value >= 0 && cantidad.value <= 3) {

        cantidad.value = ++cantidad.value;

      }

      // SI TIENE OTRO ID HACE ESTO

      if (cantidad.value >= 0 && modalId !== 'modal-hotel-passenger' && modalId !== 'modal-paquetes-passenger') {

        cantidad.value = ++cantidad.value;

      } else {



      }

    }

    // ITERA TODOS LOS BOTONES MAX DEL MODAL

    buttonMax.forEach(maxBtn => {

      // CLICK MINBUTTON

      let btn = maxBtn.firstElementChild

      btn.addEventListener('click', () => {

        plusButton(btn)

      })

    });

    // INSERTA EL VALOR TOTAL OBTENIDO DE LOS INPUT DEL MODAL

    // ALL INPUT DE BUSQUEDA PASJASEROS

    /* const insertTotalValuePassenger = () => {

      let modalPassenger = document.getElementById('modal-bus-passenger');

      // INSERTO EL VALUE

      totalPass.value = parseInt(totalAdultos.value) + parseInt(totalNinos.value) + parseInt(totalBebes.value);

      // ELIMINO EL MODAL

      modalPassenger.classList.remove('is-visible')

    }

    // ALL INPUT DE BUSQUEDA HOTEL

    const insertTotalValuePassengerHotel = () => {

      let modal = document.getElementById('modal-hotel-passenger');

      // INSERTO EL VALUE

      totalHotelPass.value = parseInt(totalHotelAdultos.value) + parseInt(totalHotelNinos.value);

      // ELIMINO EL MODAL

      modal.classList.remove('is-visible')



    }

    // ALL INPUT DE BUSQUEDA PAQUETES

    const insertTotalValuePassengerPaquetes = () => {

      let modal = document.getElementById('modal-paquetes-passenger');

      // INSERTO EL VALUE

      totalPaquetePass.value = parseInt(totalPaqueteAdultos.value) + parseInt(totalPaqueteNinos.value);

      // ELIMINO EL MODAL

      modal.classList.remove('is-visible')



    } */

    /*=====  End of MODAL PASSENGER  ======*/











    /*=============================================

    =            AUTOCOMPLETE LIBRARY            =

    =============================================*/

    // AUTOCOMPLETE JQUERY

    var EasyAutocomplete = (function(C) {

      return (

        (C.main = function(t, e) {

          function n() {

            return 0 === h.length ?

              void l.error(`Input field doesn't exist.`) :

              g.checkDataUrlProperties() ?

              g.checkRequiredProperties() ?

              (i(),

                p('autocompleteOff', !0) && h.attr('autocomplete', 'off'),

                h.focusout(function() {

                  var t,

                    e = h.val();

                  g.get('list').match.caseSensitive || (e = e.toLowerCase());

                  for (var n = 0, i = y.length; n < i; n += 1)

                    if (

                      ((t = g.get('getValue')(y[n])),

                        (t = !g.get('list').match.caseSensitive ?

                          t.toLowerCase() :

                          t) === e)

                    )

                      return void f((E = n));

                }),

                h.off('keyup').keyup(function(t) {

                  function e(i) {

                    var t, e;

                    i.length < g.get('minCharNumber') ||

                      ('list-required' !== g.get('data') &&

                        ((t = g.get('data')),

                          (e = d.init(t)),

                          (e = d.updateCategories(e, t)),

                          s((e = d.processData(e, i)), i),

                          (0 < h.parent().find('li').length ? a : r)()),

                        (void 0 !==

                          (e = (function() {

                            var t,

                              e = {},

                              n = g.get('ajaxSettings') || {};

                            for (t in n) e[t] = n[t];

                            return e;

                          })()).url &&

                          '' !== e.url) ||

                        (e.url = g.get('url')),

                        (void 0 !== e.dataType && '' !== e.dataType) ||

                        (e.dataType = g.get('dataType')),

                        void 0 !== e.url &&

                        'list-required' !== e.url &&

                        ((e.url = e.url(i)),

                          (e.data = g.get('preparePostData')(e.data, i)),

                          $.ajax(e)

                          .done(function(t) {

                            var e,

                              n = d.init(t),

                              n = d.updateCategories(n, t);

                            (n = d.convertXml(n)),

                            (e = i),

                            (t = t),

                            (!1 !== g.get('matchResponseProperty') &&

                              ('string' == typeof g.get('matchResponseProperty') ?

                                t[g.get('matchResponseProperty')] !== e :

                                'function' ==

                                typeof g.get('matchResponseProperty') &&

                                g.get('matchResponseProperty')(t) !== e)) ||

                            s((n = d.processData(n, i)), i),

                              (d.checkIfDataExists(n) &&

                                0 < h.parent().find('li').length ?

                                a :

                                r)(),

                              g.get('ajaxCallback')();

                          })

                          .fail(function() {

                            l.warning('Fail to load response data');

                          })

                          .always(function() {})));

                  }

                  switch (t.keyCode) {

                    case 27:

                      r(), h.trigger('blur');

                      break;

                    case 38:

                      t.preventDefault(),

                        0 < y.length &&

                        0 < E &&

                        (--E, h.val(g.get('getValue')(y[E])), f(E));

                      break;

                    case 40:

                      t.preventDefault(),

                        0 < y.length &&

                        E < y.length - 1 &&

                        ((E += 1), h.val(g.get('getValue')(y[E])), f(E));

                      break;

                    default:

                      var n;

                      (40 < t.keyCode || 8 === t.keyCode) &&

                      ((n = h.val()),

                        !0 !== g.get('list').hideOnEmptyPhrase ||

                        8 !== t.keyCode ||

                        '' !== n ?

                        0 < g.get('requestDelay') ?

                        (void 0 !== c && clearTimeout(c),

                          (c = setTimeout(function() {

                            e(n);

                          }, g.get('requestDelay')))) :

                        e(n) :

                        r());

                  }

                }),

                h

                .on('keydown', function(t) {

                  return 38 === (t = t || window.event).keyCode ?

                    !(suppressKeypress = !0) :

                    void 0;

                })

                .keydown(function(t) {

                  13 === t.keyCode &&

                    -1 < E &&

                    (h.val(g.get('getValue')(y[E])),

                      g.get('list').onKeyEnterEvent(),

                      g.get('list').onChooseEvent(),

                      (E = -1),

                      r(),

                      t.preventDefault());

                }),

                h.off('keypress'),

                h.focus(function() {

                  '' !== h.val() && 0 < y.length && ((E = -1), a());

                }),

                void h.blur(function() {

                  setTimeout(function() {

                    (E = -1), r();

                  }, 250);

                })) :

              void l.error('Will not work without mentioned properties.') :

              void l.error(

                `One of options variables 'data' or 'url' must be defined.`

              );

          }



          function i() {

            var a, t, e;

            h.parent().hasClass(u.getValue('WRAPPER_CSS_CLASS')) &&

              (h.next('.' + u.getValue('CONTAINER_CLASS')).remove(), h.unwrap()),

              (t = $('<div>')),

              (e = u.getValue('WRAPPER_CSS_CLASS')),

              g.get('theme') &&

              '' !== g.get('theme') &&

              (e += ' eac-' + g.get('theme')),

              g.get('cssClasses') &&

              '' !== g.get('cssClasses') &&

              (e += ' ' + g.get('cssClasses')),

              '' !== m.getTemplateClass() && (e += ' ' + m.getTemplateClass()),

              t.addClass(e),

              h.wrap(t),

              !0 === g.get('adjustWidth') &&

              ((t = h.outerWidth()), h.parent().css('width', t)),

              (a = $('<div>').addClass(u.getValue('CONTAINER_CLASS')))

              .attr('id', o())

              .prepend($('<ul>')),

              a

              .on('show.eac', function() {

                switch (g.get('list').showAnimation.type) {

                  case 'slide':

                    var t = g.get('list').showAnimation.time,

                      e = g.get('list').showAnimation.callback;

                    a.find('ul').slideDown(t, e);

                    break;

                  case 'fade':

                    (t = g.get('list').showAnimation.time),

                    (e = g.get('list').showAnimation.callback);

                    a.find('ul').fadeIn(t);

                    break;

                  default:

                    a.find('ul').show();

                }

                g.get('list').onShowListEvent();

              })

              .on('hide.eac', function() {

                switch (g.get('list').hideAnimation.type) {

                  case 'slide':

                    var t = g.get('list').hideAnimation.time,

                      e = g.get('list').hideAnimation.callback;

                    a.find('ul').slideUp(t, e);

                    break;

                  case 'fade':

                    (t = g.get('list').hideAnimation.time),

                    (e = g.get('list').hideAnimation.callback);

                    a.find('ul').fadeOut(t, e);

                    break;

                  default:

                    a.find('ul').hide();

                }

                g.get('list').onHideListEvent();

              })

              .on('selectElement.eac', function() {

                a.find('ul li').removeClass('selected'),

                  a.find('ul li').eq(E).addClass('selected'),

                  g.get('list').onSelectItemEvent();

              })

              .on('loadElements.eac', function(t, o, r) {

                var s = '',

                  e = a.find('ul');

                e.empty().detach(), (y = []);

                for (var c = 0, u = 0, n = o.length; u < n; u += 1) {

                  var l = o[u].data;

                  if (0 !== l.length) {

                    void 0 !== o[u].header &&

                      0 < o[u].header.length &&

                      e.append(

                        `<div class='eac-category' >'` + o[u].header + `'</div>`

                      );

                    for (

                      var d = 0, i = l.length; d < i && c < o[u].maxListSize; d += 1

                    )

                      (s = $(`<li><div class='eac-item'></div></li>`)),

                      (function() {

                        var t,

                          e,

                          n = d,

                          i = c,

                          a = o[u].getValue(l[n]);

                        s.find(' > div')

                          .on('click', function() {

                            h.val(a).trigger('change'),

                              f((E = i)),

                              g.get('list').onClickEvent(),

                              g.get('list').onChooseEvent();

                          })

                          .mouseover(function() {

                            f((E = i)), g.get('list').onMouseOverEvent();

                          })

                          .mouseout(function() {

                            g.get('list').onMouseOutEvent();

                          })

                          .html(

                            m.build(

                              ((t = a),

                                (e = r),

                                g.get('highlightPhrase') && '' !== e ?

                                (function(t, e) {

                                  e = e.replace(

                                    /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,

                                    '\\$&'

                                  );

                                  return (t + '').replace(

                                    new RegExp('(' + e + ')', 'gi'),

                                    '<b>$1</b>'

                                  );

                                })(t, e) :

                                t),

                              l[n]

                            )

                          );

                      })(),

                      e.append(s),

                      y.push(l[d]),

                      (c += 1);

                  }

                }

                a.append(e), g.get('list').onLoadEvent();

              }),

              h.after(a),

              (v = $('#' + o())),

              g.get('placeholder') && h.attr('placeholder', g.get('placeholder'));

          }



          function o() {

            var t = h.attr('id');

            return u.getValue('CONTAINER_ID') + t;

          }



          function a() {

            v.trigger('show.eac');

          }



          function r() {

            v.trigger('hide.eac');

          }



          function f(t) {

            v.trigger('selectElement.eac', t);

          }



          function s(t, e) {

            v.trigger('loadElements.eac', [t, e]);

          }

          var c,

            u = new C.Constans(),

            g = new C.Configuration(e),

            l = new C.Logger(),

            m = new C.Template(e.template),

            d = new C.ListBuilderService(g, C.proccess),

            p = g.equals,

            h = t,

            v = '',

            y = [],

            E = -1;

          (C.consts = u),

          (this.getConstants = function() {

            return u;

          }),

          (this.getConfiguration = function() {

            return g;

          }),

          (this.getContainer = function() {

            return v;

          }),

          (this.getSelectedItemIndex = function() {

            return E;

          }),

          (this.getItems = function() {

            return y;

          }),

          (this.getItemData = function(t) {

            return y.length < t || void 0 === y[t] ? -1 : y[t];

          }),

          (this.getSelectedItemData = function() {

            return this.getItemData(E);

          }),

          (this.build = function() {

            i();

          }),

          (this.init = function() {

            n();

          });

        }),

        (C.eacHandles = []),

        (C.getHandle = function(t) {

          return C.eacHandles[t];

        }),

        (C.inputHasId = function(t) {

          return void 0 !== $(t).attr('id') && 0 < $(t).attr('id').length;

        }),

        (C.assignRandomId = function(t) {

          for (

            var e = '';

            (e = 'eac-' + Math.floor(1e4 * Math.random())), 0 !== $('#' + e).length;



          );

          (elementId = C.consts.getValue('CONTAINER_ID') + e), $(t).attr('id', e);

        }),

        (C.setHandle = function(t, e) {

          C.eacHandles[e] = t;

        }),

        C

      );

    })(

      (EasyAutocomplete = (function(t) {

        return (

          (t.Template = function(t) {

            var e,

              n,

              i,

              a,

              o,

              r = {

                basic: {

                  type: 'basic',

                  method: function(t) {

                    return t;

                  },

                  cssClass: '',

                },

                description: {

                  type: 'description',

                  fields: {

                    description: 'description'

                  },

                  method: function(t) {

                    return t + ' - description';

                  },

                  cssClass: 'eac-description',

                },

                iconLeft: {

                  type: 'iconLeft',

                  fields: {

                    icon: ''

                  },

                  method: function(t) {

                    return t;

                  },

                  cssClass: 'eac-icon-left',

                },

                iconRight: {

                  type: 'iconRight',

                  fields: {

                    iconSrc: ''

                  },

                  method: function(t) {

                    return t;

                  },

                  cssClass: 'eac-icon-right',

                },

                links: {

                  type: 'links',

                  fields: {

                    link: ''

                  },

                  method: function(t) {

                    return t;

                  },

                  cssClass: '',

                },

                custom: {

                  type: 'custom',

                  method: function() {},

                  cssClass: ''

                },

              };

            (this.getTemplateClass =

              (a = t) && a.type && a.type && r[a.type] ?

              ((o = r[a.type].cssClass),

                function() {

                  return o;

                }) :

              function() {

                return '';

              }),

            (this.build =

              (e = t) && e.type && e.type && r[e.type] ?

              ((i = (e = e).fields),

                'description' === e.type ?

                ((n = r.description.method),

                  'string' == typeof i.description ?

                  (n = function(t, e) {

                    return t + ' - <span>' + e[i.description] + '</span>';

                  }) :

                  'function' == typeof i.description &&

                  (n = function(t, e) {

                    return t + ' - <span>' + i.description(e) + '</span>';

                  }),

                  n) :

                'iconRight' === e.type ?

                ('string' == typeof i.iconSrc ?

                  (n = function(t, e) {

                    return (

                      t +

                      `<img class='eac-icon' src=''` +

                      e[i.iconSrc] +

                      ` />`

                    );

                  }) :

                  'function' == typeof i.iconSrc &&

                  (n = function(t, e) {

                    return (

                      t +

                      `<img class='eac-icon' src=''` +

                      i.iconSrc(e) +

                      ` />`

                    );

                  }),

                  n) :

                'iconLeft' === e.type ?

                ('string' == typeof i.iconSrc ?

                  (n = function(t, e) {

                    return (

                      `<img class='eac-icon' src=''` +

                      e[i.iconSrc] +

                      ` />` +

                      t

                    );

                  }) :

                  'function' == typeof i.iconSrc &&

                  (n = function(t, e) {

                    return (

                      `<img class='eac-icon' src=''` +

                      i.iconSrc(e) +

                      ' />' +

                      t

                    );

                  }),

                  n) :

                'links' === e.type ?

                ('string' == typeof i.link ?

                  (n = function(t, e) {

                    return `<a href=''` + e[i.link] + `' >` + t + `</a>`;

                  }) :

                  'function' == typeof i.link &&

                  (n = function(t, e) {

                    return `<a href=''` + i.link(e) + `' >` + t + `</a>`;

                  }),

                  n) :

                ('custom' === e.type ? e : r.basic).method) :

              r.basic.method);

          }),

          t

        );

      })(

        (EasyAutocomplete = (function(s) {

          return (

            (s.proccess = function(o, t, e) {

              function r(t, e) {

                return (

                  o.get('list').match.caseSensitive ||

                  ('string' == typeof t && (t = t.toLowerCase()),

                    (e = e.toLowerCase())),

                  !!o.get('list').match.method(t, e)

                );

              }

              s.proccess.match = r;

              var n,

                i,

                a = (function(t, e) {

                  var n = [];

                  if (o.get('list').match.enabled)

                    for (var i = 0, a = t.length; i < a; i += 1)

                      r(o.get('getValue')(t[i]), e) && n.push(t[i]);

                  else n = t;

                  return n;

                })((a = t.data), e);

              return (

                (i = a),

                (a = i =

                  void 0 !== t.maxNumberOfElements &&

                  i.length > t.maxNumberOfElements ?

                  i.slice(0, t.maxNumberOfElements) :

                  i),

                (n = a),

                o.get('list').sort.enabled && n.sort(o.get('list').sort.method),

                n

              );

            }),

            s

          );

        })(

          (EasyAutocomplete = (function(t) {

            return (

              (t.ListBuilderService = function(s, a) {

                function o(t, e) {

                  var n,

                    i,

                    a,

                    o,

                    r = {},

                    r =

                    'XML' === s.get('dataType').toUpperCase() ?

                    ((o = {}),

                      void 0 !== t.xmlElementName &&

                      (o.xmlElementName = t.xmlElementName),

                      void 0 !== t.listLocation ?

                      (a = t.listLocation) :

                      void 0 !== s.get('listLocation') &&

                      (a = s.get('listLocation')),

                      void 0 !== a ?

                      'string' == typeof a ?

                      (o.data = $(e).find(a)) :

                      'function' == typeof a && (o.data = a(e)) :

                      (o.data = e),

                      o) :

                    ((i = {}),

                      void 0 !== t.listLocation ?

                      'string' == typeof t.listLocation ?

                      (i.data = e[t.listLocation]) :

                      'function' == typeof t.listLocation &&

                      (i.data = t.listLocation(e)) :

                      (i.data = e),

                      i);

                  return (

                    void 0 !== t.header && (r.header = t.header),

                    void 0 !== t.maxNumberOfElements &&

                    (r.maxNumberOfElements = t.maxNumberOfElements),

                    void 0 !== s.get('list').maxNumberOfElements &&

                    (r.maxListSize = s.get('list').maxNumberOfElements),

                    void 0 !== t.getValue ?

                    'string' == typeof t.getValue ?

                    ((n = t.getValue),

                      (r.getValue = function(t) {

                        return t[n];

                      })) :

                    'function' == typeof t.getValue &&

                    (r.getValue = t.getValue) :

                    (r.getValue = s.get('getValue')),

                    r

                  );

                }

                (this.init = function(t) {

                  var e = [],

                    n = {};

                  return (

                    (n.data = s.get('listLocation')(t)),

                    (n.getValue = s.get('getValue')),

                    (n.maxListSize = s.get('list').maxNumberOfElements),

                    e.push(n),

                    e

                  );

                }),

                (this.updateCategories = function(t, e) {

                  if (s.get('categoriesAssigned')) {

                    t = [];

                    for (var n = 0; n < s.get('categories').length; n += 1) {

                      var i = o(s.get('categories')[n], e);

                      t.push(i);

                    }

                  }

                  return t;

                }),

                (this.convertXml = function(t) {

                  if ('XML' === s.get('dataType').toUpperCase())

                    for (var e = 0; e < t.length; e += 1)

                      t[e].data = (function(t) {

                        var e = [];

                        return (

                          void 0 === t.xmlElementName &&

                          (t.xmlElementName = s.get('xmlElementName')),

                          $(t.data)

                          .find(t.xmlElementName)

                          .each(function() {

                            e.push(this);

                          }),

                          e

                        );

                      })(t[e]);

                  return t;

                }),

                (this.processData = function(t, e) {

                  for (var n = 0, i = t.length; n < i; n += 1)

                    t[n].data = a(s, t[n], e);

                  return t;

                }),

                (this.checkIfDataExists = function(t) {

                  for (var e = 0, n = t.length; e < n; e += 1)

                    if (

                      void 0 !== t[e].data &&

                      t[e].data instanceof Array &&

                      0 < t[e].data.length

                    )

                      return !0;

                  return !1;

                });

              }),

              t

            );

          })(

            (EasyAutocomplete = (function(t) {

              return (

                (t.Constans = function() {

                  var e = {

                    CONTAINER_CLASS: 'easy-autocomplete-container',

                    CONTAINER_ID: 'eac-container-',

                    WRAPPER_CSS_CLASS: 'easy-autocomplete',

                  };

                  this.getValue = function(t) {

                    return e[t];

                  };

                }),

                t

              );

            })(

              (EasyAutocomplete = (function(t) {

                return (

                  (t.Logger = function() {

                    (this.error = function(t) {}),

                    (this.warning = function(t) {});

                  }),

                  t

                );

              })(

                (EasyAutocomplete = (function(t) {

                  return (

                    (t.Configuration = function(o) {

                      function n(a, t) {

                        !(function t(e, n) {

                          for (var i in n)

                            void 0 === e[i] &&

                            a.log(

                              `Property '` +

                              i +

                              `' does not exist in EasyAutocomplete options API.`

                            ),

                            'object' == typeof e[i] &&

                            -1 === $.inArray(i, s) &&

                            t(e[i], n[i]);

                        })(r, t);

                      }

                      var t,

                        e,

                        i,

                        r = {

                          data: 'list-required',

                          url: 'list-required',

                          dataType: 'json',

                          listLocation: function(t) {

                            return t;

                          },

                          xmlElementName: '',

                          getValue: function(t) {

                            return t;

                          },

                          autocompleteOff: !0,

                          placeholder: !1,

                          ajaxCallback: function() {},

                          matchResponseProperty: !1,

                          list: {

                            sort: {

                              enabled: !1,

                              method: function(t, e) {

                                return (t = r.getValue(t)) < (e = r.getValue(e)) ?

                                  -1 :

                                  e < t ?

                                  1 :

                                  0;

                              },

                            },

                            maxNumberOfElements: 6,

                            hideOnEmptyPhrase: !0,

                            match: {

                              enabled: !1,

                              caseSensitive: !1,

                              method: function(t, e) {

                                return -1 < t.search(e);

                              },

                            },

                            showAnimation: {

                              type: 'normal',

                              time: 400,

                              callback: function() {},

                            },

                            hideAnimation: {

                              type: 'normal',

                              time: 400,

                              callback: function() {},

                            },

                            onClickEvent: function() {},

                            onSelectItemEvent: function() {},

                            onLoadEvent: function() {},

                            onChooseEvent: function() {},

                            onKeyEnterEvent: function() {},

                            onMouseOverEvent: function() {},

                            onMouseOutEvent: function() {},

                            onShowListEvent: function() {},

                            onHideListEvent: function() {},

                          },

                          highlightPhrase: !0,

                          theme: '',

                          cssClasses: '',

                          minCharNumber: 0,

                          requestDelay: 0,

                          adjustWidth: !0,

                          ajaxSettings: {},

                          preparePostData: function(t, e) {

                            return t;

                          },

                          loggerEnabled: !0,

                          template: '',

                          categoriesAssigned: !1,

                          categories: [{

                            maxNumberOfElements: 4

                          }],

                        },

                        s = ['ajaxSettings', 'template'];

                      (this.get = function(t) {

                        return r[t];

                      }),

                      (this.equals = function(t, e) {

                        return !(

                          void 0 === r[(n = t)] ||

                          null === r[n] ||

                          r[t] !== e

                        );

                        var n;

                      }),

                      (this.checkDataUrlProperties = function() {

                        return (

                          'list-required' !== r.url || 'list-required' !== r.data

                        );

                      }),

                      (this.checkRequiredProperties = function() {

                        for (var t in r)

                          if ('required' === r[t])

                            return (

                              logger.error('Option ' + t + ' must be defined'), !1

                            );

                        return !0;

                      }),

                      (this.printPropertiesThatDoesntExist = function(t, e) {

                        n(t, e);

                      }),

                      (function() {

                        if (

                          ('xml' === o.dataType &&

                            (o.getValue ||

                              (o.getValue = function(t) {

                                return $(t).text();

                              }),

                              o.list || (o.list = {}),

                              o.list.sort || (o.list.sort = {}),

                              (o.list.sort.method = function(t, e) {

                                return (t = o.getValue(t)) < (e = o.getValue(e)) ?

                                  -1 :

                                  e < t ?

                                  1 :

                                  0;

                              }),

                              o.list.match || (o.list.match = {}),

                              (o.list.match.method = function(t, e) {

                                return -1 < t.search(e);

                              })),

                            void 0 !== o.categories &&

                            o.categories instanceof Array)

                        ) {

                          for (

                            var t = [], e = 0, n = o.categories.length; e < n; e += 1

                          ) {

                            var i,

                              a = o.categories[e];

                            for (i in r.categories[0])

                              void 0 === a[i] && (a[i] = r.categories[0][i]);

                            t.push(a);

                          }

                          o.categories = t;

                        }

                      })(),

                      !0 ===

                        (r = (function t(e, n) {

                          var i,

                            a = e || {};

                          for (i in e)

                            void 0 !== n[i] &&

                            null !== n[i] &&

                            ('object' != typeof n[i] || n[i] instanceof Array ?

                              (a[i] = n[i]) :

                              t(e[i], n[i]));

                          return (

                            void 0 !== n.data &&

                            null !== n.data &&

                            'object' == typeof n.data &&

                            (a.data = n.data),

                            a

                          );

                        })(r, o)).loggerEnabled && n(console, o),

                        void 0 !== o.ajaxSettings &&

                        'object' == typeof o.ajaxSettings ?

                        (r.ajaxSettings = o.ajaxSettings) :

                        (r.ajaxSettings = {}),

                        'list-required' !== r.url &&

                        'function' != typeof r.url &&

                        ((t = r.url),

                          (r.url = function() {

                            return t;

                          })),

                        void 0 !== r.ajaxSettings.url &&

                        'function' != typeof r.ajaxSettings.url &&

                        ((t = r.ajaxSettings.url),

                          (r.ajaxSettings.url = function() {

                            return t;

                          })),

                        'string' == typeof r.listLocation &&

                        ((e = r.listLocation),

                          'XML' === r.dataType.toUpperCase() ?

                          (r.listLocation = function(t) {

                            return $(t).find(e);

                          }) :

                          (r.listLocation = function(t) {

                            return t[e];

                          })),

                        'string' == typeof r.getValue &&

                        ((i = r.getValue),

                          (r.getValue = function(t) {

                            return t[i];

                          })),

                        void 0 !== o.categories && (r.categoriesAssigned = !0);

                    }),

                    t

                  );

                })(EasyAutocomplete || {})) || {}

              )) || {}

            )) || {}

          )) || {}

        )) || {}

      )) || {}

    );

    !(function(i) {

      (i.fn.easyAutocomplete = function(n) {

        return this.each(function() {

          var t = i(this),

            e = new EasyAutocomplete.main(t, n);

          EasyAutocomplete.inputHasId(t) || EasyAutocomplete.assignRandomId(t),

            e.init(),

            EasyAutocomplete.setHandle(e, t.attr('id'));

        });

      }),

      (i.fn.getSelectedItemIndex = function() {

        var t = i(this).attr('id');

        return void 0 !== t ?

          EasyAutocomplete.getHandle(t).getSelectedItemIndex() :

          -1;

      }),

      (i.fn.getItems = function() {

        var t = i(this).attr('id');

        return void 0 !== t ? EasyAutocomplete.getHandle(t).getItems() : -1;

      }),

      (i.fn.getItemData = function(t) {

        var e = i(this).attr('id');

        return void 0 !== e && -1 < t ?

          EasyAutocomplete.getHandle(e).getItemData(t) :

          -1;

      }),

      (i.fn.getSelectedItemData = function() {

        var t = i(this).attr('id');

        return void 0 !== t ?

          EasyAutocomplete.getHandle(t).getSelectedItemData() :

          -1;

      });

    })(jQuery);

    /*=====  End of AUTOCOMPLETE LIBRARY  ======*/





    /*=============================================

    =            INPUT ON CHANGE ORIGEN            =

    =============================================*/

    // TAB BUS

    let inputOrigen = document.getElementById('origen')

    inputOrigen.addEventListener('change', (e) => {



      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputOrigenId = document.getElementById('origen-id')

      let inputOrigenContainer = inputOrigen.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputOrigenId.value.trim() === '' || inputOrigen.value <= 0) {

        inputOrigenContainer.classList.add('validation');

      } else {

        inputOrigenContainer.classList.remove('validation');

      }

    })

    inputOrigen.addEventListener('blur', (e) => {

      let inputOrigenId = document.getElementById('origen-id')



      let inputOrigenContainer = inputOrigen.parentNode.parentNode.parentNode.parentNode.parentNode

      if (inputOrigenId.value.trim() === '' || inputOrigen.value <= 0) {

        inputOrigenContainer.classList.add('validation');

      } else {

        inputOrigenContainer.classList.remove('validation');

      }

    })



    // TAB PAQUETES

    let inputOrigenPaquetes = document.getElementById('p-origen')

    inputOrigenPaquetes.addEventListener('change', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputOrigenPaquetesId = document.getElementById('p-origen-id')

      let inputOrigenPaquetesContainer = inputOrigenPaquetes.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputOrigenPaquetesId.value.trim() === '' || inputOrigenPaquetes.value <= 0) {

        inputOrigenPaquetesContainer.classList.add('validation');

      } else {

        inputOrigenPaquetesContainer.classList.remove('validation');

      }

    })

    inputOrigenPaquetes.addEventListener('blur', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputOrigenPaquetesId = document.getElementById('p-origen-id')

      let inputOrigenPaquetesContainer = inputOrigenPaquetes.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputOrigenPaquetesId.value.trim() === '' || inputOrigenPaquetes.value <= 0) {

        inputOrigenPaquetesContainer.classList.add('validation');

      } else {

        inputOrigenPaquetesContainer.classList.remove('validation');

      }

    })

    /*=====  End of INPUT ON CHANGE ORIGEN  ======*/





    /*=============================================

    =            INPUT ON CHANGE DESTINO            =

    =============================================*/

    // TAB BUS

    let inputDestino = document.getElementById('destino')

    inputDestino.addEventListener('change', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputDestinoId = document.getElementById('destino-id')

      let inputDestinoContainer = inputDestino.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoId.value.trim() === '' || inputDestino.value <= 0) {

        inputDestinoContainer.classList.add('validation');

      } else {

        inputDestinoContainer.classList.remove('validation');

      }

    })



    inputDestino.addEventListener('blur', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputDestinoId = document.getElementById('destino-id')

      let inputDestinoContainer = inputDestino.parentNode.parentNode.parentNode.parentNode.parentNode

      if (inputDestinoId.value.trim() === '' || inputDestino.value <= 0) {

        inputDestinoContainer.classList.add('validation');

      } else {

        inputDestinoContainer.classList.remove('validation');

      }

    })



    // HOTEL

    let inputDestinoHotel = document.getElementById('h-destino');

    inputDestinoHotel.addEventListener('change', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE HOTEL */

      let inputDestinoHotelId = document.getElementById('h-destino-id')

      let inputDestinoHotelContainer = inputDestinoHotel.parentNode.parentNode.parentNode.parentNode.parentNode



      if (e.target.value.length <= 0 || inputDestinoHotelId.value.trim() === '') {

        inputDestinoHotelContainer.classList.add('validation');

      } else {

        inputDestinoHotelContainer.classList.remove('validation');

      }

    })

    inputDestinoHotel.addEventListener('blur', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE HOTEL */



      let inputDestinoHotelId = document.getElementById('h-destino-id')

      let inputDestinoHotelContainer = inputDestinoHotel.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoHotelId.value.trim() === '' || inputDestinoHotel.value <= 0) {

        inputDestinoHotelContainer.classList.add('validation');

      } else {

        inputDestinoHotelContainer.classList.remove('validation');

      }

    })



    // Actividades

    let inputDestinoActividades = document.getElementById('a-destino');

    let regExp = /\(([^)]+)\)/;

    inputDestinoActividades.addEventListener('change', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE HOTEL */

      let inputDestinoActividadesId = document.getElementById('a-destino-id')

      // let matches = regExp.exec(inputDestinoActividades.value)

      // let newValue = matches[1].substr(-2)

      // let newMatchesId = inputDestinoActividadesId.value.substr(-2)

      let inputDestinoActividadesContainer = inputDestinoActividades.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoActividadesId.value.trim() === '') {

        inputDestinoActividadesContainer.classList.add('validation');

      } else {

        inputDestinoActividadesContainer.classList.remove('validation');

      }

    })

    inputDestinoActividades.addEventListener('blur', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE HOTEL */

      let inputDestinoActividadesId = document.getElementById('a-destino-id')

      let inputDestinoActividadesContainer = inputDestinoActividades.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoActividadesId.value.trim() === '') {

        inputDestinoActividadesContainer.classList.add('validation');

      } else {

        inputDestinoActividadesContainer.classList.remove('validation');

      }

    })



    // Paquetes

    let inputDestinoPaquetes = document.getElementById('p-destino')

    inputDestinoPaquetes.addEventListener('change', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputDestinoPaquetesId = document.getElementById('p-destino-id')

      let inputOrigenPaquetesContainer = inputDestinoPaquetes.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoPaquetesId.value.trim() === '' || inputDestinoPaquetes.value <= 0) {

        inputOrigenPaquetesContainer.classList.add('validation');

      } else {

        inputOrigenPaquetesContainer.classList.remove('validation');

      }

    })

    inputDestinoPaquetes.addEventListener('blur', (e) => {

      /* VALIDACIÓN EN TIEMPO REAL CHANGE BUS */

      let inputDestinoPaquetesId = document.getElementById('p-destino-id')

      let inputDestinoPaquetesContainer = inputDestinoPaquetes.parentNode.parentNode.parentNode.parentNode.parentNode

      if (e.target.value.length <= 0 || inputDestinoPaquetesId.value.trim() === '' || inputDestinoPaquetes.value <= 0) {

        inputDestinoPaquetesContainer.classList.add('validation');

      } else {

        inputDestinoPaquetesContainer.classList.remove('validation');

      }

    })

    /*=====  End of INPUT ON CHANGE DESTINO  ======*/





    /*=============================================

    =            FOCUS SELECTION            =

    =============================================*/

    const selectionValue = () => {

      const inputs = document.querySelectorAll('.input-text');

      inputs.forEach(input => {

        input.addEventListener('focus', (e) => {

          input.setSelectionRange(0, input.value.length);

        })

      });

    }



    selectionValue()

    /*=====  End of FOCUS SELECTION  ======*/







    /*=============================================

    =            AUTO COMPLETE ORIGEN            =

    =============================================*/

    // AUTOCOMPLETE TAB BUS

    var external_file_Cities = ["Andorra La Vella, Andorra (ALV)","Al Ain, Emiratos Arabes (AAN)","Abu Dhabi, Emiratos Arabes (AUH)","Dubai, Emiratos Arabes (DXB)","Al Fujairah, Emiratos Arabes (FJR)","Ras Al Khaimah, Emiratos Arabes (RKT)","Sharjah, Emiratos Arabes (SHJ)","Antigua, Antigua y Barbuda (ANU)","Anguilla, Anguilla (AXA)","Tirana, Albania (TIA)","Erevan, Armenia (EVN)","St Eustatius, Antillas Holandesas (EUX)","Bonaire, Antillas Holandesas (BON)","St. Maarten, Antillas Holandesas (SXM)","Saba Island, Antillas Holandesas (SAB)","Philisburg - St. Maarten, Antillas Holandesas (PH1)","Luanda, Angola (LAD)","Pago Pago, Samoa Americana (PPG)","Klagenfurt, Austria (KLU)","Innsbruck, Austria (INN)","Linz, Austria (LNZ)","Graz, Austria (GRZ)","Salzburgo, Austria (SZG)","Viena, Austria (VIE)","Warwick, Australia (WAZ)","Weipa, Australia (WEI)","Wee Waa, Australia (WEW)","Wagga Wagga, Australia (WGA)","Walgett, Australia (WGE)","Wangaratta, Australia (WGT)","Welshpool, Australia (WHL)","Warrnambool, Australia (WMB)","Wollongong, Australia (WOL)","Windorah, Australia (WNR)","Airlie Beach, Australia (WSY)","Whyalla, Australia (WYA)","Wyndham, Australia (WYN)","Colac, Australia (XCO)","Yandicoogina, Australia (YNN)","Biloela, Australia (ZBL)","Newman, Australia (ZNE)","Sydney, Australia (SYD)","Sale, Australia (SXE)","Swan Hill, Australia (SWH)","Tennant Creek, Australia (TCA)","Tocumwal, Australia (TCW)","Traralgon, Australia (TGN)","Thangool, Australia (THG)","Thursday Island, Australia (TIS)","Tamworth, Australia (TMW)","Tom Price, Australia (TPR)","Taree, Australia (TRO)","Townsville, Australia (TSV)","Toowoomba, Australia (TWB)","Queenstown, Australia (UEE)","Quirindi, Australia (UIR)","Quilpie, Australia (ULP)","Useless Loop, Australia (USL)","Rottnest, Australia (RTS)","Roma, Australia (RMA)","Renmark, Australia (RMK)","Rockhampton, Australia (ROK)","St George, Australia (SGO)","Shepparton, Australia (SHT)","Singleton, Australia (SIX)","Stanthorpe, Australia (SNH)","South Molle, Australia (SOI)","Strahan, Australia (SRN)","Lock, Australia (LOC)","Gove, Australia (GOV)","Goondiwindi, Australia (GOO)","Geelong, Australia (GEX)","Griffith, Australia (GFF)","Grafton, Australia (GFN)","Glen Innes, Australia (GLI)","Gladstone, Australia (GLT)","Forster, Australia (FOT)","Geraldton, Australia (GET)","Gunnedah, Australia (GUH)","Gympie, Australia (GYP)","Long Island, Australia (HAP)","Hobart, Australia (HBA)","Horn Island, Australia (HID)","Hayman Island, Australia (HIS)","St Helens, Australia (HLS)","Hamilton Island, Australia (HTI)","Exmouth Gulf, Australia (EXM)","Esperance, Australia (EPR)","Echuca, Australia (ECH)","Emerald, Australia (EMD)","Casino, Australia (CSI)","Charleville, Australia (CTL)","Cudal, Australia (CUG)","Carnarvon, Australia (CVQ)","Cowra, Australia (CWT)","Dubbo, Australia (DBO)","Dalby, Australia (DBY)","Daydream Island, Australia (DDI)","Mudgee, Australia (DGE)","Dunk Island, Australia (DKI)","Deniliquin, Australia (DNQ)","Dongara, Australia (DOX)","Devonport, Australia (DPO)","Derby, Australia (DRB)","Darwin, Australia (DRW)","Borroloola, Australia (BOX)","Busselton, Australia (BQB)","Bourke, Australia (BRK)","Bathurst Isl, Australia (BRT)","Blackwater, Australia (BLT)","Broome, Australia (BME)","Brisbane, Australia (BNE)","Ballina, Australia (BNK)","Bairnsdale, Australia (BSJ)","Bunbury, Australia (BUY)","Brewarrina, Australia (BWQ)","Burnie, Australia (BWT)","Bankstown, Australia (BWU)","Cobar, Australia (CAZ)","Camberra, Australia (CBR)","Cooinda, Australia (CDA)","Ceduna, Australia (CED)","Cessnock, Australia (CES)","Coffs Harbour, Australia (CFS)","Clermont, Australia (CMQ)","Coonamble, Australia (CNB)","Cloncurry, Australia (CNJ)","Cairns, Australia (CNS)","Coonabarabrn, Australia (COJ)","Coober Pedy, Australia (CPD)","Avalon, Australia (AVV)","Ayers Rock, Australia (AYQ)","Ayr Au, Australia (AYR)","Armidale, Australia (ARM)","Alice Springs, Australia (ASP)","Bundaberg, Australia (BDB)","Broken Hill, Australia (BHQ)","Bathurst, Australia (BHS)","Albany, Australia (ALH)","Bamaga, Australia (ABM)","Albury, Australia (ABX)","Adelaida, Australia (ADL)","Leonora, Australia (LNO)","Longreach, Australia (LRE)","Launceston, Australia (LST)","Lismore, Australia (LSY)","Laverton, Australia (LVO)","Lord Howe Island, Australia (LDH)","Learmonth, Australia (LEA)","Leinster, Australia (LER)","Lightning Ridge, Australia (LHG)","Maryborough, Australia (MBH)","Moorabbin, Australia (MBW)","Maroochydore, Australia (MCY)","Melbourne, Australia (MEL)","Mt Gambier, Australia (MGB)","Margaret River Station, Australia (MGV)","Mount Hotham, Australia (MHU)","Merimbula, Australia (MIM)","Meekathara, Australia (MKR)","Mackay, Australia (MKY)","Hervey Bay, Australia (HVB)","Mount Isa, Australia (ISA)","Inverell, Australia (IVR)","Jabiru, Australia (JAB)","Fremantle, Australia (JFM)","Shute Hrb, Australia (JHQ)","King Island, Australia (KNS)","Kings Canyon, Australia (KBJ)","Collinsville, Australia (KCE)","Kingscote, Australia (KGC)","Kalgoorlie, Australia (KGI)","Kununurra, Australia (KNX)","Kempsey, Australia (KPS)","Karratha, Australia (KTA)","Katherine, Australia (KTR)","Port Pirie, Australia (PPI)","Proserpine, Australia (PPP)","Pt Macquarie, Australia (PQQ)","Port Lincoln, Australia (PLO)","Paraburdoo, Australia (PBO)","Perth, Australia (PER)","Port Hedland, Australia (PHE)","Parkes, Australia (PKE)","Port Douglas, Australia (PTI)","Portland, Australia (PTJ)","Port Augusta, Australia (PUG)","Leeton, Australia (QLE)","Mount Magnet, Australia (MMG)","Middlemount, Australia (MMM)","Monto, Australia (MNQ)","Mildura, Australia (MQL)","Margaret River, Australia (MQZ)","Moree, Australia (MRZ)","Maitland, Australia (MTL)","Moruya, Australia (MYA)","Narrabri, Australia (NAA)","Young, Australia (NGA)","Norfolk Island, Australia (NLK)","Nowra, Australia (NOA)","Narrandera, Australia (NRA)","Noosa, Australia (NSA)","Norseman, Australia (NSM)","Scone, Australia (NSO)","Newcastle, Australia (NTL)","Nullarbor, Australia (NUR)","Nyngan, Australia (NYN)","Orange, Australia (OAG)","Olympic Dam, Australia (OLP)","Gold Coast, Australia (OOL)","Cooma, Australia (OOM)","Orpheus Island, Australia (ORS)","Baku, Azerbaijan (BAK)","Banja Luka, Bosnia Herzegovina (BNX)","Mostar, Bosnia Herzegovina (OMO)","Sarajevo, Bosnia Herzegovina (SJJ)","Tuzla, Bosnia Herzegovina (TZL)","Barbados, Barbados (BGI)","Chittagong, Bangladesh (CGP)","Dhaka, Bangladesh (DAC)","Sylhet, Bangladesh (ZYL)","Leuven, Belgium (ZGK)","Bruselas, Belgium (BRU)","Antwerp, Belgium (ANR)","Brujas, Belgium (OST)","Kortrijk, Belgium (KJK)","Liege, Belgium (LGG)","Ouagadougou, Burkina Faso (OUA)","Bourgas, Bulgaria (BOJ)","Varna, Bulgaria (VAR)","Sofia, Bulgaria (SOF)","Bahrain, Bahrain (BAH)","Bujumbura, Burundi (BJM)","Cotonou, Benin (COO)","Bermuda, Bermuda (BDA)","Bandar Seri, Brunei (BWN)","Kuala Belait, Brunei (KUB)","La Paz, Bolivia (LPB)","San Matias, Bolivia (MQK)","Puerto Suarez, Bolivia (PSZ)","Camiri, Bolivia (CAM)","Cochabamba, Bolivia (CBB)","Sucre, Bolivia (SRE)","Santa Cruz De La Sierra, Bolivia (SRZ)","Tarija, Bolivia (TJA)","Trinidad, Bolivia (TDD)","San Ignacio de Velasco, Bolivia (SNG)","Yacuiba, Bolivia (BYC)","Riberalta, Bolivia (RIB)","Uyuni, Bolivia (UYU)","Rurrenabaque, Bolivia (RBQ)","Guayaramerin, Bolivia (GYA)","Cobija, Bolivia (CIJ)","Oruro, Bolivia (ORU)","PotosÃ­, Bolivia (POI)","Monteagudo, Bolivia (MHW)","Chimore, Bolivia (CCA)","Iguazu Falls, Brasil (IUF)","Praia de Cumbuco, Brasil (BR1)","Arraial d'Ajuda, Brasil (BR2)","Trancoso, Brasil (BR3)","Barra da Tijuca, Brasil (BR4)","Copacabana, Brasil (BR5)","Ipanema, Brasil (BR6)","Praia do Forte, Brasil (BR7)","ImbassaÃ­, Brasil (BR8)","GuarujÃ¡, Brasil (BR9)","Olinda, Brasil (B03)","Campos do JordÃ£o, Brasil (B05)","BalneÃ¡rio de CamboriÃº, Brasil (B06)","JaraguÃ¡ do Sul, Brasil (B11)","IlhÃ©us, Brasil (B12)","Vila Velha, Brasil (B15)","Santo AndrÃ©, Brasil (B16)","ItacarÃ©, Brasil (B18)","Tibau do Sul, Brasil (B20)","Canoa Quebrada, Brasil (B21)","PetrÃ³polis, Brasil (B23)","Praia da Pipa, Brasil (PP2)","Bonito, Brasil (BR_4132)","AcailÃ¢ndia, Brasil (BR_10149)","Amparo, Brasil (BR_10164)","BarÃ£o do MelgaÃ§o, Brasil (BR_10148)","Belmonte, Brasil (BR_10156)","GuaÃ­ra, Brasil (BR_10151)","Praia Grande, Brasil (BR_10152)","Rio Claro, Brasil (BR_10154)","Rio Quente, Brasil (RQR)","Santa Luzia, Brasil (BR_10153)","SÃ£o Carlos, Brasil (BR_10147)","SÃ£o JoÃ£o del-Rei, Brasil (BR_10155)","SÃ£o Miguel do Gostoso, Brasil (BR_10150)","SÃ£o SebastiÃ£o, Brasil (BR_10146)","SertÃ£ozinho, Brasil (BR_10161)","Una, Brasil (BR_10157)","CALDAS NOVAS, Brasil (CLV)","Gramado, Brasil (GM1)","CMNET, Brasil (FSG)","Santo AntÃ´nio de Jesus, Brasil (BR_1006)","Teixeira de Freitas, Brasil (BR_1078)","ValenÃ§a, Brasil (BR_1102)","Marechal Deodoro, Brasil (BR_114)","Aquiraz, Brasil (BR_1156)","Aracati, Brasil (BR_1158)","Beberibe, Brasil (BR_1208)","Boa Viagem, Brasil (BR_1218)","Camocim, Brasil (BR_1247)","Cascavel, Brasil (BR_1278)","Caucaia, Brasil (BR_1284)","Cruz, Brasil (BR_1308)","Fortim, Brasil (BR_1348)","Jijoca de Jericoacoara, Brasil (BR_1443)","MaracanaÃº, Brasil (BR_1486)","Paracuru, Brasil (BR_1577)","SÃ£o Miguel dos Milagres, Brasil (BR_171)","SÃ£o SebastiÃ£o, Brasil (BR_172)","Sobral, Brasil (BR_1722)","Trairi, Brasil (BR_1746)","Aracruz, Brasil (BR_1816)","Colatina, Brasil (BR_1847)","ConceiÃ§Ã£o da Barra, Brasil (BR_1848)","Domingos Martins, Brasil (BR_1861)","Guarapari, Brasil (BR_1879)","ViÃ§osa, Brasil (BR_188)","Linhares, Brasil (BR_1925)","Serra, Brasil (BR_2028)","Venda Nova do Imigrante, Brasil (BR_2037)","Vila Velha, Brasil (BR_2044)","Alto ParaÃ­so de GoiÃ¡s, Brasil (BR_2060)","Buriti Alegre, Brasil (BR_2096)","CatalÃ£o, Brasil (BR_2125)","Itumbiara, Brasil (BR_2204)","JataÃ­, Brasil (BR_2209)","Mineiros, Brasil (BR_2234)","Rio Verde, Brasil (BR_2298)","Lagoa Santa, Brasil (BR_2347)","Balsas, Brasil (BR_2395)","Barreirinhas, Brasil (BR_2399)","Santa InÃªs, Brasil (BR_2562)","Santa Luzia, Brasil (BR_2563)","Alfenas, Brasil (BR_2651)","Araguari, Brasil (BR_2695)","AraxÃ¡, Brasil (BR_2704)","Betim, Brasil (BR_2762)","CaetÃ©, Brasil (BR_2834)","Caxambu, Brasil (BR_2918)","Confins, Brasil (BR_2971)","Conselheiro Lafaiete, Brasil (BR_2976)","Contagem, Brasil (BR_2980)","Coronel Fabriciano, Brasil (BR_2990)","Arapiraca, Brasil (BR_30)","Diamantina, Brasil (BR_3036)","DivinÃ³polis, Brasil (BR_3047)","Formiga, Brasil (BR_3128)","Itabira, Brasil (BR_3234)","ItajubÃ¡, Brasil (BR_3245)","Ituiutaba, Brasil (BR_3275)","Lagoa Santa, Brasil (BR_3332)","Lambari, Brasil (BR_3334)","Luz, Brasil (BR_3360)","Alagoinhas, Brasil (BR_342)","MuriaÃ©, Brasil (BR_3460)","Nova Lima, Brasil (BR_3480)","Nova Serrana, Brasil (BR_3486)","Ouro Preto, Brasil (BR_3507)","ParÃ¡ de Minas, Brasil (BR_3531)","Paracatu, Brasil (BR_3532)","Passos, Brasil (BR_3547)","Patos de Minas, Brasil (BR_3549)","PoÃ§os de Caldas, Brasil (BR_3632)","Pouso Alegre, Brasil (BR_3654)","Barra de SÃ£o Miguel, Brasil (BR_37)","Santa Luzia, Brasil (BR_3749)","SÃ£o JoÃ£o Del Rei, Brasil (BR_3857)","SÃ£o LourenÃ§o, Brasil (BR_3892)","Sete Lagoas, Brasil (BR_3986)","TeÃ³filo Otoni, Brasil (BR_4017)","TimÃ³teo, Brasil (BR_4019)","Tiradentes, Brasil (BR_4020)","Toledo, Brasil (BR_4026)","TrÃªs Pontas, Brasil (BR_4035)","UbÃ¡, Brasil (BR_4043)","Barro Preto, Brasil (BR_405)","ViÃ§osa, Brasil (BR_4082)","VirgÃ­nia, Brasil (BR_4091)","Bonito, Brasil (BYO)","Dourados, Brasil (BR_4163)","Maracaju, Brasil (BR_4197)","BelÃ©m, Brasil (BR_42)","TrÃªs Lagoas, Brasil (BR_4263)","Barra do GarÃ§as, Brasil (BR_4300)","Bonito, Brasil (BR_432)","Chapada dos GuimarÃ£es, Brasil (BR_4331)","Lucas do Rio Verde, Brasil (BR_4389)","Primavera do Leste, Brasil (BR_4451)","Rio Branco, Brasil (BR_4459)","Sinop, Brasil (BR_4493)","Sorriso, Brasil (BR_4495)","TangarÃ¡ da Serra, Brasil (BR_4498)","VÃ¡rzea Grande, Brasil (BR_4511)","Ananindeua, Brasil (BR_4539)","Bonito, Brasil (BR_4577)","Paragominas, Brasil (BR_4728)","CamaÃ§ari, Brasil (BR_473)","Parauapebas, Brasil (BR_4730)","Aparecida, Brasil (BR_4857)","Barra de SÃ£o Miguel, Brasil (BR_4874)","BelÃ©m, Brasil (BR_4877)","Boa Vista, Brasil (BR_4881)","Conde, Brasil (BR_4921)","Patos, Brasil (BR_5017)","Paulista, Brasil (BR_5018)","Santa InÃªs, Brasil (BR_5062)","Santa Luzia, Brasil (BR_5063)","Santo AndrÃ©, Brasil (BR_5073)","SertÃ£ozinho, Brasil (BR_5109)","Bonito, Brasil (BR_5182)","Cabo de Santo Agostinho, Brasil (BR_5189)","Garanhuns, Brasil (BR_5259)","GravatÃ¡, Brasil (BR_5264)","Ipojuca, Brasil (BR_5285)","JaboatÃ£o dos Guararapes, Brasil (BR_5302)","Conde, Brasil (BR_537)","Paulista, Brasil (BR_5384)","VÃ¡rzea Grande, Brasil (BR_5726)","Apucarana, Brasil (BR_5780)","Arapongas, Brasil (BR_5783)","EunÃ¡polis, Brasil (BR_579)","Ariranha do IvaÃ­, Brasil (BR_5793)","Campo Grande, Brasil (BR_58)","Feira de Santana, Brasil (BR_582)","Campina Grande do Sul, Brasil (BR_5906)","Campo Largo, Brasil (BR_5911)","Cianorte, Brasil (BR_5959)","Francisco BeltrÃ£o, Brasil (BR_6094)","GuaÃ­ra, Brasil (BR_6115)","Guarapuava, Brasil (BR_6131)","Guaratuba, Brasil (BR_6135)","JaguariaÃ­va, Brasil (BR_6212)","Palmas, Brasil (BR_6401)","ParanavaÃ­, Brasil (BR_6420)","Pinhais, Brasil (BR_6445)","Ponta Grossa, Brasil (BR_6471)","Quatro Barras, Brasil (BR_6501)","Santa InÃªs, Brasil (BR_6578)","SÃ£o JosÃ© dos Pinhais, Brasil (BR_6656)","Itabuna, Brasil (BR_683)","ItacarÃ©, Brasil (BR_684)","Barra do PiraÃ­, Brasil (BR_6862)","Cachoeiras de Macacu, Brasil (BR_6875)","Campos dos Goytacazes, Brasil (BR_6881)","Duque de Caxias, Brasil (BR_6912)","ItaboraÃ­, Brasil (BR_6941)","ItaguaÃ­, Brasil (BR_6943)","Itatiaia, Brasil (BR_6951)","Nova Friburgo, Brasil (BR_6993)","Nova IguaÃ§u, Brasil (BR_6994)","Porto Real, Brasil (BR_7023)","Resende, Brasil (BR_7035)","Rio Claro, Brasil (BR_7040)","Rio das Ostras, Brasil (BR_7042)","SÃ£o JoÃ£o de Meriti, Brasil (BR_7069)","TeresÃ³polis, Brasil (BR_7102)","TrÃªs Rios, Brasil (BR_7107)","Itaparica, Brasil (BR_711)","Volta Redonda, Brasil (BR_7121)","Campo Grande, Brasil (BR_7149)","Galinhos, Brasil (BR_7174)","NÃ­sia Floresta, Brasil (BR_7222)","Santa Maria, Brasil (BR_7256)","SÃ£o Miguel de Touros, Brasil (BR_7275)","SÃ£o Vicente, Brasil (BR_7280)","Tibau do Sul, Brasil (BR_7298)","Touros, Brasil (BR_7300)","ViÃ§osa, Brasil (BR_7308)","ItuberÃ¡, Brasil (BR_731)","Rolim de Moura, Brasil (BR_7358)","Alegrete, Brasil (BR_7396)","JequiÃ©, Brasil (BR_749)","Campo Bom, Brasil (BR_7517)","Canela, Brasil (BR_7530)","Canoas, Brasil (BR_7533)","Dois IrmÃ£os, Brasil (BR_7650)","Erechim, Brasil (BR_7675)","Farroupilha, Brasil (BR_7710)","Flores da Cunha, Brasil (BR_7719)","GuaÃ­ba, Brasil (BR_7750)","Igrejinha, Brasil (BR_7768)","IjuÃ­, Brasil (BR_7771)","Lajeado, Brasil (BR_7823)","Marau, Brasil (BR_7850)","Lauro de Freitas, Brasil (BR_789)","Nova PetrÃ³polis, Brasil (BR_7910)","Santa Cruz do Sul, Brasil (BR_8079)","Santa Rosa, Brasil (BR_8090)","Santana do Livramento, Brasil (BR_8101)","SÃ£o Leopoldo, Brasil (BR_8146)","Torres, Brasil (BR_8242)","TramandaÃ­, Brasil (BR_8245)","Mata de SÃ£o JoÃ£o, Brasil (BR_827)","VenÃ¢ncio Aires, Brasil (BR_8288)","BalneÃ¡rio CamboriÃº, Brasil (BR_8357)","Brusque, Brasil (BR_8390)","CaÃ§ador, Brasil (BR_8391)","Fraiburgo, Brasil (BR_8456)","Garopaba, Brasil (BR_8461)","Gaspar, Brasil (BR_8463)","Gravatal, Brasil (BR_8468)","Imbituba, Brasil (BR_8486)","Indaial, Brasil (BR_8488)","ItajaÃ­, Brasil (BR_8507)","JoaÃ§aba, Brasil (BR_8520)","Lages, Brasil (BR_8525)","Laguna, Brasil (BR_8528)","Mafra, Brasil (BR_8543)","PalhoÃ§a, Brasil (BR_8589)","Penha, Brasil (BR_8602)","Praia Grande, Brasil (BR_8625)","Rio do Sul, Brasil (BR_8650)","SÃ£o Bento do Sul, Brasil (BR_8684)","SÃ£o Carlos, Brasil (BR_8687)","SÃ£o Francisco do Sul, Brasil (BR_8692)","SÃ£o JosÃ©, Brasil (BR_8700)","TimbÃ³, Brasil (BR_8735)","TubarÃ£o, Brasil (BR_8742)","Barra dos Coqueiros, Brasil (BR_8774)","CanindÃ© de SÃ£o Francisco, Brasil (BR_8780)","Ãguas de LindÃ³ia, Brasil (BR_8859)","Americana, Brasil (BR_8883)","Aparecida, Brasil (BR_8896)","Araraquara, Brasil (BR_8912)","Assis, Brasil (BR_8926)","AvarÃ©, Brasil (BR_8933)","Barretos, Brasil (BR_8954)","Barueri, Brasil (BR_8956)","Bebedouro, Brasil (BR_8962)","Bertioga, Brasil (BR_8967)","Botucatu, Brasil (BR_8989)","BraganÃ§a Paulista, Brasil (BR_8992)","CaÃ§apava, Brasil (BR_9005)","Cachoeira Paulista, Brasil (BR_9007)","Caraguatatuba, Brasil (BR_9043)","Cruzeiro, Brasil (BR_9087)","Paulo Afonso, Brasil (BR_909)","Franca, Brasil (BR_9144)","GuaÃ­ra, Brasil (BR_9162)","GuaratinguetÃ¡, Brasil (BR_9177)","Guarulhos, Brasil (BR_9183)","HortolÃ¢ndia, Brasil (BR_9189)","Ilhabela, Brasil (BR_9214)","Indaiatuba, Brasil (BR_9216)","ItanhaÃ©m, Brasil (BR_9237)","Itapira, Brasil (BR_9244)","Itatiba, Brasil (BR_9254)","Itu, Brasil (BR_9260)","Itupeva, Brasil (BR_9261)","Jaboticabal, Brasil (BR_9265)","JacareÃ­, Brasil (BR_9267)","JaguariÃºna, Brasil (BR_9273)","Jarinu, Brasil (BR_9283)","JaÃº, Brasil (BR_9285)","JundiaÃ­, Brasil (BR_9295)","Limeira, Brasil (BR_9317)","Lins, Brasil (BR_9319)","Lorena, Brasil (BR_9321)","MatÃ£o, Brasil (BR_9354)","MauÃ¡, Brasil (BR_9355)","Mogi das Cruzes, Brasil (BR_9369)","Mogi GuaÃ§u, Brasil (BR_9370)","Mogi Mirim, Brasil (BR_9371)","OlÃ­mpia, Brasil (BR_9425)","Osasco, Brasil (BR_9432)","Osvaldo Cruz, Brasil (BR_9434)","Ourinhos, Brasil (BR_9435)","PaulÃ­nia, Brasil (BR_9464)","PenÃ¡polis, Brasil (BR_9477)","Prado, Brasil (BR_948)","Piedade, Brasil (BR_9483)","Pindamonhangaba, Brasil (BR_9485)","Piracicaba, Brasil (BR_9493)","Pirassununga, Brasil (BR_9501)","Porto Feliz, Brasil (BR_9518)","Praia Grande, Brasil (BR_9527)","Presidente EpitÃ¡cio, Brasil (BR_9531)","Registro, Brasil (BR_9549)","Rio Claro, Brasil (BR_9566)","Salto, Brasil (BR_9587)","Santo AntÃ´nio do Pinhal, Brasil (BR_9632)","Santos, Brasil (BR_9636)","SÃ£o Bernardo do Campo, Brasil (BR_9640)","SÃ£o Caetano do Sul, Brasil (BR_9642)","SÃ£o Carlos, Brasil (BR_9643)","SÃ£o Roque, Brasil (BR_9671)","SÃ£o SebastiÃ£o, Brasil (BR_9673)","SÃ£o Vicente, Brasil (BR_9678)","Serra Negra, Brasil (BR_9684)","SertÃ£ozinho, Brasil (BR_9686)","Socorro, Brasil (BR_9693)","TaboÃ£o da Serra, Brasil (BR_9707)","TaubatÃ©, Brasil (BR_9726)","Ubatuba, Brasil (BR_9756)","Valinhos, Brasil (BR_9768)","Vinhedo, Brasil (BR_9783)","Votuporanga, Brasil (BR_9789)","Lajeado, Brasil (BR_9874)","Santa Cruz CabrÃ¡lia, Brasil (BR_992)","Santa InÃªs, Brasil (BR_994)","Taguatinga, Brasil (BR_9948)","Santa Luzia, Brasil (BR_995)","Porto de Galinhas, Brasil (PG8)","Tabatinga, Brasil (TBT)","Telemaco Borba, Brasil (TEC)","Tefe, Brasil (TFF)","Teresina, Brasil (THE)","Trombetas, Brasil (TMT)","Toledo, Brasil (TOW)","Varginha, Brasil (VAG)","Vitoria Da Conquista, Brasil (VDC)","Uruguaina, Brasil (URG)","Umuarama, Brasil (UMU)","Una BR, Brasil (UNA)","Uberaba, Brasil (UBA)","Uberlandia, Brasil (UDI)","Tucurui, Brasil (TUR)","Salvador, Brasil (SSA)","Sorocaba, Brasil (SOD)","Santarem, Brasil (STM)","San Luis, Brasil (SLZ)","Sao Jose Dos Campos, Brasil (SJK)","Sao Jose Do Rio Preto, Brasil (SJP)","Cabo de Santo Agostino, Brasil (SA1)","Rondonopolis, Brasil (ROO)","Rio De Janeiro, Brasil (RIO)","Sumare, Brasil (RWS)","San Pablo, Brasil (SAO)","Atibaia, Brasil (ZBW)","Chapeco, Brasil (XAP)","Vitoria, Brasil (VIX)","Camboriu, Brasil (CB1)","Caucedo, Brasil (CAU)","Campos, Brasil (CAW)","Cachoeiro De Itapemirim, Brasil (CDI)","Criciuma, Brasil (CCM)","Campo Grande, Brasil (CGR)","Cuiaba, Brasil (CGB)","Cabo Frio, Brasil (CFB)","Corumba, Brasil (CMG)","Carajas, Brasil (CKS)","Cascavel, Brasil (CAC)","Buzios, Brasil (BZC)","Boa Vista, Brasil (BVB)","Vilhena, Brasil (BVH)","Brasilia, Brasil (BSB)","Blumenau, Brasil (BNU)","Porto Seguro, Brasil (BPS)","Barreiras, Brasil (BRA)","Belem, Brasil (BEL)","Bento Goncalves, Brasil (BGV)","Belo Horizonte, Brasil (BHZ)","Bauru, Brasil (JTC)","Bombinhas, Brasil (BB1)","Araguaina, Brasil (AUX)","Aracatuba, Brasil (ARU)","Altamira, Brasil (ATM)","Anapolis, Brasil (APS)","Angra dos Reis, Brasil (AR1)","Aracaju, Brasil (AJU)","Alta Floresta, Brasil (AFL)","Santo Amaro da Imperatriz, Brasil (AI1)","Arraial Do Cabo, Brasil (AC1)","Arapoti, Brasil (AAG)","Cruzeiro Do Sul, Brasil (CZS)","Caxias Do Sul, Brasil (CXJ)","Curitiba, Brasil (CWB)","Costa Do Sauipe, Brasil (CS1)","Campinas, Brasil (CPQ)","Campina Grande, Brasil (CPV)","Fernando De Noronha, Brasil (FEN)","Floriano, Brasil (FLB)","Florianopolis, Brasil (FLN)","Horizontina, Brasil (HRZ)","Goiania, Brasil (GYN)","Governador Valadares, Brasil (GVR)","Gravatai, Brasil (GCV)","Fortaleza, Brasil (FOR)","Guarajuba, Brasil (GJ1)","Parati, Brasil (PT1)","Pato Branco, Brasil (PTO)","Praia do Santinho, Brasil (PS1)","Porto Velho, Brasil (PVH)","Novo Hamburgo, Brasil (QHV)","Iguatu, Brasil (QIG)","Niteroi, Brasil (QNT)","Ribeirao Preto, Brasil (RAO)","Rio Branco, Brasil (RBR)","Recife, Brasil (REC)","Santa Maria, Brasil (RIA)","Rio Grande, Brasil (RIG)","Parnaiba, Brasil (PHB)","Passo Fundo, Brasil (PFB)","Pelotas, Brasil (PET)","Ponta Pora, Brasil (PMG)","Palmas, Brasil (PMW)","Paranagua, Brasil (PNG)","Presidente Prudente, Brasil (PPB)","Petrolina, Brasil (PNZ)","Porto Alegre, Brasil (POA)","Morro SÃ£o Paulo, Brasil (MO1)","Montes Claros, Brasil (MOC)","Natal, Brasil (NAT)","Mossoro, Brasil (MVF)","Ourilandia, Brasil (OIA)","Cacoal, Brasil (OAL)","Navegantes, Brasil (NVT)","Lencois, Brasil (LEC)","Londrina, Brasil (LDB)","Marilia, Brasil (MII)","Maringa, Brasil (MGF)","Maragogi, Brasil (MG1)","Monte Dourado, Brasil (MEU)","Maceio, Brasil (MCZ)","Macae, Brasil (MEA)","Macapa, Brasil (MCP)","Praia de Muro Alto, Brasil (MA1)","Maraba, Brasil (MAB)","Manaos, Brasil (MAO)","Lajes, Brasil (LAJ)","Juazeiro Do Norte, Brasil (JDO)","Joinville, Brasil (JOI)","Joao Pessoa, Brasil (JPA)","Ji Parana, Brasil (JPR)","Jericoacoara, Brasil (JC1)","Juiz De Fora, Brasil (JDF)","Itapema, Brasil (IT1)","Itaperuna, Brasil (ITP)","Ilha Grande, Brasil (IG1)","Foz Do IguaÃ§u, Brasil (IGU)","Imperatriz, Brasil (IMP)","Ilheus, Brasil (IOS)","Ipatinga, Brasil (IPN)","Marsh Harbour, Bahamas (MHH)","Nassau, Bahamas (NAS)","Governor S Harbour, Bahamas (GHB)","George Town, Bahamas (GGT)","Freeport, Bahamas (FPO)","Harbour Island, Bahamas (HBI)","North Eleuthera, Bahamas (ELH)","Arthurs Town, Bahamas (ATC)","Andros Town, Bahamas (ASD)","Bimini, Bahamas (BIM)","San Salvador, Bahamas (ZSA)","Rock Sound, Bahamas (RSD)","South Andros, Bahamas (TZN)","Treasure Cay, Bahamas (TCB)","Kasane, Botswana (BBK)","Francistown, Botswana (FRW)","Gaborone, Botswana (GBE)","Maun, Botswana (MUB)","Selebi Phikwe, Botswana (PKW)","Mogilev, Bielorusia (MVQ)","Minsk, Bielorusia (MSQ)","Gomel, Bielorusia (GME)","Grodna, Bielorusia (GNA)","Brest, Bielorusia (BQT)","Vitebsk, Bielorusia (VTB)","San Pedro, Belize (SPR)","Belice, Belize (BZE)","Placencia, Belize (PLJ)","Pikwitonei, Canada (PIW)","Bella Coola, Canada (QBC)","Lake Louise, Canada (LL1)","Ilford, Canada (ILF)","Creston, Canada (CFQ)","St Pierre, Canada (FSP)","Duncan, Canada (DUQ)","La Sarre, Canada (SSQ)","Capreol, Canada (XAW)","Campbellton, Canada (XAZ)","Brockville, Canada (XBR)","Killineq, Canada (XBW)","Chambord, Canada (XCI)","Chatham, Canada (XCM)","Cobourg, Canada (XGJ)","Coteau, Canada (XGK)","Gananoque, Canada (XGW)","Grimsby, Canada (XGY)","Georgetown, Canada (XHM)","Chemainus, Canada (XHS)","Chandler, Canada (XDL)","Drummondville, Canada (XDM)","Grande Riviere, Canada (XDO)","Hervey, Canada (XDU)","Lac Edouard, Canada (XEE)","Ladysmith, Canada (XEH)","Langford, Canada (XEJ)","Melville, Canada (XEK)","New Carlisle, Canada (XEL)","New Richmond, Canada (XEM)","Stratford, Canada (XFD)","Parent, Canada (XFE)","Perce, Canada (XFG)","Port Daniel, Canada (XFI)","Senneterre, Canada (XFK)","Shawinigan, Canada (XFL)","Shawnigan, Canada (XFM)","Taschereau, Canada (XFO)","Weymont, Canada (XFQ)","Alexandria, Canada (XFS)","Brantford, Canada (XFV)","Guelph, Canada (XIA)","Ingersoll, Canada (XIB)","Maxville, Canada (XID)","Napanee, Canada (XIF)","Prescott, Canada (XII)","Saint Hyacinthe, Canada (XIM)","St Marys, Canada (XIO)","Woodstock, Canada (XIP)","Sackville, Canada (XKV)","Matapedia, Canada (XLP)","Niagara Falls, Canada (XLV)","Aldershot, Canada (XLY)","Truro, Canada (XLZ)","Joliette, Canada (XJL)","Jonquiere, Canada (XJQ)","Oakville, Canada (XOK)","Carleton, Canada (XON)","Parksville, Canada (XPB)","Port Hope, Canada (XPH)","Pukatawagan, Canada (XPK)","Brampton, Canada (XPN)","Pointe Aux Trembles, Canada (XPX)","Qualicum, Canada (XQU)","Riviere A Pierre, Canada (XRP)","St Anthony, Canada (YAY)","Tofino, Canada (YAZ)","Banff, Canada (YBA)","Pelly Bay, Canada (YBB)","Baie Comeau, Canada (YBC)","New Westminster, Canada (YBD)","Bagotville, Canada (YBG)","Campbell River, Canada (YBL)","Brandon, Canada (YBR)","Blanc Sablon, Canada (YBX)","Courtenay, Canada (YCA)","Cambridge Bay, Canada (YCB)","Cornwall, Canada (YCC)","Nanaimo, Canada (YCD)","Castlegar, Canada (YCG)","Miramichi, Canada (YCH)","Colville, Canada (YCK)","Charlo, Canada (YCL)","St Catherines, Canada (YCM)","Cochrane, Canada (YCN)","Chilliwack, Canada (YCW)","Clyde River, Canada (YCY)","Fairmont Springs, Canada (YCZ)","Dawson City, Canada (YDA)","Deer Lake, Canada (YDF)","Dease Lake, Canada (YDL)","Dauphin, Canada (YDN)","Dawson Creek, Canada (YDQ)","Edmonton, Canada (YEA)","Arviat, Canada (YEK)","Elliot Lake, Canada (YEL)","Estevan, Canada (YEN)","Edson, Canada (YET)","Inuvik, Canada (YEV)","Amos, Canada (YEY)","Iqaluit, Canada (YFB)","Fredericton, Canada (YFC)","Flin Flon, Canada (YFO)","Strathroy, Canada (XTY)","Belleville, Canada (XVV)","Watford, Canada (XWA)","Casselman, Canada (XZB)","Glencoe, Canada (XZC)","Amherst, Canada (XZK)","Fort Frances, Canada (YAG)","Wyoming, Canada (XWY)","Sault Ste Marie, Canada (YAM)","Kingston, Canada (YGK)","La Grande, Canada (YGL)","Gaspe, Canada (YGP)","Iles De Madeleine, Canada (YGR)","Havre St Pierre, Canada (YGV)","Gillam, Canada (YGX)","Hudson Bay, Canada (YHB)","Dryden, Canada (YHD)","Hope, Canada (YHE)","Hearst, Canada (YHF)","Charlottetown, Canada (YHG)","Hamilton, Canada (YHM)","Hornepayne, Canada (YHN)","Chevery, Canada (YHR)","Sechelt, Canada (YHS)","Hay River, Canada (YHY)","Halifax, Canada (YHZ)","Atikokan, Canada (YIB)","Pakuashipi, Canada (YIF)","Jasper, Canada (YJA)","Stephenville, Canada (YJT)","Kamloops, Canada (YKA)","Kitchener, Canada (YKF)","Schefferville, Canada (YKL)","Ottawa, Canada (YOW)","Prince Albert, Canada (YPA)","Port Alberni, Canada (YPB)","Parry Sound, Canada (YPD)","Peace River, Canada (YPE)","Esquimalt, Canada (YPF)","Portage La Prairie, Canada (YPG)","Pickle Lake, Canada (YPL)","St Pierre, Canada (YPM)","Port Menier, Canada (YPN)","Peterborough, Canada (YPQ)","Prince Rupert, Canada (YPR)","Port Hawkesbury, Canada (YPS)","Powell River, Canada (YPW)","Burns Lake, Canada (YPZ)","Muskoka, Canada (YQA)","Quebec, Canada (YQB)","The Pas, Canada (YQD)","Windsor, Canada (YQG)","Watson Lake, Canada (YQH)","Yarmouth, Canada (YQI)","Kenora, Canada (YQK)","Lethbridge, Canada (YQL)","Moncton, Canada (YQM)","Nakina, Canada (YQN)","Comox, Canada (YQQ)","Regina, Canada (YQR)","St Thomas, Canada (YQS)","Thunder Bay, Canada (YQT)","Grande Prairie, Canada (YQU)","Yorkton, Canada (YQV)","North Battleford, Canada (YQW)","Gander, Canada (YQX)","Sydney, Canada (YQY)","Quesnel, Canada (YQZ)","Riviere Du Loup, Canada (YRI)","Roberval, Canada (YRJ)","Red Lake, Canada (YRL)","Trois Rivieres, Canada (YRQ)","Rankin Inlet, Canada (YRT)","Sudbury, Canada (YSB)","Sherbrooke, Canada (YSC)","Smith Falls, Canada (YSH)","St John, Canada (YSJ)","St Leonard, Canada (YSL)","Ft Smith, Canada (YSM)","Salmon Arm, Canada (YSN)","Marathon, Canada (YSP)","St Theris Point, Canada (YST)","Pembroke, Canada (YTA)","Thicket Portage, Canada (YTD)","Cape Dorset, Canada (YTE)","Alma, Canada (YTF)","Thompson, Canada (YTH)","Terrace Bay, Canada (YTJ)","Toronto, Canada (YTO)","Trenton, Canada (YTR)","Timmins, Canada (YTS)","Queen Charlotte Island, Canada (ZQS)","Kegaska, Canada (ZKG)","La Tabatiere, Canada (ZLT)","Bathurst, Canada (ZBF)","Gethsemanie, Canada (ZGS)","Houston, Canada (ZHO)","Swan River, Canada (ZJN)","Sandy Lake, Canada (ZSJ)","Tete A La Baleine, Canada (ZTB)","Bromont, Canada (ZBM)","Oshawa, Canada (YOO)","Rainbow Lake, Canada (YOP)","Owen Sound, Canada (YOS)","Cold Lake, Canada (YOD)","High Level, Canada (YOJ)","Rouyn Noranda, Canada (YUY)","Gatineau Hull, Canada (YND)","Waskaganish, Canada (YKQ)","Chisasibi, Canada (YKU)","Kirkland, Canada (YKX)","Chapleau, Canada (YLD)","Meadow Lake, Canada (YLJ)","Lloydminster, Canada (YLL)","La Tuque, Canada (YLQ)","Kelowna, Canada (YLW)","Merritt, Canada (YMB)","Matane, Canada (YME)","Manitouwadge, Canada (YMG)","Minaki, Canada (YMI)","Ft McMurray, Canada (YMM)","Moosonee, Canada (YMO)","Montreal, Canada (YMQ)","Chibougamau, Canada (YMT)","Natashquan, Canada (YNA)","Bonaventure, Canada (YVB)","Vernon, Canada (YVE)","Vermilion, Canada (YVG)","Val D Or, Canada (YVO)","Kuujjuaq, Canada (YVP)","Norman Wells, Canada (YVQ)","Vancouver, Canada (YVR)","Deer Lake, Canada (YVZ)","Winnipeg, Canada (YWG)","Wabush, Canada (YWK)","Williams Lake, Canada (YWL)","White River, Canada (YWR)","Whistler, Canada (YWS)","Cranbrook, Canada (YXC)","Saskatoon, Canada (YXE)","Medicine Hat, Canada (YXH)","Ft St John, Canada (YXJ)","Rimouski, Canada (YXK)","Sioux Lookout, Canada (YXL)","Pangnirtung, Canada (YXP)","Prince George, Canada (YXS)","Terrace, Canada (YXT)","London, Canada (YXU)","Abbotsford, Canada (YXX)","Whitehorse, Canada (YXY)","Wawa, Canada (YXZ)","North Bay, Canada (YYB)","Calgary, Canada (YYC)","Smithers, Canada (YYD)","Fort Nelson, Canada (YYE)","Penticton, Canada (YYF)","Charlottetown, Canada (YYG)","Rivers, Canada (YYI)","Victoria, Canada (YYJ)","Lynn Lake, Canada (YYL)","Swift Current, Canada (YYN)","Churchill, Canada (YYQ)","Goose Bay, Canada (YYR)","St Johns, Canada (YYT)","Kapuskasing, Canada (YYU)","Armstromg, Canada (YYW)","Mont Joli, Canada (YYY)","Ashcroft, Canada (YZA)","Yellowknife, Canada (YZF)","Sandspit, Canada (YZP)","Sarnia, Canada (YZR)","Port Hardy, Canada (YZT)","Sept Iles, Canada (YZV)","Kinshasa, Congo RD (FIH)","Lubumbashi, Congo RD (FBM)","Bangui, Rep. Centroafricana (BGF)","Brazzaville, Congo Rep (BZV)","Pointe Noire, Congo Rep (PNR)","Lucerne, Suiza (QLJ)","Lausanne, Suiza (QLS)","Lugano, Suiza (LUG)","Basilea, Suiza (BSL)","Berna, Suiza (BRN)","Altenrhein, Suiza (ACH)","Ginebra, Suiza (GVA)","Zurich, Suiza (ZRH)","Montreux, Suiza (ZJP)","Engelberg, Suiza (ZHB)","Interlaken, Suiza (ZIN)","Davos, Suiza (ZDV)","Sion, Suiza (SIR)","St Moritz, Suiza (SMV)","Andermatt, Suiza (S01)","Gstaad, Suiza (ZHK)","M Bahiakro, Costa de Marfil (XMB)","Abidjan, Costa de Marfil (ABJ)","Yamoussouro, Costa de Marfil (ASK)","Aitutaki, Cook Islands (AIT)","Rarotonga, Cook Islands (RAR)","Punta Arenas, Chile (PUQ)","Puerto Varas, Chile (PTV)","Puerto Natales, Chile (PNT)","Puerto Montt, Chile (PMC)","Los Angeles, Chile (LSQ)","La Serena, Chile (LSC)","Iquique, Chile (IQQ)","Isla De Pascua, Chile (IPC)","Vina Del Mar, Chile (KNA)","Arica, Chile (ARI)","Antofagasta, Chile (ANF)","Balmaceda, Chile (BBA)","Concepcion, Chile (CCP)","Calama, Chile (CJC)","Coyhaique, Chile (GXQ)","El Salvador, Chile (ESR)","Copiapo, Chile (CPO)","Temuco, Chile (ZCO)","Osorno, Chile (ZOS)","Pucon, Chile (ZPC)","Valdivia, Chile (ZAL)","Puerto Aisen, Chile (WPA)","Valparaiso, Chile (VAP)","San Pedro de Atacama, Chile (SP1)","Santiago De Chile, Chile (SCL)","Yaounde, CamerÃºn (YAO)","Douala, CamerÃºn (DLA)","Koutaba, CamerÃºn (KOB)","Maroua, CamerÃºn (MVR)","N Gaoundere, CamerÃºn (NGE)","Ningbo, China (NGB)","Nanning, China (NNG)","Nanjing, China (NKG)","Meixian, China (MXZ)","Qiqihar, China (NDG)","Guilin, China (KWL)","Guiyang, China (KWE)","Kunming, China (KMG)","Nanchang, China (KHN)","Kashgar, China (KHG)","Huangyan, China (HYN)","Jinzhou, China (JNZ)","Jiujiang, China (JIU)","Jinghong, China (JHG)","Ling Ling, China (LLF)","Lianping, China (LIA)","Lijiang City, China (LJG)","Mian Yang, China (MIG)","Liuzhou, China (LZH)","Luoyang, China (LYA)","Mudanjiang, China (MDG)","Dalian, China (DLC)","Dongguan, China (DGM)","Diqing, China (DIG)","Chengdu, China (CTU)","Changsha, China (CSX)","Dayong, China (DYG)","Guang Yuan, China (GYS)","Haikou, China (HAK)","Hohhot, China (HET)","Huanghua, China (HHA)","Hefei, China (HFE)","Hangzhou, China (HGH)","Harbin, China (HRB)","Hengyang, China (HNY)","Fuzhou, China (FOC)","Chongqing, China (CKG)","Zhengzhou, China (CGO)","Changchun, China (CGQ)","Beijing, China (BJS)","Guangzhou, China (CAN)","Beihai, China (BHY)","Yangzhou, China (YTY)","Xining, China (XNN)","Xiamen, China (XMN)","Xian, China (XIY)","Wenzhou, China (WNZ)","Wuhan, China (WUH)","Weihai, China (WEH)","Yanji, China (YNJ)","Yantai, China (YNT)","Zhangjiang, China (ZHA)","Zhuhai, China (ZUH)","Shenyang, China (SHE)","Shanghai, China (SHA)","Shantou, China (SWA)","Qinhuangdao, China (SHP)","Xiguan, China (SIA)","Sanya, China (SYX)","Suzhou , China (SZV)","Shenzhen, China (SZX)","Shanzhou, China (SZO)","Qingdao, China (TAO)","Jinan, China (TNA)","Tianjin, China (TSN)","Urumqi, China (URC)","Qionghai, China (BAR)","Changzhou, China (CZX)","Pitalito, Colombia (PTX)","Palomino, Colombia (PAL)","Espinal, Colombia (EPN)","Santa Fe de Antioquia, Colombia (SAQ)","Tolu, Colombia (TLU)","CoveÃ±as, Colombia (CVE)","San Gil, Colombia (SGL)","La Macarena, Colombia (LMC)","Arauca, Colombia (AUC)","Sogamoso, Colombia (SOX)","Pamplona, Colombia (CO_9003)","Doradal, Colombia (CO_9004)","Mucura, Colombia (MCR)","Isla Palma, Colombia (A10)","Isla BarÃº, Colombia (IBU)","Gorgona, Colombia (GOG)","Cerrito, Colombia (CO29)","Bahia Malaga, Colombia (CB3)","Rio Claro, Antioquia, Colombia (C151)","Villa Vieja, Colombia (CO_1122)","Santa Elena, Colombia (SHQ)","Circasia, Colombia (CIR)","Finlandia, Colombia (FIN)","Desierto de la tatacoa, Colombia (DDT)","San Francisco, Colombia (C120)","San Luis, Colombia (C123)","San Pedro de los Milagros, Colombia (C124)","San Rafael, Colombia (C125)","San Vicente Ferrer, Colombia (C127)","Santa Barbara, Colombia (C128)","Santo Domingo, Colombia (C130)","Toledo, Colombia (C138)","Valdivia, Colombia (C141)","Valparaiso, Colombia (C142)","Venecia, Colombia (C144)","Zaragosa, Colombia (C150)","Tierra Bomba, Colombia (BBT)","Buritaca, Colombia (BMS)","Tobia, Colombia (CO3)","Isla Tintipan, Colombia (CO4)","Amalfi, Colombia (C44)","andes, Colombia (C48)","CASA DEL AGUA, Colombia (C64)","Ciudad Bolivar, Colombia (C70)","Concepcion, Colombia (C72)","Granada, Colombia (C89)","Aguachica, Colombia (HAY)","Zapatoca, Colombia (AZP)","Santa Veronica, Juan de acosta, Colombia (SJA)","Santa RosalÃ­a, Colombia (CO_912)","Granada, Colombia (CO_99)","Mompox, Colombia (CO5)","Abriaqui, Colombia (C41)","QuibdÃ³, Colombia (UIB)","Tunja, Colombia (GUS)","Tumaco, Colombia (TCO)","Santa Marta, Colombia (SMR)","Tame Arauca, Colombia (TME)","AcandÃ­, Colombia (ACD)","Araracuara, Colombia (ACR)","Caucasia, Colombia (CAQ)","Condoto, Colombia (COG)","Cumaribo, Colombia (PCE)","El Bagre, Colombia (EBG)","Girardot, Colombia (GIR)","Guapi, Colombia (GPI)","La Chorrera, Colombia (LCR)","Maicao, Colombia (MCJ)","MitÃº, Colombia (MVP)","NuquÃ­, Colombia (NQU)","OcaÃ±a, Colombia (OCV)","Puerto InÃ­rida, Colombia (PDA)","Puerto CarreÃ±o, Colombia (PCR)","Puerto LeguÃ­zamo, Colombia (LQM)","Remedios, Colombia (OTU)","San JosÃ© del Guaviare, Colombia (SJE)","San Vicente del CaguÃ¡n, Colombia (SVI)","Saravena, Colombia (RVE)","MontelÃ­bano, Colombia (MTB)","TuluÃ¡, Colombia (ULQ)","Santander de quilichao, Colombia (CTO)","Cartago, Colombia (CRC)","ChigorodÃ³, Colombia (IGO)","Cravo Norte, Colombia (RAV)","MaganguÃ©, Colombia (MGN)","Mariquita, Colombia (MQU)","NecoclÃ­, Colombia (NCI)","Puerto GaitÃ¡n, Colombia (PGT)","Puerto Rico, Colombia (PCC)","San Pedro de UrabÃ¡, Colombia (NPD)","Turbo, Colombia (TRB)","Solano, Colombia (TQS)","Abejorral, Colombia (C40)","Alejandria, Colombia (C42)","Amaga, Colombia (C43)","Angelopolis, Colombia (C45)","Angostura, Colombia (C46)","Anori, Colombia (C47)","Anza, Colombia (C49)","Arboletes, Colombia (CO_0012)","Argelia, Colombia (C50)","Barbosa, Colombia (C51)","Belmira, Colombia (C52)","Bello, Colombia (C53)","Betania, Colombia (C54)","Betulia, Colombia (C55)","BriceÃ±o, Colombia (C56)","Buritica, Colombia (C57)","Caceres, Colombia (C58)","Caicedo, Colombia (C59)","Caldas, Colombia (C60)","Campamento, Colombia (C61)","CaÃ±asgordas, Colombia (C62)","Caracoli, Colombia (C63)","Caramanta, Colombia (C65)","Carepa, Colombia (C66)","El Carmen De Viboral, Colombia (C67)","Carolina del Principe , Colombia (C68)","Cisneros, Colombia (C69)","Cocorna, Colombia (C71)","Concordia, Colombia (C73)","Copacabana, Colombia (C74)","Dabeiba, Colombia (C75)","Don Matias, Colombia (C76)","Ebejico, Colombia (C77)","Entrerrios, Colombia (C82)","Envigado, Colombia (C83)","Fredonia, Colombia (C84)","Frontino, Colombia (C85)","Giraldo, Colombia (C86)","Girardota, Colombia (C87)","Gomez Plata, Colombia (C88)","Guadalupe, Colombia (C90)","Guarne, Colombia (C91)","Guatape, Colombia (CO_0050)","Heliconia, Colombia (C92)","Hispania, Colombia (C93)","Itagui, Colombia (C94)","Ituango, Colombia (C95)","Jardin, Colombia (C78)","Jerico, Colombia (C96)","La Ceja, Colombia (C97)","La Estrella, Colombia (C98)","La Pintada, Colombia (C99)","La Union, Colombia (C100)","Liborina, Colombia (C101)","Maceo, Colombia (C102)","Marinilla, Colombia (C103)","Montebello, Colombia (C104)","Murindo, Colombia (C105)","Mutata, Colombia (C106)","NariÃ±o, Colombia (C107)","Nechi, Colombia (C108)","Olaya, Colombia (C109)","El PeÃ±ol, Colombia (C79)","Peque, Colombia (C110)","Pueblorrico, Colombia (C111)","Puerto Nare, Colombia (C112)","Puerto Triunfo, Colombia (C113)","El Retiro, Colombia (C80)","Rionegro, Colombia (C114)","Sabanalarga, Colombia (C115)","Sabaneta, Colombia (C116)","Salgar, Colombia (C117)","San Andres De Cuerquia, Colombia (C118)","San Carlos, Colombia (C119)","San Jeronimo, Colombia (C121)","San Jose De La MontaÃ±a, Colombia (C122)","San Roque, Colombia (C126)","Santa Rosa De Osos, Colombia (C129)","El Santuario, Colombia (C81)","Segovia, Colombia (C131)","Sonson, Colombia (C132)","Sopetran, Colombia (C133)","Tamesis, Colombia (C134)","Taraza, Colombia (C135)","Tarso, Colombia (C136)","Titiribi, Colombia (C137)","Uramita, Colombia (C139)","Vegachi, Colombia (C143)","Vigia Del Fuerte, Colombia (C145)","Yali, Colombia (C146)","Yarumal, Colombia (C147)","Yolombo, Colombia (C148)","Yondo, Colombia (C149)","Calamar, Colombia (CO_0136)","Villanueva, Colombia (CO_0169)","Chiquinquira, Colombia (CO_0185)","Duitama, Colombia (CO_0199)","Tota, Colombia (CO_0279)","Belen De Los Andaquies, Colombia (CO_0312)","El Doncello, Colombia (CO_0315)","El Paujil, Colombia (CO_0316)","La MontaÃ±ita, Colombia (CO_0317)","Solita, Colombia (CO_0322)","San Martin, Colombia (CO_0379)","San Antero, Colombia (CO_0401)","San Bernardo Del Viento, Colombia (CO_0402)","Anapoima, Colombia (CO_0408)","Choachi, Colombia (CO_0423)","Choconta, Colombia (CO_0424)","Fusagasuga, Colombia (CO_0436)","Guaduas, Colombia (CO_0443)","Guatavita, Colombia (CO_0446)","La Calera, Colombia (CO_0452)","La Mesa, Colombia (CO_0453)","Medina, Colombia (CO_0459)","Puerto Salgar, Colombia (CO_0471)","Apulo, Colombia (CO_0476)","Sopo, Colombia (CO_0488)","Suesca, Colombia (CO_0490)","Ubate, Colombia (CO_0505)","Villeta, Colombia (CO_0511)","Zipacon, Colombia (CO_0514)","Alto Baudo, Colombia (CO_0517)","Atrato, Colombia (CO_0518)","Bagado, Colombia (CO_0519)","Bajo Baudo, Colombia (CO_0520)","Bojaya, Colombia (CO_0521)","Certegui, Colombia (CO_0524)","Istmina, Colombia (CO_0528)","San Agustin, Colombia (CO_0567)","Acacias, Colombia (CO_0617)","Cabuyaro, Colombia (CO_0619)","Castilla La Nueva, Colombia (CO_0620)","Cubarral, Colombia (CO_0621)","Cumaral, Colombia (CO_0622)","El Calvario, Colombia (CO_0623)","El Castillo, Colombia (CO_0624)","Fuente De Oro, Colombia (CO_0625)","Guamal, Colombia (CO_0626)","Mapiripan, Colombia (CO_0627)","Mesetas, Colombia (CO_877)","Lejanias, Colombia (CO_0630)","Puerto Concordia, Colombia (CO_0631)","Puerto Lopez, Colombia (CO_0633)","Puerto Lleras, Colombia (CO_0634)","Restrepo, Colombia (CO_0636)","San Carlos De Guaroa, Colombia (CO_0637)","San Juan De Arama, Colombia (CO_0638)","San Juanito, Colombia (CO_0639)","Vistahermosa, Colombia (CO_0641)","Montenegro, Colombia (CO_0742)","Quimbaya, Colombia (CO_0744)","Salento, Colombia (CO_0745)","Santa Rosa De Cabal, Colombia (CO_0756)","Barichara, Colombia (CO_0762)","San Onofre, Colombia (CO_0853)","Flandes, Colombia (CO_0873)","Melgar, Colombia (CO_0881)","Aguazul, Colombia (CO_0942)","Chameza, Colombia (CO_0943)","La Salina, Colombia (CO_0945)","Mani, Colombia (CO_0946)","Recetor, Colombia (CO_0951)","Sacama, Colombia (CO_0953)","Tamara, Colombia (CO_0955)","Mocoa, Colombia (CO_0958)","Puerto NariÃ±o, Colombia (CO_0975)","Barranco Minas, Colombia (CO_0979)","Mapiripana, Colombia (CO_0980)","Calamar, Colombia (CO_0987)","El Retorno, Colombia (CO_0988)","Taraira, Colombia (CO_0993)","Apiay, Colombia (API)","Barranca de Upi, Colombia (BAC)","Candilejas, Colombia (CJD)","Caquetania, Colombia (CQT)","Caruru, Colombia (CUO)","El Recreo, Colombia (ELJ)","Paratebueno, Colombia (EUO)","Hato Corozal, Colombia (HTZ)","Lauribe, Colombia (LAT)","La Pedrera, Colombia (LPD)","La Primavera, Colombia (LPE)","Lorica, Colombia (LRI)","Nare, Colombia (NAR)","Nunchia, Colombia (NUH)","Orocue, Colombia (ORC)","Puerto Berrio, Colombia (PBE)","Pore, Colombia (PRE)","Paz de Ariporo, Colombia (PZA)","San Juan de Uraba, Colombia (SJR)","San Luis de Palenque, Colombia (SQE)","Tauramena, Colombia (TAU)","Trinidad, Colombia (TDA)","Uribe, Colombia (URI)","Urrao, Colombia (URR)","Villa GarzÃ³n, Colombia (VGZ)","Villa de Leyva, Colombia (VI1)","Valledupar, Colombia (VUP)","Villavicencio, Colombia (VVC)","Bucaramanga, Colombia (BGA)","Barranquilla, Colombia (BAQ)","Armenia, Colombia (AXM)","ApartadÃ³, Colombia (APO)","San AndrÃ©s Isla, Colombia (ADZ)","Bahia Solano, Colombia (BSC)","Buga, Colombia (CO_0904)","Buenaventura, Colombia (BUN)","Boyaca, Colombia (BYA)","BogotÃ¡, Colombia (BOG)|E","Cali, Colombia (CLO)","Capurgana, Colombia (CPB)","Florencia, Colombia (FLA)","Barrancabermeja, Colombia (EJA)","Yopal, Colombia (EYP)","Cartagena, Colombia (CTG)","CÃºcuta, Colombia (CUC)","Cabo de la Vela, Colombia (CO_9001)","MedellÃ­n, Colombia (MDE)|E","Leticia, Colombia (LET)","IbaguÃ©, Colombia (IBE)","Ipiales, Colombia (IPI)","Jurado, Colombia (JUO)","Manizales, Colombia (MZL)","MonterÃ­a, Colombia (MTR)","Neiva, Colombia (NVA)","PopayÃ¡n, Colombia (PPN)","Pereira, Colombia (PEI)","Paipa, Colombia (PAI)","Pasto, Colombia (PSO)","Puerto Asis, Colombia (PUU)","Isla Providencia , Colombia (PVA)","Riohacha, Colombia (RCH)","Puntarenas, Costa Rica (JAP)","Puerto Limon, Costa Rica (LIO)","Liberia, Costa Rica (LIR)","Guanacaste, Costa Rica (GTE)","Quepos, Costa Rica (XQP)","San Jose, Costa Rica (SJO)","Tambor, Costa Rica (TMU)","Tamarindo, Costa Rica (TNO)","Tortuquero, Costa Rica (TTQ)","Canton de Santa Cruz, Costa Rica (SC1)","Samara, Costa Rica (SM3)","Puerto JimÃ©nez, Costa Rica (PJM)","Playa Tambor, Costa Rica (A18)","Arenal, Costa Rica (A19)","Cayo Guillermo, Cuba (A08)","Cayo Santa Maria, Cuba (A09)","Cayo Las Brujas, Cuba (CU_001)","ViÃ±ales, Cuba (VIÃ‘)","Remedios, Cuba (REM)","Ensenachos, Cuba (ES1)","Club Amigo Carisol los Corales, Cuba (CL2)","Topes de Collantes, Cuba (CLS)","Soroa, Cuba (SOR)","Cayo Levisa, Cuba (PRU)","Cayo Saetia, Cuba (SAE)","Santa Lucia, Cuba (LUC)","Don Lino, Cuba (LIN)","Cobarrubias, Cuba (COB)","Brisas , Cuba (BSA)","Esmeralda, Cuba (EMR)","Pesquero , Cuba (PES)","Guardalavaca, Cuba (GLV)","Brisas Los Galeones, Cuba (BLG)","Brisa Sierra Mar, Cuba (BSM)","Costa Morena, Cuba (CMO)","Bucanero, Cuba (BUC)","Jibacoa, Cuba (JCO)","Playa del Este, Cuba (PYE)","Villa Panamericana, Cuba (VPN)","PenÃ­nsula de Zapata, Cuba (C03)","Cayo Ensenachos, Cuba (C08)","Pinar del RÃ­o, Cuba (C09)","San Antonio de los Banos, Cuba (C01)","Playa Santa Lucia, Cuba (C02)","Playa Blanca, Cuba (C06)","Santiago de Cuba, Cuba (C07)","Playa Pesquero, Cuba (C04)","Villa Clara, Cuba (C05)","Baracoa, Cuba (BCA)","Sancti Spiritus, Cuba (USS)","Trinidad, Cuba (TND)","Siguanea, Cuba (SZJ)","Santa Clara, Cuba (SNU)","Santiago, Cuba (SCU)","Matanzas, Cuba (VRO)","Varadero, Cuba (VRA)","Nueva Gerona, Cuba (GER)","Holguin, Cuba (HOG)","La Habana, Cuba (HAV)","Cayo Largo Del Sur, Cuba (CYO)","Camaguey, Cuba (CMW)","Cienfuegos, Cuba (CFG)","Cayo Coco, Cuba (CCC)","Ciego De Avila, Cuba (AVI)","Manzanillo, Cuba (MZO)","Maio, Cabo Verde (MMO)","Santo Antao, Cabo Verde (NTO)","Praia, Cabo Verde (RAI)","Boa Vista, Cabo Verde (BVC)","Brava, Cabo Verde (BVR)","Sao Vicente, Cabo Verde (VXE)","Sao Filipe, Cabo Verde (SFL)","Sao Nicolau, Cabo Verde (SNE)","Ilha Do Sal, Cabo Verde (SID)","Ercan, Chipre (ECN)","Paphos, Chipre (PFO)","Larnaca, Chipre (LCA)","Marianske Lazne, Republica Checa (MKA)","Karlovy Vary, Republica Checa (KLV)","Pardubice, Republica Checa (PED)","Praga, Republica Checa (PRG)","Ostrava, Republica Checa (OSR)","Brno, Republica Checa (BRQ)","Braunschweig, Alemania (BWE)","Bayreuth, Alemania (BYU)","Bremerhaven, Alemania (BRV)","Bremenhaven, Alemania (BRE)","Bonn, Alemania (BNJ)","Colonia/Bonn, Alemania (CGN)","Barth, Alemania (BBH)","Bitburg, Alemania (BBJ)","Berlin, Alemania (BER)","Bielefeld, Alemania (BFE)","Wangerooge, Alemania (AGE)","Aachen, Alemania (AAH)","Altenburg, Alemania (AOC)","Dusseldorf, Alemania (DUS)","Dortmund, Alemania (DTM)","Eisenach, Alemania (EIB)","Friedrichshafen, Alemania (FDH)","Flensburg, Alemania (FLF)","Karlsruhe Baden Baden, Alemania (FKB)","Essen, Alemania (ESS)","Erfurt, Alemania (ERF)","Dresde, Alemania (DRS)","Heringsdorf, Alemania (HDF)","Heide Buesum, Alemania (HEI)","Heidelberg, Alemania (HDB)","Helgoland, Alemania (HGL)","Hamburgo, Alemania (HAM)","Hanover, Alemania (HAJ)","Westerland, Alemania (GWT)","Gutersloh, Alemania (GUT)","Hof De, Alemania (HOQ)","Guettin, Alemania (GTI)","Muenster, Alemania (FMO)","Fritzlar, Alemania (FRZ)","Frankfurt, Alemania (FRA)","Paderborn, Alemania (PAD)","Nuremberg, Alemania (NUE)","Munich, Alemania (MUC)","Freiburg, Alemania (QFB)","Rothenburg, Alemania (QTK)","Wurzburg, Alemania (QWU)","Mainz, Alemania (QMZ)","Straubing, Alemania (RBM)","Everglades, Alemania (PEF)","Kiel, Alemania (KEL)","Kaiserslautern, Alemania (KLT)","Kassel, Alemania (KSF)","Mannheim Germany, Alemania (MHG)","Luebeck, Alemania (LBC)","Leipzig, Alemania (LEJ)","Lahr, Alemania (LHA)","Stuttgart, Alemania (STR)","Siegen, Alemania (SGE)","Saarbruecken, Alemania (SCN)","Ramstein, Alemania (RMS)","Rostock Laage, Alemania (RLG)","Wiesbaden, Alemania (UWE)","Wilhelmshaven, Alemania (WVN)","Weimar, Alemania (ZWM)","Schwerin, Alemania (ZSR)","Baden Baden, Alemania (ZCC)","Garmisch Prtnkrch, Alemania (ZEI)","Regensburg, Alemania (ZPM)","Trier, Alemania (ZQF)","Wolfsburg, Alemania (ZQU)","Magdeburg, Alemania (ZMG)","Gunzburg, Alemania (FMM)","Berchtesgaden, Alemania (ZCE)","Yibuti, Djibouti (JIB)","Karup, Dinamarca (KRP)","Odense, Dinamarca (ODE)","Faeroe Islands, Dinamarca (FAE)","Esbjerg, Dinamarca (EBJ)","Aalborg, Dinamarca (AAL)","Aarhus, Dinamarca (AAR)","Copenhague, Dinamarca (CPH)","Billund, Dinamarca (BLL)","Vejle, Dinamarca (VEJ)","Thisted, Dinamarca (TED)","Roskilde, Dinamarca (RKE)","Bornholm, Dinamarca (RNN)","Sonderborg, Dinamarca (SGD)","Skrydstrup, Dinamarca (SKS)","Santa Lucia, Dominica (SLU)","Canovan Island, Dominica (CIW)","Dominica, Dominica (DOM)","Montserrat, Dominica (MNI)","Puerto Plata, Republica Dominicana (POP)","Punta Cana, Republica Dominicana (PUJ)","La Romana, Republica Dominicana (LRM)","Barahona, Republica Dominicana (BRX)","Samana, Republica Dominicana (AZS)","Santiago, Republica Dominicana (STI)","Santo Domingo, Republica Dominicana (SDQ)","San JosÃ© del Lagos, Republica Dominicana (SJL)","RÃ­o Canimar, Republica Dominicana (RCA)","Villa El Abra / Jibacoa, Republica Dominicana (JBC)","Playa Esmeralda, Republica Dominicana (PE1)","Pinares de MayarÃ­, Republica Dominicana (PM1)","Cayo Santa Maria, Republica Dominicana (ST1)","Campismo Arco Iris, Republica Dominicana (AC2)","CaibariÃ©n, Republica Dominicana (CB2)","LAS TERRENAS, Republica Dominicana (LT1)","Cardenas, Republica Dominicana (CDN)","Campismo Rio La Mula, Republica Dominicana (RLM)","Campismo Caleton Blanco, Republica Dominicana (CCB)","Ancon, Republica Dominicana (ACN)","Aguas Claras, Republica Dominicana (AGC)","Wajay, Republica Dominicana (WJY)","Villa Maguana, Republica Dominicana (VMG)","Rio San Juan, Republica Dominicana (RSJ)","Uvero Alto , Republica Dominicana (UVA)","Moron, Republica Dominicana (MRN)","Matanzas, Republica Dominicana (MTZ)","Maria La Gorda, Republica Dominicana (MGO)","Marea del Portillo, Republica Dominicana (MPO)","La Moka, Republica Dominicana (MOK)","La Guabina, Republica Dominicana (GUN)","Las Yagrumas, Republica Dominicana (LYG)","GuamÃ¡, Republica Dominicana (GNM)","Gran Piedra, Republica Dominicana (GPD)","Elguea, Republica Dominicana (ELG)","El Salton, Republica Dominicana (SLT)","Playa Larga, Republica Dominicana (PLG)","Playa GirÃ³n, Republica Dominicana (PGI)","Villa TrÃ³pico, Republica Dominicana (VTR)","Villa Guajimico, Republica Dominicana (VGJ)","Juan Dolio, Republica Dominicana (A12)","Boca Chica, Republica Dominicana (AR6)","Bayahibe, Republica Dominicana (BYE)","Cabarete, Republica Dominicana (D03)","SosÃºa, Republica Dominicana (D02)","Playa Dorada, Republica Dominicana (D01)","Miches, Republica Dominicana (MC2)","Skikda, Algeria (SKI)","Annaba, Algeria (AAE)","Argel, Algeria (ALG)","Constantine, Algeria (CZL)","Ghardaia, Algeria (GHA)","Oran, Algeria (ORN)","Manta, Ecuador (MEC)","Galapagos Is, Ecuador (GPS)","Guayaquil, Ecuador (GYE)","Cuenca, Ecuador (CUE)","Esmeraldas, Ecuador (ESM)","Shell Mera, Ecuador (PTZ)","Alamor, Ecuador (AL1)","Alangasi, Ecuador (AL2)","Alausi, Ecuador (AL3)","Alluriquin, Ecuador (QUI)","Aloag, Ecuador (AL4)","AmaguaÃ±a, Ecuador (AGM)","Ambato, Ecuador (ATF)","Ambuqui, Ecuador (AM1)","Ancon, Ecuador (AN1)","Andrade, Ecuador (AD1)","Arajuno, Ecuador (AA1)","Archidona, Ecuador (AA2)","Arenillas, Ecuador (AA3)","Atacames, Ecuador (AT1)","Atahualpa, Ecuador (AT2)","Atuntaqui, Ecuador (AT3)","Azogues, Ecuador (ZOG)","Baba, Ecuador (BA1)","Babahoyo, Ecuador (BA2)","Baeza, Ecuador (BA3)","Bahia De Caraquez, Ecuador (BA4)","Balao, Ecuador (BA5)","Balsas, Ecuador (BA6)","Balzar, Ecuador (BA7)","BaÃ±os, Ecuador (BA8)","Biblian, Ecuador (BI1)","BolÃ­var, Ecuador (BO1)","BorbÃ³n, Ecuador (BO2)","Cajabamba, Ecuador (CA1)","Calceta, Ecuador (CA2)","Calderon, Ecuador (CA3)","Caluma, Ecuador (CA4)","CaÃ±ar, Ecuador (CA5)","Cariamanga, Ecuador (CA6)","Carlos Julio Arosemena Tola, Ecuador (CA7)","Cascales, Ecuador (CA8)","Catacocha, Ecuador (CA9)","Catamayo, Ecuador (C10)","Catarama, Ecuador (C11)","Cayambe, Ecuador (C12)","Celica, Ecuador (C13)","Cevallos, Ecuador (C14)","Chaguarpamba, Ecuador (C15)","Chambo, Ecuador (C16)","Chantillin, Ecuador (C17)","Chilla, Ecuador (C18)","Chillanes, Ecuador (C19)","Chinchipe, Ecuador (C20)","Chone, Ecuador (C21)","Chunchi, Ecuador (C22)","Colimes, Ecuador (C23)","Colta, Ecuador (C24)","Conocoto, Ecuador (C25)","Cotacachi, Ecuador (C26)","CumandÃ¡, Ecuador (C27)","Cumbaya, Ecuador (C28)","Cuyabeno, Ecuador (C29)","Daule, Ecuador (DA1)","Duran, Ecuador (DU1)","EcheandÃ­a, Ecuador (ECE)","El Carmen, Ecuador (C30)","El Chaco, Ecuador (C31)","El CorazÃ³n, Ecuador (C32)","El Empalme, Ecuador (EM2)","El Guabo, Ecuador (GU1)","El Quinche, Ecuador (GU2)","El Triunfo, Ecuador (ET1)","Eloy Alfaro, Ecuador (EA1)","Espindola, Ecuador (EA2)","Flavio Alfaro, Ecuador (FA1)","GirÃ³n, Ecuador (GI1)","Gonzalo Pizarro, Ecuador (GP1)","Gonzanama, Ecuador (GO1)","Guamote, Ecuador (GU3)","Guano, Ecuador (GU4)","Guaranda, Ecuador (GU5)","Guayllabamba, Ecuador (GU6)","Guaytacama, Ecuador (GU7)","Huaca, Ecuador (HU1)","Huaquillas, Ecuador (HU2)","Huigra, Ecuador (HU3)","Ibarra, Ecuador (IB1)","Isabela, Ecuador (IS1)","Izamba, Ecuador (IZ1)","Jama, Ecuador (JA1)","Jipijapa, Ecuador (JIP)","Julio, Ecuador (JU1)","Junin, Ecuador (JNI)","La AsunciÃ³n, Ecuador (LA1)","La Concordia, Ecuador (LC1)","La Independencia, Ecuador (LI1)","La Joya De Los Sachas, Ecuador (LJ1)","La Libertad, Ecuador (LJ2)","La Magdalena, Ecuador (LM1)","La Mana, Ecuador (LM2)","La Merced, Ecuador (LM3)","La Paz, Ecuador (LP1)","La Troncal, Ecuador (LP2)","La UniÃ³n, Ecuador (LU1)","Las Naves, Ecuador (LN1)","Lasso, Ecuador (LA2)","Latacunga, Ecuador (LTX)","Limon Indanza, Ecuador (LI2)","Limones, Ecuador (LI3)","Loja, Ecuador (LOH)","Loreto, Ecuador (LO1)","Luz De AmÃ©rica, Ecuador (LU2)","Macara, Ecuador (MRR)","Macas, Ecuador (XMS)","Machachi, Ecuador (MC1)","Machala, Ecuador (MCH)","Marcabeli, Ecuador (MA2)","Mera, Ecuador (ME1)","Milagro, Ecuador (MI1)","Mindo, Ecuador (MI2)","Mira, Ecuador (MI3)","Mocha, Ecuador (MO2)","Montalvo, Ecuador (MO3)","Montecristi, Ecuador (MO4)","Muisne, Ecuador (MU1)","Mulalo, Ecuador (MU2)","Nabon, Ecuador (NA1)","Nangaritza, Ecuador (NA2)","Naranjal, Ecuador (NA3)","Naranjito, Ecuador (NA4)","Nayon, Ecuador (NA5)","Nueva Loja, Ecuador (NU1)","Nuevo Rocafuerte, Ecuador (NU2)","Otavalo, Ecuador (OT1)","Pajan, Ecuador (PA2)","Palestina, Ecuador (PA3)","Pallatanga, Ecuador (PA4)","Palora, Ecuador (PA5)","Pasaje, Ecuador (PA6)","Pastocalle, Ecuador (PA7)","Patate, Ecuador (PA8)","Paute, Ecuador (PA9)","Pedernales, Ecuador (PDZ)","Pedro Carbo, Ecuador (PE3)","Penipe, Ecuador (PE4)","Pichincha, Ecuador (PI1)","Pifo, Ecuador (PI2)","Pillaro, Ecuador (PI3)","Pimampiro, Ecuador (PI4)","Pindal, Ecuador (PI5)","Pintag, Ecuador (PI6)","PiÃ±as, Ecuador (PI7)","Playas, Ecuador (PL1)","Poalo, Ecuador (PO2)","Pomasqui, Ecuador (PO3)","Portovelo, Ecuador (PO4)","Portoviejo, Ecuador (PVO)","Pucara, Ecuador (PU1)","Puebloviejo, Ecuador (PU2)","Puembo, Ecuador (PU3)","Puerto Ayora, Ecuador (PU4)","Puerto Baquerizo Moreno, Ecuador (PU5)","Puerto Bolivar, Ecuador (PU6)","Puerto Lopez, Ecuador (PU7)","Puerto Quito, Ecuador (PU8)","Pujili, Ecuador (PU9)","Pusuqui, Ecuador (P10)","Putumayo, Ecuador (PYO)","Puyo, Ecuador (P11)","Quero, Ecuador (QU1)","Quevedo, Ecuador (QU2)","Quilanga, Ecuador (QU3)","Quininde, Ecuador (QU4)","Quisapincha, Ecuador (QU5)","Riobamba, Ecuador (RI1)","Rioverde, Ecuador (RI2)","Rocafuerte, Ecuador (RO1)","Salcedo, Ecuador (SA3)","Samborondon, Ecuador (SA4)","Same Casablanca, Ecuador (SA5)","San Andres, Ecuador (SA6)","San Antonio De Ibarra, Ecuador (SA7)","San Antonio De Pichincha, Ecuador (SA8)","San Fernando, Ecuador (FDO)","San Gabriel, Ecuador (SA9)","San JosÃ© De Chimbo, Ecuador (S10)","San Juan, Ecuador (S11)","San Lorenzo, Ecuador (S12)","San Miguel, Ecuador (NMG)","San Miguel De Urcuqui, Ecuador (S13)","San Pablo, Ecuador (S14)","San Rafael, Ecuador (S15)","San Vicente, Ecuador (S16)","SangolquÃ­, Ecuador (S17)","Santa Ana, Ecuador (S18)","Santa Clara, Ecuador (S19)","Santa Cruz, Ecuador (S20)","Santa Elena, Ecuador (S21)","Santa Isabel, Ecuador (S22)","Santa Lucia, Ecuador (S23)","Santa Rosa, Ecuador (S24)","Santo Domingo, Ecuador (S26)","Santo Domingo de los Colorados, Ecuador (S27)","Saquisili, Ecuador (S28)","Saraguro, Ecuador (S29)","Shell, Ecuador (S30)","Shushufindi, Ecuador (S31)","Sigchos, Ecuador (S32)","Sigsig, Ecuador (S33)","Sozoranga, Ecuador (S34)","SuaTonsupa, Ecuador (S35)","Sucua, Ecuador (S36)","Sucumbios, Ecuador (S37)","Tababela, Ecuador (TA1)","Tabacundo, Ecuador (TA2)","Tambillo, Ecuador (TA3)","Tanicuchi, Ecuador (TA4)","Taura, Ecuador (TA5)","Tena, Ecuador (TE1)","Tisaleo, Ecuador (TI1)","Toacaso, Ecuador (TO1)","TonchigÃ¼Ã©, Ecuador (TO2)","Tosagua, Ecuador (TO3)","Tulcan, Ecuador (TUA)","Tumbabiro, Ecuador (TU1)","Tumbaco, Ecuador (TU2)","Urbina Jado, Ecuador (UR1)","Valencia, Ecuador (VA1)","Valle Hermoso, Ecuador (VA2)","Veinticuatro De Mayo, Ecuador (VE1)","Ventanas, Ecuador (VE2)","Yacuambi, Ecuador (YA1)","Yaguachi, Ecuador (YA2)","Yantzaza, Ecuador (YA3)","Yaruqui, Ecuador (YA4)","Zamora, Ecuador (ZMM)","Zapotillo, Ecuador (ZA1)","Zaruma, Ecuador (ZA2)","Pedro Vicente Maldonado, Ecuador (P18)","Vinces, Ecuador (VI2)","MontaÃ±ita, Ecuador (AR5)","Salinas, Ecuador (AR4)","Coca, Ecuador (OCC)","Lago Agrio, Ecuador (LGQ)","Punta Centinela, Ecuador (A03)","San Cristobal, Ecuador (SCY)","Quito, Ecuador (UIO)","Tartu, Estonia (TAY)","Tallinn, Estonia (TLL)","Taba, Egypt (TCP)","Kharga, Egypt (UVL)","Marsa Alam, Egypt (RMF)","Sharm El Sheik, Egypt (SSH)","Dakhla Oasis, Egypt (DAK)","Borg El Arab, Egypt (HBE)","Hurghada, Egypt (HRG)","Alejandria, Egypt (ALY)","Abu Simbel, Egypt (ABS)","Aswan, Egypt (ASW)","Cairo, Egypt (CAI)","Luxor, Egypt (LXR)","Asmara, Eritrea (ASM)","Barcelona, EspaÃ±a (BCN)","Bilbao, EspaÃ±a (BIO)","Lanzarote, Islas Canarias, EspaÃ±a (ACE)","Albacete, EspaÃ±a (ABC)","Malaga, EspaÃ±a (AGP)","Alicante, EspaÃ±a (ALC)","Cadiz, EspaÃ±a (CDZ)","Badajoz, EspaÃ±a (BJZ)","Gerona, EspaÃ±a (GRO)","Granada, EspaÃ±a (GRX)","San Sebastian De La Gomera, EspaÃ±a (GMZ)","Fuerte Ventura, Islas Canarias, EspaÃ±a (FUE)","San Sebastion, EspaÃ±a (EAS)","Menorca, EspaÃ±a (MAH)","Madrid, EspaÃ±a (MAD)","Murcia, EspaÃ±a (MJV)","Gran Canaria Islas Canarias, EspaÃ±a (LPA)","Almeira, EspaÃ±a (LEI)","Leon, EspaÃ±a (LEN)","La CoruÃ±a, EspaÃ±a (LCG)","Ibiza, EspaÃ±a (IBZ)","Oviedo, EspaÃ±a (OVD)","Cordoba, EspaÃ±a (ODB)","Melilla, EspaÃ±a (MLN)","Reus, EspaÃ±a (REU)","Marbella, EspaÃ±a (QRL)","Palma De Mallorca, EspaÃ±a (PMI)","Pamplona, EspaÃ±a (PNA)","Santa Cruz, EspaÃ±a (SPC)","Sevilla, EspaÃ±a (SVQ)","LogroÃ±o, EspaÃ±a (RJL)","Santiago De Compostela, EspaÃ±a (SCQ)","Santander, EspaÃ±a (SDR)","Torre Molinos, EspaÃ±a (UTL)","Valverde, EspaÃ±a (VDE)","Tenerife, Islas Canarias, EspaÃ±a (TCI)","Vigo, EspaÃ±a (VGO)","Vitoria, EspaÃ±a (VIT)","Valencia, EspaÃ±a (VLC)","Valladolid, EspaÃ±a (VLL)","Jerez De La Frontera, EspaÃ±a (XRY)","Toledo, EspaÃ±a (XTJ)","Zaragoza, EspaÃ±a (ZAZ)","Alicante Railway Station, EspaÃ±a (YJE)","Merida, EspaÃ±a (QWX)","Santillana del Mar, EspaÃ±a (S38)","Salamanca, EspaÃ±a (SLM)","Caceres, EspaÃ±a (QUQ)","San Antonio, EspaÃ±a (SN1)","Puerto de la Cruz, EspaÃ±a (PCZ)","Barajas, EspaÃ±a (BJ1)","Figueretas, EspaÃ±a (FU1)","Calella, EspaÃ±a (CE5)","Cala Mayor, EspaÃ±a (CM5)","OcaÃ±a, EspaÃ±a (OC1)","San Lorenzo del Escorial, EspaÃ±a (SL4)","Jimma, EtiopÃ­a (JIM)","Dire Dawa, EtiopÃ­a (DIR)","Addis Ababa, EtiopÃ­a (ADD)","Helsinki, Finlandia (HEL)","Joensuu, Finlandia (JOE)","Ivalo, Finlandia (IVL)","Kokkola, Finlandia (KOK)","Kuopio, Finlandia (KUO)","Kittila, Finlandia (KTT)","Kemi, Finlandia (KEM)","Kuusamo, Finlandia (KAO)","Jyvaskyla, Finlandia (JYV)","Kajaani, Finlandia (KAJ)","Lappeenranta, Finlandia (LPP)","Mikkeli, Finlandia (MIK)","Mariehamn, Finlandia (MHQ)","Pori, Finlandia (POR)","Oulu, Finlandia (OUL)","Laponia, Finlandia (RVI)","Ylivieska, Finlandia (YLI)","Varkaus, Finlandia (VRK)","Turku, Finlandia (TKU)","Tampere, Finlandia (TMP)","Vaasa, Finlandia (VAA)","Rovaniemi, Finlandia (RVN)","Savonlinna, Finlandia (SVL)","Seinajoki, Finlandia (SJY)","Savusavu, Fiji Islands (SVU)","Suva, Fiji Islands (SUV)","Taveuni, Fiji Islands (TVU)","Nadi, Fiji Islands (NAN)","Labasa, Fiji Islands (LBS)","Ba City, Fiji Islands (BFJ)","Mount Pleasant, Islas Malvinas (MPN)","Pohnpei, Micronesia (PNI)","Truk, Micronesia (TKK)","Toulon, Francia (TLN)","Toulouse, Francia (TLS)","Estrasburgo, Francia (SXB)","Sophia Antipolis, Francia (SXD)","Tours, Francia (TUF)","Valence, Francia (VAF)","Quimper, Francia (UIP)","Rouen, Francia (URO)","St Nazaire, Francia (SNR)","Rennes, Francia (RNS)","Roanne, Francia (RNE)","St Brieuc, Francia (SBK)","Vannes, Francia (VNE)","Vichy, Francia (VHY)","Dunkerque, Francia (XDK)","St Malo, Francia (XSB)","Cahors, Francia (ZAO)","Arles, Francia (ZAF)","Menton, Francia (XMT)","Pontoise, Francia (POX)","Paris, Francia (PAR)","Perpignan, Francia (PGF)","Poitiers, Francia (PIS)","Perigueux, Francia (PGX)","Arras, Francia (QRV)","Reims, Francia (RHE)","Rochefort, Francia (RCO)","Rodez, Francia (RDZ)","Amiens, Francia (QAM)","Pau Fr, Francia (PUF)","Propriano, Francia (PRP)","Montpellier, Francia (MPL)","Marsella, Francia (MRS)","Metz, Francia (MZM)","Niza, Francia (NCE)","Annecy, Francia (NCY)","Morlaix, Francia (MXN)","Nantes, Francia (NTE)","Albi, Francia (LBI)","Lannion, Francia (LAI)","La Baule, Francia (LBY)","Lourdes, Francia (LDE)","Le Havre, Francia (LEH)","Lille, Francia (LIL)","Limoges, Francia (LIG)","Le Mans, Francia (LME)","Lorient, Francia (LRT)","La Rochelle, Francia (LRH)","Le Puy, Francia (LPY)","Les Sables, Francia (LSO)","Le Touquet, Francia (LTQ)","Mulhouse, Francia (MLH)","St Tropez, Francia (LTT)","Lyon, Francia (LYS)","Montlucon, Francia (MCU)","Brest, Francia (BES)","Biarritz, Francia (BIQ)","Bastia, Francia (BIA)","Avignon, Francia (AVN)","Aurillac, Francia (AUR)","Agen, Francia (AGF)","Ajaccio, Francia (AJA)","Angouleme, Francia (ANG)","Angers, Francia (ANE)","Burdeos, Francia (BOD)","Brive La Gaill, Francia (BVE)","Beziers, Francia (BZR)","Cholet, Francia (CET)","Cannes, Francia (CEQ)","Cherbourg, Francia (CER)","Clermont Ferrand, Francia (CFE)","Caen, Francia (CFR)","Carcassonne, Francia (CCF)","Chateauroux, Francia (CHR)","Colmar, Francia (CMR)","Calvi, Francia (CLY)","Chambery, Francia (CMF)","Cognac, Francia (CNG)","Gap France, Francia (GAT)","Frejus, Francia (FRJ)","Figari, Francia (FSC)","Nimes, Francia (FNI)","Grenoble, Francia (GNB)","Dijon, Francia (DIJ)","Castres, Francia (DCM)","Deauville, Francia (DOL)","Dieppe, Francia (DPE)","Dinard, Francia (DNR)","Dole, Francia (DLE)","Calais, Francia (CQF)","Creil, Francia (CSF)","Courchevel, Francia (CVF)","Mulhouse Basel, Francia (EAP)","St Etienne, Francia (EBU)","La Roche, Francia (EDM)","Bergerac, Francia (EGC)","Nancy, Francia (ENC)","Epinal, Francia (EPL)","Evreux, Francia (EVX)","Metz Nancy, Francia (ETZ)","Albany, Gabon (ABY)","Libreville, Gabon (LBV)","Franceville Mvengue, Gabon (MVB)","Port Gentil, Gabon (POG)","Papa Westray, Inglaterra (PPW)","Portsmouth, Inglaterra (PME)","Plymouth, Inglaterra (PLH)","Perth, Inglaterra (PSL)","Leicester, Inglaterra (QEW)","Penzance, Inglaterra (PZE)","Bath, Inglaterra (QQX)","York, Inglaterra (QQY)","Newcastle, Inglaterra (NCL)","Sanday, Inglaterra (NDY)","Manston, Inglaterra (MSE)","Teesside, Inglaterra (MME)","Oxford, Inglaterra (OXF)","Northampton, Inglaterra (ORM)","Oban, Inglaterra (OBN)","Norwich, Inglaterra (NWI)","North Ronaldsay, Inglaterra (NRL)","Nottingham UK, Inglaterra (NQT)","Newquay, Inglaterra (NQY)","Leeds, Inglaterra (LBA)","Londonderry, Inglaterra (LDY)","Londres, Inglaterra (LON)","Lands End, Inglaterra (LEQ)","Luton, Inglaterra (LTN)","Lerwick, Inglaterra (LSI)","Liverpool, Inglaterra (LPL)","Manchester, Inglaterra (MAN)","Lydd, Inglaterra (LYX)","Mildenhall, Inglaterra (MHZ)","Kings Lynn, Inglaterra (KNF)","Milton Keynes, Inglaterra (KYN)","Kirkwall, Inglaterra (KOI)","Isles Of Scilly, Inglaterra (ISC)","Ipswich, Inglaterra (IPW)","Jersey, Inglaterra (JER)","Humberside, Inglaterra (HUY)","Islay, Inglaterra (ILY)","Inverness, Inglaterra (INV)","Isle Of Man, Inglaterra (IOM)","Aberdeen, Inglaterra (ABZ)","Alderney, Inglaterra (ACI)","St. Andrews, Inglaterra (ADX)","Birmingham, Inglaterra (BHX)","Belfast, Inglaterra (BFS)","Bury St Edmunds, Inglaterra (BEQ)","Benbecula, Inglaterra (BEB)","Puerto Caldera, Inglaterra (CAL)","Carlisle, Inglaterra (CAX)","Cambridge, Inglaterra (CBG)","Chester, Inglaterra (CEG)","Barrow In Furness, Inglaterra (BWF)","Barra, Inglaterra (BRR)","Bristol, Inglaterra (BRS)","Brighton, Inglaterra (BSH)","Bradford, Inglaterra (BRF)","Bournemouth, Inglaterra (BOH)","Blackpool, Inglaterra (BLK)","Exeter, Inglaterra (EXT)","Fair Isle, Inglaterra (FIE)","Eday, Inglaterra (EOI)","Edimburgo, Inglaterra (EDI)","Coventry, Inglaterra (CVT)","Cardiff, Inglaterra (CWL)","Dornoch, Inglaterra (DOC)","Dundee, Inglaterra (DND)","Doncaster, Inglaterra (DCS)","Gloucester, Inglaterra (GLO)","Glasgow, Inglaterra (GLA)","Fort William, Inglaterra (FWM)","Guernsey, Inglaterra (GCI)","Grimsby, Inglaterra (GSY)","Harrogate, Inglaterra (HRT)","Hatfield, Inglaterra (HTF)","Holyhead, Inglaterra (HLY)","Coll Island, Inglaterra (COL)","Chesterfield, Inglaterra (ZFI)","Salisbury, Inglaterra (XSR)","Rugby, Inglaterra (XRU)","Berwick Upon Tweed, Inglaterra (XQG)","Preston, Inglaterra (XPT)","Nottingham, Inglaterra (XNM)","Wakefield Westgate, Inglaterra (XWD)","Stoke On Trent, Inglaterra (XWH)","Wolverhampton, Inglaterra (XVW)","Stockport, Inglaterra (XVA)","Stevenage, Inglaterra (XVJ)","Braintree, Inglaterra (WXF)","Wick, Inglaterra (WIC)","Westray, Inglaterra (WRY)","Shetland Islands Area, Inglaterra (SDZ)","Southend, Inglaterra (SEN)","Isle Of Skye Hebrides Islands, Inglaterra (SKL)","Southampton, Inglaterra (SOU)","Stronsay, Inglaterra (SOY)","Unst, Inglaterra (UNT)","Tiree, Inglaterra (TRE)","Swindon, Inglaterra (SWI)","Swansea, Inglaterra (SWS)","Sheffield, Inglaterra (SZD)","Stornoway, Inglaterra (SYY)","Granada, Grenada (GND)","Batumi, Georgia (BUS)","Tbilisi, Georgia (TBS)","Cayenne, French Guiana (CAY)","Accra, Ghana (ACC)","Gibraltar, Gibraltar (GIB)","Nuuk, Groenlandia (GOH)","Sisimiut, Groenlandia (JHS)","Aasiaat, Groenlandia (JEG)","Groennedal, Groenlandia (JGR)","Narsaq, Groenlandia (JNS)","Qaqortoq, Groenlandia (JJU)","Ilulissat, Groenlandia (JAV)","Maniitsoq, Groenlandia (JSU)","Alluitsup Paa, Groenlandia (LLU)","Narsarsuaq, Groenlandia (UAK)","Kangerlussuaq, Groenlandia (SFJ)","Banjul, Gambia (BJL)","Conakry, Guinea (CKY)","Saint Martin, Guadeloupe (CCE)","Basse Terre, Guadeloupe (BBR)","La Desirade, Guadeloupe (DSD)","Pointe A Prite, Guadeloupe (PTP)","St Barthelemy, Guadeloupe (SBH)","Malabo, Guinea Ecuatorial (SSG)","Thessaloniki, Grecia (SKG)","Samos, Grecia (SMI)","Rhodes, Grecia (RHO)","Volos, Grecia (VOL)","Zakynthos, Grecia (ZTH)","Santorini Thira Island, Grecia (JTR)","Preveza, Grecia (PVK)","Paros, Grecia (PAS)","Leros, Grecia (LRS)","Mytilene, Grecia (MJT)","Limnos, Grecia (LXS)","Syros Island, Grecia (JSY)","Naxos, Grecia (JNX)","Skiathos, Grecia (JSI)","Kos, Grecia (KGS)","Kalamata, Grecia (KLX)","Kastoria, Grecia (KSO)","Kavalla, Grecia (KVA)","Chios, Grecia (JKH)","Mykonos, Grecia (JMK)","Ikaria Island, Grecia (JIK)","Ioannina, Grecia (IOA)","Kefalonia, Grecia (EFL)","Patras, Grecia (GPA)","Heraklion, Grecia (HER)","Alexandroupolis, Grecia (AXD)","Atenas, Grecia (ATH)","Karpathos, Grecia (AOK)","Kerkyra, Grecia (CFU)","Chania, Grecia (CHQ)","Antigua Guatemala, Guatemala (ATG)","Atitlan, Guatemala (ATT)","Guatemala, Guatemala (GUA)","Flores, Guatemala (FRS)","Rio Dulce, Guatemala (LCF)","Puerto Barrios, Guatemala (PBR)","Playa Grande, Guatemala (PKJ)","Retalhuleu, Guatemala (RER)","Tikal, Guatemala (TKM)","Guam, Guam (GUM)","Bissau, Guinea Bissau (OXB)","Georgetown, Guyana (GEO)","Hong Kong, Hong Kong (HKG)","Cauquira, Honduras (CDD)","La Ceiba, Honduras (LCE)","Tela, Honduras (TEA)","Tegucigalpa, Honduras (TGU)","San Pedro Sula, Honduras (SAP)","Roatan, Honduras (RTB)","Comayagua, Honduras (XPL)","Puerto CortÃ©s, Honduras (PC3)","Choluteca, Honduras (CH0)","Danli, Honduras (D4N)","Gracias, Honduras (GAC)","Puerto Lempira, Honduras (PEU)","El Progreso, Honduras (A20)","Tocoa, Honduras (TCF)","Santa Rosa de Copan, Honduras (SDH)","Utila, Honduras (UII)","Guanaja, Honduras (GJA)","Slunj, Croacia (HR3)","Zagreb, Croacia (ZAG)","Zadar, Croacia (ZAD)","Rijeka, Croacia (RJK)","Split, Croacia (SPU)","Pula, Croacia (PUY)","Dubrobvnik, Croacia (DBV)","Cap Haitien, Haiti (CAP)","Port Au Prince, Haiti (PAP)","Budapest, HungrÃ­a (BUD)","Saarmelleek, HungrÃ­a (SOB)","Solo, Indonesia (SOC)","Sorong, Indonesia (SOQ)","Semarang, Indonesia (SRG)","Surabaya, Indonesia (SUB)","Tanjung Pandan, Indonesia (TJQ)","Bandar Lampung, Indonesia (TKG)","Timika, Indonesia (TIM)","Ujung Pandang, Indonesia (UPG)","Ubud, Indonesia (UBU)","Seminyak, Indonesia (SMN)","Batam, Indonesia (BTH)","Banda Aceh, Indonesia (BTJ)","Biak, Indonesia (BIK)","Balikpapan, Indonesia (BPN)","Cirebon, Indonesia (CBN)","Bali, Indonesia (BAJ)","Bandung, Indonesia (BDO)","Mataram, Indonesia (AMI)","Amahai, Indonesia (AHI)","Jambi, Indonesia (DJB)","Denpasar, Indonesia (DPS)","Cilacap, Indonesia (CXP)","Gunungsitoli, Indonesia (GNS)","Padang, Indonesia (PDG)","Pekanbaru, Indonesia (PKU)","Palembang, Indonesia (PLM)","Menado, Indonesia (MDC)","Medan, Indonesia (MES)","Jakarta, Indonesia (JKT)","Yogjakarta, Indonesia (JOG)","Kerry County, Irlanda (KIR)","Limerick, Irlanda (LMK)","Cork, Irlanda (ORK)","Knock, Irlanda (NOC)","Spiddal, Irlanda (NNR)","Galway, Irlanda (GWY)","Enniskillen, Irlanda (ENK)","Dublin, Irlanda (DUB)","Donegal, Irlanda (CFN)","Sligo, Irlanda (SXL)","Shannon, Irlanda (SNN)","Waterford, Irlanda (WAT)","Wexford, Irlanda (WEX)","Yotvata, Israel (YOT)","Rosh Pina, Israel (RPN)","Tel Aviv, Israel (TLV)","Ovda, Israel (VDA)","Elat, Israel (ETH)","Haifa, Israel (HFA)","Jerusalem, Israel (JRS)","Kiryat Shmona, Israel (KSW)","Kulu, India (KUU)","Jamnagar, India (JGA)","Jodhpur, India (JDH)","Jaipur, India (JAI)","Bagdogra, India (IXB)","Mangalore, India (IXE)","Jammu, India (IXJ)","Madurai, India (IXM)","Aurangabad, India (IXU)","Port Blair, India (IXZ)","Hyderabad, India (HYD)","Lucknow, India (LKO)","Madras, India (MAA)","Mysore, India (MYQ)","Nagpur, India (NAG)","Pune, India (PNQ)","Patna, India (PAT)","Porbandar, India (PBD)","Khajuraho, India (HJR)","Goa, India (GOI)","Diu In, India (DIU)","Delhi, India (DEL)","Kozhikode, India (CCJ)","Calcuta, India (CCU)","Coimbatore, India (CJB)","Kochi, India (COK)","Bombay, India (BOM)","Bangalore, India (BLR)","Agra, India (AGR)","Ahmedabad, India (AMD)","Vadodara, India (BDQ)","Bhuj, India (BHJ)","Bhopal, India (BHO)","Bhubaneswar, India (BBI)","Udaipur, India (UDR)","Thiruvananthapuram, India (TRV)","Tirupati, India (TIR)","Srinagar, India (SXR)","Simla, India (SLV)","Vishakhapatanam, India (VTZ)","Varanasi, India (VNS)","Baghdad, Irak (BGW)","Basra, Irak (BSR)","Yazd, Iran (AZD)","Shahre Kord, Iran (CQD)","Mashad, Iran (MHD)","Isfahan, Iran (IFN)","Kermanshah, Iran (KSH)","Zanjan, Iran (JWN)","Khorramabad, Iran (KHD)","Kerman, Iran (KER)","Shiraz, Iran (SYZ)","Teheran, Iran (THR)","ReykjavÃ­k, Islandia (149696)","Isafjordur, Islandia (IFJ)","Reykjavyk, Islandia (REK)","Kopasker, Islandia (OPA)","Egilsstadir, Islandia (EGS)","Akureyri, Islandia (AEY)","Alghero, Italia (AHO)","Ancona, Italia (AOI)","Albenga, Italia (ALL)","Bergamo, Italia (BGY)","Brindisi, Italia (BDS)","Cagliari, Italia (CAG)","Bolzano, Italia (BZO)","Bolonia, Italia (BLQ)","Bari, Italia (BRI)","Elba Island, Italia (EBA)","Crotone, Italia (CRV)","Cuneo, Italia (CUF)","Catania, Italia (CTA)","Genoa, Italia (GOA)","Grosseto, Italia (GRS)","San Giovanni Rotondo, Italia (GBN)","Forli, Italia (FRL)","Foggia, Italia (FOG)","Florencia, Italia (FLR)","Olbia, Italia (OLB)","Napoles, Italia (NAP)","Reggio Calabria, Italia (REG)","Padova, Italia (QPA)","Salerno, Italia (QSR)","Varese, Italia (QVA)","Ravenna, Italia (RAN)","Brescia, Italia (QBS)","Pescara, Italia (PSR)","Pisa, Italia (PSA)","Capri, Italia (PRJ)","Perugia, Italia (PEG)","Peschiei, Italia (PEJ)","Parma, Italia (PMF)","Palermo, Italia (PMO)","Ischia, Italia (ISH)","Milan, Italia (MIL)","Lecce, Italia (LCC)","Lucca, Italia (LCV)","Saluzzo, Italia (SZ2)","La Thuile, Italia (TH1)","Portofino, Italia (PTF)","La Spezia, Italia (LZ1)","Amalfi, Italia (LFI)","Positano, Italia (OSI)","Vietri Sul Mare, Italia (SUL)","Ravello, Italia (ELL)","Praiano, Italia (IAN)","PRATO, Italia (QPR)","Tropea Calabria, Italia (IT2)","Alberobello Bari, Italia (IT3)","Taormina, Italia (TFC)","Taranto, Italia (TAR)","Treviso, Italia (TSF)","Trieste, Italia (TRS)","San Domino Island, Italia (TQR)","Trapani, Italia (TPS)","Torino, Italia (TRN)","Tortoli, Italia (TTB)","Udine, Italia (UDN)","Venecia, Italia (VCE)","Lamezia Terme, Italia (SUF)","Sorrento, Italia (RRO)","Rimini, Italia (RMI)","Roma, Italia (ROM)","Siena, Italia (SAY)","Verona, Italia (VRN)","Vieste, Italia (VIF)","Vicenza, Italia (VIC)","Trento, Italia (ZIA)","Runaway Bay, Jamaica (A05)","Port Antonio, Jamaica (PAN)","Tryall, Jamaica (TRY)","Starfish, Jamaica (STF)","Rose Hall, Jamaica (RHL)","Freeport, Jamaica (FRE)","South Coast / Treasure Beach, Jamaica (SCT)","Montego Bay, Jamaica (MBJ)","Kingston, Jamaica (KIN)","Negril, Jamaica (NEG)","Ocho Rios, Jamaica (OCJ)","Maan, Jordan (MPQ)","Petra, Jordan (PE2)","Amman, Jordan (AMM)","Aqaba, Jordan (AQJ)","Aomori, JapÃ³n (AOJ)","Asahikawa, JapÃ³n (AKJ)","Akita, JapÃ³n (AXT)","Amami O Shima, JapÃ³n (ASJ)","Fukue, JapÃ³n (FUJ)","Fukuoka, JapÃ³n (FUK)","Yamagata, JapÃ³n (GAJ)","Hiroshima, JapÃ³n (HIJ)","Hakodate, JapÃ³n (HKD)","Morioka, JapÃ³n (HNA)","Saga, JapÃ³n (HSG)","Hachijo Jima, JapÃ³n (HAC)","Fukushima, JapÃ³n (FKS)","Miyako Jima, JapÃ³n (MMY)","Memambetsu, JapÃ³n (MMB)","Matsumoto, JapÃ³n (MMJ)","Misawa, JapÃ³n (MSJ)","Matsuyama, JapÃ³n (MYJ)","Obihiro, JapÃ³n (OBO)","Nagoya, JapÃ³n (NGO)","Nagasaki, JapÃ³n (NGS)","Narita, JapÃ³n (NRT)","Odate Noshiro, JapÃ³n (ONJ)","Okino Erabu, JapÃ³n (OKE)","Okayama, JapÃ³n (OKJ)","Oshima, JapÃ³n (OIM)","Oita, JapÃ³n (OIT)","Okinawa, JapÃ³n (OKA)","Osaka, JapÃ³n (OSA)","Miyazaki, JapÃ³n (KMI)","Kumamoto, JapÃ³n (KMJ)","Komatsu, JapÃ³n (KMQ)","Kochi, JapÃ³n (KCZ)","Niigata, JapÃ³n (KIJ)","Kita Kyushu, JapÃ³n (KKJ)","Kagoshima, JapÃ³n (KOJ)","Kushiro, JapÃ³n (KUH)","Izumo, JapÃ³n (IZO)","Ishigaki, JapÃ³n (ISG)","Iwami, JapÃ³n (IWJ)","Yokohama, JapÃ³n (YOK)","Wakkanai, JapÃ³n (WKJ)","Yonago, JapÃ³n (YGJ)","Sendai, JapÃ³n (SDJ)","Nakashibetsu, JapÃ³n (SHB)","Sapporo, JapÃ³n (SPK)","Kume Jima, JapÃ³n (UEO)","Kyoto, JapÃ³n (UKY)","Ube Jp, JapÃ³n (UBJ)","Tokio, JapÃ³n (TYO)","Tottori, JapÃ³n (TTJ)","Tsushima, JapÃ³n (TSJ)","Takamatsu, JapÃ³n (TAK)","Shonai, JapÃ³n (SYO)","Tokunoshima, JapÃ³n (TKN)","Tokushima, JapÃ³n (TKS)","Tanega Shima, JapÃ³n (TNE)","Toyama, JapÃ³n (TOY)","Samburu, Kenya (UAS)","Kisumu, Kenya (KIS)","Mombasa, Kenya (MBA)","Lokichoggio, Kenya (LKG)","Nyeri, Kenya (NYE)","Nanyuki, Kenya (NYK)","Malindi, Kenya (MYD)","Nairobi, Kenya (NBO)","Mara Lodges, Kenya (MRE)","Eldoret, Kenya (EDL)","Bishkek, Kyrgyzstan (FRU)","Phnom Penh, Cambodia (PNH)","Siem Reap, Cambodia (REP)","Maiana, Kiribati (MNK)","Tarawa, Kiribati (TRW)","Moroni, Comoros (YVA)","St Kitts, St. Kitts - Nevis (SKB)","Nevis, St. Kitts - Nevis (NEV)","Busan, Corea del Sur (PUS)","Kunsan, Corea del Sur (KUV)","Kwangju, Corea del Sur (KWJ)","Pohang, Corea del Sur (KPO)","Chinju, Corea del Sur (HIN)","Jeju City, Corea del Sur (CJU)","Cheongju, Corea del Sur (CJJ)","Sokcho, Corea del Sur (SHO)","Seul, Corea del Sur (SEL)","Yeosu, Corea del Sur (RSU)","Ulsan, Corea del Sur (USN)","Daegu, Corea del Sur (TAE)","Daejeon, Corea del Sur (QTW)","Kuwait, Kuwait (KWI)","Little Cayman, Islas Cayman (LYB)","Grand Cayman Island, Islas Cayman (GCM)","Cayman Brac, Islas Cayman (CYB)","Shimkent, Kazakstan (CIT)","Almaty, Kazakstan (ALA)","Ust Kamenogorsk, Kazakstan (UKK)","Astana, Kazakstan (TSE)","Vientiane, Rep. de Lao (VTE)","Beirut, LÃ­bano (BEY)","Santa Lucia, Saint Lucia (UVF)","Sigiriya, Sri Lanka (GIU)","Colombo, Sri Lanka (CMB)","Monrovia, Liberia (MLW)","Sinoe, Liberia (SNI)","Maseru, Lesotho (MSU)","Lesobeng, Lesotho (LES)","Kaunas, Lithuania (KUN)","Klaipeda, Lithuania (KLJ)","Palanga, Lithuania (PLQ)","Vilnius, Lithuania (VNO)","Luxemburgo, Luxemburgo (LUX)","Liepaja, Latvia (LPX)","Daugavpils, Latvia (DGP)","Riga, Latvia (RIX)","Tobruk, Libia (TOB)","Tripoli, Libia (TIP)","Benghazi, Libia (BEN)","Al Hoceima, Marruecos (AHU)","Agadir, Marruecos (AGA)","Casablanca, Marruecos (CAS)","Fez Ma, Marruecos (FEZ)","El Aaiun, Marruecos (EUN)","Essaouira, Marruecos (ESU)","Ourzazate, Marruecos (OZZ)","Rabat, Marruecos (RBA)","Marrakech, Marruecos (RAK)","Nador, Marruecos (NDR)","Oujda, Marruecos (OUD)","Tanger, Marruecos (TNG)","Dakhla, Marruecos (VIL)","Monte Carlo, Monaco (MCM)","Chisinau, Moldova (KIV)","Moldovia, Moldova (MV8)","Tamatave, Madagascar (TMM)","Antananarivo, Madagascar (TNR)","Tulear, Madagascar (TLE)","Saint Marie, Madagascar (SMS)","Majunga, Madagascar (MJN)","Nossi Be, Madagascar (NOS)","Morondava, Madagascar (MOQ)","Majuro, Marshall Islands (MAJ)","Kwajalein, Marshall Islands (KWA)","Bikini Atoll, Marshall Islands (BII)","Ohrid, Macedonia (OHD)","Skopje, Macedonia (SKP)","Mopti, Mali (MZI)","Bamako, Mali (BKO)","Heho, Myanmar (HEH)","Nyaung, Myanmar (NYU)","Yangon, Myanmar (RGN)","Mandalay, Myanmar (MDL)","Ulan Bator, Mongolia (ULN)","Macau, Macau (MFM)","Tinian, Marianna Islands (TIQ)","Saipan, Marianna Islands (SPN)","Rota, Marianna Islands (ROP)","Fort De France, Martinique (FDF)","Nouakchott, Mauritania (NKC)","Nouadhibou, Mauritania (NDB)","Malta, Malta (MLA)","Gozo, Malta (GZM)","Mauricio, Mauritius (MRU)","Rodrigues Island, Mauritius (RRG)","Maldivas, Maldives (MLE)","Lilongwe, Malawi (LLW)","Blantyre, Malawi (BLZ)","Chelinda, Malawi (CEH)","Ciudad Obregon, Mexico (CEN)","Ciudad Del Carmen, Mexico (CME)","Cordoba, Mexico (CO1)","Chignahuapan, Mexico (CP1)","Campeche, Mexico (CPE)","Ciudad Juarez, Mexico (CJS)","Colima, Mexico (CLQ)","Leon, Mexico (BJX)","Bahia De Los Angeles, Mexico (BHL)","Barrancas del Cobre, Mexico (BC1)","Apatzingan, Mexico (AZG)","Aguascalientes, Mexico (AGU)","Acapulco, Mexico (ACA)","Guaymas, Mexico (GYM)","Hermosillo, Mexico (HMO)","Huejotzingo, Mexico (HP2)","Huamantla, Mexico (HT2)","Guadalajara, Mexico (GDL)","Grutas de Cacahuamilpa, Mexico (GG2)","Guanajuato, Mexico (GMO)","Ensenada, Mexico (ESE)","El Oro, Mexico (EM1)","Durango, Mexico (DGO)","Celestin, Mexico (CY1)","Coatepec, Mexico (CV2)","Cozumel, Mexico (CZM)","Chichen Itza, Mexico (CZA)","Chihuahua, Mexico (CUU)","Cuernavaca, Mexico (CVJ)","Ciudad Victoria, Mexico (CVM)","Chetumal, Mexico (CTM)","Ciudad Constitucion, Mexico (CUA)","Culiacan, Mexico (CUL)","Cancun, Mexico (CUN)","Los Mochis, Mexico (LMM)","Lagos De Moreno, Mexico (LOM)","Monclova, Mexico (LOV)","Loreto, Mexico (LTO)","Morelia, Mexico (MLM)","Merida, Mexico (MID)","Ciudad de Mexico, Mexico (MEX)","Matamoros, Mexico (MAM)","Lazaro Cardenas, Mexico (LZC)","La Paz, Mexico (LAP)","Jalcomulco, Mexico (JV3)","Ixtepec, Mexico (IZT)","Xalapa, Mexico (JAL)","Isla Mujeres, Mexico (ISJ)","Holbox, Mexico (IH1)","Huatulco, Mexico (HUX)","Ciudad Mante, Mexico (MMC)","Mazatlan, Mexico (MZT)","Mexicali, Mexico (MXL)","Monterrey, Mexico (MTY)","Minatitlan, Mexico (MTT)","Nogales, Mexico (NOG)","Nuevo Laredo, Mexico (NLD)","Oaxaca, Mexico (OAX)","Otongo, Mexico (OT2)","Reynosa, Mexico (REX)","Real del Monte, Mexico (RH1)","Coatzacoalcos, Mexico (QTZ)","Queretaro, Mexico (QRO)","Papantla, Mexico (PV4)","Puerto Vallarta, Mexico (PVR)","Puerto Escondido, Mexico (PXM)","Palenque, Mexico (PQM)","Poza Rica, Mexico (PAZ)","Puebla, Mexico (PBC)","Pachuca, Mexico (PC1)","Playa del Carmen, Mexico (PCM)","Piedras Negras, Mexico (PDS)","Pinotepa Nacional, Mexico (PNO)","Puerto Penasco, Mexico (PPE)","Riviera Maya  Tulum, Mexico (RM4)","Riviera Nayarit, Mexico (RNA)","San Felipe, Mexico (SFH)","San miguel de Allende, Mexico (SG2)","San Jose Iturbide, Mexico (SG3)","Salina Cruz, Mexico (SCX)","San Juan del Rio, Mexico (SQ2)","Santa Rosalia, Mexico (SRL)","San Luis Potosi, Mexico (SLP)","Saltillo, Mexico (SLW)","San Quintin, Mexico (SNQ)","San Jose del Cabo, Mexico (SJD)","Tijuana, Mexico (TIJ)","Toluca, Mexico (TLC)","Tlalpujahua, Mexico (TM1)","San Cristobal De Las Casas, Mexico (SZT)","Tapachula, Mexico (TAP)","Tampico, Mexico (TAM)","Tuxtla Gutierrez, Mexico (TGZ)","Tehuacan, Mexico (TCN)","Taxco, Mexico (TG1)","Tierra Blanca, Mexico (TG4)","Uruapan, Mexico (UPN)","Torreon, Mexico (TRC)","Tequisquiapan, Mexico (TQ1)","Tepic, Mexico (TPQ)","Tlaxcala, Mexico (TT1)","San Luis Rio Colorado, Mexico (UAC)","Tepotzotlan, Mexico (TZ9)","Valle de Bravo, Mexico (VM2)","Villa Constitucion, Mexico (VIB)","Veracruz, Mexico (VER)","Villahermosa, Mexico (VSA)","Alamos, Mexico (XAL)","Xico, Mexico (XV1)","Ixtapa, Mexico (ZIH)","Zacatecas, Mexico (ZCL)","Manzanillo, Mexico (ZLO)","ZacatlÃ¡n, Mexico (ZP3)","Silao, Mexico (SL1)","Celaya, Mexico (CL1)","Irapuato, Mexico (IP1)","Puerto Morelos, Mexico (PT2)","Puerto Aventuras, Mexico (PT3)","Ciudad Valles, Mexico (CV1)","Valladolid, Mexico (VL1)","Bacalar, Mexico (BC2)","Creel, Mexico (CR1)","El Fuerte, Mexico (FR1)","Posadas Barrancas, Mexico (PB1)","Uayamon, Mexico (UA1)","Cholula, Mexico (CH1)","Ciudad Madero, Mexico (CM1)","Copper Canyon, Mexico (CC1)","El Tajin, Mexico (TJ1)","Salamanca, Mexico (SL2)","Patzcuaro, Mexico (PT4)","Uxmal, Mexico (UX1)","Corredor Turistico, Mexico (CTR)","Nuevo Vallarta, Mexico (NVR)","Cozumel Zona 3, Mexico (CZ3)","Cozumel Zona 2, Mexico (CZ2)","Cozumel Zona 1, Mexico (CZ1)","Cabo San Lucas, Mexico (A06)","TeotihuacÃ¡n, Mexico (TE2)","Chiapas, Mexico (CI1)","Akumal, Mexico (AK5)","TepatitlÃ¡n de Morelos, Mexico (M21)","TÃºxpam de RodrÃ­guez Cano, Mexico (M22)","Naucalpan, Mexico (M19)","Tixkokob, Mexico (M20)","Sayulita, Mexico (M18)","Tlaquepaque, Mexico (M02)","Tula de Allende, Mexico (M01)","Kohunlich, Mexico (M03)","Punta Mita, Mexico (M04)","Zapopan, Mexico (M06)","Zihuatanejo, Mexico (M07)","Mahahual, Mexico (M09)","San Pedro Garza GarcÃ­a, Mexico (M10)","Apodaca, Mexico (M12)","ParaÃ­so, Mexico (M11)","TemozÃ³n, Mexico (M13)","Gomez Palacio, Mexico (M14)","Tangolunda, Mexico (M15)","BucerÃ­as, Mexico (M16)","Sonora, Mexico (M17)","Mazamitla, Mexico (MZ1)","Rincon de Guayabitos, Mexico (RG1)","Tequila, Mexico (TQ2)","Orizaba, Mexico (OZ1)","Costa Mujeres, Mexico (CMJ)","Tecate, Mexico (TCT)","Tawau, Malasia (TWU)","Kuala Terengganu, Malasia (TGG)","Tioman, Malasia (TOD)","Sandakan, Malasia (SDK)","Sibu, Malasia (SBW)","Penang, Malasia (PEN)","Pangkor, Malasia (PKG)","Marudi, Malasia (MUR)","Miri, Malasia (MYY)","Ipoh, Malasia (IPH)","Johor Bahru, Malasia (JHB)","Kuching, Malasia (KCH)","Kota Bharu, Malasia (KBR)","Kuala Lumpur, Malasia (KUL)","Kuantan, Malasia (KUA)","Kerteh, Malasia (KTE)","Lawas, Malasia (LWY)","Malacca, Malasia (MKZ)","Mukah, Malasia (MKM)","Limbang, Malasia (LMN)","Long Akah, Malasia (LKH)","Langkawi, Malasia (LGK)","Lahad Datu, Malasia (LDU)","Labuan, Malasia (LBU)","Alor Setar, Malasia (AOR)","Kota Kinabalu, Malasia (BKI)","Nampula, Mozambique (APL)","Beira, Mozambique (BEW)","Maputo, Mozambique (MPM)","Tete, Mozambique (TET)","Vilanculos, Mozambique (VNX)","Windhoek, Namibia (WDH)","Walvis Bay, Namibia (WVB)","Rosh Pina, Namibia (RHN)","Noumea, New Caledonia (NOU)","Lifou, New Caledonia (LIF)","Koumac, New Caledonia (KOC)","Ile Des Pins, New Caledonia (ILP)","Niamey, Niger (NIM)","Port Harcourt, Nigeria (PHC)","Kano, Nigeria (KAN)","Lagos, Nigeria (LOS)","Abuja, Nigeria (ABV)","Managua, Nicaragua (MGA)","San Juan del Sur Rivas, Nicaragua (SJN)","Jinotepe, Nicaragua (JI9)","Bocao, Nicaragua (OA9)","Chinandega, Nicaragua (CH9)","EstelÃ, Nicaragua (ET9)","Granada, Nicaragua (GN9)","Jinotega, Nicaragua (JN9)","Leon, Nicaragua (LN9)","Somoto, Nicaragua (OM9)","Masaya, Nicaragua (MY9)","Matagalpa, Nicaragua (MT9)","Ocotal, Nicaragua (OCT)","San Carlos, Nicaragua (NCR)","Rivas, Nicaragua (ECI)","Puerto Cabezas RegiÃ³n AutÃ³noma AtlÃ¡ntico Norte, Nicaragua (PUZ)","Bluefields RegiÃ³n AutÃ³noma AtlÃ¡ntico Sur, Nicaragua (BEF)","Juigalpa, Nicaragua (JGP)","Uden, Holanda (UDE)","Utrecht, Holanda (UTC)","Rotterdam, Holanda (RTM)","Leiden, Holanda (LID)","Lelystad, Holanda (LEY)","Maastricht, Holanda (MST)","Amsterdam, Holanda (AMS)","Eindhoven, Holanda (EIN)","Enschede, Holanda (ENS)","Groningen, Holanda (GRQ)","La Haya, Holanda (HAG)","Hammerfest, Noruega (HFT)","Haugesund, Noruega (HAU)","Orsta Volda, Noruega (HOV)","Hamar, Noruega (HMR)","Gol City, Noruega (GLL)","Floro, Noruega (FRO)","Harstad Narvik, Noruega (EVE)","Forde, Noruega (FDE)","Geilo, Noruega (DLD)","Alta, Noruega (ALF)","Andenes, Noruega (ANX)","Aalesund, Noruega (AES)","Bardufoss, Noruega (BDU)","Bergen, Noruega (BGO)","Bronnoysund, Noruega (BNN)","Bodo, Noruega (BOO)","Mo I Rana, Noruega (MQN)","Molde, Noruega (MOL)","Notodden, Noruega (NTB)","Narvik, Noruega (NVK)","Oslo, Noruega (OSL)","Namsos, Noruega (OSY)","Maloy, Noruega (QFQ)","Lakselv, Noruega (LKL)","Leknes, Noruega (LKN)","Mosjoen, Noruega (MJF)","Longyearbyen, Noruega (LYR)","Mehamn, Noruega (MEH)","Kirkenes, Noruega (KKN)","Kristiansund, Noruega (KSU)","Kristiansand, Noruega (KRS)","Honningsvag, Noruega (HVG)","Roros, Noruega (RRS)","Roervik, Noruega (RVK)","Sandane, Noruega (SDN)","Skien, Noruega (SKE)","Stokmarknes, Noruega (SKN)","Sogndal, Noruega (SOG)","Stord, Noruega (SRP)","Sandnessjoen, Noruega (SSJ)","Stavanger, Noruega (SVG)","Svolvaer, Noruega (SVJ)","Vardoe, Noruega (VAW)","Fagernes, Noruega (VDB)","Vadso, Noruega (VDS)","Trondheim, Noruega (TRD)","Tromso, Noruega (TOS)","Lille Hammer, Noruega (XXL)","Steinkjer, Noruega (XKJ)","Fauske, Noruega (ZXO)","Katmandu, Nepal (KTM)","Phaplu, Nepal (PPL)","Pokhara, Nepal (PKR)","Nauru Island, Nauru (INU)","Niue Island, Niue (IUE)","Invercargill, Nueva Zelanda (IVC)","Kerikeri, Nueva Zelanda (KKE)","Kaitaia, Nueva Zelanda (KAT)","Milford Sound, Nueva Zelanda (MFN)","Palmerston, Nueva Zelanda (PMR)","Paraparaumu, Nueva Zelanda (PPQ)","Picton, Nueva Zelanda (PCN)","Oamaru, Nueva Zelanda (OAM)","Napier Hastings, Nueva Zelanda (NPE)","New Plymouth, Nueva Zelanda (NPL)","Nelson, Nueva Zelanda (NSN)","Mount Cook, Nueva Zelanda (MON)","Masterton, Nueva Zelanda (MRO)","Christchurch, Nueva Zelanda (CHC)","Coromandel, Nueva Zelanda (CMV)","Blenheim, Nueva Zelanda (BHE)","Auckland, Nueva Zelanda (AKL)","Dunedin, Nueva Zelanda (DUD)","Great Barrier Island, Nueva Zelanda (GBZ)","Gisborne, Nueva Zelanda (GIS)","Hamilton, Nueva Zelanda (HLZ)","Hokitika, Nueva Zelanda (HKK)","Queenstown, Nueva Zelanda (ZQN)","Westport, Nueva Zelanda (WSZ)","Whangarei, Nueva Zelanda (WRE)","Wanaka, Nueva Zelanda (WKA)","Wellington, Nueva Zelanda (WLG)","Whakatane, Nueva Zelanda (WHK)","Wanganui, Nueva Zelanda (WAG)","Thames, Nueva Zelanda (TMZ)","Timaru, Nueva Zelanda (TIU)","Te Anau, Nueva Zelanda (TEU)","Tauranga, Nueva Zelanda (TRG)","Taupo, Nueva Zelanda (TUO)","Rotorua, Nueva Zelanda (ROT)","Sur Om, OmÃ¡n (SUH)","Salalah, OmÃ¡n (SLL)","Muscat, OmÃ¡n (MCT)","PanamÃ¡, PanamÃ¡ (PTY)","David, PanamÃ¡ (DAV)","Changuinola, PanamÃ¡ (CHX)","Bocas Del Toro, PanamÃ¡ (BOC)","PanamÃ¡ Playa, PanamÃ¡ (A07)","Playa Blanca, PanamÃ¡ (AML)","Playa Bonita, PanamÃ¡ (A02)","Colon, PanamÃ¡ (ONX)","Andahuaylas, Peru (ANS)","Huaraz, Peru (ATA)","Cajamarca, Peru (CJA)","Chachapoyas, Peru (CHH)","Pisco, Peru (PIO)","Ayacucho, Peru (AYP)","Jauja, Peru (JAU)","Moyobamba, Peru (MBP)","Tacna, Peru (TCQ)","Talara, Peru (TYL)","Pacasmayo, Peru (PA1)","San Roman, Peru (SA2)","Jaen, Peru (JAE)","Mancora-Talara, Peru (A13)","Mancora-Piura, Peru (MCA)","Alerta, Peru (ALD)","Bellavista, Peru (BLP)","Chimbote, Peru (CHM)","Iberia, Peru (IBP)","Rioja, Peru (RIJ)","Rodriguez de Mendoza, Peru (RIM)","Santa Maria, Peru (SMG)","Tingo MarÃ­a, Peru (TGI)","Quince Mil, Peru (UMI)","Atalaya, Peru (AYX)","Ilo, Peru (ILQ)","Contamana, Peru (CTF)","Caballococha, Peru (LHC)","Requena, Peru (REQ)","Satipo, Peru (SFK)","San Pablo, Peru (SPO)","San Francisco, Peru (SQD)","Tocache, Peru (TCG)","Uchiza, Peru (UCZ)","Saposoa, Peru (SQU)","Moquegua, Peru (PE5)","Santa Clara, Peru (SCP)","Puno, Peru (P1N)","Nazca, Peru (P2N)","Valle Sagrado, Peru (P3N)","Aguas Calientes, Peru (AG1)","Paracas, Peru (PR9)","Vichayito-Piura, Peru (VCH)","Vichayito-Talara , Peru (VC2)","Tarapoto, Peru (TPP)","Trujillo, Peru (TRU)","Tumbes, Peru (TBP)","Ica, Peru (ICA)","Chiclayo, Peru (CIX)","Arequipa, Peru (AQP)","Cusco, Peru (CUZ)","Huanuco, Peru (HUU)","Puerto Maldonado, Peru (PEM)","Pucalpa, Peru (PCL)","Piura, Peru (PIU)","Lima, Peru (LIM)","Juliaca, Peru (JUL)","Iquitos, Peru (IQT)","Papeete, French Polynesia (PPT)","Raiatea, French Polynesia (RFP)","Rangiroa Island, French Polynesia (RGI)","Moorea, French Polynesia (MOZ)","Huahine, French Polynesia (HUH)","Atuona, French Polynesia (AUQ)","Bora Bora, French Polynesia (BOB)","Tikehau, French Polynesia (TIH)","Tari, Papua Nueva Guinea (TIZ)","Tububil, Papua Nueva Guinea (TBG)","Vanimo, Papua Nueva Guinea (VAI)","Wewak, Papua Nueva Guinea (WWK)","Aragip, Papua Nueva Guinea (ARP)","Hoskins, Papua Nueva Guinea (HKN)","Mt Hagen, Papua Nueva Guinea (HGU)","Alotau, Papua Nueva Guinea (GUR)","Goroka, Papua Nueva Guinea (GKA)","Rabaul, Papua Nueva Guinea (RAB)","Port Moresby, Papua Nueva Guinea (POM)","Kokoro, Papua Nueva Guinea (KOR)","Kavieng, Papua Nueva Guinea (KVG)","Lae Pg, Papua Nueva Guinea (LAE)","Lihir Island, Papua Nueva Guinea (LNV)","Mendi, Papua Nueva Guinea (MDU)","Madang, Papua Nueva Guinea (MAG)","Manus Island, Papua Nueva Guinea (MAS)","Kalibo, Filipinas (KLO)","Ilo, Filipinas (ILO)","Puerto Princesa, Filipinas (PPS)","Manila, Filipinas (MNL)","Davao, Filipinas (DVO)","Cebu, Filipinas (CEB)","Cagayan, Filipinas (CGY)","Baguio, Filipinas (BAG)","Angeles Mabalacat, Filipinas (CRK)","Tacloban, Filipinas (TAC)","San Fernando, Filipinas (SFE)","Subic Bay, Filipinas (SFS)","Sialkot, Pakistan (SKT)","Turbat, Pakistan (TUK)","Nawabshah, Pakistan (WNS)","Gilgit, Pakistan (GIL)","Hyderabad, Pakistan (HDD)","Peshawar, Pakistan (PEW)","Islamabad, Pakistan (ISB)","Karachi, Pakistan (KHI)","Mohenjodaro, Pakistan (MJD)","Lahore, Pakistan (LHE)","Lodz, Polonia (LCJ)","Lublinek, Polonia (LLK)","Katowice, Polonia (KTW)","Krakow, Polonia (KRK)","Zielona, Polonia (IEG)","Poznan, Polonia (POZ)","Olsztyn, Polonia (QYO)","Gdansk, Polonia (GDN)","Bydgoszcz, Polonia (BZG)","Wroclaw, Polonia (WRO)","Varsovia, Polonia (WAW)","Szczecin, Polonia (SZZ)","Rzeszow, Polonia (RZE)","Gaza, Palestina (GZA)","Horta, Islas Azores, Portugal (HOR)","Santa Cruz Flores, Portugal (FLW)","Funchal Isla Madeira, Portugal (FNC)","Graciosa Island, Portugal (GRW)","Faro, Portugal (FAO)","Corvo Island, Portugal (CVU)","Coimbra, Portugal (CBP)","Portimao, Portugal (PRM)","Porto Santo, Isla Madera, Portugal (PXO)","Pointe Delgado, Azores, Portugal (PDL)","Pico Island, Portugal (PIX)","Oporto, Portugal (OPO)","Lisboa, Portugal (LIS)","Sao Jorge Island, Portugal (SJZ)","Santa Maria, Portugal (SMA)","Terceira, Islas Azores, Portugal (TER)","Vila Real, Portugal (VRL)","Lamego, Portugal (NEJ)","Viseu, Portugal (VSE)","Obidos, Portugal (OB1)","Sintra, Portugal (SI1)","Evora, Portugal (EVO)","Albufeira, Portugal (AFE)","Cascais, Portugal (CSC)","Carvoeiro, Portugal (CVR)","Ã“bidos, Portugal (OB5)","Koror, Palau (ROR)","Vallemi, Paraguay (VMI)","Fuerte Olimpo, Paraguay (OLK)","Puerto La Victoria, Paraguay (PCJ)","AsunciÃ³n, Paraguay (ASU)","Ciudad Del Este, Paraguay (AGT)","Doha, Qatar (DOH)","St Pierre Dela Reunion, Reunion (ZSE)","St. Denis, Isla Reunion, Reunion (RUN)","Sibiu, Rumania (SBZ)","Suceava, Rumania (SCV)","Tirgu Mures, Rumania (TGM)","Tulcea, Rumania (TCE)","Timisoara, Rumania (TSR)","Brasov, Rumania (XHV)","Arad, Rumania (ARW)","Baia Mare, Rumania (BAY)","Constanta, Rumania (CND)","Bucarest, Rumania (BUH)","Oradea, Rumania (OMR)","Iasi, Rumania (IAS)","Yaroslavl, Rusia (IAR)","Irkutsk, Rusia (IKT)","Ivanovo, Rusia (IWA)","Krasnodar, Rusia (KRR)","Samara, Rusia (KUF)","Kazan, Rusia (KZN)","Khabarovsk, Rusia (KHV)","Krasnoyarsk, Rusia (KJA)","Kostroma, Rusia (KMW)","Kaliningrad, Rusia (KGD)","St Petersburg, Rusia (LED)","Omsk, Rusia (OMS)","Novosibirsk, Rusia (OVB)","Nizhnevartovsk, Rusia (NJC)","Murmansk, Rusia (MMK)","Moscu, Rusia (MOW)","Mineralnye Vody, Rusia (MRV)","Naberevnye Chelny, Rusia (NBC)","Perm, Rusia (PEE)","Port Elizabeth, Rusia (PEZ)","Pskov, Rusia (PKV)","Blagoveshchensk, Rusia (BQS)","Chelyabinsk, Rusia (CEK)","Astrakhan, Rusia (ASF)","Arkhangelsk, Rusia (ARH)","Adler Sochi, Rusia (AER)","Anapa, Rusia (AAQ)","Elista, Rusia (ESL)","Belgorod, Rusia (EGO)","Nizhniy Novgorod, Rusia (GOJ)","Magadan, Rusia (GDX)","Chita, Rusia (HTA)","Vologda, Rusia (VGD)","Volgograd, Rusia (VOG)","Vladivostok, Rusia (VVO)","Ulan Ude, Rusia (UUD)","Yuzhno Sakhalinsk, Rusia (UUS)","Kursk, Rusia (URS)","Tyumen, Rusia (TJM)","Tobolsk, Rusia (TOX)","Tomsk, Rusia (TOF)","Syktyvkar, Rusia (SCW)","Saratov, Rusia (RTW)","Rostov, Rusia (ROV)","Stavropol, Rusia (STW)","Ekaterinburg, Rusia (SVX)","Saransk, Rusia (SKX)","Kigali, Rwanda (KGL)","Khamis Mushait, Saudi Arabia (KMX)","King Khalid Military City, Saudi Arabia (KMC)","Jeddah, Saudi Arabia (JED)","Madinah, Saudi Arabia (MED)","Hafr Albatin, Saudi Arabia (HBT)","Hail, Saudi Arabia (HAS)","Gizan, Saudi Arabia (GIZ)","Gassim, Saudi Arabia (ELQ)","Nejran, Saudi Arabia (EAM)","Dammam, Saudi Arabia (DMM)","Dhahran, Saudi Arabia (DHA)","Al Baha, Saudi Arabia (ABT)","Abha, Saudi Arabia (AHB)","Qaisumah, Saudi Arabia (AQI)","Sharurah, Saudi Arabia (SHW)","Riyadh, Saudi Arabia (RUH)","Taif, Saudi Arabia (TIF)","Turaif, Saudi Arabia (TUI)","Tabuk, Saudi Arabia (TUU)","Yanbo, Saudi Arabia (YNB)","Honiara, Solomon Islands (HIR)","Praslin Island, Seychelles Islands (PRI)","Mahe Island, Seychelles Islands (SEZ)","Khartoum, Sudan (KRT)","Karlstad, Suecia (KSD)","Kiruna, Suecia (KRN)","Kramfors, Suecia (KRF)","Skovde, Suecia (KVB)","Kalmar, Suecia (KLR)","Kristianstad, Suecia (KID)","Sodertalje, Suecia (JSO)","Helsingborg, Suecia (JHE)","Jonkoping, Suecia (JKG)","Landskrona, Suecia (JLD)","Lycksele, Suecia (LYC)","Lidkoping, Suecia (LDK)","Lulea, Suecia (LLA)","Linkoping, Suecia (LPI)","Mora, Suecia (MXX)","Malmo, Suecia (MMA)","Norrkoping, Suecia (NRK)","Oskarshamn, Suecia (OSK)","Ostersund, Suecia (OSD)","Orebro Bofors, Suecia (ORB)","Ornskoldsvik, Suecia (OER)","Hultsfred, Suecia (HLF)","Hudiksvall, Suecia (HUV)","Halmstad, Suecia (HAD)","Gavle, Suecia (GVX)","Gotemburgo, Suecia (GOT)","Gallivare, Suecia (GEV)","Eskilstuna, Suecia (EKT)","Sveg, Suecia (EVG)","Arvidsjaur, Suecia (AJR)","Angelholm, Suecia (AGH)","Borlange, Suecia (BLE)","Skelleftea, Suecia (SFT)","Sundsvall, Suecia (SDL)","Ronneby, Suecia (RNB)","Soderhamn, Suecia (SOO)","Estocolmo, Suecia (STO)","Storuman, Suecia (SQO)","Uppsala, Suecia (UPS)","Umea, Suecia (UME)","Visby, Suecia (VBY)","Trollhattan, Suecia (THN)","Vaxjo, Suecia (VXO)","Vasteras, Suecia (VST)","Vilhelmina, Suecia (VHM)","Lund C, Suecia (XGC)","Singapur, Singapur (SIN)","Portoroz, Slovenia (POW)","Ljubljana, Slovenia (LJU)","Maribor, Slovenia (MBX)","Kosice, Slovakia (KSC)","Piestany, Slovakia (PZY)","Bratislava, Slovakia (BTS)","Tatry Poprad, Slovakia (TAT)","Gbangbatok, Sierra Leone (GBK)","Freetown, Sierra Leone (FNA)","Dakar, Senegal (DKR)","Paramaribo, Suriname (PBM)","San Salvador, El Salvador (SAL)","Golfo de Fonseca La Union, El Salvador (S39)","Playas de Oriente, El Salvador (EV_001)","Playas de La Libertad, El Salvador (EV_002)","Ruta de Las Flores, El Salvador (EV_003)","Suchitoto, El Salvador (EV_004)","Latakia, Siria (LTK)","Damasco, Siria (DAM)","Aleppo, Siria (ALP)","Manzini, Swazilandia (MTS)","North Caicos, Turks y Caicos (NCA)","Providenciales, Turks y Caicos (PLS)","Grand Turk, Turks y Caicos (GDT)","Parrot Cay, Turks y Caicos (PIC)","South Caicos, Turks y Caicos (XSC)","NÂ´Djamena, Chad (NDJ)","Lome, Togo (LFW)","Lampang, Tailandia (LPT)","Mae Sot, Tailandia (MAQ)","Nakhon Phanom, Tailandia (KOP)","Krabi, Tailandia (KBV)","Khon Kaen, Tailandia (KKC)","Narathiwat, Tailandia (NAW)","Nakhon Ratchasima, Tailandia (NAK)","Nan Th, Tailandia (NNT)","Nakhon Si Tham, Tailandia (NST)","Phitsanulok, Tailandia (PHS)","Phi Phi, Tailandia (PHZ)","Pattaya, Tailandia (PYX)","Phrae, Tailandia (PRH)","Hat Yai, Tailandia (HDY)","Hua Hin, Tailandia (HHQ)","Mae Hongson, Tailandia (HGN)","Phuket, Tailandia (HKT)","Buri Ram, Tailandia (BFV)","Bangkok, Tailandia (BKK)","Chiang Rai, Tailandia (CEI)","Chiang Mai, Tailandia (CNX)","Chumphon, Tailandia (CJM)","Sakon Nakhon, Tailandia (SNO)","Trat, Tailandia (TDX)","Sukhothai, Tailandia (THS)","U Tapao, Tailandia (UTP)","Udon Thani, Tailandia (UTH)","Koh Samui, Tailandia (USM)","Surat Thani, Tailandia (URT)","Ranong, Tailandia (UNN)","Ubon Ratchath, Tailandia (UBP)","Trang, Tailandia (TST)","Dushanbe, Tajikistan (DYU)","Turkmenabad, Turkmenistan (CRZ)","Ashgabat, Turkmenistan (ASB)","Mary, Turkmenistan (MYP)","Turkmanbashi, Turkmenistan (KRW)","Dashoguz, Turkmenistan (TAZ)","Tabarka, Tunisia (TBJ)","Tozeur, Tunisia (TOE)","Tunez, Tunisia (TUN)","Sfax, Tunisia (SFA)","Monastir, Tunisia (MIR)","Djerba, Tunisia (DJE)","Ha Apai, Tonga (HPA)","Vava U, Tonga (VAV)","Tongatapu, Tonga (TBU)","Dili, East Timor (DIL)","Baucau, East Timor (BCH)","Antalya, TurquÃ­a (AYT)","Kayseri, TurquÃ­a (ASR)","Ankara, TurquÃ­a (ANK)","Adana, TurquÃ­a (ADA)","AntioquÃ­a, TurquÃ­a (4AN )","Capadocia, TurquÃ­a (4CD)","Kusadasi, TurquÃ­a (4KS)","Kahta, TurquÃ­a (4KT)","Mardin, TurquÃ­a (4MN)","Pamukkale, TurquÃ­a (4PK)","Canakkale, TurquÃ­a (CKZ)","Bursa, TurquÃ­a (BTZ)","Bodrum, TurquÃ­a (BXN)","Balikesir, TurquÃ­a (BZI)","Dalaman, TurquÃ­a (DLM)","Denizli, TurquÃ­a (DNZ)","Diyarbai, TurquÃ­a (DIY)","Erzurum, TurquÃ­a (ERZ)","Elazig, TurquÃ­a (EZS)","Gaziantep, TurquÃ­a (GZT)","Sanliurfa Guney, TurquÃ­a (GNY)","Konya, TurquÃ­a (KYA)","Esmirna, TurquÃ­a (IZM)","Estambul, TurquÃ­a (IST)","Nevsehir, TurquÃ­a (NAV)","Mus Turkey, TurquÃ­a (MSR)","Trabzon, TurquÃ­a (TZX)","Sabiha Gokcen, TurquÃ­a (SAW)","Samsun, TurquÃ­a (SSX)","Fethiye-Oludeniz, TurquÃ­a (FE1)","Cunupia, Trinidad & Tobago (TB1)","San Fernando, Trinidad & Tobago (TB2)","Tobago, Trinidad & Tobago (TAB)","Puerto EspaÃ±a, Isla Madeira, Trinidad & Tobago (POS)","Funafuti, Tuvalu (FUN)","Hengchun, Taiwan (HCN)","Hualien, Taiwan (HUN)","Hsinchun, Taiwan (HSZ)","Makung, Taiwan (MZG)","Kinmen, Taiwan (KNH)","Kaohsiung, Taiwan (KHH)","Matsu, Taiwan (MFK)","Taipei, Taiwan (TPE)","Tainan, Taiwan (TNN)","Taichung, Taiwan (TXG)","Taitung, Taiwan (TTT)","Tanga, Tanzania (TGT)","Zanzibar, Tanzania (ZNZ)","Kilimanjaro, Tanzania (JRO)","Mtwara, Tanzania (MYW)","Mwanza, Tanzania (MWZ)","Dar Es Salaam, Tanzania (DAR)","Arusha, Tanzania (ARK)","Dnepropetrovsk, Ucrania (DNK)","Donetsk, Ucrania (DOK)","Kharkov, Ucrania (HRK)","Odesa, Ucrania (ODS)","Zaporozhe, Ucrania (OZH)","Kiev, Ucrania (IEV)","Ivano Frankovsk, Ucrania (IFO)","Lvov, Ucrania (LWO)","Vinnitsa, Ucrania (VIN)","Lugansk, Ucrania (VSG)","Lutsk, Ucrania (UCK)","Sumy, Ucrania (UMY)","Simferopol, Ucrania (SIP)","Entebbe Kampala, Uganda (EBB)","Midway Island, Islas de EEUU (MDY)","Chicago Midway Apt, Estados Unidos (MDW)","Meridian, Estados Unidos (MEI)","Carbondale, Estados Unidos (MDH)","Merced, Estados Unidos (MCE)","McCook, Estados Unidos (MCK)","Manistee, Estados Unidos (MBL)","Saginaw, Estados Unidos (MBS)","Mason City, Estados Unidos (MCW)","Macon, Estados Unidos (MCN)","Orlando, Estados Unidos (ORL)","Lewiston, Estados Unidos (LWS)","Lewistown, Estados Unidos (LWT)","Livermore, Estados Unidos (LVK)","Livingston, Estados Unidos (LVM)","Lexington, Estados Unidos (LXN)","Lynchburg, Estados Unidos (LYH)","Ely Mn, Estados Unidos (LYU)","Mayaguez, Estados Unidos (MAZ)","Madera, Estados Unidos (MAE)","Midland, Estados Unidos (MAF)","Medford, Estados Unidos (MFR)","Michigan City, Estados Unidos (MGC)","Marietta, Estados Unidos (MGE)","Mansfield, Estados Unidos (MFD)","McAllen, Estados Unidos (MFE)","Mesquite, Estados Unidos (MFH)","Marshfield, Estados Unidos (MFI)","Minden, Estados Unidos (MEV)","Memphis, Estados Unidos (MEM)","Mitchell, Estados Unidos (MHE)","Morgantown, Estados Unidos (MGW)","Montgomery, Estados Unidos (MGM)","Moultrie, Estados Unidos (MGR)","Miami, Estados Unidos (MIA)","Mojave, Estados Unidos (MHV)","Manchester, Estados Unidos (MHT)","Manhattan, Estados Unidos (MHK)","Millville, Estados Unidos (MIV)","Marshalltown, Estados Unidos (MIW)","Muncie, Estados Unidos (MIE)","Muskogee, Estados Unidos (MKO)","Mankato, Estados Unidos (MKT)","Kansas, Estados Unidos (MKC)","Milwaukee, Estados Unidos (MKE)","Muskegon, Estados Unidos (MKG)","Hoolehua, Estados Unidos (MKK)","Jackson, Estados Unidos (MKL)","Melbourne, Estados Unidos (MLB)","McAlester, Estados Unidos (MLC)","Moline, Estados Unidos (MLI)","Lopez Island, Estados Unidos (LPS)","La Porte, Estados Unidos (LPO)","Lanai, Estados Unidos (LNY)","Lancaster, Estados Unidos (LNS)","London, Estados Unidos (LOZ)","Mount Holly, Estados Unidos (LLY)","Klamath Falls, Estados Unidos (LMT)","Lincoln, Estados Unidos (LNK)","Altus, Estados Unidos (LTS)","Laurel, Estados Unidos (LUL)","Kalaupapa, Estados Unidos (LUP)","Greenbrier, Estados Unidos (LWB)","Lawrence, Estados Unidos (LWC)","Lawrence, Estados Unidos (LWM)","La Crosse, Estados Unidos (LSE)","Las Cruces, Estados Unidos (LRU)","Laredo, Estados Unidos (LRD)","Long Beach, Estados Unidos (LGB)","Lafayette, Estados Unidos (LFT)","Lufkin, Estados Unidos (LFK)","Lewiston, Estados Unidos (LEW)","Lexington, Estados Unidos (LEX)","Little Rock, Estados Unidos (LIT)","Lake Jackson, Estados Unidos (LJN)","Logan, Estados Unidos (LGU)","Lihue, Estados Unidos (LIH)","Lebanon, Estados Unidos (LEB)","Leesburg, Estados Unidos (LEE)","Lake Charles, Estados Unidos (LCH)","Laconia, Estados Unidos (LCI)","Linden, Estados Unidos (LDJ)","Lubbock, Estados Unidos (LBB)","Latrobe, Estados Unidos (LBE)","North Platte, Estados Unidos (LBF)","Liberal, Estados Unidos (LBL)","Lumberton, Estados Unidos (LBT)","Bullhead City, Estados Unidos (IFP)","Kingman, Estados Unidos (IGM)","Killeen, Estados Unidos (ILE)","Wilmington, Estados Unidos (ILG)","Wilmington, Estados Unidos (ILM)","Jacksonville, Estados Unidos (IJX)","Kankakee, Estados Unidos (IKK)","New Haven, Estados Unidos (HVN)","Havre, Estados Unidos (HVR)","Hayward, Estados Unidos (HWD)","Hyannis, Estados Unidos (HYA)","Wichita, Estados Unidos (ICT)","Idaho Falls, Estados Unidos (IDA)","Indiana, Estados Unidos (IDI)","Hayward, Estados Unidos (HYR)","Hays, Estados Unidos (HYS)","Niagara Falls, Estados Unidos (IAG)","Houston, Estados Unidos (HOU)","Iron Mountain, Estados Unidos (IMT)","Indianappolis, Estados Unidos (IND)","International Falls, Estados Unidos (INL)","Winston-Salem, Estados Unidos (INT)","Winslow, Estados Unidos (INW)","Williamsport, Estados Unidos (IPT)","El Centro, Estados Unidos (IPL)","Kissimmee, Estados Unidos (ISM)","Williston, Estados Unidos (ISN)","Kinston, Estados Unidos (ISO)","Islip, Estados Unidos (ISP)","Manistique, Estados Unidos (ISQ)","Wiscasset, Estados Unidos (ISS)","Pasadena, Estados Unidos (JPD)","Juneau, Estados Unidos (JNU)","Joplin, Estados Unidos (JLN)","Sausalito, Estados Unidos (JMC)","Jamestown, Estados Unidos (JMS)","Jacksonville, Estados Unidos (JKV)","Jefferson City, Estados Unidos (JEF)","Jamestown, Estados Unidos (JHW)","Kapalua, Estados Unidos (JHM)","Kirksville, Estados Unidos (IRK)","Sturgis, Estados Unidos (IRS)","Ironwood, Estados Unidos (IWD)","Ithaca, Estados Unidos (ITH)","Hilo, Estados Unidos (ITO)","Jackson, Estados Unidos (JAN)","Jacksonville, Estados Unidos (JAX)","Jonesboro, Estados Unidos (JBR)","Pleasanton, Estados Unidos (JBS)","Jackson, Estados Unidos (JAC)","Inyokern, Estados Unidos (IYK)","Kansas City, Estados Unidos (KCK)","Johnstown, Estados Unidos (JST)","Beloit, Estados Unidos (JVL)","Kake, Estados Unidos (KAE)","Jackson, Estados Unidos (JXN)","Kennett, Estados Unidos (KNT)","Klawock, Estados Unidos (KLW)","Kentland, Estados Unidos (KKT)","Kalskag, Estados Unidos (KLG)","Key Largo, Estados Unidos (KYL)","Lamar, Estados Unidos (LAA)","Lafayette, Estados Unidos (LAF)","Laramie, Estados Unidos (LAR)","Las Vegas, Estados Unidos (LAS)","Lawton, Estados Unidos (LAW)","Los Angeles, Estados Unidos (LAX)","Lakeland, Estados Unidos (LAL)","Los Alamos, Estados Unidos (LAM)","Lansing, Estados Unidos (LAN)","Ketchikan, Estados Unidos (KTN)","Brevig Mission, Estados Unidos (KTS)","Hawaiian Big Island, Estados Unidos (KOA)","King Of Prussia, Estados Unidos (KPD)","Everett, Estados Unidos (PAE)","Paducah, Estados Unidos (PAH)","Oxnard, Estados Unidos (OXR)","Owatonna, Estados Unidos (OWA)","Owensboro, Estados Unidos (OWB)","Norwood, Estados Unidos (OWD)","Oshkosh, Estados Unidos (OSH)","Norwalk, Estados Unidos (ORQ)","Worthington, Estados Unidos (OTG)","North Bend, Estados Unidos (OTH)","Ottumwa, Estados Unidos (OTM)","Ancortes, Estados Unidos (OTS)","Kotzebue, Estados Unidos (OTZ)","Norfolk Nebraska, Estados Unidos (OFK)","Ogallala, Estados Unidos (OGA)","Ogden, Estados Unidos (OGD)","Maui, Estados Unidos (OGG)","Ogdensburg, Estados Unidos (OGS)","Oklahoma City, Estados Unidos (OKC)","Kokomo, Estados Unidos (OKK)","Nogales, Estados Unidos (OLS)","Columbus, Estados Unidos (OLU)","Omaha, Estados Unidos (OMA)","Nome, Estados Unidos (OME)","Chicago, Estados Unidos (ORD)","Norfolk Virginia Newport News, Estados Unidos (ORF)","Worcester, Estados Unidos (ORH)","Oneonta, Estados Unidos (ONH)","Olympia, Estados Unidos (OLM)","Newport, Estados Unidos (ONP)","Ontario, Estados Unidos (ONT)","Oak Harbor, Estados Unidos (ODW)","Vincennes, Estados Unidos (OEA)","Okeechobee, Estados Unidos (OBE)","Oceanside, Estados Unidos (OCN)","Jacksonville, Estados Unidos (OAJ)","Oakland, Estados Unidos (OAK)","Ocean Reef, Estados Unidos (OCA)","Ocean City, Estados Unidos (OCE)","Ocala, Estados Unidos (OCF)","Nulato, Estados Unidos (NUL)","Mountain View, Estados Unidos (NUQ)","Nueva York, Estados Unidos (NYC)","Brunswick, Estados Unidos (NHZ)","Milton, Estados Unidos (NSE)","Newport, Estados Unidos (NPT)","Kingsville, Estados Unidos (NQI)","Mt Vernon, Estados Unidos (MVN)","Marion, Estados Unidos (MWA)","Moses Lake, Estados Unidos (MWH)","Middletown, Estados Unidos (MWO)","Mccall, Estados Unidos (MYL)","Manitowoc, Estados Unidos (MTW)","Kamuela, Estados Unidos (MUE)","Monroeville, Estados Unidos (MVC)","Martha S Vineyard, Estados Unidos (MVY)","Myrtle Beach, Estados Unidos (MYR)","Marysville, Estados Unidos (MYV)","Fallon, Estados Unidos (NFL)","Massena, Estados Unidos (MSS)","Mesa, Estados Unidos (MSC)","Muscle Shoals, Estados Unidos (MSL)","Madison, Estados Unidos (MSN)","Missoula, Estados Unidos (MSO)","Minneapolis, Estados Unidos (MSP)","Mattoon, Estados Unidos (MTO)","Montauk, Estados Unidos (MTP)","Monticello, Estados Unidos (MSV)","New Orleans, Estados Unidos (MSY)","Marathon, Estados Unidos (MTH)","Montrose, Estados Unidos (MTJ)","Monterey, Estados Unidos (MRY)","Marquette, Estados Unidos (MQT)","Smyrna, Estados Unidos (MQY)","Martinsburgh, Estados Unidos (MRB)","Columbia, Estados Unidos (MRC)","Miles City, Estados Unidos (MLS)","Monroe, Estados Unidos (MLU)","Mammoth Lakes, Estados Unidos (MMH)","Marshall, Estados Unidos (MML)","Morristown, Estados Unidos (MMU)","Menominee, Estados Unidos (MNM)","Minot, Estados Unidos (MOT)","Mountain Village, Estados Unidos (MOU)","McPherson, Estados Unidos (MPR)","Montpelier, Estados Unidos (MPV)","Mount Pleasant, Estados Unidos (MOP)","Modesto, Estados Unidos (MOD)","Mobile, Estados Unidos (MOB)","Poughkeepsie, Estados Unidos (POU)","Sherman, Estados Unidos (PNX)","Pensacola, Estados Unidos (PNS)","La Verne, Estados Unidos (POC)","Fort Polk, Estados Unidos (POE)","Poplar Bluff, Estados Unidos (POF)","Pompano Beach, Estados Unidos (PPM)","Presque Isle, Estados Unidos (PQI)","Paso Robles, Estados Unidos (PRB)","Prescott, Estados Unidos (PRC)","Plymouth, Estados Unidos (PLY)","Pellston, Estados Unidos (PLN)","Branson Point Lookout, Estados Unidos (PLK)","Plattsburgh, Estados Unidos (PLB)","Palmdale, Estados Unidos (PMD)","Ponca City, Estados Unidos (PNC)","Peoria, Estados Unidos (PIA)","St. Petersburgo, Estados Unidos (PIE)","Pocatello, Estados Unidos (PIH)","Pierre, Estados Unidos (PIR)","Paris, Estados Unidos (PHT)","Phoenix, Estados Unidos (PHX)","Payson, Estados Unidos (PJB)","Parkersburg, Estados Unidos (PKB)","Pittsburgo, Estados Unidos (PIT)","Newport News, Estados Unidos (PHF)","Filadelfia, Estados Unidos (PHL)","Port Huron, Estados Unidos (PHN)","Pascagoula, Estados Unidos (PGL)","Puerto Progreso, Estados Unidos (PGO)","Greenville, Estados Unidos (PGV)","Page, Estados Unidos (PGA)","Punta Gorda, Estados Unidos (PGD)","Pine Bluff, Estados Unidos (PBF)","West Palm Beach, Estados Unidos (PBI)","Palo Alto, Estados Unidos (PAO)","Pendleton, Estados Unidos (PDT)","Portland, Estados Unidos (PDX)","Paris, Estados Unidos (PRX)","Philipsburg, Estados Unidos (PSB)","Pasco, Estados Unidos (PSC)","Portsmouth, Estados Unidos (PSM)","Pittsfield, Estados Unidos (PSF)","Petersburg, Estados Unidos (PSG)","Dublin, Estados Unidos (PSK)","Pueblo, Estados Unidos (PUB)","Price, Estados Unidos (PUC)","Pottstown, Estados Unidos (PTW)","Pittsburg, Estados Unidos (PTS)","Pontiac, Estados Unidos (PTK)","Palm Springs, Estados Unidos (PSP)","Petersburg, Estados Unidos (PTB)","Provincetown, Estados Unidos (PVC)","Providence, Estados Unidos (PVD)","Copper Mountain, Estados Unidos (QCE)","Provo, Estados Unidos (PVU)","Plainview, Estados Unidos (PVW)","Painesville, Estados Unidos (PVZ)","Portland, Estados Unidos (PWM)","Bremerton, Estados Unidos (PWT)","Pullman, Estados Unidos (PUW)","Racine, Estados Unidos (RAC)","Riverside, Estados Unidos (RAL)","Big Bear City, Estados Unidos (RBF)","Roseburg, Estados Unidos (RBG)","Rapid City, Estados Unidos (RAP)","Breckenridge, Estados Unidos (QKB)","Keystone, Estados Unidos (QKS)","Ranger, Estados Unidos (RGR)","Rockford, Estados Unidos (RFD)","Rhinelander, Estados Unidos (RHI)","Rifle, Estados Unidos (RIL)","Richmond, Estados Unidos (RIC)","Richfield, Estados Unidos (RIF)","Redding, Estados Unidos (RDD)","Reading, Estados Unidos (RDG)","Redmond Bend, Estados Unidos (RDM)","Raleigh, Estados Unidos (RDU)","Roche Harbor, Estados Unidos (RCE)","Walterboro, Estados Unidos (RBW)","Elizabeth City, Estados Unidos (ECG)","Eagle, Estados Unidos (EAA)","Kearney, Estados Unidos (EAR)","Wenatchee, Estados Unidos (EAT)","Eau Claire, Estados Unidos (EAU)","Douglas, Estados Unidos (DUG)","Dubois, Estados Unidos (DUJ)","Detroit, Estados Unidos (DTW)","Dutch Harbor, Estados Unidos (DUT)","Devils Lake, Estados Unidos (DVL)","Davenport, Estados Unidos (DVN)","Doylestown, Estados Unidos (DYL)","Danbury, Estados Unidos (DXR)","Elizabethtown, Estados Unidos (EKX)","El Dorado, Estados Unidos (ELD)","Elkhart, Estados Unidos (EKI)","Elkins, Estados Unidos (EKN)","Elko, Estados Unidos (EKO)","Needles, Estados Unidos (EED)","Keene, Estados Unidos (EEN)","Eagle River, Estados Unidos (EGV)","East Hartford, Estados Unidos (EHT)","Eagle, Estados Unidos (EGE)","Ely Nv, Estados Unidos (ELY)","Elk City, Estados Unidos (ELK)","Elmira, Estados Unidos (ELM)","El Paso, Estados Unidos (ELP)","Kenosha, Estados Unidos (ENW)","Keokuk, Estados Unidos (EOK)","Centralia, Estados Unidos (ENL)","El Monte, Estados Unidos (EMT)","Kenai, Estados Unidos (ENA)","Key West, Estados Unidos (EYW)","Fargo, Estados Unidos (FAR)","Fresno, Estados Unidos (FAT)","Fayetteville, Estados Unidos (FAY)","Forrest City, Estados Unidos (FCY)","Frederick, Estados Unidos (FDK)","Fairbanks, Estados Unidos (FAI)","Fajardo, Estados Unidos (FAJ)","Kalispell, Estados Unidos (FCA)","Franklin, Estados Unidos (FKL)","Fergus Falls, Estados Unidos (FFM)","Ft Huachuca, Estados Unidos (FHU)","Florence, Estados Unidos (FLO)","Flagstaff, Estados Unidos (FLG)","Fort Lauderdale Florida, Estados Unidos (FLL)","Escanaba, Estados Unidos (ESC)","East Sound, Estados Unidos (ESD)","Erie, Estados Unidos (ERI)","Easton, Estados Unidos (ESN)","East Stroudsburg, Estados Unidos (ESP)","West Bend, Estados Unidos (ETB)","Eveleth, Estados Unidos (EVM)","Enterprise, Estados Unidos (ETS)","Eufaula, Estados Unidos (EUF)","Eugene, Estados Unidos (EUG)","Evansville, Estados Unidos (EVV)","New Bedford, Estados Unidos (EWB)","Newton, Estados Unidos (EWK)","New Bern, Estados Unidos (EWN)","Excursion Inlet, Estados Unidos (EXI)","Danville, Estados Unidos (DNV)","Sedalia, Estados Unidos (DMO)","The Dalles, Estados Unidos (DLS)","Dillon, Estados Unidos (DLL)","Dillingham, Estados Unidos (DLG)","Duluth, Estados Unidos (DLH)","Dover, Estados Unidos (DOV)","Destin, Estados Unidos (DSI)","Des Moines, Estados Unidos (DSM)","Del Rio, Estados Unidos (DRT)","Durango, Estados Unidos (DRO)","Dubuque, Estados Unidos (DBQ)","Danville, Estados Unidos (DAN)","Dayton, Estados Unidos (DAY)","Dublin, Estados Unidos (DBN)","Denver, Estados Unidos (DEN)","Defiance, Estados Unidos (DFI)","Dallas, Estados Unidos (DFW)","Decatur, Estados Unidos (DCU)","Dodge City, Estados Unidos (DDC)","Dothan, Estados Unidos (DHN)","Dickinson, Estados Unidos (DIK)","Decatur, Estados Unidos (DEC)","Decorah, Estados Unidos (DEH)","Cape Town, Estados Unidos (CTW)","Columbus, Estados Unidos (CUS)","Charleston, Estados Unidos (CRW)","Corinth, Estados Unidos (CRX)","Columbus, Estados Unidos (CSG)","San Luis Obispo, Estados Unidos (CSL)","Clinton, Estados Unidos (CSM)","Carson City, Estados Unidos (CSN)","Casper, Estados Unidos (CPR)","Corpus Christi, Estados Unidos (CRP)","Clinton, Estados Unidos (CWI)","Calexico, Estados Unidos (CXL)","Conroe, Estados Unidos (CXO)","Cincinati, Estados Unidos (CVG)","Clovis, Estados Unidos (CVN)","Corvallis, Estados Unidos (CVO)","Daytona Beach, Estados Unidos (DAB)","Cheyenne, Estados Unidos (CYS)","Harlingen, Estados Unidos (HRL)","Harrison, Estados Unidos (HRO)","Hastings, Estados Unidos (HSI)","Hot Springs, Estados Unidos (HSP)","Homestead, Estados Unidos (HST)","Huntsville, Estados Unidos (HSV)","Batesville, Estados Unidos (HLB)","White Plains, Estados Unidos (HPN)","Princeville, Estados Unidos (HPV)","East Hampton, Estados Unidos (HTO)","Huntington, Estados Unidos (HTS)","Huntsville, Estados Unidos (HTV)","Humacao, Estados Unidos (HUC)","Terre Haute, Estados Unidos (HUF)","Hutchinson, Estados Unidos (HUT)","Houma, Estados Unidos (HUM)","Hickory, Estados Unidos (HKY)","Holland, Estados Unidos (HLM)","Helena, Estados Unidos (HLN)","Hillsboro, Estados Unidos (HIO)","Blytheville, Estados Unidos (HKA)","Healy Lake, Estados Unidos (HKB)","Hot Springs, Estados Unidos (HOT)","Hobbs, Estados Unidos (HOB)","Homer, Estados Unidos (HOM)","Huron, Estados Unidos (HON)","Hopkinsville, Estados Unidos (HOP)","Huntingburg, Estados Unidos (HNB)","Hoonah, Estados Unidos (HNH)","Honolulu, Estados Unidos (HNL)","Hana, Estados Unidos (HNM)","Haines, Estados Unidos (HNS)","Hattiesburg, Estados Unidos (HBG)","Hayden, Estados Unidos (HDN)","Natchez, Estados Unidos (HEZ)","Hagerstown, Estados Unidos (HGR)","Hartford, Estados Unidos (HFD)","Hawthorne, Estados Unidos (HHR)","Hibbing, Estados Unidos (HIB)","Lake Havasu Cty, Estados Unidos (HII)","Hilton Head, Estados Unidos (HHH)","Gary, Estados Unidos (GYY)","Goodyear, Estados Unidos (GYR)","Harrisburg, Estados Unidos (HAR)","Greenwood, Estados Unidos (GWO)","Glenwood Springs, Estados Unidos (GWS)","Gallup, Estados Unidos (GUP)","Gunnison, Estados Unidos (GUC)","Gulf Shores, Estados Unidos (GUF)","Ft Wayne, Estados Unidos (FWA)","Galena, Estados Unidos (GAL)","Great Bend, Estados Unidos (GBD)","Galesburg, Estados Unidos (GBG)","Fullerton, Estados Unidos (FUL)","Fayetteville, Estados Unidos (FYV)","Gadsden, Estados Unidos (GAD)","Gaithersburg, Estados Unidos (GAI)","Georgetown, Estados Unidos (GED)","Spokane, Estados Unidos (GEG)","Gillette, Estados Unidos (GCC)","Garden City, Estados Unidos (GCK)","Grand Canyon, Estados Unidos (GCN)","Greenville, Estados Unidos (GCY)","Fort Madison, Estados Unidos (FMS)","Fort Myers, Estados Unidos (FMY)","Falmouth, Estados Unidos (FMH)","Farmington, Estados Unidos (FMN)","Fort Pierce, Estados Unidos (FPR)","Ft Collins, Estados Unidos (FNL)","Flint, Estados Unidos (FNT)","Fort Dodge, Estados Unidos (FOD)","Front Royal, Estados Unidos (FRR)","Fairmont, Estados Unidos (FRM)","Friday Harbor, Estados Unidos (FRD)","Farmingdale, Estados Unidos (FRG)","Sioux Falls, Estados Unidos (FSD)","Ft Smith, Estados Unidos (FSM)","Fort Stockton, Estados Unidos (FST)","Ft Worth, Estados Unidos (FTW)","Gulfport, Estados Unidos (GPT)","Grand Rapids, Estados Unidos (GPZ)","Galion, Estados Unidos (GQQ)","Green Bay, Estados Unidos (GRB)","Grand Island, Estados Unidos (GRI)","New London, Estados Unidos (GON)","Gainesville, Estados Unidos (GNV)","Killen, Texas, Estados Unidos (GRK)","Grand Rapids, Estados Unidos (GRR)","Columbus, Estados Unidos (GTR)","Gettysburg, Estados Unidos (GTY)","Great Falls, Estados Unidos (GTF)","Goldsboro, Estados Unidos (GSB)","Greensboro, Estados Unidos (GSO)","Greenville, Estados Unidos (GSP)","Glacier Bay, Estados Unidos (GST)","Grand Junction, Estados Unidos (GJT)","Winter Haven, Estados Unidos (GIF)","Longview, Estados Unidos (GGG)","Glasgow, Estados Unidos (GGW)","Grand Forks, Estados Unidos (GFK)","Glens Falls, Estados Unidos (GFL)","Greenfield, Estados Unidos (GFD)","Gaylord, Estados Unidos (GLR)","Galveston, Estados Unidos (GLS)","Monument Valley, Estados Unidos (GMV)","Gatlinburg, Estados Unidos (GKT)","Goodland, Estados Unidos (GLD)","Gainesville, Estados Unidos (GLE)","Greenville, Estados Unidos (GLH)","Ann Arbor, Estados Unidos (ARB)","Alpena, Estados Unidos (APN)","Napa, Estados Unidos (APC)","Naples, Estados Unidos (APF)","Annapolis, Estados Unidos (ANP)","Aniak, Estados Unidos (ANI)","Altoona, Estados Unidos (AOO)","Lima, Estados Unidos (AOH)","Ainsworth, Estados Unidos (ANW)","Alamosa, Estados Unidos (ALS)","Alamogordo, Estados Unidos (ALM)","Waterloo, Estados Unidos (ALO)","Amarillo, Estados Unidos (AMA)","Walla Walla, Estados Unidos (ALW)","Alexander City, Estados Unidos (ALX)","Ames, Estados Unidos (AMW)","Anaheim , Estados Unidos (ANA)","Anniston, Estados Unidos (ANB)","Anchorage, Estados Unidos (ANC)","Anderson, Estados Unidos (AND)","Atlantic City, Estados Unidos (AIY)","Lake Of The Ozarks, Estados Unidos (AIZ)","King Salmon, Estados Unidos (AKN)","Albany, Estados Unidos (ALB)","Alpine, Estados Unidos (ALE)","Allentown, Estados Unidos (ABE)","Abilene, Estados Unidos (ABI)","Albuquerque, Estados Unidos (ABQ)","Aberdeen, Estados Unidos (ABR)","Nantucket, Estados Unidos (ACK)","Waco, Estados Unidos (ACT)","Eureka, Estados Unidos (ACV)","Magnolia, Estados Unidos (AGO)","Augusta, Estados Unidos (AGS)","Athens, Estados Unidos (AHN)","Alliance, Estados Unidos (AIA)","Anderson, Estados Unidos (AID)","Aiken, Estados Unidos (AIK)","Alexandria, Estados Unidos (AEX)","Albert Lea, Estados Unidos (AEL)","Adrian, Estados Unidos (ADG)","Adak Island, Estados Unidos (ADK)","Ardmore, Estados Unidos (ADM)","Kodiak, Estados Unidos (ADQ)","Ada OK, Estados Unidos (ADT)","Camp Springs, Estados Unidos (ADW)","Nashua, Estados Unidos (ASH)","Marhsall, Estados Unidos (ASL)","Aspen, Estados Unidos (ASE)","Minocqua, Estados Unidos (ARV)","Watertown, Estados Unidos (ART)","Atlanta, Estados Unidos (ATL)","Athens, Estados Unidos (ATO)","Augusta, Estados Unidos (AUG)","Auburn, Estados Unidos (AUO)","Appleton, Estados Unidos (ATW)","Watertown, Estados Unidos (ATY)","Beaver Creek , Estados Unidos (BBC)","Kalamazoo, Estados Unidos (AZO)","Waycross, Estados Unidos (AYS)","Wapakoneta, Estados Unidos (AXV)","Alexandria, Estados Unidos (AXN)","Catalina Island, Estados Unidos (AVX)","Austin, Estados Unidos (AUS)","Wausau, Estados Unidos (AUW)","Aurora, Estados Unidos (AUZ)","Scranton, Estados Unidos (AVP)","Asheville, Estados Unidos (AVL)","Broken Bow, Estados Unidos (BBW)","Blacksburg, Estados Unidos (BCB)","Bryce, Estados Unidos (BCE)","Bedford, Estados Unidos (BED)","Bridgeport, Estados Unidos (BDR)","Bethel, Estados Unidos (BET)","Benton Harbor, Estados Unidos (BEH)","Bradford, Estados Unidos (BFD)","Scottsbluff, Estados Unidos (BFF)","Bullfrog Basin, Estados Unidos (BFG)","Bakersfield, Estados Unidos (BFL)","Beaver Falls, Estados Unidos (BFP)","Bedford, Estados Unidos (BFR)","Beaufort, Estados Unidos (BFT)","Bangor, Estados Unidos (BGR)","Binghamton, Estados Unidos (BGM)","Bar Harbor, Estados Unidos (BHB)","Birmingham, Estados Unidos (BHM)","Block Island, Estados Unidos (BID)","Bishop, Estados Unidos (BIH)","Bismarck, Estados Unidos (BIS)","Bozeman, Estados Unidos (BZN)","Blakely Island, Estados Unidos (BYW)","Brazoria, Estados Unidos (BZT)","Cadillac, Estados Unidos (CAD)","Columbia, Estados Unidos (CAE)","Borrego Springs, Estados Unidos (BXS)","Burley, Estados Unidos (BYI)","Bowling Green, Estados Unidos (BWG)","Baltimore, Estados Unidos (BWI)","Batesville, Estados Unidos (BVX)","Brawley, Estados Unidos (BWC)","Brownwood, Estados Unidos (BWD)","Buffalo, Estados Unidos (BUF)","Burbank, Estados Unidos (BUR)","Bartlesville, Estados Unidos (BVO)","Burlington, Estados Unidos (BTV)","Battle Creek, Estados Unidos (BTL)","Butte, Estados Unidos (BTM)","Baton Rouge, Estados Unidos (BTR)","Blairsville, Estados Unidos (BSI)","Bisbee, Estados Unidos (BSQ)","Billings, Estados Unidos (BIL)","Broomfield, Estados Unidos (BJC)","Bemidji, Estados Unidos (BJI)","Bluefield, Estados Unidos (BLF)","Blythe, Estados Unidos (BLH)","Bellingham, Estados Unidos (BLI)","Beckley, Estados Unidos (BKW)","Brookings, Estados Unidos (BKX)","Bloomington, Estados Unidos (BMG)","Bloomington, Estados Unidos (BMI)","Nashville, Estados Unidos (BNA)","Belleville, Estados Unidos (BLV)","Brainerd, Estados Unidos (BRD)","Barrow, Estados Unidos (BRW)","Brownsville, Estados Unidos (BRO)","Burlington, Estados Unidos (BRL)","Boise, Estados Unidos (BOI)","Boston, Estados Unidos (BOS)","Beaumont, Estados Unidos (BPT)","Brunswick, Estados Unidos (BQK)","Clear Lake City, Estados Unidos (CLC)","Carlsbad, Estados Unidos (CLD)","Cleveland, Estados Unidos (CLE)","Callao, Estados Unidos (CLL)","Port Angeles, Estados Unidos (CLM)","Charlotte, Estados Unidos (CLT)","Columbus, Estados Unidos (CLU)","Clarksburg, Estados Unidos (CKB)","Clarksdale, Estados Unidos (CKM)","Clarksville, Estados Unidos (CKV)","El Cajon, Estados Unidos (CJN)","Chico, Estados Unidos (CIC)","Cedar Rapids, Estados Unidos (CID)","Craig, Estados Unidos (CIG)","Chandler, Estados Unidos (CHD)","Charlottesville, Estados Unidos (CHO)","Charleston, Estados Unidos (CHS)","Moab, Estados Unidos (CNY)","Carlsbad, Estados Unidos (CNM)","Houghton, Estados Unidos (CMX)","Sparta, Estados Unidos (CMY)","Columbus, Estados Unidos (CMH)","Champaign, Estados Unidos (CMI)","Colorado Springs, Estados Unidos (COS)","Columbia, Estados Unidos (COU)","Cody, Estados Unidos (COD)","Coeur D Alene, Estados Unidos (COE)","Cocoa Metro Area, Estados Unidos (COI)","Crescent City, Estados Unidos (CEC)","Camden, Estados Unidos (CDH)","Clemson, Estados Unidos (CEU)","Murray, Estados Unidos (CEY)","Cortez, Estados Unidos (CEZ)","Chattanooga, Estados Unidos (CHA)","Cape Girardeau, Estados Unidos (CGI)","Craig, Estados Unidos (CGA)","Colby, Estados Unidos (CBK)","Cumberland, Estados Unidos (CBE)","Council Bluffs, Estados Unidos (CBF)","Akron, Estados Unidos (CAK)","Cedar City, Estados Unidos (CDC)","Chadron, Estados Unidos (CDR)","Cordova, Estados Unidos (CDV)","Caldwell, Estados Unidos (CDW)","Concord, Estados Unidos (CCR)","Sheridan, Estados Unidos (SHR)","Shreveport, Estados Unidos (SHV)","Sitka, Estados Unidos (SIT)","San Jose, Estados Unidos (SJC)","San Angelo, Estados Unidos (SJT)","Sandusky, Estados Unidos (SKY)","Salt Lake City, Estados Unidos (SLC)","Salem, Estados Unidos (SLE)","Saranac Lake, Estados Unidos (SLK)","Salinas, Estados Unidos (SNS)","Sidney, Estados Unidos (SNY)","Pinehurst, Estados Unidos (SOP)","Somerset, Estados Unidos (SME)","Salina, Estados Unidos (SLN)","Santa Maria, Estados Unidos (SMX)","Santa Ana, Estados Unidos (SNA)","Shawnee, Estados Unidos (SNL)","Sault Ste Marie, Estados Unidos (SSM)","St Simons Is, Estados Unidos (SSI)","Sarasota, Estados Unidos (SRQ)","Santa Cruz, Estados Unidos (SRU)","Santa Ynez, Estados Unidos (SQA)","Sterling, Estados Unidos (SQI)","Spencer, Estados Unidos (SPW)","Springdale, Estados Unidos (SPZ)","Wichita Falls, Estados Unidos (SPS)","Seldovia, Estados Unidos (SOV)","Show Low, Estados Unidos (SOW)","Spearfish, Estados Unidos (SPF)","Springfield, Estados Unidos (SPI)","St Paul, Estados Unidos (STP)","Santa Rosa, Estados Unidos (STS)","St. Thomas, Islas Virgenes, Estados Unidos (STT)","St. Louis, Estados Unidos (STL)","Saint Cloud, Estados Unidos (STC)","St. Croix, Islas Virgenes, Estados Unidos (STX)","Stevens Point, Estados Unidos (STE)","Stuart, Estados Unidos (SUA)","Sumter, Estados Unidos (SUM)","Sun Valley, Estados Unidos (SUN)","Fairfield, Estados Unidos (SUU)","Seward, Estados Unidos (SWD)","Newburgh, Estados Unidos (SWF)","Statesville, Estados Unidos (SVH)","Sturgeon Bay, Estados Unidos (SUE)","Superior, Estados Unidos (SUW)","Sioux City, Estados Unidos (SUX)","Silver City, Estados Unidos (SVC)","Santa Barbara, Estados Unidos (SBA)","Sheboygan, Estados Unidos (SBM)","South Bend, Estados Unidos (SBN)","Steamboat Springs, Estados Unidos (SBS)","Prudhoe Bay Deadhorse, Estados Unidos (SCC)","State College, Estados Unidos (SCE)","Scottsdale, Estados Unidos (SCF)","Schenectady, Estados Unidos (SCH)","Salisbury Ocean City, Estados Unidos (SBY)","Stockton, Estados Unidos (SCK)","Seattle, Estados Unidos (SEA)","Sebring, Estados Unidos (SEF)","Sedona, Estados Unidos (SDX)","Sidney, Estados Unidos (SDY)","Louisville, Estados Unidos (SDF)","Sanford, Estados Unidos (SFB)","Stephenville, Estados Unidos (SEP)","San Francisco, Estados Unidos (SFO)","Springfield, Estados Unidos (SFY)","Smithfield, Estados Unidos (SFZ)","Sugar Land, Estados Unidos (SGR)","Stuttgart, Estados Unidos (SGT)","St George, Estados Unidos (SGU)","Skagway, Estados Unidos (SGY)","Staunton, Estados Unidos (SHD)","Springfield, Estados Unidos (SGF)","San Diego, Estados Unidos (SAN)","Roanoke Rapids, Estados Unidos (RZZ)","Sparta, Estados Unidos (SAR)","San Antonio, Estados Unidos (SAT)","Savannah, Estados Unidos (SAV)","Green River, Estados Unidos (RVR)","Rocky Mount, Estados Unidos (RWI)","Rawlins, Estados Unidos (RWL)","Sacramento, Estados Unidos (SMF)","Santa Fe, Estados Unidos (SAF)","Ruidoso, Estados Unidos (RUI)","Rutland, Estados Unidos (RUT)","Raton, Estados Unidos (RTN)","Ruston, Estados Unidos (RSN)","Rochester, Estados Unidos (RST)","Rome, Estados Unidos (RMG)","Riverton, Estados Unidos (RIW)","Rockland, Estados Unidos (RKD)","Rockport, Estados Unidos (RKP)","Rock Springs, Estados Unidos (RKS)","Roswell, Estados Unidos (ROW)","New Richmond, Estados Unidos (RNH)","Roanoke, Estados Unidos (ROA)","Rochester, Estados Unidos (ROC)","Reno, Estados Unidos (RNO)","Unalakleet, Estados Unidos (UNK)","University Oxford, Estados Unidos (UOX)","Waukegan, Estados Unidos (UGN)","Quakertown, Estados Unidos (UKT)","New Ulm, Estados Unidos (ULM)","Quincy, Estados Unidos (UIN)","Ukiah, Estados Unidos (UKI)","Victoria, Estados Unidos (VCT)","Victorville, Estados Unidos (VCV)","Valdez, Estados Unidos (VDZ)","Vidalia, Estados Unidos (VDI)","St Augustine, Estados Unidos (UST)","Palm Desert, Estados Unidos (UDD)","Utica, Estados Unidos (UCA)","Texarkana, Estados Unidos (TXK)","Twin Falls, Estados Unidos (TWF)","Tyler, Estados Unidos (TYR)","Knoxville, Estados Unidos (TYS)","Traverse City, Estados Unidos (TVC)","Thief River Falls, Estados Unidos (TVF)","Lake Tahoe, Estados Unidos (TVL)","Tupelo, Estados Unidos (TUP)","Tucson, Estados Unidos (TUS)","Tulsa, Estados Unidos (TUL)","Trenton, Estados Unidos (TTN)","Troutdale, Estados Unidos (TTD)","Taos, Estados Unidos (TSM)","Bristol, Estados Unidos (TRI)","Terrell, Estados Unidos (TRL)","Telluride, Estados Unidos (TEX)","Ft Leonard Wood, Estados Unidos (TBN)","Tuscaloosa, Estados Unidos (TCL)","Tanana, Estados Unidos (TAL)","Syracuse, Estados Unidos (SYR)","Stillwater, Estados Unidos (SWO)","Tifton, Estados Unidos (TMA)","Temple, Estados Unidos (TPL)","Tampa, Estados Unidos (TPA)","Toledo, Estados Unidos (TOL)","Topeka, Estados Unidos (TOP)","Torrington, Estados Unidos (TOR)","York, Estados Unidos (THV)","Thermopolis, Estados Unidos (THP)","Tacoma, Estados Unidos (TIW)","Titusville, Estados Unidos (TIX)","Tok Ak, Estados Unidos (TKJ)","Tallahassee, Estados Unidos (TLH)","Tulare, Estados Unidos (TLR)","Valparaiso / Ft Walton Beach, Estados Unidos (VPS)","Vero Beach, Estados Unidos (VRB)","Chincoteague, Estados Unidos (WAL)","Washington D. C., Estados Unidos (WAS)","Peru, Estados Unidos (VYS)","Boulder, Estados Unidos (WBU)","Enid, Estados Unidos (WDG)","Weatherford, Estados Unidos (WEA)","Visalia, Estados Unidos (VIS)","Abingdon, Estados Unidos (VJI)","Vicksburg, Estados Unidos (VKS)","Vallejo, Estados Unidos (VLO)","Valdosta, Estados Unidos (VLD)","Van Horn, Estados Unidos (VHN)","Vernal, Estados Unidos (VEL)","West Yellowstone, Estados Unidos (WYS)","Cape May, Estados Unidos (WWD)","Wrangell, Estados Unidos (WRG)","Worland, Estados Unidos (WRL)","Westerly, Estados Unidos (WST)","Watsonville, Estados Unidos (WVI)","Waterville, Estados Unidos (WVL)","Lancaster, Estados Unidos (WJF)","Winchester, Estados Unidos (WGO)","Selawik, Estados Unidos (WLK)","Winnemucca, Estados Unidos (WMC)","Mountain Home, Estados Unidos (WMH)","Winfield, Estados Unidos (WLD)","Cocoa Beach, Estados Unidos (XMR)","Yakutat, Estados Unidos (YAK)","Yakima, Estados Unidos (YKM)","Yankton, Estados Unidos (YKN)","Yuma, Estados Unidos (YUM)","Zanesville, Estados Unidos (ZZV)","Youngstown, Estados Unidos (YNG)","Ciudad de PanamÃ¡, Estados Unidos (ECP)","Cross City, Estados Unidos (CTY)","Estherville, Estados Unidos (EST)","Covington Louisiana, Estados Unidos (C33)","Schaumburg, Estados Unidos (JMH)","Bay Lake, Estados Unidos (BA9)","Hoover, Estados Unidos (BIR)","Lewisville, Estados Unidos (LE1)","Cape Canaveral, Estados Unidos (CC2)","Vail, Estados Unidos (VC3)","Park City - Utah, Estados Unidos (PC4)","Foley, Estados Unidos (NHX)","Cambridge, Estados Unidos (JHY)","San Marcos, Estados Unidos (SMT)","Boca Raton Airport, Estados Unidos (BCT)","Salto, Uruguay (STY)","Colonia, Uruguay (CYR)","Punta Del Este, Uruguay (PDP)","Montevideo, Uruguay (MVD)","Bukhara, Uzbekistan Sum (BHK)","Samarkand, Uzbekistan Sum (SKD)","Tashkent, Uzbekistan Sum (TAS)","Urgench, Uzbekistan Sum (UGC)","Union Island, San Vicente (UNI)","St Vincent, San Vicente (SVD)","San Antonio, Venezuela (SVZ)","Santo Domingo, Venezuela (STD)","Santa Barbara, Venezuela (STB)","San Felipe, Venezuela (SNF)","San Tome, Venezuela (SOM)","San Felix, Venezuela (SFX)","San Fernando, Venezuela (SFD)","San Crystobal, Venezuela (SCI)","El Vigia, Venezuela (VIG)","Valencia, Venezuela (VLN)","Valera, Venezuela (VLV)","Isla Margarita, Venezuela (IM2)","Caracas, Venezuela (CCS)","Ciudad Bolivar, Venezuela (CBL)","Cabimas, Venezuela (CBS)","Barquisimeto, Venezuela (BRM)","Barinas, Venezuela (BNS)","Barcelona, Venezuela (BLA)","Coro, Venezuela (CZE)","Maturin, Venezuela (MUN)","Maracay, Venezuela (MYC)","Merida, Venezuela (MRD)","Porlamar, Venezuela (PMV)","Puerto Ayacucho, Venezuela (PYH)","Puerto Ordaz, Venezuela (PZO)","La Fria, Venezuela (LFR)","Las Piedras, Venezuela (LSP)","Maracaibo, Venezuela (MAR)","Beef Island, Islas VÃ­rgenes (EIS)","Virgin Gorda, Islas VÃ­rgenes (VIJ)","Tortola Westend, Islas VÃ­rgenes (TOV)","Ho Chi Minh, Vietnam (SGN)","Da Nang, Vietnam (DAD)","Dalat, Vietnam (DLI)","Hanoi, Vietnam (HAN)","Hu PG, Vietnam (HUI)","Haiphong, Vietnam (HPH)","Phan Thiet, Vietnam (PHH)","Nha Trang, Vietnam (NHA)","Espiritu Santo, Vanuatu (SON)","Port Vila, Vanuatu (VLI)","Maota, Samoa del oeste (MXS)","Apia, Samoa del oeste (APW)","Aden, Yemen (ADE)","Sana, Yemen (SAH)","Dzaoudzi, Mayotte (DZA)","Belgrado, Serbia (BEG)","Pristina, Serbia (PRN)","Nis Yu, Serbia (INI)","Tivat, Serbia (TIV)","Podgorica, Serbia (TGD)","Thaba Nchu, South Africa (TCU)","Skukuza, South Africa (SZK)","Upington, South Africa (UTN)","Umtata, South Africa (UTT)","Queenstown, South Africa (UTW)","Springbok, South Africa (SBU)","Saldanha Bay, South Africa (SDB)","Welkom, South Africa (WEL)","Vryheid, South Africa (VYD)","Secunda, South Africa (ZEC)","Johannesburgo, South Africa (JNB)","Ladysmith, South Africa (LAY)","Kimberley, South Africa (KIM)","Mmabatho, South Africa (MBD)","Mannheim, South Africa (MGH)","Tzaneen, South Africa (LTA)","Malelane, South Africa (LLE)","Pretoria, South Africa (PRY)","Pietersburg, South Africa (PTG)","Pietermaritzburg, South Africa (PZB)","Richards Bay, South Africa (RCB)","Phalaborwa, South Africa (PHW)","Plettenberg Bay, South Africa (PBZ)","Port Elizabeth, South Africa (PLZ)","Newcastle, South Africa (NCS)","Nelspruit, South Africa (NLP)","Sun City, South Africa (NTY)","Oudtshoorn, South Africa (OUH)","Bloemfontein, South Africa (BFN)","Bisho, South Africa (BIY)","Alexander Bay, South Africa (ALJ)","Durban, South Africa (DUR)","East London, South Africa (ELS)","Ficksburg, South Africa (FCB)","Ciudad Del Cabo, South Africa (CPT)","Harrismith, South Africa (HRS)","Hluhluwe, South Africa (HLW)","Lanseria, South Africa (HLA)","Hoedspruit, South Africa (HDS)","George, South Africa (GRJ)","N Dola, Zambia (NLA)","Livingstone, Zambia (LVI)","Lusaka, Zambia (LUN)","Mahenye, Zimbabwe (MJW)","Kariba Dam, Zimbabwe (KAB)","Hwange, Zimbabwe (HWN)","Masvingo, Zimbabwe (MVZ)","Gweru, Zimbabwe (GWE)","Harare, Zimbabwe (HRE)","Bulawayo, Zimbabwe (BUQ)","Bumi Hills, Zimbabwe (BZH)","Cataratas Victoria, Zimbabwe (VFA)","Mutare, Zimbabwe (UTA)","San Juan, Puerto Rico (SJU)","Ceiba, Puerto Rico (NRR)","Culebra, Puerto Rico (CPX)","Vieques, Puerto Rico (VQS)","Aguadilla, Puerto Rico (BQN)","Ponce, Puerto Rico (PSE)","Posadas, Argentina (PSS)","Resistencia, Argentina (RES)","Trelew, Argentina (REL)","Rio Grande, Argentina (RGA)","Rio Gallegos, Argentina (RGL)","Puerto Madryn, Argentina (PMY)","Neuquen, Argentina (NQN)","Puerto Iguazu, Argentina (IGR)","La Rioja, Argentina (IRJ)","Jujuy, Argentina (JUJ)","Mar del Plata, Argentina (MDQ)","Mendoza, Argentina (MDZ)","San Luis, Argentina (LUQ)","Malargue, Argentina (LGS)","Bariloche, Argentina (BRC)","Buenos Aires, Argentina (BUE)","Cordoba, Argentina (COR)","San Martin de los Andes, Argentina (CPC)","Corrientes, Argentina (CNQ)","San Rafael, Argentina (AFA)","Bahia Blanca, Argentina (BHI)","Calafate, Argentina (FTE)","Formosa, Argentina (FMA)","Comodoro Rivadavia, Argentina (CRD)","Catamarca, Argentina (CTC)","Esquel, Argentina (EQS)","Plaza Grand Bourg-Palermo, Argentina (CAB)","Las LeÃ±as, Argentina (LL9)","Adrogue, Argentina (03A)","Allen, Argentina (04A)","Almafuerte, Argentina (05A)","Arguello, Argentina (07A)","Arroyito, Argentina (08A)","Avellaneda, Argentina (09A)","Balnearia, Argentina (10A)","Bell Ville, Argentina (11A)","Bolivar, Argentina (12A)","Caleta Olivia, Argentina (14A)","Canada De Gomez, Argentina (15A)","Castelar, Argentina (18A)","Chaco, Argentina (19A)","Chacras De Coria, Argentina (20A)","ChosMalal, Argentina (21A)","Cinco Saltos, Argentina (22A)","Cipolletti, Argentina (23A)","Concordia, Argentina (24A)","Coronel Pringles, Argentina (25A)","CutralCo, Argentina (26A)","ElBolson, Argentina (27A)","Eldorado, Argentina (28A)","Esperanza, Argentina (29A)","Florida, Argentina (30A)","Funes, Argentina (31A)","General Roca, Argentina (32A)","Gobernador Crespo, Argentina (33A)","Godoy Cruz, Argentina (34A)","Gral Rodriguez, Argentina (35A)","Guaymallen, Argentina (36A)","Haedo, Argentina (37A)","Huinca Renanco, Argentina (38A)","Juan Jose Castelli, Argentina (39A)","Junin De Los Andes, Argentina (40A)","La Cumbre, Argentina (41A)","La Plata, Argentina (LPG)","La Tablada, Argentina (43A)","Laboulaye, Argentina (44A)","Lanus Oeste, Argentina (45A)","Las Heras, Argentina (46A)","Lomas De Zamora, Argentina (47A)","Lujan, Argentina (48A)","Lujan De Cuyo, Argentina (49A)","Mar De Ajo, Argentina (50A)","Mendiolaza, Argentina (52A)","Moron, Argentina (53A)","Morteros, Argentina (54A)","Necochea, Argentina (55A)","NeuquÃ©n, Argentina (56A)","Olavarria, Argentina (57A)","Olivos, Argentina (58A)","Paso De Los Libres, Argentina (59A)","Pergamino, Argentina (60A)","Perico, Argentina (61A)","Plottier, Argentina (62A)","Puerto San Julian, Argentina (63A)","Quilmes, Argentina (64A)","Ramos Mejia, Argentina (65A)","Reconquista, Argentina (66A)","Rio Primero, Argentina (68A)","Rivadavia, Argentina (69A)","Roque Saenz Pena, Argentina (70A)","San Fernando Del Valle, Argentina (71A)","SanGuillermo, Argentina (72A)","San Jeronimo Norte, Argentina (73A)","San Jose De Metan, Argentina (74A)","San Nicolas, Argentina (77A)","San Salvador, Argentina (78A)","San Vicente, Argentina (80A)","SantiagoTemple, Argentina (81A)","Tandil, Argentina (83A)","Tres Arroyos, Argentina (85A)","Tunuyan, Argentina (86A)","Venado Tuerto, Argentina (87A)","Villa Adelina, Argentina (88A)","Villa Constitucion, Argentina (89A)","Villa Cura Brochero, Argentina (90A)","Villa Maria, Argentina (91A)","Yerba Buena, Argentina (92A)","Zarate, Argentina (93A)","Merlo, Argentina (RLO)","Cafayate, Argentina (9A4)","Purmamarca, Argentina (95A)","Tilcara, Argentina (96A)","San Miguel de TucumÃ¡n, Argentina (97A)","Tafi del Valle, Argentina (98A)","El ChaltÃ©n, Argentina (99A)","TREVELIN, Argentina (AR_18743)","San Martin, Argentina (SM1)","Tartagal, Argentina (AR_39)","Monteros, Argentina (AR_40)","Gualeguaychu, Argentina (GHU)","Alta Gracia, Argentina (06A)","Arroyito, Argentina (AR_002)","Capilla del Monte, Argentina (AR_003)","Colonia Caroya, Argentina (AR_004)","Cosquin, Argentina (AR_005)","Cruz del Eje, Argentina (AR_006)","Dean Funes, Argentina (AR_007)","Devoto, Argentina (AR_008)","General Cabrera, Argentina (AR_009)","Hernando, Argentina (AR_010)","Inriville, Argentina (AR_011)","Isla Verde, Argentina (AR_012)","Jesus MarÃ­a, Argentina (AR_013)","La Carlota, Argentina (AR_014)","La Falda, Argentina (AR_015)","Las Perdices, Argentina (AR_016)","Las Varillas, Argentina (AR_017)","Luque, Argentina (AR_018)","Marcos Juarez, Argentina (AR_019)","Mina Clavero, Argentina (AR_020)","Monte MaÃ­z, Argentina (AR_021)","Morteros, Argentina (AR_022)","Oliva, Argentina (AR_023)","Oncativo, Argentina (AR_024)","Pilar, Argentina (AR_025)","RÃ­o Ceballos, Argentina (AR_026)","RÃ­o Segundo, Argentina (AR_027)","RÃ­o Tercero, Argentina (AR_028)","Sampacho, Argentina (AR_029)","San Francisco , Argentina (AR_030)","Santa Rosa de Calamuchita, Argentina (AR_031)","Unquillo, Argentina (AR_032)","Valle Hermoso, Argentina (AR_033)","Villa Allende, Argentina (AR_034)","Villa Ascasubi, Argentina (AR_035)","Villa Carlos Paz, Argentina (AR_036)","Villa Dolores, Argentina (VDR)","Villa General Belgrano, Argentina (AR_037)","Villa Giardino, Argentina (AR_038)","Villa MarÃ­a, Argentina (AR_039)","Rafaela, Argentina (RAF)","San Jorge, Argentina (01A)","Sunchales, Argentina (NCJ)","Chilecito, Argentina (02A)","ParanÃ¡, Argentina (PRA)","Salta, Argentina (SLA)","Santiago del Estero, Argentina (SDE)","Santa Fe, Argentina (SFN)","Santa Rosa, Argentina (RSA)","Rosario, Argentina (ROS)","Viedma, Argentina (VDM)","Ushuaia, Argentina (USH)","Tucuman, Argentina (TUC)","San Juan, Argentina (UAQ)","Villa la Angostura, Argentina (VLA)","Termas de Rio Hondo, Argentina (RHD)","Rio Cuarto, Argentina (RCU)","Villa Mercedes, Argentina (VME)","Villa Gesell, Argentina (VGL)","Aruba, Aruba (AUA)","CuraÃ§ao, Curazao (CUR)"];

    var external_file_Airports = ["Annaba, Les Salines Arpt (AAE)","Arapoti, Arapoti Arpt (AAG)","Aachen, Aachen Merzbruck Arpt (AAH)","Aalborg, Aalborg Arpt (AAL)","Al Ain, Al Ain Arpt (AAN)","Anapa, Anapa Arpt (AAQ)","Aarhus, Tirstrup Arpt (AAR)","Allentown, Lehigh Valley Intl Arpt (ABE)","Abilene, Abilene Municipal Arpt (ABI)","Abidjan, Felix Houphouet Boigny Arpt (ABJ)","Bamaga, Bamaga Arpt (ABM)","Albuquerque, Albuquerque Intl Arpt (ABQ)","Aberdeen, Aberdeen Arpt (ABR)","Abu Simbel, Abu Simbel Arpt (ABS)","Al Baha, Al Aqiq (ABT)","Abuja, Abuja Intl (ABV)","Albury, Albury Airport (ABX)","Albany, Dougherty Cty Arpt (ABY)","Aberdeen, Dyce Airport (ABZ)","Acapulco, Alvarez Intl Arpt (ACA)","Accra, Kotoka Airport (ACC)","Lanzarote, Islas Canarias, Lanzarote Airport (ACE)","Altenrhein, Altenrhein Arpt (ACH)","Alderney, The Blaye Arpt (ACI)","Nantucket, Nantucket Memorial (ACK)","Waco, Madison Cooper Arpt (ACT)","Eureka, Arcata Eureka Arpt (ACV)","Atlantic City, Pomona Field (ACY)","Adana, Adana Airport (ADA)","Esmirna, Adnan Menderes Airport (ADB)","Addis Ababa, Bole Airport (ADD)","Aden, Yemen Intl Arpt (ADE)","Adrian, Lenawee County Arpt (ADG)","Adak Island, Adak Island Ns (ADK)","Adelaida, Adelaide Arpt (ADL)","Ardmore, Ardmore Municipal Arpt (ADM)","Kodiak, Kodiak Arpt (ADQ)","Ada OK, Ada Municipal Arpt (ADT)","Camp Springs, Andrews Air Force Base (ADW)","San AndrÃ©s Isla, Gustavo Rojas Pinilla Arpt (ADZ)","Albert Lea, Albert Lea Arpt (AEL)","Adler Sochi, Alder Sochi Arpt (AER)","Aalesund, Vigra Airport (AES)","Alexandria, Alexandria Intl Arpt (AEX)","Akureyri, Akureyri (AEY)","Alta Floresta, Alta Floresta Arpt (AFL)","Agadir, Agadir Almassira Arpt (AGA)","Munich, Muehlhausen (AGB)","Pittsburgo, Allegheny Cty Arpt (AGC)","Wangerooge, Flugplatz Arpt (AGE)","Agen, La Garenne Arpt (AGF)","Angelholm, Angelholm Helsingborg Arpt (AGH)","Magnolia, Magnolia Municipal (AGO)","Malaga, Malaga Arpt (AGP)","Agra, Kheria Arpt (AGR)","Augusta, Bush Field (AGS)","Ciudad Del Este, Alejo Garcia Arpt (AGT)","Aguascalientes, Aguascalientes Municipal Arpt (AGU)","Abha, Abha Airport (AHB)","Amahai, Amahai Airport (AHI)","Athens, Athens Municipal (AHN)","Alghero, Fertilia Arpt (AHO)","Al Hoceima, Charif Al Idrissi Arpt (AHU)","Alliance, Alliance Municipal (AIA)","Anderson, Anderson Municipal Arpt (AID)","Aiken, Aiken Municipal Arpt (AIK)","Aitutaki, Aitutaki Arpt (AIT)","Atlantic City, Bader Field (AIY)","Lake Of The Ozarks, Lee C Fine Memorial (AIZ)","Ajaccio, Campo Dell Oro Arpt (AJA)","Arvidsjaur, Arvidsjaur Arpt (AJR)","Aracaju, Santa Maria Arpt (AJU)","Asahikawa, Asahikawa Arpt (AKJ)","Auckland, Auckland Intl Arpt (AKL)","King Salmon, King Salmon Arpt (AKN)","Almaty, Almaty Arpt (ALA)","Albany, Albany Cty Arpt (ALB)","Alicante, Alicante Arpt (ALC)","Alpine, Alpine Aprt (ALE)","Alta, Elvebakken Arpt (ALF)","Argel, Houari Boumedienne Arpt (ALG)","Albany, Albany Airport (ALH)","Alexander Bay, Kortdoorn Arpt (ALJ)","Albenga, Albenga Arpt (ALL)","Alamogordo, Alamogordo Municipal (ALM)","Waterloo, Livingston Betsworth Fld (ALO)","Aleppo, Nejrab Arpt (ALP)","Alamosa, Alamosa Municipal (ALS)","Andorra La Vella, Andorra La Vella Hlpt_ZZ (ALV)","Walla Walla, Walla Walla City County (ALW)","Alexander City, Thomas C Russell Field (ALX)","Alejandria, El Nouzha Arpt (ALY)","Amarillo, Amarillo Intl Arpt (AMA)","Ahmedabad, Ahmedabad Arpt (AMD)","Mataram, Selaparang Airport (AMI)","Amman, Queen Alia Intl Arpt (AMM)","Amsterdam, Schiphol Arpt (AMS)","Ames, Ames Minicipal Arpt (AMW)","Anniston, Anniston Municipal Arpt (ANB)","Anchorage, Anchorage Intl Arpt (ANC)","Anderson, Anderson Arpt (AND)","Angers, Marce Arpt (ANE)","Antofagasta, Cerro Moreno Arpt (ANF)","Angouleme, Brie Champniers (ANG)","Aniak, Aniak Arpt (ANI)","Ankara, Etimesgut Arpt (ANK)","Annapolis, Lee Annapolis Arpt (ANP)","Antwerp, Deurne Airport (ANR)","Antigua, V C Bird Intl Arpt (ANU)","Ainsworth, Ainsworth Minicipal Arpt (ANW)","Andenes, Andenes Arpt (ANX)","Altenburg, Altenburg Nobitz Arpt (AOC)","Lima, Allen County Arpt (AOH)","Ancona, Falconara Arpt (AOI)","Aomori, Aomori Arpt (AOJ)","Karpathos, Karpathos Arpt (AOK)","Altoona, Blair Cty Arpt (AOO)","Alor Setar, Sultan Abdul Hamlin Arpt (AOR)","Denver, Centennial Arpt (APA)","Napa, Napa Cty Arpt (APC)","Naples, Naples Municipal (APF)","Baltimore, Phillips Army Air Field (APG)","Nampula, Nampula Arpt (APL)","Alpena, Phelps Collins Arpt (APN)","ApartadÃ³, Antonio Roldan Betancourt Arpt (APO)","Anapolis, Anapolis Arpt (APS)","Apia, Apia Airport (APW)","Qaisumah, Qaisumah Arpt (AQI)","Aqaba, Aqaba Airport (AQJ)","Arequipa, Rodriguez Ballon Arpt (AQP)","Ann Arbor, Ann Arbor Municipal (ARB)","Arkhangelsk, Arkhangelsk Arpt (ARH)","Arica, Chacalluta Arpt (ARI)","Armidale, Armidale Arpt (ARM)","Estocolmo, Arlanda Arpt (ARN)","Watertown, Watertown Arpt (ART)","Aracatuba, Aracatuba Arpt (ARU)","Minocqua, Noble F Lee Memorial Field (ARV)","Arad, Arad Arpt (ARW)","Ashgabat, Ashgabat Arpt (ASB)","Aspen, Pitkin Cty Arptt Sardy Fld (ASE)","Astrakhan, Astrakhan Arpt (ASF)","Nashua, Boire Field Arpt (ASH)","Amami O Shima, Amami O Shima Arpt (ASJ)","Yamoussouro, Yamoussoukro Arpt (ASK)","Marhsall, Harrison County Arpt (ASL)","Asmara, Asmara Intl Arpt (ASM)","Alice Springs, Alice Springs Arpt (ASP)","Kayseri, Kayseri Arpt (ASR)","AsunciÃ³n, Silvio Pettirosse Arpt (ASU)","Aswan, Daraw Arpt (ASW)","Arthurs Town, Arthurs Town Arpt (ATC)","Atenas, Eleftherios Venizielos Intl Ar (ATH)","Atlanta, Hartsfield Intl Arpt (ATL)","Altamira, Altamira Arpt (ATM)","Athens, Ohio University Arpt (ATO)","Appleton, Outagamie Cty Arpt (ATW)","Watertown, Watertown Municipal (ATY)","Augusta, Maine State Arpt (AUG)","Abu Dhabi, Dhabi Intl Arpt (AUH)","Auburn, Auburn Opelika (AUO)","Atuona, Atuona Arpt (AUQ)","Aurillac, Tronquieres Arpt (AUR)","Austin, Bergstrom Intl Arpt (AUS)","Wausau, Wausau Municipal Arpt (AUW)","Araguaina, Araguaina Arpt (AUX)","Aurora, Aurora Municipal Arpt (AUZ)","Ciego De Avila, Maximo Gomez Arpt (AVI)","Asheville, Asheville Municipal Arpt (AVL)","Avignon, Avignon Caumont Arpt (AVN)","Scranton, Wilkes Barre Scranton Intl Arp (AVP)","Avalon, Avalon Arpt (AVV)","Anguilla, Wallblake Municipal (AXA)","Alexandroupolis, Dhmokritos Arpt (AXD)","Alexandria, Alexandria Airport (AXN)","Altus, Altus Municipal Arpt (AXS)","Akita, Akita Airport (AXT)","Wapakoneta, Neil Armstrong Arpt (AXV)","Ayers Rock, Connellan Arpt (AYQ)","Ayr Au, Ayr Arpt (AYR)","Waycross, Ware County Arpt (AYS)","Antalya, Antalya Airport (AYT)","Yazd, Yazd Arpt (AZD)","Apatzingan, Apatzingan Arpt (AZG)","Kalamazoo, Kalamazoo Cty Arpt (AZO)","Baguio, Loakan Arpt (BAG)","Bahrain, Muharraq Arpt (BAH)","Baku, Baku Arpt (BAK)","Barranquilla, E Cortissoz Arpt (BAQ)","Bauru, Moussa Nakhl Tobias (JTC)","Balmaceda, Teniente Vidal Airport (BBA)","Barth, Barth Arpt (BBH)","Bhubaneswar, Bhubaneswar Arpt (BBI)","Bitburg, Bitburg Air Base (BBJ)","Kasane, Kasane Arpt (BBK)","Basse Terre, Baillif Arpt (BBR)","Bucarest, Baneasa Airport (BBU)","Broken Bow, Broken Bow Municipal (BBW)","Blacksburg, Virginia Tech Arpt (BCB)","Bryce, Bryce Arpt (BCE)","Baucau, English Madeira Arpt (BCH)","Barcelona, Barcelona Arpt (BCN)","Fort Lauderdale Florida, Boca Raton Public (BCT)","Bermuda, Bermuda International (BDA)","Bundaberg, Bundaberg Arpt (BDB)","Hartford, Bradley Intl Arpt (BDL)","Bandung, Husein Sastranegara Arpt (BDO)","Vadodara, Vadodara Arpt (BDQ)","Bridgeport, Sikorsky Memorial Arpt (BDR)","Brindisi, Apaola Casale Arpt (BDS)","Bardufoss, Bardufoss Arpt (BDU)","Benbecula, Benbecula Arpt (BEB)","Bedford, Bedford Arpt (BED)","Belgrado, Belgrade Beograd Arpt (BEG)","Benton Harbor, Ross Field (BEH)","Belem, Val De Cans Arpt (BEL)","Benghazi, Benina Intl Arpt (BEN)","Newcastle, Belmont Airport (BEO)","Bury St Edmunds, Honington Arpt (BEQ)","Brest, Guipavas Arpt (BES)","Bethel, Bethel Airport (BET)","Beira, Beira Arpt (BEW)","Beirut, Beirut Intl Arpt (BEY)","Bradford, Bradford Regional Arpt (BFD)","Bielefeld, Bielefeld Arpt (BFE)","Scottsbluff, Scottsbluff Municipal (BFF)","Bullfrog Basin, Bullfrog Basin Arpt (BFG)","Seattle, Seattle Boeing Field (BFI)","Ba City, Ba Arpt (BFJ)","Bakersfield, Meadows Field (BFL)","Bloemfontein, Bloemfontein International (BFN)","Beaver Falls, Beaver Falls Arpt (BFP)","Bedford, Virgil I Grissom Municipal (BFR)","Belfast, Belfast Intl Arpt (BFS)","Beaufort, Beaufort County Arpt (BFT)","Buri Ram, Buri Ram Arpt (BFV)","Bucaramanga, Palonegro Arpt (BGA)","Bangui, Bangui Airport (BGF)","Barbados, Grantley Adams Intl Arpt (BGI)","Binghamton, Edwin A Link Field (BGM)","Bergen, Flesland Airport (BGO)","Bangor, Bangor Intl Arpt (BGR)","Bento Goncalves, Bento Goncalves Arpt (BGV)","Baghdad, Al Muthana Arpt (BGW)","Bergamo, Orio Al Serio Arpt (BGY)","Bar Harbor, Hancock County (BHB)","Belfast, Belfast City Arpt (BHD)","Blenheim, Woodbourne Arpt (BHE)","Bhuj, Rudra Mata Arpt (BHJ)","Bukhara, Bukhara Arpt (BHK)","Bahia De Los Angeles, Bahia De Los Angeles Arpt (BHL)","Birmingham, Seibels Bryan Arpt (BHM)","Bhopal, Bhopal Arpt (BHO)","Broken Hill, Broken Hill Arpt (BHQ)","Bathurst, Raglan Airport (BHS)","Birmingham, Birmingham Intl Arpt (BHX)","Beihai, Beihai Airport (BHY)","Bastia, Poretta Airport (BIA)","Block Island, Block Island Municipal (BID)","El Paso, Biggs Army Air Field (BIF)","Bishop, Bishop Airport (BIH)","Bikini Atoll, Enyu Airfield (BII)","Biak, Mokmer Arpt (BIK)","Billings, Logan Field (BIL)","Bimini, Bimini Airport (BIM)","Bilbao, Sondica Arpt (BIO)","Biarritz, Bayonne Anglet Arpt (BIQ)","Bismarck, Bismarck Municipal Arpt (BIS)","Bisho, Bisho Arpt (BIY)","Broomfield, Jeffco Arpt (BJC)","Bemidji, Bemidji Municipal Arpt (BJI)","Banjul, Yundum Intl Arpt (BJL)","Bujumbura, Bujumbura Intl Arpt (BJM)","Bodrum, Milas Arpt (BJV)","Leon, Del Bajio Arpt (BJX)","Badajoz, Talaveral La Real Arpt (BJZ)","Moscu, Bykovo Arpt (BKA)","Kota Kinabalu, Kota Kinabalu Arpt (BKI)","Bangkok, Bangkok Intl Arpt (BKK)","Cleveland, Burke Lakefront Arpt (BKL)","Bamako, Senou Airport (BKO)","Beckley, Raleigh Cty Memorial (BKW)","Brookings, Brookings Municipal (BKX)","Barcelona, Gen J A Anzoategui Arpt (BLA)","Borlange, Dala Airport (BLE)","Bluefield, Mercer County (BLF)","Blythe, Blythe Airport (BLH)","Bellingham, Bellingham Intl Arpt (BLI)","Blackpool, Blackpool Airport (BLK)","Billund, Billund Airport (BLL)","Bolonia, Guglielmo Marconi (BLQ)","Bangalore, Hindustan Arpt (BLR)","Blackwater, Blackwater Arpt (BLT)","Belleville, Scott AFB Mid America (BLV)","Blantyre, Chileka Airport (BLZ)","Estocolmo, Bromma Airport (BMA)","Broome, Broome Airport (BME)","Bloomington, Monroe Cty Arpt Indiana (BMG)","Bloomington, Normal Airport (BMI)","Nashville, Nashville Metro Arpt (BNA)","Brisbane, Brisbane Arpt (BNE)","Bonn, Bonn Railroad Station (BNJ)","Ballina, Ballina Arpt (BNK)","Bronnoysund, Bronnoy Arpt (BNN)","Blumenau, Blumenau Arpt (BNU)","Banja Luka, Banja Luka Arpt (BNX)","Bora Bora, Motu Mute Arpt (BOB)","Bocas Del Toro, Bocas Del Toro Arpt (BOC)","Burdeos, Merignac (BOD)","BogotÃ¡, Eldorado Airport (BOG)","Bournemouth, Bournemouth Intl Arpt (BOH)","Boise, Boise Municipal Arpt Gowen Fie (BOI)","Bourgas, Bourgas Arpt (BOJ)","Bombay, Chhatrapati Shivaji Intl (BOM)","Bonaire, Flamingo Field (BON)","Bodo, Bodo Arpt (BOO)","Boston, Logan Intl Arpt (BOS)","Borroloola, Borroloola Arpt (BOX)","Balikpapan, Sepingan Arpt (BPN)","Porto Seguro, Porto Seguro Arpt (BPS)","Beaumont, Jefferson Cty Arpt (BPT)","Busselton, Busselton Arpt (BQB)","Brunswick, Gylnco Jet Port (BQK)","Aguadilla, Borinquen Municipal Arpt (BQN)","Blagoveshchensk, Blagoveshchensk Arpt (BQS)","Brest, Brest Arpt (BQT)","Barreiras, Barreiras Arpt (BRA)","Brainerd, Crowwing Cty Arpt (BRD)","Bremenhaven, Bremen Airport (BRE)","Bradford, Bradford Rail Station (BRF)","Bari, Bari Airport (BRI)","Bourke, Bourke Airport (BRK)","Burlington, Burlington Municipal Arpt (BRL)","Barquisimeto, Airport Barquisimeto (BRM)","Berna, Belp Airport (BRN)","Brownsville, South Padre Island Intl Arpt (BRO)","Brno, Turnay Arpt (BRQ)","Barra, North Bay Arpt (BRR)","Bristol, Bristol Intl Arpt (BRS)","Bathurst Isl, Bathurst Island Arpt (BRT)","Bruselas, Brussels National Arpt (BRU)","Bremerhaven, Bremerhaven Arpt (BRV)","Barrow, Barrow WBAS (BRW)","Barahona, Barahona Arpt (BRX)","Brasilia, Brasilia Intl Arpt (BSB)","Brighton, Brighton Rail Station (BSH)","Blairsville, Blairsville Arpt (BSI)","Bairnsdale, Bairnsdale Airport (BSJ)","Basilea, Basel Mulhouse Arpt (BSL)","Bisbee, Bisbee Municipal Arpt (BSQ)","Basra, Basra Intl Arpt (BSR)","Batam, Hang Nadim Arpt (BTH)","Banda Aceh, Blang Bintang Arpt (BTJ)","Battle Creek, WK Kellogg Regional (BTL)","Butte, Bert Mooney Arpt (BTM)","Baton Rouge, Ryan Airport (BTR)","Bratislava, Ivanka Arpt (BTS)","Burlington, Burlington Intl Arpt (BTV)","Bursa, Bursa Arpt (BTZ)","Budapest, Ferihegy Arpt (BUD)","Buffalo, Greater Buffalo Intl Arpt (BUF)","Bulawayo, Bulawayo Arpt (BUQ)","Burbank, Burbank Glendale Pasadena Arpt (BUR)","Batumi, Batumi Arpt (BUS)","Bunbury, Bunbury Arpt (BUY)","Paris, Beauvais Tille Arpt (BVA)","Boa Vista, Boa Vista Arpt (BVB)","Boa Vista, Rabil Arpt (BVC)","Brive La Gaill, Laroche Airport (BVE)","Vilhena, Vilhena Arpt (BVH)","Bartlesville, Bartlesville Municipal Arpt (BVO)","Brava, Esperadinha (BVR)","Batesville, Batesville Municipal (BVX)","Brawley, Brawley Arpt (BWC)","Brownwood, Brownwood Municipal Arpt (BWD)","Braunschweig, Braunschweig Arpt (BWE)","Barrow In Furness, Walney Island (BWF)","Bowling Green, Warren Cty Arpt (BWG)","Bandar Seri, Brunei Intl Arpt (BWN)","Brewarrina, Brewarrina Arpt (BWQ)","Burnie, Burnie Wynyard Arpt (BWT)","Bankstown, Bankstown Aerodrome (BWU)","Bodrum, Imsik Arpt (BXN)","Borrego Springs, Borrego Valley Arpt (BXS)","Burley, Burley Arpt (BYI)","Bayreuth, Bindlacher Berg Arpt (BYU)","Blakely Island, Blakely Is Municipal (BYW)","Buzios, Buzios Arpt (BZC)","Belice, Belize Intl Arpt (BZE)","Bydgoszcz, Bydgoszcz Arpt (BZG)","Bumi Hills, Bumi Hills Arpt (BZH)","Bozeman, Gallatin Field (BZN)","Bolzano, Bolzano Arpt (BZO)","Beziers, Beziers Vias Arpt (BZR)","Brazoria, Brazoria County Arpt (BZT)","Brazzaville, Maya Maya Arpt (BZV)","Oxford, Brize Norton Raf Station (BZZ)","Cascavel, Cascavel Arpt (CAC)","Cadillac, Wexford County Arpt (CAD)","Columbia, Columbia Metro Arpt (CAE)","Cagliari, Elmas Airport (CAG)","Cairo, Cairo Intl Arpt (CAI)","Akron, Akron Canton Regional Arpt (CAK)","Puerto Caldera, Machrihanish Arpt (CAL)","Camiri, Choreti Arpt (CAM)","Guangzhou, Baiyun Airport (CAN)","Cap Haitien, Cap Haitien Numicipal (CAP)","Casablanca, Anfa Airport (CAS)","Caucedo, Caruaru Arpt (CAU)","Campos, Bartolomeu Lisandro Arpt (CAW)","Carlisle, Carlisle Arpt (CAX)","Cayenne, Rochambeau Airport (CAY)","Cobar, Cobar Arpt (CAZ)","Cochabamba, J Wilsterman Arpt (CBB)","Cumberland, Cumberland Municipal Arpt (CBE)","Council Bluffs, Council Bluffs Municipal Arpt (CBF)","Cambridge, Cambridge Arpt (CBG)","Colby, Colby Municipal Arpt (CBK)","Ciudad Bolivar, Ciudad Bolivar Arpt (CBL)","Cirebon, Penggung Arpt (CBN)","Coimbra, Coimbra Arpt (CBP)","Camberra, Canberra Arpt (CBR)","Cabimas, Oro Negro Arpt (CBS)","Saint Martin, Grand Chase Arpt (CCE)","Carcassonne, Salvaza (CCF)","Kozhikode, Kozhikode Arpt (CCJ)","Criciuma, Criciuma Arpt (CCM)","Concepcion, Carriel Sur Arpt (CCP)","Concord, Buchanan Field (CCR)","Caracas, Simon Bolivar Arpt (CCS)","Calcuta, Netaji Subhas Chandra Bose Int (CCU)","Cooinda, Cooinda Airport (CDA)","Cedar City, Cedar City Municipal (CDC)","Cauquira, Cauquira Arpt (CDD)","Paris, Charles De Gaulle Intl Arpt (CDG)","Camden, Harrell Fieldsandro Arpt (CDH)","Cachoeiro De Itapemirim, Cachoeiro De Itapemirim Arpt (CDI)","Chadron, Chadron Arpt (CDR)","Cordova, Mudhole Smith Arpt (CDV)","Caldwell, Caldwell Wright Arpt (CDW)","Cebu, Cebu Intl (CEB)","Crescent City, Crescent City Municipal Arpt (CEC)","Ceduna, Ceduna Arpt (CED)","Springfield, Westover Metro (CEF)","Chester, Chester Arpt (CEG)","Chelinda, Chelinda Arpt (CEH)","Chiang Rai, Chaing Rai Arpt (CEI)","Chelyabinsk, Chelyabinsk Arpt (CEK)","Ciudad Obregon, Ciudad Obregon Arpt (CEN)","Cannes, Mandelieu Arpt (CEQ)","Cherbourg, Maupertius Arpt (CER)","Cessnock, Cessnock Arpt (CES)","Cholet, Le Pontreau Arpt (CET)","Clemson, Clemson Oconee Cty Arpt (CEU)","Murray, Calloway Cty Arpt (CEY)","Cortez, Montezuma County (CEZ)","Cabo Frio, Cabo Frio Arpt (CFB)","Clermont Ferrand, Aulnat Arpt (CFE)","Cienfuegos, Cienfuegos Arpt (CFG)","Donegal, Donegal Arpt (CFN)","Creston, Creston Arpt (CFQ)","Caen, Carpiquet Arpt (CFR)","Coffs Harbour, Coffs Harbour Arpt (CFS)","Kerkyra, I Kapodistrias Arpt (CFU)","Craig, Craig Seaplane Base (CGA)","Cuiaba, Marechal Rondon Arpt (CGB)","Cleveland, Cuyahoga County Airport (CGF)","San Pablo, Congonhas Arpt (CGH)","Cape Girardeau, Cape Girardeau Municipal Arpt (CGI)","Jakarta, Soekarno Hatta Intl (CGK)","Colonia/Bonn, Koeln Bonn Arpt (CGN)","Zhengzhou, Zhengzhou Arpt (CGO)","Chittagong, Patenga Arpt (CGP)","Changchun, Changchun Arpt (CGQ)","Campo Grande, Internacional (CGR)","Chicago, Meigs Field (CGX)","Cagayan, Lumbia Arpt (CGY)","Chattanooga, Chattanooga Lovell Fld (CHA)","Christchurch, Christchurch Intl Arpt (CHC)","Chandler, Williams Airforce Base (CHD)","Charlottesville, Charlottesville Albemarle Arpt (CHO)","Chania, Souda Arpt (CHQ)","Chateauroux, Chateauroux Arpt (CHR)","Charleston, Charleston Intl Arpt (CHS)","Changuinola, Changuinola Arpt (CHX)","Roma, Ciampino Arpt (CIA)","Chico, Chico Municipal Arpt (CIC)","Cedar Rapids, Cedar Rapids Municipal Arpt (CID)","Craig, Craig Moffat (CIG)","Corinto, Mcal Lopez Arpt (CIO)","Shimkent, Shimkent Arpt (CIT)","Sault Ste Marie, Chippewa Cnty Intl (CIU)","Canovan Island, Canouan Island Minicipal (CIW)","Chiclayo, Cornel Ruiz Arpt (CIX)","Coimbatore, Peelamedu Airport (CJB)","Calama, El Loa Arpt (CJC)","Cheongju, Cheongju Arpt (CJJ)","Chumphon, Chumphon Arpt (CJM)","El Cajon, El Cajon Arpt (CJN)","Ciudad Juarez, Intl Abraham Gonzalez (CJS)","Jeju City, Jeju Intl Arpt (CJU)","Clarksburg, Clarksburg Benedum Arpt (CKB)","Chongqing, Chongqing Arpt (CKG)","Clarksdale, Fletcher Field (CKM)","Carajas, Carajas Arpt (CKS)","Clarksville, Outlaw Field (CKV)","Conakry, Conakry Airport (CKY)","Clear Lake City, Metroport (CLC)","Carlsbad, Carlsbad Arpt (CLD)","Cleveland, Hopkins Intl Arpt (CLE)","Clinton, Clinton Municipal Arpt (CLK)","Callao, Easterwood Field (CLL)","Port Angeles, William Fairchild Intl Arpt (CLM)","Cali, Alfonso Bonilla AragÃ³n (CLO)","Colima, Colima Arpt (CLQ)","Charlotte, Charlotte Douglas Intl Arpt (CLT)","Columbus, Columbus Municipal Arpt (CLU)","Calvi, Ste Catherine Arpt (CLY)","Colombo, Bandaranaike Intl Arpt (CMB)","Ciudad Del Carmen, Ciudad Del Carmen Arpt (CME)","Chambery, Chambery Aix Les Bains Arpt (CMF)","Corumba, Internacional Corumba (CMG)","Columbus, Port Columbus Intl Arpt (CMH)","Champaign, Univ Of Illinois Willard Arpt (CMI)","Casablanca, Mohamed V Arpt (CMN)","Clermont, Clermont Arpt (CMQ)","Colmar, Colmar Houssen Arpt (CMR)","Coromandel, Coromandel Arpt (CMV)","Camaguey, Ign Agramonte Intl Arpt (CMW)","Houghton, Houghton Cty Memorial Arpt (CMX)","Sparta, Camp Mccoy Aaf (CMY)","Coonamble, Coonamble Arpt (CNB)","Constanta, Kogalniceanu Arpt (CND)","Belo Horizonte, Tancredo Neves Intl Arpt (CNF)","Cognac, Parvaud Arpt (CNG)","Lebanon, Claremont Municipal (CNH)","Cloncurry, Cloncurry Arpt (CNJ)","Carlsbad, Carlsbad Airport (CNM)","Cairns, Cairns Airport (CNS)","Chiang Mai, Chiang Mai Intl Arpt (CNX)","Moab, Moab Municipal (CNY)","Cody, Yellowstone Regional Arpt (COD)","Coeur D Alene, Coeur d Alene Municipal Arpt (COE)","Cocoa Metro Area, Patrick AFB (COF)","Cocoa Metro Area, Merrit Island Arpt (COI)","Coonabarabrn, Coonabarabran Arpt (COJ)","Kochi, Kochi Intl Arpt (COK)","Cotonou, Cotonou Airport (COO)","Colorado Springs, Colorado Springs Municipal Arp (COS)","Columbia, Columbia Regional (COU)","Coober Pedy, Coober Pedy Arpt (CPD)","Campeche, Campeche Intl Arpt (CPE)","Copenhague, Copenhagen Arpt (CPH)","Copiapo, Chamonate Arpt (CPO)","Campinas, International Campinas (CPQ)","Casper, Natrona Cty Intl Arpt (CPR)","St. Louis, East St Louis (CPS)","Ciudad Del Cabo, Cape Town International (CPT)","Campina Grande, Joao Suassuana Arpt (CPV)","Shahre Kord, Shahre Kord Arpt (CQD)","Myrtle Beach, Grand Strand Arpt (CRE)","Jacksonville, Craig Municipal Arpt (CRG)","Bruselas, Brussles South Charleroi Arpt (CRL)","Corpus Christi, Corpus Christi Intl Arpt (CRP)","Crotone, Crotone Arpt (CRV)","Charleston, Yeager Arpt (CRW)","Corinth, Roscoe Turner Arpt (CRX)","Turkmenabad, Turkmenabad Arpt (CRZ)","Creil, Creil Arpt (CSF)","Columbus, Columbus Metro Ft Benning Arpt (CSG)","Casino, Casino Arpt (CSI)","San Luis Obispo, O Sullivan Army Air Field (CSL)","Clinton, Sherman Arpt (CSM)","Carson City, Carson Arpt (CSN)","Changsha, Changsha Arpt (CSX)","Catania, Fontanarossa Arpt (CTA)","Charleville, Charleville Arpt (CTL)","Chetumal, Chetumal International (CTM)","Sapporo, Chitose Arpt (CTS)","Chengdu, Chengdu Arpt (CTU)","Cape Town, Cottonwood Airport (CTW)","Ciudad Constitucion, Ciudad Constitucion Arpt (CUA)","CÃºcuta, Camilo Dazo Arpt (CUC)","Cuenca, Cuenca Arpt (CUE)","Cuneo, Levaldigi Arpt (CUF)","Cudal, Cudal Arpt (CUG)","Culiacan, Fedl De Bachigualato Arpt (CUL)","Cancun, Cancun Aeropuerto Internacional (CUN)","CuraÃ§ao, Areopuerto Hato Arpt (CUR)","Columbus, Columbus Municipal (CUS)","Chihuahua, Chihuahua Airport (CUU)","Cusco, Tte Velazco Astete Arpt (CUZ)","Courchevel, Courchevel Arpt (CVF)","Cincinati, Cincinnati No Kentucky Intl Ar (CVG)","Ciudad Victoria, Ciudad Victoria Arpt (CVM)","Clovis, Clovis Airport (CVN)","Corvallis, Corvallis Municipal Arpt (CVO)","Carnarvon, Carnarvon Arpt (CVQ)","Coventry, Baginton Arpt (CVT)","Corvo Island, Corvo Arpt (CVU)","Wausau, Central Wisconsin Arpt (CWA)","Curitiba, Afonso Pena Arpt (CWB)","Clinton, Clinton Municipal (CWI)","Cardiff, Cardiff Wales Arpt (CWL)","Cowra, Cowra Arpt (CWT)","Vancouver, Coal Harbor Sea Plane Arpt (CXH)","Caxias Do Sul, Campo Dos Bugres Arpt (CXJ)","Calexico, Calexico Intl Arpt (CXL)","Conroe, Montgomery Co Arpt (CXO)","Cilacap, Tunggul Wulung Arpt (CXP)","Cayman Brac, Gerrard Smith Arpt (CYB)","Cayo Largo Del Sur, Cayo Largo Del Sur Arpt (CYO)","Colonia, Colonia Arpt (CYR)","Cheyenne, Cheyenne Arpt (CYS)","Chichen Itza, Chichen Itza Arpt (CZA)","Coro, Coro Arpt (CZE)","Constantine, Ain El Bey Arpt (CZL)","Cozumel, Aeropuerto Intl De Cozumel (CZM)","Cruzeiro Do Sul, Campo Intl Arpt (CZS)","Daytona Beach, Daytona Beach Regional Arpt (DAB)","Dhaka, Zia Intl Airport (DAC)","Da Nang, Da Nang Arpt (DAD)","Dakhla Oasis, Dakhla Arpt (DAK)","Dallas, Love Field (DAL)","Damasco, Damascus Intl (DAM)","Danville, Danville Municipal (DAN)","Dar Es Salaam, Es Salaam Intl (DAR)","David, Enrique Malek Arpt (DAV)","Dayton, Dayton International Airport (DAY)","Dublin, Dublin Municipal Arpt (DBN)","Dubbo, Dubbo Arpt (DBO)","Dubuque, Dubuque Municipal Arpt (DBQ)","Dubrobvnik, Dubrovnik Arpt (DBV)","Dalby, Dalby (DBY)","Washington D. C., Ronald Reagan National Arpt (DCA)","Dominica, Canefield Arpt (DCF)","Castres, Mazamet Arpt (DCM)","Doncaster, Finningley (DCS)","Decatur, Pyor Arpt (DCU)","Dodge City, Dodge City Municipal Arpt (DDC)","Daydream Island, Daydream Island Arpt (DDI)","Decatur, Decatur Municipal (DEC)","Decorah, Decorah Municipal (DEH)","Delhi, Delhi Indira Gandhi Intl (DEL)","Denver, Denver Intl Arpt (DEN)","Detroit, Detroit City Apt (DET)","Defiance, Defiance Memorial Arpt (DFI)","Dallas, Dallas Ft Worth Intl (DFW)","Mudgee, Mudgee Arpt (DGE)","Dongguan, Dongguan Arpt (DGM)","Durango, Guadalupe Victoria Arpt (DGO)","Daugavpils, Daugavpils Arpt (DGP)","Dhahran, Dhahran Intl (DHA)","Dothan, Dothan Municipal (DHN)","Diqing, Diging Arpt (DIG)","Dijon, Longvic Airport (DIJ)","Dickinson, Dickinson Municipal (DIK)","Dili, Comoro Arpt (DIL)","Dire Dawa, Aba Tenna D Yilma Arpt (DIR)","Diu In, Diu Arpt (DIU)","Diyarbai, Diyarbakirarpt (DIY)","Jambi, Sultan Taha Syarifudin Arpt (DJB)","Djerba, Melita Airport (DJE)","Dunk Island, Dunk Island Arpt (DKI)","Dakar, Leopold Sedar Senghor Arpt (DKR)","Douala, Douala Arpt (DLA)","Dalian, Dalian Airport (DLC)","Geilo, Dagali Arpt (DLD)","Dole, Tavaux Arpt (DLE)","Dillingham, Dillingham Municipal Arpt (DLG)","Duluth, Duluth Intl (DLH)","Dalat, Lienkhang (DLI)","Dillon, Dillon Arpt (DLL)","Dalaman, Dalman Airport (DLM)","Paris, Disneyland Paris Heliport (DLP)","The Dalles, The Dalles Municipal Arpt (DLS)","Moscu, Domodedovo Arpt (DME)","Dammam, King Fahad Arpt (DMM)","Sedalia, Sedalia Memorial Arpt (DMO)","Dundee, Riverside Park Arpt (DND)","Dnepropetrovsk, Dnepropetrovsk Arpt (DNK)","Augusta, Daniel Airport (DNL)","Deniliquin, Denilinquin Arpt (DNQ)","Dinard, Pleurtuit Arpt (DNR)","Danville, Vermillion Cty (DNV)","Denizli, Cardak Arpt (DNZ)","Dornoch, Dornoch Arpt (DOC)","Doha, Doha International Arpt (DOH)","Donetsk, Donetsk Arpt (DOK)","Deauville, Saint Gatien Arpt (DOL)","Dominica, Melville Hall Arpt (DOM)","Dover, Dover AFB (DOV)","Dongara, Dongara (DOX)","Chicago, Dupage County Arpt (DPA)","Dieppe, Dieppe Arpt (DPE)","Devonport, Devonport Arpt (DPO)","Denpasar, Ngurah Rai Arpt (DPS)","Derby, Derby Airport (DRB)","Durango, Durango La Plata Cty Arpt (DRO)","Dresde, Dresden Arpt (DRS)","Del Rio, International Del Rio (DRT)","Darwin, Darwin Airport (DRW)","La Desirade, La Desirade Arpt (DSD)","Destin, Destin Arpt (DSI)","Des Moines, Des Moines Municipal Airport (DSM)","Dortmund, Wickede Dortmund Arpt (DTM)","Detroit, Detroit Wayne County Arpt (DTW)","Dublin, Dublin Arpt (DUB)","Dunedin, Momona Airport (DUD)","Douglas, Bisbee Douglas Intl (DUG)","Dubois, Dubois Jefferson Cty Arpt (DUJ)","Duncan, Duncan/Quam Rail Station (DUQ)","Durban, Durban International (DUR)","Dusseldorf, Dusseldorf Arpt (DUS)","Dutch Harbor, Emergency Field (DUT)","Devils Lake, Devils Lake Arpt (DVL)","Davenport, Davenport Airport (DVN)","Davao, Mati Airport (DVO)","Dover, Delaware Airpark (DVX)","Houston, David Wayne Hooks Arpt (DWH)","Oklahoma City, Downtown Airpark (DWN)","Dubai, Dubai Intl Arpt (DXB)","Danbury, Danbury Municipal Arpt (DXR)","Dayong, Dayong Arpt (DYG)","Doylestown, Doylestown Arpt (DYL)","Dushanbe, Dushanbe Arpt (DYU)","Dzaoudzi, Dzaoudzi Arpt (DZA)","Eagle, Eagle Airport (EAA)","Nejran, Nejran Arpt (EAM)","Kearney, Kearney Municipal Arrpt (EAR)","San Sebastion, Fuenterrabia Arpt (EAS)","Wenatchee, Pangborn Memorial Fld (EAT)","Eau Claire, Claire Municipal Airport (EAU)","Elba Island, Marina Di Campo Arpt (EBA)","Entebbe Kampala, Entebbe Airport (EBB)","Esbjerg, Esbjerg Arpt (EBJ)","Baton Rouge, Baton Rouge Downtown Arpt (EBR)","St Etienne, Boutheon Arpt (EBU)","Elizabeth City, Elizabeth Municipal Cgas (ECG)","Echuca, Echuca Arpt (ECH)","Ercan, Ercan Airport (ECN)","Edimburgo, Turnhouse Arpt (EDI)","Eldoret, Eldoret Arpt (EDL)","La Roche, Les Ajoncs Arpt (EDM)","Needles, Needles Arpt (EED)","Keene, Dilliant Hopkins Arpt (EEN)","Houston, Ellington Field (EFD)","Kefalonia, Argostoli Arpt (EFL)","Bergerac, Roumaniere Arpt (EGC)","Eagle, Eagle County Arpt (EGE)","Belgorod, Belgorod Arpt (EGO)","Egilsstadir, Egilsstadir Arpt (EGS)","Eagle River, Eagle River Union Arpt (EGV)","East Hartford, Rentschler Arpt (EHT)","Eisenach, Eisenach Arpt (EIB)","Eindhoven, Welschap Arpt (EIN)","Beef Island, Beef Island Arpt (EIS)","Barrancabermeja, YariguÃ­es Arpt (EJA)","Elkhart, Elkhart Municipal Arpt (EKI)","Elkins, Randolph County Arpt (EKN)","Elko, J C Harris Field (EKO)","Elizabethtown, Addington Field (EKX)","El Dorado, Goodwin Field (ELD)","North Eleuthera, North Eleuthera Intl (ELH)","Elk City, Elk City Municipal (ELK)","Elmira, Elmira Corning Regional Arpt (ELM)","El Paso, El Paso Intl Arpt (ELP)","Gassim, Gassim Arpt (ELQ)","East London, East London Arpt (ELS)","Ely Nv, Yelland Field (ELY)","Nottingham UK, East Midlands Arpt (EMA)","Emerald, Emerald Arpt (EMD)","El Monte, El Monte Arpt (EMT)","Kenai, Kenai Municipal Arpt (ENA)","Nancy, Essey Airport (ENC)","Enniskillen, St Angelo Arpt (ENK)","Centralia, Centralia Municipal Arpt (ENL)","Enschede, Twente Airport (ENS)","Kenosha, Kenosha Municipal Arpt (ENW)","MedellÃ­n, Enrique Olaya Herrara (EOH)","Eday, Eday Arpt (EOI)","Keokuk, Keokuk Arpt (EOK)","Epinal, Mirecourt Arpt (EPL)","Esperance, Esperance Arpt (EPR)","Erfurt, Erfurt Arpt (ERF)","Erie, Erie Intl (ERI)","Windhoek, Eros Arpt (ERS)","Erzurum, Erzurum Arpt (ERZ)","Ankara, Esenboga Arpt (ESB)","Escanaba, Delta County (ESC)","East Sound, Eastsound Orcas Is Arpt (ESD)","Ensenada, Ensenada Arpt (ESE)","Alexandria, Esler Field (ESF)","Elista, Elista Arpt (ESL)","Easton, Easton Municipal Arpt (ESN)","East Stroudsburg, Birchwood Pocono Arpt (ESP)","El Salvador, El Salvador Arpt (ESR)","Essen, Essen Arpt (ESS)","Essaouira, Essaouira Arpt (ESU)","West Bend, West Bend Arpt (ETB)","Elat, Elat Airport (ETH)","Enterprise, Enterprise Municipal (ETS)","Metz Nancy, Metz Nancy Lorraine (ETZ)","Eufaula, Weedon Field (EUF)","Eugene, Eugene Arpt (EUG)","El Aaiun, Hassan I Arpt (EUN)","St Eustatius, Roosevelt Field (EUX)","Harstad Narvik, Evenes Arpt (EVE)","Sveg, Sveg Arpt (EVG)","Eveleth, Eveleth Virginia Municipal Arp (EVM)","Erevan, Yerevan Arpt (EVN)","Evansville, Evansville Regional Arpt (EVV)","Evreux, Evreux Arpt (EVX)","New Bedford, New Bedford Municipal (EWB)","Newton, Newton City County Arpt (EWK)","New Bern, Simmons Nott Arpt (EWN)","Excursion Inlet, Excursion Inlet Municipal (EXI)","Exmouth Gulf, Exmouth Gulf (EXM)","Exeter, Exeter Arpt (EXT)","Key West, Key West Intl (EYW)","Elazig, Elazig Arpt (EZS)","Faeroe Islands, Faeroe Airport (FAE)","Fairbanks, Fairbanks Intl Arpt (FAI)","Fajardo, Fajardo Arpt (FAJ)","Faro, Faro Airport (FAO)","Fargo, Hector Airport (FAR)","Fresno, Fresno Air Terminal (FAT)","Fayetteville, Fayetteville Municipal (FAY)","Lubumbashi, Luano (FBM)","Kalispell, Glacier Park Intl (FCA)","Ficksburg, Ficksburg Sentra Oes (FCB)","Roma, Leonardo Da Vinci (FCO)","Forrest City, Forrest City Municipal Arpt (FCY)","Forde, Bringeland Arpt (FDE)","Fort De France, Lamentin Arpt (FDF)","Friedrichshafen, Friedrichshafen Lowenthal (FDH)","Frederick, Frederick Municipal (FDK)","Fernando De Noronha, Fernando De Noronha Arpt (FEN)","Fez Ma, Fez Airport (FEZ)","Fergus Falls, Fergus Falls Municipal (FFM)","Apia, Fagali Arpt (FGI)","Ft Huachuca, Sierra Vista Municipal (FHU)","Fair Isle, Fair Isle Arpt (FIE)","Kinshasa, Kinshasa Arpt (FIH)","Al Fujairah, Fujairah Intl Arpt (FJR)","Karlsruhe Baden Baden, Soellingen Arpt (FKB)","Franklin, Chess Lamberton Arpt (FKL)","Fukushima, Fukushima Arpt (FKS)","Floriano, Cangapara Arpt (FLB)","Flensburg, Schaferhaus Arpt (FLF)","Flagstaff, Flagstaff Arpt (FLG)","Fort Lauderdale Florida, Ft Lauderdale Hollywood Intl A (FLL)","Florianopolis, Hercilio Luz Arpt (FLN)","Florence, Gilbert Field (FLO)","Florencia, Peretola Arpt (FLR)","Santa Cruz Flores, Aerodromo Das Flores (FLW)","Fallon, Fallon Municipal Arpt (FLX)","Falmouth, Otis AFB (FMH)","Farmington, Four Corners Regional Arpt (FMN)","Muenster, Muenster Airport (FMO)","Fort Madison, Fort Madison Municipal (FMS)","Fort Myers, Page Field (FMY)","Freetown, Lungi Intl Arpt (FNA)","Funchal Isla Madeira, Funchal Airport (FNC)","Nimes, Nimes Airport (FNI)","Ft Collins, Municipal Airport (FNL)","Flint, Bishop Intl Arpt (FNT)","Fuzhou, Fuzhou Arpt (FOC)","Fort Dodge, Ft Dodge Municipal (FOD)","Topeka, Forbes Field (FOE)","Foggia, Gino Lisa Arpt (FOG)","Fortaleza, Pinto Martines Arpt (FOR)","Forster, Forster (FOT)","Freeport, Freeport Intl Arpt (FPO)","Fort Pierce, St Lucie County Arpt (FPR)","Frankfurt, Frankfurt Intl (FRA)","Friday Harbor, Friday Harbor Airport (FRD)","Farmingdale, Republic Arpt (FRG)","Frejus, Frejus Arpt (FRJ)","Forli, Luigi Ridolfi Arpt (FRL)","Fairmont, Fairmont Municipal (FRM)","Floro, Flora Arpt (FRO)","Front Royal, Warren County Arpt (FRR)","Flores, Flores Airport (FRS)","Bishkek, Bishkek Arpt (FRU)","Francistown, Francistown Arpt (FRW)","Fritzlar, Fritzlar Airbase (FRZ)","Figari, Sud Corse Arpt (FSC)","Sioux Falls, Joe Foss Field (FSD)","Ft Smith, Ft Smith Municipal (FSM)","St Pierre, St Pierre Arpt (FSP)","Fort Stockton, Pecos County Arpt (FST)","Ft Worth, Meacham Field (FTW)","Atlanta, Fulton Cty Arpt (FTY)","Fuerte Ventura, Islas Canarias, Fuerteventura Arpt (FUE)","Fukue, Fukue Arpt (FUJ)","Fukuoka, Itazuke Arpt (FUK)","Fullerton, Fullerton Municipal Arpt (FUL)","Funafuti, Funafuti Intl Arpt (FUN)","Ft Wayne, Baer Field (FWA)","Fort William, Fort William Arpt (FWM)","Fort Lauderdale Florida, Ft Lauderdale Excutive (FXE)","Fayetteville, Fayetteville Municipal Arpt (FYV)","Gadsden, Gadsden Municipal (GAD)","Gaithersburg, Montgomery Cty Arpt (GAI)","Yamagata, Yamagata Airport (GAJ)","Galena, Galena Arpt (GAL)","Gap France, Tallard Arpt (GAT)","Great Bend, Greate Bend Municipal (GBD)","Gaborone, Gaborone Arpt (GBE)","Galesburg, Galesburg Arpt (GBG)","Gbangbatok, Gbangbatok Arpt (GBK)","San Giovanni Rotondo, San Giovanni Rotondo Arpt (GBN)","Great Barrier Island, Great Barrier Island Arpt (GBZ)","Gillette, Campbell Cty Arpt (GCC)","Guernsey, Guernsey Arpt (GCI)","Johannesburgo, Grand Central Arpt (GCJ)","Garden City, Garden City Municipal (GCK)","Grand Cayman Island, Owen Roberts Arpt (GCM)","Grand Canyon, Grand Canyon Natl Park Arpt (GCN)","Gravatai, Gravatai Arpt (GCV)","Greenville, Municipal Greenville (GCY)","Guadalajara, Miguel Hidalgo Arpt (GDL)","Gdansk, Rebiechowo (GDN)","Grand Turk, Grand Turk Is Arpt (GDT)","Magadan, Magadan Arpt (GDX)","Noumea, Magenta Arpt (GEA)","Georgetown, Sussex County Arpt (GED)","Spokane, Spokane Intl Arpt (GEG)","Georgetown, Cheddi Jagan Intl (GEO)","Nueva Gerona, Rafael Cabrera Arpt (GER)","Geraldton, Geraldton Arpt (GET)","Gallivare, Gallivare Arpt (GEV)","Geelong, Geelong Arpt (GEX)","Greenfield, Pope Field Arpt (GFD)","Griffith, Griffith Arpt (GFF)","Grand Forks, Grand Forks Mark Andrews Intl (GFK)","Glens Falls, Warren County (GFL)","Grafton, Grafton Arpt (GFN)","Myrtle Beach, George Town Arpt (GGE)","Longview, Greg County Arpt (GGG)","George Town, George Town Airport (GGT)","Glasgow, International Glasgow (GGW)","Ghardaia, Noumerate Arpt (GHA)","Governor S Harbour, Governors Harbour Municipal Ar (GHB)","Gibraltar, North Front Arpt (GIB)","Winter Haven, Gilbert Fld (GIF)","Rio De Janeiro, Rio Internacional (GIG)","Gilgit, Gilgit Arpt (GIL)","Gisborne, Gisborne Arpt (GIS)","Gizan, Gizan Aprt (GIZ)","Grand Junction, Walker Field Arpt (GJT)","Goroka, Goroka Arpt (GKA)","Gatlinburg, Gatlinburg Arpt (GKT)","Glasgow, Glasgow Intl (GLA)","Goodland, Goodland Municipal (GLD)","Gainesville, Gainesville Municipal Arpt (GLE)","Greenville, Greenville Municipal (GLH)","Glen Innes, Glen Innes Arpt (GLI)","Gol City, Klanten Arpt (GLL)","Gloucester, Staverton Arpt (GLO)","Gaylord, Otsego Arpt (GLR)","Galveston, Scholes Field (GLS)","Gladstone, Gladstone Airport (GLT)","Gomel, Gomel Arpt (GME)","Seul, Gimpo Intl Arpt (GMP)","Greenville, Greenville Downtown Arpt (GMU)","San Sebastian De La Gomera, La Gomera Arpt (GMZ)","Grodna, Grodna Arpt (GNA)","Grenoble, Saint Geoirs Arpt (GNB)","Granada, Port Saline Intl (GND)","Gunungsitoli, Gunungsitoli Arpt (GNS)","Gainesville, Gainesville Regional (GNV)","Genoa, Christoforo Colombo (GOA)","Nuuk, Nuuk Arpt (GOH)","Goa, Dabolim Arpt (GOI)","Nizhniy Novgorod, Nizhniy Novgorod Arpt (GOJ)","New London, Groton New London Arpt (GON)","Goondiwindi, Goondiwindi Arpt (GOO)","Gotemburgo, Landvetter Arpt (GOT)","Gove, Nhulunbuy Arpt (GOV)","Patras, Araxos Arpt (GPA)","Galapagos Is, Baltra Arpt (GPS)","Gulfport, Biloxi Regional Arpt (GPT)","Grand Rapids, Itasca County (GPZ)","Galion, Galion Municipal Arpt (GQQ)","Green Bay, Austin Straubel Fld (GRB)","Grand Island, Hall Cty Regional (GRI)","George, George Arpt (GRJ)","Gerona, Costa Brava Arpt (GRO)","Groningen, Eelde Arpt (GRQ)","Grand Rapids, Kent County Intl (GRR)","Grosseto, Baccarini Arpt (GRS)","San Pablo, Guarulhos Arpt (GRU)","Graciosa Island, Graciosa Arpt (GRW)","Granada, Granada Arpt (GRX)","Graz, Thalerhof Arpt (GRZ)","Goldsboro, Seymour Johnson AFB (GSB)","Gotemburgo, Saeve Arpt (GSE)","Greensboro, Piedmont Triad Intl (GSO)","Greenville, Greenville Spartanburg Arpt (GSP)","Glacier Bay, Gustavus Arpt (GST)","Grimsby, Binbrook Arpt (GSY)","Great Falls, Great Falls Intl Arpt (GTF)","Guettin, Guettin Arpt (GTI)","Columbus, Golden Arpt (GTR)","Gettysburg, Gettysburg Arpt (GTY)","Guatemala, La Aurora Arpt (GUA)","Gunnison, Gunnison Cty (GUC)","Gulf Shores, Edwards Arpt (GUF)","Gunnedah, Gunnedah Airport (GUH)","Guam, Antonio B Won Pat Intl (GUM)","Gallup, Gallup Municipal (GUP)","Alotau, Gurney Arpt (GUR)","Gutersloh, Guetersloh Arpt (GUT)","Ginebra, Ginebra Cointrin (GVA)","Governador Valadares, Governador Valadares Arpt (GVR)","Gavle, Sandviken Arpt (GVX)","Gweru, Gweru Airport (GWE)","Greenwood, Leflore Arpt (GWO)","Glenwood Springs, Glenwood Springs Arpt (GWS)","Westerland, Westerland Airport (GWT)","Galway, Carnmore Arpt (GWY)","Coyhaique, Teniente Vidal Arpt (GXQ)","Guayaquil, Aeropuerto Int. Jose Joaquin de Olmedo (GYE)","Guaymas, Gen Jose M Yanez Arpt (GYM)","Goiania, Santa Genoveva (GYN)","Gympie, Gympie Arpt (GYP)","Goodyear, Litchfield Goodyear Arpt (GYR)","Guang Yuan, Guang Yuan Arpt (GYS)","Gary, Gary Regional Arpt (GYY)","Gaza, Gaza International Arpt (GZA)","Gozo, Gozo Arpt (GZM)","Gaziantep, Gaziantep Arpt (GZT)","Hachijo Jima, Hachijo Jima Arpt (HAC)","Halmstad, Halmstad Arpt (HAD)","Moroni, Prince Said Ibrahim In (HAH)","Hanover, Hanover Arpt (HAJ)","Haikou, Haikou Arpt (HAK)","Hamburgo, Fuhlsbuettel Arpt (HAM)","Hanoi, Noibai Arpt (HAN)","Long Island, Long Island Arpt (HAP)","Hail, Hail Arpt (HAS)","Haugesund, Karmoy Arpt (HAU)","La Habana, Jose Marti Intl Arpt (HAV)","Hobart, Hobart International Arpt (HBA)","Borg El Arab, Borg El Arab Arpt (HBE)","Hattiesburg, Bobby L Chain Municipal (HBG)","Harbour Island, Harbour Island Arpt (HBI)","Hafr Albatin, Hafr Albatin Arpt (HBT)","Hengchun, Hengchun Arpt (HCN)","Heidelberg, Heidelberg Arpt (HDB)","Hyderabad, Hyderabad (HDD)","Heringsdorf, Heringsdorf Arpt (HDF)","Hayden, Hayden Arpt (HDN)","Hoedspruit, Hoedspruit Arpt (HDS)","Hat Yai, Hat Yai Arpt (HDY)","Heho, Heho Arpt (HEH)","Heide Buesum, Heide Arpt (HEI)","Helsinki, Helsinki Arpt (HEL)","Heraklion, N Kazantzakis Arpt (HER)","Hohhot, Hohhot Arpt (HET)","Atenas, Athens Hellinikon Arpt (HEW)","Natchez, Natchez Adams Cty (HEZ)","Haifa, Haifa Arpt (HFA)","Hartford, Brainard Arpt (HFD)","Hefei, Hefei Arpt (HFE)","Hammerfest, Hammerfest Arpt (HFT)","Hangzhou, Hangzhou Arpt (HGH)","Helgoland, Helgoland Arpt (HGL)","Mae Hongson, Mae Hong Son Arpt (HGN)","Hagerstown, Washington Cty Regional (HGR)","Mt Hagen, Kagamuga Arpt (HGU)","Huanghua, Changsha Huanghua Arpt (HHA)","Hilton Head, Hilton Head Municipal (HHH)","Frankfurt, Hahn Arpt (HHN)","Hua Hin, Hua Hin Arpt (HHQ)","Hawthorne, Hawthorne Arpt (HHR)","Hibbing, Hibbing Chisolm Arpt (HIB)","Horn Island, Horn Island Arpt (HID)","Lake Havasu Cty, Lake Havasu City Municipal Air (HII)","Hiroshima, Hiroshima Airport (HIJ)","Honolulu, Hickam Air Force Base (HIK)","Chinju, Sacheon Arpt (HIN)","Hillsboro, Portland Hillsboro Arpt (HIO)","Honiara, Henderson Intl Arpt (HIR)","Hayman Island, Hayman Island Arpt (HIS)","Khajuraho, Khajuraho Arpt (HJR)","Blytheville, Blytheville Municipal Arpt (HKA)","Healy Lake, Healy Lake Arpt (HKB)","Hakodate, Kakodate Arpt (HKD)","Hong Kong, Hong Kong Intl (HKG)","Hokitika, Hokitika Airport (HKK)","Hoskins, Hoskins Arpt (HKN)","Jackson, Hawkins Field (HKS)","Phuket, Phuket Intl Airport (HKT)","Hickory, Hickory Municipal (HKY)","Lanseria, Lanseria Arpt (HLA)","Batesville, Hillenbrand (HLB)","Hultsfred, Hultsfred Arpt (HLF)","Holland, Park Township (HLM)","Helena, Helena Municipal (HLN)","Jakarta, Halim Perdana Kusuma Arpt (HLP)","St Helens, St Helens Arpt (HLS)","Hluhluwe, Hluhluwe Arpt (HLW)","Holyhead, Holyhead Rail Station (HLY)","Hamilton, Hamilton Arpt (HLZ)","Natashquan, Natashquan Arpt (YNA)","Yanbo, Yanbo Arpt (YNB)","Gatineau Hull, Gatineau Hull Municipal Arpt (YND)","Youngstown, Youngstown Municipal (YNG)","Yanji, Yanji Arpt (YNJ)","Yandicoogina, Yandi Arpt (YNN)","Yantai, Laishan Arpt (YNT)","Cold Lake, Cold Lake Arpt (YOD)","High Level, Footner Lake Arpt (YOJ)","Oshawa, Oshawa Municipal Arpt (YOO)","Rainbow Lake, Rainbow Lake Arpt (YOP)","Owen Sound, Billy Bishop Regional Arpt (YOS)","Yotvata, Yotvata Arpt (YOT)","Ottawa, Ottawa Intl Arpt (YOW)","Prince Albert, Prince Albert Municipal Arpt (YPA)","Port Alberni, Port Alberni Arpt (YPB)","Parry Sound, Parry Sound Municipal Arpt (YPD)","Peace River, Peace River Municipal Arpt (YPE)","Esquimalt, Esquimalt Rail Station (YPF)","Portage La Prairie, Portage La Prairie Rail Statio (YPG)","Pickle Lake, Provincial Airport (YPL)","St Pierre, Pikangikum Arpt (YPM)","Port Menier, Port Menier Arpt (YPN)","Peterborough, Peterborough Arpt (YPQ)","Prince Rupert, Digby Island Arpt (YPR)","Port Hawkesbury, Port Hawkesbury Arpt (YPS)","Powell River, Westview Arpt (YPW)","Burns Lake, Burns Lake Rail Station (YPZ)","Muskoka, Muskoka Arpt (YQA)","Quebec, Quebec Intl (YQB)","The Pas, The Pas Municipal Arpt (YQD)","Calgary, Red Deer Arpt (YQF)","Windsor, Windsor Intl Arpt (YQG)","Watson Lake, Watson Lake Arpt (YQH)","Yarmouth, Yarmouth Municipal Arpt (YQI)","Kenora, Kenora Airport (YQK)","Lethbridge, Lethbridge Arpt (YQL)","Moncton, Moncton Municipal (YQM)","Nakina, Nakina Rail Station (YQN)","Comox, Royal Canadian Air Force Stati (YQQ)","Regina, Regina Municipal (YQR)","St Thomas, Pembroke Area Municipal Arpt (YQS)","Thunder Bay, Thunder Bay Arpt (YQT)","Grande Prairie, Grande Prairie Arpt (YQU)","Yorkton, Yorkton Airport (YQV)","North Battleford, North Battleford Arpt (YQW)","Gander, Gander International (YQX)","Sydney, Sydney Airport (YQY)","Quesnel, Quesnel Arpt (YQZ)","Riviere Du Loup, Riviere Du Loup Arpt (YRI)","Roberval, Roberval Airport (YRJ)","Red Lake, Federal Red Lake (YRL)","Trois Rivieres, Three Rivers Arpt (YRQ)","Rankin Inlet, Rankin Inlet Arpt (YRT)","Sudbury, Sudbury Municipal Arpt (YSB)","Sherbrooke, Sherbrooke Airport (YSC)","Smith Falls, Smith Falls Rail Station (YSH)","St John, St John Municipal (YSJ)","St Leonard, Edmunston Arpt (YSL)","Ft Smith, Ft Smith Municipal Arpt (YSM)","Salmon Arm, Salmon Arm Municipal (YSN)","Marathon, Marathon Municipal Arpt (YSP)","St Theris Point, St Therese Pt Municipal (YST)","Pembroke, Pembroke And Area (YTA)","Thicket Portage, Thicket Portage Rail Station (YTD)","Cape Dorset, Cape Dorset Arpt (YTE)","Alma, Alma Municipal Arpt (YTF)","Thompson, Thompson Arpt (YTH)","Terrace Bay, Terrace Bay Municipal Arpt (YTJ)","Trenton, Trenton Municipal Arpt (YTR)","Timmins, Timmins Municipal Arpt (YTS)","Toronto, Toronto City Centre Airport (YTZ)","Yuma, Yuma Intl (YUM)","Rouyn Noranda, Rouyn Noranda Arpt (YUY)","Moroni, Iconi Arpt (YVA)","Bonaventure, Bonaventure Municipal Arpt (YVB)","Vernon, Vernon Municipal Arpt (YVE)","Vermilion, Vermilion Arpt (YVG)","Val D Or, Val d Or Municipal Arpt (YVO)","Kuujjuaq, Kuujjuaq Arpt (YVP)","Norman Wells, Norman Wells Municipal Arpt (YVQ)","Vancouver, Vancouver Intl Arpt (YVR)","Deer Lake, Deer Lake Arpt (YVZ)","Winnipeg, Winnipeg Intl Arpt (YWG)","Victoria, Inner Harbor Sea Plane Arpt (YWH)","Wabush, Wabush Municipal Arpt (YWK)","Williams Lake, Williams Lake Municipal (YWL)","White River, White River Rail Station (YWR)","Whistler, Whistler Arpt (YWS)","Cranbrook, Cranbrook Municipal (YXC)","Edmonton, Edmonton Municipal Arpt (YXD)","Saskatoon, Saskatoon Municipal (YXE)","Medicine Hat, Medicine Hat Airport (YXH)","Ft St John, Ft St John Municipal Arpt (YXJ)","Rimouski, Rimouski Municipal Arpt (YXK)","Sioux Lookout, Sioux Lookout Municipal Arpt (YXL)","Pangnirtung, Pangnirtung Arpt (YXP)","Prince George, Prince George Municipal (YXS)","Terrace, Terrace Municipal Arpt (YXT)","London, London Municipal (YXU)","Abbotsford, Abbotsford Arpt (YXX)","Whitehorse, Whitehorse Arpt (YXY)","Wawa, Wawa Municipal Arpt (YXZ)","North Bay, North Bay Municipal (YYB)","Calgary, Calgary Intl Arpt (YYC)","Smithers, Smithers Municipal (YYD)","Fort Nelson, Ft Nelson Municipal Arpt (YYE)","Penticton, Penticton Municipal Arpt (YYF)","Charlottetown, Charlottetown Municipal (YYG)","Rivers, Rivers Rail Station (YYI)","Victoria, Victoria Intl Arpt (YYJ)","Lynn Lake, Lynn Lake Rail Station (YYL)","Swift Current, Swift Current Arpt (YYN)","Churchill, Churchill Arpt (YYQ)","Goose Bay, Goose Bay Municipal Arpt (YYR)","St Johns, St Johns Arpt (YYT)","Kapuskasing, Japuskasing Municipal Arpt (YYU)","Armstromg, Armstrong Rail Station (YYW)","Mont Joli, Mont Joli Arpt (YYY)","Toronto, Lester B Pearson Intl (YYZ)","Ashcroft, Ashcroft Rail Station (YZA)","Yellowknife, Yellowknife Arpt (YZF)","Sandspit, Federal Airport (YZP)","Sarnia, Sarnia Airport (YZR)","Port Hardy, Port Hardy Municipal (YZT)","Sept Iles, Sept Iles Municipal Arpt (YZV)","Zadar, Zadar Arpt (ZAD)","Zagreb, Zagreb Arpt (ZAG)","Valdivia, Pichoy (ZAL)","Cahors, Laberandie Arpt (ZAO)","Nuremberg, Nuremberg Rail Station (ZAQ)","Zaragoza, Zaragoza Airport (ZAZ)","Mulhouse Basel, Basel/Mullhouse Rail Service (ZBA)","Bathurst, Bathurst Arpt (ZBF)","Biloela, Biloela Arpt (ZBL)","Bromont, Bromont Regional Arpt (ZBM)","Odense, Odense Railroad Station (ZBQ)","Eagle, Beaver Creek Van Service (ZBV)","Atibaia, Atibaia Arpt (ZBW)","Zacatecas, Zacatecas Airport (ZCL)","Temuco, Manquehue Arpt (ZCO)","Mulhouse Basel, Basel/Mullhouse SBB Rail Servi (ZDH)","Berna, Berne Railroad Station (ZDJ)","Dundee, Dundee ScotRail (ZDU)","Secunda, Secunda Arpt (ZEC)","Londres, London - Victoria Railway Stat (ZEP)","Chesterfield, Chesterfield Bus Station (ZFI)","Rennes, Gare de Rennes (ZFJ)","Burdeos, Gare de Bordeaux (ZFQ)","Glasgow, Glasgow ScotRail (ZGG)","Copenhague, Copenhagen Rail Station (ZGH)","Gethsemanie, Gethsemanie Arpt (ZGS)","Zhangjiang, Zhanjiang Airport (ZHA)","Houston, Houston Bus Station (ZHO)","Aarhus, Aarhus Bus Service (ZID)","Ixtapa, Zihuatanejo Intl (ZIH)","Odense, Odense Bus Service (ZIM)","Inverness, Inverness ScotRail Station (ZIV)","Swan River, Swan River Municipal Arpt (ZJN)","San Jose, San Jose Bus Service (ZJO)","Kegaska, Kegaska Arpt (ZKG)","Manzanillo, Manzanillo Arpt (ZLO)","Liverpool, Liverpool Street Station (ZLS)","La Tabatiere, La Tabatiere Arpt (ZLT)","Hamburgo, Hamburg Railway Service (ZMB)","Munich, Munich HBF Railway Service (ZMU)","Nanaimo, Harbour Seaplane Base (ZNA)","Newman, Newman Airport (ZNE)","Zanzibar, Kisauni Arpt (ZNZ)","Osorno, Canal Balo Arpt (ZOS)","Queenstown, Frankton Airport (ZQN)","Queen Charlotte Island, Queen Charlotte Island Arpt (ZQS)","Frankfurt, Frankfurt HBF Railway Service (ZRB)","Rockford, Peoria Rockford Bus Terminal (ZRF)","Bratislava, Bratislava Bus Station (ZRG)","Zurich, Zurich Airport (ZRH)","Rockford, Van Galder Bus Terminal (ZRK)","San Salvador, San Salvador Arpt (ZSA)","St Pierre Dela Reunion, St Pierre Dela Reunion Arpt (ZSE)","Sandy Lake, Sandy Lake Arpt (ZSJ)","San Jose, Santa Clara Bus Service (ZSM)","Prince Rupert, Seal Cove Arpt (ZSW)","Tete A La Baleine, Tete A La Baleine Arpt (ZTB)","Zakynthos, Zakinthos Arpt (ZTH)","Zhuhai, Zhuhai Arpt (ZUH)","Hanover, Hanover HBF Railway Service (ZVR)","Stuttgart, Stuttgart Railway Service (ZWS)","Aberdeen, Aberdeen ScotRail Station (ZXA)","Edimburgo, Edinburgh ScotRail (ZXE)","Bodo, Rognan Rail Station (ZXM)","Fauske, Fauske Rail Station (ZXO)","Amsterdam, Amsterdam Central Rail Station (ZYA)","Eindhoven, Eindhoven Rail Station (ZYE)","Sylhet, Sylhet Osmany Arpt (ZYL)","Arnhem, Arnhem Rail Station (ZYM)","Antwerp, Roosendaal Rail Station (ZYO)","Bruselas, Midi Railway Station (ZYR)","Maastricht, Maastricht Rail Station (ZYT)","Utrecht, Utrecht Rail Station (ZYU)","Antwerp, Antwerp Berchem Rail Station (ZYZ)","Zanesville, Zanesville Arpt (ZZV)","Malmo, Malmo City Hvc Arpt (HMA)","Hermosillo, Gen Ignacio Pesqueira Garcia A (HMO)","Hamar, Hamar Arpt (HMR)","Morioka, Hanamaki Arpt (HNA)","Huntingburg, Huntingburg Municipal (HNB)","Tokio, Haneda Arpt (HND)","Hoonah, Hoonah Municipal Arpt (HNH)","Honolulu, Honolulu Intl (HNL)","Hana, Hana Municipal (HNM)","Haines, Haines Municipal Arpt (HNS)","Hengyang, Hengyang Arpt (HNY)","Hobbs, Lea County Arpt (HOB)","Holguin, Frank Pias Arpt (HOG)","Homer, Homer Municipal Arpt (HOM)","Huron, Huron Municipal (HON)","Hopkinsville, Hopkinsville Christian Country (HOP)","Hof De, Hof Pirk Arpt (HOQ)","Horta, Islas Azores, Horta Arpt (HOR)","Hot Springs, Memorial Field (HOT)","Houston, Houston Hobby Arpt (HOU)","Orsta Volda, Hovden Arpt (HOV)","Ha Apai, Salote Pilolevu Arpt (HPA)","Haiphong, Catbi Arpt (HPH)","White Plains, Westchester Cty Arpt (HPN)","Princeville, Princeville Arpt (HPV)","Harbin, Harbin Arpt (HRB)","Harare, Harare Arpt (HRE)","Hurghada, Hurghada Airport (HRG)","Kharkov, Kharkov Arpt (HRK)","Harlingen, Rio Grande Valley Intl Arpt (HRL)","Harrison, Boone County Arpt (HRO)","Harrismith, Harrismith Arpt (HRS)","Harrogate, Linton On Ouse (HRT)","Horizontina, Horizontina Arpt (HRZ)","Saga, Saga Arpt (HSG)","Las Vegas, Henderson Sky Harbor Arpt (HSH)","Hastings, Hastings Municipal (HSI)","Hot Springs, Ingalls Field (HSP)","Homestead, Homestead Municipal (HST)","Huntsville, Huntsville Intl Arpt (HSV)","Hsinchun, Hsinchun Arpt (HSZ)","Chita, Chita Arpt (HTA)","Hatfield, Hatfield Arpt (HTF)","Hamilton Island, Hamilton Island Arpt (HTI)","East Hampton, East Hampton Arpt (HTO)","Huntington, Tri State Milton Arpt (HTS)","Huntsville, Huntsville Arpt (HTV)","Humacao, Humacao Arpt (HUC)","Terre Haute, Hulman Field (HUF)","Huahine, Huahine Arpt (HUH)","Hu PG, Phu Bai Arpt (HUI)","Houma, Terrebonne Arpt (HUM)","Hualien, Hualien Arpt (HUN)","Hutchinson, Hutchinson Municipal (HUT)","Huanuco, Huanuco Arpt (HUU)","Hudiksvall, Hudiksvall Arpt (HUV)","Huatulco, Bahia De Huatulco Arpt (HUX)","Humberside, Humberside Arpt (HUY)","Hervey Bay, Hervey Bay (HVB)","Honningsvag, Valan Arpt (HVG)","New Haven, Tweed New Haven Arpt (HVN)","Havre, City County (HVR)","Hayward, Hayward Air Terminal (HWD)","Hwange, Hwange Arpt (HWN)","Hyannis, Barnstable Cty Arpt (HYA)","Hyderabad, Begumpet Arpt (HYD)","Huangyan, Huangyan Arpt (HYN)","Hayward, Hayward Municipal (HYR)","Hays, Hays Municipal (HYS)","Washington D. C., Washington Dulles Intl (IAD)","Niagara Falls, Niagara Falls Intl Arpt (IAG)","Houston, George Bush Intercontinental (IAH)","Yaroslavl, Yaroslavl Arpt (IAR)","Ibiza, Ibiza Airport (IBZ)","Seul, Incheon Intl Arpt (ICN)","Wichita, Mid Continent Arpt (ICT)","Idaho Falls, Fanning Field (IDA)","Indiana, Indiana County Arpt (IDI)","Zielona, Babimost Arpt (IEG)","Kiev, Zhulhany Arpt (IEV)","Isafjordur, Isafjordur Arpt (IFJ)","Isfahan, Isfahan Arpt (IFN)","Ivano Frankovsk, Ivano Frankovsk Arpt (IFO)","Bullhead City, Laughlin Bullhead Intl Arpt (IFP)","Esmirna, Izmir Cigli Military (IGL)","Kingman, Mohave County (IGM)","Foz Do IguaÃ§u, Cataratas Arpt (IGU)","Jacksonville, Jacksonville Municipal Arpt (IJX)","Kankakee, Greater Kankakee Arpt (IKK)","Irkutsk, Irkutsk Airport (IKT)","Killeen, Killeen Municipal (ILE)","Ilford, Ilford Rail Station (ILF)","Wilmington, Greater Wilmington New Castle (ILG)","Wilmington, New Hanover Cty Arpt (ILM)","Ilo, Mandurriao Arpt (ILO)","Ile Des Pins, Ile Des Pins Arpt (ILP)","Islay, Islay Arpt (ILY)","Imperatriz, Imperatriz Arpt (IMP)","Iron Mountain, Ford Arpt (IMT)","Indianappolis, Indianapolis Intl Arpt (IND)","Nis Yu, Nis Arpt (INI)","International Falls, Intl Falls Arpt (INL)","Innsbruck, Kranebitten Airport (INN)","Winston-Salem, Smith Reynolds Arpt (INT)","Nauru Island, Nauru Intl Arpt (INU)","Inverness, Inverness Arpt (INV)","Winslow, Winslow Municipal (INW)","Ioannina, Ioannina Arpt (IOA)","Isle Of Man, Ronaldsway Arpt (IOM)","Ilheus, Eduardo Gomes Airport (IOS)","Isla De Pascua, Mataveri Intl Arpt (IPC)","Ipoh, Ipoh Arpt (IPH)","Ipiales, San Luis Arpt (IPI)","El Centro, Imperial County (IPL)","Ipatinga, Usiminas Arpt (IPN)","Williamsport, Williamsport Lycoming Municipa (IPT)","Ipswich, Ipswitch Rail Station (IPW)","Iquique, Cavancha Chucumata Arpt (IQQ)","Iquitos, C F Secada Arpt (IQT)","Kirksville, Kirksville Municipal (IRK)","Sturgis, Kirsch Municipal (IRS)","Mount Isa, Mount Isa Arpt (ISA)","Islamabad, Islamabad Intl (ISB)","Isles Of Scilly, St Marys Arpt (ISC)","Ishigaki, Ishigaki Airport (ISG)","Ischia, Ischia Arpt (ISH)","Isla Mujeres, Isla Mujeres Arpt (ISJ)","Kissimmee, Kissimmee Municipal Arpt (ISM)","Williston, Sloulin Field Intl Arpt (ISN)","Kinston, East Reg Jetport Stallings (ISO)","Islip, Long Island Macarthur Arpt (ISP)","Manistique, Schoolcraft County Arpt (ISQ)","Wiscasset, Wiscasset Arpt (ISS)","Estambul, Ataturk Arpt (IST)","Ithaca, Tomkins County (ITH)","Osaka, Itami Arpt (ITM)","Hilo, Hilo Hawaii Intl (ITO)","Itaperuna, Itaperuna Arpt (ITP)","Niue Island, Hanan Arpt (IUE)","Invercargill, Invercargill Arpt (IVC)","Ivalo, Ivalo Arpt (IVL)","Inverell, Inverell Arpt (IVR)","Ivanovo, Ivanovo Arpt (IWA)","Ironwood, Ironwood Arpt (IWD)","Iwami, Iwami Arpt (IWJ)","Houston, West Houston (IWS)","Bagdogra, Bagdogra Arpt (IXB)","Mangalore, Bajpe Arpt (IXE)","Jammu, Satwari Arpt (IXJ)","Madurai, Madurai Airport (IXM)","Aurangabad, Chikkalthana Arpt (IXU)","Port Blair, Port Blair Arpt (IXZ)","Inyokern, Kern Cty Airport (IYK)","Izumo, Izumo Arpt (IZO)","Ixtepec, Ixtepec Arpt (IZT)","Jabiru, Jabiru Arpt (JAB)","Jackson, Jackson Arpt (JAC)","Jaipur, Sanganeer Arpt (JAI)","Atlanta, Perimeter Hlpt (JAJ)","Xalapa, Jalapa Arpt (JAL)","Jackson, Jackson Airport (JAN)","Atlanta, Beaver Ruin Helpt (JAO)","Puntarenas, Punta Renes Arpt (JAP)","Ilulissat, Ilulissat Arpt (JAV)","Jacksonville, Jacksonville Intl Arpt (JAX)","Los Angeles, Commerce Business Plaza Helipo (JBP)","Jonesboro, Jonesboro Municipal (JBR)","Pleasanton, Hacienda Business Park Hlpt (JBS)","Cannes, Croisette Heliport (JCA)","San Francisco, China Basin Hlpt (JCC)","Oakland, Oakland Conv Ctr Hlpt (JCE)","Jeju City, Chuja Heliport (JCJ)","Juiz De Fora, Francisco De Assis Arpt (JDF)","Jodhpur, Jodhpur Arpt (JDH)","Juazeiro Do Norte, Regional Do Cariri Arpt (JDO)","Paris, Issy Les Moulineaux Arpt (JDP)","Jeddah, Jeddah Intl (JED)","Jefferson City, Jefferson City Memorial (JEF)","Aasiaat, Aasiaat Arpt (JEG)","Jersey, States Airport (JER)","Nueva York, John F Kennedy Intl (JFK)","Fremantle, Fremantle Heliport (JFM)","Jamnagar, Govardhanpur Arpt (JGA)","Groennedal, Groennedal Heliport (JGR)","Burbank, Heliport (JGX)","Johor Bahru, Sultan Ismail Intl Arpt (JHB)","Helsingborg, Helsingborg Heliport (JHE)","Jinghong, Jinghong Arpt (JHG)","Kapalua, Kapalua Arpt (JHM)","Shute Hrb, Shute Harbour Heliport (JHQ)","Sisimiut, Sisimiut Arpt (JHS)","Jamestown, Chautauqua Cty Arpt (JHW)","Yibuti, Ambouli Airport (JIB)","Ikaria Island, Ikaria Arpt (JIK)","Jimma, Jimma Arpt (JIM)","Jiujiang, Jiujiang Airport (JIU)","Qaqortoq, Qaqortoq Arpt (JJU)","Jonkoping, Axamo Airport (JKG)","Chios, Chios Arpt (JKH)","Jacksonville, Cherokee County Arpt (JKV)","Landskrona, Landskrona Heliport (JLD)","Joplin, Joplin Municipal Arpt (JLN)","Sausalito, Marin County Arpt (JMC)","Chicago, Marriott Heliport (JMH)","Mykonos, Mykonos Greece Arpt (JMK)","Malmo, Malmo Harbour Heliport (JMM)","Jamestown, Jamestown Arpt (JMS)","Johannesburgo, Johannesburg International (JNB)","Santa Ana, Newport Beach Heliport (JNP)","Narsaq, Narsaq Heleport (JNS)","Juneau, Juneau Arpt (JNU)","Naxos, Naxos Arpt (JNX)","Jinzhou, Jinzhou Arpt (JNZ)","Joensuu, Joensuu Arpt (JOE)","Yogjakarta, Adisutjipto Arpt (JOG)","Joinville, Cubatao Arpt (JOI)","Santa Ana, Santa Ana Heliport (JOR)","Joao Pessoa, Castro Pinto Arpt (JPA)","Pasadena, Burbank Heliport (JPD)","Ji Parana, Ji Parana Arpt (JPR)","Nueva York, East 60th St Hlpt (JRE)","Kilimanjaro, Kilimanjaro Arpt (JRO)","Jerusalem, Atarot Airport (JRS)","Skiathos, Skiathos Arpt (JSI)","Jeju City, Seogwipo Heliport (JSP)","Johnstown, Johnstown Cambria Arpt (JST)","Maniitsoq, Maniitsoq Heleport (JSU)","Syros Island, Syros Island Arpt (JSY)","St Tropez, St Tropez Hlpt (JSZ)","Los Angeles, Thousand Oaks Hlpt (JTO)","Santorini Thira Island, Santorini Arpt (JTR)","Juliaca, Juliaca Arpt (JUL)","Jurado, Jurado Arpt (JUO)","Beloit, Rock County Arpt (JVL)","Zanjan, Zanjan Arpt (JWN)","Jackson, Jackson Reynolds Municipal (JXN)","Jyvaskyla, Jyvaskyla Arpt (JYV)","Kariba Dam, Kariba Arpt (KAB)","Kake, Kake Arpt (KAE)","Kajaani, Kajaani Airport (KAJ)","Kano, Aminu Kano Intl Arpt (KAN)","Kuusamo, Kuusamo (KAO)","Kaitaia, Kaitaia Arpt (KAT)","Kings Canyon, Kings Canyon Arpt (KBJ)","Kiev, Borispol Arpt (KBP)","Kota Bharu, Pengkalan Chepa (KBR)","Krabi, Krabi Arpt (KBV)","Collinsville, Collinsville Arpt (KCE)","Kuching, Kuching Airport (KCH)","Kansas City, Fairfax Municipal Arpt (KCK)","Kochi, Kochi Airport (KCZ)","Kodiak, Kodiak Municipal (KDK)","Reykjavyk, Keflavik Intl Arpt (KEF)","Seattle, Kenmore Air Harbor (KEH)","Kiel, Holtenau Arpt (KEL)","Kemi, Kemi Tornio Arpt (KEM)","Kerman, Kerman Arpt (KER)","Kingscote, Kingscote Arpt (KGC)","Kaliningrad, Kaliningrad Arpt (KGD)","Kalgoorlie, Kalgoorlie Arpt (KGI)","Kigali, Kayibanda Arpt (KGL)","Kos, Kos Arpt (KGS)","Khorramabad, Khorramabad Arpt (KHD)","Kaohsiung, Kaohsiung Intl (KHH)","Karachi, Quaid E Azam International (KHI)","Nanchang, Nanchang Airport (KHN)","Khabarovsk, Novyy Arpt (KHV)","Kristianstad, Kristianstad Arpt (KID)","Niigata, Niigata Arpt (KIJ)","Kimberley, Kimberley Arpt (KIM)","Kingston, Norman Manly Arpt (KIN)","Kerry County, Kerry County Arpt (KIR)","Kisumu, Kisumu Arpt (KIS)","Chisinau, Chisinau Arpt (KIV)","Osaka, Kansai International Arpt (KIX)","Krasnoyarsk, Krasnojarsk Arpt (KJA)","Kortrijk, Kortrijk Arpt (KJK)","Khon Kaen, Khon Kaen Arpt (KKC)","Kerikeri, Kerikeri Arpt (KKE)","Kita Kyushu, Kokura Arpt (KKJ)","Kirkenes, Hoeybuktmoen Arpt (KKN)","Kentland, Kentland Arpt (KKT)","Kalskag, Kalskag Municipal Arpt (KLG)","Klaipeda, Klaipeda Arpt (KLJ)","Kalibo, Kalibo Arpt (KLO)","Kalmar, Kalmar Arpt (KLR)","Kaiserslautern, Kaiserslautern Arpt (KLT)","Klagenfurt, Klagenfurt Arpt (KLU)","Karlovy Vary, Karlovy Vary Arpt (KLV)","Klawock, Klawock Arpt (KLW)","Kalamata, Kalamata Airport (KLX)","King Khalid Military City, King Khalid Military Airport (KMC)","Kunming, Kunming Airport (KMG)","Miyazaki, Miyazaki Arpt (KMI)","Kumamoto, Kumamoto Airport (KMJ)","Komatsu, Komatsu Airport (KMQ)","Kostroma, Kostroma Arpt (KMW)","Khamis Mushait, Khamis Mushait Arpt (KMX)","Vina Del Mar, Vina Del Mar Arpt (KNA)","Kings Lynn, Marham Raf (KNF)","Kinmen, Shang Yi Arpt (KNH)","King Island, King Island Arpt (KNS)","Kennett, Kennett Municipal Arpt (KNT)","Kununurra, Kununurra Airport (KNX)","Hawaiian Big Island, Keahole Arpt (KOA)","Koutaba, Koutaba Arpt (KOB)","Koumac, Koumac Arpt (KOC)","Kirkwall, Kirkwall Arpt (KOI)","Kagoshima, Kagoshima Arpt (KOJ)","Kokkola, Kokkola Arpt (KOK)","Nakhon Phanom, Nakhon Phanom Arpt (KOP)","Kokoro, Kokoro Arpt (KOR)","King Of Prussia, King Of Prussia Arpt (KPD)","Pohang, Pohang Arpt (KPO)","Kempsey, Kempsey Arpt (KPS)","Kramfors, Kramfors Arpt (KRF)","Krakow, John Paul 11 Balice Intl (KRK)","Kiruna, Kiruna Airport (KRN)","Karup, Karup Arpt (KRP)","Krasnodar, Krasnodar Arpt (KRR)","Kristiansand, Kjevik Airport (KRS)","Khartoum, Civil Arpt (KRT)","Turkmanbashi, Turkmanbashi Arpt (KRW)","Kosice, Barca Arpt (KSC)","Karlstad, Karlstad Arpt (KSD)","Kassel, Kassel Calden Arpt (KSF)","Kermanshah, Kermanshah Arpt (KSH)","Kastoria, Aristoteles Arpt (KSO)","Kristiansund, Kvernberget Arpt (KSU)","Kiryat Shmona, Kiryat Shmona Arpt (KSW)","Karratha, Karratha Airport (KTA)","Kerteh, Kertech Arpt (KTE)","Katmandu, Tribuvan Arpt (KTM)","Ketchikan, Ketchikan Intl Arpt (KTN)","Kingston, Tinson Arpt (KTP)","Katherine, Tindal Airport (KTR)","Brevig Mission, Brevig Mission Arpt (KTS)","Kittila, Kittila Arpt (KTT)","Katowice, Pyrzowice (KTW)","Kuantan, Kuantan Arpt (KUA)","Kuala Belait, Kuala Belait Arpt (KUB)","Samara, Samara Arpt (KUF)","Kushiro, Kushiro Arpt (KUH)","Kuala Lumpur, Kuala Lumpur International Arp (KUL)","Kaunas, Kaunas Arpt (KUN)","Kuopio, Kuopio Arpt (KUO)","Kulu, Bhuntar Arpt (KUU)","Kunsan, Kunsan Arpt (KUV)","Kavalla, Megas Alexandros (KVA)","Skovde, Skovde Arpt (KVB)","Kavieng, Kavieng Arpt (KVG)","Kwajalein, Kwajalein Arpt (KWA)","Guiyang, Guiyang Arpt (KWE)","Kuwait, Kuwait Intl (KWI)","Kwangju, Kwangju Arpt (KWJ)","Guilin, Guilin Airport (KWL)","Konya, Konya Arpt (KYA)","Key Largo, Port Largo Arpt (KYL)","Milton Keynes, Milton Keynes Rail Station (KYN)","Kazan, Kazan Arpt (KZN)","Lamar, Lamar Arpt (LAA)","Luanda, Four De Fevereiro Arpt (LAD)","Lae Pg, Nadzab Arpt (LAE)","Lafayette, Lafayette Arpt (LAF)","Lannion, Servel Airport (LAI)","Lajes, Lages Arpt (LAJ)","Lakeland, Lakeland Arpt (LAL)","Los Alamos, Los Alamos Municipal (LAM)","Lansing, Lansing Arpt (LAN)","La Paz, Aeropuerto Gen Marquez De Leon (LAP)","Laramie, General Brees Fld (LAR)","Las Vegas, McCarran Intl (LAS)","Lawton, Lawton Municipal (LAW)","Los Angeles, Los Angeles Intl (LAX)","Ladysmith, Ladysmith Arpt (LAY)","Leeds, Leeds Bradford Arpt (LBA)","Lubbock, Lubbock Intl (LBB)","Luebeck, Blankensee Arpt (LBC)","Latrobe, Westmorland County (LBE)","North Platte, Lee Bird Field (LBF)","Paris, Le Bourget Arpt (LBG)","Sydney, Palm Beach Arpt (LBH)","Albi, Le Sequestre Arpt (LBI)","Liberal, Liberal Municipal (LBL)","Labasa, Labasa Arpt (LBS)","Lumberton, Lumberton Arpt (LBT)","Labuan, Labuan Arpt (LBU)","Libreville, Libreville Arpt (LBV)","La Baule, Montoir Arpt (LBY)","Larnaca, Larnaca Intl (LCA)","Lecce, Galatina Arpt (LCC)","La Ceiba, Goloson Intl Arpt (LCE)","Rio Dulce, Las Vegas Airport (LCF)","La CoruÃ±a, La Coruna Arpt (LCG)","Lake Charles, Lake Charles Municipal (LCH)","Laconia, Laconia Municipal Arpt (LCI)","Lodz, Lodz Lublinek Arpt (LCJ)","Columbus, Rickenbacker Intl Arpt (LCK)","Londres, London City Arpt (LCY)","Londrina, Londrina Arpt (LDB)","Lourdes, Tarbes Intl Arpt (LDE)","Lord Howe Island, Lord Howe Island Arpt (LDH)","Linden, Linden Municipal (LDJ)","Lidkoping, Hovby Airport (LDK)","Lahad Datu, Lahad Datu Arpt (LDU)","Londonderry, Eglinton Arpt (LDY)","Learmonth, Learmonth Arpt (LEA)","Lebanon, Lebanon Regional (LEB)","Lencois, Chapada Diamantina Arpt (LEC)","St Petersburg, Pulkovo Airport (LED)","Leesburg, Leesburg Municipal Arpt (LEE)","Le Havre, Octeville Arpt (LEH)","Almeira, Almeria Arpt (LEI)","Leipzig, Leipzig Arpt (LEJ)","Leon, Leon Arpt (LEN)","Lands End, St Just Arpt (LEQ)","Leinster, Leinster Arpt (LER)","Lesobeng, Lesobeng Arpt (LES)","Leticia, Alfredo VÃ¡squez Cobo Arpt (LET)","Lewiston, Auburn Lewiston Arpt (LEW)","Lexington, Blue Grass Field (LEX)","Lelystad, Lelystad Arpt (LEY)","Lufkin, Angelina Cty Arpt (LFK)","Lafayette, Lafayette Municipal (LFT)","Lome, Lome Airport (LFW)","Nueva York, La Guardia (LGA)","Long Beach, Long Beach Municipal (LGB)","Liege, Bierset Airport (LGG)","Langkawi, Langkawi Arpt (LGK)","Logan, Logan Arpt (LGU)","Londres, Gatwick Arpt (LGW)","Lahr, Lahr Arpt (LHA)","Lahore, Lahore Arpt (LHE)","Lightning Ridge, Lightning Ridge Arpt (LHG)","Londres, Heathrow (LHR)","Lianping, Lianping Arpt (LIA)","Lifou, Lifou Arpt (LIF)","Limoges, Bellegarde Arpt (LIG)","Lihue, Lihue Municipal (LIH)","Lille, Lesquin Arpt (LIL)","Lima, Jorge Chavez International Arp (LIM)","Milan, Linate Arpt (LIN)","Puerto Limon, Limon Arpt (LIO)","Liberia, Liberia Arpt (LIR)","Lisboa, Lisboa (LIS)","Little Rock, Little Rock Regional (LIT)","Lijiang City, Lijiang Arpt (LJG)","Lake Jackson, Lake Jackson Arpt (LJN)","Ljubljana, Brnik Airport (LJU)","Seattle, Lake Union Seaplane Base (LKE)","Lokichoggio, Lokichoggio Arpt (LKG)","Long Akah, Long Akah Arpt (LKH)","Lakselv, Banak Airport (LKL)","Leknes, Leknes Arpt (LKN)","Lucknow, Amausi Arpt (LKO)","Lulea, Kallax Airport (LLA)","Malelane, Municipal Arpt (LLE)","Ling Ling, Ling Ling Arpt (LLF)","Alluitsup Paa, Alluitsup Paa Arpt (LLU)","Lilongwe, Lilongwe Intl Arpt (LLW)","Mount Holly, Burlington Country Arpt (LLY)","Le Mans, Arnage Arpt (LME)","Los Mochis, Federal Los Mochis Arpt (LMM)","Limbang, Limbang Arpt (LMN)","Klamath Falls, Kingsley Field (LMT)","West Palm Beach, Palm Beach County Arpt (LNA)","Lincoln, Lincoln Municipal (LNK)","Leonora, Leonora Arpt (LNO)","Lancaster, Lancaster Arpt (LNS)","Lihir Island, Lihir Island Arpt (LNV)","Lanai, Lanai Airport (LNY)","Linz, Hoersching Arpt (LNZ)","Lagos De Moreno, Francisco P V Y R (LOM)","Lagos, Murtala Muhammed Arpt (LOS)","Monclova, Monclova Arpt (LOV)","London, Corbin London Arpt (LOZ)","Gran Canaria Islas Canarias, Aeropuerto De Gran Canaria (LPA)","La Paz, El Alto Arpt (LPB)","Santa Maria, Lompoc Arpt (LPC)","Linkoping, Saab Airport (LPI)","Liverpool, Liverpool Arpt (LPL)","La Porte, La Porte Arpt (LPO)","Lappeenranta, Lappeenranta Arpt (LPP)","Lopez Island, Lopez Island Arpt (LPS)","Lampang, Lampang Arpt (LPT)","Liepaja, Liepaya Arpt (LPX)","Le Puy, Loudes Airport (LPY)","Laredo, Laredo Intl (LRD)","Longreach, Longreach Arpt (LRE)","La Rochelle, Laleu Airport (LRH)","La Romana, La Romana Arpt (LRM)","Leros, Leros Arpt (LRS)","Lorient, Lann Bihoue Arpt (LRT)","Las Cruces, Las Cruces Crawford Arpt (LRU)","La Serena, La Florida (LSC)","La Crosse, La Crosse Municipal (LSE)","Lerwick, Sumburgh Airport (LSI)","Les Sables, Talmont Arpt (LSO)","Las Piedras, Josefa Camejo Arpt (LSP)","Los Angeles, Maria Dolores Arpt (LSQ)","Launceston, Launceston Arpt (LST)","Lismore, Lismore Airport (LSY)","Tzaneen, Letaba Aprt (LTA)","Latakia, Hmelmin Airport (LTK)","Luton, Luton Arpt (LTN)","Loreto, Loreto Intl Arpt (LTO)","Le Touquet, Le Touquet Arpt (LTQ)","St Tropez, La Mole Arpt (LTT)","Lugano, Agno Airport (LUG)","Cincinati, Cincinnati Municipal Arpt (LUK)","Laurel, Hesler Noble Field (LUL)","Lusaka, Lusaka Airport (LUN)","Kalaupapa, Kalaupapa Municipal (LUP)","Luxemburgo, Luxembourg Arpt (LUX)","Livingstone, Livingstone Arpt (LVI)","Livermore, Bron Airport (LVK)","Livingston, Mission Field (LVM)","Laverton, Laverton Airport (LVO)","Greenbrier, Greenbrier Valley Arpt (LWB)","Lawrence, Lawrence Municipal (LWC)","Lerwick, Tingwall Arpt (LWK)","Lawrence, Lawrence Arpt (LWM)","Lvov, Snilow Arpt (LWO)","Lewiston, Lewiston Nez Pierce Arpt (LWS)","Lewistown, Lewistown Municipal (LWT)","Lawas, Lawas Arpt (LWY)","Lexington, Jim Kelley Field (LXN)","Luxor, Luxor Airport (LXR)","Limnos, Limnos Airport (LXS)","Little Cayman, Little Cayman Arpt (LYB)","Lycksele, Hedlunda Arpt (LYC)","Lynchburg, Lynchburg Municipal (LYH)","Lyon, Bron Arpt (LYN)","Longyearbyen, Svalbard Arpt (LYR)","Lyon, Lyon Saint Exupery Arpt (LYS)","Ely Mn, Ely Municipal Arpt (LYU)","Lydd, Lydd Intl Arpt (LYX)","Lazaro Cardenas, Lazaro Cardenas Arpt (LZC)","Liuzhou, Liuzhou Airport (LZH)","Madras, Chennai Arpt (MAA)","Maraba, Maraba Arpt (MAB)","Madrid, Barajas Arpt (MAD)","Madera, Madera Municipal Arpt (MAE)","Midland, Midland Intl Arpt (MAF)","Madang, Madang Arpt (MAG)","Menorca, Aerop De Menorca (MAH)","Majuro, Amata Kabua Intl Arpt (MAJ)","Matamoros, Servando Canales Arpt (MAM)","Manchester, Manchester Intl (MAN)","Manaos, Intl Arpt Eduardo Gomes (MAO)","Mae Sot, Mae Sot Arpt (MAQ)","Maracaibo, La Chinita Arpt (MAR)","Manus Island, Momote Arpt (MAS)","Mayaguez, Eugenio M De Hostos Arpt (MAZ)","Mombasa, Moi Intl (MBA)","Mmabatho, Mmabatho International Arpt (MBD)","Maryborough, Maryborough Arpt (MBH)","Montego Bay, Sangster Arpt (MBJ)","Manistee, Manistee Arpt (MBL)","Saginaw, Saginaw Arpt (MBS)","Moorabbin, Moorabbin Arpt (MBW)","Maribor, Maribor Arpt (MBX)","Merced, Merced Municipal Arpt (MCE)","Kansas, Kansas City Intl (MCI)","McCook, McCook Municipal (MCK)","Monte Carlo, Hel De Monte Carlo Airport (MCM)","Macon, Lewis B Wilson (MCN)","Orlando, Orlando Intl Arpt (MCO)","Macapa, Macapa Intl Arpt (MCP)","Muscat, Seeb Intl (MCT)","Montlucon, Gueret Arpt (MCU)","Mason City, Mason City Municipal (MCW)","Maroochydore, Maroochydore Arpt (MCY)","Maceio, Palmeres Airport (MCZ)","Menado, Samratulang Arpt (MDC)","MedellÃ­n, JosÃ© MarÃ­a Cordova (MDE)","Mudanjiang, Mudanjiang Arpt (MDG)","Carbondale, Southern Illinois Arpt (MDH)","Mandalay, Annisaton Arpt (MDL)","Harrisburg, Harrisburg Intl (MDT)","Mendi, Mendi Arpt (MDU)","Chicago Midway Apt, Midway Apt (MDW)","Midway Island, Sand Island Field (MDY)","Macae, Macae Airport (MEA)","Melbourne, Essendon Arpt (MEB)","Manta, Manta Arpt (MEC)","Madinah, Prince Mohammad Bin Abdulaziz  (MED)","Mehamn, Mehamn Arpt (MEH)","Meridian, Key Field (MEI)","Melbourne, Tullamarine Arpt (MEL)","Memphis, Memphis Intl (MEM)","Medan, Polonia Airport (MES)","Monte Dourado, Monte Dourado Arpt (MEU)","Minden, Douglas County Arpt (MEV)","Ciudad de Mexico, Juarez Intl (MEX)","Mansfield, Mansfield Municipal (MFD)","McAllen, Miller Intl (MFE)","Mesquite, Mesquite Arpt (MFH)","Marshfield, Marshfield Municipal Arpt (MFI)","Matsu, Matsu Arpt (MFK)","Macau, Macau Arpt (MFM)","Milford Sound, Milford Sound Arpt (MFN)","Medford, Medford Jackson Cty (MFR)","Managua, Augusto C Sandino (MGA)","Mt Gambier, Mount Gambier Arpt (MGB)","Michigan City, Michigan City Arpt (MGC)","Marietta, Dobbins Air Force Base (MGE)","Maringa, Maringa Arpt (MGF)","Mannheim, Margate Arpt (MGH)","Dusseldorf, Moenchen Gi Dus Exp (MGL)","Montgomery, Dannelly Field (MGM)","Moultrie, Moultrie Arpt (MGR)","Margaret River Station, Margaret River Station Arpt (MGV)","Morgantown, Morgantown Municipal (MGW)","Mashad, Mashad Arpt (MHD)","Mitchell, Mitchell Municipal (MHE)","Mannheim Germany, Mannheim Neuostheim (MHG)","Marsh Harbour, Marsh Harbour Intl Arpt (MHH)","Manhattan, Manhattan Municipal Arpt (MHK)","Minsk, Minsk Intl 2 (MHP)","Mariehamn, Mariehamn Arpt (MHQ)","Sacramento, Mather Air Force Base (MHR)","Manchester, Manchester Arpt (MHT)","Mount Hotham, Mount Hotham Arpt (MHU)","Mojave, Kern County Arpt (MHV)","Mildenhall, Mildenhall Arpt (MHZ)","Miami, Miami Intl (MIA)","Merida, Merida Intl (MID)","Muncie, Johnson Field (MIE)","Mian Yang, Mian Yang Arpt (MIG)","Marilia, Dr Gastao Vidigal (MII)","Mikkeli, Mikkeli Aprt (MIK)","Merimbula, Merimbula Arpt (MIM)","Omaha, Millard Airport (MIQ)","Monastir, Habib Bourguiba Intl (MIR)","Millville, Millville Arpt (MIV)","Marshalltown, Marshalltown Municipal (MIW)","Mohenjodaro, Mohenjodaro Arpt (MJD)","Mosjoen, Kjaerstad Arpt (MJF)","Majunga, Amborovy Arpt (MJN)","Mytilene, Mytilene Arpt (MJT)","Murcia, San Javier Airport (MJV)","Mahenye, Mahenye Airfield (MJW)","Marianske Lazne, Marianske Lazne Arpt (MKA)","Kansas, Kansas City Municipal Arpt (MKC)","Milwaukee, General Mitchell Intl Arpt (MKE)","Muskegon, Muskegon Cty Intl (MKG)","Hoolehua, Hoolehua Municipal (MKK)","Jackson, McKellar Fld (MKL)","Mukah, Mukah Arpt (MKM)","Muskogee, Davis Field (MKO)","Meekathara, Meekatharra Arpt (MKR)","Mankato, Mankato Municipal Arpt (MKT)","Mackay, Mackay Arpt (MKY)","Malacca, Batu Berendum Arpt (MKZ)","Malta, Luqa Airport (MLA)","Melbourne, Melbourne Regional (MLB)","McAlester, McAlester Municipal Arpt (MLC)","Maldivas, Male Intl Arpt (MLE)","Mulhouse, Euroairport French (MLH)","Moline, Quad City Arpt (MLI)","Morelia, Michoacan Municipal Arpt (MLM)","Melilla, Melilla Arpt (MLN)","Miles City, Miles City Municipal Arpt (MLS)","Monroe, Monroe Regional (MLU)","Monrovia, Sprigg Payne Arpt (MLW)","Malmo, Malmo Metropolitan Area Arpt (MMA)","Memambetsu, Memanbetsu Arpt (MMB)","Ciudad Mante, Ciudad Mante Arpt (MMC)","Teesside, Teesside Arpt (MME)","Mount Magnet, Mount Magnet Arpt (MMG)","Mammoth Lakes, Mammoth Lakes Municipal (MMH)","Matsumoto, Matsumoto Arpt (MMJ)","Murmansk, Murmansk Arpt (MMK)","Marshall, Marshall Municipal (MML)","Middlemount, Middlemount Arpt (MMM)","Maio, Vila Do Maio Arpt (MMO)","Morristown, Morristown Arpt (MMU)","Malmo, Sturup Arpt (MMX)","Miyako Jima, Hirara Arpt (MMY)","Montserrat, Bramble Arpt (MNI)","Maiana, Maiana Airport (MNK)","Manila, Ninoy Aquino Intl (MNL)","Menominee, Menominee County (MNM)","Monto, Monto Arpt (MNQ)","Mobile, Mobile Municipal (MOB)","Montes Claros, Montes Claros Arpt (MOC)","Modesto, Harry Sham Fld (MOD)","Molde, Aro Arpt (MOL)","Mount Cook, Mount Cook Arpt (MON)","Mount Pleasant, Mt Pleasant Municipal (MOP)","Morondava, Morondava Arpt (MOQ)","Minot, Minot Intl (MOT)","Mountain Village, Mountain Village Arpt (MOU)","Moorea, Temae Airport (MOZ)","Miami, Mpb Seaplane Base (MPB)","Montpellier, Frejorgues Arpt (MPL)","Maputo, Maputo Intl (MPM)","Mount Pleasant, Mount Pleasant Arpt (MPN)","Maan, Maan Arpt (MPQ)","McPherson, McPherson Arpt (MPR)","Montpelier, E F Knapp Arpt (MPV)","Balikesir, Merkez Arpt (MQJ)","San Matias, San Matias Arpt (MQK)","Mildura, Mildura Arpt (MQL)","Mo I Rana, Rossvoll (MQN)","Marquette, Sawyer Intl Airport (MQT)","Smyrna, Smyrna Arpt (MQY)","Margaret River, Margaret River Arpt (MQZ)","Martinsburgh, Martinsburgh Regional Arpt (MRB)","Columbia, Maury Country Arpt (MRC)","Merida, Alberto Carnevalli Arpt (MRD)","Mara Lodges, Mara Lodges Arpt (MRE)","Masterton, Masterton Arpt (MRO)","Marsella, Marseille Provence Arpt (MRS)","Mauricio, Plaisance Arptt (MRU)","Mineralnye Vody, Mineralnye Vody Arpt (MRV)","Monterey, Monterey Peninsula (MRY)","Moree, Moree Arpt (MRZ)","Mesa, Falcon Field (MSC)","Manston, Kent International Arpt (MSE)","Misawa, Misawa Arpt (MSJ)","Muscle Shoals, Muscle Shoals Arpt (MSL)","Madison, Dane County Regional (MSN)","Missoula, Missoula Intl (MSO)","Minneapolis, Minneapolis St Paul Intl (MSP)","Minsk, Minsk Intl 1 (MSQ)","Mus Turkey, Mus Arpt (MSR)","Massena, Richards Field (MSS)","Maastricht, Maastricht Aachen Arpt (MST)","Maseru, Moshoeshoe Intl Arpt (MSU)","Monticello, Catskills Sulivan (MSV)","New Orleans, Louis Armstrong Intl Arpt (MSY)","Detroit, Selfridge Air Natl Guard (MTC)","Marathon, Marathon Arpt (MTH)","Montrose, Montrose County (MTJ)","Maitland, Maitland Airport (MTL)","Mattoon, Coles County (MTO)","Montauk, Sky Portal Arpt (MTP)","MonterÃ­a, Los Garzones Arpt (MTR)","Manzini, Matsapha Intl Arpt (MTS)","Minatitlan, Minatitlan Municipal Arpt (MTT)","Manitowoc, Manitowoc Municipal Arpt (MTW)","Monterrey, Escobedo Arpt (MTY)","Maun, Maun Arpt (MUB)","Munich, Munich Intl Arpt (MUC)","Kamuela, Waimea Arpt (MUE)","Maturin, Quiriquire Arpt (MUN)","Marudi, Marudi Arpt (MUR)","Franceville Mvengue, Franceville Mvengue Arpt (MVB)","Monroeville, Monroe County Arpt (MVC)","Montevideo, Carrasco Arpt (MVD)","Mossoro, Dixsept Rosado Arpt (MVF)","Mt Vernon, Mt Vernon Outland Arpt (MVN)","Mogilev, Mogilev Arpt (MVQ)","Maroua, Salam Arpt (MVR)","Martha S Vineyard, Dukes County (MVY)","Masvingo, Ft Victoria Arpt (MVZ)","Marion, Williamson County (MWA)","Moses Lake, Grant County (MWH)","Middletown, Hook Field (MWO)","Mwanza, Mwanza Arpt (MWZ)","Mexicali, Rodolfg Sachez Taboada (MXL)","Morlaix, Morlaix Arpt (MXN)","Milan, Malpensa Arpt (MXP)","Maota, Maota Arpt (MXS)","Mora, Mora Arpt (MXX)","Meixian, Meixian Arpt (MXZ)","Moruya, Moruya Arpt (MYA)","Maracay, Maracay Arpt (MYC)","Malindi, Malindi Arpt (MYD)","San Diego, Montgomery Field (MYF)","Matsuyama, Matsuyama Airport (MYJ)","Mccall, Mccall Arpt (MYL)","Mary, Mary Arpt (MYP)","Mysore, Mysore Airport (MYQ)","Myrtle Beach, Myrtle Beach Jetway (MYR)","Marysville, Yuba County Arpt (MYV)","Mtwara, Mtwara Arpt (MYW)","Miri, Miri Arpt (MYY)","Makung, Makung Arpt (MZG)","Mopti, Mopti Airport (MZI)","Metz, Frescaty Airport (MZM)","Manzanillo, Sierra Maestra Arpt (MZO)","Mazatlan, Buelina Arpt (MZT)","Narrabri, Narrabri Arpt (NAA)","Nagpur, Sonegaon Arpt (NAG)","Nakhon Ratchasima, Nakhon Ratchasima Arpt (NAK)","Nadi, Nadi Intl (NAN)","Napoles, Capodichino Arpt (NAP)","Nassau, Nassau Intl (NAS)","Natal, Augusto Severo Intl Arpt (NAT)","Nevsehir, Nevsehir Arpt (NAV)","Narathiwat, Narathiwat Arpt (NAW)","Naberevnye Chelny, Nijnekamsk Arpt (NBC)","Nairobi, Jomo Kenyatta Intl (NBO)","North Caicos, North Caicos Municipal Arpt (NCA)","Niza, Cote D Azur Arpt (NCE)","Newcastle, Newcastle Arpt (NCL)","Newcastle, Newcastle Intl Arpt (NCS)","Annecy, Annecy Meythet Arpt (NCY)","Nouadhibou, Nouadhibou Arpt (NDB)","Qiqihar, Qiqihar Arpt (NDG)","NÂ´Djamena, N Djamena Arpt (NDJ)","Nador, Nador Arpt (NDR)","Sanday, Sanday Arpt (NDY)","Nevis, Nevis Airport (NEV)","New Orleans, New Lakefront Arpt (NEW)","Young, Young Arpt (NGA)","Ningbo, Ningbo Arpt (NGB)","N Gaoundere, N Gaoundere Arpt (NGE)","Nagoya, Komaki Arpt (NGO)","Nagasaki, Nagasaki Airport (NGS)","Nha Trang, Nha Trang Arpt (NHA)","Brunswick, Naval Air Station (NHZ)","Niamey, Niamey Airport (NIM)","Jacksonville, Jacksonville Nas (NIP)","Nizhnevartovsk, Nizhnevartovsk Arpt (NJC)","Nouakchott, Nouakchott Arpt (NKC)","Nanjing, Nanjing Airport (NKG)","N Dola, Ndola Arpt (NLA)","Nuevo Laredo, Quetzalcoatl Intl (NLD)","Norfolk Island, Norfolk Intl Airport (NLK)","Nelspruit, Nelspruit Airport (NLP)","Nanning, Nanning Airport (NNG)","Spiddal, Connemara Arpt (NNR)","Nan Th, Nan Arpt (NNT)","Nowra, Nowra Arpt (NOA)","Knock, Knock International (NOC)","Nogales, Nogales Arpt (NOG)","Nossi Be, Fascene Arpt (NOS)","Noumea, Tontouta Arpt (NOU)","Napier Hastings, Hawkes Bay Arpt (NPE)","New Plymouth, New Plymouth Arpt (NPL)","Newport, Newport State Arpt (NPT)","Memphis, Memphis Naval Air Station (NQA)","Kingsville, Naval Air Station Arpt (NQI)","Nottingham UK, Nottingham Arpt (NQT)","Newquay, Newquay Civil Arpt (NQY)","Narrandera, Narrandera Arpt (NRA)","Norrkoping, Kungsangen Arpt (NRK)","North Ronaldsay, North Ronaldsay Arpt (NRL)","Tokio, Narita (NRT)","Noosa, Noosa Airport (NSA)","Bimini, North Seaplane Base (NSB)","Milton, Whiting Field Nas (NSE)","Yaounde, Nsimalen Arpt (NSI)","Norseman, Norseman Arpt (NSM)","Nelson, Nelson Arpt (NSN)","Scone, Scone Airport (NSO)","Nakhon Si Tham, Nakhon Si Thammarat Arpt (NST)","Notodden, Notodden Arpt (NTB)","Nantes, Nantes Atlantique (NTE)","Newcastle, Williamtown Arpt (NTL)","Santo Antao, Santo Antao Arpt (NTO)","Sun City, Pilansberg Arpt (NTY)","Nuremberg, Nuremberg Arpt (NUE)","Nulato, Nulato Arpt (NUL)","Mountain View, Moffett Field (NUQ)","Nullarbor, Nullarbor Arpt (NUR)","Narvik, Framnes Arpt (NVK)","Navegantes, Navegantes Arpt (NVT)","Norwich, Norwich Airport (NWI)","Nyeri, Nyeri Arpt (NYE)","Nanyuki, Nanyuki Arpt (NYK)","Nyngan, Nyngan Arpt (NYN)","Estocolmo, Skavsta Airport (NYO)","Nueva York, New York Pennsylvania Station (NYP)","Nyaung, Nyaung U Arpt (NYU)","Jacksonville, Cecil Field Nas (NZC)","Orange, Springhill Arpt (OAG)","Jacksonville, Albert J Ellis (OAJ)","Oakland, Metro Oakland Intl Arpt (OAK)","Cacoal, Cacoal Arpt (OAL)","Oamaru, Oamaru Airport (OAM)","Oaxaca, Xoxocotlan Arpt (OAX)","Okeechobee, Okeechobee County Arpt (OBE)","Obihiro, Obihiro Arpt (OBO)","Ocean Reef, Ocean Reef Arpt (OCA)","Ocean City, Ocean City Airport (OCE)","Ocala, Taylor Field Arpt (OCF)","Ocho Rios, Boscobel Arpt (OCJ)","Oceanside, Oceanside Municipal Arpt (OCN)","Cordoba, Cordoba Airport (ODB)","Odense, Odense Airport (ODE)","Odesa, Central Arpt (ODS)","Oak Harbor, Oak Harbour Municipal (ODW)","Vincennes, Oneal Arpt (OEA)","Ornskoldsvik, Ornskoldsvik Arpt (OER)","Norfolk Nebraska, Karl Stefan Fld (OFK)","Ogallala, Searle Field (OGA)","Columbia, Orangeburg Municpal (OGB)","Ogden, Ogden Municipal (OGD)","Maui, Kahului Airport (OGG)","Ogdensburg, Ogdensburg Municipal (OGS)","Ohrid, Ohrid Arpt (OHD)","Ourilandia, Ourilandia Arpt (OIA)","Oshima, Oshima Arpt (OIM)","Oita, Oita Arpt (OIT)","Kansas City, Johnson Executive Arpt (OJC)","Okinawa, Naha Field (OKA)","Oklahoma City, Will Rogers World Arpt (OKC)","Sapporo, Okadama Arpt (OKD)","Okino Erabu, Okino Erabu Arpt (OKE)","Okayama, Okayama Arpt (OKJ)","Kokomo, Kokomo Municipal (OKK)","Olbia, Costa Smeralda Arpt (OLB)","Fuerte Olimpo, Fuerte Olimpo Arpt (OLK)","Olympia, Olympia Arpt (OLM)","Olympic Dam, Olympic Dam Arpt (OLP)","Nogales, International Arpt (OLS)","Columbus, Columbus Arpt (OLU)","Omaha, Eppley Airfield (OMA)","Nome, Nome Arpt (OME)","Mostar, Mostar Arpt (OMO)","Omsk, Omsk Arpt (OMS)","Oneonta, Oneonta Municpal (ONH)","Odate Noshiro, Odate Noshiro Arpt (ONJ)","Newport, Newport Municipal Arpt (ONP)","Ontario, Ontario Intl (ONT)","Gold Coast, Gold Coast Arpt (OOL)","Cooma, Cooma Airport (OOM)","Kopasker, Kopasker Arpt (OPA)","Miami, Opa Locka Arpt (OPF)","Oporto, Porto Airport (OPO)","Orebro Bofors, Orebro Bofors Arpt (ORB)","Chicago, O Hare Intl Arpt (ORD)","Norfolk Virginia Newport News, Norfolk Intl Arpt (ORF)","Paramaribo, Zorg En Hoop Arpt (ORG)","Worcester, Worcester Arpt (ORH)","Cork, Cork International Arpt (ORK)","Orlando, Herndon Arpt (ORL)","Northampton, Northampton Rail Station (ORM)","Oran, Es Senia (ORN)","Norwalk, Norwalk Heliport (ORQ)","Orpheus Island, Waterport Arpt (ORS)","Paris, Orly Arpt (ORY)","Ostersund, Froesoe Airport (OSD)","Oshkosh, Wittman Field (OSH)","Oskarshamn, Oskarshamn (OSK)","Oslo, Oslo Arpt (OSL)","Ostrava, Mosnov Arpt (OSR)","Brujas, Ostend Airport (OST)","Columbus, Ohio State Univ Arpt (OSU)","Namsos, Namsos Arpt (OSY)","Worthington, Worthington Arpt (OTG)","North Bend, North Bend Municipal (OTH)","Ottumwa, Industrial Arpt (OTM)","Bucarest, Otopeni International (OTP)","Ancortes, Ancortes Arpt (OTS)","Kotzebue, Ralph Wien Memorial (OTZ)","Ouagadougou, Ouagadougou Arpt (OUA)","Oujda, Les Angades Arpt (OUD)","Oudtshoorn, Oudtshoorn Arpt (OUH)","Oulu, Oulu Airport (OUL)","Novosibirsk, Tolmachevo Arpt (OVB)","Oviedo, Asturias Airport (OVD)","Owatonna, Owatonna Arpt (OWA)","Owensboro, Daviess County Arpt (OWB)","Norwood, Norwood Ma Arpt (OWD)","Bissau, Airport Osvaldo Viera (OXB)","Oxford, Kidlington Arpt (OXF)","Oxnard, Oxnard Ventura Arpt (OXR)","Zaporozhe, Zaporozhye Arpt (OZH)","Ourzazate, Ourzazate (OZZ)","PanamÃ¡, Paitilla Arpt (PAC)","Paderborn, Paderborn Airport (PAD)","Everett, Snohomish Cty Arpt (PAE)","Paducah, Barkley Regional (PAH)","Palo Alto, Palo Alto Arpt (PAO)","Port Au Prince, Mais Gate Arpt (PAP)","Paros, Paros Community Arpt (PAS)","Patna, Patna Arpt (PAT)","Poza Rica, Tajin Arpt (PAZ)","Puebla, Huejotzingo Arpt (PBC)","Porbandar, Porbandar Arpt (PBD)","Pine Bluff, Pine Bluff Arpt (PBF)","West Palm Beach, Palm Beach Intl Arpt (PBI)","Paramaribo, Zanderij Intl Arpt (PBM)","Paraburdoo, Paraburdoo Arpt (PBO)","Puerto Barrios, Puerto Barrios Arpt (PBR)","Plettenberg Bay, Plettenberg Bay Arpt (PBZ)","Puerto La Victoria, Puerto La Victoria Arpt (PCJ)","Pucalpa, Capitan Rolden Arpt (PCL)","Picton, Koromiko (PCN)","Padang, Tabing Arpt (PDG)","Atlanta, Dekalb Peachtree (PDK)","Pointe Delgado, Azores, Nordela Arpt (PDL)","Punta Del Este, Cap Curbelo Arpt (PDP)","Piedras Negras, Piedras Negras Intl Arpt (PDS)","Pendleton, Pendleton Municipal (PDT)","Portland, Portland Intl Arpt (PDX)","Pardubice, Pardubice Arpt (PED)","Perm, Perm Arpt (PEE)","Everglades, Peenemuende Arpt (PEF)","Perugia, Sant Egidio Arpt (PEG)","Pereira, Matecana Arpt (PEI)","Peschiei, Peschiei Arpt (PEJ)","Puerto Maldonado, Puerto Maldonado Arpt (PEM)","Penang, Penang Intl Arpt (PEN)","Perth, Perth Arpt (PER)","Pelotas, Pelotas Federal Arpt (PET)","Peshawar, Peshawar Arpt (PEW)","Port Elizabeth, Penza Arpt (PEZ)","Passo Fundo, Passo Fundo Arpt (PFB)","PanamÃ¡ City, Bay County Arpt (PFN)","Paphos, Paphos Intl Airport (PFO)","Page, Page Airport (PGA)","Punta Gorda, Charlotte County (PGD)","Perpignan, Llabanere Arpt (PGF)","Pascagoula, Jackson Cnty (PGL)","Puerto Progreso, Stevens Field Arpt (PGO)","Greenville, Pitt Greenville Arpt (PGV)","Perigueux, Perigueux Arpt (PGX)","Parnaiba, Santos Dumont Airport (PHB)","Port Harcourt, Port Harcourt Arpt (PHC)","Port Hedland, Port Hedland Arpt (PHE)","Newport News, Williamsburg Intl Arpt (PHF)","Phan Thiet, Phan Thiet Arpt (PHH)","Filadelfia, Philadelphia Intl Arpt (PHL)","Port Huron, St Clair County Intl Arpt (PHN)","Phitsanulok, Phitsanulok Arpt (PHS)","Paris, Henry County Arpt (PHT)","Phalaborwa, Phalaborwa Airport (PHW)","Phoenix, Sky Harbor Intl Arpt (PHX)","Peoria, Greater Peoria Arpt (PIA)","Laurel, Hattiesburg Laurel Regional Ar (PIB)","Nassau, Paradise Island Arpt (PID)","St. Petersburgo, St Petersburg Clearwater Arpt (PIE)","Pocatello, Pocatello Municipal Arpt (PIH)","Glasgow, Prestwick Arpt (PIK)","Pierre, Pierre Municipal (PIR)","Poitiers, Biard Airport (PIS)","Pittsburgo, Pittsburgh Intl Arpt (PIT)","Piura, Piura Arpt (PIU)","Pikwitonei, Pikwitonei Rail Station (PIW)","Pico Island, Pico Arpt (PIX)","Payson, Payson Arpt (PJB)","Parkersburg, Wood County (PKB)","Parkes, Parkes Arpt (PKE)","Pangkor, Pangkor Arpt (PKG)","Playa Grande, Playa Grand Arpt (PKJ)","Pokhara, Pokhara Arpt (PKR)","Pekanbaru, Simpang Tiga Arpt (PKU)","Pskov, Pskov Arpt (PKV)","Selebi Phikwe, Selebi Phikwe Arpt (PKW)","Plattsburgh, Clinton Cty Municipal (PLB)","Plymouth, Roborough Arpt (PLH)","Placencia, Placencia Arpt (PLJ)","Branson Point Lookout, M Graham Clark Arpt (PLK)","Palembang, Mahmud Badaruddin Li Arpt (PLM)","Pellston, Emmet Cty Arpt (PLN)","Port Lincoln, Port Lincoln Arpt (PLO)","Palanga, Palanga Arpt (PLQ)","Providenciales, Providenciales Intl (PLS)","Belo Horizonte, Pampulha Arpt (PLU)","Plymouth, Plymouth Municipal Arpt (PLY)","Port Elizabeth, Port Elizabeth Airport (PLZ)","Puerto Montt, Tepual Airport (PMC)","Palmdale, Airforce 42 (PMD)","Portsmouth, Portsmouth Arpt (PME)","Parma, Parma Arpt (PMF)","Ponta Pora, International (PMG)","Huntington, Portsmith Regional (PMH)","Palma De Mallorca, Palma Mallorca Arpt (PMI)","Palermo, Punta Raisi Arpt (PMO)","Palmerston, Palmerstown North Arpt (PMR)","Porlamar, Delcaribe Gen S Marino Arpt (PMV)","Palmas, Palmas Arpt (PMW)","Pamplona, Pamplona Noain Arpt (PNA)","Ponca City, Ponca City Municipal Arpt (PNC)","Filadelfia, N Philadelphia (PNE)","Paranagua, Paranagua Municipal (PNG)","Phnom Penh, Pochentong Arpt (PNH)","Pohnpei, Pohnpei Arpt (PNI)","Pinotepa Nacional, Pinotepa Nacional Arpt (PNO)","Pune, Lohegaon Arpt (PNQ)","Pointe Noire, Pointe Noire Arpt (PNR)","Pensacola, Pensacola Regional Municipal (PNS)","Puerto Natales, Teniente Julio Gallardo Arpt (PNT)","Sherman, Grayson County Arpt (PNX)","Petrolina, Petrolina Internacional Arpt (PNZ)","Porto Alegre, Porto Alegre Airport (POA)","La Verne, Brackett Field (POC)","Fort Polk, Fort Polk Arpt (POE)","Poplar Bluff, Earl Fields Memorial (POF)","Port Gentil, Port Gentil Arpt (POG)","Port Moresby, Jackson Field (POM)","Puerto Plata, La Union Arpt (POP)","Pori, Pori Airport (POR)","Puerto EspaÃ±a, Isla Madeira, Piarco Arpt (POS)","Poughkeepsie, Dutchess County (POU)","Portoroz, Portoroz Airport (POW)","Pontoise, Cormeille En Vexin (POX)","Poznan, Lawica Arpt (POZ)","Presidente Prudente, A De Barros (PPB)","Puerto Penasco, Puerto Penasco Municipal (PPE)","Pago Pago, Pago Pago Arpt (PPG)","Port Pirie, Port Porie Arpt (PPI)","Phaplu, Phaplu Airport (PPL)","Pompano Beach, Pompano Beach Arpt (PPM)","PopayÃ¡n, Guillermo LeÃ³n Valencia Arpt (PPN)","Proserpine, Whitsunday Coast Arpt (PPP)","Paraparaumu, Paraparaumu Arpt (PPQ)","Puerto Princesa, Puerto Princesa Arpt (PPS)","Papeete, Intl Tahiti Faaa (PPT)","Papa Westray, Papa Westray Arpt (PPW)","Presque Isle, Northern Maine Regional (PQI)","Palenque, Palenque Arpt (PQM)","Pt Macquarie, Port Mac Quarie Airport (PQQ)","Paso Robles, Paso Robles Cty (PRB)","Prescott, Prescott Municipal (PRC)","Praga, Ruzyne Arpt (PRG)","Phrae, Phrae Arpt (PRH)","Praslin Island, Praslin Island Arpt (PRI)","Portimao, Portimao Airport (PRM)","Pristina, Pristina Arpt (PRN)","Propriano, Propriano Arpt (PRP)","Paris, Paris Cox Field Arpt (PRX)","Pretoria, Wonderboom Arpt (PRY)","Pisa, Gal Galilei Arpt (PSA)","Philipsburg, Midstate Arpt (PSB)","Pasco, Tri Cities Arpt (PSC)","Ponce, Mercedita Arpt (PSE)","Pittsfield, Pittsfield Municipal (PSF)","Petersburg, Petersburg Municipal (PSG)","Dublin, New River Valley Arpt (PSK)","Portsmouth, Pease AFB (PSM)","Palm Springs, Palm Springs Municipal (PSP)","Pescara, Liberi Airport (PSR)","Petersburg, Petersburg Municipal Arpt (PTB)","Pietersburg, Pietersburg Arpt (PTG)","Port Douglas, Port Douglas Arpt (PTI)","Portland, Portland Arpt (PTJ)","Pontiac, Oakland Pontiac Arpt (PTK)","Pato Branco, Pato Branco Municipal (PTO)","Pointe A Prite, Le Raizet Arpt (PTP)","Pittsburg, Atkinson Municipal Arpt (PTS)","Pottstown, Pottstown Limerick (PTW)","PanamÃ¡, Tocumen Intl Arpt (PTY)","Pueblo, Pueblo Arpt (PUB)","Price, Carbon Cty Municipal Arpt (PUC)","Pau Fr, Uzein Airport (PUF)","Port Augusta, Port Augusta Arpt (PUG)","Punta Cana, Punta Cana Arpt (PUJ)","Punta Arenas, Presidente Ibanez Arpt (PUQ)","Busan, Kimhae Arpt (PUS)","Pullman, Pullman Moscow Arpt (PUW)","Pula, Pula Arpt (PUY)","Provincetown, Provincetown Municipal (PVC)","Providence, T F Green St Arpt (PVD)","Shanghai, Pu Dong Arpt (PVG)","Porto Velho, Belmonte Arpt (PVH)","Preveza, Aktion Arpt (PVK)","Puerto Vallarta, Ordaz Arpt (PVR)","Provo, Provo Arpt (PVU)","Plainview, Hale County Arpt (PVW)","Painesville, Casement Arpt (PVZ)","Oklahoma City, Wiley Post Arpt (PWA)","Chicago, Pal Waukee Arpt (PWK)","Portland, Portland Intl Jetport (PWM)","Bremerton, Bremerton Municipal (PWT)","Puerto Escondido, Puerto Escondido Municipal (PXM)","Porto Santo, Isla Madera, Porto Santo Arpt (PXO)","Pattaya, Pattaya Arpt (PYX)","Pietermaritzburg, Pietermaritzburg Arpt (PZB)","Penzance, Penzance Arpt (PZE)","Puerto Ordaz, Puerto Ordaz Arpt (PZO)","Piestany, Piestany Arpt (PZY)","Bella Coola, Bella Colla Municipal (QBC)","Eagle, Vail Van Service (QBF)","Copper Mountain, Copper Mountain Van Service (QCE)","Dusseldorf, Dusseldorf Rail Station (QDU)","Maloy, Harbour Arpt (QFQ)","Saarbruecken, Saarbruecken Rail Station (QFZ)","Frankfurt, Neu Isenburg Arpt (QGV)","Novo Hamburgo, Novo Hamburgo Arpt (QHV)","Iguatu, Iguatu Arpt (QIG)","Nantes, Nantes Rail Station (QJZ)","Breckenridge, Breckenridge Van Service (QKB)","Colonia/Bonn, Cologne Railroad Station (QKL)","Keystone, Keystone Van Service (QKS)","Leeton, Leeton Arpt (QLE)","Niteroi, Niteroi Arpt (QNT)","Londres, Harwich Rail Station (QQH)","Londres, London - Kings Cross Rail Serv (QQK)","Manchester, Manchester - Piccadilly Rail S (QQM)","Birmingham, Birmingham - New Street Rail S (QQN)","Londres, London - Paddington Rail Servi (QQP)","Londres, London - Euston Rail Service (QQU)","Londres, London - Waterloo Rail Service (QQW)","Bath, Bath Rail Service (QQX)","York, York Rail Station (QQY)","Johannesburgo, Randgermiston Arpt (QRA)","Queretaro, Queretaro Arpt (QRO)","Arras, Arras Arpt (QRV)","Ft Collins, Fort Collins Bus Service (QWF)","Ft Collins, Loveland Bus Service (QWH)","Denver, Longmont Bus Service (QWM)","Corvallis, Albany Bus Service (QWY)","Olsztyn, Olsztyn Arpt (QYO)","Gavle, Gavle Rail Station (QYU)","Rabaul, Lakunai Arpt (RAB)","Racine, Horlick Arpt (RAC)","Tortola Westend, Road Town Arpt (RAD)","Praia, Francisco Mendes (RAI)","Marrakech, Menara Airport (RAK)","Riverside, Riverside Municipal (RAL)","Ribeirao Preto, Leite Lopes (RAO)","Rapid City, Rapid City Regional Arpt (RAP)","Rarotonga, Rarotonga Arpt (RAR)","Rabat, Sale Airport (RBA)","Big Bear City, Big Bear City Arpt (RBF)","Roseburg, Roseburg Municipal (RBG)","Redding, Flight Service Station (RBL)","Straubing, Wallmuhle (RBM)","Rio Branco, Pres Medici Arpt (RBR)","Walterboro, Walterboro Municipal Arpt (RBW)","Richards Bay, Richards Bay Arpt (RCB)","Roche Harbor, Roche Harbor Arpt (RCE)","Rochefort, St Agnant Arpt (RCO)","Redding, Redding Municipal Arpt (RDD)","Reading, Reading Municipal (RDG)","Redmond Bend, Roberts Field (RDM)","Raleigh, Raleigh Durham Intl Arpt (RDU)","Rodez, Marcillac (RDZ)","Recife, Recife Airport (REC)","Reggio Calabria, Tito Menniti Arpt (REG)","Siem Reap, Siem Reap Arpt (REP)","Retalhuleu, Base Aerea Del Sur Arpt (RER)","Reus, Reus Arpt (REU)","Reynosa, General Lucio Blanco Arpt (REX)","Rockford, Rockford Arpt (RFD)","Raiatea, Raiatea Arpt (RFP)","Rangiroa Island, Rangiroa Arpt (RGI)","Yangon, Mingaladon (RGN)","Ranger, Ranger Municipal Arpt (RGR)","Reims, Champagne Arpt (RHE)","Rhinelander, Oneida County (RHI)","Rosh Pina, Rosh Pina Airport (RHN)","Rhodes, Diagoras Arpt (RHO)","San Jose, Reid Hillview Of Santa Clara C (RHV)","Santa Maria, Base Aerea Arpt (RIA)","Richmond, Byrd Intl (RIC)","Richfield, Reynolds Municipal Arpt (RIF)","Rio Grande, Rio Grande Airport (RIG)","Rifle, Garfield Cty Arpt (RIL)","Riverton, Riverton Municipal (RIW)","Riga, Riga Arpt (RIX)","Rijeka, Rijeka Arpt (RJK)","Rockland, Rockland Arpt (RKD)","Roskilde, Roskilde Arpt (RKE)","Rockport, Aransas County Arpt (RKP)","Rock Springs, Rock Springs Municipal (RKS)","Ras Al Khaimah, Ras Al Khaimah Arpt (RKT)","Reykjavyk, Reykjavik Domestic Arpt (RKV)","Rostock Laage, Laage Arpt (RLG)","Roma, Roma Arpt (RMA)","Rome, Richard B Russell Arpt (RMG)","Rimini, Miramare Airport (RMI)","Renmark, Renmark Airport (RMK)","Ramstein, Ramsteim Arpt (RMS)","Ronneby, Kallinge Arpt (RNB)","Roanne, Renaison Arpt (RNE)","New Richmond, New Richmond Municipal (RNH)","Bornholm, Bornholm Arpt (RNN)","Reno, Reno Tahoe Intl Arpt (RNO)","Rennes, Saint Jacques Arpt (RNS)","Roanoke, Roanoke Regional Arpt (ROA)","Monrovia, Roberts Intl (ROB)","Rochester, Monroe Cty Arpt New York (ROC)","Rockhampton, Rockhampton Arpt (ROK)","Rondonopolis, Rondonopolis Arpt (ROO)","Rota, Rota Arpt (ROP)","Koror, Airai Airport (ROR)","Rotorua, Rotorua Arpt (ROT)","Rostov, Rostov Arpt (ROV)","Roswell, Industrial Aircenter (ROW)","Rosh Pina, Rosh Pina Arpt (RPN)","Rodrigues Island, Rodrigues Island Arpt (RRG)","Sorrento, Sorrento Arpt (RRO)","Roros, Roros Arpt (RRS)","Rock Sound, S Eleuthera Arpt (RSD)","Sydney, Au Rose Bay Arpt (RSE)","Ruston, Ruston Regional Arpt (RSN)","Rochester, Rochester Municipal (RST)","Yeosu, Yeosu Arpt (RSU)","Fort Myers, Regional Southwest Arpt (RSW)","Roatan, Roatan Arpt (RTB)","Rotterdam, Rotterdam Arpt (RTM)","Raton, Crews Fld (RTN)","Rottnest, Rottnest Island Arpt (RTS)","Saratov, Saratov Arpt (RTW)","Riyadh, King Khaled Intl (RUH)","Ruidoso, Ruidoso Municipal Arpt (RUI)","St. Denis, Isla Reunion, Gillot Airport (RUN)","Rutland, Rutland Arpt (RUT)","St Petersburg, Rzhevka Arpt (RVH)","Roervik, Ryumsjoen Arpt (RVK)","Rovaniemi, Rovaniemi Arpt (RVN)","Green River, Green River Arpt (RVR)","Rocky Mount, Wilson Arpt (RWI)","Rawlins, Rawlins Municipal (RWL)","Sumare, Sumare Arpt (RWS)","Rzeszow, Jasionka Arpt (RZE)","Roanoke Rapids, Halifax Cty Arpt (RZZ)","Laramie, Lar Shively Arpt (SAA)","Saba Island, Juancho Yraus Quinl (SAB)","Sacramento, Sacramento Executive Arpt (SAC)","Santa Fe, Santa Fe Municipal (SAF)","Sana, Sanaa Intl Airport (SAH)","San Salvador, Comalapa Intl Arpt (SAL)","San Diego, Lindbergh Intl Arpt (SAN)","San Pedro Sula, Ramon Villeda Morales Arpt (SAP)","Sparta, Sparta Community Airport (SAR)","San Antonio, San Antonio Intl (SAT)","Savannah, Savannah Intl Arpt (SAV)","Sabiha Gokcen, Sabiha Gokcen Arpt (SAW)","Siena, Siena Arpt (SAY)","Santa Barbara, Santa Barbara Arpt (SBA)","St Barthelemy, St Jean Arpt (SBH)","St Brieuc, Tremuson Armor Arpt (SBK)","Sheboygan, Sheboygan Arpt (SBM)","South Bend, Michiana Regional Arpt (SBN)","San Luis Obispo, San Luis Obispo Cty Arpt (SBP)","Steamboat Springs, Steamboat Arpt (SBS)","Springbok, Springbok Arpt (SBU)","Sibu, Sibu Arpt (SBW)","Salisbury Ocean City, Wicomico Regional Arpt (SBY)","Prudhoe Bay Deadhorse, Prudhoe Bay Deadhorse Arpt (SCC)","State College, University Park Arpt (SCE)","Scottsdale, Scottsdale Municipal Arpt (SCF)","Schenectady, Schenectady County Arpt (SCH)","San Crystobal, Aeropuerto Santo Domingo (SCI)","Stockton, Stockton Metro Arpt (SCK)","Santiago De Chile, Arturo Merino Benitez (SCL)","Saarbruecken, Ensheim Arpt (SCN)","Santiago De Compostela, Santiago Airport (SCQ)","Shetland Islands Area, Scatsta Arpt (SCS)","Santiago, Antonio Maceo Arpt (SCU)","Syktyvkar, Syktyvkar Arpt (SCW)","Salina Cruz, Salina Cruz Arpt (SCX)","San Cristobal, San Cristobal Airport (SCY)","Baghdad, Saddam Intl (SDA)","Saldanha Bay, Langebaanweg Arpt (SDB)","Louisville, Standiford Fld (SDF)","Sendai, Sendai Arpt (SDJ)","Sandakan, Sandakan Arpt (SDK)","Sundsvall, Sundsvall Arpt (SDL)","San Diego, Brown Field Municipal (SDM)","Sandane, Anda Airport (SDN)","Santo Domingo, Las Americas Arpt (SDQ)","Santander, Santander Airport (SDR)","Rio De Janeiro, Santos Dumont Arpt (SDU)","Tel Aviv, Dov Airport (SDV)","Sedona, Sedona Arpt (SDX)","Sidney, Sindey Richland Municipal Arpt (SDY)","Seattle, Seattle Tacoma Intl Arpt (SEA)","San Diego, Gillespie Field (SEE)","Sebring, Air Terminal Arpt (SEF)","Seul, Gimpo Intl (SEL)","Southend, Southend Municipal Arpt (SEN)","Stephenville, Clark Field (SEP)","Mahe Island, Seychelles Intl Arpt (SEZ)","Sfax, El Maou Airport (SFA)","Sanford, Orlando Sanford Arpt (SFB)","San Fernando, Las Flecheras (SFD)","San Fernando, San Fernando Arpt (SFE)","Spokane, Felts Field (SFF)","St. Martin, Esperance Airport (SFG)","San Felipe, San Felipe Arpt (SFH)","Kangerlussuaq, Sondre Stromfjord Arpt (SFJ)","Sao Filipe, Sao Filipe Arpt (SFL)","San Francisco, San Francisco Intl Arpt (SFO)","Subic Bay, Subic Bay Intl Arpt (SFS)","Skelleftea, Skelleftea Arpt (SFT)","San Felix, San Felix Arpt (SFX)","Smithfield, Smithfield Arpt (SFZ)","Sonderborg, Sonderborg Arpt (SGD)","Siegen, Siegerland Arpt (SGE)","Springfield, Springfield Branson Regional A (SGF)","Ho Chi Minh, Tan Son Nhut Arpt (SGN)","St George, St George Arpt (SGO)","Sugar Land, Hull Field Arpt (SGR)","Stuttgart, Stuttgart Municipal (SGT)","St George, Saint George Municipal Arpt (SGU)","Skagway, Skagway Minicipal Arpt (SGY)","Shanghai, Hongqiao Arpt (SHA)","Nakashibetsu, Nakashibetsu Arpt (SHB)","Staunton, Shenandoah Valley Arpt (SHD)","Shenyang, Shenyang Arpt (SHE)","Sharjah, Sharjah Airport (SHJ)","Sokcho, Seolak Arpt (SHO)","Qinhuangdao, Qinhuangdao Arpt (SHP)","Sheridan, Sheridan Cty Arpt (SHR)","Shepparton, Shepparton (SHT)","Shreveport, Shreveport Regional Arpt (SHV)","Sharurah, Sharurah Arpt (SHW)","Xiguan, Xiguan Airport (SIA)","Ilha Do Sal, Amilcar Cabral Intl Arpt (SID)","Singapur, Changi Intl Arpt (SIN)","Simferopol, Simferopol Arpt (SIP)","Sion, Sion Arpt (SIR)","Sitka, Sitka Airport (SIT)","Singleton, Singleton Arpt (SIX)","San Jose, San Jose Intl Arpt (SJC)","San Jose del Cabo, Los Cabos Arpt (SJD)","Sarajevo, Butmir Arpt (SJJ)","Sao Jose Dos Campos, Sao Jose Dos Campos Arpt (SJK)","San Jose, Juan Santa Maria Intl (SJO)","Sao Jose Do Rio Preto, Sao Jose Do Rio Preto (SJP)","San Angelo, Mathis Field (SJT)","San Juan, Luiz Munoz Marin Intl (SJU)","Seinajoki, Ilmajoki Arpt (SJY)","Sao Jorge Island, Sao Jorge Arpt (SJZ)","St Kitts, Robert L Bradshaw Arpt (SKB)","Samarkand, Samarkand Arpt (SKD)","Skien, Skien Arpt (SKE)","Thessaloniki, Makedonia Arpt (SKG)","Isle Of Skye Hebrides Islands, Broadford Arpt (SKL)","Stokmarknes, Skagen Arpt (SKN)","Skopje, Skopje Arpt (SKP)","Skrydstrup, Skrydstrup Airport (SKS)","Sialkot, Sialkot Arpt (SKT)","Sandusky, Griffing Sandusky Arpt (SKY)","Salt Lake City, Salt Lake City Intl Arpt (SLC)","Salem, McNary Field (SLE)","Saranac Lake, Adirondack Arpt (SLK)","Salalah, Salalah Arpt (SLL)","Salina, Salina Municipal (SLN)","San Luis Potosi, San Luis Potosi Municipal Arpt (SLP)","Santa Lucia, Vigie Field Arpt (SLU)","Simla, Simla Arpt (SLV)","Saltillo, Saltillo Arpt (SLW)","San Luis, Mal Cunha Machado (SLZ)","Santa Maria, Vilo Do Porto Arpt (SMA)","Somerset, Pulaski County Arpt (SME)","Sacramento, Sacramento International (SMF)","Samos, Samos Arpt (SMI)","Los Angeles, Santa Monica Municipal Arpt (SMO)","Santa Marta, Simon Bolivar (SMR)","Saint Marie, St Marie Arpt (SMS)","St Moritz, Samedan Arpt (SMV)","Santa Maria, Santa Maria Public Arpt (SMX)","Santa Ana, John Wayne Arpt (SNA)","Sao Nicolau, Preguica Arpt (SNE)","San Felipe, San Felipe Airport (SNF)","Stanthorpe, Stanthorpe (SNH)","Sinoe, R E Murray Arpt (SNI)","Shawnee, Shawnee Airport (SNL)","Shannon, Shannon Arpt (SNN)","Sakon Nakhon, Sakon Nakhon Arpt (SNO)","San Quintin, San Quintin Arpt (SNQ)","St Nazaire, Montoir Airport (SNR)","Salinas, Salinas Arpt (SNS)","Santa Clara, Santa Clara Arpt (SNU)","Sidney, Sidney Municipal (SNY)","Saarmelleek, Saarmelleek Balaton Arpt (SOB)","Solo, Adi Sumarno Arpt (SOC)","Sorocaba, Sorocaba Arpt (SOD)","Sofia, Sofia Vrazhdebna Arpt (SOF)","Sogndal, Haukasen Arpt (SOG)","South Molle, South Molle Island Arpt (SOI)","San Tome, El Tigre Arpt (SOM)","Espiritu Santo, Pekoa Arpt (SON)","Soderhamn, Soderhamn Arpt (SOO)","Pinehurst, Pinehurst Arpt (SOP)","Sorong, Jefman Arpt (SOQ)","Southampton, Southampton Intl Arpt (SOU)","Seldovia, Seldovia Arpt (SOV)","Show Low, Show Low Arpt (SOW)","Stronsay, Stronsay Arpt (SOY)","Greenville, Downtown Memorial (SPA)","Santa Cruz, La Palma Arpt (SPC)","Spearfish, Black Hills Clyde Ice Field (SPF)","St. Petersburgo, Whitted Airport (SPG)","Springfield, Capital Airport (SPI)","Saipan, Saipan Intl (SPN)","San Pedro, San Pedro Arpt (SPR)","Wichita Falls, Wichita Falls Municipal Arpt (SPS)","Split, Split Arpt (SPU)","Spencer, Spencer Municipal (SPW)","Springdale, Springdale Municipal Arpt (SPZ)","Santa Ynez, Santa Ynez Arpt (SQA)","Sterling, Whiteside Cty Municipal (SQI)","Storuman, Gunnarn Arpt (SQO)","Sucre, Alcantari (SRE)","Semarang, Achmad Uani Arpt (SRG)","Santa Rosalia, Santa Rosalia Arpt (SRL)","Strahan, Strahan Arpt (SRN)","Stord, Stord Arpt (SRP)","Sarasota, Sarasota Bradenton Arpt (SRQ)","Santa Cruz, Skypark Arpt (SRU)","Santa Cruz De La Sierra, El Trompillo Arpt (SRZ)","Salvador, Luis E Magalhaes Arpt (SSA)","Malabo, Santa Isabel Arpt (SSG)","Sharm El Sheik, Ophira Arpt (SSH)","St Simons Is, McKinnon Arpt (SSI)","Sandnessjoen, Stokka Arpt (SSJ)","La Sarre, La Sarre Rail Station (SSQ)","Santa Barbara, Las Delicias Arpt (STB)","Saint Cloud, Saint Cloud Municipal (STC)","Santo Domingo, Mayor Humberto Vivas Guerrero  (STD)","Stevens Point, Stevens Point Municipal (STE)","Santiago, Santiago Municipal (STI)","St. Louis, Lambert St Louis Intl (STL)","Santarem, Eduardo Gomes Arpt (STM)","Londres, Stansted (STN)","St Paul, Downtown St Paul Arpt (STP)","Stuttgart, Echterdingen Arpt (STR)","Santa Rosa, Sonoma Cty Arpt (STS)","St. Thomas, Islas Virgenes, Cyril E King Arpt (STT)","Stavropol, Stavropol Arpt (STW)","St. Croix, Islas Virgenes, Alexander Hamilton Arpt (STX)","Salto, Salto Arpt (STY)","Stuart, Witham Field (SUA)","Surabaya, Juanda Arpt (SUB)","Sturgeon Bay, Door Country Arpt (SUE)","Lamezia Terme, S Eufemia Arpt (SUF)","Sur Om, Sur Arpt (SUH)","Sumter, Sumter Municipal Arpt (SUM)","Sun Valley, Friedman Memorial (SUN)","St. Louis, Spirit Of St Louis Arpt (SUS)","Fairfield, Travis AFB (SUU)","Suva, Nausori Airport (SUV)","Superior, Richard I Bong Arpt (SUW)","Sioux City, Sioux Gateway Arpt (SUX)","Silver City, Grant County Airport (SVC)","St Vincent, Arnos Vale Arpt (SVD)","Stavanger, Sola Airport (SVG)","Statesville, Statesville Municipal Arpt (SVH)","Svolvaer, Helle (SVJ)","Savonlinna, Savonlinna Arpt (SVL)","Moscu, Sheremetyevo Arpt (SVO)","Sevilla, San Pablo Arpt (SVQ)","Savusavu, Savusavu Airport (SVU)","Ekaterinburg, Sverdolovsk Arpt (SVX)","San Antonio, San Antonio Arpt (SVZ)","Shantou, Shantou Airport (SWA)","Seward, Seward Airport (SWD)","Newburgh, Stewart Airport (SWF)","Swan Hill, Swan Hill Arpt (SWH)","Swindon, Swindon Rail Station (SWI)","Milan, Segrate Arpt (SWK)","Stillwater, Searcy Fld (SWO)","Swansea, Fairwood Comm (SWS)","Estrasburgo, Enzheim Arpt (SXB)","Catalina Island, Catalina Arpt (SXC)","Sophia Antipolis, Sophia Antipolis Hlpt (SXD)","Sale, Sale Arpt (SXE)","Berlin, Schoenefeld Arpt (SXF)","Sligo, Collooney (SXL)","St. Maarten, Princ Juliana Arpt (SXM)","Srinagar, Srinagar Arpt (SXR)","Sydney, Sydney Kingsford Smith Arpt (SYD)","Shonai, Shonai Arpt (SYO)","San Jose, Tobias Bolanos Intl (SYQ)","Syracuse, Hancock Intl (SYR)","Sanya, Sanya Arpt (SYX)","Stornoway, Stornoway Arpt (SYY)","Shiraz, Shiraz Arpt (SYZ)","Kuala Lumpur, Sultan Abdul Aziz Shah Arpt (SZB)","Sheffield, Sheffield City Arpt (SZD)","Samsun, Carsamba Arpt (SZF)","Salzburgo, W A Mozart Arpt (SZG)","Siguanea, Siguanea Arpt (SZJ)","Skukuza, Skukuza Arpt (SZK)","Shanzhou, Shanzhou Arpt (SZO)","San Cristobal De Las Casas, San Cristobal Arpt (SZT)","Schwerin, Parchim Arpt (SZW)","Shenzhen, Shenzhen Arpt (SZX)","Szczecin, Goleniow Arpt (SZZ)","Tobago, Crown Point Arpt (TAB)","Tacloban, D Z Romualdez Arpt (TAC)","Pueblo, Las Animas Arpt (TAD)","Daegu, Daegu Arpt (TAE)","Takamatsu, Takamatsu Arpt (TAK)","Tanana, Ralph Calhoun Arpt (TAL)","Tampico, General F Javier Mina (TAM)","Qingdao, Liuting Arpt (TAO)","Tapachula, Tapachula Arpt (TAP)","Taranto, M A Grottag Arpt (TAR)","Tashkent, Vostochny Arpt (TAS)","Tatry Poprad, Tatry Poprad (TAT)","Tartu, Tartu Arpt (TAY)","Dashoguz, Dashoguz Arpt (TAZ)","Tububil, Tabubil Arpt (TBG)","Tabarka, Tabarka Arpt (TBJ)","Ft Leonard Wood, Forney Field (TBN)","Tumbes, Tumbes Arpt (TBP)","Tbilisi, Novo Alexeyevka Arpt (TBS)","Tabatinga, Tabatinga Intl Arpt (TBT)","Tongatapu, Tongatapu Intl (TBU)","Tennant Creek, Tennant Creek Arpt (TCA)","Treasure Cay, Treasure Cay Arpt (TCB)","Tuscaloosa, Van De Graff Arpt (TCL)","Tehuacan, Tehuacan Arpt (TCN)","Taba, Ras An Naqb Arpt (TCP)","Thaba Nchu, Thaba Nchu Arpt (TCU)","Tocumwal, Tocumwal Arpt (TCW)","Trinidad, Trinidad Arpt (TDD)","Amarillo, Tradewind Airport (TDW)","Tela, Tela Arpt (TEA)","Telemaco Borba, Telemaco Borba Arpt (TEC)","Thisted, Thisted Arpt (TED)","Terceira, Islas Azores, Lajes Arpt (TER)","Tete, Matunda Arpt (TET)","Te Anau, Manapouri Airport (TEU)","Telluride, Telluride Municipal Arpt (TEX)","Tefe, Tefe Arpt (TFF)","Tenerife, Islas Canarias, Tenerife Norte Los Rodeos Arpt (TFN)","Tenerife, Islas Canarias, Reina Sofia Arpt (TFS)","Podgorica, Golubovci Arpt (TGD)","Kuala Terengganu, Sultan Mahmood Arpt (TGG)","Tirgu Mures, Tirgu Mures Arpt (TGM)","Traralgon, La Trobe Traralgon Arpt (TGN)","Tanga, Tanga Arpt (TGT)","Tegucigalpa, Toncontin Arpt (TGU)","Tuxtla Gutierrez, Llano San Juan Arpt (TGZ)","Teresina, Teresina Arpt (THE)","Berlin, Tempelhof Arpt (THF)","Thangool, Thangool Arpt (THG)","Trollhattan, Trollhattan Arpt (THN)","Thermopolis, Hot Springs (THP)","Teheran, Mehrabad Arpt (THR)","Sukhothai, Sukhothai Arpt (THS)","York, York Arpt (THV)","Tirana, Rinas Arpt (TIA)","Taif, Taif Airport (TIF)","Tikehau, Tikehau Arpt (TIH)","Tijuana, Gen Abelardo Rodriguez (TIJ)","Timika, Timika Arpt (TIM)","Tripoli, Tripoli Intl Arpt (TIP)","Tinian, Tinian Arpt (TIQ)","Tirupati, Tirupati Arpt (TIR)","Thursday Island, Thursday Island Arpt (TIS)","Timaru, Timaru Arpt (TIU)","Tivat, Tivat Arpt (TIV)","Tacoma, Tacoma Industrial Arpt (TIW)","Titusville, Space Center Executive Arpt (TIX)","Tari, Tari Arpt (TIZ)","Tarija, Tarija Arpt (TJA)","Tyumen, Tyumen Arpt (TJM)","Tanjung Pandan, Bulutumbang Arpt (TJQ)","Reno, Truckee Airport (TKF)","Bandar Lampung, Bandar Lampung Arpt (TKG)","Tok Ak, Tok Arpt (TKJ)","Truk, Truk Arpt (TKK)","Tikal, El Peten Arpt (TKM)","Tokunoshima, Tokunoshima Arpt (TKN)","Tokushima, Tokushima Arpt (TKS)","Turku, Turku Arpt (TKU)","Toluca, Arpt. Intl. Lic. Adolfo LÃ³pez Mateos (TLC)","Tulear, Tulear Arpt (TLE)","Tallahassee, Tallahassee Municipal (TLH)","Tallinn, Ulemiste Arpt (TLL)","Toulon, Le Palyvestre Arpt (TLN)","Tulare, Mefford Field (TLR)","Toulouse, Blagnac Arpt (TLS)","Tel Aviv, Ben Gurion Intl Arpt (TLV)","Tifton, Henry Tift Myers Arpt (TMA)","Miami, Tamiami Airport (TMB)","Tamatave, Tamatave Arpt (TMM)","Tampere, Tampere Pirkkala Arpt (TMP)","Trombetas, Trombetas Arpt (TMT)","Tambor, Tambor Arpt (TMU)","Tamworth, Tamworth Arpt (TMW)","Thames, Thames Arpt (TMZ)","Jinan, Jinan Arpt (TNA)","Tanega Shima, Tanega Shima Arpt (TNE)","Tanger, Boukhalef Arpt (TNG)","Tainan, Tainan Arpt (TNN)","Tamarindo, Tamarindo Arpt (TNO)","Antananarivo, Ivato Arpt (TNR)","Tobruk, Tobruk Arpt (TOB)","Tioman, Tioman Arpt (TOD)","Tozeur, Tozeur Arpt (TOE)","Tomsk, Tomsk Arpt (TOF)","Toledo, Toledo Express Arpt (TOL)","Topeka, Phillip Billard Arpt (TOP)","Torrington, Torrington Minicipal Arpt (TOR)","Tromso, Tromso Langnes Arpt (TOS)","Toledo, Toledo Arpt (TOW)","Tobolsk, Tobolsk Arpt (TOX)","Toyama, Toyama Arpt (TOY)","Tampa, Tampa Intl (TPA)","Taipei, Chiang Kai Shek Arpt (TPE)","Tampa, Peter O Knight Arpt (TPF)","Temple, Temple Arpt (TPL)","Tepic, Tepic Arpt (TPQ)","Tom Price, Tom Price (TPR)","Trapani, Birgi Arpt (TPS)","San Domino Island, San Domino Island Arpt (TQR)","Torreon, Francisco Sarabia Arpt (TRC)","Trondheim, Trondheim Vaernes Arpt (TRD)","Tiree, Tiree Arpt (TRE)","Oslo, Sandefjord Arpt (TRF)","Tauranga, Tauranga Arpt (TRG)","Bristol, Tri City Regional Arpt (TRI)","Terrell, Terrell Field Arpt (TRL)","Torino, Citta Di Torino Arpt (TRN)","Taree, Taree Arpt (TRO)","Trieste, Ronchi Dei Legionari Arpt (TRS)","Trujillo, Trujillo Arpt (TRU)","Thiruvananthapuram, Thiruvananthapuram Arpt (TRV)","Tarawa, Bonriki Arpt (TRW)","Taipei, Sung Shan Arpt (TSA)","Astana, Astana Arpt (TSE)","Treviso, Treviso Arpt (TSF)","Tsushima, Tsushima Arpt (TSJ)","Taos, Taos Airport (TSM)","Tianjin, Tianjin Airport (TSN)","Isles Of Scilly, Tresco Arpt (TSO)","Timisoara, Timisoara Arpt (TSR)","Nueva York, East 34th St Hlpt (TSS)","Trang, Trang Arpt (TST)","Townsville, Townsville Arpt (TSV)","Tortoli, Arbatax Arpt (TTB)","Troutdale, Troutdale Arpt (TTD)","Tottori, Tottori Arpt (TTJ)","Trenton, Trenton Mercer Arpt (TTN)","Tortuquero, Tortuquero Arpt (TTQ)","Taitung, Taitung Arpt (TTT)","Tours, Saint Symphorien Arpt (TUF)","Turaif, Turaif Arpt (TUI)","Turbat, Turbat Arpt (TUK)","Tulsa, Tulsa Intl (TUL)","Tunez, Carthage Arpt (TUN)","Taupo, Taupo Arpt (TUO)","Tupelo, C D Lemons Municipal (TUP)","Tucurui, Tucurui Arpt (TUR)","Tucson, Tucson Intl Arpt (TUS)","Tabuk, Tabuk Arpt (TUU)","Riviera Maya  Tulum, Tulum Arpt (TUY)","Traverse City, Cherry Capital Arpt (TVC)","Thief River Falls, Thief River Falls Numicipal (TVF)","Lake Tahoe, Lake Tahoe Arpt (TVL)","Taveuni, Matei Arpt (TVU)","Toowoomba, Toowoomba Arpt (TWB)","Twin Falls, Sun Valley Regional (TWF)","Tawau, Tawau Arpt (TWU)","Taichung, Taichung Arpt (TXG)","Texarkana, Texarkana Municipal (TXK)","Berlin, Tegel Airport (TXL)","Tyler, Pounds Field (TYR)","Knoxville, McGhee Tyson Arpt (TYS)","Belice, Belize City Municipal (TZA)","Tuzla, Tuzla Intl Arpt (TZL)","South Andros, South Andros Arpt (TZN)","Trabzon, Trabzon Arpt (TZX)","San Luis Rio Colorado, San Luis Rio Colorado Municipa (UAC)","Narsarsuaq, Narsarsuaq Arpt (UAK)","Samburu, Samburu Airstrip (UAS)","Uberaba, Uberaba Airport (UBA)","Ube Jp, Ube Airport (UBJ)","Ubon Ratchath, Muang Ubon Arpt (UBP)","Columbus, Lowndes Cty Arpt (UBS)","Utica, Oneida County Arpt (UCA)","Lutsk, Lutsk Arpt (UCK)","Palm Desert, Bermuda Dunes Arpt (UDD)","Uden, Volkel Arpt (UDE)","Uberlandia, Eduardo Gomes Airprt (UDI)","Udine, Campoformido Arpt (UDN)","Udaipur, Dabok Airport (UDR)","Queenstown, Queenstown Arpt (UEE)","Kume Jima, Kumejima Arpt (UEO)","Urgench, Urgench Arpt (UGC)","Waukegan, Memorial Arpt (UGN)","Quincy, Baldwin Field (UIN)","Quito, Mariscal Arpt (UIO)","Quimper, Pluguffan Arpt (UIP)","Quirindi, Quirindi Arpt (UIR)","Detroit, Berz Macomb Arpt (UIZ)","Ukiah, Ukiah Arpt (UKI)","Ust Kamenogorsk, Ust Kamenogorsk Arpt (UKK)","Quakertown, Upper Bucks Arpt (UKT)","Kyoto, Kyoto Arpt (UKY)","Santiago De Chile, Los Cerrillos (ULC)","New Ulm, New Ulm Arpt (ULM)","Ulan Bator, Buyant Uhaa Airport (ULN)","Quilpie, Quilpie Arpt (ULP)","Umea, Umea Airport (UME)","Umuarama, Ernesto Geisel Arpt (UMU)","Sumy, Sumy Arpt (UMY)","Una BR, Una Airport (UNA)","Union Island, Union Island Arpt (UNI)","Unalakleet, Unalakleet Arpt (UNK)","Ranong, Ranong Arpt (UNN)","Unst, Baltasound Arpt (UNT)","University Oxford, University Oxford (UOX)","Ujung Pandang, Hasanudin Arpt (UPG)","Uruapan, Uruapan Arpt (UPN)","Urumqi, Urumqi Arpt (URC)","Uruguaina, Ruben Berta Arpt (URG)","Rouen, Boos Airport (URO)","Kursk, Kursk Arpt (URS)","Surat Thani, Surat Thani Arpt (URT)","Useless Loop, Useless Loop Arpt (USL)","Koh Samui, Koh Samui Arpt (USM)","Ulsan, Ulsan Arpt (USN)","Sancti Spiritus, Sancti Spiritus Arpt (USS)","St Augustine, St Augustine Arpt (UST)","Mutare, Mutare Arpt (UTA)","Udon Thani, Udon Thani Arpt (UTH)","Upington, Upington Airport (UTN)","U Tapao, U Tapao Arpt (UTP)","Umtata, K D Matamzima Arpt (UTT)","Queenstown, Queenstown Airport (UTW)","Ulan Ude, Ulan Ude Arpt (UUD)","Yuzhno Sakhalinsk, Yuzhno Sakhalinsk Arpt (UUS)","Santa Lucia, Hewanorra (UVF)","Kharga, Kharga Arpt (UVL)","Vaasa, Vaasa Arpt (VAA)","Valence, Chabeuil Airport (VAF)","Varginha, Maj Brig Trompowsky Arpt (VAG)","Vanimo, Vanimo Arpt (VAI)","Varna, Varna (VAR)","Vava U, Lupepau U Arpt (VAV)","Vardoe, Vardoe Luftan (VAW)","Brescia, Montichiari Arpt (VBS)","Visby, Visby Airport (VBY)","Venecia, Marco Polo Arpt (VCE)","San Pablo, Viracopos Arpt (VCP)","Victoria, Victoria Regional Arpt (VCT)","Victorville, George AFB (VCV)","Ovda, Ovda Arpt (VDA)","Fagernes, Valdres Arpt (VDB)","Vitoria Da Conquista, Vitoria Da Conquista Arpt (VDC)","Valverde, Hierro Arpt (VDE)","Vidalia, Vidalia Municipal Arpt (VDI)","Vadso, Vadso (VDS)","Valdez, Valdez Municipal Arpt (VDZ)","Vejle, Vejle Arpt (VEJ)","Vernal, Vernal Municipal (VEL)","Veracruz, Las Bajadas General Heriberto  (VER)","Cataratas Victoria, Victoria Falls Arpt (VFA)","Vologda, Vologda Arpt (VGD)","Vigo, Vigo Airport (VGO)","Las Vegas, Las Vegas North Air Terminal (VGT)","Vilhelmina, Vilhelmina Arpt (VHM)","Van Horn, Culberson Cty Arpt (VHN)","Vichy, Charmeil Arpt (VHY)","Villa Constitucion, Villa Constitucion Arpt (VIB)","Vicenza, Vicenza Arpt (VIC)","Viena, Vienna Intl Arpt (VIE)","Vieste, Vieste Arpt (VIF)","El Vigia, El Vigia Arpt (VIG)","Virgin Gorda, Virgin Gorda Arpt (VIJ)","Dakhla, Dakhla Airport (VIL)","Vinnitsa, Vinnitsa Arpt (VIN)","Durban, Virginia Arpt (VIR)","Visalia, Visalia Municipal (VIS)","Vitoria, Vitoria Arpt (VIT)","Vitoria, Eurico Sales Arpt (VIX)","Abingdon, Virginia Highlands Arpt (VJI)","Moscu, Vnukovo Arpt (VKO)","Vicksburg, Vicksburg Arpt (VKS)","Valencia, Valencia Arpt (VLC)","Valdosta, Valdosta Regional (VLD)","Port Vila, Bauerfield Arpt (VLI)","Valladolid, Valladolid Arpt (VLL)","Valencia, Valenica Arpt (VLN)","Vallejo, Stolport Arpt (VLO)","Valera, Carvajal Arpt (VLV)","Vallemi, Inc Arpt (VMI)","Vannes, Muecon Arpt (VNE)","Vilnius, Vilnius Arpt (VNO)","Varanasi, Babatpur Airport (VNS)","Vilanculos, Vilanculos Arpt (VNX)","Los Angeles, Los Angeles Van Nuys Arpt (VNY)","Volgograd, Volgograd Arpt (VOG)","Volos, Nea Anchialos Arpt (VOL)","Valparaiso / Ft Walton Beach, Northwest Florida Regional Arpt (VPS)","Varadero, Juan Gualberto Gomez Arpt (VRA)","Vero Beach, Vero Beach Municipal Arpt (VRB)","Varkaus, Varkaus (VRK)","Vila Real, Vila Real Arpt (VRL)","Verona, Verona Airport (VRN)","Matanzas, Kawama Arpt (VRO)","Villahermosa, Capt Carlos Rovirosa Perez (VSA)","Lugansk, Lugansk Arpt (VSG)","Estocolmo, Hasslo Airport (VST)","Vitebsk, Vitebsk Arpt (VTB)","Vientiane, Wattay Arpt (VTE)","Vishakhapatanam, Vishakhapatnam (VTZ)","Santa Cruz De La Sierra, Viru Viru Intl Arpt (VVI)","Vladivostok, Vladivostok Arpt (VVO)","Sao Vicente, San Pedro Airport (VXE)","Vaxjo, Vaxjo Airport (VXO)","Vryheid, Vryheid Arpt (VYD)","Peru, Illinois Valley Regional Arpt (VYS)","Wanganui, Wanganui Arpt (WAG)","Chincoteague, Wallops Arpt (WAL)","Waterford, Waterford Arpt (WAT)","Varsovia, Warsaw Intl Arpt (WAW)","Warwick, Warwick Arpt (WAZ)","Boulder, Boulder Municipal Arpt (WBU)","Enid, Woodring Municipal (WDG)","Windhoek, Hosea Kutako International Arp (WDH)","Weatherford, Parker County Airport (WEA)","Weihai, Weihai Arpu (WEH)","Weipa, Weipa Arpt (WEI)","Welkom, Welkom Arpt (WEL)","Wee Waa, Wee Waa Airport (WEW)","Wexford, Castlebridge Arpt (WEX)","Wagga Wagga, Forest Hill Arpt (WGA)","Walgett, Walgett Arpt (WGE)","Winchester, Winchester Arpt (WGO)","Wangaratta, Wangaratta (WGT)","Whakatane, Whakatane Arpt (WHK)","Welshpool, Welshpool Arpt (WHL)","Wick, Wick Arpt (WIC)","Nairobi, Wilson Airport (WIL)","Lancaster, Williams J Fox Arpt (WJF)","Wanaka, Wanaka Arpt (WKA)","Wakkanai, Hokkaido Arpt (WKJ)","Winfield, Arkansas City Arpt (WLD)","Wellington, Wellington Intl (WLG)","Selawik, Selawik Arpt (WLK)","Warrnambool, Warrnambool Arpt (WMB)","Winnemucca, Winnemucca Municipal Arpt (WMC)","Mountain Home, Mountain Home Arpt (WMH)","Windorah, Windorah Arpt (WNR)","Nawabshah, Nawabshah Arpt (WNS)","Wenzhou, Wenzhou Arpt (WNZ)","Wollongong, Wollongong Arpt (WOL)","Puerto Aisen, Puerto Aisen Arpt (WPA)","Whangarei, Whangarei Arpt (WRE)","Wrangell, Wrangell Seaplane Base (WRG)","Worland, Worland Municipal (WRL)","Wroclaw, Strachowice (WRO)","Westray, Westray Arpt (WRY)","Westerly, Westerly Municipal (WST)","Airlie Beach, Whitsunday Airstrip (WSY)","Westport, Westport Airport (WSZ)","Wuhan, Wuhan Arpt (WUH)","Walvis Bay, Rooikop Arpt (WVB)","Watsonville, Watsonville Municipal Arpt (WVI)","Waterville, Robert La Fleur Arpt (WVL)","Wilhelmshaven, Wilhelmshaven Arpt (WVN)","Cape May, Cape May Arpt (WWD)","Wewak, Boram Arpt (WWK)","Braintree, Wether Field RAF (WXF)","Whyalla, Whyalla Arpt (WYA)","Wyndham, Wyndham Arpt (WYN)","West Yellowstone, West Yellowstone Arpt (WYS)","Nassau, Seaplane Base Arpt (WZY)","Churchill, Churchill Rail Station (XAD)","Alamos, Alamos Arpt (XAL)","Chapeco, Chapeco Arpt (XAP)","Capreol, Capreol Rail Station (XAW)","Montreal, Dorval Rail Station (XAX)","Campbellton, Campbellton Rail Station (XAZ)","Brockville, Brockville Arpt (XBR)","Killineq, Killineq Arpt (XBW)","Chambord, Chambord Rail Station (XCI)","Chatham, Chatham Airport (XCM)","Colac, Colac Arpt (XCO)","Gaspe, Gaspe Rail Station (XDD)","Halifax, Halifax Rail Station (XDG)","Jasper, Jasper Rail Station (XDH)","Dunkerque, Dunkerque Arpt (XDK)","Chandler, Chandler Rail Station (XDL)","Drummondville, Drummondville Rail Station (XDM)","Grande Riviere, Grande-Riviere Rail Station (XDO)","Moncton, Moncton Rail Station (XDP)","London, London Rail Station (XDQ)","Ottawa, Ottawa Rail Station (XDS)","Hervey, Hervey Rail Station (XDU)","Prince George, Prince George Rail Station (XDV)","Prince Rupert, Prince Rupert Rail Station (XDW)","Sarnia, Sarina Rail Station (XDX)","Sudbury, Sudbury Junction Rail Station (XDY)","The Pas, The Pas Rail Station (XDZ)","Vancouver, Vancouver Rail Station (XEA)","Windsor, Windsor Rail Station (XEC)","Lac Edouard, Lac Edouard Rail Station (XEE)","Winnipeg, Winnipeg Rail Station (XEF)","Kingston, Kingston Rail Station (XEG)","Ladysmith, Ladysmith Rail Station (XEH)","Langford, Langford Rail Station (XEJ)","Melville, Melville Rail Station (XEK)","New Carlisle, New Carlisle Rail Station (XEL)","New Richmond, New Richmond Rail Station (XEM)","Estrasburgo, Strasbourg Bus Service (XER)","Estocolmo, Stockholm Rail Station (XEV)","Estocolmo, Flemingsberg Rail Station (XEW)","Miramichi, Newcastle Rail Station (XEY)","Sodertalje, Sodertalje S Rail Station (XEZ)","Stratford, Stratford Rail Station (XFD)","Parent, Parent Rail Station (XFE)","Perce, Perce Rail Station (XFG)","Port Daniel, Port-Daniel Rail Station (XFI)","Eskilstuna, Eskilstuna Rail Station (XFJ)","Senneterre, Senneterre Rail Station (XFK)","Shawinigan, Shawinigan Rail Station (XFL)","Shawnigan, Shawnigan Rail Station (XFM)","Taschereau, Taschereau Rail Station (XFO)","Malmo, Malmo Railway Service (XFP)","Weymont, Weymont Rail Station (XFQ)","Malmo, Malmo South Railway Service (XFR)","Alexandria, Alexandria Rail Station (XFS)","Gavle, Tierp Rail Station (XFU)","Brantford, Brantford Rail Station (XFV)","Quebec, Sainte-Foy Rail Station (XFY)","Quebec, Charny Rail Station (XFZ)","Lund C, Lund C Rail Station (XGC)","Kristiansand, Arendal Rail Station (XGD)","Kristiansund, Andalsnes Rail Station (XGI)","Cobourg, Cobourg Rail Station (XGJ)","Coteau, Coteau Rail Station (XGK)","Leicester, Grantham Rail Station (XGM)","Gol City, Dombas Rail Station (XGP)","Oslo, Asker Rail Station (XGU)","Gananoque, Gananoque Rail Station (XGW)","Grimsby, Grimsby Rail Station (XGY)","Oslo, Honefoss Rail Station (XHF)","Georgetown, Georgetown Rail Station (XHM)","Chemainus, Chemainus Rail Station (XHS)","Guelph, Guelph Rail Station (XIA)","Ingersoll, Ingersoll Rail Station (XIB)","Maxville, Maxville Rail Station (XID)","Napanee, Napanee Rail Station (XIF)","Prescott, Prescott Rail Station (XII)","Saint Hyacinthe, Saint Hyacinthe Rail Station (XIM)","St Marys, St Marys Rail Station (XIO)","Woodstock, Woodstock Rail Station (XIP)","Xian, Xianyang Arpt (XIY)","Joliette, Joliette Rail Station (XJL)","Jonquiere, Jonquiere Rail Station (XJQ)","Oslo, Kongsberg Rail Station (XKB)","Stavanger, Sandnes Rail Station (XKC)","Oslo, Halden Rail Station (XKD)","Oslo, Rena Rail Station (XKE)","Oslo, Fredrikstad Rail Station (XKF)","Steinkjer, Grong Rail Station (XKG)","Oslo, Lillestrom Rail Station (XKI)","Steinkjer, Steinkjer Rail Station (XKJ)","Skien, Larvik Rail Station (XKK)","Oslo, Moss Rail Station (XKM)","Bergen, Finse Rail Station (XKN)","Skien, Porsgrunn Rail Station (XKP)","Oslo, Sarpsborg Rail Station (XKQ)","Kristiansand, Kristiansand Rail Station (XKR)","Sackville, Sackville Rail Station (XKV)","Gol City, Vinstra Rail Station (XKZ)","Quebec, Quebec City Rail Station (XLJ)","Quebec, Levis Rail Station (XLK)","Montreal, Saint Lambert Rail Station (XLM)","Matapedia, Matapedia Rail Station (XLP)","Toronto, Guildwood Rail Station (XLQ)","Niagara Falls, Niagara Falls Rail Station (XLV)","Aldershot, Aldershot Rail Station (XLY)","Truro, Truro Rail Station (XLZ)","Xiamen, Xiamen Intl Airport (XMN)","Fayetteville, Northwest Arkansas Regional Ar (XNA)","Oslo, Drammen Rail Station (XND)","Nottingham, Nottingham Rail Station (XNM)","Teesside, Northallerton Rail Station (XNO)","Birmingham, Nuneaton Rail Station (XNV)","Trondheim, Oppdal Rail Station (XOD)","Oakville, Oakville Rail Station (XOK)","Carleton, Carleton Rail Station (XON)","Poitiers, Poitiers Rail Station (XOP)","Stavanger, Sira Rail Station (XOQ)","Gol City, Otta Rail Station (XOR)","Parksville, Parksville Rail Station (XPB)","Carlisle, Penrith Rail Station (XPF)","Paris, Gare du Nord Railway Station (XPG)","Port Hope, Port Hope Rail Station (XPH)","Pukatawagan, Pukatawagan Rail Station (XPK)","Comayagua, Palmerola Air Base (XPL)","Brampton, Brampton Rail Station (XPN)","Preston, Preston Rail Station (XPT)","Pointe Aux Trembles, Pointe-aux-Trembles Rail Stati (XPX)","Berwick Upon Tweed, Berwick Rail Station (XQG)","Blackpool, Lancaster Rail Station (XQL)","Quepos, Quepos Arpt (XQP)","Qualicum, Qualicum Arpt (XQU)","Chester, Runcorn Rail Station (XRC)","Stavanger, Egersund Rail Station (XRD)","Marsella, Marseille Rail Station (XRF)","Riviere A Pierre, Pine Ridge Rail Station (XRP)","Rugby, Rugby Rail Station (XRU)","Jerez De La Frontera, La Parra Arpt (XRY)","South Caicos, South Caicos Intl Arpt (XSC)","Tours, St Pierre des Corps/Tours Rail (XSH)","Salisbury, Salisbury Rail Station (XSR)","Harrogate, Thirsk Rail Station (XTK)","Strathroy, Strathroy Rail Station (XTY)","Stockport, Stockport Rail Station (XVA)","Birmingham, Stafford Rail Station (XVB)","Manchester, Crewe Rail Station (XVC)","Teesside, Darlington Rail Station (XVG)","Cambridge, Peterborough Rail Station (XVH)","Stevenage, Stevenage Rail Station (XVJ)","Bergen, Voss Rail Station (XVK)","Newcastle, Durham Rail Station (XVU)","Belleville, Belleville Rail Station (XVV)","Wolverhampton, Belleville Rail Station (XVW)","Watford, Watford Rail Station (XWA)","Wakefield Westgate, Wakefield Westgate Rail Statio (XWD)","Stoke On Trent, Stroke on Trent Rail Station (XWH)","Gotemburgo, Gothenburg Rail (XWL)","Orebro Bofors, Hallsberg Rail Station (XWM)","Liverpool, Warrington B Q Rail Station (XWN)","Helsingborg, Hassleholm Rail Station (XWP)","Orebro Bofors, Orebro Bofors Railway Service (XWR)","Halmstad, Varberg Rail Station (XWV)","Wyoming, Wyoming Rail Station (XWY)","Orebro Bofors, Degerfors Rail Station (XXD)","Lille Hammer, Lillehammer Rail Station (XXL)","Lidkoping, Mjolby Rail Station (XXM)","Borlange, Leksand Rail Station (XXO)","Borlange, Hedemora Rail Station (XXU)","Ronneby, Ronneby Railway Service (XXY)","Sundsvall, Sundsvall Rail Service (XXZ)","Jonkoping, Herrljunga Rail Station (XYC)","Lyon, Lyon Rail Station (XYD)","Jonkoping, Falkoping Rail Station (XYF)","Norrkoping, Norrkoping Railway Service (XYK)","Halmstad, Falkenberg Rail Station (XYM)","Karlstad, Kristinehamn Rail Station (XYN)","Ronneby, Karlshamn Rail Station (XYO)","Borlange, Avesta Krylbo Rail Station (XYP)","Kristianstad, Solvesborg Rail Station (XYU)","Vasteras, Sala Rail Station (XYX)","Karlstad, Arvika Rail Station (XYY)","Sundsvall, Harnosand Rail Station (XYZ)","Casselman, Casselman Rail Station (XZB)","Glencoe, Glencoe Rail Station (XZC)","Amherst, Amherst Rail Station (XZK)","Edmonton, Edmonton Rail Station (XZL)","Oslo, Oslo Central Station (XZO)","Trondheim, Trondheim Rail Station (XZT)","Fort Frances, Fort Frances Municipal (YAG)","Yakutat, Yakutat Arpt (YAK)","Sault Ste Marie, Sault Ste Marie Arpt (YAM)","Yaounde, Yaounde Airport (YAO)","St Anthony, St Anthony Arpt (YAY)","Tofino, Tofino Arpt (YAZ)","Banff, Banff Arpt (YBA)","Pelly Bay, Townsite Arpt (YBB)","Baie Comeau, Baie Comeau Arpt (YBC)","New Westminster, New Westminster Rail Station (YBD)","Bagotville, Bagotville Arpt (YBG)","Campbell River, Campbell River Municipal (YBL)","Brandon, Brandon Municipal Arpt (YBR)","Blanc Sablon, Blanc Sablon Arpt (YBX)","Toronto, Toronto Downtown Rail Station (YBZ)","Courtenay, Courtenay Rail Station (YCA)","Cambridge Bay, Cambridge Bay Arpt (YCB)","Cornwall, Cornwall Regional Arpt (YCC)","Nanaimo, Nanaimo Arpt (YCD)","Castlegar, Ralph West Arpt (YCG)","Miramichi, Miramichi Arpt (YCH)","Colville, Colville Municipal (YCK)","Charlo, Charlo Municipal Arpt (YCL)","St Catherines, St Catharines Rail Station (YCM)","Cochrane, Cochrane Rail Station (YCN)","Chilliwack, Chilliwack Arpt (YCW)","Clyde River, Clyde River (YCY)","Fairmont Springs, Fairmont Springs Arpt (YCZ)","Dawson City, Dawson City Arpt (YDA)","Deer Lake, Deer Lake Municipal (YDF)","Dease Lake, Dease Lake Arpt (YDL)","Dauphin, Dauphin Rail Station (YDN)","Dawson Creek, Dawson Creek Arpt (YDQ)","Edmonton, Namao Field (YED)","Edmonton, Edmonton Intl Arpt (YEG)","Bursa, Yenisehir Arpt (YEI)","Arviat, Arviat Arpt (YEK)","Elliot Lake, Elliot Lake Arpt (YEL)","Estevan, Estevan Arpt (YEN)","Edson, Edson Arpt (YET)","Inuvik, Inuvik Mike Zubko Arpt (YEV)","Amos, Amos Rail Station (YEY)","Iqaluit, Iqaluit Arpt (YFB)","Fredericton, Fredericton Municipal (YFC)","Flin Flon, Flin Flon Municipal Arpt (YFO)","Yonago, Miho Arpt (YGJ)","Kingston, Norman Rodgers Arpt (YGK)","La Grande, La Grande Municipal Arpt (YGL)","Gaspe, Gaspe Municipal Arpt (YGP)","Iles De Madeleine, House Harbour Arpt (YGR)","Havre St Pierre, Havre St Pierre Municipal Arpt (YGV)","Gillam, Gillam Rail Station (YGX)","Hudson Bay, Hudson Bay Rail Station (YHB)","Dryden, Dryden Municipal (YHD)","Hope, Hope Arpt (YHE)","Hearst, Hearst Municipal Arpt (YHF)","Charlottetown, Charlottetown Municipal Arpt (YHG)","Campbell River, Harbor Airport (YHH)","Hamilton, Civic Airport (YHM)","Hornepayne, Hornepayne Rail Station (YHN)","Chevery, Chevery Arpt (YHR)","Sechelt, Sechelt Arpt (YHS)","Montreal, St Hubert Arpt (YHU)","Hay River, Hay River Municipal Arpt (YHY)","Halifax, Halifax Intl (YHZ)","Atikokan, Atikokan Municipal Arpt (YIB)","Pakuashipi, Pakuashipi Arpt (YIF)","Detroit, Willow Run Arpt (YIP)","Jasper, Jasper Airport (YJA)","Stephenville, Stephenville Municipal (YJT)","Kamloops, Davie Fulton Arpt (YKA)","Kitchener, Kitchener Waterloo Regional (YKF)","Schefferville, Schefferville Arpt (YKL)","Yakima, Yakima Terminal Arpt (YKM)","Yankton, Chan Gurney Municipal (YKN)","Waskaganish, Waskaganish Arpt (YKQ)","Chisasibi, Chisasibi Arpt (YKU)","Kirkland, Kirkland Lake Municipal Arpt (YKX)","Saskatoon, Kindersley Arpt (YKY)","Toronto, Buttonville Arpt (YKZ)","Chapleau, Chapleau Rail Station (YLD)","Ylivieska, Ylivieska Arpt (YLI)","Meadow Lake, Meadow Lake Arpt (YLJ)","Lloydminster, Lloydminster Arpt (YLL)","La Tuque, La Tuque Rail Station (YLQ)","Kelowna, Ellison Field (YLW)","Merritt, Merritt Arpt (YMB)","Matane, Matane Airport (YME)","Manitouwadge, Manitouwadge Municipal Arpt (YMG)","Minaki, Minaki Rail Station (YMI)","Regina, Moose Jaw Arpt (YMJ)","Ft McMurray, Ft McMurray Municipal Arpt (YMM)","Moosonee, Moosonee Arpt (YMO)","Chibougamau, Chibougamau Arpt (YMT)","Montreal, Mirabel Intl Arpt (YMX)","Montreal, Montreal Downtown Rail Station (YMY)","Xining, Xining (XNN)","LogroÃ±o, LogroÃ±o (RJL)","Toronto, Toronto Metropolitan Area (YTO)","Buenos Aires, Ezeiza Intl (EZE)","Buenos Aires, Jorge Newbery (AEP)","Nueva York, Newark Airport (EWR)","Washington D. C., Baltimore Washington Airport (BWI)","Villavicencio, Vanguardia Arpt (VVC)","Isla Providencia , El Embrujo Arpt (PVA)","Pasto, Antonio Narino Arpt (PSO)","IbaguÃ©, Perales Arpt (IBE)","Armenia, El Eden Arpt (AXM)","Manizales, La Nubia Arpt (MZL)","Riohacha, Almirante Padilla Arpt (RCH)","Tumaco, La Florida Arpt (TCO)","QuibdÃ³, El Carano Airpt (UIB)","Buenaventura, Gerardo Tobar Lopez Arpt (BUN)","Neiva, Benito Salas Arpt (NVA)","Cartagena, Rafael Nunez Arpt (CTG)","Puerto Asis, Tres de Mayo Arpt (PUU)","Capurgana, Capurgana Arpt (CPB)","Villa GarzÃ³n, Villa Garzon Arpt (VGZ)","Paipa, Juan Jose Rondon Arpt (PYA)","Rosario, Rosario (ROS)","Tarapoto, Tarapoto (TPP)","Esmeraldas, Esmeraldas (ESM)","Florencia, Gustavo Artunduaga Paredes Arpt (FLA)","Killen, Texas, Fort Hood Killen Texas (GRK)","Mendoza, Arp. Internacional Gobernador Francisco Gabrielli (MDZ)","Cordoba, Pajas Blancas (COR)","Salta, MartÃ­n Miguel de GÃ¼emes Arpt (SLA)","Aruba, Aruba (AUA)","San Juan, San Juan (UAQ)","Baku, Heydar Aliyev International Apt (GYD)","Tucuman,  Arp. International Teniente General BenjamÃ­n Mati (TUC)","Tame Arauca, Aeropuerto Gustavo Vargas (TME)","Yopal, Aeropuerto El Alcaravan (EYP)","Calafate, Aeropuerto El Calafate (FTE)","Changzhou, Aeropuerto Changzhou Benniu (CZX)","Valledupar, Aeropuerto Alfonso Lopez Pumarejo (VUP)","M Bahiakro, Aeropuerto M Bahiakro (XMB)","Comodoro Rivadavia, Comodoro Rivadavia Aeropuerto (CRD)","Puerto Iguazu, Cataratas del IguazÃº Intl Aeropuerto (IGR)","Santiago del Estero, Aeropuerto Vicecomodoro Angel de la Paz Aragones (SDE)","Termas de Rio Hondo, Aeropuerto Intl Termas de RÃ­o Hondo (RHD)","Malargue, Aeropuerto Malargue (LGS)","Villa Mercedes, Aeropuerto Villa Mercedes (VME)","Rio Cuarto, Aeropuerto de Rio Cuarto  (RCU)","Villa Gesell, Aeropuerto de Villa Gesell (VGL)","Beijing, Beijing Capital Arpt (PEK)","Bariloche, Aeropuerto Internacional San Carlos de Bariloche (BRC)","ParanÃ¡,  Aeropuerto General Justo JosÃ© de Urquiza  (PRA)","Arauca, Aeropuerto Santiago Perez Quiroz (AUC)","Formosa, Aeropuerto Formosa International (FMA)","Resistencia, Aeropuerto Internacional de Resistencia (RES)","Catamarca, Aeropuerto Coronel Felipe Varela International (CTC)","Ushuaia, Aeropuerto Malvinas Argentinas International (USH)","Jujuy, Aeropuerto Internacional Gobernador Horacio Guzman (JUJ)","San Luis, Aeropuerto Brigadier Mayor Cesar Raul Ojeda (LUQ)","La Rioja, Aeropuerto Capitan Vicente Almandos Almonacid (IRJ)","Posadas, Aeropuerto Libertador General Jose de San Martin (PSS)","Viedma, Aeropuerto Gobernador Edgardo Castello (VDM)","San Rafael, Aeropuerto San Rafael (AFA)","San Martin de los Andes, Aeropuerto Aviador Carlos Campos (CPC)","Rio Gallegos, Aeropuerto Int Piloto Civil Norberto Fernandez (RGL)","Santa Rosa, Aeropuerto de Santa Rosa (RSA)","Puerto Madryn, Aeropuerto El Tehuelche (PMY)","Santa Fe, Aeropuerto de Sauce Viejo (SFN)","Neuquen, Aeropuerto Internacional Peron (NQN)","Alicante, EstaciÃ³n de Tren Alicante (YJE)","Mar del Plata, Aeropuerto Internacional Astor Piazzolla (MDQ)","Madrid, EstaciÃ³n de Tren Atocha (XOC)","Barcelona, EstaciÃ³n de Tren Sants (YJB)","Madrid, EstaciÃ³n de Tren Chamartin (XTI)","Cayo Coco, Aeropuerto Internacional de Jardines del Rey (CCC)","Cayo Las Brujas, Aeropuerto Cayo Las Brujas (BWW)","Filadelfia, 30th Street Station, EstaciÃ³n de tren (ZFV)","Cajamarca, Aeropuerto Mayor General FAP Armando Revoredo Igle (CJA)","Talara, CapitÃ¡n FAP VÃ­ctor Montes Arias (TYL)","Ayacucho, Coronel FAP Alfredo Mendivil Duarte (AYP)","Jaen, Aeropuerto Shumba (JAE)","San AndrÃ©s Isla, Muelle Toninos (SAD)","Isla Providencia , Muelle Municipal (PRO)","Zaragoza, EstaciÃ³n de Tren Delicias (XZZ)","Bahia Blanca, Aeropuerto Comandante Espora (BHI)","Washington D. C., Aeropuerto Akron (AKO)","PanamÃ¡, Aeropuerto Internacional PanamÃ¡ PacÃ­fico (BLB)","Malaga, Malaga Train Station Airport (YJM)","Albacete, EstaciÃ³n de Trenes de Albacete (EEM)","Almeira, Almeria Railway Station (AMR)","Bilbao, EstaciÃ³n de Abando Indalecio Prieto (YJI)","Murcia, EstaciÃ³n de Murcia del Carmen (XUT)","Oviedo, EstaciÃ³n de Oviedo (OVI)","Valladolid, EstaciÃ³n de Valladolid-Campo Grande (XIV)","Cordova, EstaciÃ³n de Trenes Central de CÃ³rdoba (XOJ)","Granada, EstaciÃ³n de trenes de Granada (YJG)","Jerez De La Frontera, EstaciÃ³n de Jerez de la Frontera (YJW)","Pamplona, EstaciÃ³n de Trenes de Pamplona (EEP)","Sevilla, EstaciÃ³n de Sevilla - Santa Justa (XQA)","Valencia, EstaciÃ³n del Norte (YJV)","Corrientes, Aerop. Internacional Doctor Fernando Piragine Nive (CNQ)","Jauja, Jauja (JAU)","Saransk, Aeropuerto de Saransk (SKX)","Uyuni, Aeropuerto Joya Andina (UYU)","Rurrenabaque, Rurrenabaque Aeropuerto (RBQ)","San Ignacio de Velasco, San Ignacio de Velasco (SNG)","Puerto Suarez, Puerto Suarez (PSZ)","Yacuiba, Yacuiba Aeropuerto (BYC)","Riberalta, Riberalta (RIB)","NuquÃ­, Aeropuerto de Nuqui Reyes Murillo (NQU)","La Macarena, Aeropuerto La macarena Javier NoreÃ±a Valencia (LMC)","Bahia Solano, Aeropuerto Bahia Solano Jose Celestino Mutis (BSC)","Guayaramerin, Emilio Beltran Airport (GYA)","Cobija, Capitan Anibal Arab (CIJ)","PotosÃ­, Capitan Nicolas Rojas (POI)","Trelew, Aeropuerto Internacional Almirante A. Zar (REL)","Oruro, Aeropuerto Juan Mendoza (ORU)","Angeles Mabalacat, Internacional Clark (CRK)","Cartagena, Muelle La Bodeguita, Muelle de Los Pegasos (CO2)","Mucura, Mucura (MCR)","Santa Rosa, Aeropuerto Santa Rosa (ETR)","Isla Tintipan, Isla Tintipan  (CO4)","Araracuara, Araracuara (ACR)","Corozal, Las Brujas (CZU)","Guapi, Juan Casiano (GPI)","Aguachica, Hacaritama (HAY)","La Chorrera, Aeropuerto la Chorrera (LCR)","La Pedrera, Aeropuerto la Pedrera (LPD)","Puerto LeguÃ­zamo, Caucaya (LQM)","Mompos, San Bernardo (MMP)","MitÃº, Fabio A Leon Bantley (MVP)","Puerto CarreÃ±o, German Olano (PCR)","Puerto InÃ­rida, Obando (PDA)","Pitalito, Aeropuerto de Pitalito (PTX)","Saravena, Los Colonizadores (RVE)","San JosÃ© del Guaviare, Jorge E Gonzalez T (SJE)","San Vicente del CaguÃ¡n, Eduardo Falla Solano (SVI)","Tarapaca, Aeropuerto TarapacÃ¡ (TCD)","Tolu, Golfo de Morrosquillo (TLU)","Buenos Aires, El Palomar (EPA)","Jericoacoara, Aeropuerto Regional Comandante Ariston Pessoa (JJD)","Isla Palma, Isla Palma (A10)","Ciudad de PanamÃ¡, Florida Beach Intl (ECP)","Berlin, Brandenburg (BER)"];

    let getCities = external_file_Cities;

    let getAirports = external_file_Airports;



    let options_origen = {

      data: external_paradas,

      getValue: 'descripcion_parada',

      list: {

        maxNumberOfElements: 10,

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          let value = $('#origen').getSelectedItemData().idparada;

          $('#origen-id').val(value).trigger('change');

        }

      }

    };

    $('#origen').easyAutocomplete(options_origen);



    // Paquetes

    let options_paquetes_origen = {

      data: getAirports,

      list: {

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          var selection = $('#p-origen').getSelectedItemData();

          var sel = selection.substr(selection.length - 5);

          var value = sel.substring(1, sel.length - 1);

          $('#p-origen-id').val(value).trigger('change');

        }

      }

    }

    $('#p-origen').easyAutocomplete(options_paquetes_origen);



    /*=====  End of AUTO COMPLETE ORIGEN  ======*/







    /*=============================================

    =            AUTO COMPLETE DESTINO            =

    =============================================*/

    // BUS

    let options_destino = {

      // url: `${URL_DOM}/paradas.json`,

      data: external_paradas,

      getValue: 'descripcion_parada',

      list: {

        maxNumberOfElements: 10,

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          let value = $('#destino').getSelectedItemData().idparada;

          $('#destino-id').val(value).trigger('change');



        }

      }

    };

    $('#destino').easyAutocomplete(options_destino);

    // HOTEL

    let options_hotel_destino = {

      data: getCities,

      list: {

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          var selection = $('#h-destino').getSelectedItemData();

          var sel = selection.substr(selection.length - 5);

          var value = sel.substring(1, sel.length - 1);

          $('#h-destino-id').val(value).trigger('change');

        }

      }

    }

    $('#h-destino').easyAutocomplete(options_hotel_destino);

    // Actividades

    let options_actividades_destino = {

      data: getCities,

      list: {

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          var selection = $('#a-destino').getSelectedItemData();

          var sel = selection.substr(selection.length - 5);

          var value = sel.substring(1, sel.length - 1);

          $('#a-destino-id').val(value).trigger('change');

        }

      }

    }

    $('#a-destino').easyAutocomplete(options_actividades_destino);

    // Paquetes

    let options_paquetes_destino = {

      data: getAirports,

      list: {

        match: {

          enabled: true

        },

        onSelectItemEvent: function() {

          var selection = $('#p-destino').getSelectedItemData();

          var sel = selection.substr(selection.length - 5);

          var value = sel.substring(1, sel.length - 1);

          $('#p-destino-id').val(value).trigger('change');

        }

      }

    }

    $('#p-destino').easyAutocomplete(options_paquetes_destino);

    /*=====  End of AUTO COMPLETE DESTINO  ======*/







    /*=============================================

    =            CUPON VALIDATION             =

    =============================================*/

    let codigoDescuento = document.querySelectorAll('.c-descuento');

    let fecha_ida = document.getElementById('bus-entrada-id');

    let pasajeros_pase = document.getElementById('pasajeros');

    

    codigoDescuento.forEach(cdescuento => {

      let codigoDescuentoContainer = cdescuento.parentNode;

      // ON CLICK BTN 

      let btnSubmit = cdescuento.nextElementSibling;

      btnSubmit.addEventListener('click', () => {

        if(fecha_ida.value == ''){

          $( ".response_codigo" ).text("Debe ingresar fecha de ida");

          setTimeout(function() { $(".response_codigo").text(""); }, 5000);

        }else {

          $.ajax({

            url: url_api + "pases/codigo_validar?CodigoDescuento=" + cdescuento.value + "&Fecha=" + fecha_ida.value.split('/').join('-')+"&Pasajeros="+pasajeros_pase.value,

            type: "GET",

            headers: {

              'X-API-KEY': session_api,

              'Content-Type':'application/json'

            }

          }).done(function(data) {

              codigoDescuentoContainer.classList.remove('validation');

              cdescuento.classList.add('cupon-valido');

              cdescuento.style.color = '#b6c62d';



              $(".response_codigo").text("Código válido");

              setTimeout(function() { $(".response_codigo").text(""); }, 5000);



              $(".IdPase").val(data.IdPase);

          }).fail(function(jqXHR, errorThrown) {

            codigoDescuentoContainer.classList.add('validation');

            cdescuento.classList.remove('cupon-valido');

            cdescuento.style.color = '#f44336';



            try {

              var errorData = JSON.parse(jqXHR.responseText);

              $( ".response_codigo" ).text(errorData.message_detail);

              setTimeout(function() { $(".response_codigo").text(""); }, 5000);

            } catch (e) {

              $( ".response_codigo" ).text("Error en la solicitud: " + errorThrown);

              setTimeout(function() { $(".response_codigo").text(""); }, 5000);

            }

          });

        }

      });

    });

    /*=====  End of CUPON VALIDATION   ======*/









    /*=============================================

    =            SEND FORM             =

    =============================================*/

    // FORMULARIO DE ENVIO BUS

    const searchBus = (empresa, logo, env = 'test') => {

      // VALUE INPUT BUS

      let inputOrigen = document.getElementById('origen-id');

      let inputOrigenContainer = inputOrigen.parentNode.parentNode.parentNode.parentNode

      let inputDestino = document.getElementById('destino-id');

      let inputDestinoContainer = inputDestino.parentNode.parentNode.parentNode.parentNode

      let inputFechaIda = document.getElementById('bus-ida');

      let inputFechaIdaId = document.getElementById('bus-entrada-id');

      let inputFechaIdaContainer = inputFechaIda.parentNode.parentNode.parentNode.parentNode

      let inputFechaVuelta = document.getElementById('bus-vuelta');

      let inputFechaVueltaId = document.getElementById('bus-salida-id');

      let codigoDescuento = document.getElementById('b-descuento');

      let cliente_empresarial = document.getElementById('IdPase');

      let pasajerosTotal = totalPass.value





      if (inputOrigen.value.trim() === '') {

        inputOrigenContainer.classList.add('validation')

      } else {

        inputOrigenContainer.classList.remove('validation')

      }

      if (inputDestino.value.trim() === '') {

        inputDestinoContainer.classList.add('validation')

      } else {

        inputDestinoContainer.classList.remove('validation')

      }

      if (inputFechaIda.value.trim() === '') {

        inputFechaIdaContainer.classList.add('validation')

      } else {

        inputFechaIdaContainer.classList.remove('validation')

      }

      if (inputOrigen.value.trim() === '' || inputDestino.value.trim() === '' || inputFechaIda.value.trim() === '') {

        return;

      }
      let cdescuento = codigoDescuento.value === '' ? '0' : codigoDescuento.value;

      inputOrigenContainer.classList.remove('validation')
      inputDestinoContainer.classList.remove('validation')
      inputFechaIdaContainer.classList.remove('validation')
      let viaje = {}
      
      viaje = {
        origen: inputOrigen.value,
        destino: inputDestino.value,
        ida: inputFechaIdaId.value,
        vuelta: inputFechaVueltaId.value,
        descuento: cdescuento,
        pass: pasajerosTotal,
      }
      

      const {
        origen,
        destino,
        ida,
        vuelta,
        descuento,
        pass
      } = viaje

      let BASE_URL = '';

      if(env === 'test') {
        BASE_URL = 'http://checkout.viatesting.com.ar';
      }else{
        BASE_URL = 'https://check.busplus.com.ar';
      }

      if(vuelta.trim() === ''){
        BASE_URL = BASE_URL + `/viaje/${origen}/${ida}/${pass}/${destino}/${descuento}`;
      }else{
        BASE_URL = BASE_URL + `/viajes/${origen}/${ida}/${pass}/${destino}/${vuelta}/${descuento}`;
      }


      if(empresa === 'undefined')

      {

        empresa = '';

      }



      if(logo === 'undefined')

      {

        logo = '';

      }



      BASE_URL = BASE_URL + '?emp=' + empresa;



      window.open(BASE_URL);

    }

    const searchHotel = () => {

      // totalHotelPass

      // INPUTS

      let destinoId = document.getElementById('h-destino-id');

      let destinoIdContainer = destinoId.parentNode.parentNode.parentNode.parentNode

      let fechaEntrada = document.getElementById('hotel-entrada');

      let fechaEntradaId = document.getElementById('h-entrada-id');

      let fechaEntradaContainer = fechaEntrada.parentNode.parentNode.parentNode.parentNode

      let fechaSalida = document.getElementById('hotel-salida');

      let fechaSalidaId = document.getElementById('h-salida-id');

      let fechaSalidaContainer = fechaSalida.parentNode.parentNode.parentNode.parentNode

      let habitaciones = totalHotelPass.value;

      let url;

      if (destinoId.value.trim() === '') {

        destinoIdContainer.classList.add('validation')



      } else {

        destinoIdContainer.classList.remove('validation')

      }

      if (fechaSalida.value.trim() === '') {

        fechaSalidaContainer.classList.add('validation')

      } else {

        fechaSalidaContainer.classList.remove('validation')

      }

      if (fechaEntrada.value.trim() === '') {

        fechaEntradaContainer.classList.add('validation')

      } else {

        fechaEntradaContainer.classList.remove('validation')

      }

      if (destinoId.value.trim() === '' || fechaSalida.value.trim() === '') {

        return;

      }



      /* if (totalHotelNinos.value > 0) {

        if (totalHotelNinos.value == 1) {

          url = '2'

        }

        if (totalHotelNinos.value == 2) {

          url = '2-4'

        }

        if (totalHotelNinos.value == 3) {

          url = '1-3-5'

        }

        if (totalHotelNinos.value == 4) {

          url = '0-2-4-6'

        }

        habitaciones = totalHotelAdultos.value + `-` + url;

      } else {

        habitaciones = totalHotelAdultos.value

      } */





      let hotel = {

        destino: destinoId.value,

        entrada: fechaEntradaId.value,

        salida: fechaSalidaId.value,

        pasajeros: totalHotelPass.value,

        habi: habitaciones

      }

      const {

        destino,

        entrada,

        salida,

        pasajeros,

        habi

      } = hotel

      let BASE_URL = 'https://reservas.busplus.com.ar/es-ES/Hotel/';

      let busqueda = destino + '/' + entrada + '/' + salida + '/' + habi + '/NA/gyra-show-B2C---------  '

      window.open(BASE_URL + busqueda);

    }

    const searchActividades = () => {

      // VALUE INPUTS

      let destinoId = document.getElementById('a-destino-id');

      let destinoInput = document.getElementById('a-destino')

      let destinoContainer = destinoInput.parentNode.parentNode.parentNode.parentNode.parentNode

      let fechaEntrada = document.getElementById('actividades-entrada');

      let fechaEntradaId = document.getElementById('actividades-entrada-id');

      let fechaEntradaContainer = fechaEntrada.parentNode.parentNode.parentNode.parentNode

      let fechaSalida = document.getElementById('actividades-salida');

      let fechaSalidaId = document.getElementById('actividades-salida-id');

      let fechaSalidaContainer = fechaSalida.parentNode.parentNode.parentNode.parentNode

      //Validaciones



      if (destinoId.value.trim() === '' || destinoInput.value.trim() === '') {

        destinoContainer.classList.add('validation')

      } else {

        destinoContainer.classList.remove('validation')

      }

      if (fechaSalida.value.trim() === '') {

        fechaSalidaContainer.classList.add('validation')

      } else {

        fechaSalidaContainer.classList.remove('validation')

      }

      if (fechaEntrada.value.trim() === '') {

        fechaEntradaContainer.classList.add('validation')

      } else {

        fechaEntradaContainer.classList.remove('validation')

      }

      if (destinoId.value.trim() === '' || fechaSalida.value.trim() === '' || destinoInput.value.trim() === '') {

        return;

      } else {



      }

      let actividades = {

        destino: destinoId.value,

        entrada: fechaEntradaId.value,

        salida: fechaSalidaId.value

      }



      const {

        destino,

        entrada,

        salida

      } = actividades

      let BASE_URL = 'https://reservas.busplus.com.ar/es-ES/Extras/';

      let busqueda = destino + '/NA/' + entrada + '/' + salida + '/gyra-show-B2C---------  '

      window.open(BASE_URL + busqueda);

    }

    const searchPaquetes = () => {

      // VALUES

      let inputOrigenId = document.getElementById('p-origen-id');

      let inputOrigenContainer = inputOrigenId.parentNode.parentNode.parentNode.parentNode

      let inputDestinoId = document.getElementById('p-destino-id');

      let inputDestinoContainer = inputDestinoId.parentNode.parentNode.parentNode.parentNode

      let inputIda = document.getElementById('p-ida');

      let inputIdaId = document.getElementById('p-ida-id');

      let inputIdaContainer = inputIda.parentNode.parentNode.parentNode.parentNode

      let inputVuelta = document.getElementById('p-vuelta');

      let inputVueltaId = document.getElementById('p-vuelta-id');

      let inputVueltaContainer = inputVuelta.parentNode.parentNode.parentNode.parentNode

      let habitaciones = totalPaquetePass.value;

      let url;





      if (inputOrigenId.value.trim() === '') {

        inputOrigenContainer.classList.add('validation')

      } else {

        inputOrigenContainer.classList.remove('validation')

      }

      if (inputDestinoId.value.trim() === '') {

        inputDestinoContainer.classList.add('validation')

      } else {

        inputDestinoContainer.classList.remove('validation')

      }

      if (inputVuelta.value.trim() === '') {

        inputVueltaContainer.classList.add('validation')

      } else {

        inputVueltaContainer.classList.remove('validation')

      }

      if (inputIda.value.trim() === '') {

        inputIdaContainer.classList.add('validation')

      } else {

        inputIdaContainer.classList.remove('validation')

      }

      if (inputOrigenId.value.trim() === '' || inputDestinoId.value.trim() === '' || inputVuelta.value.trim() === '') {

        return;

      } else {

        inputOrigenContainer.classList.remove('validation')

        inputDestinoContainer.classList.remove('validation')

        inputVueltaContainer.classList.remove('validation')

        inputIdaContainer.classList.remove('validation')

      }

      /* if (totalPaqueteNinos.value > 0) {

        if (totalPaqueteNinos.value == 1) {

          url = '2'

        }

        if (totalPaqueteNinos.value == 2) {

          url = '2-4'

        }

        if (totalPaqueteNinos.value == 3) {

          url = '1-3-5'

        }

        if (totalPaqueteNinos.value == 4) {

          url = '0-2-4-6'

        }

        habitaciones = totalPaqueteAdultos.value + `-` + url;

      } else {

        habitaciones = totalPaqueteAdultos.value

      } */



      let paquetes = {

        origen: inputOrigenId.value,

        destino: inputDestinoId.value,

        ida: inputIdaId.value,

        vuelta: inputVueltaId.value,

        habi: habitaciones

      }

      const {

        origen,

        destino,

        ida,

        vuelta,

        habi

      } = paquetes;

      let BASE_URL = 'https://reservas.busplus.com.ar/es-ES/Package/';

      let busqueda = origen + '/' + destino + '/' + ida + '/' + vuelta + '/1/1/0/' + ida + '/' + vuelta + '/' + habi + '/' + 'gyra--B2C-----'

      window.open(BASE_URL + busqueda);

    }

    /*=====  End of SEND FORM   ======*/;

