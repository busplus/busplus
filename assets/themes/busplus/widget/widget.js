function init_widget(modo, empresa, logo, color1, color2, env) {

  let URL = window.location.origin + "/assets/themes/busplus/widget";

  let colorWhite = "";

  let rgbColor1 = "";

  let styleHTML = "";

  let widget = document.getElementById("wd_id");

  // Guardamos los datos en la sesion

  sessionStorage.setItem('env', env);

  // COVERT HEX A RGB

  const hexToRgb = (hex) => {

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    return result

      ? {

          r: parseInt(result[1], 16),

          g: parseInt(result[2], 16),

          b: parseInt(result[3], 16),

        }

      : null;

  };



  if (modo == "v") {

    var clase = "vertical";

  } else {

    clase = 'horizontal'

    var w = "100%";

  }



  if (color1 != null || color2 != null) {

    if (color1 !== "") {

      colorWhite = "#fff";

      rgbColor1 = `rgb(${hexToRgb(`${color1}`).r}, ${

        hexToRgb(`${color1}`).g

      }, ${hexToRgb(`${color1}`).b}, 0.5)`;

      styleHTML += `

      <style>

      #wd_id .widget{

       background: ${color1};

       }

       #wd_id .widget .buscador .title-tab{

         color: ${colorWhite};
         padding:0;

       }

       #wd_id .widget .form-btn > button{

         border: 3px solid ${color2};

         background: ${color2};

       }

       #wd_id .widget .form-btn > button:hover{

         background: ${colorWhite};

         color: ${color2};

       }

       #wd_id .widget .svg-icon{

         color: ${color2};

       }

       #wd_id .widget .descuento__group button{

         color: ${color2};

         border: 1px solid ${color2};

       }

       .widget .descuento__group button:hover{

         background: ${color2} !important;

         color: ${colorWhite} !important;

       }

    

       #wd_id .widget .tablinks{

         background: ${color1};

         border-bottom: 3px solid ${color1};

       }

       #wd_id .widget .tablinks.selected,

       #wd_id .widget .tablinks:hover{

         color: ${colorWhite};

         background: ${color1};

         border-bottom: 3px solid ${colorWhite};

       }

       #wd_id .widget .tablinks.selected span,

       #wd_id .widget .tablinks:hover span {

         color: ${colorWhite};

       }

       #wd_id .widget .tablinks.selected .tab-icon,

       #wd_id .widget .tablinks:hover .tab-icon{

         color: ${colorWhite};

       }

       #wd_id .widget .fadeButton{

         background: ${rgbColor1};

       }

       body .litepicker .container__days .day-item{

         color: ${color1};

       }

       body .litepicker .container__days .day-item.is-today {

         color: ${color1} !important;

       }

       body  .litepicker .container__days .day-item.is-end-date{

         background: ${color1};

       }

       body  .litepicker .container__days .day-item:hover {

         box-shadow: inset 0 0 0 1px ${color1};

         color: ${color1} !important;

       }

       #wd_id .widget .actions a{

         color: ${color2};

       }

       #wd_id .widget .button-icon > svg{

         color: ${color2};

       }

       #wd_id .widget .descuento__group .search__inputs input.cupon-valido{

         color: ${color2};

       }

       #wd_id .modal-item-text.habitacion{

         color: ${color2} !important;

       }

    </style>

      

      `;

    } else {

      colorWhite = "";

      rgbColor1 = "";

    }

  } else {

    styleHTML = "";

  }





  // ESTILOS DEL WIDGET

  let head = document.head || document.getElementsByTagName("head")[0];

  let link = document.createElement("link");

  link.rel = "stylesheet";

  link.type = "text/css";

  link.href = `${URL}/index.css`;

  head.appendChild(link);



  // LIBS REQUERIDOS PARA EL WIDGET

  let script = document.getElementById("wd_script");

  let scriptParadas = document.createElement("script");

  let scriptFunc = document.createElement("script");

  scriptParadas.setAttribute("src", `${URL}/paradas.js`);

  script.before(scriptParadas);

  scriptFunc.setAttribute("src", `${URL}/widget-func.js`);



  script.after(scriptFunc);



  let widgetHTML = "";

  widgetHTML += `



  ${styleHTML}

  <div class='widget ${clase}' style='width: ${w};z-index: 10;'>

    <div class='tab'>

      <button type='button' class='tablinks selected' data-content='.bus'>

        <div class='fadeButton'></div>

        <div class='tab-icon__container'>

            <svg viewBox="0 0 18.96 18.52" class="icon-svg tab-icon">

              <use xlink:href="${URL}/icons/bus.svg#bus"></use>

            </svg>

        </div>

        <span>Bus</span>

      </button>

      <button type='button' class='tablinks' data-content='.hoteles'>

        <div class='fadeButton'></div>

        <div class='tab-icon__container'>

          <svg viewBox="0 0 25.31 14.03" class="icon-svg-hotel tab-icon">

            <use xlink:href="${URL}/icons/hoteles.svg#hotel"></use>

          </svg>

        </div>

        <span>Hoteles</span>

      </button>

      <button type='button' class='tablinks ' data-content='.actividades'>

        <div class='fadeButton'></div>

        <div class='tab-icon__container'>

          <svg viewBox="0 0 24 24" class="icon-svg-actividades tab-icon">

            <use xlink:href="${URL}/icons/actividades.svg#actividades"></use>

          </svg>

        </div>

        <span>Actividades</span>

      </button>

      <button type='button' class='tablinks ' data-content='.paquetes'>

        <div class='fadeButton'></div>

        <div class='tab-icon__container'>

          <svg viewBox="0 0 20.4 17.45" class="icon-svg-paquetes tab-icon">

            <use xlink:href="${URL}/icons/paquetes.svg#paquetes"></use>

          </svg>

        </div>

        <span>Paquetes</span>

      </button>

    </div>

    <div class='tab-content'>

      <!-- BUS -->

      <div class='bus tab-info show'>

        <div class='buscador'>

          <h2 class='title-tab'>Bus</h2>

          <form id='do_search'>

            <!-- ORIGEN Y DESTINO-->

            <div class='input__group origen' id='origen__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='origen'>Origen</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='origen' id='origen' placeholder='Ciudad de origen'

                        value='' autocomplete='off'>

                      <input class='input-text' type='hidden' onselectstart="return false" name='origen-id' id='origen-id'

                        value=''>

                    </div>

                  </div>

                </div>

              </div>

              <div class='d-arrow'>

                <div class='d-arrow-line'></div>

                <div class='d-arrow-img'>

                  <svg class='svg-icon' id='sync_alt_black_24dp' xmlns='http://www.w3.org/2000/svg' width='24' height='24'

                    viewBox='0 0 24 24'>

                    <g id='Grupo_3' data-name='Grupo 3'>

                      <rect id='Rectángulo_9' data-name='Rectángulo 9' width='24' height='24' fill='none' />

                    </g>

                    <g id='Grupo_5' data-name='Grupo 5'>

                      <g id='Grupo_4' data-name='Grupo 4'>

                        <path id='Trazado_10' data-name='Trazado 10'

                          d='M7.41,13.41,6,12,2,16l4,4,1.41-1.41L5.83,17H21V15H5.83Z' fill='currentColor' />

                        <path id='Trazado_11' data-name='Trazado 11'

                          d='M16.59,10.59,18,12l4-4L18,4,16.59,5.41,18.17,7H3V9H18.17Z' fill='currentColor' />

                      </g>

                    </g>

                  </svg>

                </div>

                <div class='d-arrow-line'></div>

              </div>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='destino'>Destino</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='destino' id='destino' placeholder='Ciudad de destino'

                        value='' autocomplete='off'>

                      <input class='input-text' type='hidden' name='destino-id' id='destino-id' value=''>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FECHA IDA Y VUELTA -->

            <div class='input__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='ida'>Ida</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='date-ida date' readonly='true' type='text' name='date-id' id='bus-ida'

                        placeholder='Ida' value='' autocomplete='off'>

                        <input type='hidden' name='bus-entrada-id' id='bus-entrada-id'>

                    </div>

                  </div>

                </div>

              </div>

              <div class='search__inputs destino'>

                <div class='input__container'>

                  <label for='vuelta'>Vuelta</label>

                  <div class='input'>

                    <div class='autocomplete'>

                      <input class='date-vuelta date' readonly='true' type='text' name='date-vuelta' id='bus-vuelta'

                        placeholder='Opcional' autocomplete='off'>

                      <input type='hidden' name='bus-salida-id' id='bus-salida-id'>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- PASAJEROS -->

            <div class='input__group'>

              <div class='search__inputs pasajeros input__group-pasajeros'>

                <div class='input__container' data-open='modal-bus-passenger'>

                  <label for='pasajeros'>Pasajeros</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='11.15' height='12.75'

                        viewBox='0 0 11.15 12.75'>

                        <path id='Composite_Path' data-name='Composite Path'

                          d='M1059.675,429.775a3.222,3.222,0,0,1-3.2,3.2,3.184,3.184,0,0,1-3.075-2.35,4.791,4.791,0,0,1-.1-.6v-.475a3.112,3.112,0,0,1,3.175-2.925A3.164,3.164,0,0,1,1059.675,429.775Zm-5,4.025a5.324,5.324,0,0,0,1.8.4,5.989,5.989,0,0,0,1.825-.4,1.408,1.408,0,0,1,.325,0,3.232,3.232,0,0,1,3.175,2.05,3.412,3.412,0,0,1,.275,1.05v1.4a1.3,1.3,0,0,1-1.075,1.075h-9.05c-.35-.025-1.025-.525-1.025-.925v-1.775a3.322,3.322,0,0,1,3.4-2.875C1054.4,433.8,1054.575,433.75,1054.675,433.8Z'

                          transform='translate(-1050.925 -426.625)' fill='currentColor' />

                      </svg>

                    </a>

                    <input type='number' name='pasajeros' id='pasajeros' autocomplete='off' value='1'>

                  </div>

                </div>



              </div>

              <!-- Codigo descuento -->

              <div class='descuento__group' style="z-index: 15;">

              <p class="response_codigo"></p>

                <div class='search__inputs'>

                  <div class='input__container'>

                    <div class='input'>

                      <input class='c-descuento' type='text' placeholder='Código de descuento' value=''

                      name='b-descuento' id='b-descuento' max-length="12" autocomplete='off'>

                      <button type='button'>

                      Ok

                      </button>

                      </div>

                      <input type="hidden" class="IdPase" id="IdPase" name="IdPase">

                  </div>

                </div>

              </div>

            </div>

            <!-- FORM BTN -->

            <div class='form-btn'>

              <button type='button' onclick='searchBus("${empresa}","${logo}","${env}")'>

                Buscar

              </button>

            </div>

          </form>

        </div>

      </div>

      <!-- HOTELES -->

      <div class='hoteles tab-info'>

        <div class='buscador'>

          <h2 class='title-tab'>Hoteles</h2>

          <form id='do_search'>

            <!-- DESTINO -->

            <div class='input__group origen'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='h-destino'>Destino</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='h-destino' id='h-destino'

                        placeholder='Ingrese una ciudad o punto de interés' autocomplete='off'>

                      <input class='input-text' type='hidden' name='h-destino-id' id='h-destino-id' value=''>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FECHA IDA Y FECHA VUELTA -->

            <div class='input__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='h-entrada'>Entrada</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>



                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='h-entrada' id='hotel-entrada' placeholder='Entrada'

                        value='' autocomplete='off'>

                      <input type='hidden' name='h-entrada-id' id='h-entrada-id'>

                    </div>

                  </div>

                </div>

              </div>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='h-salida'>Salida</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='h-salida' id='hotel-salida' placeholder='Salida' value=''

                        autocomplete='off'>

                      <input type='hidden' name='h-salida-id' id='h-salida-id'>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- PASAJEROS -->

            <div class='input__group'>

              <div class='search__inputs pasajeros'>

                <div class='input__container' data-open='modal-hotel-passenger'>

                  <label for='pasajeros'>Pasajeros</label>

                  <div class='pasajeros-group'>

                    <div class='input'>

                      <a href='javascript:void(0);' class='icon__hotel'>

                        <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg'

                          xmlns:xlink='http://www.w3.org/1999/xlink' width='25.312' height='14.303'

                          viewBox='0 0 25.312 14.303'>

                          <defs>

                            <clipPath id='clip-path'>

                              <rect id='Rectángulo_1007' data-name='Rectángulo 1007' width='25.312' height='14.303'

                                class='tab-content__color' fill='currentColor' />

                            </clipPath>

                          </defs>

                          <g id='Grupo_2064' data-name='Grupo 2064' transform='translate(0 0)'>

                            <g id='Grupo_2051' data-name='Grupo 2051' transform='translate(0 0)'

                              clip-path='url(#clip-path)'>

                              <path id='Trazado_1331' data-name='Trazado 1331'

                                d='M2.352,14.3H0V1.176a1.176,1.176,0,0,1,2.352,0Z' transform='translate(0 0)'

                                fill='currentColor' />

                              <path id='Trazado_1332' data-name='Trazado 1332'

                                d='M68.935,24.675H66.583V17.009a1.176,1.176,0,1,1,2.352,0Z'

                                transform='translate(-43.623 -10.373)' fill='currentColor' />

                              <rect id='Rectángulo_1006' data-name='Rectángulo 1006' width='22.536' height='2.164'

                                transform='translate(1.176 9.786)' fill='currentColor' />

                              <path id='Trazado_1333' data-name='Trazado 1333'

                                d='M13.157,15.832h-1.5a1.745,1.745,0,0,0-1.746,1.745v1.829H14.9V17.577a1.745,1.745,0,0,0-1.745-1.745'

                                transform='translate(-6.496 -10.373)' fill='currentColor' />

                              <path id='Trazado_1334' data-name='Trazado 1334'

                                d='M37.917,19.375H28.837A1.639,1.639,0,0,0,27.2,21.014v.713H39.556v-.713a1.639,1.639,0,0,0-1.639-1.639'

                                transform='translate(-17.819 -12.694)' fill='currentColor' />

                            </g>

                          </g>

                        </svg>

                      </a>

                      <div class='autocomplete'>

                        <input type='number' name='h-habitaciones' id='h-habitaciones' placeholder='Ida' value='1' autocomplete='off'>

                      </div>

                    </div>

                    <div class='input'>

                      <a href='javascript:void(0);'>

                        <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='11.15' height='12.75'

                          viewBox='0 0 11.15 12.75'>

                          <path id='Composite_Path' data-name='Composite Path'

                            d='M1059.675,429.775a3.222,3.222,0,0,1-3.2,3.2,3.184,3.184,0,0,1-3.075-2.35,4.791,4.791,0,0,1-.1-.6v-.475a3.112,3.112,0,0,1,3.175-2.925A3.164,3.164,0,0,1,1059.675,429.775Zm-5,4.025a5.324,5.324,0,0,0,1.8.4,5.989,5.989,0,0,0,1.825-.4,1.408,1.408,0,0,1,.325,0,3.232,3.232,0,0,1,3.175,2.05,3.412,3.412,0,0,1,.275,1.05v1.4a1.3,1.3,0,0,1-1.075,1.075h-9.05c-.35-.025-1.025-.525-1.025-.925v-1.775a3.322,3.322,0,0,1,3.4-2.875C1054.4,433.8,1054.575,433.75,1054.675,433.8Z'

                            transform='translate(-1050.925 -426.625)' fill='currentColor' />

                        </svg>

                      </a>

                      <div class='autocomplete'>

                        <input type='number' name='pasajeros' id='hotel-pasajeros' value='1' autocomplete='off'>

                      </div>

                    </div>

                  </div>



                </div>

              </div>

              <!-- Codigo descuento -->

              <div class='descuento__group'>

                <div class='search__inputs'>

                  <div class='input__container'>

                    <div class='input'>

                      <input class='c-descuento' type='text' name='b-descuento' id='b-descuento'

                        placeholder='Código de descuento' value='' autocomplete='off'>

                      <button type='button'>

                        Ok

                      </button>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FORM BTN -->

            <div class='form-btn'>

              <button type='button' onclick='searchHotel()'>

                Buscar

              </button>

            </div>

          </form>

        </div>

      </div>

      <!--  ACTIVIDADES -->

      <div class='actividades tab-info '>

        <div class='buscador'>

          <h2 class='title-tab'>Actividades</h2>

          <form id='do_search'>

            <!-- DESTINO -->

            <div class='input__group origen'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='h-destino'>Destino</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='a-destino' id='a-destino'

                        placeholder='Ingrese una ciudad o punto de interés' autocomplete='off'>

                      <input class='input-text' type="hidden" name="a-destino-id" id="a-destino-id" value="">

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FECHA IDA Y FECHA VUELTA -->

            <div class='input__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='a-entrada'>Entrada</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='actividades-entrada' id='actividades-entrada'

                        placeholder='Entrada' autocomplete='off'>

                      <input type='hidden' name='actividades-entrada-id' id='actividades-entrada-id'>

                    </div>

                  </div>

                </div>

              </div>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='a-salida'>Salida</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='actividades-salida' id='actividades-salida'

                        placeholder='Salida' autocomplete='off'>

                      <input type='hidden' name='actividades-salida-id' id='actividades-salida-id'>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- Codigo descuento -->

            <div class='descuento__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <div class='input'>

                    <input class='c-descuento' type='text' name='b-descuento' id='b-descuento'

                      placeholder='Código de descuento' value='' autocomplete='off'>

                    <button type='button'>

                      Ok

                    </button>

                  </div>

                </div>

              </div>

            </div>

            <!-- FORM BTN -->

            <div class='form-btn'>

              <button type='button' onclick='searchActividades()'>

                Buscar

              </button>

            </div>

          </form>

        </div>

      </div>

      <!--  Paquetes -->

      <div class='paquetes tab-info '>

        <div class='buscador'>

          <h2 class='title-tab'>Paquetes</h2>

          <form id='do_search'>

            <div class='input__group origen'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='origen'>Origen</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='p-origen' id='p-origen' placeholder='Ciudad de origen'

                        autocomplete='off'>

                      <input class='input-text' type="hidden" name="p-origen-id" id="p-origen-id" value="">

                    </div>

                  </div>

                </div>

              </div>

              <div class='d-arrow'>

                <div class='d-arrow-line'></div>

                <div class='d-arrow-img' id='d-arrow-paquetes'>

                  <svg class='svg-icon' id='sync_alt_black_24dp' xmlns='http://www.w3.org/2000/svg' width='24' height='24'

                    viewBox='0 0 24 24'>

                    <g id='Grupo_3' data-name='Grupo 3'>

                      <rect id='Rectángulo_9' data-name='Rectángulo 9' width='24' height='24' fill='none' />

                    </g>

                    <g id='Grupo_5' data-name='Grupo 5'>

                      <g id='Grupo_4' data-name='Grupo 4'>

                        <path id='Trazado_10' data-name='Trazado 10'

                          d='M7.41,13.41,6,12,2,16l4,4,1.41-1.41L5.83,17H21V15H5.83Z' fill='currentColor' />

                        <path id='Trazado_11' data-name='Trazado 11'

                          d='M16.59,10.59,18,12l4-4L18,4,16.59,5.41,18.17,7H3V9H18.17Z' fill='currentColor' />

                      </g>

                    </g>

                  </svg>

                </div>

                <div class='d-arrow-line'></div>

              </div>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='destino'>Destino</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.655' height='16.54'

                        viewBox='0 0 12.655 16.54'>

                        <path id='Sustracción_2' data-name='Sustracción 2'

                          d='M-5812.19-1314.46c-.068,0-.393-.072-.463-.215-.828-1.072-1.607-2.214-2.361-3.317l-.008-.012c-.447-.655-.909-1.332-1.374-1.981a7.474,7.474,0,0,1-1.6-3.884v-.642a5.935,5.935,0,0,1,2.023-4.107,5.839,5.839,0,0,1,3.787-1.381,5.766,5.766,0,0,1,5.845,5.81,5.133,5.133,0,0,1-.356,1.924,34.839,34.839,0,0,1-3.508,5.509l0,.006c-.494.681-1,1.385-1.479,2.076A.7.7,0,0,1-5812.19-1314.46Zm.018-11.77a2,2,0,0,0-2,2,2,2,0,0,0,2,2,2,2,0,0,0,2-2A2,2,0,0,0-5812.173-1326.231Z'

                          transform='translate(5818.5 1330.501)' fill='currentColor' stroke='rgba(0,0,0,0)'

                          stroke-miterlimit='10' stroke-width='1' />

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input class='input-text' type='text' name='p-destino' id='p-destino'

                        placeholder='Ciudad de destino' autocomplete='off'>

                      <input class='input-text' type="hidden" name="p-destino-id" id="p-destino-id" value="">

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FECHA IDA Y FECHA VUELTA -->

            <div class='input__group'>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='ida'>Ida</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='p-ida' id='p-ida' placeholder='Ida' value=''

                        autocomplete='off'>

                      <input type='hidden' name='p-ida-id' id='p-ida-id'>

                    </div>

                  </div>

                </div>

              </div>

              <div class='search__inputs'>

                <div class='input__container'>

                  <label for='vuelta'>Vuelta</label>

                  <div class='input'>

                    <a href='javascript:void(0);'>

                      <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='12.292' height='14.364'

                        viewBox='0 0 12.292 14.364'>

                        <g id='Grupo_46' data-name='Grupo 46' transform='translate(-681.813 -382.856)'>

                          <g id='Grupo_698' data-name='Grupo 698' transform='translate(681.813 383.856)'>

                            <path id='Sustracción_1' data-name='Sustracción 1'

                              d='M10.817,8.1H1.475A1.4,1.4,0,0,1,0,6.779V-1.013h12.29V6.779A1.4,1.4,0,0,1,10.817,8.1ZM9.1,4.437a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H9.1Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H5.655Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22H2.213ZM9.1.875a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22h1.229a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H6.884a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Zm-3.442,0a.234.234,0,0,0-.246.22v1.1a.234.234,0,0,0,.246.22H3.443a.234.234,0,0,0,.246-.22v-1.1a.234.234,0,0,0-.246-.22Z'

                              transform='translate(0 5.268)' fill='currentColor' />

                            <path id='Unión_2' data-name='Unión 2'

                              d='M9.6,4.39H0V3A1.433,1.433,0,0,1,1.475,1.617H2.7V.231A.239.239,0,0,1,2.95,0H4.179a.238.238,0,0,1,.246.231V1.617H7.867V.231A.238.238,0,0,1,8.113,0H9.342a.239.239,0,0,1,.246.231V1.617h1.229A1.433,1.433,0,0,1,12.292,3V4.39Z'

                              transform='translate(0 -1)' fill='currentColor' />

                          </g>

                        </g>

                      </svg>

                    </a>

                    <div class='autocomplete'>

                      <input type='text' readonly='true' name='p-vuelta' id='p-vuelta' placeholder='Vuelta' value=''

                        autocomplete='off'>

                      <input type='hidden' name='p-vuelta-id' id='p-vuelta-id'>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- PASAJEROS -->

            <div class='input__group'>

              <div class='search__inputs pasajeros'>

                <div class='input__container' data-open='modal-paquetes-passenger'>

                  <label for='pasajeros'>Pasajeros</label>

                  <div class='pasajeros-group'>

                    <div class='input'>

                      <a href='javascript:void(0);' class='icon__hotel'>

                        <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg'

                          xmlns:xlink='http://www.w3.org/1999/xlink' width='25.312' height='14.303'

                          viewBox='0 0 25.312 14.303'>

                          <defs>

                            <clipPath id='clip-path'>

                              <rect id='Rectángulo_1007' data-name='Rectángulo 1007' width='25.312' height='14.303'

                                class='tab-content__color' fill='currentColor' />

                            </clipPath>

                          </defs>

                          <g id='Grupo_2064' data-name='Grupo 2064' transform='translate(0 0)'>

                            <g id='Grupo_2051' data-name='Grupo 2051' transform='translate(0 0)'

                              clip-path='url(#clip-path)'>

                              <path id='Trazado_1331' data-name='Trazado 1331'

                                d='M2.352,14.3H0V1.176a1.176,1.176,0,0,1,2.352,0Z' transform='translate(0 0)'

                                fill='currentColor' />

                              <path id='Trazado_1332' data-name='Trazado 1332'

                                d='M68.935,24.675H66.583V17.009a1.176,1.176,0,1,1,2.352,0Z'

                                transform='translate(-43.623 -10.373)' fill='currentColor' />

                              <rect id='Rectángulo_1006' data-name='Rectángulo 1006' width='22.536' height='2.164'

                                transform='translate(1.176 9.786)' fill='currentColor' />

                              <path id='Trazado_1333' data-name='Trazado 1333'

                                d='M13.157,15.832h-1.5a1.745,1.745,0,0,0-1.746,1.745v1.829H14.9V17.577a1.745,1.745,0,0,0-1.745-1.745'

                                transform='translate(-6.496 -10.373)' fill='currentColor' />

                              <path id='Trazado_1334' data-name='Trazado 1334'

                                d='M37.917,19.375H28.837A1.639,1.639,0,0,0,27.2,21.014v.713H39.556v-.713a1.639,1.639,0,0,0-1.639-1.639'

                                transform='translate(-17.819 -12.694)' fill='currentColor' />

                            </g>

                          </g>

                        </svg>

                      </a>

                      <div class='autocomplete'>

                        <input type='number' name='paquetes-habitacion' id='paquetes-habitacion' placeholder='Ida' value='1' autocomplete='off'>

                      </div>

                    </div>

                    <div class='input'>

                      <a href='javascript:void(0);'>

                        <svg class='svg-icon' xmlns='http://www.w3.org/2000/svg' width='11.15' height='12.75'

                          viewBox='0 0 11.15 12.75'>

                          <path id='Composite_Path' data-name='Composite Path'

                            d='M1059.675,429.775a3.222,3.222,0,0,1-3.2,3.2,3.184,3.184,0,0,1-3.075-2.35,4.791,4.791,0,0,1-.1-.6v-.475a3.112,3.112,0,0,1,3.175-2.925A3.164,3.164,0,0,1,1059.675,429.775Zm-5,4.025a5.324,5.324,0,0,0,1.8.4,5.989,5.989,0,0,0,1.825-.4,1.408,1.408,0,0,1,.325,0,3.232,3.232,0,0,1,3.175,2.05,3.412,3.412,0,0,1,.275,1.05v1.4a1.3,1.3,0,0,1-1.075,1.075h-9.05c-.35-.025-1.025-.525-1.025-.925v-1.775a3.322,3.322,0,0,1,3.4-2.875C1054.4,433.8,1054.575,433.75,1054.675,433.8Z'

                            transform='translate(-1050.925 -426.625)' fill='currentColor' />

                        </svg>

                      </a>

                      <div class='autocomplete'>

                        <input type='number' name='paquetes-pasajeros' id='paquetes-pasajeros' placeholder='Ida' value='1'autocomplete='off'>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <!-- Codigo descuento -->

              <div class='descuento__group'>

                <div class='search__inputs'>

                  <div class='input__container'>

                    <div class='input'>

                      <input class='c-descuento' type='text' name='b-descuento' id='b-descuento'

                        placeholder='Código de descuento' value='' autocomplete='off'>

                      <button type='button'>

                        Ok

                      </button>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <!-- FORM BTN -->

            <div class='form-btn'>

              <button type='button' onclick='searchPaquetes()'>

                Buscar

              </button>

            </div>

          </form>

        </div>

      </div>

    </div>

  </div>

`;



  widget.innerHTML = widgetHTML;



  /*=====  End of SEND FORM   ======*/

}



