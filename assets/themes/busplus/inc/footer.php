<footer id="footer">
  <div class="container">
    <div class="cont-footer">
      <div class="cont-list-foo">
        <h4>Mis viajes</h4>
        <ul>
          <li><a href="">Gestioná tus pasajes</a></li>
          <li><a href="">Estado de tu viaje</a></li>
          <li><a href="">Localizá tu micro</a></li>
          <li><a href="">Protocolo COVID</a></li>
          <li><button class="btn-e-2">ARREPENTIMIENTO</button></li>
        </ul>
      </div>
      <div class="cont-list-foo ml-auto-footer">
        <h4>Busplus</h4>
        <ul>
          <li><a href="">Vía Club</a></li>
          <li><a href="">Promociones</a></li>
          <li><a href="">Novedades</a></li>
          <li><a href="">Quiénes Somos</a></li>
        </ul>
      </div>
      <div class="cont-list-foo">
        <h4>Atención al cliente</h4>
        <ul>
          <li><a href="">Como comprar</a></li>
          <li><a href="">Preguntas frecuentes</a></li>
          <li><a href="">Whatsapp</a></li>
          <li><a href="">Contacto</a></li>
        </ul>
      </div>
      <div class="cont-list-foo ml-auto-footer">
        <h4>Seguinos</h4>
        <ul class="redes">
          <!-- <li><a href=""><img src="dist/img/redes-sociales/003-facebook.svg" style="height: 20px;" alt="Facebook logo"></a></li> -->
          <li><a target="_blank" href="">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <g>
              <g>
                <path d="M448,0H64C28.704,0,0,28.704,0,64v384c0,35.296,28.704,64,64,64h192V336h-64v-80h64v-64c0-53.024,42.976-96,96-96h64v80
                h-32c-17.664,0-32-1.664-32,16v64h80l-32,80h-48v176h96c35.296,0,64-28.704,64-64V64C512,28.704,483.296,0,448,0z"/>
              </g>
            </g>
          </svg>
        </a></li>
        <!-- <li><a href=""><img src="dist/img/redes-sociales/001-instagram.svg" style="height: 20px;" alt="Instagram logo"></a></li> -->
        <li><a target="_blank" href="">
          <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
          <g>
            <g>
              <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
              C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
              h192c61.76,0,112,50.24,112,112V352z"/>
            </g>
          </g>
          <g>
            <g>
              <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
              c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
            </g>
          </g>
          <g>
            <g>
              <circle cx="393.6" cy="118.4" r="17.056"/>
            </g>
          </g>
        </svg>
      </a></li>
      <!-- <li><a href=""><img src="dist/img/redes-sociales/002-twitter.svg" style="height: 20px;" alt="twitter logo"></a></li> -->
    </ul>
  </div>
</div>
<div class="b-footer">
  <div class="logo-footer">
    <img src="dist/img/logo-busplus.svg" alt="Logo Busplus">
  </div>
  <div class="legales-footer">
    <a target="_blank" href="">Términos y condiciones1</a>
    <a target="_blank" href="">Política de privacidad</a>
  </div>
</div>
</div>
</footer>
<?php /*
<div class="fixed whatsapp"><a href="https://api.whatsapp.com/message/ZTTNEHZDQS2HH1" target="_blank">
<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="whatsapp" class="svg-inline--fa fa-whatsapp fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"></path></svg></a></div>
*/ ?>
<div class="modal closed" id="geoloc">
  <a href="#0" class="close-button" onclick="closeMap()"><i class="fa fa-times"></i></a>
  <div class="modal-guts">
    <div id="cont-map"></div>
  </div>
</div>
<div class="modal-overlay closed" id="modal-overlay"></div>
<script src="dist/js/app.min.js?v=<?php echo rand(); ?>"></script>
<?php
if ($_SERVER['HTTP_HOST']!='dev.zetenta.com') {?>
  <script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:8888/browser-sync/browser-sync-client.js?v=2.26.12'><\/script>".replace("HOST", location.hostname));
    //]]></script>
  <?php } ?>
</body>
</html>
