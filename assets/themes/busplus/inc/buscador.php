<style> .remove { display: none; position:absolute; top: 0.7rem; right: 0.5rem; font-size:1.5rem; font-weight: 700; transition: all ease 0.5; z-index:1; color: #4A5568; }</style>
<input type="hidden" name="theme" id="theme" value="<?php echo get_template_directory_uri(); ?>">
<div class="modal mini closed" id="codigo">
  <a href="#0" class="close-button"><i class="fa fa-times"></i></a>
  <div class="modal-guts a">
    <div class="form">
      <div class="f-line">
        <div class="f-item f-8">
          <label for="codigo">Código de descuento</label>
          <input type="text" name="descuento" id="descuento" class="" />
		</div>
		<div class="f-item f-2">
            <button type="submit" class="btn" id="validar">
              Validar</button>
		  </div>
		</div>
    <div class="responseDesktop">
      <p class="form_error" style="color:red;"></p>
      <p class="form_valido" style="color:green;"></p>
      <p class="disc">Los descuentos solo aplican a los pasajes de omnibus.</p>
    </div>
	  </div>
	</div>
</div>
<div class="modal closed" id="geoloc">
	<a href="#0" class="close-button" onclick="closeMap()"><i class="fa fa-times"></i></a>
	<div class="modal-guts">
		<div id="cont-map"></div>
	</div>
</div>
<div class="modal-overlay closed" id="modal-overlay"></div>
<!-- <div class="b-top">
	<div class="cupones" style="display:none;text-align: center;margin: auto;max-width: 900px;position: relative; z-index: 12; top: -45px; max-height: 0px;">
		<div style="display: inline-flex;  background-color: #4a5568; border-radius: 5px; padding: 5px;
">

			<input type="text" name="cupondescuento" id="cupondescuento" class="form-control" placeholder="C&oacute;digo descuento"  style="margin: auto;text-align: center;max-width: 150px;margin-right: 10px; padding: 0;" id="coupon-code"  data-trigger="focus" data-placement="left">
			<div   value="Validar" style="margin: auto; max-width: 100px; display: table; max-height: 35px; padding: 3px;" class="btn btn-block info btn-success validarcupon button-validate">Validar</div>
			<label class="btn btn-danger cuponcerrar" style="cursor:pointer;margin: auto;max-height: 36px; display: table; padding: 3px;">Cerrar</label>
		</div>
		<h3><label class="cuponError" style="color:red" id="cuponError"></label>
			<label class="cuponValido" style="color:#fff" id="cuponValido"></label></h3>
	</div>
	<div class="btr cupon abricupon" style="cursor: pointer;">
		<div title="Agregar código de descuento" class="descuento-popover " style="color: white; cursor: pointer; font-size: 12px;font-weight: bold;">Tengo un código de descuento</div>
	</div>
</div> -->
  <div class="b-top">

       <div class="btr cupon abricupon" style="cursor: pointer;">
        <a href="#" title="Tengo un código de descuento" class="open-modal-disc l-disc" style="color: white; cursor: pointer; font-size: 12px;font-weight: bold;">Tengo un código de descuento</a>
       </div>
     </div>
<input type="hidden" name="cupondescuento" id="cupondescuento" >
<div id="buscador">
  <a href="#" class="c-search"><i class="fa fa-times"></i></a>
  <form id="do_search">




    <div class="b-bot">
      <div class="b-group bg1">
        <label for="">Origen</label>
        <div class="sel-origen">
          <a href="javascript:void(0);" title="Geolocalización Origen" class="geoloc" onclick="openMap('origen')"><i class="fa fa-map-marker-alt"></i></a>
          <a href="javascript:void(0);"><span data-input="origen" class="remove material-symbols-outlined">close</span></a>
          <input type="text" name="origen" id="origen" placeholder="Ciudad de origen">
          <input type="hidden" name="origen-id" id="origen-id" value="0">
        </div>
      </div>
      <div class="d-arrow">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/icons/d-arrow.svg" alt="double arrow">
      </div>

      <div class="b-group bg2">
        <label for="">Destino</label>
        <div class="sel-destino">
          <a href="javascript:void(0);" title="Geolocalización Destino" class="geoloc" onclick="openMap('destino')"><i class="fa fa-map-marker-alt"></i></a>
          <a href="javascript:void(0);"><span data-input="destino" class="remove material-symbols-outlined">close</span></a>
          <input type="text" name="destino" id="destino" placeholder="Ciudad de destino">
          <input type="hidden" name="destino-id" id="destino-id"  value="0">
        </div>  </div>
        <div class="b-group bg3">
          <div class="iv">
            <label for="">Ida</label>
            <div class="sel-fecha">
              <i class="fa fa-calendar-alt"></i>
              <input type="text" readonly="true" placeholder="Ida" id="start-date" name="start-date" autocomplete="off" class="f-desde">
              <input type="hidden" name="start-date-formatted" id="start-date-formatted"  value="0">
            </div>
          </div>
          <div class="iv">
            <label for="">Vuelta <span>(Opcional)</span></label>
            <div class="sel-fecha">
              <input type="text" readonly="true" placeholder="Vuelta" id="end-date" name="end-date" autocomplete="off" class="f-hasta" >
              <input type="hidden" name="end-date-formatted" id="end-date-formatted"  value="0">
            </div>
          </div>
        </div>
        <div class="b-group bg4">
          <label for="">Pasajeros</label>
            <div class="sel-codigo" style="position: relative;width: 100%;">
                <i class="fa fa-user" style="position: absolute;top: 0.938rem;left: 0.5rem;font-size: .938rem;z-index: 1;color: #4a5568;"></i>
                <input type="number" name="passengers-count" id="passengers-count" value="1">
            </div>
          </div>
        <div class="b-group m-disc">
          <a href="#" title="Tengo un código de descuento" class="cupondescuentoMobile" style=" font-size: 12px;font-weight: bold;">Tengo un código de descuento</a>
        </div>
		<div class="f-item f-2" style="">
			<div class="" style="display: flex;width: 100%;">
				<input type="text" name="cupondescuento2" id="cupondescuento2" class="form-control" placeholder="C&oacute;digo descuento"  style="display:none;margin: auto;text-align: center; padding: 0; margin-bottom: 10px;" data-trigger="focus" data-placement="left">
				<button type="submit" class="btn validar" style="padding: 0;max-width: 100px;display:none; max-height: 37px;">Validar</button>
				</div>
        <style>
          .responseMobile { display: flex; justify-content: center; align-items: center; }
          .responseMobile .responseMsg { display:none; text-align: center; width: 100%; margin-bottom: 5px; padding: 10px; border-radius: 5px;}
          .responseMobile .form_error { background-color: rgb(229 62 62 / 50%); color: #F7FAFC; }
          .responseMobile .form_valido { background-color: rgb(72 187 120 / 50%); color: #F7FAFC; }
        </style>
        <div class="responseMobile">
          <p class="responseMsg form_error"></p>
          <p class="responseMsg form_valido"></p>
        </div>
		  
			</div>
        <div class="btn btn-search">
          <a href="#" onclick="buscarIdaVuelta();" title="Buscar">Buscar</a>
        </div>
      </div>
      <div class="b-multi">
        <div class="b-multi-line">
          <div class="b-line">
            <h3>Tramo 1</h3>
            <div class="b-group bg1">
              <label for="origen">Origen</label>
              <div class="sel-origen">
                <a href="#" title="Geolocalización Origen" class="geoloc"><i class="fa fa-map-marker-alt"></i></a>
                <a href="javascript:void(0);"><span data-input="origen" class="remove material-symbols-outlined">close</span></a>
                <input type="text" name="origen-1" id="origen" placeholder="Desde: Ciudad, Terminal o Aeropuerto">
                <input type="hidden" name="origen-id" id="origen-id" value="0">
              </div>
            </div>
            <div class="b-group bg2">
              <label for="">Destino</label>
              <div class="sel-destino">
                <a href="#" title="Geolocalización Destino" class="geoloc"><i class="fa fa-map-marker-alt"></i></a>
                <a href="javascript:void(0);"><span data-input="destino" class="remove material-symbols-outlined">close</span></a>
                <input type="text" name="destino-1" id="destino" placeholder="Hasta: Ciudad, Terminal o Aeropuerto">
                <input type="hidden" name="destino-id" id="destino-id"  value="0">
              </div>  </div>
              <div class="b-group bg3">
                <div class="iv">
                  <label for="">Fecha</label>
                  <div class="sel-fecha">
                    <i class="fa fa-calendar-alt"></i>
                    <input type="text" readonly="true" placeholder="Ida" id="start-date-1" name="start-date" autocomplete="off" class="f-desde">
                    <input type="hidden" name="start-date-formatted" id="start-date-formatted"  value="0">
                  </div>
                </div>
              </div>
              <div class="b-group bg4">
              </div>
            </div>
            <div class="b-line first" id="multi-tramo">
              <h3>Tramo 2</h3>
              <div class="b-group bg1">
                <label for="">Origen</label>
                <div class="sel-origen">
                  <a href="#" title="Geolocalización Origen" class="geoloc"><i class="fa fa-map-marker-alt"></i></a>
                  <a href="javascript:void(0);"><span data-input="origen" class="remove material-symbols-outlined">close</span></a>
                  <input type="text" name="origen-2" id="origen" placeholder="Desde: Ciudad, Terminal o Aeropuerto">
                  <input type="hidden" name="origen-id" id="origen-id" value="0">
                </div>
              </div>
              <div class="b-group bg2">
                <label for="">Destino</label>
                <div class="sel-destino">
                  <a href="#" title="Geolocalización Destino" class="geoloc"><i class="fa fa-map-marker-alt"></i></a>
                  <a href="javascript:void(0);"><span data-input="destino" class="remove material-symbols-outlined">close</span></a>
                  <input type="text" name="destino-2" id="destino" placeholder="Hasta: Ciudad, Terminal o Aeropuerto">
                  <input type="hidden" name="destino-id" id="destino-id"  value="0">
                </div>  </div>
                <div class="b-group bg3">
                  <div class="iv">
                    <label for="">Fecha</label>
                    <div class="sel-fecha">
                      <i class="fa fa-calendar-alt"></i>
                      <input type="text" readonly="true" placeholder="Ida" id="start-date-2" name="start-date" autocomplete="off" class="f-desde">
                      <input type="hidden" name="start-date-formatted" id="start-date-formatted"  value="0">
                    </div>
                  </div>
                </div>
                <div class="b-group bg4">
                  <a href="#0" title="Eliminar tramo" class="eliminar_tramo">- Eliminar tramo</a>
                  <a href="#0" title="Agregar tramo" class="agregar_tramo">+ Agregar tramo</a>
                </div>
              </div>
            </div>
            <div class="b-line b-submit">
              <div class="b-group bg4">
                <label for="">Pasajeros</label>
                  <div class="sel-codigo" style="position: relative;width: 100%;">
                      <i class="fa fa-user" style="position: absolute;top: 0.938rem;left: 0.5rem;font-size: .938rem;z-index: 1;color: #4a5568;"></i>
                      <input type="number" name="passengers-count" id="passengers-count" value="1">
                  </div>
                </div>
              <!-- <div class="b-group m-disc">
                <a href="#" title="Tengo un código de descuento" class="open-modal-disc">Tengo un código de descuento</a>
              </div> -->
              <div class="btn btn-search">
                <a href="#" onclick="buscarIdaVuelta();" title="Buscar">Buscar</a>
              </div>
            </div>
          </form>
        </div>
      </div>