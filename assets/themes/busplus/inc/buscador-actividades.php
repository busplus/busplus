<div id="buscador" class="actividades">
  <a href="#" class="c-search"><i class="fa fa-times"></i></a>
  <form id="do_search">
    <div class="b-bot">
      <div class="b-group">
        <label for="">Ciudad de destino</label>
        <div class="sel-hotel-destino">
      <i class="fa fa-map-marker-alt"></i>
          <input type="text" name="actividad-destino" id="actividad-destino" placeholder="Ingrese una ciudad">
          <input type="hidden" name="actividad-destino-id" id="actividad-destino-id"  value="0">
</div>
      </div>
      <div class="b-group">
          <label for=""> Fecha Check in</label>
          <div class="sel-fecha">
            <i class="fa fa-calendar-alt"></i>
            <input type="text" readonly="true" placeholder="Entrada" id="actividad-check-in" name="actividad-check-in" autocomplete="off">
            <input type="hidden" name="actividad-check-in-formatted" id="actividad-check-in-formatted"  value="0">
          </div>
      </div>
      <div class="b-group">
          <label for=""> Fecha Check out</label>
          <div class="sel-fecha">
            <i class="fa fa-calendar-alt"></i>
            <input type="text" readonly="true" placeholder="Salida" id="actividad-check-out" name="actividad-check-out" autocomplete="off">
            <input type="hidden" name="actividad-check-out-formatted" id="actividad-check-out-formatted"  value="0">
          </div>
      </div>
      <div class="btn">
        <a href="#" onclick="buscarActividades();" title="Buscar">Buscar</a>
      </div>
    </div>
  </div>
