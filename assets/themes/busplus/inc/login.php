<div class="modal closed" id="login_busplus">
  <a href="#0" class="close-button"><i class="fa fa-times"></i></a>
  <div class="modal-guts">
    <form class="form">
      <div class="f-line">
        <div class="f-item">
          <label for="tipo_doc">Tipo de documento</label>
          <div class="selectAwesome">    <select placeholder="Seleccionar" id="tipo_doc" name="tipo_doc" class="error">
            <option value="1">D.N.I.</option>
            <option value="2">CI</option>
            <option value="3">Pasaporte</option>
            <option value="4">Libreta de Enrolamiento</option>
            <option value="5">Libreta Civica</option>
            <option value="6">Otro</option>
            <option value="8">CUIT/CUIL</option>
            <option value="9">R.U.N.</option>
            <option value="10">Documento Paraguayo</option>
            <option value="11">CI Extranjero</option>
          </select> </div><p class="form_error error-text">Este campo es requerido</p></div></div>
          <div class="f-line">
            <div class="f-item">
              <label for="documento">Documento</label>
              <input type="text" name="documento" id="documento" /><p class="form_error"></p></div></div>
              <div class="f-line">
                <div class="f-item">
                  <label for="password">Contraseña</label>
                  <input type="password" name="password" id="password" /><p class="form_error"></p></div></div>   <div class="f-line">
                    <p> <a href="#">Olvidé mi contraseña</a></p> </div>    <div class="f-line">
                    <div class="f-item">
                      <div class="btn"> <a href="#">Ingresar</a> </div></div></div>
                    </form>  </div>
                  </div>
