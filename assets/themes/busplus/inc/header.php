<header id="site-header">
  <nav class="container">
    <a href="#" id="logo" title="Busplus"><img src="dist/img/logo-busplus.svg" alt="Busplus"> </a>
    <div id="menu">

      <!-- TOGGLE -->
      <div id="tog" class="container-T" onclick="myFunction(this)">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </div>
      <!-- /TOGGLE -->

      <div class="menu-list">
        <ul class="m-list">
          <li class="mr-e-1">
            <a href="#">VÍA CLUB</a>
          </li>
          <li>
            <a href="#">ATENCIÓN AL CLIENTE</a>
          </li>
        </ul>
      </div>

      <div class="lang">
        <div class="dropdown">
          <a href="#" class="lang-link" style="color:black;">Español <i class="fa fa-chevron-down"></i></a>
          <ul class="lang-dropdown-list">
            <li class="first-li">Español</li>
            <li class="dropdown-li">English</li>
            <li class="last-dropdown-li">Portugues</li>
          </ul>
        </div>
      </div>

    </div>
  </nav>

  <div id="mySidebar" class="sidebar">
      <!-- TOGGLE -->
      <div class="container-T change" onclick="closeNav()">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </div>

<!--       <div class="lang">
        <div class="dropdown">
          <a href="#" class="lang-link" style="color:black;">Español <i class="fa fa-chevron-down"></i></a>
          <ul class="lang-dropdown-list">
            <li class="first-li">Español</li>
            <li class="dropdown-li">English</li>
            <li class="last-dropdown-li">Portugues</li>
          </ul>
        </div>
      </div> -->

      <!-- /TOGGLE -->
    <a href="#" onclick="closeNav()">VÍA CLUB</a>
    <a href="#" onclick="closeNav()">ATENCIÓN AL CLIENTE</a>
  </div>
</header>
