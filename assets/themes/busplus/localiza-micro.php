<?php
use function GuzzleHttp\json_decode;
/**
* Template Name: Localiza Micro
*/

get_header();

// include('conexion.php');
$result = new WebService();

$mensaje = $error = $llegada = $salida = NULL;

if(!empty(htmlspecialchars($_GET["serie"])) and !empty(htmlspecialchars($_GET["boleto"]))){
  $url = "viajes/estado_viaje?posicionamiento=1&serie=".$_GET['serie']."&boleto=".$_GET['boleto'];
	$respuesta =  $result->call_curl($url);

	if(isset($respuesta->status) and ($respuesta->status == 0 or  $respuesta->status == false)){
		$error = 1;
		$mensaje = $respuesta->message;
	}else{
    $llegada = explode(":",$respuesta->boleto->fechallegada);
		$salida = explode(":",$respuesta->boleto->fechasalida);
    $posicionamiento = json_encode($respuesta->posicionamiento);
    $encodedPosicionamiento = urlencode($posicionamiento);

	  $urlMapa = $result->getUrl()."viajes/mapa?idviaje=".$respuesta->boleto->idviaje."&data=".$encodedPosicionamiento;
	}
} ?>
<?php if(empty(htmlspecialchars($_GET["serie"])) or empty(htmlspecialchars($_GET["boleto"]))){ ?>
<div class="fechar">
  <div class="container">
    <div class="title">
		<h1><?php the_title(); ?></h1>
		<?php
		if($error === NULL){
		?>		
      <p>Si querés saber la ubicación de tu micro, por favor ingresá serie y número de boleto. </p>
      <div class="breadcrumb">
        <h2>Completá los siguientes datos</h2>
      </div>		
		<?php
		}else{
			if($error){ ?>		
		<div class="flex">
			<div class="left">
				<div class="block grey">
				  <p><?= $mensaje; ?></p>
				</div>
				<br>
				<div class="block white">
					<p>Si necesitás comunicarte con nosotros, podés hacerlo a través del chat de la página o comunicándote a nuestro Centro de Atención al Cliente: 0810-333-7575.</p>
				</div>
			</div>
		</div>
		<?php }			
		} ?>
    </div>
	  <?php
		if($error === null){ ?>
      <div class="flex">
        <div class="left">
          <div class="block">
            <div class="pas">
              <form class="form">
                <div class="f-line">
                  <div class="f-item">
                    <label for="serie">Serie<span>. EJEMPLO: VB0 (CERO), W, VTI, ETC</span> </label>
                    <input type="text" name="serie" id="serie" value="<?= $_GET["serie"] ?>">
					  <?php if(isset($_GET["serie"]) and htmlspecialchars($_GET["serie"]) == "") echo "<span style='color:red'>ERROR, debe ingresar la serie</span>" ?>
					  
                  </div>
                  <div class="f-item">
                    <label for="boleto">Número de boleto</label>
                    <input type="text" name="boleto" id="boleto" value="<?= $_GET["boleto"] ?>">
					<?php if(isset($_GET["serie"]) and htmlspecialchars($_GET["boleto"]) == "") echo "<span style='color:red'>ERROR, debe ingresar Número de boleto</span>" ?>
                  </div>
                </div>
                <div class="f-line">
                  <div class="f-item">
                    <button type="submit" class="btn">Buscar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
	  <?php } ?>
  </div>
</div>
<?php 
  } else{ ?>
  <div class="container">
    <div class="breadcrumb">
      <h1><?php the_title(); ?></h1>
      <div class="volver">
        <a href="/localiza-micro" title="« Volver a la página anterior">« Volver a la página anterior</a>
      </div>
    </div>
	  <?php
		if(!empty($mensaje)){ ?>
    <div class="flex">
      <div class="left">
        <div class="block grey">
          <p> <strong><?= $mensaje ?></strong> </p>
        </div>
          <div class="block white">
            <p>Si necesitás comunicarte con nosotros, podés hacerlo a través del chat de la página o comunicándote a nuestro Centro de Atención al Cliente: 0810-333-7575.</p>
          </div>
          </div><div class="right">
            <div class="right__sticky">
              <div class="block">
                <div class="banner">
                </div>
              </div>
            </div>
          </div>
        </div>
	  <?php
		} else{	?>
	<div class="flex">
      <div class="left full">
          <div class="block">
            <h2>Ubicación en tiempo real</h2>

          </div>
      </div>
      </div>
     <?php if (isset($urlMapa)){ ?>
      <iframe src="<?= htmlspecialchars($urlMapa) ?>" frameborder="0" width="100%" height="600"></iframe>
    <?php } ?>
      <div class="flex mt-5">
      <div class="left full">
          <div class="block">
            <h2>Datos del pasajero y viaje</h2>
            <div class="pasajero" id="pas1-ida">
              <h3><?= isset($respuesta->pasajero->apellido) && isset($respuesta->pasajero->nombre) ? $respuesta->pasajero->apellido." ".$respuesta->pasajero->nombre : '' ?> <span>DNI: <?= isset($respuesta->pasajero->nrodocumento) ? $respuesta->pasajero->nrodocumento : '' ?></span></h3>
              <div class="inner datosdelviaje">
                <p> <strong> Boleto <?= isset($respuesta->boleto->nroserie) && isset($respuesta->boleto->idboleto) ? $respuesta->boleto->nroserie.$respuesta->boleto->idboleto : '' ?> | Butaca <?= isset($respuesta->pasajero->nrobutaca) ? $respuesta->pasajero->nrobutaca :'' ?> | Categoría: <?= $respuesta->boleto->categoria ?></strong></p>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col" >ORIGEN</th>
                      <th scope="col" >DESTINO</th>
                      <th scope="col" >SALIDA</th>
                      <th scope="col" >LLEGADA</th>
                      <th scope="col" >EMPRESA</th>
                      <th scope="col" >IMPORTE</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    <td data-label="ORIGEN"><?= isset($respuesta->boleto->asc_en) ? $respuesta->boleto->asc_en :'' ?></td>
                      <td data-label="DESTINO"><?= isset($respuesta->boleto->des_en) ? $respuesta->boleto->des_en : '' ?></td>
                      <td data-label="SALIDA"><?= isset($salida[0]) ?  $salida[0].":".$salida[1] : ''?></td>
                      <td data-label="LLEGADA"><?= isset($llegada[0]) ? $llegada[0].":".$llegada[1] : '' ?></td>
                      <td data-label="EMPRESA"><?= isset($respuesta->boleto->razonsocial) ? $respuesta->boleto->razonsocial : '' ?></td>
                      <td data-label="IMPORTE">$ <?= isset($respuesta->boleto->precio_viajado) ? $respuesta->boleto->precio_viajado :'' ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>  
    </div>
	<?php
	}
	echo "</div>";
	}

include('inc/ilustracion.php'); 
get_footer();
