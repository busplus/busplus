<?php /* Template Name: Línea Ética*/ ?>

<?php get_header(); ?>

<div class="etica">
    <?php $imagen = get_field( 'imagen_intro_linea_etica' ); ?>
    <?php if ( $imagen ) : ?>
    <div class="intro-cont-img">
        <img src="<?php echo esc_url( $imagen['url'] ); ?>" alt="<?php echo get_field('titulo_linea_etica'); ?>" loading="lazy">
    </div>
    <?php endif; ?>

    <section class="d-flex justify-center">
        <div class="container text-center d-flex flex-column align-items-center">
            <h1><?php echo get_field('titulo_linea_etica'); ?></h1>
            <p class="subtitulo"><?php echo get_field('subtitulo_linea_etica'); ?></p>
        </div>
    </section>
    <section class="d-flex justify-center section-2">
        <div class="container text-center  d-flex flex-column align-items-center">
            <h2><?php echo get_field('titulo_2_linea_etica'); ?></h2>
            <p><?php echo get_field('texto_2_linea_etica'); ?></p>
            <div class="cont-card">
            <?php if ( have_rows( 'cards_etica' ) ) : ?>
            <?php while ( have_rows( 'cards_etica' ) ) : the_row(); ?>
                <div class="card d-flex justify-center align-items-center flex-row">
                <?php $imagen = get_sub_field( 'imagen' ); ?>
                <?php if ( $imagen ) : ?>
                    <div class="cont-img">
                        <img src="<?php echo esc_url( $imagen['url'] ); ?>" alt="">
                    </div>
                    <?php endif; ?>
                    <p><?php echo get_sub_field('text')?></p>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
            </div>
            <div class="background-white d-flex flex-column text-center align-items-center">
                <p class="cairo-bold"><?php echo get_field('texto_incidencia_bold'); ?></p>
                <p><?php echo get_field('texto_incidencia'); ?></p>
                <div class="d-flex cont-dato flex-row-md flex-column">
                <?php if ( have_rows( 'datos_incidencia' ) ) : ?>
                <?php while ( have_rows( 'datos_incidencia' ) ) : the_row(); ?>
                    <div class="dato d-flex align-items-center justify-center">
                        <?php $icono = get_sub_field( 'icono' ); ?>
                        <?php if ( $icono ) : ?>
                        <img class="mr-1" src="<?php echo esc_url( $icono['url'] ); ?>" alt="">
                        <?php endif; ?>
                        <?php $link_dato = get_sub_field( 'link' ); ?>
                        <?php if ( $link_dato ) : ?>
                            <a href="<?php echo esc_url( $link_dato['url'] ); ?>"><?php echo get_sub_field('texto') ?></a>
                        <?php else: ?>
                            <p><?php echo get_sub_field('texto') ?></p>
                        <?php endif;?>
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>
                </div>
            </div>
            <div class="cont-card">
                <div><?php echo get_field('texto_3_etica'); ?></div>
            </div>
        </div>
    </section>
    <section class="section-3 d-flex justify-center">
        <div class="container">
            <?php if ( have_rows( 'preguntas_etica' ) ) : ?>
            <?php while ( have_rows( 'preguntas_etica' ) ) : the_row(); ?>
            <div class="d-flex flex-row-md flex-column justify-between align-items-center info">
                <?php $imagen_preguntas = get_sub_field( 'imagen' ); ?>
                <?php if ( $imagen_preguntas ) : ?>
                <div class="cont-img">
                    <img src="<?php echo esc_url( $imagen_preguntas['url'] ); ?>" alt="">
                </div>
                <?php endif; ?>
                <div class="cont-text">
                    <h2><?php echo get_sub_field('titulo') ?></h2>
                    <p><?php echo get_sub_field('texto') ?></p>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>
    <section class="d-flex justify-center section-4">
        <div class="container d-flex flex-column align-items-center">
            <p><?php echo get_field('texto_4_etica') ?></p>
            <?php $logo_etica = get_field( 'logo_etica' ); ?>
            <?php if ( $logo_etica ) : ?>
                <img class="logo" src="<?php echo esc_url( $logo_etica['url'] ); ?>" alt="">
            <?php endif; ?>
        </div>
    </section>
</div>

<?php get_footer(); ?>