<?php
if (is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')) {?>
  <div id="footer-widget" class="row m-0 <?php if (!is_theme_preset_active()) {
    ;
}?>">
  <div class="container">
    <div class="row">
      <?php if (is_active_sidebar('footer-1')) : ?>
        <div class="col-xs-6 col-md-3"><?php dynamic_sidebar('footer-1'); ?></div>
      <?php endif; ?>
      <?php if (is_active_sidebar('footer-2')) : ?>
        <div class="col-xs-6 col-md-3"><?php dynamic_sidebar('footer-2'); ?></div>
      <?php endif; ?>
      <?php if (is_active_sidebar('footer-3')) : ?>
        <div class="col-xs-6 col-md-3"><?php dynamic_sidebar('footer-3'); ?></div>
      <?php endif; ?>
      <div class="col-xs-6 col-md-3">
        <?php if (is_active_sidebar('footer-4')) : ?>
          <div ><?php dynamic_sidebar('footer-4'); ?></div>
        <?php endif; ?>
        <h3 class="widget-title">Seguinos</h3>
        <ul class="redes">
          <li><a target="_blank" href="https://www.facebook.com/busplusarg">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <g>
              <g>
                <path d="M448,0H64C28.704,0,0,28.704,0,64v384c0,35.296,28.704,64,64,64h192V336h-64v-80h64v-64c0-53.024,42.976-96,96-96h64v80
                h-32c-17.664,0-32-1.664-32,16v64h80l-32,80h-48v176h96c35.296,0,64-28.704,64-64V64C512,28.704,483.296,0,448,0z"/>
              </g>
            </g>
          </svg>
        </a></li>
        <!-- <li><a href=""><img src="dist/img/redes-sociales/001-instagram.svg" style="height: 20px;" alt="Instagram logo"></a></li> -->
        <li><a target="_blank" href="https://www.instagram.com/busplusok/">
          <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
          <g>
            <g>
              <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
              C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
              h192c61.76,0,112,50.24,112,112V352z"/>
            </g>
          </g>
          <g>
            <g>
              <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
              c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
            </g>
          </g>
          <g>
            <g>
              <circle cx="393.6" cy="118.4" r="17.056"/>
            </g>
          </g>
        </svg>
      </a></li>
      <!-- <li><a href=""><img src="dist/img/redes-sociales/002-twitter.svg" style="height: 20px;" alt="twitter logo"></a></li> -->
    </ul>
  </div>
</div>
</div>
</div>
<?php }
