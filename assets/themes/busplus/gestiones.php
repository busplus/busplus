<?php
/**
* Template Name: Gestiones
*/
get_header('', array('rolePage' => 'Gestiones'));
?>
<main>
  <section class="section-1 bg-gray">
    <div class="w-1200">
      <div class="d-flex col">
        <h1 class="title-h2 bold text-center">¿Qué gestión querés realizar?</h1>
        <?php
        $link_fechar_boleto_abierto = get_field('fechar_boleto_abierto');
        $link_seguro_del_menor = get_field('seguro_del_menor');
        $link_modificar_boleto = get_field('modificar_boleto');
        $link_reenvio_de_boletos = get_field('reenvio_de_boletos');
        $link_devolver_boleto = get_field('devolver_boleto');
        $link_fechar_boleto_abierto_url = $link_fechar_boleto_abierto['url'];
        $link_seguro_del_menor_url = $link_seguro_del_menor['url'];
        $link_modificar_boleto_url = $link_modificar_boleto['url'];
        $link_reenvio_de_boletos_url = $link_reenvio_de_boletos['url'];
        $link_devolver_boleto_url = $link_devolver_boleto['url'];
        ?>
        <div class="content-cards d-flex row flex-wrap space-between">
          <a href="<?php echo $link_fechar_boleto_abierto_url; ?>" class="card bg-white">
            <div class="d-flex col">
              <svg class="m-auto" xmlns="http://www.w3.org/2000/svg" width="91" height="91.07" viewBox="0 0 91 91.07">
                <g id="Grupo_841" data-name="Grupo 841" transform="translate(-215 -372.192)">
                  <g transform="translate(215 372.193)">
                    <g data-name="086---Desktop-Calendar" transform="translate(0 -0.001)">
                      <path d="M6,0H45.473a6,6,0,0,1,6,6L64.68,61.914a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6V6A6,6,0,0,1,6,0Z" transform="translate(26.32 18.525)" fill="#38c172"/>
                      <path data-name="Rectangle-path" d="M6,0H74a6,6,0,0,1,6,6V78a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6V6A6,6,0,0,1,6,0Z" transform="translate(0 7.07)" fill="#8fe164"/>
                      <path d="M71.915,13.087v9.261H4V13.087A3.1,3.1,0,0,1,7.087,10H68.828A3.1,3.1,0,0,1,71.915,13.087Z" transform="translate(2.174 3.893)" fill="#7c51a1"/>
                      <path data-name="Shape" d="M71.915,18V64.568A3.1,3.1,0,0,1,71,66.76l-8.983,8.983a3.1,3.1,0,0,1-2.192.911H7.087A3.1,3.1,0,0,1,4,73.567V18Z" transform="translate(2.174 8.241)" fill="#ecf0f1"/>
                      <path data-name="Shape" d="M51.527,49a3.113,3.113,0,0,1-.633.911l-8.983,8.983a3.113,3.113,0,0,1-.911.633v-7.44A3.1,3.1,0,0,1,44.087,49Z" transform="translate(22.284 25.09)" fill="#b7bbc3"/>
                      <g data-name="Grupo 830" transform="translate(15.433)">
                        <path data-name="Shape" d="M31.085,8.718H27.859a7.718,7.718,0,1,0-7.054,10.8,1.544,1.544,0,1,1,0,3.087,10.8,10.8,0,1,1,10.187-14.4,1.418,1.418,0,0,1,.093.509Z" transform="translate(-9.999 -0.999)" fill="#95a5a5"/>
                        <path data-name="Shape" d="M50.085,8.718H46.859a7.718,7.718,0,1,0-7.054,10.8,1.544,1.544,0,1,1,0,3.087,10.8,10.8,0,1,1,10.187-14.4,1.419,1.419,0,0,1,.093.509Z" transform="translate(0.328 -0.999)" fill="#95a5a5"/>
                      </g>
                      <rect data-name="Rectángulo 282" width="10" height="10" rx="3" transform="translate(13.523 35.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 283" width="10" height="10" rx="3" transform="translate(28.523 35.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 284" width="10" height="10" rx="3" transform="translate(42.523 35.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 285" width="10" height="10" rx="3" transform="translate(57.523 35.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 286" width="10" height="10" rx="3" transform="translate(13.523 49.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 287" width="10" height="10" rx="3" transform="translate(28.523 49.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 288" width="10" height="10" rx="3" transform="translate(42.523 49.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 289" width="10" height="10" rx="3" transform="translate(57.523 49.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 290" width="10" height="10" rx="3" transform="translate(13.523 63.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 291" width="10" height="10" rx="3" transform="translate(28.523 63.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 292" width="10" height="10" rx="3" transform="translate(42.523 63.07)" fill="#b7bbc3"/>
                      <rect data-name="Rectángulo 293" width="10" height="10" rx="3" transform="translate(57.523 63.07)" fill="#b7bbc3"/>
                    </g>
                  </g>
                  <path data-name="Icon awesome-check" d="M3.18,11.423.137,8.38a.468.468,0,0,1,0-.662L.8,7.055a.468.468,0,0,1,.662,0l2.05,2.05L7.9,4.714a.468.468,0,0,1,.662,0l.662.662a.468.468,0,0,1,0,.662L3.843,11.423A.468.468,0,0,1,3.18,11.423Z" transform="translate(243.677 418.509)" fill="#1d5c37"/>
                </g>
              </svg>
              <h4 class="bold text-cblack text-center">
                Fechar <br>
                boleto abierto
              </h4>
            </div>
          </a>
          <a href="<?php echo $link_seguro_del_menor_url; ?>" class="card bg-white">
            <div class="d-flex col">
              <svg class="m-auto" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="88.123" height="97.209" viewBox="0 0 88.123 97.209">
                <defs>
                  <clipPath>
                    <path data-name="Trazado 1245" d="M0-47.5H52.633V5.133H0Z" transform="translate(0 47.5)"/>
                  </clipPath>
                </defs>
                <g data-name="Grupo 854" transform="translate(-463.47 -371.512)">
                  <g data-name="Layer 21" transform="translate(463.47 371.512)">
                    <path data-name="Trazado 1231" d="M91.491,13.543H83.4A51.182,51.182,0,0,1,50.147,1.4a1.682,1.682,0,0,0-2.17,0A51.182,51.182,0,0,1,14.723,13.543H6.632A1.6,1.6,0,0,0,5,15.111V55.876c0,23.38,19.727,42.333,44.061,42.333S93.123,79.256,93.123,55.876V15.111A1.6,1.6,0,0,0,91.491,13.543ZM89.859,55.876c0,21.648-18.266,39.2-40.8,39.2s-40.8-17.549-40.8-39.2c0,0-.369-25.42,1.246-35.22s5.213-3.977,5.213-3.977A54.591,54.591,0,0,0,49.061,4.641,54.591,54.591,0,0,0,83.4,16.679l6.635,2.583Z" transform="translate(-5 -1)" fill="#4a5568"/>
                    <path data-name="Trazado 1232" d="M73.286,17.386A43.739,43.739,0,0,1,45.291,7.363a1.617,1.617,0,0,0-2.049,0,43.735,43.735,0,0,1-27.99,10.019H11.558A1.522,1.522,0,0,0,10,18.87V50.026c0,18.027,15.343,32.64,34.269,32.64s34.269-14.614,34.269-32.64V18.87a1.522,1.522,0,0,0-1.558-1.484Zm2.137,32.64c0,16.388-13.948,29.673-31.154,29.673S13.115,66.414,13.115,50.026V20.353h2.137a47.023,47.023,0,0,0,29.02-9.925,47.019,47.019,0,0,0,29.013,9.925h2.137Z" transform="translate(-0.208 4.75)" fill="#4a5568" stroke="#4a5568" stroke-width="19"/>
                  </g>
                  <g id="Grupo_853" data-name="Grupo 853">
                    <rect data-name="Rectángulo 294" width="63" height="63" rx="25" transform="translate(476 390.924)" fill="#4a5568"/>
                    <g data-name="6a55ae0b0372d859dadf75a13c389346" transform="translate(482.478 395.891)" clip-path="url(#clip-path)">
                      <path data-name="Trazado 1241" d="M51.113-20.9a4.756,4.756,0,0,0-3.148-4.688A22.155,22.155,0,0,0,26.181-43.75,22.155,22.155,0,0,0,4.4-25.585,4.756,4.756,0,0,0,1.25-20.9a4.677,4.677,0,0,0,3.477,4.767A22.151,22.151,0,0,0,26.181.573a22.151,22.151,0,0,0,21.455-16.7A4.678,4.678,0,0,0,51.113-20.9" transform="translate(0.135 47.905)" fill="#dad3d7"/>
                      <path data-name="Trazado 1242" d="M24.7-13.784c-4.155,0-5.54-1.385-5.54,1.385a5.958,5.958,0,0,0,5.54,5.54,5.958,5.958,0,0,0,5.54-5.54c0-2.77-1.385-1.385-5.54-1.385m9.7-9a3.463,3.463,0,0,1-3.463,3.463,3.463,3.463,0,0,1-3.463-3.463,3.463,3.463,0,0,1,3.463-3.463,3.463,3.463,0,0,1,3.463,3.463m-12.466,0a3.463,3.463,0,0,1-3.463,3.463A3.463,3.463,0,0,1,15-22.787a3.463,3.463,0,0,1,3.463-3.463,3.463,3.463,0,0,1,3.463,3.463" transform="translate(1.621 49.796)" fill="#95898c"/>
                      <path data-name="Trazado 1243" d="M23.75-13.75h2.77v1.385a1.357,1.357,0,0,1-1.385,1.385,1.357,1.357,0,0,1-1.385-1.385Z" transform="translate(2.567 51.147)" fill="#fff"/>
                      <path data-name="Trazado 1244" d="M24.559-31.062h-.043a6.042,6.042,0,0,1-4.342-1.806,5.179,5.179,0,0,1-1.49-3.943,1.373,1.373,0,0,1,.461-.954,1.373,1.373,0,0,1,1-.341,1.385,1.385,0,0,1,.954.464,1.385,1.385,0,0,1,.346,1,2.446,2.446,0,0,0,.7,1.852,3.37,3.37,0,0,0,2.389,1.01,3.624,3.624,0,0,0,2.4-.978,4.585,4.585,0,0,0,.044-6.477,6.082,6.082,0,0,0-8.59-.058,1.385,1.385,0,0,1-1.959-.014,1.385,1.385,0,0,1,.014-1.959,8.858,8.858,0,0,1,12.507.085,7.327,7.327,0,0,1,2.114,5.2,7.327,7.327,0,0,1-2.185,5.17,6.094,6.094,0,0,1-4.326,1.748" transform="translate(1.733 47.683)" fill="#95898c"/>
                      <rect data-name="Rectángulo 295" width="24" height="9" transform="translate(14.522 22.034)" fill="#dad3d7"/>
                    </g>
                  </g>
                </g>
              </svg>
              <h4 class="bold text-cblack text-center">
                Seguro <br>
                del menor
              </h4>
            </div>
          </a>
          <a href="<?php echo $link_modificar_boleto_url; ?>" class="card bg-white">
            <div class="d-flex col">
              <svg class="m-auto" xmlns="http://www.w3.org/2000/svg" width="112.321" height="74.424" viewBox="0 0 112.321 74.424">
                <g data-name="Grupo 855" transform="translate(-692.34 -392.195)">
                  <g data-name="Grupo 200" transform="matrix(0.839, 0.545, -0.545, 0.839, 210.242, -1553.714)">
                    <path data-name="Trazado 1246" d="M5,0h96.152a5,5,0,0,1,5,5V35.828a5,5,0,0,1-5,5H5a5,5,0,0,1-5-5V5A5,5,0,0,1,5,0Z" transform="translate(1476.16 1387.918) rotate(-45)" fill="#e9e5e7"/>
                    <path data-name="Rectángulo 84" d="M5,0h96.152a5,5,0,0,1,5,5V7.537a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V5A5,5,0,0,1,5,0Z" transform="translate(1476.16 1387.918) rotate(-45)" fill="#a17c51" style="isolation: isolate"/>
                    <line data-name="Línea 63" x2="28.87" y2="28.869" transform="translate(1530.79 1333.288)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4 4" style="isolation: isolate"/>
                    <rect data-name="Rectángulo 85" width="7.537" height="23.24" transform="translate(1531.234 1349.721) rotate(-45)" fill="#b2aaac" style="isolation: isolate"/>
                    <line data-name="Línea 64" x1="37.752" y2="37.752" transform="translate(1491.261 1354.607)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 65" x1="37.752" y2="37.752" transform="translate(1495.924 1359.27)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 66" x1="37.752" y2="37.752" transform="translate(1500.588 1363.934)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 67" x1="37.752" y2="37.752" transform="translate(1505.251 1368.597)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 68" x1="15.545" y2="15.545" transform="translate(1542.782 1325.293)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 69" x1="15.545" y2="15.545" transform="translate(1547.445 1329.957)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 70" x1="15.545" y2="15.545" transform="translate(1552.109 1334.62)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <line data-name="Línea 71" x1="15.545" y2="15.545" transform="translate(1556.772 1339.284)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  </g>
                  <path data-name="Icon awesome-edit" d="M26.138,5.4l5.856,5.856a.635.635,0,0,1,0,.9L17.815,26.333,11.79,27a1.263,1.263,0,0,1-1.4-1.4l.669-6.025L25.242,5.4A.635.635,0,0,1,26.138,5.4ZM36.655,3.915,33.487.747a2.539,2.539,0,0,0-3.584,0l-2.3,2.3a.635.635,0,0,0,0,.9L33.461,9.8a.635.635,0,0,0,.9,0l2.3-2.3a2.539,2.539,0,0,0,0-3.584ZM24.93,22.477v6.609H4.155V8.311H19.074a.8.8,0,0,0,.552-.227l2.6-2.6a.779.779,0,0,0-.552-1.331H3.116A3.117,3.117,0,0,0,0,7.272V30.125a3.117,3.117,0,0,0,3.116,3.116H25.969a3.117,3.117,0,0,0,3.116-3.116V19.88a.78.78,0,0,0-1.331-.552l-2.6,2.6A.8.8,0,0,0,24.93,22.477Z" transform="translate(746 428.11)" fill="#a17c51"/>
                </g>
              </svg>
              <h4 class="bold text-cblack text-center">
                Modificar <br>
                boleto
              </h4>
            </div>
          </a>
          <a href="<?php echo $link_reenvio_de_boletos_url; ?>" class="card bg-white">
            <div class="d-flex col">
              <svg class="m-auto" xmlns="http://www.w3.org/2000/svg" width="113.143" height="83.876" viewBox="0 0 113.143 83.876">
                <g data-name="Grupo 838" transform="translate(-935.855 -392.586)">
                  <g data-name="Grupo 837" transform="translate(935.855 392.586)">
                    <g data-name="Grupo 200">
                      <path data-name="Rectángulo 83" d="M108.5,39.551,20.1,1.938a4.905,4.905,0,0,0-5.363,1.023,4.964,4.964,0,0,0-1.1,1.637L1.9,32.85a4.981,4.981,0,0,0-.383,1.931,5.041,5.041,0,0,0,1.4,3.467A4.94,4.94,0,0,0,4.53,39.365l87.929,37.8a5.01,5.01,0,0,0,5.45-.976,4.946,4.946,0,0,0,1.127-1.632l12.133-28.412a5,5,0,0,0,.037-3.847,5.028,5.028,0,0,0-2.7-2.75Z" transform="translate(0 0)" fill="#e9e5e7"/>
                      <path data-name="Rectángulo 84" d="M95.835,39.551,7.433,1.938A4.905,4.905,0,0,0,2.07,2.961,4.964,4.964,0,0,0,.969,4.6L0,6.929,97.5,48.494l1-2.345a5,5,0,0,0,.037-3.847,5.028,5.028,0,0,0-2.7-2.75Z" transform="translate(12.668 0)" fill="#38c172" style="isolation: isolate"/>
                      <g data-name="Línea 63" transform="translate(15.01 10.762)" fill="none" stroke-miterlimit="10" stroke-dasharray="4 4" style="isolation: isolate">
                        <path d="M26.913.461,11.225,37.936" stroke="none"/>
                        <path d="M 11.6801700592041 38.13130187988281 L 11.99706077575684 37.3744010925293 L 11.08657550811768 36.9830207824707 L 10.76978302001953 37.7398796081543 L 11.6801700592041 38.13130187988281 Z M 13.5292797088623 33.71467208862305 L 15.06262874603271 30.05224990844727 L 14.1511754989624 29.66124534606934 L 12.61830711364746 33.32348251342773 L 13.5292797088623 33.71467208862305 Z M 16.59709358215332 26.38712310791016 L 18.13270378112793 22.71929740905762 L 17.22029685974121 22.32868766784668 L 15.68517971038818 25.99632263183594 L 16.59709358215332 26.38712310791016 Z M 19.66944313049316 19.04875946044922 L 21.20730781555176 15.37551975250244 L 20.29395866394043 14.98530292510986 L 18.75655937194824 18.65834426879883 L 19.66944313049316 19.04875946044922 Z M 22.74631500244141 11.69956398010254 L 24.28646278381348 8.020901679992676 L 23.37214279174805 7.631070613861084 L 21.83248710632324 11.30954265594482 L 22.74631500244141 11.69956398010254 Z M 25.82774543762207 4.339515209197998 L 27.37015914916992 0.6554114818572998 L 26.45488929748535 0.2659772634506226 L 24.91294479370117 3.94987940788269 L 25.82774543762207 4.339515209197998 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <path data-name="Rectángulo 85" d="M15.833,2.944,8.935,0,0,21.324l6.877,2.952Z" transform="translate(31.834 23.676)" fill="#b2aaac" style="isolation: isolate"/>
                      <g data-name="Línea 64" transform="translate(51.084 15.677)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.458,14.641,49.626,35.636" stroke="none"/>
                        <path d="M 49.42947387695312 36.09690475463867 L 49.82260894775391 35.17449569702148 L 0.6514627933502197 14.18115234375 L 0.2648076415061951 15.10094356536865 L 49.42947387695312 36.09690475463867 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 65" transform="translate(48.536 21.779)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.458,14.6,49.583,35.614" stroke="none"/>
                        <path d="M 49.38665771484375 36.07422256469727 L 49.77931213378906 35.15294647216797 L 0.6508831381797791 14.14241409301758 L 0.2646997570991516 15.06108665466309 L 49.38665771484375 36.07422256469727 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 66" transform="translate(45.991 27.873)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.457,14.563,49.54,35.592" stroke="none"/>
                        <path d="M 49.34376907348633 36.05160522460938 L 49.73594665527344 35.1314582824707 L 0.6501708626747131 14.10379123687744 L 0.2644510865211487 15.0213451385498 L 49.34376907348633 36.05160522460938 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 67" transform="translate(43.449 33.96)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.457,14.523,49.5,35.57" stroke="none"/>
                        <path d="M 49.30106353759766 36.02901458740234 L 49.69275665283203 35.10999298095703 L 0.6495621204376221 14.06525135040283 L 0.2643131017684937 14.981689453125 L 49.30106353759766 36.02901458740234 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 68" transform="translate(13.243 7.867)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.455,6.292l20.033,8.554" stroke="none"/>
                        <path d="M 20.29533767700195 15.30511665344238 L 20.67967224121094 14.38628196716309 L 0.6455060839653015 5.83280086517334 L 0.2637936174869537 6.750565052032471 L 20.29533767700195 15.30511665344238 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 69" transform="translate(10.728 13.933)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.454,6.275,20.47,14.836" stroke="none"/>
                        <path d="M 20.27794075012207 15.29526042938232 L 20.66181182861328 14.37754249572754 L 0.644920825958252 5.817003726959229 L 0.2636706233024597 6.733654499053955 L 20.27794075012207 15.29526042938232 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 70" transform="translate(8.215 19.991)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.454,6.259l20,8.568" stroke="none"/>
                        <path d="M 20.26056861877441 15.28541851043701 L 20.64397239685059 14.36881446838379 L 0.644330620765686 5.801239967346191 L 0.263542115688324 6.716779708862305 L 20.26056861877441 15.28541851043701 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                      <g data-name="Línea 71" transform="translate(5.706 26.042)" fill="none" stroke-miterlimit="10" style="isolation: isolate">
                        <path d="M.453,6.243l19.981,8.575" stroke="none"/>
                        <path d="M 20.24315643310547 15.27558612823486 L 20.6260929107666 14.36009502410889 L 0.6436737179756165 5.785509586334229 L 0.263346403837204 6.69994068145752 L 20.24315643310547 15.27558612823486 Z" stroke="none" fill="#b2aaac"/>
                      </g>
                    </g>
                  </g>
                  <g data-name="2750b32fb6352d09a710c163e6285714" transform="translate(943.955 411.59)">
                    <path data-name="Trazado 1235" d="M30.472,47.776a2.472,2.472,0,0,0,1.747-.724l7.416-7.416a2.472,2.472,0,0,0,0-3.5L32.22,28.724a2.472,2.472,0,1,0-3.5,3.5l5.668,5.668-5.668,5.668a2.472,2.472,0,0,0,1.748,4.22Z" transform="translate(15.685 2.092)" fill="#38c172"/>
                    <path data-name="Trazado 1236" d="M42.738,56.722H58.661a3.215,3.215,0,0,0,0-6.43H42.738a6.431,6.431,0,0,1,0-12.861H58.661a3.215,3.215,0,0,0,0-6.43H42.738a12.862,12.862,0,0,0,0,25.722Z" transform="translate(-10.53 6.007)" fill="#38c172"/>
                  </g>
                </g>
              </svg>
              <h4 class="bold text-cblack text-center">
                Reenvío <br>
                de boletos
              </h4>
            </div>
          </a>
          <a href="<?php echo $link_devolver_boleto_url; ?>" class="card bg-white">
            <div class="d-flex col">
              <svg class="m-auto" xmlns="http://www.w3.org/2000/svg" width="113.117" height="86.823" viewBox="0 0 113.117 86.823">
                <g data-name="Grupo 839" transform="translate(-1176.922 -387.235)">
                  <g data-name="Grupo 835" transform="matrix(0.951, 0.309, -0.309, 0.951, 195.43, -1349.931)">
                    <g data-name="Grupo 200" transform="translate(9 6.762)">
                      <rect data-name="Rectángulo 83" width="106.152" height="40.828" rx="5" transform="translate(1476.16 1387.918) rotate(-45)" fill="#e9e5e7"/>
                      <path data-name="Rectángulo 84" d="M5,0h96.152a5,5,0,0,1,5,5V7.537a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V5A5,5,0,0,1,5,0Z" transform="translate(1476.16 1387.918) rotate(-45)" fill="#7c51a1" style="isolation: isolate"/>
                      <line data-name="Línea 63" x2="28.87" y2="28.869" transform="translate(1530.79 1333.288)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4 4" style="isolation: isolate"/>
                      <rect data-name="Rectángulo 85" width="7.537" height="23.24" transform="translate(1531.234 1349.721) rotate(-45)" fill="#b2aaac" style="isolation: isolate"/>
                      <line data-name="Línea 64" x1="37.752" y2="37.752" transform="translate(1491.261 1354.607)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 65" x1="37.752" y2="37.752" transform="translate(1495.924 1359.27)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 66" x1="37.752" y2="37.752" transform="translate(1500.588 1363.934)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 67" x1="37.752" y2="37.752" transform="translate(1505.251 1368.597)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 68" x1="15.545" y2="15.545" transform="translate(1542.782 1325.293)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 69" x1="15.545" y2="15.545" transform="translate(1547.445 1329.957)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 70" x1="15.545" y2="15.545" transform="translate(1552.109 1334.62)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                      <line data-name="Línea 71" x1="15.545" y2="15.545" transform="translate(1556.772 1339.284)" fill="none" stroke="#b2aaac" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    </g>
                  </g>
                  <g id="_2750b32fb6352d09a710c163e6285714" data-name="2750b32fb6352d09a710c163e6285714" transform="translate(1200.52 407.59)">
                    <path data-name="Trazado 1235" d="M37.887,47.776a2.472,2.472,0,0,1-1.747-.724l-7.416-7.416a2.472,2.472,0,0,1,0-3.5l7.416-7.416a2.472,2.472,0,1,1,3.5,3.5l-5.668,5.668,5.668,5.668a2.472,2.472,0,0,1-1.748,4.22Z" transform="translate(-8.53 2.092)" fill="#7c51a1"/>
                    <path data-name="Trazado 1236" d="M48.531,56.722H33.089a3.218,3.218,0,0,1,0-6.43H48.531a6.436,6.436,0,0,0,0-12.861H33.089a3.218,3.218,0,0,1,0-6.43H48.531A12.615,12.615,0,0,1,60.885,43.861,12.615,12.615,0,0,1,48.531,56.722Z" transform="translate(-5.801 6.007)" fill="#7c51a1"/>
                  </g>
                </g>
              </svg>
              <h4 class="bold text-cblack text-center">
                Devolver <br>
                boleto
              </h4>
            </div>
          </a>
        </div>
      </div>
    </div>
    <div class="content-deco w-100 hidden-md-down">
      <svg class="w-100 d-flex" xmlns="http://www.w3.org/2000/svg" width="1492" height="140.617" viewBox="0 0 1492 140.617">
        <g class="mt-auto" data-name="Grupo 861" transform="translate(0 -626)">
          <g data-name="Grupo 671" transform="translate(0 -668)">
            <g data-name="Grupo 670" transform="translate(0 408.928)">
              <g data-name="Grupo 189" transform="translate(0 -571.311)">
                <path d="M280.619,1221.734c7.653,6.6,36.132,35.493,35.925,35.728l-.03.025a.1.1,0,0,0,.03-.025l33.656-27.477s26.42-24.492,51.909-9.35c4.967,2.95,35.058,17.26,35.058,17.26l13.538-6.259s20.56-15.335,51.357,13.2c36.113,29.595,71.938,24.912,87.252,13.751s28.716-26.951,28.716-26.951v48.953H0l65.8-73.155s16.773-24.1,34.791-4.951l52.07,45.246,52.3-29.845S241.028,1187.571,280.619,1221.734Z" transform="translate(873.969 315.697)" fill="#ffeed6"/>
                <g data-name="Grupo 177" transform="translate(1269.583 1502.083)">
                  <path data-name="Composite Path" d="M842.037,1299.306v37.716h-7.928l0-37.988a28.939,28.939,0,0,0,4.907.416h.105A29.017,29.017,0,0,0,842.037,1299.306Zm25.767-28.529a28.684,28.684,0,0,1-25.767,28.529,29.017,29.017,0,0,1-2.921.144h-.105a28.673,28.673,0,1,1,0-57.346h.105A28.68,28.68,0,0,1,867.8,1270.777Z" transform="translate(-810.325 -1242.104)" fill="#b3e3c8"/>
                </g>
                <g data-name="Grupo 178" transform="translate(1140.617 1456.383)">
                  <path data-name="Composite Path" d="M903.825,1305.933v55.341H892.119l-.005-55.74a42.954,42.954,0,0,0,7.245.61h.155Q901.7,1306.145,903.825,1305.933Zm38.046-41.861a42.189,42.189,0,0,1-38.046,41.861q-2.127.213-4.312.211h-.155a42.954,42.954,0,0,1-7.245-.61A42.056,42.056,0,0,1,899.358,1222h.155A42.216,42.216,0,0,1,941.871,1264.072Z" transform="translate(-857 -1222)" fill="#c8e9d7"/>
                </g>
                <g data-name="Grupo 179" transform="translate(1038.909 1502.083)">
                  <path data-name="Composite Path" d="M948.037,1299.306v37.716h-7.928l0-37.988a28.937,28.937,0,0,0,4.907.416h.1A29.015,29.015,0,0,0,948.037,1299.306Zm25.767-28.529a28.684,28.684,0,0,1-25.767,28.529,29.015,29.015,0,0,1-2.921.144h-.1a28.673,28.673,0,1,1,0-57.346h.1A28.68,28.68,0,0,1,973.8,1270.777Z" transform="translate(-916.325 -1242.104)" fill="#b3e3c8"/>
                </g>
              </g>
              <path data-name="Path" d="M356.108,1223.211c-8.077,6.97-38.134,37.46-37.916,37.707l.032.026a.1.1,0,0,1-.032-.026l-35.52-29s-27.884-25.85-54.786-9.869c-5.242,3.114-37,18.217-37,18.217L176.6,1233.66s-21.7-16.185-54.2,13.932c-38.114,31.235-75.924,26.292-92.087,14.513S0,1233.66,0,1233.66v51.665H652.276l-69.448-77.208s-17.7-25.434-36.718-5.225l-54.955,47.753-55.2-31.5S397.893,1187.154,356.108,1223.211Z" transform="translate(0 -259.811)" fill="#e0f1e9"/>
              <g data-name="Grupo 202" transform="translate(0 -259.811)">
                <rect width="12.22" height="21.48" transform="translate(35.199 1264.02)" fill="#b3e3c8"/>
                <path data-name="Path" d="M40.727,1162.746,64,1268.4H18.618Z" fill="#b3e3c8"/>
              </g>
              <g id="Grupo_203" data-name="Grupo 203" transform="translate(0 -259.811)">
                <rect data-name="Rectangle" width="12.22" height="21.48" transform="translate(448.289 1264.02)" fill="#b3e3c8"/>
                <path data-name="Path" d="M453.818,1162.746,477.091,1268.4H431.709Z" fill="#b3e3c8"/>
              </g>
              <g data-name="Grupo 204" transform="translate(0 -259.811)">
                <rect data-name="Rectangle" width="10.03" height="17.66" transform="translate(139.859 1267.42)" fill="#b3e3c8"/>
                <path data-name="Path" d="M144.4,1184.806l19.1,86.862H126.255Z" fill="#b3e3c8"/>
              </g>
              <g data-name="Grupo 205" transform="translate(0 -259.811)">
                <rect data-name="Rectangle" width="10.03" height="17.66" transform="translate(508.15 1267.42)" fill="#b3e3c8"/>
                <path data-name="Path" d="M512.686,1184.806l19.1,86.862H494.546Z" fill="#b3e3c8"/>
              </g>
            </g>
          </g>
          <g data-name="Grupo 692" transform="translate(39.713 548.084)">
            <path data-name="Trazado 763" d="M429.922,175.818l-3.6-7.552s-2.158.119,0,4.315,4.315,8.99,4.315,8.99.119,1.557,2.158,1.079Z" transform="translate(-44.467 -0.462)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
            <path data-name="Trazado 764" d="M436.945,196.065l1.438,10.069-1.8-1.079-1.438-7.192-1.8-7.911s2.517.36,2.517.719S436.945,196.065,436.945,196.065Z" transform="translate(-46.096 -4.888)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
            <path data-name="Trazado 765" d="M213.927,166.006h157.5s7.8-.223,10.428,2.158a3.636,3.636,0,0,0-1.078,2.157l6.113,11.867h1.078l1.8,3.955-2.517-.719,2.517,12.226s-.493,2.046,2.158,3.6c.213,2.435,0,8.99,0,8.99s-1.359,4.765-3.956,5.034-23.014,0-23.014,0v-5.394a6.866,6.866,0,0,0-7.192-5.753,6.642,6.642,0,0,0-7.192,5.034v6.113h-80.19v-4.315c-.047-2.065-2.2-6.473-7.911-6.473s-7.192,5.753-7.192,5.753h-1.438a7.372,7.372,0,0,0-14.384,0v5.394H226.872l-15.463-1.438s-3.955-.993-3.955-7.192V177.153C208.381,169.1,207.828,166,213.927,166.006Z" transform="translate(0)" fill="#996dbf" fill-rule="evenodd"/>
            <circle data-name="Elipse 151" cx="6.653" cy="6.653" r="6.653" transform="translate(240.062 205.227)" fill="#4a5568" style="isolation: isolate"/>
            <circle data-name="Elipse 152" cx="6.653" cy="6.653" r="6.653" transform="translate(256.244 205.227)" fill="#4a5568" style="isolation: isolate"/>
            <circle data-name="Elipse 153" cx="6.653" cy="6.653" r="6.653" transform="translate(350.818 205.227)" fill="#4a5568" style="isolation: isolate"/>
            <circle data-name="Elipse 154" cx="3.956" cy="3.956" r="3.956" transform="translate(242.759 207.924)" fill="#f3f3f3"/>
            <circle data-name="Elipse 155" cx="3.956" cy="3.956" r="3.956" transform="translate(258.941 207.924)" fill="#f3f3f3"/>
            <circle data-name="Elipse 156" cx="3.956" cy="3.956" r="3.956" transform="translate(353.515 207.924)" fill="#f3f3f3"/>
            <path data-name="Trazado 766" d="M376.428,208.754H299.474a4.932,4.932,0,0,1-5.034-2.158c-2.28-2.942-5.394-6.832-5.394-6.832s-2.736-3.956,2.517-3.956H367.8s1.978-.593,3.956,1.8,5.753,7.552,5.753,7.552S379.455,208.643,376.428,208.754Z" transform="translate(-16.504 -6.069)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
            <path data-name="Trazado 767" d="M371.1,167.357H215.392s-2.579-.42-2.518,2.518v11.147H354.916s8.735.324,10.428,5.754c5.178,6.761,11.508,14.743,11.508,14.743s.985,2.518,6.473,2.518h3.6a2.756,2.756,0,0,0,3.236-2.877,63.639,63.639,0,0,0-.72-7.912l-2.517-10.069-5.034-11.507S381.425,167.357,371.1,167.357Z" transform="translate(-1.106 -0.273)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
            <path data-name="Trazado 768" d="M374.935,195.279c.809-.09,2.607.45,2.607,1.708V220.72H367.293V196.628c0-.54.989-1.529,1.349-1.349ZM369,194.47l6.294.09c1.258,0,3.056,1.168,3.056,2.607V221.08c-.09.18-.27.27-.36.36H366.844c-.18-.09-.27-.27-.36-.36V196.987A2.457,2.457,0,0,1,369,194.47Z" transform="translate(-32.452 -5.81)" fill="#996dbf" fill-rule="evenodd"/>
          </g>
        </g>
      </svg>
    </div>
    <div class="content-deco-mobile w-100 hidden-md-up">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="360" height="132" viewBox="0 0 360 132">
        <defs>
          <pattern id="pattern" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" viewBox="0 0 625 110">
            <image width="625" height="110" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAnEAAABuCAYAAABIkkVfAAAABHNCSVQICAgIfAhkiAAAIABJREFUeF7tfQl4VNeV5r2vqrTvQgtYAil2jAXuiUg7IDmOEe54bLdZ3Z046UkCJNPjdBJ/QOdz7Ex6gsiX1ZkJ4htP4sSTIJJ0222Px8IYL51xEE5iCey2cNogoB2rtABaAFWptKvq3Tm3VCWXilredqveqzr3+wjBuvfcc/779Oqvc89CCQ5EABFIGwS8Xu92MHYrIbQJ/i6CP07GSJvdLh2glDrTBgg0NCICjLF6n8+3F54PeEYIfx7abTZpHz4b+MAgAuZEgJpTLdQKEUAEjESAfzh7vfJBSkl9NLlA5lqAzPEPbJeRe6MsayAwN+fbD8/H7gjauoDIbYDn4pQ1LEEtEYH0QQBJXPqcNVqapgjMe1fkYwHPWzwUXIxJ2xwO2h5vIv48dRAAAtcCBG5XDIv8z4UkyeslSTqMhC51zh4tsTYCSOKsfX6oPSIQEwEgcEVA4HoUErgQWdI2u522IbypjwA8IzWBZ0SFsWyn3W5vVbEApyICiIAABJDECQAVRSICyUIgEPO2A2Ka4NqUgTfNfzUK/1Y9+BVaLV6tqsbNMgu4h1aW5fWyTGqiXKPGtAWej6bAhF6MmbPMsaOiKYYAkrgUO1A0Jz0R4B43iHk7FivmTS0yIGuPzWZrUbsO55sfAa/Xd1AjuY9iHIMECNsevGY1/9mjhqmFAJK41DpPtCZNEVAQ06QFGafdbqvVshDXmA8B7nkDrQrh6nSHsQRuwVYXEP9Q0u+C+LlD6M0137OAGqUOAkjiUucs0ZI0QgA+kOEaTG4KmgyZpTyrkJcMMXTwjNWgQErZKYiDOmToBigsIQgY73lTpjY8P6ccDtsaZbNxFiKACKhFAEmcWsRwPiKQJAQCSQr7YXtew8twwqbQLCfEQm3DazOFaJlgGtR92w1kij83SRqYBJEk4HHbNEAASVwaHDKaaH0EIGFhByQp8A/iZJG3UBCxbpiFHil+1Q5JLlHrA4o3hbaDN65Z/D64AyKQfgggiUu/M0eLTYoAL/UAV6Tb4e+moIrg8WqH7MEiLdmDgs2ErFdph81G/IWBQc/jgvdD8YgAIoAIIAJhCCCJw0cCETABAgFPG88YtOpwAqnbg7XlrHp8qDcigAhYEQEkcVY8NdQ5pRAIdFToSg2jsEhwapwjWoEIIAJWQABJnBVOCXVMaQTACwedEeiWFDESiwSnyEGiGYgAImB+BJDEmf+MUMMURiCQcTqaSiZCJuQ+DGRPpRNFWxABRMCsCCCJM+vJoF5pgQC/SoUaXinWFYHy2mC8bh0ORAARQAQQAYEIIIkTCC6KRgQQAUQAEUAEEAFEQBQCSOJEIYtyEQFEABFABBABRAAREIgAkjiB4KJoRAARQAQQAUQAEUAERCGAJE4UsigXEUAEEAFEABFABBABgQggiRMILopGBBABRAARQAQQAURAFAJI4kQhi3IRAUQAEUAEEAFEABEQiACSOIHgomhEABFABBABRAARQAREIYAkThSyKBcRQAQQAUQAEUAEEAGBCCCJEwguikYEEAFEABFABJQg4PP5dssy2c7nUspa7Hb7ISXrcE56I4AkLr3PH61HBBABRAARSBICvO0ebP0hr1feQCnZG6ZGq91u25kk1XBbiyCAJM4iB4VqIgKIACKACKQOAkDganw+uQss4kQuypC22e20LXWsRkuMRgBJnNGIojxEABFABBABRCACAtzzJsvyLrg23Qqet3oFILnm57A9cL3aqmA+TkkzBJDEpdmBo7mIACKACCACiUPA62VbCfHtmt+R1sD/8D+qB2OkRZKIU5KkQ5TSALlTLQYXpBgCSOJS7EDRHEQAEUAEEAFzIBC4Mu0xUhsgcwccDttuI2WiLOsigCTOumeHmgtCIHDlwbPEihiT3saYFEFAo1hEIMURgHdJPSQtgCfO2AEkrtlYiSjNqgggibPqyaHeQhDgBA5euscWx6uwdpvNtg2vMIRAjkIRAUQAEUAENCKAJE4jcLgstRDwer07IF5lP1jFY01qIlh3CNL9YQ4ORAARQAQQAUTAHAggiTPHOaAWSUCAx6tAptgW2BquTUlzHBWA3LF28MZxr9yBJKiLWyICiAAigAggAosQQBKHD0RaIcA9bkDEVkCKfxFcmXLPWowaTVGhabXZpFaQczytwENjEQFEABFABEyFAJI4Ux0HKiMSgbk5XwsQt0Cqv/6dwHt3CgKM1+iXhBIQAUQAEUAEEAH1CCCJU48ZrrAoAuCFawPipcXzFsNiW7PDQdstCgmqjQggAogAImBhBJDEWfjwUHVEABFABBABRAARSF8EkMSl79mj5YgAIoAIIAKIACJgYQSQxFn48FB1RAARQAQQAUQAEUhfBJDEpe/Zo+WIACKACCACiAAiYGEEkMRZ+PBQdUQAEUAEEAFEABFIXwSQxKXv2aPliAAigAggAogAImBhBJDEWfjwUHVEABFABBABRAARSF8EkMSl79mj5YgAIoAIIAKIACJgYQSQxFn48FB1RAARQAQQAUQAEUhfBJDEpe/Zo+WIACKACCACSUTg5Ej3etlH6yllRQz+EJnWE4mdooy6uFpUIu1ry+qwR3MSz8jsWyOJM/sJoX6IACKACCACKYNA5+DZLYSyrYTwP1RhG0DWRilty3BkHV5TXOsneDgQAT/RRxgQAUQAEUAEEAFEQCwCJ4a7tzOZNMOnbo32nZiLMKm5ofKmA9pl4MpUQgBJXCqdJtqCCCACiAAiYCoETg6fr5dl334gb02GKcaIk1Jpz7qKlW2GyURBlkQASZwljw2VRgQQAUQAETA7Ah3D3TsoY/uVX5uqswg+wJvXVdTtU7cKZ6cSAkjiUuk00RZEABFABBABUyDQOXxmP2F0dwKUac3MyNqDsXIJQNqEWyCJM+GhoEqIACKACCAC1kWgY6i7GT5c9ybQgtaGirqdCdwPtzIJAkjiTHIQqIY+BNounKzeet3afn1S4q9+ZfAP5e456vlk9a1T8WfjDEQAEUg3BOavUMnBRNvNCNnXWFHXnOh9cb/kIoAkLrn44+4GIfB8/4nvb65e94hB4qKKOdJ3YiuT2ODmqoZO0XuhfEQAEbAWAv4kBuY9JioGLh4alEjbMNkhHkqp9XMkcal1nmlpzdHhk5XyjK9zU3VjjWgAOFmUKBvcWNXQInovlI8IIALWQqBzsPuYoVmoas2HrNXMzKw1GB+nFjjrzkcSZ92zQ80DCBztP3m3TOSXpExp6b3lawdFAnOkv/Nlxsj05uUNUKgTByKACCAC8wicGDq3lRH5uWTjgdeqyT6BxO6PJC6xeONuAhA40t/RDNcXeyETbNum5euE1U1ijNEXBjqH4C8ZSFylAFNQJCKACFgUAfDC9egr5GuU4cyVmZFdi944o/A0txwkceY+H9ROAQLcOwbT7mKE/kBkXNyRi2/eRHzebq6S3c5q71na6FSgHk5BBBCBFEfALF64BZgZ29NQuQpDPlL8uePmIYlLg0NOdROf7+scpJRUMMKOb65ubBJl7wt9nZ9ilDzp/8Vh5NMblzc8JWovlIsIIALWQaBz8EwLoXSXWTSGkI/jjZV1wt6FZrET9UASh8+AxRF46VJHjddLe7gZQOKm8r2VJRtqa6dFmAUeP/7NNviiPrCpuiERhTxFmIIyEQFEwEAEzHOV+r5RUAC4GK9UDTxkk4pCT5xJDwbVUoZAqHfMT+QoaRRV/uP5/s5O+IVZN08YyYnN1Q0NyrTEWYgAIpCqCMyXFfF1mc0+eBfubCyvazWbXqiPsQggiTMWT5SWYAR4yQ9K2MPBbeFadY+I8h88qeHIQOcEJTR7nsSJ9folGEbcDhFABDQi0DHS3URlArXhzDUwS9Vc5yFKGyRxopBFuQlB4Pn+jnYgVusXNmPknzctb/iU0ZsfHTgJ37blRd+2RXr9EtWBwmicUB4ikG4IQDzcboiHgyb35hpI4sx1HqK0QRInClmUKxyBcO/Y/IasV0TR3yMDJ75IGPtJqFGivH5P97+enUWkh+C69lvCQcQNEAFEQBcCSeiTqkhfTG5QBJPlJyGJs/wRpq8BoSU/QlEQUfQXMmAfB9L2wCK0BXn9oO5dE+QcPQKJE3en7+mi5YiANRBAEmeNc0pVLZHEperJpoFdzw90QKNpem2jaQFFfyEz9RRA+qHFsIrx+kECxSPwi/nVjVXrKiiFaBsciAAiYFoEzEri4FbicEPFKuwsY9onxxjFkMQZgyNKSQICEb1joIfRRX+P9fRkeexDkNRApHAzBXn92sDrt4XY7HWblt1yNgnQ4paIgOkQeG60q8gxMbsFwii2wlVhPfyO1IQoyb9kOeFLT9tcbsbhbcVrXIkyoGO4G75Mkmu/TCZKgSj7JDomjp+PzTPD45O3wtnUw9/8j3/AecHZEH5G7VAo/TAWSjfu4UASZxyWKCnBCISW/Ajd2uiiv88PdDbAS7ojonkCvH4LxYsp27m5qrHVCFh7xoa2SoxsAWzWAsuthUDsUXiznmOS9CuSX/ZkLaVCausZoTvKSG8EAuRgL5AARXUZgTC4gMy1ePMyDiSCzGF2KiFH+k/sBXK9G86oSMnTCmfU6nCwfUjmlKAVew6SOP0YooQkIDDvHRu8Giz5EUbipjZVNeTCixy+jOof0C91N7x0ImafGe31Ozp8slKekS8Fvr3+FHq0flGPBQOTV6q8s3O/BNK2IbocdgVi8H6cU0AfLafl43r2w7WIgJEIHOk7AU3l2UGl5GDRewDInE2SNtxbtZZ7gISNrtGeopnZ6VFhG2gULFHbmrXlNwq1nWft+2T5uTCvqCKNOdkmEttj1BdVRZum4CQkcSl4qOlgUkzvGAAgUWmNUS/vI32dT0GDuvsj4Wq0149/aEFPr+cCe70NyQ0LVxJqz9U5NtRIZHIUdC9WtJaRUxlS9p3LCgouK5qPkxABgQhEjXlVuScz0KMdbevOoe4IMbMqFTV2uruhok6RV0zrtnoIdhjZboEvq3u06pHu65DEpfsTYFH7I5X8WGQKpX+3qWrd40aYB9e2Z+EXZWUUEmeo1y+0eDG4EeVpIud9svrWKbV29LqHPgvrf6l2HVyJ9Dkk251VBWXn1a7F+eZHgI12b4dr9B2gaVOItk64Ym+Dbz4HaHGd0wxWGEXgFmwREPYQipPZkhvgd/9QY0UdP2chI+CBO6bFQxpJIVHlmoQYbzKhSOJMdiCojjIEjvR1tMIV4faosxk7tGl5o+6XGJQxWcJ83qFISQ3BvQ31+vV3vgxy73rfLrYB6t61K0NlfpbTNfhfAZvvqFkTOhc+AIYyHPaPXJdT2q9VBq4zFwJA3pqAvPHg+5romjEXoVILELl9ydTeaILAbeFXdxCDtUZUDFbHaHcNnSX+Hs5mGEwiGxrL6lS9N5TqHYhR7DGKwC3sK5hoK7XPavOQxFntxFBf0utyFb/jOfdHmbCqaHA4qH1mZX71a/CAd0JA/++zC6WOMlrmUQvf0f6Td8tEfinmOoO8fuAFkyD+bhDi08qC+wGh+joU/f2+Er37Gcv2jQ3/DOZ+Rsn8OHM6M6WczUvz80cMkIUikogAELi9wGKaFavAWCstXb1T8XyDJ0I5H97CqslgsVxcO4QnxIgN1bcjeONa4X0T/YulPvGKV4su8hv3C7RiTRdP5BmscK1aq3F52i5DEpe2R28tw/vdwzf4CPs0XPts9TLfh7vH++IaUJe3nNgl28I8iF97A+rKtRMbe7kmv/K3cQXAhEDNtu/FnGuc1+8m4vN2h+4FL7bD8GKLW+vp4tToitmZmRfAA3ezEruUzWHdBQUZjSW0xK1sPs4yGwLsyumD8Ezs0KBXG7QJ3kmLaxNWqoPrGBYTqkHteEvUe7bjSQz+3CzeOJFeuJcuddR4vVSYxzER8YtKz9Mq85DEWeWk0lRPXhoDyt1+GYLzPx6EYNw7RXomwWEVZ9TmVJI8u79ffYTB3oT0h701heUvxhID5T7ma7bFGOAtOwfespvi6RPv5y/0dX4K+rE+GUbihoDEVcZa2+cZvFmW6W9gTsx58faP8vMOVlB+B5Yg0YheEpexK2da4Pdml3YV2ClCczYkksgp+X3Tbg+sjPKFC7yVgf7LMiQDSMFkovbAXr1KYwWhjypgTnVgrss6fm18vLGyrkmflOirwUvaAj8Vad8p8JauEaV/KspFEpeKp5oCNvW5hu+XqbwXrhbrws0ZmXWRQQUZ/RWZRaQ8M05iJmP/SiVb84qCshciwRas2RaHxMleSsvvq1oHpTq0j2gvSCiOWRstlqdvbPhjPpkdAaJZqH3nOCsZea6mqOI+YfJRsOEIACnZEYiB0ymbx8kRIHKrhZaq4EryWCv7+IzQUh08Nm5TfgVc2U1vIbLcBLYBYaMKMsABBwJefEqA2NHDsUhdEjNV3ZkZWTVrBHpP4R3VBUelAC/tj12s9512qam7EkmcRc+WXy/KlFUQandlUe/liryKIYuaskht/7Xg7Oyv4T/eFs2e3skhMuadjGtuPnjhasAbp2SAN+1fqF3aUZNb5q/RxkdozbZ4MiQi3XNv9VqelKB5RCteDNXuPr1xecNT4YKdY8Ofhw/qn2veUN3CH9cUVnxZ3RKcnQwE2OjpesIo/7A1aPgTHrYBcWk3SGBEMYGewTweTuhozM4lpSFhFho3A0JHWwGTQ+HrA9eqnPSK+2IVQWnRdeESQbK5WXilqu6JRBKnDq+kzL4wNrJyjsn/CTbnV4o88DMiM4GYr5MQ8/U7eLm8CteEsYPxk2JJ7E2d7sG/YYzyRvP5sWZ2e/oIxMXFtcBGJcLj4qDob9y58xPYFcqkL60oKn+a/0tdfA7bB1mkzQo3umYaT2o4MtA5Hql4MUw+AFcMi6rVO11Dj4JX4CGt+2lax9g3aooqv6tpLS5KCALzBI4AEaLG1wijFGLk6lpFGWJ4WZEoihpE4oLSnfCCaQ4ncyeHz9fLzNcOkxJC5CAMY2djubiz8b8P+zua4LkSTrLhPazrXSrq+TSrXKWfbmbVP6X1mg9Wn/0RfFirv8pirAe+Pf8oo6DsF8soje+2SiKSQGDsTs/wY+BxeiCeGpy8cRKndNyYV0UyJYfS6fPzOHY2+tAfXc6PUMIeVrj4FSBadyuce800XlZBZnJE7wl4CU9AzF3DvGrM3use/id4Jj6hdS+d6z4DHrl/1CkDlwtAAK5Qa4j/GRJA4IL6zhMWISVIgCQ0g+4QQiF2rM7MIrX2DKM34WSOk9z2oOAEETk3JDJsFVVOJBSkxJG4a7+0Gn1YqSQPSZwJT7Pf7S7xkqlvglfGgABSdhleLt+rKaj4kQlNJZc8nrIZebINdLtViX5jc5Okd0r5zXFV9hJS7Ijp2Iu67b9PDLinfXNKv0lf3li1rlxrq69YxYvBwzo1TVjpx6tuyhhzz0LWIG1SgpWoOdBq7J7awnJdV8eidEtXuQxaPxE2yT1wQuOV/PgKKkFicRI3/+gx0kKk7H3BZBDekmt6ZponRwUSJwx9Qt+GK9Qdeltr+Z8dMgn6QUIHY/z5WezFpRALSORTL4yPQZY6euIMPUEDhCGJMwBEI0X0uIea4UWwB37pC4yUC7IuwlXrnuBVocGyNYnjfT3n5ryvwkN4o1IBPKGBJzYoHSVA4K4DIqd2gMeLnPb0wlGoaL9qs9dtWnbLWbV78fmQQMGvkaN6IgsdOVuXZ5dDvTiqOwtWi36L1jA2KRHb+uVFZZDhiyPZCCSUwC0Ya3zmqkWvUyMcP8fGH0PoDP4QslZ382tX+LfSL4UxHyt4K+2DjgxcnqYxT9wguYPJEKahjPhf9nnHO6cn8zRtqGoRXqeqgQtJnBq0BM3lnjeZTv9nJrMvwi+64GKH7EWbw/5fqnOWXBBkjiKxgQSGV2Hy9YoWBCY5obSIB0qMKB3ZtgxyQ+51SqcvzJuR58j58QFV6/QE5ELWV8zei5WZxRNlmUW5qhQSOpldthGpsbqw/F2h26DwuAiwq2d4r924tQTjClI94VqyolpEyIJYIQV65IavvT07jxRIkpEiI8jiySD8enUVv2XwD79XbnZ6N297Bh77FRoUcHMvKMukLY0a26MFyNuuAHlTFTc5Bwq/MjGmQW11S/S8R9XtlBqzkcSFnWMPGy2yeWZulwldCc6YhQ9N8JJ4qMRO5edmvGlU8dOLjOXMuEfgypR9PV4wv6GPGyPj0GT9gZrCyn8yVK5CYc6J4TXEK0NdM1qqcIl/GveO8SK/PiarWUZW56+AtpDqXtqjcx4yMKWyD7zGor9P97+enUUkSGogUZUssueR6pyFRg6q7Bc2mZH3Mm05DdjVQRjCcQXrKOYbV7ayCcaWIAGPtEtkuZxsEP4XOdrCK5ThETYrSjLIiaFzWxnzQYkTf2jEh2LIfhu8buDZI+16ExfmO3f4PW+qyFuobq9PT5CrvvhJZZqwCizy5mUWbyteo/y6Rc9mKbAWSVzgEHvHRjZCNtFnIQ7tk3HPlZFniE36SU1+maZMHX97JM/w3wEpeQT2S+InM3sMiNyDce01cEK/e3Cdl9FXtLyotXjHuOofgDIjuVGL/kY27gIQuKtA5NQMrUV/nx/obICkjo5Ye2VIdrIyr1qNOomZy0hXTiG9vZyWjydmQ9wliID+Yr5GYekvQbLHiMxVUS2dgpbyhAae2JDQQclu8MgdiLZnx2B3e3jMnJEN7AMZy9A3V9m1aSxseryz5PTMtDD4lHaoEaaABQVT59RgbU12pbA2GmbG5OLU1eWzs3Pg3iY74ZuO+m8njP3CIdkeva6g7JwSO/2to5j8CASGf0JAzJsSFa6Zw1tROWjOPVUFBboK1SrZ3OkeuZcw39Pw7TNHyfzwOe65cdI3pb6VZ2VWMSnLUHe8f5q4SCZ9M6rUhBevpqK/0C91N7y89sfbrC4f2ojR99uIxZufqJ/zGnu1hRV3JWo/3Ae80oYV8zUQTQNKkIhu6/QXOXkkW6VX3hCEYiSDRCFxumLeFoi+2r65cYzlV6qvTniIV02ssCoAF7dF8xNQQuDqeaGLBvxT5qEn0EVDfAFqVaonaTKF2lxncgts67Q0B0+Szrq35TFoPjb1QyATn9ctjAuAivY2yn5QXVh5IpI858TIh5lX3gVuz88Zsp/RQhi5APr/VTT9jdiub2xwi8zoQnyIFpmXpq+Qy7PqYzIK7DlkRU6F4i2h1Ac5A2VMVCU1BKRrKfp7pK/zKfgScX88BVdkV5AChyb+G0+05p97p33krV/3k4vdl8ncHH/F4whHIBM8PxkZme3M6316aHDgyea2nbquikxJ4BZYA2ulpat36nkKRLXeqrLZSX1WEn9/ohA5ESTOH/smT+7X2Dc35vGdn50m5+dm9RxxxLXwvj2+ubqxyV8qR4ZuPZTHeca6+gUPMCNtEITSBoTusOEKWUQgkLghnn73MtR+usciOutSE+yForkMXNvq4rEUbvp7+Mbw/ZrCsqN8fr/78lof8W2H//slheuTOg2yV+8Xkb3a4x6+G+qt6S4+rMU7xgFVW/SXe+D4XlpGnj3zmQ1L18S/kg8RDp0azgLBXxlvP+5N5F5FM40LXW7yzrMXyIUL/UQGdyKOyAjYbDZSUlwyMDIy8vNvt21v1oqT8d0YtGoSYx2QFSLlwPVqrSayyjsDQFyyU0vIRTSteCJDIyQ0qKwYaTw4EYic0SQuEdnKr02NkzFZXWxyLDDh1eFemZW18Uab4wsaiSev09dKSNYBrc+d8YctViI/Z25rkMTxqPFfQDX2L4jdNnnSe12uYkZmngB2/1fitWCngSS+Dfv8jfi9jN6BPgjdHh4zSmqva+hOqCb+L3rlaSr5EbKpmqK/PBaOx8RpGbzVF3jMjhO7tLs2rzxuv8kXL71V5vXODsZKagjqkWvPgvi+pVrUErbm3VdHyJ+OjZD+AeUFmIUpY3LBWVlZc3Ozs+f2PfvZP9OiqtBuDFoUirlGXwkSnqnqk2UeK6a7JIcdXvq3Qqst8RmpCkEMI3JGkrhEPSNTcFtxfHLCsGvVmzOyflvjyLhDIYKxvkHw+EyIQby2HZp+2eaRECTqtGT1mvdJ3Lx+/ws8cl8xj6rGaNI7NnwbeAn+D3xQKr9TM2Zra0qB62FWmPH5Wlqs6Zt00GinZ2QDuMV/awQI075Z8u8T2quiqCn6OwBxd6MQf6dlhHr9gLz+NIvm/LdY2ZtH+0/eLRNZkZeStw+7Ob9Gi1rC1pz83z1k1DmFJE4hwkWFRSeuutyPf7ftc60Kl/inJaQbgxqFlM0F7wiDemnqY5fA3qZRn/fxE1OTK/XEX5mOwAVxCyFyRpG4RF+zc0/cG5CtOqXDAw8Rvt6bs7Lt1TbDfaS8ty3voOFU9qhaZ1aop5WWrKLhJC7liFyvZ/ivof7aM9Y5InNoCp6vXkja+I9VBWXntWjk9AzeQXzkBfhFytayPnyNHu8Yl6Wm6C+vD8czYbWORV4/Xs5FIt+Cjhk/jCTv+OAfD0MXis1K9/og1LzLgtp3ZhlI4tSdRHZ27uzszNR5Nd64RFyPqbNCzWx1JUjmySrjST7+unfc49M1M6WprEUJXGGvycxOTiKDEogCRM4IEpesUjP8LXlqapIMyV4lFi+awz2j9Zk5Aj2kxmVNqzZO4ILQupDRSFzKEDnn2NBDEPj4qEA8U1s0Iy7JRrcszy9/TY2hvJE9XCcb2l9TS8mPUJ2VFv3lSQ28U4OeEdHrBzXVJIn9/fKCyoUAXKd7+Nu9k4PfGPMqb23Lu09wQmqWgSRO3UlI4E0tKij5w1XXyD98u21ne7zV1iZwIdbFyVx9vwgta46ESb8Pim9DaQslXh9ODm7MyCKVkMhg+gFErnNOqo1QYkRRdur88zHFiz03JdPWK7KPnIOEByU15Hitvg9kZIroXxsZAkFt4pKBdzhZj0XiuH7/CFern0mGonr3HGEj+RNu+edJbBKu1wTC3d5NAAAMIElEQVRTrWeQxVtbUA51huKP3rHBTYzR5+PPVDfjXbhKnYIrVT1DSdHfCegG8R50hdAzYnr9GDsGLau+JhMf787xhW7IgvUy5cUzix15pCo7iaUFw4AJkrgZgbWj9JyF6LXT0zPE64VOx9OT/mLUSkZhQfG0Z3zsuW89+5m4MbPJ68agxBKVc6LUSwPv23YoQtuipAgtv8K7Al6f92ZnFggdvzKtBK9bod1OSqGeomli3xTC88c5Ogg9aCpDpytpqxVIcuEErkbhVsKncc/poM9L+iF7NTTxoQLOpdAmkUqo05ek84Hr1Wy42teWbCMcOAUbRPK2xiNxvBLM7woLHJuM6lCgQE/dU/rHB//M56P/FwTdoFsYClhAAD6fHq0tqng4FiT9npH1Pp/vJaOuUIN7GeEd47KUFP3lfVl5f1Y9Q6nXj5M3TuLUjEzJQfh1rVlGkMSZRZ9k6SEDuRgdHSWTUxNxVeCZqsuWXufKsJHanS0bosadJuuKLK4BeiaEeEV43BuQN7g6VV+ENrRzAL82vTXLRB3pVOJzxgvtgML4fzwSx0bPbIVvDbyAr7oCmCp10zo9vAzJxlyjW4Fr0Ywn2xCIk1Mfo6llN6PWxCoXE5fEzSvBztodjjurckrVNZI0ygIVciAT8gHou8a/0SW4JLcKJS08lZN6B3ybiVQYuMcz0kR9vheNJnAcLj0lP0LhVlL0t39yhLi82pIaQvdS4vWDWDjSOzWk+okwU9FfJHGLj294ZIjMzMQvEr2krOy9uVnv/3yodTO8r64d5unGoPrxVLKgHSZx8qq532s6k7j59lmRr52VgJ+IOeYkcX4+c01P20TgoXWPeOEUCkkcr2VLhhw2cldVXgUvm2G64fRcriOy72eg2G2mUy7VFGJkgEm2O2sLlpwNmgaxXVBjkL0oytQrUOD3IhT61TuUFP09N95PZjUE6YbrpsTrxz1+3POndpip6G+qkzh+Q0qhJ9r8iN+lcBaukYaHh+JerebnF77r8bgvfPu57U3h55/oLEO1z58Z5qcjiRNZwNfoMzUviQtYykgLLV21x2i7jZQXj8D530hRslMj6gEvMw90/NlZk1/xrJGK6pHFe5B6x4ah/yj5ph45uFYdAlBZexgCtO9bUVDxB39BY+Y9prWVlpKdjfKO8ZZV3IsVbWi53owmS4nXzwmxdx6IwVM7zFT0N9EkbmJigkxM6PeUKsE8OzvHH+8WHDwpgRc0hmJv/v8kQQB9SUmp/+/QMQXZepevxK4zCLLH4erV953ndiy6DkMCp+RkCEk3EhcoMQPxb+qvnpUhauws05O4eXN5gWBehqTdWOv1S1Ma76iKxAXVgnfYz0hh+S5IpxHXBTcOBoEG8l+GQOKvJbeBvP7DsrIEHicHBPpvwUEhtI2A3pIfoRjfBPFkDogrizTGgVD16ExqCMpV4vVTm9QQlG2mor+JJnEjV0bI9JR64qvl96wwv5C4Pe5FS8P/m8PuIOXlFdcQOR4fNw49JqMNHhcH7rp3qM/7sWAbLkt0Y9ACpIA1qU7iyig7dX35qjUcukQV8DXymCxC4uZNBq8ckbL3mSXpIRArygl73HhHTSRu3mZyHr6VfmFFQTm0mUrc8Pc8JdNfBg12CWqblThjcCdFCHghdb17XF3wfyzByyGzsxAyPCON4ZlRMjSj/nozkqx4Xj9eh46TUy3DTEV/E03ilF5XasE1fI0SEsfX5OTkklLwyIWPePFxUDPulalJz/d5qRErflAbgbFWGalO4q6TGKmyE7juo9AflCcwWGtYisTNMzkeK9dMi1dBS87kDbXxjppJXMBEqCRBfpaXLz1URsuif+U0AI9ez5VVzDf39+Dxgb6nmLRgAKSWEWGkd4wbvSSjgCzNuvYDl/9M6/VmNDBjef1GobXXgMbWXnw/sxT9TTSJ47Yrua404gFXSuL4XiXFpSQ3d3GGJM9YvXjpQtT4uKKC4teuul0Hv3PwI+2Qpdml5Ju3EXalgoy0IHHgrLXqsB6JW0CaX7ECmUts265AkWtO1pvUnLleEjfPXxkZg+bmT1NJOmSUZw6uSaU+z8itssw2gux7AdSb1RiGc1MHASNKfoSikWPLJNfnLosIkNbrzWhox/L66S1ebJaiv8kgcRxvt9tNxsKuOo1+6tWQOB4vV1GxlNihVlno4J7DoaHIdQdLS5acG59wtf3Dj+vvskqsk9EYa5WHJE4rcolZZ2ESFwDI3+2BZ44fEt26a977Ju/W8iXOEBK36JFgzAmFYY9LhHUBqevKzqNdSrx0A5NXquRZ7yofJR8FeQ2QDHYreN0i33kl5hnEXSyAwBxctZ6NcNW6DDxtpeBxM3IMzlwlIzOL46N4ruLNBbWatvnTxEV/6RStwyxFf5NF4jhuPO5sYnI8biaoVozVkDh+xc1j4zIc17ZE48kYV0evza5eWrmM3NJUcGntnflLteqYruuQxJn75K1P4hbh2waOpDZCsg4bFTfn97wRAkWu2Q74m/9/TcN4EhdJDcamgNiNwY+4x84NnruFapjw3suFALs6AMi6lRo1QY+LjEDAyiSOexgjVfjn5VS8UPU8ODKh0nlRhBg+HnNXYjBR1XImeklcYXXWtOfidJfsZcvgPbBikQ7Qvxe+zDnhG+p6LbrpXsMri4RXFbnmv7HjhNH2+Rcx2wo2FEbal2e5+qCS/ezsHOHXrNk52X7C9+frc8mf34HfV9WeFZI4tYgldn6KkbhQ8CD0gZwiEoPf+ZzjSkldoK3ch0AQL3AN9RGNyTJODIlL7LODu6URAlYmcdGOKTwTt8CeTVbkLOrIY6oT1kri7JkScX+gn7z1b6fJ1cujPLvzSEPhbaQsc+kmYLfuV0deesoje+4CDlUDX/zaP1pyu8f/s0SOWCQOdHz9cvu+Ed/Q5+ZfyOxUma3il7cuadobjchFUh1JnLYDRRKnDbdErUphEhcBQugEwRNQ+KD+L3Q8zgzeCSSYXQpf8LR722KdGZK4RD3RuI8QBJDECYFVlVCtJE5e7SJHXn2FbP/0ZlJWWkza//AmeaPrtHNrxSfp2+43n3ROv/cI/9mK6mXkxd/8jrz51juuLUvvh2LjCfTKxSBxozNXD742+pudf3nnx8gta1aT117/V9L++zfI2oL1Dy3NqfyhUhCRxClFavE8JHHacEvUqvQicYlC9dp9kMQlD3vc2QAEkMQZAKJOEb0dV8nZo5ED92OJ9nxwgIyNu8hDD+7wTxseuUoefPh7ZHVW/b2np06tveXDq/eG/2xtwUd/ujSn6gGdKitfHoPEnXafOuzOHdny2KNfX5D3+a98kyyRq/bUF30Y+oEqG+u3FZKV9dglUBla789CEqcWscTORxKXGLyRxCUGZ9xFEAJI4gQBq1LsW7/uJyNn1VUZ4iTuvd4e8oPm+c43zr6L5OHm/eQ7//2rT3zjq//jYs2KZXuDPxuB69avfO275GNFHz9QklUKNSITNGKQuHOeM//cz969/xePfcuvzMTkFHnwa99TReJKKmzkr7+0JEHGpNY2SOLMfZ5I4hJzPkjiEoMz7iIIASRxgoDVIdZzaZrMTcvEO+0jYxejN3WZKnSTnzz+K9J020cWrlOLSgvInod2kq633ml54sfP7A7+7I2ud4DkXXh7U8WndkODqyYd6qlaChzuJojHu8YICZIthmcuv/m66//9evXKGwpX33Q9OX32T+T0uXfdqwtvuf2DWTfcF75R0RJ75fU3Zy54ETNyJPDAQXJDVvx+rKqUTpPJSOLMfdBI4hJzPn4S1+MabE/MdrgLImAsAtDr1AFtsv5DuFQo6NtX7MgfMXK34Vn3de658UUZBowydmNO1VtG7gP142p9RF6oU5EpOcYrMosvGLmHWWR1/L5ryfH2k1Vcn+Kigukt993prFy2xE+aDj7x7K/eOPlvn/XryoiTeUnzy22PO82iO9fjLz/xt/WESc2QfV8EjwLUlZKbX3zmCQhyXjywG4Pxp4YkznhMjZSIJM5INKPL8pO4xGyFuyACiAAikJ4IsNEzu4nM4vZBTE90tFl9Ynry7gnm83+pyqW2wXVZOS9rk5T8VZCbXTnH2KLAyBxKXHm8FZRFx5m5mfpB7yzP0PSPO7LzWy1qiqnVpqWrm/8/x26QbhYzTkoAAAAASUVORK5CYII="/>
          </pattern>
        </defs>
        <rect id="Grupo_641" data-name="Grupo 641" width="360" height="132" fill="url(#pattern)"/>
      </svg>
    </div>
  </section>
</main>
<?php
get_footer();
