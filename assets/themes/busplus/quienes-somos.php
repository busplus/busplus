<?php
/**
 * Template Name: Quienes somos
 */

get_header();
?>
    <section id="primary" class="content-area inner-section">
        <div id="main" class="site-main" role="main">
            <section class="hero mt-3 pt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                          <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section-block arcos-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-1"></div>
                        <div class="col-xs-12 col-md-10 text-left">
                            <p><?php the_field( 'texto_superior' ); ?></p>
                        </div>
                        <div class="col-xs-12 col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-1"></div>
                        <div class="col-xs-12 col-md-10">
                            <div class="data-wrapper my-2">
                                <?php if ( have_rows( 'datos' ) ) : ?>
                                        <?php while ( have_rows( 'datos' ) ) : the_row(); ?>
                                        <div>
                                            <h2>+ <?php the_sub_field( 'numero' ); ?></h2>
                                            <span><?php the_sub_field( 'texto' ); ?></span>
                                        </div>
                                        <?php endwhile; ?>
                                    <?php else : ?>
                                    <?php // no rows found ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-1"></div>
                        <div class="col-xs-12 col-md-10 text-left">
                            <p><?php the_field( 'texto_inferior' ); ?></p>
                        </div>
                        <div class="col-xs-12 col-md-1"></div>
                    </div>
                </div>
                <svg class="svg-arcos" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 1555 563" style="enable-background:new 0 0 1555 563;" xml:space="preserve">
                    <style type="text/css">
                    .st0-b{fill-rule:evenodd;clip-rule:evenodd;fill:#7C51A1;}
                    .st1-b{fill-rule:evenodd;clip-rule:evenodd;fill:#DC272D;}
                    .st2-b{fill-rule:evenodd;clip-rule:evenodd;fill:#C0BC00;}
                    </style>
                    <g>
                    <path class="st0-b" d="M194,259.8l34.6-19.8c0,0,107.8,156.9,254.1,143.8c-1.3,22.4,1.3,46.2,1.3,46.2
                    C378.9,432.6,253.9,373.2,194,259.8z"/>
                    <path class="st1-b" d="M0,210.5L42.6,235c0,0,132.6-194.1,312.8-177.8C353.7,29.5,357,0.1,357,0.1C227.6-3.2,73.7,70.3,0,210.5z"/>
                    <path class="st2-b" d="M1198,538.5l42.6,24.5c0,0,132.6-194.1,312.8-177.8c-1.6-27.7,1.6-57.1,1.6-57.1
                    C1425.6,324.8,1271.7,398.3,1198,538.5z"/>
                    </g>
                </svg>
            </section>
            <section>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" viewBox="0 0 1492 399">
                    <defs>
                        <clipPath id="clip-path">
                        <rect id="Rectangle_125" data-name="Rectangle 125" width="577" height="399" transform="translate(480 1044)" fill="#fff" stroke="#707070" stroke-width="1"/>
                        </clipPath>
                    </defs>
                    <g id="Group_672" data-name="Group 672" transform="translate(0 -1045)">
                        <path id="Path" d="M555.522,1238.957c-12.6,10.874-59.488,58.437-59.148,58.823l.05.041a.154.154,0,0,1-.05-.041l-55.411-45.239s-43.5-40.325-85.465-15.395c-8.177,4.858-57.721,28.418-57.721,28.418l-22.289-10.306s-33.851-25.248-84.556,21.734c-59.458,48.726-118.44,41.015-143.654,22.64S0,1255.258,0,1255.258v80.6H1017.54L909.2,1215.412s-27.615-39.677-57.28-8.151l-85.729,74.494-86.11-49.137S620.707,1182.71,555.522,1238.957Z" transform="translate(0 107.702)" fill="#ffeed6"/>
                        <g id="Group_582" data-name="Group 582">
                        <g id="Group_252" data-name="Group 252" transform="translate(-252 158.233)">
                            <rect id="Rectangle" width="12.22" height="21.48" transform="translate(448.289 1264.02)" fill="#b3e3c8"/>
                            <path id="Path-2" data-name="Path" d="M453.818,1162.746,477.091,1268.4H431.709Z" fill="#b3e3c8"/>
                        </g>
                        <g id="Group_253" data-name="Group 253" transform="translate(-252 158.233)">
                            <rect id="Rectangle-2" data-name="Rectangle" width="10.03" height="17.66" transform="translate(508.15 1267.42)" fill="#b3e3c8"/>
                            <path id="Path-3" data-name="Path" d="M512.686,1184.806l19.1,86.862H494.546Z" fill="#b3e3c8"/>
                        </g>
                        </g>
                        <g id="Group_581" data-name="Group 581">
                        <path id="Path-4" data-name="Path" d="M173.788,1211.591c4.74,4.09,22.377,21.981,22.249,22.126l-.019.015a.057.057,0,0,0,.019-.015L216.88,1216.7s16.362-15.168,32.148-5.791c3.076,1.827,21.712,10.689,21.712,10.689l8.384-3.876s12.733-9.5,31.806,8.175c22.365,18.328,44.551,15.428,54.035,8.516a119.51,119.51,0,0,0,17.784-16.691v30.317H0l40.752-45.3s10.388-14.925,21.546-3.066l32.247,28.021,32.391-18.483S149.269,1190.433,173.788,1211.591Z" transform="translate(1109.252 194.968)" fill="#e0f1e9"/>
                        <rect id="Rectangle-3" data-name="Rectangle" width="82.596" height="238.242" transform="translate(1140.436 1205.757)" fill="#e0f1e9"/>
                        <g id="Group_254" data-name="Group 254" transform="translate(1140.023 1205.757)">
                            <rect id="Rectangle-4" data-name="Rectangle" width="40.548" height="238.242" transform="translate(0 0)" fill="#e0f1e9" opacity="0.502"/>
                        </g>
                        <g id="Group_255" data-name="Group 255" transform="translate(1017.54 1187.222)">
                            <path id="Path-5" data-name="Path" d="M995.964,1152.313,986.1,1130l-9.533,22.341-3.556,6.129-.96,42.612-3.454,112.112L967,1383.939h39.575l-1.6-70.746-3.474-112.112-.931-41.114Z" transform="translate(-967 -1130)" fill="#deefe7"/>
                        </g>
                        <g id="Group_256" data-name="Group 256" transform="translate(1065.342 1316.638)">
                            <path id="Path-6" data-name="Path" d="M1156.691,1211.48s-1.5-3-3-3-157.7,101.89-157.7,101.89h235.8v-7.492H1167.2s-60.076.749-96.121-11.987C1110.884,1256.431,1156.691,1211.48,1156.691,1211.48Z" transform="translate(-995.989 -1208.483)" fill="#fff"/>
                        </g>
                        <path id="Path-7" data-name="Path" d="M987.23,1278.481a23.976,23.976,0,0,0-23.843,26.971h127.42a21.047,21.047,0,0,0-30.023-17.441,36.819,36.819,0,0,0-71.82-9.47Q988.1,1278.48,987.23,1278.481Z" transform="translate(48.074 137.05)" fill="#b3e3c8"/>
                        <path id="Composite_Path" data-name="Composite Path" d="M1129.849,1285.448v28.578h6.007l0-28.784a21.952,21.952,0,0,1-3.718.315h-.079A22.023,22.023,0,0,1,1129.849,1285.448Zm-19.525-21.617a21.736,21.736,0,0,0,19.525,21.617,22.023,22.023,0,0,0,2.213.109h.079a21.952,21.952,0,0,0,3.718-.315,21.728,21.728,0,0,0-3.718-43.138h-.079A21.732,21.732,0,0,0,1110.324,1263.831Z" transform="translate(143.552 129.973)" fill="#b3e3c8"/>
                        <path id="Path-8" data-name="Path" d="M1258.2,1164.756s1.491,197.787-1.649,197.787H1149.916V1164.756Z" transform="translate(169.245 79.777)" fill="#b3e3c8"/>
                        </g>
                        <path id="Path-9" data-name="Path" d="M377.418,1204.762h141.5s-2.823-15.691-17.327-12.966c-6.136-5.637-7.6-10.838-18.771-8.644-3.628-7.81-1.96-11.346-17.327-18.729-12.779-5.515-25.142,7.349-24.546,11.526-3.869-4.559-15.961-1.317-17.327,7.2-7.183-1.361-17.816,1.967-22.4,8C389.754,1189.7,380.8,1188.5,377.418,1204.762Z" transform="translate(15.761 -49.07)" fill="#fcfcfd"/>
                        <path id="Path-10" data-name="Path" d="M377.418,1179.87h57.018s-1.137-6.323-6.982-5.225c-2.472-2.271-3.061-4.367-7.564-3.483-1.462-3.147-.79-4.572-6.982-7.547-5.149-2.222-10.131,2.961-9.891,4.644-1.559-1.837-6.431-.531-6.982,2.9-2.894-.548-7.179.793-9.027,3.224C382.389,1173.8,378.78,1173.318,377.418,1179.87Z" transform="translate(-116.254 63.63)" fill="#fcfcfd"/>
                        <path id="Path-11" data-name="Path" d="M925.618,1173.115h57.018s-1.137-6.323-6.982-5.225c-2.472-2.271-3.06-4.367-7.564-3.483-1.462-3.147-.79-4.572-6.982-7.547-5.149-2.222-10.131,2.961-9.891,4.644-1.559-1.837-6.431-.531-6.982,2.9-2.894-.548-7.179.793-9.027,3.224C930.589,1167.044,926.981,1166.563,925.618,1173.115Z" transform="translate(156.246 -58)" fill="#fcfcfd"/>
                        <path id="Path-12" data-name="Path" d="M865,1198H778s1.735-9.785,10.653-8.086c3.772-3.515,4.67-6.759,11.541-5.391,2.231-4.87,1.2-7.076,10.653-11.68,7.857-3.439,15.458,4.583,15.092,7.188,2.379-2.843,9.813-.821,10.653,4.492,4.416-.849,10.954,1.227,13.774,4.989C857.416,1188.6,862.921,1187.86,865,1198Z" transform="translate(156.246 -58)" fill="#fcfcfd"/>
                        <g id="Mask_Group_2" data-name="Mask Group 2" transform="translate(0 1)" clip-path="url(#clip-path)">
                        <g id="Group_283" data-name="Group 283" transform="translate(491.162 -803.096)">
                            <g id="Group_283-2" data-name="Group 283" transform="translate(306.073 1940.25)">
                            <path id="Path_698" data-name="Path 698" d="M226.963,1961.316c0,17.031-7.873,29.9-18.668,29.9s-18.67-12.871-18.67-29.9,7.616-26.631,18.411-26.631S226.963,1944.286,226.963,1961.316Z" transform="translate(-151.46 -1934.685)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_699" data-name="Path 699" d="M188.63,2020s-13.475,15.718-10.481,40.423,8.235,80.845,8.235,80.845l3.742,99.558,23.954-1.5,3.744-84.589,11.977-68.118,32.189,79.347s15.719,74.108,16.466,74.108,22.458-2.246,22.458-2.246l-10.479-87.583s-16.469-127.255-26.2-129.5S188.63,2020,188.63,2020Z" transform="translate(-165.903 -1831.823)" fill="#7e6a70"/>
                            <path id="Path_700" data-name="Path 700" d="M190.936,1955.982s6.737,18.714,1.5,29.194,8.235,22.456,8.235,22.456,35.181-17.964,30.689-26.948-11.977-20.96-10.479-28.446Z" transform="translate(-149.871 -1913.4)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_701" data-name="Path 701" d="M194.82,1971.187s24.034,20,41.96-8.339l18.672,10.585,9.731,113.782s-54.645,31.439-80.845,4.492l3.744-105.547Z" transform="translate(-157.87 -1900.535)" fill="#38c172" style="isolation: isolate"/>
                            <path id="Path_702" data-name="Path 702" d="M213.034,1961.553l-33.748,22.432,11.977,55.394-15.72,86.085,25.451-38.177Z" transform="translate(-168.537 -1902.104)" fill="#4a5568" style="isolation: isolate"/>
                            <path id="Path_703" data-name="Path 703" d="M210.96,1958.731l8.289,5.47,33.685,11.977L247.695,2069l14.223,59.887-20.212,10.479s-3.742-28.446-26.948-42.669S205.666,1962.991,210.96,1958.731Z" transform="translate(-136.637 -1905.527)" fill="#4a5568" style="isolation: isolate"/>
                            <path id="Path_704" data-name="Path 704" d="M185.541,2024.469l9.731,6.737s-12.351,13.1-14.6,10.855a6.134,6.134,0,0,1-1.872-4.865l-2.246-5.242Z" transform="translate(-167.306 -1825.812)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_705" data-name="Path 705" d="M215.955,2023.81l-3.308,5.2s-9.731,4.491-7.485,7.485,20.958,9.731,24.7,2.994l3.742-6.737Z" transform="translate(-133.029 -1826.609)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_706" data-name="Path 706" d="M187.618,1971.354l-5.242.748-8.607,55.77s-3.37,10.1.374,34.059,7.45,39.553,7.45,39.553l16.5-11.855-7.485-19.465,9.731-71.112Z" transform="translate(-172.377 -1890.221)" fill="#4a5568" style="isolation: isolate"/>
                            <path id="Path_707" data-name="Path 707" d="M232.652,1965.94l11.977,1.5s13.475,14.223,15.721,29.2,1.5,57.639,1.5,57.639l-34.433,54.645L208.7,2095.442l26.2-46.41-12.725-55.4Z" transform="translate(-128.332 -1896.784)" fill="#4a5568" style="isolation: isolate"/>
                            </g>
                            <g id="Group_285" data-name="Group 285" transform="translate(62.124 1951.184)">
                            <g id="Group_284" data-name="Group 284">
                                <path id="Path_708" data-name="Path 708" d="M74.729,2065.913a8.243,8.243,0,0,0,2.81-11.312,8.1,8.1,0,0,0-.856-1.175L91.2,2027.99l-15.185.98-11.455,24a8.287,8.287,0,0,0,10.174,12.938Z" transform="translate(-62.124 -1832.474)" fill="#dad3d7" style="isolation: isolate"/>
                                <path id="Path_709" data-name="Path 709" d="M126.439,2144.741H116.391l-4.78-38.755H126.44Z" transform="translate(-2.114 -1737.894)" fill="#ffb6b6"/>
                                <path id="Path_710" data-name="Path 710" d="M137.226,2135.038h-32.4v-.409a12.61,12.61,0,0,1,12.61-12.61h19.788Z" transform="translate(-10.339 -1718.453)" fill="#2f2e41"/>
                                <path id="Path_711" data-name="Path 711" d="M95.326,2144.741H85.278l-4.78-38.755H95.328Z" transform="translate(-39.842 -1737.894)" fill="#ffb6b6"/>
                                <path id="Path_712" data-name="Path 712" d="M106.113,2135.038h-32.4v-.409a12.61,12.61,0,0,1,12.61-12.61h19.788Z" transform="translate(-48.067 -1718.453)" fill="#2f2e41"/>
                                <path id="Path_713" data-name="Path 713" d="M75.907,2028.484s-2.764,39.356,2.679,59.175S85,2227.343,85,2227.343h18.243l15.814-163.157,36.453,163.732,17.378-1.195-9.53-192.937Z" transform="translate(-46.261 -1831.875)" fill="#7e6a70"/>
                                <path id="Path_714" data-name="Path 714" d="M157.457,1974.1l-13.948-10.178-23.37,5.089-6.326,15.032-10.9,5.406a24.7,24.7,0,0,0-13.479,18.6L73.859,2115.594,165,2112.071l11.034-126.653A9.929,9.929,0,0,0,167,1974.664c-.1-.009-.19-.016-.285-.022Z" transform="translate(-47.894 -1910.161)" fill="#cd4149" style="isolation: isolate"/>
                                <path id="Path_715" data-name="Path 715" d="M109.089,1979.259l-8.417,11.347L64.008,2102.429l17.186,6.881,27.518-52.107Z" transform="translate(-59.839 -1891.567)" fill="#cd4149" style="isolation: isolate"/>
                                <path id="Path_716" data-name="Path 716" d="M128.822,1982.17l-5.679,56.617-20.057,39.531Z" transform="translate(-12.454 -1888.038)" opacity="0.1" style="isolation: isolate"/>
                                <path id="Path_717" data-name="Path 717" d="M99.877,2054.018a8.243,8.243,0,0,1,7.645-8.8,8.452,8.452,0,0,1,1.454.027l12.488-26.49,7.919,12.995-13.072,23.164a8.287,8.287,0,0,1-16.433-.9Z" transform="translate(-16.368 -1843.671)" fill="#dad3d7" style="isolation: isolate"/>
                                <path id="Path_718" data-name="Path 718" d="M129.915,1975.817l5.142-6.707s12.532-2.551,14.661,14.659l-1.341,76.517-27.459,43.219-17.834-4.553L126.4,2047.61Z" transform="translate(-12.454 -1904.036)" fill="#cd4149" style="isolation: isolate"/>
                                <path id="Path_719" data-name="Path 719" d="M138.052,1963.57c-4.231,12.528-10.72,23.943-23.943,23.943s-23.2-10.74-23.943-23.943c-.954-16.869,10.415-24.014,23.943-23.943C130.09,1939.709,143.074,1948.7,138.052,1963.57Z" transform="translate(-28.188 -1939.627)" fill="#95888c" style="isolation: isolate"/>
                                <rect id="Rectangle_136" data-name="Rectangle 136" width="39.164" height="57.075" transform="translate(66.166 20.759)" fill="#95888c" style="isolation: isolate"/>
                                <path id="Path_720" data-name="Path 720" d="M103.442,1963.156s4.81,18.56-6.629,21.047c-5.72,1.243-5.222,4.6-3.295,7.647a12.175,12.175,0,0,0,11.008,5.465l3.213-.179a82.268,82.268,0,0,0,17.619-2.921l3.784-1.06,3.978-6.963s-16.035-10.6-11.559-23.036Z" transform="translate(-25.655 -1911.094)" fill="#dad3d7" style="isolation: isolate"/>
                                <path id="Path_721" data-name="Path 721" d="M110.131,1948.4s-.2-1.1-7.917,1.795-12.344,24.255-8.485,36.8c1.555,5.056.29,8.7-1.775,11.249-7.749,9.57-4.7,23.941,6.587,28.861q.478.209.976.412l4.824,2.894a22.063,22.063,0,0,1-3.071-18.442,53.067,53.067,0,0,0,.958-27.167c-.354-1.516-.755-3.078-1.21-4.673-3.859-13.508-1.982-11.948,11.526-25.454,0,0,7.718,5.789,14.473,8.683s12.541,5.79,7.718,15.437-16.4,18.332-8.685,30.873,8.685,7.72,7.72,12.544h0l-.965,4.823,5.788-5.788-3.859,7.718s12.541,0,16.4-9.647l1.929,1.929s7.718-13.508-6.755-26.049c0,0-4.824-2.894,1.932-12.544s1.567-30.39-7.118-34.25S110.131,1948.4,110.131,1948.4Z" transform="translate(-31.162 -1931.069)" fill="#95888c" style="isolation: isolate"/>
                                <path id="Path_722" data-name="Path 722" d="M114.665,1968.825a13.115,13.115,0,0,1,.735-5.669H97.28a37.221,37.221,0,0,1,.894,5.689,11.748,11.748,0,0,0,16.491-.02Z" transform="translate(-19.493 -1911.094)" fill="#c0b9bc" style="isolation: isolate"/>
                                <path id="Path_723" data-name="Path 723" d="M126.384,1967.067c0,14.719-6.8,25.843-16.135,25.843s-16.137-11.125-16.137-25.843,6.583-23.018,15.913-23.018S126.384,1952.349,126.384,1967.067Z" transform="translate(-23.333 -1934.264)" fill="#dad3d7" style="isolation: isolate"/>
                            </g>
                            <path id="Path_724" data-name="Path 724" d="M123.892,1960.955a12.635,12.635,0,0,1-5.543,2.253c-5.985.878-12.2-2.573-17.989-.808a2.833,2.833,0,0,1,3.133-2.5c.093.011.186.027.277.044-2.8-2.031-6.224-4.168-9.492-3.027a6.483,6.483,0,0,1,4.934-4.87c-2.392-1.932-3.8-5.554-2.142-8.145a6.115,6.115,0,0,1,5.33-2.478,15.75,15.75,0,0,1,5.881,1.651C113.751,1945.5,133.979,1953.662,123.892,1960.955Z" transform="translate(-23.133 -1937.445)" fill="#95888c" style="isolation: isolate"/>
                            </g>
                            <g id="Group_286" data-name="Group 286" transform="translate(174.148 1930.096)">
                            <path id="Path_725" data-name="Path 725" d="M145.821,1931.636s-18.115,6.176-15.227,32.313,13.431,52.145,13.431,52.145H180.69s11.032-47.123,1.945-71.36" transform="translate(-91.497 -1928.229)" fill="#95888c"/>
                            <path id="Path_726" data-name="Path 726" d="M128.968,2022.089l12.01,58.044,65.741.968-16.435-60.946Z" transform="translate(-93.091 -1820.89)" fill="#3f3d56"/>
                            <path id="Path_727" data-name="Path 727" d="M128.968,2022.089l12.01,58.044,65.741.968-16.435-60.946Z" transform="translate(-93.091 -1820.89)" fill="#8e7c81"/>
                            <path id="Path_728" data-name="Path 728" d="M146.932,2000.584s-25.917,36.03-24.653,50.57,17.7,251.585,17.7,251.585l38.56,1.9-11.38-170.674,9.483-46.144,23.388,214.922,47.41,1.263-42.352-278.766-8.218-24.653Z" transform="translate(-101.257 -1844.62)" fill="#764d96" style="isolation: isolate"/>
                            <path id="Path_729" data-name="Path 729" d="M152.567,1950.952s3.093,15-7.654,25.113-15.8,37.294-15.8,37.294l60.684-4.425-5.056-34.134s-17-8.045-13.84-22.584Z" transform="translate(-92.917 -1904.806)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_730" data-name="Path 730" d="M134.211,1969.158s3.792,11.38,20.228,10.114a56.368,56.368,0,0,0,27.813-10.114v73.96l-50.57-1.9V1996.34Z" transform="translate(-89.8 -1882.728)" fill="#00b0ac" style="isolation: isolate"/>
                            <path id="Path_731" data-name="Path 731" d="M160.22,1959.576l-4.832,3.5-29.076,13.906,10.114,52.466,6.952,11.38v13.906l-17.066,24.02a260.709,260.709,0,0,0-6.321,51.833c-.633,29.078,6.954,21.493,10.747,20.86s12.01-2.529,12.01-2.529l-13.276-61.947,22.124-32.238,3.792-12.643s-1.263-29.709-6.319-41.722C149.068,2000.373,158.73,1968.714,160.22,1959.576Z" transform="translate(-104.024 -1894.348)" fill="#7e6a70"/>
                            <path id="Path_732" data-name="Path 732" d="M155.578,1958.38l11.942,8.045,22.757,6.955L179.529,2032.8l-6.322,12.643,1.266,12.643s23.686,25.406,27.18,75.854c1.836,26.5-1.956,25.233-1.956,25.233l-46.718-.58L145.4,2053.658l14.539-79.015Z" transform="translate(-73.171 -1895.798)" fill="#7e6a70"/>
                            <path id="Path_733" data-name="Path 733" d="M182.629,2025.516s6.322,27.182-6.321,30.341-8.218-30.341-8.218-30.341Z" transform="translate(-46.56 -1814.387)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_734" data-name="Path 734" d="M126.656,2012.688s24.341,13.65,18.157,25.12-27.954-14.38-27.954-14.38Z" transform="translate(-107.776 -1829.942)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_735" data-name="Path 735" d="M162.088,1966.423l9.481-1.263s5.689,7.585,6.322,13.273,10.747,101.773,10.747,101.773l10.114,18.331s-.633,3.16-5.691,3.16H188l-1.9,7.585s-3.792-14.537-18.332-6.952l-5.689-1.9,3.792-6.955s-3.792-33.5-3.792-49.3-1.266-32.871-1.266-32.871Z" transform="translate(-54.464 -1887.578)" fill="#7e6a70"/>
                            <path id="Path_736" data-name="Path 736" d="M141.993,1967.158l-6.952.633-15.8,61.317s-6.321,40.456-5.058,42.352,0,5.056-1.263,6.322,5.056,18.962,11.377,18.331,8.851-2.529,8.851-2.529-5.689-16.436,8.848-22.124l3.795-12.643-3.795-20.861,5.058-29.076Z" transform="translate(-112.753 -1885.154)" fill="#7e6a70"/>
                            <path id="Path_737" data-name="Path 737" d="M140.432,1950.952a31.54,31.54,0,0,1-1.314,15.358,14.181,14.181,0,0,0,10.019,4.393c4.569,0,8.6-2.335,11.74-6.3a16.268,16.268,0,0,1-2.113-12.187Z" transform="translate(-80.783 -1904.806)" fill="#c0b9bc" style="isolation: isolate"/>
                            <path id="Path_738" data-name="Path 738" d="M172.409,1959.09c0,16.951-7.835,29.76-18.579,29.76s-18.582-12.809-18.582-29.76,7.58-26.505,18.325-26.505S172.409,1942.142,172.409,1959.09Z" transform="translate(-85.475 -1927.078)" fill="#dad3d7" style="isolation: isolate"/>
                            <path id="Path_739" data-name="Path 739" d="M133.29,1958.258s34.464,3.179,34.464-11.656c0,0-1.405,11.656,12.207,13.776l-.974-13.776-3.337-5.457a26.084,26.084,0,0,0-33.477-7.643c-.467.283-.9.569-1.286.856l-.792.586-5.348,16.066Z" transform="translate(-87.85 -1930.096)" fill="#95888c" style="isolation: isolate"/>
                            </g>
                            <g id="Group_288" data-name="Group 288" transform="translate(129.358 1979.126)">
                            <g id="Group_287" data-name="Group 287">
                                <path id="Path_740" data-name="Path 740" d="M92.525,1955.494a1.286,1.286,0,0,0,.392,1.053,1.178,1.178,0,0,1,.146.181,1.521,1.521,0,0,1,.179.529c.746,4.516.914,5.131,2.8,6.127a11.552,11.552,0,0,0,4.629.8,11.852,11.852,0,0,0,5.3-1.1c2.545-1.327,3.492-5.907,3.536-6.137,0-.024.077-.571,1.239-.571h.073c1.1,0,1.177.547,1.186.606.038.195.987,4.775,3.529,6.1a11.824,11.824,0,0,0,5.3,1.1,11.544,11.544,0,0,0,4.629-.8c1.892-1,2.058-1.613,2.8-6.127a1.52,1.52,0,0,1,.179-.529,1.179,1.179,0,0,1,.146-.181,1.279,1.279,0,0,0,.392-1.053c0-.1,0-.288.007-.489.007-.308.013-.653,0-.839a1.5,1.5,0,0,0-1.27-1.352,33.839,33.839,0,0,0-9.016-.452l-.173.011a11.97,11.97,0,0,0-4.436.812c-.305.137-.622.292-.9.436a8.189,8.189,0,0,1-1.6.617c-.155.033-.312.069-.451.1a2.478,2.478,0,0,1-.788,0l-.451-.1a8.189,8.189,0,0,1-1.6-.617c-.279-.144-.593-.3-.9-.436a11.961,11.961,0,0,0-4.436-.812l-.173-.011a33.809,33.809,0,0,0-9,.449,1.5,1.5,0,0,0-1.281,1.354c-.011.186,0,.533,0,.839C92.523,1955.209,92.527,1955.392,92.525,1955.494Zm35.708-1.122v.675l-.843-.336Zm-14.592.969c.126-.317.929-1.37,5.9-1.66.872-.051,1.613-.075,2.261-.075,2.932,0,3.865.527,4.421,1.378a5.055,5.055,0,0,1,.562,2.246c0,1.474-.29,4.091-2.334,5.083a9.171,9.171,0,0,1-3.753.724c-2.715,0-4.883-.923-5.8-2.469C112.963,1957.322,113.616,1955.395,113.64,1955.342Zm-18.36-.358c.558-.85,1.489-1.376,4.421-1.376.65,0,1.39.024,2.261.075,4.974.29,5.775,1.343,5.919,1.7.007.015.659,1.945-1.27,5.191-.92,1.545-3.089,2.467-5.8,2.467a9.154,9.154,0,0,1-3.755-.724c-2.042-.991-2.337-3.609-2.332-5.083A5.032,5.032,0,0,1,95.28,1954.983Zm-2.007-.611.841.336-.841.338Z" transform="translate(-92.51 -1952.255)" fill="#4a5568"/>
                            </g>
                            </g>
                            <path id="Path_741" data-name="Path 741" d="M78.918,2071.367l12.09-85.5S87.585,2063.647,78.918,2071.367Z" transform="translate(20.365 67.635)" opacity="0.1" style="isolation: isolate"/>
                            <g id="Group_289" data-name="Group 289" transform="translate(338.429 1932.412)">
                            <path id="Path_742" data-name="Path 742" d="M228.8,1961.722s-.87-.4-1.378.765c-.4-.885.509-2.536.509-2.536s-.474,0-1.525,1.449a8.067,8.067,0,0,1,.907-3.3l-.327-2.213-1.633,1.449.582-3.461a11.177,11.177,0,0,1-6.93-.283c-3.412-1.407-9.255-1.972-11.939-.6s-6.023,3.259-7.33,12.388c-2.25-1.569,1.56-9.694,1.56-9.694s-1.812.04-3.12,3.9c-1.2-3.339,3.737-6.837,3.737-6.837s-6.751,1.932-6.388,3.578c.146,1.128.33,18.907-.724,18.748s-1.56-4.669-2-7.4a40.484,40.484,0,0,0-3.264-8.488c-.92-1.834-2.078-2.845-2.106-4.142-.073-3.381,3.593-8.005,3.593-8.005s-1.671-.122-4.029,2.854a13.08,13.08,0,0,1,9.435-8.97c1.086-.164.412-1.925,1.175-2.889,1.56-2.414,6.237-2.206,11.45-4.395,2.863-1.2,5.4-3,5.952-2.369a9.365,9.365,0,0,0-3.593,2.573s4.173-2.454,4.839-1c-1.9.277-4.111,1.243-3.206,1.323,9.581,2.252,13.716,6.4,13.716,6.4s4.352,1.288,2.213,4.7c1.053-.2,4.246,2.857,4.463,5.027s-1.27,12.754-1.27,12.754-.872,9.818-2.649,11.185C228.436,1974.115,228.8,1961.722,228.8,1961.722Z" transform="translate(-187 -1931.143)" fill="#7e6a70"/>
                            </g>
                            <g id="Group_292" data-name="Group 292" transform="translate(345.066 1975.378)">
                            <g id="Group_291" data-name="Group 291">
                                <g id="Group_290" data-name="Group 290">
                                <path id="Path_743" data-name="Path 743" d="M225.389,1953.586c-.007-.739-.016-1.493-.016-2.254a.769.769,0,1,0-1.538,0c0,11.7-6.4,13.2-11.977,13.2-.389,0-.77-.007-1.139-.02a3.373,3.373,0,0,0-3.031-3.182c-1.5,0-2.753,1.35-3.018,3.182-.383.013-.774.02-1.173.02-5.656,0-11.5-1.571-11.962-13.229a.781.781,0,0,0-.8-.739.769.769,0,0,0-.739.8v0c.049,1.259.062,2.492.073,3.682.049,5.231.091,9.747,3.295,12.269,3.883,3.053,9.682,6.317,14.32,6.317s10.435-3.264,14.318-6.317c3.485-2.744,3.438-7.835,3.383-13.73Z" transform="translate(-190 -1950.561)" fill="#95898c" style="isolation: isolate"/>
                                <path id="Path_744" data-name="Path 744" d="M204.8,1956.563c2.129-.223,4.189,1.042,5.821,2.04a6.326,6.326,0,0,0,2.823,1.235,1.459,1.459,0,0,0,1.051-.42,1.174,1.174,0,0,0,.3-1.131c-.493-2.035-7.169-5.5-9.078-6.1a2.413,2.413,0,0,0-.352-.122,3.085,3.085,0,0,0-.639-.115,2.066,2.066,0,0,0-.772.15,2.308,2.308,0,0,0-.825-.148,3.721,3.721,0,0,0-.639.129,1.02,1.02,0,0,0-.19.064c-2.018.641-8.691,4.1-9.185,6.137a1.173,1.173,0,0,0,.3,1.131,1.442,1.442,0,0,0,1.049.423h0a6.32,6.32,0,0,0,2.821-1.235c1.509-.921,3.388-2.067,5.337-2.067.161,0,.323.009.564.031" transform="translate(-186.267 -1948.875)" fill="#95898c" style="isolation: isolate"/>
                                </g>
                            </g>
                            </g>
                        </g>
                        </g>
                    </g>
                    </svg>
            </section>
        </div><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();
