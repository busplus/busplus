<?php get_header(); ?>
<section id="primary" class="content-area inicio inner-section">
	<div id="main" class="site-main" role="main">
		<section class="mt-3 pt-5">
			<div class="container">
				<div class="row">
					<div class="col-2"></div>
					<div class="col-8 text-center">
						<h1 class="px-5"><?php the_title(); ?></h1>
						<div class="etica">
							<h2>Pasajes desde la <strong><?php echo get_the_title(); ?></strong> a todo el país.</h2>
						</div>
					</div>
					<div class="col-2"></div>
				</div>
			</div>
		</section>
		<section id="section1" class="mt-3 pt-5">
			<div class="container text-center">
				<h3>Reservá tu pasaje ahora:</h3>
				<div class="tab">
					<button class="tablinks" onclick="openCity(event, 'Bus')" id="defaultOpen">Bus</button>
					<button class="tablinks" onclick="openCity(event, 'Hoteles')">Hoteles</button>
					<button class="tablinks t-last" onclick="openCity(event, 'Actividades')">Actividades</button>
					<!-- <button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Paquetes</button>
					<button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Nieve</button>
					<button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Asistencia</button>
					<button class="tablinks last-tablink" onclick="openCity(event, 'Actividades')">Traslados</button> -->
					<button id="entercode" class="tablinks last-tablink-2">Agregar código de descuento</button>
				</div>
				<div class="cont-tabcont">
					<div id="Bus" class="tabcontent block">
						<?php require 'assets/themes/busplus/inc/buscador.php';?>
					</div>
					<div id="Hoteles" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador-hoteles.php';?>
					</div>
					<div id="Actividades" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador-actividades.php';?>
					</div>
					<div id="Paquetes" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador.php';?>
					</div>
					<div id="Nieve" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador.php';?>
					</div>
					<div id="Asistencia" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador.php';?>
					</div>
					<div id="Traslados" class="tabcontent">
						<?php require 'assets/themes/busplus/inc/buscador.php';?>
					</div>
				</div>
			</div>
		</section>
		<section class="section-block">
			<style media="screen">
	section.inner-section	.hero	#map {
				margin: 0 auto 2rem auto;
			}
	section.inner-section		.hero		h2 {
					font-family: "montserratbold" !important;
    font-size: 1.5rem;
		margin: 0 auto 1rem auto;
				}
	section.inner-section	.hero			h3 {
					font-family: "montserratbold" !important;
    font-size: 1.5rem;
		margin: 0 auto 1rem auto;
				}
	section.inner-section	.hero			p {
		margin: 0 auto 1rem auto;
				}
			</style>
			<div class="hero container text-left">
				<div class="row">
					<div class="col-xs-12 col-md-1"></div>
					<div class="col-xs-12 col-md-10">
						<h2>Donde está la <?php echo get_the_title(); ?></h2>
						<?php if(get_field('direccion')) { ?>
						<p>Dirección: <?php the_field('direccion'); ?></p>
						<div id="map" style="width: 100%;min-height: 600px;"></div>

						<?php
						} ?>
						<?php if(get_field('como_llegar')) { ?>
						<h2>Como llegar a la <?php echo get_the_title(); ?></h2>
						<p><?php the_field('como_llegar'); ?></p>
						<?php
						} ?>
						<?php if(get_field('estaciones')) { ?>
						<h3>Estaciones de tren cercanas a la estación</h3>
						<p><?php the_field('estaciones'); ?></p>
						<?php
						} ?>
						<?php if(get_field('telefono')) { ?>
						<p>Teléfono: <?php the_field('telefono'); ?></p>
						<?php
						} ?>
					</div>
					<div class="col-xs-12 col-md-1"></div>
				</div>
			</div>
		</section>
		<section>
			<svg xmlns="http://www.w3.org/2000/svg" width="100%" viewBox="0 0 1492 140.7">
				<g id="Group_329" data-name="Group 329" transform="translate(0 -885.072)">
					<g id="Group_189" data-name="Group 189" transform="translate(0 -571.311)">
						<path id="Path" d="M280.619,1221.734c7.653,6.6,36.132,35.493,35.925,35.728l-.03.025a.1.1,0,0,0,.03-.025l33.656-27.477s26.42-24.492,51.909-9.35c4.967,2.95,35.058,17.26,35.058,17.26l13.538-6.259s20.56-15.335,51.357,13.2c36.113,29.595,71.938,24.912,87.252,13.751s28.716-26.951,28.716-26.951v48.953H0l65.8-73.155s16.773-24.1,34.791-4.951l52.07,45.246,52.3-29.845S241.028,1187.571,280.619,1221.734Z" transform="translate(873.969 315.697)" fill="#ffeed6"/>
						<g id="Group_177" data-name="Group 177" transform="translate(1269.583 1502.083)">
							<path id="Composite_Path" data-name="Composite Path" d="M842.037,1299.306v37.716h-7.928l0-37.988a28.939,28.939,0,0,0,4.907.416h.105A29.017,29.017,0,0,0,842.037,1299.306Zm25.767-28.529a28.684,28.684,0,0,1-25.767,28.529,29.017,29.017,0,0,1-2.921.144h-.105a28.673,28.673,0,1,1,0-57.346h.105A28.68,28.68,0,0,1,867.8,1270.777Z" transform="translate(-810.325 -1242.104)" fill="#b3e3c8"/>
						</g>
						<g id="Group_178" data-name="Group 178" transform="translate(1140.617 1456.383)">
							<path id="Composite_Path-2" data-name="Composite Path" d="M903.825,1305.933v55.341H892.119l-.005-55.74a42.954,42.954,0,0,0,7.245.61h.155Q901.7,1306.145,903.825,1305.933Zm38.046-41.861a42.189,42.189,0,0,1-38.046,41.861q-2.127.213-4.312.211h-.155a42.954,42.954,0,0,1-7.245-.61A42.056,42.056,0,0,1,899.358,1222h.155A42.216,42.216,0,0,1,941.871,1264.072Z" transform="translate(-857 -1222)" fill="#c8e9d7"/>
						</g>
						<g id="Group_179" data-name="Group 179" transform="translate(1038.909 1502.083)">
							<path id="Composite_Path-3" data-name="Composite Path" d="M948.037,1299.306v37.716h-7.928l0-37.988a28.937,28.937,0,0,0,4.907.416h.1A29.015,29.015,0,0,0,948.037,1299.306Zm25.767-28.529a28.684,28.684,0,0,1-25.767,28.529,29.015,29.015,0,0,1-2.921.144h-.1a28.673,28.673,0,1,1,0-57.346h.1A28.68,28.68,0,0,1,973.8,1270.777Z" transform="translate(-916.325 -1242.104)" fill="#b3e3c8"/>
						</g>
					</g>
					<path id="Path-2" data-name="Path" d="M356.108,1223.211c-8.077,6.97-38.134,37.46-37.916,37.707l.032.026a.1.1,0,0,1-.032-.026l-35.52-29s-27.884-25.85-54.786-9.869c-5.242,3.114-37,18.217-37,18.217L176.6,1233.66s-21.7-16.185-54.2,13.932c-38.114,31.235-75.924,26.292-92.087,14.513S0,1233.66,0,1233.66v51.665H652.276l-69.448-77.208s-17.7-25.434-36.718-5.225l-54.955,47.753-55.2-31.5S397.893,1187.154,356.108,1223.211Z" transform="translate(0 -259.811)" fill="#e0f1e9"/>
					<g id="Group_202" data-name="Group 202" transform="translate(0 -259.811)">
						<rect id="Rectangle" width="12.22" height="21.48" transform="translate(35.199 1264.02)" fill="#b3e3c8"/>
						<path id="Path-3" data-name="Path" d="M40.727,1162.746,64,1268.4H18.618Z" fill="#b3e3c8"/>
					</g>
					<g id="Group_203" data-name="Group 203" transform="translate(0 -259.811)">
						<rect id="Rectangle-2" data-name="Rectangle" width="12.22" height="21.48" transform="translate(448.289 1264.02)" fill="#b3e3c8"/>
						<path id="Path-4" data-name="Path" d="M453.818,1162.746,477.091,1268.4H431.709Z" fill="#b3e3c8"/>
					</g>
					<g id="Group_204" data-name="Group 204" transform="translate(0 -259.811)">
						<rect id="Rectangle-3" data-name="Rectangle" width="10.03" height="17.66" transform="translate(139.859 1267.42)" fill="#b3e3c8"/>
						<path id="Path-5" data-name="Path" d="M144.4,1184.806l19.1,86.862H126.255Z" fill="#b3e3c8"/>
					</g>
					<g id="Group_205" data-name="Group 205" transform="translate(0 -259.811)">
						<rect id="Rectangle-4" data-name="Rectangle" width="10.03" height="17.66" transform="translate(508.15 1267.42)" fill="#b3e3c8"/>
						<path id="Path-6" data-name="Path" d="M512.686,1184.806l19.1,86.862H494.546Z" fill="#b3e3c8"/>
					</g>
					<g id="Group_326" data-name="Group 326" transform="translate(15.713 814.302)">
						<path id="Path_763" data-name="Path 763" d="M429.31,174.8l-3.113-6.537s-1.868.1,0,3.735,3.735,7.782,3.735,7.782.1,1.348,1.868.934Z" transform="translate(-67.785 -0.705)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
						<path id="Path_764" data-name="Path 764" d="M436.462,195.243l1.245,8.715-1.556-.934-1.245-6.225-1.556-6.847s2.178.311,2.178.622S436.462,195.243,436.462,195.243Z" transform="translate(-70.268 -7.451)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
						<path id="Path_765" data-name="Path 765" d="M213.056,166H349.386s6.748-.193,9.026,1.868a3.147,3.147,0,0,0-.934,1.867l5.291,10.272h.934l1.556,3.423-2.178-.622L365.26,193.4s-.426,1.771,1.868,3.112c.184,2.107,0,7.782,0,7.782s-1.177,4.124-3.424,4.358-19.92,0-19.92,0v-4.669a5.943,5.943,0,0,0-6.225-4.98,5.749,5.749,0,0,0-6.225,4.358v5.291h-69.41v-3.735c-.041-1.787-1.9-5.6-6.847-5.6s-6.225,4.98-6.225,4.98h-1.245a6.381,6.381,0,0,0-12.45,0v4.669H224.261l-13.384-1.245s-3.423-.859-3.423-6.225V175.653C208.257,168.685,207.778,166,213.056,166Z" transform="translate(0 0)" fill="#996dbf" fill-rule="evenodd"/>
						<circle id="Ellipse_151" data-name="Ellipse 151" cx="5.758" cy="5.758" r="5.758" transform="translate(235.678 199.953)" fill="#4a5568" style="isolation: isolate"/>
						<circle id="Ellipse_152" data-name="Ellipse 152" cx="5.758" cy="5.758" r="5.758" transform="translate(249.685 199.953)" fill="#4a5568" style="isolation: isolate"/>
						<circle id="Ellipse_153" data-name="Ellipse 153" cx="5.758" cy="5.758" r="5.758" transform="translate(331.545 199.953)" fill="#4a5568" style="isolation: isolate"/>
						<circle id="Ellipse_154" data-name="Ellipse 154" cx="3.424" cy="3.424" r="3.424" transform="translate(238.013 202.288)" fill="#f3f3f3"/>
						<circle id="Ellipse_155" data-name="Ellipse 155" cx="3.424" cy="3.424" r="3.424" transform="translate(252.019 202.288)" fill="#f3f3f3"/>
						<circle id="Ellipse_156" data-name="Ellipse 156" cx="3.424" cy="3.424" r="3.424" transform="translate(333.879 202.288)" fill="#f3f3f3"/>
						<path id="Path_766" data-name="Path 766" d="M364.584,207.005H297.976a4.269,4.269,0,0,1-4.358-1.868c-1.974-2.546-4.669-5.914-4.669-5.914s-2.368-3.424,2.178-3.424h65.986s1.712-.513,3.424,1.556,4.98,6.537,4.98,6.537S367.205,206.909,364.584,207.005Z" transform="translate(-25.159 -9.252)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
						<path id="Path_767" data-name="Path 767" d="M349.827,167.354H215.054s-2.232-.364-2.179,2.179v9.649H335.82s7.56.28,9.026,4.98c4.482,5.852,9.961,12.761,9.961,12.761s.852,2.179,5.6,2.179h3.113a2.385,2.385,0,0,0,2.8-2.49,55.085,55.085,0,0,0-.623-6.848l-2.178-8.715-4.358-9.96S358.766,167.354,349.827,167.354Z" transform="translate(-1.686 -0.416)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
						<path id="Path_768" data-name="Path 768" d="M373.8,195.17c.7-.078,2.256.389,2.256,1.478v20.543h-8.871V196.338c0-.467.856-1.323,1.168-1.168Zm-5.136-.7,5.447.078c1.089,0,2.646,1.011,2.646,2.256v20.7c-.078.156-.234.234-.311.311H366.8c-.156-.078-.234-.234-.311-.311V196.648A2.127,2.127,0,0,1,368.662,194.47Z" transform="translate(-49.469 -8.856)" fill="#996dbf" fill-rule="evenodd"/>
					</g>
				</g>
			</svg>
		</section>
	</div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
