<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WP_Bootstrap_Starter
*/

$result = new WebService();
$paradas = $result->call_curl("paradas?X-API-KEY=".$result->getKey());
?>
<?php if (!is_page_template('blank-page.php') && !is_page_template('blank-page-with-container.php')): ?>
</div><!-- .row -->
</div><!-- .container -->
</div><!-- #content -->
<section id="newsletter">
	<div class="container">
		<h2>Recibí ofertas y novedades en tu mail</h2>
		<div id="form">
			<div class="letter-content">
				<img class="img-letter" src="<?php bloginfo('template_url'); ?>/dist/img/icons/letter.png" alt="letter icon">
			</div>
			<form id="top_newsletter_form" action="/newsletter/agregar/go" method="post">
				<input type="email" name="email" placeholder="Tu email" class="top_newsletter_form input-form">
				<button class="btn-e" type="submit">SUSCRIBITE</button>
			</form>
		</div>
	</div>
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	viewBox="0 0 182.2 196" style="enable-background:new 0 0 182.2 196;" xml:space="preserve">
	<style type="text/css">
	.st0-a{fill-rule:evenodd;clip-rule:evenodd;fill:#525D6F;}
	.st1-a{fill-rule:evenodd;clip-rule:evenodd;fill:#4D586B;}
	.st2-a{fill-rule:evenodd;clip-rule:evenodd;fill:#7D8593;}
	.st3-a{fill-rule:evenodd;clip-rule:evenodd;fill:#737C8B;}
	.st4-a{fill-rule:evenodd;clip-rule:evenodd;fill:#6A7383;}
	.st5-a{fill:none;stroke:#7D8593;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}
	.st6-a{fill:none;stroke:#7D8593;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:3.0176,5.0293;}
	</style>
	<g>
		<path class="st0-a" d="M165.1,35c0,0,17,1,17-8c1-8.5-4.2-8.8-8-11c0-0.5-2.2-13.8-16-15c-8.5,0-13.1,4-15.5,8.6
		c0.2-0.2,0.3-0.4,0.5-0.6c2.2,0.2,15.5,4,17,16c8.8,0.8,8,10,8,10H165.1z"/>
		<path class="st1-a" d="M168.1,35h-73c0,0-9,0.8-9-9c0.8-5,9-4,9-4s1-7.8,12.8-6c1.5-4.5,31.2-0.2,35.2-7c2.2,0.2,15.5,4,17,16
		C168.8,25.8,168.1,35,168.1,35z"/>
		<polygon class="st2-a" points="80.1,66 97.1,66 80.1,83 	"/>
		<polygon class="st3-a" points="80.1,66 63.1,66 80.1,83 	"/>
		<polygon class="st3-a" points="80.1,66 63.1,66 140.6,0 	"/>
		<polygon class="st4-a" points="80.1,66 125.1,66 140.6,0 	"/>
		<polygon class="st4-a" points="30.1,66 63.1,66 140.6,0 	"/>
		<path class="st0-a" d="M42.1,7c-4.1,0-6.5,2.4-8,5c9.4-1.1,9,12.4,9,13.9c0.1-0.3,0.9-1,6-1.4c6.5-0.5,5,6.5,5,6.5c3,0,7-1,7-7
		s-10-4-10-4S51.3,7,42.1,7z"/>
		<path class="st0-a" d="M146.7,61c-5.7,0-9.1,3.3-11.1,7.1c13.1-1.6,12.5,17.6,12.4,19.7c0.2-0.4,1.3-1.4,8.3-1.9
		c9-0.7,6.9,9.2,6.9,9.2c4.2,0,9.7-1.4,9.7-9.9c0-8.5-13.9-5.7-13.9-5.7S159.6,61,146.7,61z"/>
		<path class="st1-a" d="M23.1,31h31c0,0,1.5-7-5-6.5s-6,1.5-6,1.5s1-15.2-9-14c-9.2-0.5-10.5,5.5-11,6s-4.8,0.8-6,7
		C18.6,31.5,23.1,31,23.1,31z"/>
		<path class="st1-a" d="M120.4,95h43c0,0,2.1-9.9-6.9-9.2c-9,0.7-8.3,2.1-8.3,2.1s1.4-21.6-12.5-19.8c-12.8-0.7-14.6,7.8-15.2,8.5
		s-6.6,1.1-8.3,9.9C114.2,95.7,120.4,95,120.4,95z"/>
		<g>
			<g>
				<path class="st5-a" d="M65.1,70c0,0-0.5,0.2-1.3,0.7"/>
				<path class="st6-a" d="M59.4,73.2C48.1,79.9,23.1,97,15.1,120c-10.5,30.1,39.8,52.4,59,23c18-35-46-56.8-69-11
				c-12.1,29.9,5.8,52.6,13.1,60.2"/>
				<path class="st5-a" d="M20,194c0.7,0.7,1.1,1,1.1,1"/>
			</g>
		</g>
	</g>
</svg>
</section>
<?php
$logo = $result->call_curl("agencias/logos?IdEmpresa=$id","GET");
?>
<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
	<?php get_template_part('footer-widget'); ?>
	<div class="container pt-3 pb-3">
		<div class="site-info b-footer">
			<div class="logo-footer">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php if(!$logo->logo){ echo esc_url(get_theme_mod('wp_bootstrap_starter_logo')); } else { echo $logo->logo; } ?>" alt="<?php if(!$logo->alt) { echo esc_attr(get_bloginfo('name')); } else { echo $logo->alt; }?>">
				</a>
			</div>
			<div style="color: white;">VIA CARGO S.A. Viacargo S.A</div>
			<div class="legal-menu">
				<a href="<?php echo esc_url(home_url('/')); ?>terminos-y-condiciones/">Términos y condiciones</a>
				<a href="<?php echo esc_url(home_url('/')); ?>politicas-de-proteccion/">Políticas de privacidad</a>
			</div>
		</div><!-- close .site-info -->
	</div>
</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->
<?php wp_footer(); ?>
<!-- <div class="fixed whatsapp"><a href="https://api.whatsapp.com/message/ZTTNEHZDQS2HH1" target="_blank">
<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="whatsapp" class="svg-inline--fa fa-whatsapp fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"></path></svg></a>
</div> -->
<div class="modal closed" id="geoloc">
  <a href="#0" class="close-button" onclick="closeMap()"><i class="fa fa-times"></i></a>
  <div class="modal-guts">
    <div id="cont-map"></div>
  </div>
</div>
<!-- info -->
<div class="modal-overlay closed" id="modal-overlay"></div>
<script src="<?php bloginfo('template_url'); ?>/dist/js/app.min.js?v=<?php echo rand(); ?>"></script>
<script>
  window.erxesSettings = {
		messenger: {
			brand_id: "bsDsRH",
		},
	/*forms: [{
			brand_id: "bsDsRH",
			form_id: "soekxi",
	}],*/

			};


var paradas = JSON.parse('<?= json_encode($paradas) ?>')

function openMap(id){
	let identificadorId = '#'+id;
	let identificador = '#'+id+'-id';

	if($(identificador).val() > 0 && ($(identificadorId).val() != '' || $(identificadorId).val().length > 5)){

		for (var propiedad in paradas) {
			if ($(identificador).val() == paradas[propiedad].idparada){

				let desc = '';
				if(typeof(paradas[propiedad].descripcion_parada) === 'string'){
					desc = (paradas[propiedad].descripcion_parada.length === 0) ? paradas[propiedad].descripcion : paradas[propiedad].descripcion_parada;
				}

				if(paradas[propiedad].latitud == null || paradas[propiedad].longitud == null){
						alert("La parada no posee longitud o latitud");
						return false;
				}
				$("#cont-map").append('<iframe src="https://checkout.busplus.com.ar/mapas?latitud='+paradas[propiedad].latitud+'&longitud='+paradas[propiedad].longitud+'&direccion='+paradas[propiedad].direccion+'&nombreparada='+desc+'"></iframe>');

				$("#geoloc").removeClass("closed");
				$("#modal-overlay").removeClass("closed");
			}
		}
	}
	else{
		alert("Debe seleccionar una parada válida");
	}
}

$(document).ready(function() {
	//-- Inicio de la tarea GDM-2539 --//

	/* Evento que al hacer click al boton de "Ingresar" muestra el login y oculta lo anterior. */
	$(document).on('click', '.btnIngresar', function(e){
		$('.sectionQueEsViaClub').addClass('d-none');
		$('.sectionLoginViaClub').removeClass('d-none').addClass('d-flex justify-content-center');
	});

	$(document).on('click', '.btnSubmitLogin', function(e){
		/* Si los campos nrodocumento y password no estan vacios 
		hace el submit del formulario. */
		if( $('#nro').val().length > 1 && $('#password').val().length > 0 ){
			document.getElementById("frmLoginSocio").submit();	
		} 
		
		/* si el campo nrodocumento esta vacio arroja un error. */
		if( $('#nro').val().length < 1 ){
			document.getElementById('nro').style.border ="1px solid red";
			$('.msgEmptyNro').removeClass('d-none');	
		} else{
			document.getElementById('nro').style.border ="1px solid #2d9fd9";
			$('.msgEmptyNro').addClass('d-none');
		}

		/* si el campo password esta vacio arroja un error. */
		if( $('#password').val().length < 1 ){
			document.getElementById('password').style.border ="1px solid red";	
			$('.msgEmptyPassword').removeClass('d-none');
		} else{
			document.getElementById('password').style.border ="1px solid #2d9fd9";
			$('.msgEmptyPassword').addClass('d-none');
		}
	});
	//-- Fin de la tarea GDM-2539 --//
});

$(document).ready(function() {

	var urls = '<?= $_SERVER["REQUEST_URI"]?>'
	if(urls == '/'){
		setTimeout(function() {

			external_file_Cities = external_file_Cities.filter(city => city.includes('Argentina'));
		}, 4000);
	}

	s={
		//url:"<? /*$result->getUrl()*/ ?>paradas?X-API-KEY=<? /*$result->getKey()*/ ?>", //"servicios/paradas.json",
		data:paradas,
		getValue:"descripcion_parada",
		requestDelay:250,
		list:{
			maxNumberOfElements:50,
			match:{enabled:!0},
			onSelectItemEvent:function(){
				var e=$("#origen").getSelectedItemData().idparada;
				$("#origen-id").val(e).trigger("change");
			}
		}
	}
	$("#origen").easyAutocomplete(s)

	i={
		//url:"<? /*$result->getUrl()*/ ?>paradas?X-API-KEY=<? /*$result->getKey()*/ ?>", //"servicios/paradas.json",
		data:paradas,
		getValue:"descripcion_parada",
		requestDelay:250,
		list:{
			maxNumberOfElements:50,
			match:{enabled:!0},
			onSelectItemEvent:function(){
				var e=$("#destino").getSelectedItemData().idparada;
				$("#destino-id").val(e).trigger("change");
			}
		}
	}
	$("#destino").easyAutocomplete(i);

	var mostrar = true
	$(document).on('click', '.cupondescuentoMobile', function(e){
		e.preventDefault();
  		$('#cupondescuento2').val("");
		(mostrar) ? $("#cupondescuento2, .validar").show(  ) : $("#cupondescuento2, .validar").hide(  );
		mostrar = !mostrar
		 //.css("display", "block");
	})

	$(document).on('click', '.abricupon', function(e){
		e.preventDefault();
		$('#cuponValido').text( "");
  		$('#cuponError').text( "");
  		$('#coupon-code').val("");

		$(".cupones").show(  ); //.css("display", "block");
	})

	$(document).on('click', '.cuponcerrar', function(e){
		e.preventDefault();
		$('#cuponValido').text( "");
  		$('#cuponError').text( "");
  		$('#coupon-code').val("");
		$(".cupones").hide(  ); //.css("display", "none");
	})

	$( "#descuento" ).val()

	$(document).on('click', '#validar', function(e){
		let url = '<?= $result->getUrl() ?>'
	
		let pass = $('#passengers-count').val();

		let CodigoDescuento = $( "#descuento" ).val() ? $( "#descuento" ).val() : $( "#cupondescuento2" ).val() ;

		$('.responseMobile').hide();
		$('.responseDesktop').show();
		
		if(!CodigoDescuento){
			showResponse('error',"ERROR, debe ingresar el código");
			return false
		}


		const ida = $( "#start-date-formatted" ).val().split("/");
		let fechaIda = ida[2]+'-'+ida[1]+'-'+ida[0]

		if($( "#start-date-formatted" ).val() == 0){
			showResponse('error',"ERROR, debe ingresar la fecha ida");
			return false
		}

		const vuelta = $( "#end-date-formatted" ).val().split("/");
		let fechaVuelta = vuelta[2]+'-'+vuelta[1]+'-'+vuelta[0];

		if( vuelta != 0){
			pass = pass * 2;
			var round_trip = true;
		}else{
			var round_trip = false;
		}

		$.ajax({
		  url: url+"pases/codigo_validar?CodigoDescuento="+CodigoDescuento+"&Fecha="+fechaIda+"&Pasajeros="+pass,
			headers: {
				'X-API-KEY':'<?= $result->getKey() ?>',
				'Content-Type':'application/json'
			},

		}).done(function(data) {
            showResponse('valido',"Código válido");
			$( "#cupondescuento" ).val(CodigoDescuento)

			if(round_trip == true){
				$.ajax({
				url: url+"pases/codigo_validar?CodigoDescuento="+CodigoDescuento+"&Fecha="+fechaVuelta+"&Pasajeros="+pass,
					headers: {
						'X-API-KEY':'<?= $result->getKey() ?>',
						'Content-Type':'application/json'
					},
				
				}).done(function(data) {
					showResponse('valido',"Código válido");
					$( "#cupondescuento" ).val(CodigoDescuento)
					if(round_trip == true){
						
					}
					}).fail(function(data, textStatus, xhr) {
						//This shows status code eg. 403
						// console.log("fail", data.responseJSON);
						let mensaje = data.responseJSON.message_detail ? data.responseJSON.message_detail : data.responseJSON.message
						showResponse('error',mensaje);
						//This shows status message eg. Forbidden
						// console.log("STATUS: "+xhr);
					}).always(function(data) {
						//TO-DO after fail/done request.
						//console.log("always", data);
					});
			}
            }).fail(function(data, textStatus, xhr) {
                 //This shows status code eg. 403
                 // console.log("fail", data.responseJSON);
				let mensaje = data.responseJSON.message_detail ? data.responseJSON.message_detail : data.responseJSON.message
				showResponse('error',mensaje);
                 //This shows status message eg. Forbidden
                 // console.log("STATUS: "+xhr);
            }).always(function(data) {
                 //TO-DO after fail/done request.
                 //console.log("always", data);
            });

	});

	$(document).on('click', '.validar', function(e){
		e.preventDefault();
		let url = '<?= $result->getUrl() ?>'		

		let pass = $('#passengers-count').val();
			
		const ida = $( "#start-date-formatted" ).val().split("/");
		let fechaIda = ida[2]+'-'+ida[1]+'-'+ida[0]

		$('.responseDesktop').hide();
		$('.responseMobile').show();

		if(!$( "#start-date-formatted" ).val()){
			showResponse('error',"ERROR, debe ingresar la fecha ida");
			return false
		}
		
		if(!$( "#cupondescuento2" ).val()){
			showResponse('error',"ERROR, debe ingresar el código");
			return false
		}

		let CodigoDescuento = $( "#descuento" ).val() ? $( "#descuento" ).val() : $( "#cupondescuento2" ).val() ;

		if(ida == 0){			
			showResponse('error',"ERROR, debe ingresar la fecha ida");
			return false
		}

		const vuelta = $( "#end-date-formatted" ).val().split("/");
		let fechaVuelta = vuelta[2]+'-'+vuelta[1]+'-'+vuelta[0];

		if( vuelta != 0){
			pass = pass * 2;
			var round_trip = true;
		}else{
			var round_trip = false;
		}

		$.ajax({
		  url: url+"pases/codigo_validar?CodigoDescuento="+CodigoDescuento+"&Fecha="+fechaIda+"&Pasajeros="+pass,
			headers: {
				'X-API-KEY':'<?= $result->getKey() ?>',
				'Content-Type':'application/json'
			},

		}).done(function(data) {
            showResponse('valido',"Código válido");
			$( "#cupondescuento" ).val(CodigoDescuento)
			if(round_trip == true){
				$.ajax({
				url: url+"pases/codigo_validar?CodigoDescuento="+CodigoDescuento+"&Fecha="+fechaVuelta+"&Pasajeros="+pass,
					headers: {
						'X-API-KEY':'<?= $result->getKey() ?>',
						'Content-Type':'application/json'
					},
				
				}).done(function(data) {
					showResponse('valido',"Código válido");
					$( "#cupondescuento" ).val(CodigoDescuento)
					if(round_trip == true){
						
					}
					}).fail(function(data, textStatus, xhr) {
						//This shows status code eg. 403
						// console.log("fail", data.responseJSON);
						let mensaje = data.responseJSON.message_detail ? data.responseJSON.message_detail : data.responseJSON.message
						showResponse('error',mensaje);
						//This shows status message eg. Forbidden
						// console.log("STATUS: "+xhr);
					}).always(function(data) {
						//TO-DO after fail/done request.
						//console.log("always", data);
					});
			}
            }).fail(function(data, textStatus, xhr) {
                 //This shows status code eg. 403
                 // console.log("fail", data.responseJSON);
				let mensaje = data.responseJSON.message_detail ? data.responseJSON.message_detail : data.responseJSON.message
				showResponse('error',mensaje);
                 //This shows status message eg. Forbidden
                 // console.log("STATUS: "+xhr);
            }).always(function(data) {
                 //TO-DO after fail/done request.
                 //console.log("always", data);
            });

	})
// DB000201079E
	function showResponse(form,message){
		if(form == 'error'){
			$(".form_valido").css("display", "none");
			$(".form_valido").text("");
			$(".form_error").css("display", "block");
			$(".form_error").text(message);
			setTimeout(function(){
				$(".form_error").css("display", "none");
				$(".form_error").text("");
			}, 2000);
		} else if(form == 'valido') {
			$(".form_error").css("display", "none");
			$(".form_error").text("");
			$(".form_valido").css("display", "block");
			$(".form_valido").text(message);
			setTimeout(function(){
				$(".form_valido").css("display", "none");
				$(".form_valido").text("");
			}, 4000);
		}
	}
	
	$(document).on('change','#origen',function(e){ $('#destino').focus(); });
	$(document).on('change','#destino',function(e){ $('#start-date').focus(); });
	$(document).on('click','.day-item',function(e){ $('#passengers-count').focus(); });
	$(document).on('input','#passengers-count',function(e){ 
		if($('#passengers-count').val().length == 1){
			$('.btn-search a').css('background','#FA9D1B');
			$('.btn-search a').focus();
		}
	});
	$(document).on('focusout','.btn-search',function(e){ $('.btn-search a').css('background','#38C172'); })

	// borrar busqueda
    $(document).on('input','#origen',function(e){
        if ($(this).val().length >= '1'){
            $(".sel-origen .remove").css("display","block");
        } else {
            $(".sel-origen .remove").css("display","none");
        }
    });
    $(document).on('input','#destino',function(e){
        if ($(this).val().length >= '1'){
            $(".sel-destino .remove").css("display","block");
        } else {
            $(".sel-destino .remove").css("display","none");
        }
    });
    $(document).on('click','.remove',function(e){
        let input = $(this).attr('data-input');
        $('#'+input).val('');
        $(".sel-"+input+" .remove").css("display","none");
		$('#'+input).focus();
    });
});

</script>
<?php if(is_page_template('home.php')) { ?>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(".swiper-container").each(function(i) {
	  var pagination = $(".swiper-pagination")[i];
	  var navNext = $(".swiper-button-next")[i];
	  var navPrev = $(".swiper-button-prev")[i];
	  var title = $(this).closest('.swiper').find('h3').html();
	  var slides = $(this).find('.card-slider').children().length;
	  var cardSlider = $('.card-slider')[i];
	  console.log('slides: '+i+" tiene "+slides);
	  console.log('title: '+title);

	  if (slides.length <= 4) {
	    console.log('es menor');
	    $(".card-slider").removeClass("swiper-wrapper");
	    $(".card").removeClass("swiper-slide");
	    navNext.style.display = 'none';
	    navPrev.style.display = 'none';
	    pagination.style.display = 'none';
	}
	  var mySwiper = new Swiper($(this)[0], {
	    // Optional parameters
	    slidesPerView: 1, // mostrar tres tarjetas a la vez
	    slidesPerGroup: 1,
	    spaceBetween: 16, // espacio entre tarjetas
	    loop: false, // bucle infinito
	    breakpoints: {
	      768: {
	        slidesPerView: 4,
	        slidesPerGroup: 4,
	        spaceBetween: 0
	      }
	    },
	    pagination: {
	      el: pagination,
	      clickable: true
	    },
	    navigation: {
	      nextEl: navNext,
	      prevEl: navPrev,
	               }
	  });
	});
	});
</script>
<?php } ?>
</body>
</html>
