<?php
/**
* Template Name: Atencion al cliente
*/
get_header();
?>
<section id="primary" class="content-area inner-section">
  <div id="main" class="site-main" role="main">
    <section class="hero mt-3 pt-5">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="col-12 text-center mt-2">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </section>
    <section class="section-block form-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6" style="background: #f4f5f7;">
            <div class="icon-wrapper">
              <svg id="phone_black_24dp" xmlns="http://www.w3.org/2000/svg" width="40.598" height="40.598" viewBox="0 0 40.598 40.598">
                <path id="Path_215" data-name="Path 215" d="M0,0H40.6V40.6H0Z" fill="none"/>
                <path id="Path_216" data-name="Path 216" d="M9.124,16.177A25.625,25.625,0,0,0,20.271,27.325L23.992,23.6a1.682,1.682,0,0,1,1.725-.406,19.3,19.3,0,0,0,6.039.964,1.7,1.7,0,0,1,1.692,1.692v5.9a1.7,1.7,0,0,1-1.692,1.692A28.754,28.754,0,0,1,3,4.692,1.7,1.7,0,0,1,4.692,3h5.921A1.7,1.7,0,0,1,12.3,4.692a19.217,19.217,0,0,0,.964,6.039,1.7,1.7,0,0,1-.423,1.725Z" transform="translate(2.075 2.075)" fill="#4a5568"/>
              </svg>
              <p>
                <?php the_field('telefono'); ?>
              </p>
            </div>
            <div class="icon-wrapper">
              <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
                <g id="watch_later_black_24dp" transform="translate(0 -0.566)">
                  <g id="Group_83" data-name="Group 83" transform="translate(0 0.247)">
                    <rect id="Rectangle_20" data-name="Rectangle 20" width="37" height="37" transform="translate(0 0.318)" fill="none"/>
                  </g>
                  <g id="Group_86" data-name="Group 86" transform="translate(3.163 3.11)">
                    <g id="Group_85" data-name="Group 85">
                      <g id="Group_84" data-name="Group 84">
                        <path id="Path_217" data-name="Path 217" d="M17.814,2A15.814,15.814,0,1,0,33.628,17.814,15.86,15.86,0,0,0,17.814,2Zm6.642,22.456L16.233,19.4V9.907H18.6V18.13l7.116,4.27Z" transform="translate(-2 -2)" fill="#4a5568"/>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              <div>
                <?php the_field('horario'); ?>
              </div>
            </div>
            <div class="btn my-5">
              <a href="<?php echo esc_url(home_url('/')); ?>preguntas-frecuentes/" title="Ver preguntas frecuentes">Ver preguntas frecuentes</a>
            </div>
            <div class="at-svg mt-5">
              <svg xmlns="http://www.w3.org/2000/svg" width="382.889" height="269.306" viewBox="0 0 382.889 269.306">
                <g id="Group_330" data-name="Group 330" transform="translate(-28 -1695)">
                  <path id="Path_799" data-name="Path 799" d="M39.908,1939.783H374.444a224.947,224.947,0,0,0,14.791-19.114c7.343-10.316,14.945-22.179,14.546-35.858-.415-14.322-10-26.363-20.975-31.333s-23.072-4.208-34.694-2.595c-3.755.523-7.894,1.019-11-1.617-2-1.694-3.222-4.427-4.225-7.136-13.435-36.3.231-84.354-20.4-115.173-3.217-4.808-7.367-9.028-12.331-10.317-4.371-1.137-8.906.1-13.2,1.6-30.7,10.671-59.808,34.911-91.5,30.337-14.917-2.154-28.78-10.659-43.6-13.662a62.453,62.453,0,0,0-46.135,9.063,52.487,52.487,0,0,0-18.081,18.715c-7.039,12.816-8.087,29.144-13.232,43.246-6,16.449-17.262,29.11-24.708,44.646-9,18.777-12.064,41.118-11.671,62.89C38.193,1922.288,38.894,1931.055,39.908,1939.783Z" transform="translate(7.093 15.035)" fill="#daeef4"/>
                  <ellipse id="Ellipse_170" data-name="Ellipse 170" cx="191.445" cy="12.436" rx="191.445" ry="12.436" transform="translate(28 1939.434)" fill="#e9e6e7" style="isolation: isolate"/>
                  <path id="Path_800" data-name="Path 800" d="M231.352,1803.334s-25.329-19.865-56.462-20.074v-.01c-.084,0-.164.005-.246.005s-.164-.005-.246-.005v.01c-31.134.209-56.462,20.074-56.462,20.074-17.374,13.675-15.926,73.5-15.926,73.5,19.151,14.536,67.147,15.24,72.388,15.275v0l.246,0,.246,0v0c5.239-.034,53.237-.738,72.388-15.275C247.278,1876.835,248.726,1817.009,231.352,1803.334Z" transform="translate(52.476 62.598)" fill="#4a5568" style="isolation: isolate"/>
                  <path id="Path_801" data-name="Path 801" d="M180.285,1789.1c-1.275-7.806-48.047-7.806-49.323,0,0,0-3.279,14.1,24.228,14.221v.009c.15,0,.285-.005.432-.005s.284.005.434.005v-.009C183.564,1803.2,180.285,1789.1,180.285,1789.1Z" transform="translate(72.948 62.598)" fill="#0e2f32" style="isolation: isolate"/>
                  <path id="Path_802" data-name="Path 802" d="M180.285,1788.361c-1.275-6.815-48.047-6.815-49.323,0,0,0-3.279,12.309,24.228,12.417v.007c.15,0,.285,0,.432,0s.284,0,.434,0v-.007C183.564,1800.67,180.285,1788.361,180.285,1788.361Z" transform="translate(72.948 62.598)" fill="#cd4149" style="isolation: isolate"/>
                  <path id="Path_803" data-name="Path 803" d="M138.179,1766.5h30.631s-1.863,19.466,2.231,32.491a4.66,4.66,0,0,1-4.412,5.986h0a59.3,59.3,0,0,1-26.529.714l-1.046-.21a4.674,4.674,0,0,1-4.569-6.021A98.571,98.571,0,0,0,138.179,1766.5Z" transform="translate(75.39 50.717)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_804" data-name="Path 804" d="M188.536,1763.5c0,21.29-3.86,53.386-30.768,53.386-17.792,0-30.768-22.98-30.768-53.386s12.552-47.546,30.344-47.546S188.536,1733.093,188.536,1763.5Z" transform="translate(70.223 14.862)" fill="#b2aaac" style="isolation: isolate"/>
                  <path id="Path_805" data-name="Path 805" d="M192.43,1761.547c0,30.406-14.056,53.386-33.33,53.386s-33.33-22.98-33.33-53.386S139.366,1714,158.64,1714,192.43,1731.141,192.43,1761.547Z" transform="translate(69.351 13.477)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_806" data-name="Path 806" d="M116.277,1865.711s-1.694-38.745,1.355-50.711l1.923,51.28Z" transform="translate(62.284 85.119)" fill="#a9b4ad" style="isolation: isolate"/>
                  <path id="Path_807" data-name="Path 807" d="M171.083,1865.711s1.426-38.745-1.142-50.711l-1.619,51.28Z" transform="translate(99.534 85.119)" fill="#0e2f32" style="isolation: isolate"/>
                  <path id="Path_808" data-name="Path 808" d="M267.244,1695h-89.1A10.144,10.144,0,0,0,168,1705.145v46.833a10.143,10.143,0,0,0,10.143,10.143h4.772v13.2a1.739,1.739,0,0,0,2.969,1.231l14.434-14.434h66.922a10.143,10.143,0,0,0,10.143-10.143v-46.833A10.144,10.144,0,0,0,267.244,1695Z" transform="translate(99.309)" fill="#fff"/>
                  <line id="Line_148" data-name="Line 148" x2="77.067" transform="translate(283.471 1713.645)" fill="none" stroke="#76559c" stroke-miterlimit="10" stroke-width="2"/>
                  <line id="Line_149" data-name="Line 149" x2="34.804" transform="translate(284.714 1740.991)" fill="none" stroke="#76559c" stroke-miterlimit="10" stroke-width="2"/>
                  <line id="Line_150" data-name="Line 150" x2="52.206" transform="translate(284.714 1728.561)" fill="none" stroke="#76559c" stroke-miterlimit="10" stroke-width="2"/>
                  <line id="Line_151" data-name="Line 151" x2="17.402" transform="translate(341.892 1728.561)" fill="none" stroke="#76559c" stroke-miterlimit="10" stroke-width="2"/>
                  <path id="Path_809" data-name="Path 809" d="M69.932,1754.406h37.412a18.668,18.668,0,0,1,4.677,36.75v14.69l-17.066-14.028H69.932a18.706,18.706,0,0,1,0-37.412Z" transform="translate(16.476 42.138)" fill="#fff" style="isolation: isolate"/>
                  <circle id="Ellipse_171" data-name="Ellipse 171" cx="4.473" cy="4.473" r="4.473" transform="translate(117.923 1809.964)" fill="#ee9726" style="isolation: isolate"/>
                  <circle id="Ellipse_172" data-name="Ellipse 172" cx="4.473" cy="4.473" r="4.473" transform="translate(100.031 1809.964)" fill="#ee9726" style="isolation: isolate"/>
                  <circle id="Ellipse_173" data-name="Ellipse 173" cx="4.473" cy="4.473" r="4.473" transform="translate(82.139 1809.964)" fill="#ee9726" style="isolation: isolate"/>
                  <path id="Path_810" data-name="Path 810" d="M168.58,1881.269H73.5a5.373,5.373,0,0,1-5.28-4.383L53.5,1798.362A5.374,5.374,0,0,1,58.78,1792h95.076a5.371,5.371,0,0,1,5.28,4.383l14.724,78.525A5.372,5.372,0,0,1,168.58,1881.269Z" transform="translate(18.021 68.805)" fill="#b2aaac" style="isolation: isolate"/>
                  <path id="Rectangle_170" data-name="Rectangle 170" d="M.5,0H150.037a2,2,0,0,1,2,2V9.159a2,2,0,0,1-2,2H.5a.5.5,0,0,1-.5-.5V.5A.5.5,0,0,1,.5,0Z" transform="translate(85.672 1944.495)" fill="#b2aaac" style="isolation: isolate"/>
                  <path id="Path_811" data-name="Path 811" d="M50.635,1799.418l6.327-6.482H71.011v6.769Z" transform="translate(16.056 69.469)" fill="#b2aaac" style="isolation: isolate"/>
                  <path id="Path_812" data-name="Path 812" d="M165.316,1884.534H70.24a5.373,5.373,0,0,1-5.28-4.383l-14.724-78.525a5.373,5.373,0,0,1,5.28-6.362h95.076a5.372,5.372,0,0,1,5.28,4.383l14.724,78.525A5.373,5.373,0,0,1,165.316,1884.534Z" transform="translate(15.706 71.12)" fill="#dad3d7" style="isolation: isolate"/>
                  <ellipse id="Ellipse_174" data-name="Ellipse 174" cx="7.652" cy="8.369" rx="7.652" ry="8.369" transform="matrix(0.937, -0.349, 0.349, 0.937, 109.816, 1904.69)" fill="#b2aaac" style="isolation: isolate"/>
                  <circle id="Ellipse_175" data-name="Ellipse 175" cx="24.785" cy="24.785" r="24.785" transform="translate(316.876 1780.466)" fill="#00b0ac" style="isolation: isolate"/>
                  <text id="_" data-name="?" transform="translate(331.708 1813.107)" fill="#fff" font-size="28" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">?</tspan></text>
                  <path id="Path-7" d="M72,1754.984h57.115s-1.138-6.333-6.993-5.234c-2.477-2.275-3.067-4.374-7.577-3.489-1.465-3.152-.791-4.581-6.995-7.56-5.157-2.226-10.148,2.966-9.907,4.653-1.562-1.841-6.442-.533-6.995,2.9-2.9-.549-7.191.795-9.042,3.229C76.979,1748.9,73.364,1748.42,72,1754.984Z" transform="translate(36.977 31.582)" fill="#fcfcfd"/>
                  <path id="Path-9" d="M275.334,1831.221H200.978s1.484-8.362,9.106-6.911c3.224-3,3.991-5.776,9.863-4.607,1.908-4.162,1.026-6.047,9.106-9.983,6.714-2.94,13.211,3.916,12.9,6.143,2.032-2.431,8.386-.7,9.1,3.838,3.774-.725,9.362,1.05,11.772,4.265C268.852,1823.187,273.556,1822.555,275.334,1831.221Z" transform="translate(122.382 89.153)" fill="#fcfcfd"/>
                  <circle id="Ellipse_176" data-name="Ellipse 176" cx="28.953" cy="28.953" r="28.953" transform="translate(119.718 1702.51)" fill="#fff" style="isolation: isolate"/>
                  <path id="Path_216" data-name="Path 216" d="M95.869,1720.518a25.126,25.126,0,0,0,10.923,10.922l3.646-3.649a1.651,1.651,0,0,1,1.691-.4,18.9,18.9,0,0,0,5.916.945A1.664,1.664,0,0,1,119.7,1730v5.781a1.664,1.664,0,0,1-1.658,1.656,28.172,28.172,0,0,1-28.175-28.17,1.666,1.666,0,0,1,1.658-1.658h5.8a1.669,1.669,0,0,1,1.653,1.658,18.858,18.858,0,0,0,.944,5.918,1.665,1.665,0,0,1-.414,1.691Z" transform="translate(43.885 8.942)" fill="#38c172"/>
                  <g id="Group_330-2" data-name="Group 330" transform="translate(285.726 1826.618)">
                    <path id="Path_813" data-name="Path 813" d="M178.776,1796.309s-.067,16.606,25.572,33.115l17.191-17.189S201.008,1790.75,178.776,1796.309Z" transform="translate(-178.776 -1755.408)" fill="#38c172"/>
                    <path id="Path_814" data-name="Path 814" d="M238.617,1796.309s.067,16.606-25.572,33.115l-17.191-17.189S216.386,1790.75,238.617,1796.309Z" transform="translate(-166.661 -1755.408)" fill="#38c172"/>
                    <path id="Path_815" data-name="Path 815" d="M182.811,1784.994s-.055,13.423,20.669,26.768l13.9-13.9S200.781,1780.5,182.811,1784.994Z" transform="translate(-175.914 -1763.309)" fill="#74d49c"/>
                    <path id="Path_816" data-name="Path 816" d="M195.574,1772s-7.607,11.059,2,33.761l19.309-3.653S212.951,1778.412,195.574,1772Z" transform="translate(-168.888 -1772)" fill="#38c172"/>
                    <path id="Path_817" data-name="Path 817" d="M231.182,1784.994s.055,13.423-20.671,26.768l-13.9-13.9S213.212,1780.5,231.182,1784.994Z" transform="translate(-166.122 -1763.309)" fill="#74d49c"/>
                    <path id="Path_818" data-name="Path 818" d="M217.887,1775.874s6.993,11.457-3.834,33.6l-19.079-4.7S200.185,1781.33,217.887,1775.874Z" transform="translate(-167.287 -1769.252)" fill="#c3ecd5"/>
                    <path id="Path_819" data-name="Path 819" d="M193.342,1825.872s-5.374-29.226,12.16-43.981c0,0,13.493,9.68,11.842,40.13" transform="translate(-168.994 -1764.984)" fill="#38c172"/>
                    <path id="Path_820" data-name="Path 820" d="M195.636,1790.164s-11.791,11.7-5.333,41.5h24.308S215.287,1801.953,195.636,1790.164Z" transform="translate(-171.971 -1759.116)" fill="#c3ecd5"/>
                    <path id="Path_821" data-name="Path 821" d="M195.927,1831.661s-.673-29.708,18.975-41.5c0,0,11.791,11.7,5.335,41.5" transform="translate(-166.612 -1759.116)" fill="#60cd8e"/>
                    <rect id="Rectangle_171" data-name="Rectangle 171" width="35.37" height="67.933" transform="translate(0.707 58.558)" fill="#ee9726" style="isolation: isolate"/>
                    <rect id="Rectangle_172" data-name="Rectangle 172" width="35.37" height="67.933" transform="translate(36.077 58.558)" fill="#b3711d" style="isolation: isolate"/>
                  </g>
                  <g id="Group_332" data-name="Group 332" transform="translate(158.313 1717.221)" style="isolation: isolate">
                    <g id="Group_331" data-name="Group 331">
                      <path id="Path_822" data-name="Path 822" d="M161.185,1712.979s-28.812,1.376-32.241,35.672c0,0-.34,9.256-6.513,13.026,0,0-9.606,6.174-4.8,22.633,0,0,2.06,10.292-6.861,16.122,0,0-7.543,6.518-2.737,15.781,0,0,3.087,3.089-2.4,8.574,0,0-4.8,8.574,3.431,20.235,0,0-1.027-8.565,1.027-10.627,0,0,15.746,11.984,14.037,17.823a76.4,76.4,0,0,1,28.464-7.887s9.707-1.222,7.653-26.934c0,0-13.675-13.675-16.2-33.766,0,0-6.022-31.188,15.028-36.256,5.347-1.287,6.28-7.767,6.924-7.637,0,0,5.829,15.086,22.984,16.119,0,0,2.735,17.141,13.37,18.863,0,0,.27,25.722-10.27,38.909-4.569,5.716-6.219,13.393-3.306,20.105,2.634,6.068,8.733,11.7,22.16,12.314,0,0-5.489,13.716,6.516,16.459,0,0,1.362-16.792,11.307-16.792l4.458,5.482s10.635-13.37,5.831-22.626c0,0-9.945-4.8-6.171-17.49,0,0,6.171-4.8-2.4-10.975,0,0-1.714-2.4,1.385-5.144,0,0,10.629-8.921-7.207-28.47l-.679-17.835s-4.8-26.75-37.381-39.445C186.576,1709.207,163.245,1704.744,161.185,1712.979Z" transform="translate(-104.236 -1708)" fill="#95898c"/>
                    </g>
                  </g>
                  <g id="Group_333" data-name="Group 333" transform="translate(255.143 1776.442)">
                    <path id="Path_823" data-name="Path 823" d="M165.125,1758.639h0a2.779,2.779,0,0,0,3-2.6l.583-5.547a2.909,2.909,0,0,0-2.388-3.275h0a2.775,2.775,0,0,0-3,2.6l-.583,5.549A2.911,2.911,0,0,0,165.125,1758.639Z" transform="translate(-159.583 -1739.42)" fill="#ee9726" style="isolation: isolate"/>
                    <path id="Path_824" data-name="Path 824" d="M163.29,1768.7h0a2.779,2.779,0,0,0,3-2.6l2.118-20.158a2.911,2.911,0,0,0-2.388-3.275h0a2.778,2.778,0,0,0-3,2.6l-2.118,20.16A2.913,2.913,0,0,0,163.29,1768.7Z" transform="translate(-160.885 -1742.645)" fill="#764d96" style="isolation: isolate"/>
                  </g>
                  <g id="Group_334" data-name="Group 334" transform="translate(190.545 1778.151)">
                    <path id="Path_825" data-name="Path 825" d="M126.7,1759.638h0a2.779,2.779,0,0,1-3.005-2.6l-.583-5.547a2.912,2.912,0,0,1,2.388-3.275h0a2.778,2.778,0,0,1,3.005,2.6l.583,5.548A2.913,2.913,0,0,1,126.7,1759.638Z" transform="translate(-123.093 -1740.42)" fill="#ee9726" style="isolation: isolate"/>
                    <path id="Path_826" data-name="Path 826" d="M129.17,1769.7h0a2.779,2.779,0,0,1-3-2.6l-2.116-20.158a2.91,2.91,0,0,1,2.386-3.275h0a2.778,2.778,0,0,1,3.005,2.6l2.118,20.16A2.912,2.912,0,0,1,129.17,1769.7Z" transform="translate(-122.427 -1743.646)" fill="#764d96" style="isolation: isolate"/>
                  </g>
                  <g id="Group_335" data-name="Group 335" transform="translate(196.16 1793.432)">
                    <path id="Path_827" data-name="Path 827" d="M153.826,1787.626c-15.16,0-27.448-15.688-27.448-35.041" transform="translate(-126.378 -1752.585)" fill="none" stroke="#764d96" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                    <path id="Path_828" data-name="Path 828" d="M139.4,1773.507h0a2.777,2.777,0,0,1,3-2.6l5.09.634a2.911,2.911,0,0,1,2.388,3.275h0a2.778,2.778,0,0,1-3,2.6l-5.09-.636A2.911,2.911,0,0,1,139.4,1773.507Z" transform="translate(-117.154 -1739.604)" fill="#764d96" style="isolation: isolate"/>
                  </g>
                </g>
              </svg>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 px-4">
                        <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>

  <style type="text/css" media="screen, projection">

                  @import url(https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css);

  </style>

  <iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://wcentrix.net/app/form_web.html?accountID=Va4488&wcboxID=996d7eaebc954d46a79af8e7017ebd19" scrolling="no" height="750px" width="100%" frameborder="0" >

  </iframe>
            <?php //echo do_shortcode('[contact-form-7 id="245" title="Atencion al cliente"]');?>
          </div>
        </div>
      </div>
    </section>
  </div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
