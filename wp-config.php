<?php

define('WP_ENVIRONMENT', 'production');

define('WP_CACHE', true);

if (WP_ENVIRONMENT === 'production') {
    define('WPCACHEHOME', '/www/wwwroot/busplus.com.ar/assets/scripts/wp-super-cache/');
    define('WP_CONTENT_DIR', '/www/wwwroot/busplus.com.ar/assets');
    define('WP_CONTENT_URL', 'https://busplus.com.ar/assets');
    define('WP_PLUGIN_DIR', '/www/wwwroot/busplus.com.ar/assets/scripts');
    define('WP_PLUGIN_URL', 'https://busplus.com.ar/assets/scripts');
    define('WPMU_PLUGIN_DIR', '/www/wwwroot/busplus.com.ar/assets/mu-plugins');
    define('WPMU_PLUGIN_URL', 'https://busplus.com.ar/assets/mu-plugins');
    define('DB_NAME', "db_busplus");
    define('DB_USER', "db_busplus");
    define('DB_PASSWORD', "dWT2T8KdSSezxpb7");
    define('DB_HOST', "localhost");
    define('DB_CHARSET', 'latin1');
    define('DB_COLLATE', '');
} else {
    define('WPCACHEHOME', '/www/wwwroot/web-busplus.viatesting.com.ar/assets/scripts/wp-super-cache/');
    define('WP_CONTENT_DIR', '/www/wwwroot/web-busplus.viatesting.com.ar/assets');
    define('WP_CONTENT_URL', 'https://web-busplus.viatesting.com.ar/assets');
    define('WP_PLUGIN_DIR', '/www/wwwroot/web-busplus.viatesting.com.ar/assets/scripts');
    define('WP_PLUGIN_URL', 'https://web-busplus.viatesting.com.ar/assets/scripts');
    define('WPMU_PLUGIN_DIR', '/www/wwwroot/web-busplus.viatesting.com.ar/assets/mu-plugins');
    define('WPMU_PLUGIN_URL', 'https://web-busplus.viatesting.com.ar/assets/mu-plugins');
    define('DB_NAME', "db_busplus1");
    define('DB_USER', "db_busplus1");
    define('DB_PASSWORD', "PfK5YtNLwyThwXWS");
    define('DB_HOST', "localhost");
    define('DB_CHARSET', 'latin1');
    define('DB_COLLATE', '');
}

$table_prefix = 'wpbspls_';
/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Location of your WordPress configuration. */
require_once(ABSPATH . 'config/wp-config.php');
