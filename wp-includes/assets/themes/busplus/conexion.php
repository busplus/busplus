<?php

class WebService {
	
	private $entorno = "prod";
	private $usuario = '';
	private $clave = '';
	private $clave_acceso = '';
	private $url = '';
	
	var $key = null;
	var $errores = null;
	
   function __construct() {
	   	   
	   if($this->entorno === "prod"){		   
			$this->clave_acceso = "qu3r1c4qu3s0s";
			$this->url = 'https://ws.busplus.com.ar/';
	   }
	   else{		   
		   // $this->usuario = "desarrollo";
		   // $this->clave = "55446";
		   $this->clave_acceso = "qu3r1c4qu3s0s"; // "8daf7shf8ahgf8g8";
		   $this->url = 'https://ws.viatesting.com.ar/';
	   }
	   
	   $this->sesion();
   }
	
	/**
	 *  funcion para hacer llamadas al WS, ya internamente tiene la api-key
	 *  url = url del webservice que se desea ejecutar, por ejemplo: sesion
	 *  tipo = el tipo de peticion GET o POST
	 *  retorna un objeto de la llamada al web service
	 * */
	function call_curl_sesion($url, $tipo = "GET"){
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->url.$url,
		  CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('clave_acceso' => $this->clave_acceso),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		return json_decode($response);
	}
	
	/**
	 *  funcion para hacer llamadas al WS, ya internamente tiene la api-key
	 *  url = url del webservice que se desea ejecutar, por ejemplo: sesion
	 *  tipo = el tipo de peticion GET o POST
	 *  retorna un objeto de la llamada al web service
	 * */
	function call_curl($url, $tipo = "GET"){
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->url.$url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $tipo,
		  
		  CURLOPT_HTTPHEADER => array(
			'X-API-KEY: '.$this->key,
    		'Content-Type: application/json'
		  ),
		));
		
		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response);
	}
	
	private function sesion(){
				
		$valores = $this->call_curl_sesion("sesion", "POST");		
		
		if(isset($valores) and isset($valores->message)){
			$this->errores = $valores->message;
		}
		else{
			if(isset($valores->key)){ 
				$this->key = $valores->key;
			}
			else{
				$this->errores = "ERROR, no se pudo conectar al WS";
			}			
		}
	}
	
	public function getKey(){
		return $this->key;
	}
	
	public function getUrl(){
		return $this->url;
	}
}

?>