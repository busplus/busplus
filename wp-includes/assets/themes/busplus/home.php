<?php
/**
* Template Name: Home Template
*/
get_header();
?>
<section id="primary" class="content-area inicio">
  <div id="main" class="site-main" role="main">
    <section id="section1">
      <div class="container">
        <h2>Tu boletería <br>online</h2>
        <div class="tab">
          <button class="tablinks" onclick="openCity(event, 'Bus')" id="defaultOpen">Bus</button>
          <button class="tablinks" onclick="openCity(event, 'Hoteles')">Hoteles</button>
          <button class="tablinks t-last" onclick="openCity(event, 'Actividades')">Actividades</button>
          <!-- <button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Paquetes</button>
          <button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Nieve</button>
          <button class="tablinks br-w" onclick="openCity(event, 'Actividades')">Asistencia</button>
          <button class="tablinks last-tablink" onclick="openCity(event, 'Actividades')">Traslados</button> -->
          <!-- <button id="entercode" class="tablinks last-tablink-2">Agregar código de descuento</button> -->
        </div>
        <div class="cont-tabcont">
          <div id="Bus" class="tabcontent block">
            <?php require 'assets/themes/busplus/inc/buscador.php';?>
          </div>
          <div id="Hoteles" class="tabcontent">
            <?php require 'assets/themes/busplus/inc/buscador-hoteles.php';?>
          </div>
          <div id="Actividades" class="tabcontent">
            <?php require 'assets/themes/busplus/inc/buscador-actividades.php';?>
          </div>
          <!-- <div id="Paquetes" class="tabcontent">
            <?php //require 'assets/themes/busplus/inc/buscador.php';?>
          </div>
          <div id="Nieve" class="tabcontent">
            <?php //require 'assets/themes/busplus/inc/buscador.php';?>
          </div>
          <div id="Asistencia" class="tabcontent">
            <?php //require 'assets/themes/busplus/inc/buscador.php';?>
          </div>
          <div id="Traslados" class="tabcontent">
            <?php //require 'assets/themes/busplus/inc/buscador.php';?>
          </div> -->
        </div>
        <?php if (have_rows('botones')):
?>
          <div class="content-b-s1">
  
          <?php
          $i = 0;
          $numrows = count(get_field('botones'));

          while (have_rows('botones')) : the_row();
           $i++;
          $link = get_sub_field('link');
			$link_url = $i === 2 ? '/estado-viaje/' : '/localiza-micro/';
          if (isset($link['url'])) {
              $link_url = $link['url'];
          }?>
              <a href="<?php if ($link_url) {
              echo esc_url($link_url);
          } else {
              echo $link_url;
          } ?>" class="item-s1 br-s1-item <?php if ($i == 1) {
              echo "br-1";
          } if ($i == $numrows) {
              echo "br-2";
          } ?>">
                <i class="fa fa-<?php if (get_sub_field('icono')) {
              the_sub_field('icono');
          } else {
              echo "calendar";
          }?>" aria-hidden="true"></i>
                <p><?php the_sub_field('texto_del_boton'); ?></p>
              </a>
          <?php endwhile; ?>
        <?php else: ?>
          <?php // no layouts found?>
        </div>
  <?php endif; ?>
        <!--
          <a href="https://checkout.busplus.com.ar/gestionpasaje" class="item-s1 br-1 br-s1-item">
            <i class="fa fa-address-card" aria-hidden="true"></i>
            <p>GESTIONÁ TUS PASAJES</p>
          </a>
          <a href="" class="item-s1 br-s1-item">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <p>ESTADO DE TU VIAJE</p>
          </a>
          <a href="" class="item-s1 br-s1-item">
            <i class="fa fa-map-marker-alt" aria-hidden="true"></i>
            <p>LOCALIZÁ TU MICRO</p>
          </a>
          <a href="<?php echo esc_url(home_url('/')); ?>protocolo-covid" class="item-s1 br-2">
            <i class="fa fa-virus" aria-hidden="true"></i>
            <p>PROTOCOLO COVID</p>
          </a> -->
      </div>
      <div class="svg-hero">
        <svg class="desk" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 949.7 345.1" style="enable-background:new 0 0 949.7 345.1;" xml:space="preserve">
        <style type="text/css">
        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#B3E3C8;}
        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#EBF9F1;}
        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#E0F1E9;}
        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:#F5FCF8;}
        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:#38C172;}
        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:#DAEFE4;}
        .st7{fill-rule:evenodd;clip-rule:evenodd;fill:#C8E9D7;}
        .st8{fill-rule:evenodd;clip-rule:evenodd;fill:#F3F3F3;}
        .st9{fill-rule:evenodd;clip-rule:evenodd;fill:#FDFDFD;}
        .st10{fill-rule:evenodd;clip-rule:evenodd;fill:#D0EBDD;}
        .st11{fill-rule:evenodd;clip-rule:evenodd;fill:#D0ECDD;}
        .st12{fill-rule:evenodd;clip-rule:evenodd;fill:#BECBC4;}
        .st13{fill-rule:evenodd;clip-rule:evenodd;fill:#AEB8B2;}
        .st14{fill-rule:evenodd;clip-rule:evenodd;fill:#897378;}
        .st15{fill-rule:evenodd;clip-rule:evenodd;fill:#DAD3D7;}
        .st16{fill-rule:evenodd;clip-rule:evenodd;fill:#B1BCB6;}
        .st17{fill-rule:evenodd;clip-rule:evenodd;fill:#95898C;}
        .st18{fill-rule:evenodd;clip-rule:evenodd;fill:#DDD4D9;}
        .st19{fill-rule:evenodd;clip-rule:evenodd;fill:#856A71;}
        .st20{fill-rule:evenodd;clip-rule:evenodd;fill:#C0CDC5;}
        .st21{fill-rule:evenodd;clip-rule:evenodd;fill:#8E777E;}
        .st22{fill-rule:evenodd;clip-rule:evenodd;fill:#95888C;}
        .st23{fill-rule:evenodd;clip-rule:evenodd;fill:#928387;}
        .st24{fill-rule:evenodd;clip-rule:evenodd;fill:#B3ABAD;}
        .st25{fill-rule:evenodd;clip-rule:evenodd;fill:#B9B1B3;}
        .st26{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;stroke:#FFFFFF;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        .st27{fill-rule:evenodd;clip-rule:evenodd;fill:#E6E3E5;}
        .st28{fill-rule:evenodd;clip-rule:evenodd;fill:#DFF3E8;}
        </style>
        <g>
          <g>
            <path class="st0" d="M346.6,32.5h-45.1c0,0,0.9-5.1,5.5-4.2c2-1.8,2.4-3.5,6-2.8c1.2-2.5,0.6-3.6,5.5-6c4.1-1.8,8,2.4,7.8,3.7
            c1.2-1.5,5.1-0.4,5.5,2.3c2.3-0.4,5.7,0.6,7.1,2.6C342.7,27.7,345.6,27.3,346.6,32.5z"/>
            <rect x="75.7" y="157.1" class="st1" width="622" height="79"/>
            <path class="st2" d="M947.7,0H730.8L594.6,106.4c-79.9,69.3-46.5,248,133.4,146.2c94.6,121.1,199.5,89.8,219.7,82.2V0z"/>
            <path class="st3" d="M596.4,236.1c27.4-115.6-272-196.8-272-196.8s-88.8-79.7-141.7,74.9c-13.8-4.6-28.6-7.1-44-7.1
            C65.2,107.1,5.1,164,0,236.1H596.4z"/>
            <path class="st4" d="M764.8,27.5c13.3,0,24.1,11,24.1,24.5c0,1.2-0.1,2.4-0.2,3.5h-96c1.2-8.9,8.8-15.8,17.9-15.8
            c2.9,0,5.6,0.7,8,1.9c4.4-4.4,10.4-7.1,17-7.1c3.7,0,7.2,0.8,10.3,2.3C750.3,31.2,757.1,27.5,764.8,27.5z"/>
            <path class="st0" d="M502.8,22.5c13.3,0,24.1,11,24.1,24.5c0,1.2-0.1,2.4-0.2,3.5h-96.1c1.2-8.9,8.8-15.8,17.9-15.8
            c2.9,0,5.6,0.7,8,1.9c4.4-4.4,10.4-7.1,17-7.1c3.7,0,7.2,0.8,10.3,2.3C488.3,26.2,495.1,22.5,502.8,22.5z"/>
            <path class="st5" d="M801.8,124.3c17.4,0,31.4,14.1,31.4,31.4c0,17.4-14.1,31.4-31.4,31.4s-31.5-14.1-31.5-31.4
            C770.3,138.4,784.4,124.3,801.8,124.3z"/>
            <path class="st6" d="M640,188.2H137c-1.6,0-3,1.3-3,3c0,1.7,1.4,3,3,3h503c1.7,0,3-1.3,3-3C643,189.5,641.6,188.2,640,188.2z"/>
            <path class="st1" d="M147.6,73.1l10-21c0,0,6,0.3,0,12s-12,25-12,25s-0.3,4.3-6,3L147.6,73.1z"/>
            <path class="st1" d="M132.6,117.1l-4,28l5-3l4-20l5-22c0,0-7,1-7,2S132.6,117.1,132.6,117.1z"/>
            <path class="st7" d="M624.7,47.1h-438c0,0-21.7-0.6-29,6c2.9,2.9,3,6,3,6l-17,33h-3l-5,11l7-2l-7,34c0,0,1.4,5.7-6,10
            c-0.6,6.8,0,25,0,25s3.8,13.2,11,14s64,0,64,0v-15c0,0,2.1-16,20-16c16.9-0.9,20,14,20,14v17h223v-12c0.1-5.7,6.1-18,22-18
            s20,16,20,16h4c0,0,2-16,20-16c18,0,20,16,20,16v15h35l43-4c0,0,11-2.8,11-20v-83C640.1,55.7,641.6,47.1,624.7,47.1z"/>
            <circle class="st1" cx="533.5" cy="174.7" r="18.5"/>
            <circle class="st1" cx="488.5" cy="174.7" r="18.5"/>
            <circle class="st1" cx="225.5" cy="174.7" r="18.5"/>
            <circle class="st8" cx="533.5" cy="174.7" r="11"/>
            <circle class="st8" cx="488.5" cy="174.7" r="11"/>
            <circle class="st8" cx="225.5" cy="174.7" r="11"/>
            <path class="st1" d="M218.6,149.1h169h45c0,0,7.7,2.2,14-6c6.3-8.2,15-19,15-19s7.6-11-7-11h-212c0,0-5.5-1.6-11,5
            c-5.5,6.7-16,21-16,21S210.2,148.8,218.6,149.1z"/>
            <path class="st1" d="M190.6,50.1h267h166c0,0,7.2-1.2,7,7v31h-168h-227c0,0-24.3,0.9-29,16c-14.4,18.8-32,41-32,41s-2.7,7-18,7
            h-10c0,0-9,1.2-9-8c0-9.2,2-22,2-22l7-28l14-32C160.6,62.1,161.9,50.1,190.6,50.1z"/>
            <path class="st4" d="M819.5,32.5h45.2c0,0-0.9-5.1-5.5-4.2c-2-1.8-2.4-3.5-6-2.8c-1.2-2.5-0.6-3.6-5.5-6c-4.1-1.8-8,2.4-7.8,3.7
            c-1.2-1.5-5.1-0.4-5.5,2.3c-2.3-0.4-5.7,0.6-7.1,2.6C823.4,27.7,820.6,27.3,819.5,32.5z"/>
            <polygon class="st9" points="72.7,124.1 61.7,130.1 180.9,130.1 180.9,236 187.3,236 187.3,130.1 386.9,130.1 386.9,236
            393.3,236 393.3,130.1 593.8,130.1 593.8,236 600.3,236 600.3,130.1 800.8,130.1 800.8,236 807.3,236 807.3,130.1 949.7,130.1
            949.7,124.1 807.3,124.1 807.3,0 800.8,0 800.8,124.1 600.3,124.1 600.3,0 593.8,0 593.8,124.1 393.3,124.1 393.3,0 386.9,0
            386.9,124.1 187.3,124.1 187.3,0 180.9,0 180.9,124.1 		"/>
            <path class="st10" d="M768.7,169v31.1h-9.3h-4.2V169v-6.1v-6.1c0-17.9-14.5-32.4-32.4-32.4c-17.9,0-32.4,14.5-32.4,32.4v6.1v6.1
            v31.1h-9.9c-2.6,0-4.8,2.1-4.8,4.8c0,2.7,2.2,4.8,4.8,4.8h9.6V236h3.8v-26.3h65.5h0h9.6V236h0.5h3.3h0.5v-26.3h73.9l0,26.3h1.2
            h2.6h0.2v-26.3h15.7h59.3l0,26.3h3.8v-26.3h15.9c1.4,0,2.7-0.6,3.6-1.6v-6.5c-0.1-0.1-0.2-0.2-0.3-0.3h0.3v-75.8
            c-13.7,3.8-23.8,16.3-23.8,31.2v6.1v6.1v31.1h-13.5V169v-6.1v-6.1c0-17.9-14.5-32.4-32.4-32.4l-0.4,0l-0.4,0
            c-17.9,0-32.4,14.5-32.4,32.4v6.1v6.1v31.1h-12.7V169v-6.1v-6.1c0-17.9-14.5-32.4-32.4-32.4l-0.4,0l-0.4,0
            c-17.9,0-32.4,14.5-32.4,32.4v6.1V169z"/>
            <path class="st11" d="M284.6,112.4c1-0.5,3.8,2.2,3.8,3.8v67h-28.5v-66c0-3.5,5-5,7.2-4.8H284.6z M290.6,117.1v67
            c-0.2,0.2-0.5,0.8-1,1h-31c-0.2-0.2-0.8-0.5-1-1v-66.5c0-4,5-7.2,8.5-7.2l17.5-0.2C287.6,110.1,290.6,113.1,290.6,117.1z"/>
            <path class="st12" d="M382.7,56l-1.5,3l-9,6.8v4.3l-4.9,10.2l-6.9,19.5l-4.6,17.8l-1.9,13.2c0,0,7.2-1.7,17.2-1.2
            c10,0.5,18.2,3.8,18.2,3.8l-2.4-10c0,0-2.5-3.5-0.1-13.1c2.4-9.6,5.2-22.5,5.2-22.5l2.9-7.9c0,0,1.6-5.8-1.9-10.8
            s-6.5-7.6-6.5-7.6L382.7,56z"/>
            <polygon class="st13" points="357.9,107.2 357.1,109.9 372.3,112.6 382.2,96.5 385,97.4 393.4,78.7 383.8,95.8 373.8,91
            369.2,101.8 		"/>
            <path class="st14" d="M332.3,97.7l-9.8-9.7c0,0-1.7-0.7-1.8-0.2s0.7,1.8,0.7,1.8l8.3,8.2L332.3,97.7z"/>
            <path class="st15" d="M341.7,101.1c0,0.2,26.8-2.3,26.8-2.3s4.8-8.1,5-8.1c0.2,0,8.6,5,8.6,5l-10.4,15.5l-29.4-5.1
            c0,0-4.1,1.6-5.7,0.9c-1.6-0.7-3.7-1.6-3.7-1.6l-0.5-1.8l-1.7-1.6l0.1-1.9l2-0.3l-1.9-1.5l-1.9,0.4l-0.5-1l4.5-0.6l4.9,0.1
            C337.9,97.2,341.7,99.1,341.7,101.1z"/>
            <path class="st15" d="M357.9,41.3l0.7,4.9l2.6,3.6l-0.6,7.9l2.5,2.4l10,1.2c0,0,0.1,4.9,0.3,4.7c0.2-0.2,7.8-5.7,7.8-5.7l1.6-4.3
            l-1.3-6.7l-7.2-9.4L357.9,41.3z"/>
            <path class="st16" d="M378.2,64.8v3.8c0,0,0.4,1.3,2.3,1.3h13l-3.7-4.8l-3.6-4.5L378.2,64.8z"/>
            <polygon class="st0" points="373,66.2 373,72.4 377.5,67.2 382.8,63 385.8,61.1 383.4,57.8 375.5,63.7 		"/>
            <path class="st17" d="M376.3,34.5c0.1,0.1,0,0.5,0.2,0.7c0.4,0.4,0.9,1.2,1.1,1.6c0.2,0.3,0.6,0.4,0.7,0.8c0.1,0.1,0,0.5,0.2,0.7
            c0.2,0.3,0.5,0.5,0.7,0.8c0.2,0.2,0.3,0.7,0.5,1.1c0,0.1,0.3,0.1,0.4,0.4c0.3,0.4,0.5,1.1,0.7,1.6c0.1,0.1,0.5,0.3,0.5,0.6
            c0.2,0.7,0.1,1.6,0.2,2.4s0.5,2.3,0.5,2.5c0,0.3-0.1,0.6-0.1,4.4c0,0.2-0.3,0.7-0.6,0.7c-0.7,0-3.1-2.1-3.7-2.8
            c-0.5-0.5-0.9-1.3-1.3-2c-0.3-0.7-0.6-1.6-1.1-2.3c-0.1-0.3-0.4-0.5-0.6-0.7c-0.4-0.4-1-0.7-1.2-0.7c-1.9,0-2.7,5-2.7,5.5
            c0,2.2,1.3,4.6,2.8,5.3c1.1,0.6,2.5,0.6,3.6,1.3c1.2,0.8,1.1,1.1,1.1,1.8c0,1.4-0.9,1.6-2.4,2.9c-0.3,0.2-0.8,0.3-1.1,0.6
            c-0.7,0.4-1.1,1-1.6,1.4c-0.5,0.4-1.3,0.8-2,1.2c-1.1,0.5-3.1,1.1-3.4,1.1c-1.6,0-2.4-1-3.6-2.2c-0.1-0.1-0.1-0.4-0.2-0.5
            c-0.1-0.3-0.5-0.4-0.6-0.7c-0.3-0.8-0.1-1.7-0.3-2.4c0-0.4-0.5-0.9-0.5-1.1c0-0.6,2.1-0.8,2.2-0.8c0.3,0,0.4,0.1,0.7,0.2
            c2.1,1.1,1.7,1.6,3.4,1.6c1.3,0,2.4-0.7,2.4-2.1c0-1.2-0.5-1.1-0.8-2.4c-0.2-0.8-0.3-1.7-0.7-2.4c-0.3-0.8-1-1.7-1.2-2.5
            c-0.3-0.8-0.3-1.8-0.7-2.5c-0.2-0.5-0.4-0.8-0.7-1.1c-0.8-0.8-1.8-1.9-2.9-1.9c-0.9,0-2.3,1.3-3.8,1.3c-0.6,0-1.8-0.3-2.2-0.6
            c-0.4-0.2-0.6-0.7-0.9-0.9c-0.3-0.1-0.7-0.1-0.9-0.2c-0.3-0.1-0.3-0.5-0.6-0.7c-0.2-0.2-0.6-0.1-0.8-0.3c-0.4-0.4-0.9-0.9-0.9-1.4
            c-0.2-2.7-0.1-2.9-0.1-3.1c0-0.3,0.8-0.9,1.3-1.4c0.1-0.2,0.5-0.1,0.8-0.3c0.2-0.1,0.2-0.5,0.5-0.6c0.4-0.3,1.2-0.8,1.8-1.1
            c0.4-0.3,1-0.2,1.5-0.4c0.1,0,0.4-0.4,0.7-0.5c0.3-0.2,0.9-0.4,1.4-0.6c0.1,0,0.3-0.2,0.4-0.3c0.3-0.1,0.7-0.3,1-0.3
            c1.9-0.3,3.8,0,5.7-0.5c1.8-0.4,2.2-0.6,3.3-0.6C374.2,32.1,375,32.5,376.3,34.5z"/>
            <polygon class="st18" points="353.3,221.6 352,228.7 360.9,228.7 362.3,221.2 		"/>
            <polygon class="st19" points="375.4,150.1 412.7,187.2 410.6,189.3 373.3,152.2 		"/>
            <path class="st20" d="M332.6,233.9l-2.3-1.3c0,0,1.8-2.1,6.5-3.5c4.4-1.3,12.2-6.6,12.2-6.6l2.1,3.6c0,0,0.4,2,2.4,2h5.3l3.8-2.5
            v7.5L332.6,233.9z"/>
            <path class="st21" d="M330.2,232.8c0,0,0.7,3.3,3.2,3.3H360c0,0,2.8-0.6,3-1.8c0.2-1.2-0.5-1.8-1.4-1.8h-28.1
            C333.5,232.5,330.4,232.4,330.2,232.8z"/>
            <polygon class="st0" points="343.7,225.8 344.9,228.1 343.4,229 342.3,226.6 		"/>
            <polygon class="st0" points="345.9,224.5 347.2,226.8 345.7,227.6 344.5,225.3 		"/>
            <path class="st18" d="M398.6,219l-0.3,4.9c0,0,1.8,1.7,2,1.7c0.2,0,6.4-2.3,6.5-2.6c0.1-0.3,1.6-2.7,1.6-2.7l-2.8-4.6L398.6,219z"
            />
            <polygon class="st0" points="348,223.1 349.5,225.6 347.9,226.4 346.6,224 		"/>
            <path class="st22" d="M357.5,130l-1,25.7l-2.1,40.9l-1.5,27.5h9.7l4.8-23c0,0,3-15.5,2.4-18.8c-0.6-3.3,7.6,8.8,7.6,8.8l20.3,29.7
            l9.1-4.8l-13.6-26.7l-11.8-16.4c0,0,1.8-26.9,0.3-34.2c3.2-3.1,3.4-6.5,3.4-6.5C374.2,127.8,357.5,130,357.5,130z"/>
            <path class="st23" d="M381.7,137.6c0,0.3-0.2,0.5-0.5,0.7c-0.5,0.3-1.1,1-1.6,1.5c-0.6,0.7-1.6,1.2-1.9,2.1
            c-1.6,5.8-2.5,11.9-3.7,17.8l-0.8,3.9c-0.2,1.1-0.4,2.1-0.6,3.2c-0.2,0.9-0.4,1.9-0.6,2.9c-0.2,0.8-0.3,1.6-0.5,2.4
            c-0.1,0.8-0.3,1.5-0.5,2.3c-0.1,0.4-0.2,1-0.3,1.4c-0.1,0.5-0.2,1.1-0.4,1.7c-0.1,0.5-0.2,1.1-0.4,1.7c-0.1,0.4-0.2,0.9-0.3,1.3
            c-0.1,0.5-0.2,1.2-0.9,1.2c-0.5,0-0.7-0.4-0.7-0.8c0-0.1,3-14.9,4.6-22.3c1.2-5.7,2.4-11.5,3.6-17.2c0-0.2,0.4-0.6,0.6-0.9
            c0.4-0.4,3.1-3.7,4.1-3.7C381.4,136.8,381.7,137.2,381.7,137.6z"/>
            <path class="st23" d="M371.1,131c4.1,0.2,8.1,0.7,12.2,2.1v0.1c-0.2,0.7-0.5,1.5-1,2.1c-0.3,0.5-0.7,1.2-1.2,1.7
            c-0.7,0.8-0.9,1-0.9,1.7c0,0.1,0.2,1.3,0.2,2c0.1,0.9,0.2,1.8,0.2,3.1l0.1,3.9l-0.1,7.4l-0.2,6l-0.2,4.8l-0.4,7c0,1,1.1,2,1.7,2.9
            c3.4,4.8,7,9.4,10.3,14.3c0.9,1.2,1.4,2.7,2.1,4c3.6,7.1,7.2,14.2,10.8,21.2l-6.5,3.4L384.7,199c-3.4-4.9-7.6-11.3-10.1-14.6
            c-0.6-0.8-1.3-1.7-2-2.5c-1-1.1-1.6-1.8-2.6-1.8c-1.1,0-1.7,0.9-1.7,1.8l0.1,1.4c0,0.2,0,0.3,0,0.9c-0.5,6.5-1.8,13.3-3,19.1
            c-1.3,6.3-2.6,12.7-4,19.1h-6.9l2.2-41.3l1.3-24.8l1-25.1l4.6-0.3l2.6-0.1L371.1,131z M369.2,129.4c0.8,0,0.8,0.1,1.2,0.1
            c0.2,0,0.3,0,0.7,0c0.9,0,1.7,0.1,2.6,0.2c0.6,0.1,1.3,0.1,1.9,0.2c0.5,0.1,1.1,0.1,1.6,0.2c2.4,0.4,4.9,0.9,7.2,1.8
            c0.2,0.1,0.6,0.2,0.6,0.3c0,0.1-0.1,0.9-0.3,1.3c-0.1,0.3-0.2,0.6-0.3,1c-0.1,0.4-0.4,0.9-0.6,1.3c-0.1,0.2-0.2,0.4-0.3,0.6
            c-0.2,0.4-0.5,0.7-0.8,1.1c-0.3,0.4-1.1,1.2-1.1,1.2c0,0.1,0.2,1.1,0.2,1.7c0.1,0.9,0.2,2,0.2,2.9l0.1,3v7.3l-0.2,6.6l-0.2,4.5
            l-0.5,8c0,0.1,0,0.1,0,0.2c0.6,0.9,1.3,1.8,1.9,2.6c3.3,4.6,6.8,9,9.9,13.7c1,1.6,1.7,3.4,2.6,5.1l11,21.6v0.1l-9.1,4.8
            c-4.5-6.6-9-13.1-13.5-19.7c-1.8-2.6-3.6-5.3-5.4-7.9c-0.1-0.2-7.6-11.5-8.9-11.5c-0.2,0-0.2,0.3-0.2,0.3c0,0.1,0.1,1.3,0.1,1.4
            l0,0.9c-0.1,2.7-0.6,5.4-1,8.1c-0.6,3.9-1.3,7.9-2.1,11.8c-1.4,6.6-2.8,13.3-4.2,19.9h-9.7c0,0,0,0,0-0.1l2.3-42.6l1.2-24.4l1-27
            c1.8-0.2,3.2-0.4,5.6-0.4l2.1-0.1H369.2z"/>
            <path class="st23" d="M385.1,232.1c0,0-3.7,1.4-3.4,1.8c0.3,0.4,2.4,2.2,2.4,2.2h9.8l18.5-8c0,0,1.7-1.2,0.8-1.7
            c-0.9-0.5-1.5-1.7-1.5-1.7L385.1,232.1z"/>
            <path class="st20" d="M399.7,221.9l-2.1,2.2l-1-2l-5,5.9c0,0-5.9,2.9-8.3,4.4c-0.7,0.4-1.1,0.7-1,0.8c0.7,0.1,12.8-0.4,12.8-0.4
            l16.8-7.7l-2.2-4.8L399.7,221.9z"/>
            <path class="st24" d="M458,229.2l19.6-20.4l-32.3-32c0,0-13.6-16.1-30.9-10.4c-1.8,0.6-3.1,2.1-4.5,2.7c-3.9,3.7-7.9,7.5-10,9.3
            c1.1,1.2,1.8,2,1.8,2l48.8,48.8H458z"/>
            <polygon class="st25" points="414.4,166.4 467.5,219.4 464.5,222.3 410.7,168.7 		"/>
            <polygon class="st23" points="413.1,167.1 466.4,220.4 465.5,221.3 412.1,168 		"/>
            <polygon class="st23" points="402.1,175 409.4,168.2 410.2,169.1 403,175.8 		"/>
            <polygon class="st0" points="391,228.4 392.9,229.9 394,228.6 392.3,227.3 		"/>
            <polygon class="st0" points="392.8,226.5 394.7,228 395.8,226.7 394,225.2 		"/>
            <polygon class="st26" points="394.5,224.5 396.4,226 397.5,224.7 395.7,223.2 		"/>
            <path class="st23" d="M455.5,220.2L455.5,220.2c4.3,0,7.8,3.5,7.8,7.8c0,4.3-3.5,7.8-7.8,7.8h0c-4.3,0-7.8-3.5-7.8-7.8
            C447.7,223.7,451.2,220.2,455.5,220.2z"/>
            <path class="st27" d="M455.5,223.3L455.5,223.3c2.6,0,4.7,2.1,4.7,4.7c0,2.6-2.1,4.7-4.7,4.7h0c-2.6,0-4.7-2.1-4.7-4.7
            C450.8,225.4,452.9,223.3,455.5,223.3z"/>
          </g>
          <path class="st28" d="M561,236.4c18.5,44.8,72.5,70.7,167,17.3c94.6,121.1,199.5,89.8,219.7,82.2v-99.8L561,236.4z"/>
        </g>
      </svg>
      <!-- ********************************************************************************** -->
      <!-- **************************************MOBILE************************************** -->
      <!-- ********************************************************************************** -->
      <img src="assets/themes/busplus/dist/img/ilustracion-mobile.svg" alt="hero-mobile" class="mobile">
    </div>
  </section>
  <!-- Section 2 -->
  <section id="section2">
    <div class="container cont-s2">
      <?php
  $promociones = get_field('promociones');
  if ($promociones): ?>
      <?php foreach ($promociones as $post):
          setup_postdata($post);
          if (get_field('link')) {
              $link = get_field('link');
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
          } else {
              $link_url = esc_url(home_url('/'))."promociones";
              $link_title = get_the_title();
          } ?>
          <a class="cont-s2-img <?php if (get_field('doble_ancho')) {
              echo " doble";
          } ?>" href="<?php echo $link_url; ?>" target="<?php echo esc_attr($link_target); ?>" title="<?php echo esc_html($link_title); ?>"><?php echo get_the_post_thumbnail(get_the_ID(), 'large'); ?></a>
      <?php endforeach; ?>
      <?php
      wp_reset_postdata(); ?>
  <?php endif; ?>
    </div>
    <div class="btn" >
      <a href="<?php echo esc_url(home_url('/')); ?>promociones">VER MÁS</a>
    </div>
    <!-- ARCOS -->
    <svg class="svg-arcos" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 1555 563" style="enable-background:new 0 0 1555 563;" xml:space="preserve">
    <style type="text/css">
    .st0-b{fill-rule:evenodd;clip-rule:evenodd;fill:#7C51A1;}
    .st1-b{fill-rule:evenodd;clip-rule:evenodd;fill:#DC272D;}
    .st2-b{fill-rule:evenodd;clip-rule:evenodd;fill:#C0BC00;}
    </style>
    <g>
      <path class="st0-b" d="M194,259.8l34.6-19.8c0,0,107.8,156.9,254.1,143.8c-1.3,22.4,1.3,46.2,1.3,46.2
      C378.9,432.6,253.9,373.2,194,259.8z"/>
      <path class="st1-b" d="M0,210.5L42.6,235c0,0,132.6-194.1,312.8-177.8C353.7,29.5,357,0.1,357,0.1C227.6-3.2,73.7,70.3,0,210.5z"/>
      <path class="st2-b" d="M1198,538.5l42.6,24.5c0,0,132.6-194.1,312.8-177.8c-1.6-27.7,1.6-57.1,1.6-57.1
      C1425.6,324.8,1271.7,398.3,1198,538.5z"/>
    </g>
  </svg>
  <!-- LANDSCAPE -->
  <svg class="svg-1" xmlns="http://www.w3.org/2000/svg" width="1471.738" height="183.943" viewBox="0 0 1471.738 183.943">
    <g id="Group_694" data-name="Group 694" transform="translate(0 -1102.057)">
      <rect id="Rectangle" width="50.09" height="144.48" transform="translate(1041.529 1141.24)" fill="#e0f1e9"/>
      <rect id="Rectangle-2" data-name="Rectangle" width="25" height="145" transform="translate(1054 1140)" fill="#e0f1e9" opacity="0.502"/>
      <g id="Group_120" data-name="Group 120">
        <rect id="Rectangle-3" data-name="Rectangle" width="24.59" height="144.48" transform="translate(1041.279 1141.24)" fill="#e0f1e9" opacity="0.502"/>
      </g>
      <path id="Path" d="M356.108,1223.211c-8.077,6.97-38.134,37.46-37.916,37.707l.032.026a.1.1,0,0,1-.032-.026l-35.52-29s-27.884-25.85-54.786-9.869c-5.242,3.114-37,18.217-37,18.217L176.6,1233.66s-21.7-16.185-54.2,13.932c-38.114,31.235-75.924,26.292-92.087,14.513S0,1233.66,0,1233.66v51.665H652.276l-69.448-77.208s-17.7-25.434-36.718-5.225l-54.955,47.753-55.2-31.5S397.893,1187.154,356.108,1223.211Z" fill="#ffeed6"/>
      <g id="Group_111" data-name="Group 111">
        <rect id="Rectangle-4" data-name="Rectangle" width="12.22" height="21.48" transform="translate(35.199 1264.02)" fill="#b3e3c8"/>
        <path id="Path-2" data-name="Path" d="M40.727,1162.746,64,1268.4H18.618Z" fill="#b3e3c8"/>
      </g>
      <g id="Group_113" data-name="Group 113">
        <rect id="Rectangle-5" data-name="Rectangle" width="12.22" height="21.48" transform="translate(448.289 1264.02)" fill="#b3e3c8"/>
        <path id="Path-3" data-name="Path" d="M453.818,1162.746,477.091,1268.4H431.709Z" fill="#b3e3c8"/>
      </g>
      <g id="Group_112" data-name="Group 112">
        <rect id="Rectangle-6" data-name="Rectangle" width="10.03" height="17.66" transform="translate(139.859 1267.42)" fill="#b3e3c8"/>
        <path id="Path-4" data-name="Path" d="M144.4,1184.806l19.1,86.862H126.255Z" fill="#b3e3c8"/>
      </g>
      <g id="Group_114" data-name="Group 114">
        <rect id="Rectangle-7" data-name="Rectangle" width="10.03" height="17.66" transform="translate(508.15 1267.42)" fill="#b3e3c8"/>
        <path id="Path-5" data-name="Path" d="M512.686,1184.806l19.1,86.862H494.546Z" fill="#b3e3c8"/>
      </g>
      <path id="Path-6" data-name="Path" d="M185.342,1172.615a17.809,17.809,0,0,0-17.6,20.319h70.8a13.34,13.34,0,0,0-19.08-10.064,17.786,17.786,0,0,0-20.147-3.467A17.748,17.748,0,0,0,185.342,1172.615Z" fill="#fcfcfd"/>
      <path id="Path-7" data-name="Path" d="M377.418,1179.87h57.018s-1.137-6.323-6.982-5.225c-2.472-2.271-3.061-4.367-7.564-3.483-1.462-3.147-.79-4.572-6.982-7.547-5.149-2.222-10.131,2.961-9.891,4.644-1.559-1.837-6.431-.531-6.982,2.9-2.894-.548-7.179.793-9.027,3.224C382.389,1173.8,378.78,1173.318,377.418,1179.87Z" fill="#fcfcfd"/>
      <path id="Path-8" data-name="Path" d="M925.618,1173.115h57.018s-1.137-6.323-6.982-5.225c-2.472-2.271-3.06-4.367-7.564-3.483-1.462-3.147-.79-4.572-6.982-7.547-5.149-2.222-10.131,2.961-9.891,4.644-1.559-1.837-6.431-.531-6.982,2.9-2.894-.548-7.179.793-9.027,3.224C930.589,1167.044,926.981,1166.563,925.618,1173.115Z" fill="#fcfcfd"/>
      <path id="Path-9" data-name="Path" d="M1206.618,1183.115h57.018s-1.137-6.323-6.982-5.225c-2.473-2.271-3.061-4.367-7.564-3.483-1.462-3.147-.79-4.572-6.982-7.547-5.149-2.222-10.131,2.961-9.891,4.644-1.559-1.837-6.431-.531-6.982,2.9-2.894-.548-7.179.793-9.027,3.224C1211.589,1177.044,1207.98,1176.564,1206.618,1183.115Z" fill="#fcfcfd"/>
      <path id="Path-10" data-name="Path" d="M865,1198H778s1.735-9.785,10.653-8.086c3.772-3.515,4.67-6.759,11.541-5.391,2.231-4.87,1.2-7.076,10.653-11.68,7.857-3.439,15.458,4.583,15.092,7.188,2.379-2.843,9.813-.821,10.653,4.492,4.416-.849,10.954,1.227,13.774,4.989C857.416,1188.6,862.921,1187.86,865,1198Z" fill="#fcfcfd"/>
      <path id="Path-11" data-name="Path" d="M1348,1198h87s-1.735-9.785-10.653-8.086c-3.772-3.515-4.67-6.759-11.541-5.391-2.23-4.87-1.2-7.076-10.653-11.68-7.857-3.439-15.458,4.583-15.092,7.188-2.379-2.843-9.813-.821-10.653,4.492-4.416-.849-10.954,1.227-13.774,4.989C1355.584,1188.6,1350.078,1187.86,1348,1198Z" fill="#fcfcfd"/>
      <g id="Group_119" data-name="Group 119">
        <path id="Path-12" data-name="Path" d="M984.565,1143.531,978.581,1130l-5.781,13.548-2.157,3.717-.582,25.842-2.095,67.989L967,1284h24l-.972-42.9-2.107-67.989-.565-24.933Z" fill="#deefe7"/>
      </g>
      <g id="Group_121" data-name="Group 121">
        <path id="Path-13" data-name="Path" d="M1093.446,1210.3s-.911-1.817-1.822-1.817-95.635,61.791-95.635,61.791h143v-4.543h-39.165s-36.432.454-58.292-7.27C1065.666,1237.561,1093.446,1210.3,1093.446,1210.3Z" fill="#fff"/>
      </g>
      <path id="Path-14" data-name="Path" d="M977.773,1268.456a14.54,14.54,0,0,0-14.459,16.356h77.273a12.764,12.764,0,0,0-18.207-10.577,22.329,22.329,0,0,0-43.555-5.743Q978.3,1268.455,977.773,1268.456Z" fill="#b3e3c8"/>
      <g id="Group_115" data-name="Group 115">
        <path id="Path-15" data-name="Path" d="M681.773,1268.456a14.54,14.54,0,0,0-14.459,16.356h77.273a12.764,12.764,0,0,0-18.207-10.577,22.329,22.329,0,0,0-43.555-5.743Q682.3,1268.455,681.773,1268.456Z" fill="#c8e9d7"/>
        <path id="Path-16" data-name="Path" d="M671.7,1272.143a11.364,11.364,0,0,1,11.3,11.428,11.679,11.679,0,0,1-.088,1.429H623a9.935,9.935,0,0,1,9.862-9.286,9.753,9.753,0,0,1,4.255.971,17.275,17.275,0,0,1,33.769-4.514Q671.29,1272.142,671.7,1272.143Z" fill="#b3e3c8"/>
      </g>
      <line id="Path-17" data-name="Path" x2="14.58" y2="23.63" transform="translate(1046.99 1242.1)" fill="#b2b200" opacity="0.502"/>
      <line id="Path-18" data-name="Path" x2="16.4" y2="24.53" transform="translate(1054.279 1241.2)" fill="#b2b200"/>
      <line id="Path-19" data-name="Path" x2="16.39" y2="25.44" transform="translate(1062.48 1240.29)" fill="#b2b200"/>
      <line id="Path-20" data-name="Path" x2="21.4" y2="32.71" transform="translate(1065.67 1233.02)" fill="#b2b200"/>
      <line id="Path-21" data-name="Path" x2="24.14" y2="36.8" transform="translate(1072.039 1228.93)" fill="#b2b200"/>
      <line id="Path-22" data-name="Path" x2="28.24" y2="42.71" transform="translate(1076.141 1223.02)" fill="#b2b200" opacity="0.502"/>
      <line id="Path-23" data-name="Path" x2="31.42" y2="46.8" transform="translate(1082.061 1218.93)" fill="#b2b200" opacity="0.502"/>
      <line id="Path-24" data-name="Path" x2="36.43" y2="52.7" transform="translate(1086.16 1213.03)" fill="#b2b200" opacity="0.502"/>
      <path id="Composite_Path" data-name="Composite Path" d="M1122.165,1268.39v17.331h3.643l0-17.456a13.3,13.3,0,0,1-2.255.191h-.048A13.361,13.361,0,0,1,1122.165,1268.39Zm-11.84-13.11a13.181,13.181,0,0,0,11.84,13.11,13.361,13.361,0,0,0,1.342.066h.048a13.176,13.176,0,1,0,0-26.352h-.048A13.179,13.179,0,0,0,1110.324,1255.28Z" fill="#b3e3c8"/>
      <g id="Group_116" data-name="Group 116">
        <path id="Composite_Path-2" data-name="Composite Path" d="M822.165,1268.39v17.331h3.643l0-17.456a13.3,13.3,0,0,1-2.255.191h-.048A13.361,13.361,0,0,1,822.165,1268.39Zm-11.841-13.11a13.181,13.181,0,0,0,11.841,13.11,13.361,13.361,0,0,0,1.342.066h.048a13.176,13.176,0,1,0,0-26.352h-.048A13.179,13.179,0,0,0,810.324,1255.28Z" fill="#b3e3c8"/>
      </g>
      <g id="Group_117" data-name="Group 117">
        <path id="Composite_Path-3" data-name="Composite Path" d="M874.483,1260.569V1286h5.379l0-25.614a19.733,19.733,0,0,1-3.329.281h-.071Q875.461,1260.667,874.483,1260.569ZM857,1241.333a19.387,19.387,0,0,0,17.483,19.236q.977.1,1.982.1h.071a19.733,19.733,0,0,0,3.329-.281A19.326,19.326,0,0,0,876.536,1222h-.071A19.4,19.4,0,0,0,857,1241.333Z" fill="#c8e9d7"/>
      </g>
      <g id="Group_118" data-name="Group 118">
        <path id="Composite_Path-4" data-name="Composite Path" d="M928.165,1268.39v17.331h3.643l0-17.456a13.3,13.3,0,0,1-2.255.191h-.048A13.361,13.361,0,0,1,928.165,1268.39Zm-11.841-13.11a13.181,13.181,0,0,0,11.841,13.11,13.361,13.361,0,0,0,1.342.066h.048a13.176,13.176,0,1,0,0-26.352h-.048A13.179,13.179,0,0,0,916.324,1255.28Z" fill="#b3e3c8"/>
      </g>
      <path id="Composite_Path-5" data-name="Composite Path" d="M1457.165,1268.39v17.331h3.643l0-17.456a13.3,13.3,0,0,1-2.255.191h-.048A13.358,13.358,0,0,1,1457.165,1268.39Zm-11.841-13.11a13.181,13.181,0,0,0,11.841,13.11,13.358,13.358,0,0,0,1.342.066h.048a13.176,13.176,0,1,0,0-26.352h-.048A13.179,13.179,0,0,0,1445.324,1255.28Z" fill="#b3e3c8"/>
      <g id="Group_122" data-name="Group 122">
        <path id="Path-25" data-name="Path" d="M1133.522,1285.611V1109.326l61.024-7.269v183.554Z" fill="#e0f1e9"/>
        <path id="Path-26" data-name="Path" d="M1207.3,1210.989l25.5,10v63.608h-39.165v-79.964Z" fill="#e0f1e9"/>
      </g>
      <path id="Path-27" data-name="Path" d="M1215.584,1164.756s.9,119.946-1,119.946h-64.667V1164.756Z" fill="#b3e3c8"/>
      <g id="Group_692" data-name="Group 692" transform="translate(15.713 1073)">
        <path id="Path_763" data-name="Path 763" d="M429.31,174.8l-3.113-6.537s-1.868.1,0,3.735,3.735,7.782,3.735,7.782.1,1.348,1.868.934Z" transform="translate(-67.785 -0.705)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_764" data-name="Path 764" d="M436.462,195.243l1.245,8.715-1.556-.934-1.245-6.225-1.556-6.847s2.178.311,2.178.622S436.462,195.243,436.462,195.243Z" transform="translate(-70.268 -7.451)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_765" data-name="Path 765" d="M213.056,166H349.386s6.748-.193,9.026,1.868a3.147,3.147,0,0,0-.934,1.867l5.291,10.272h.934l1.556,3.423-2.178-.622L365.26,193.4s-.426,1.771,1.868,3.112c.184,2.107,0,7.782,0,7.782s-1.177,4.124-3.424,4.358-19.92,0-19.92,0v-4.669a5.943,5.943,0,0,0-6.225-4.98,5.749,5.749,0,0,0-6.225,4.358v5.291h-69.41v-3.735c-.041-1.787-1.9-5.6-6.847-5.6s-6.225,4.98-6.225,4.98h-1.245a6.381,6.381,0,0,0-12.45,0v4.669H224.261l-13.384-1.245s-3.423-.859-3.423-6.225V175.653C208.257,168.685,207.778,166,213.056,166Z" transform="translate(0 0)" fill="#996dbf" fill-rule="evenodd"/>
        <circle id="Ellipse_151" data-name="Ellipse 151" cx="5.758" cy="5.758" r="5.758" transform="translate(235.678 199.953)" fill="#4a5568" style="isolation: isolate"/>
        <circle id="Ellipse_152" data-name="Ellipse 152" cx="5.758" cy="5.758" r="5.758" transform="translate(249.685 199.953)" fill="#4a5568" style="isolation: isolate"/>
        <circle id="Ellipse_153" data-name="Ellipse 153" cx="5.758" cy="5.758" r="5.758" transform="translate(331.545 199.953)" fill="#4a5568" style="isolation: isolate"/>
        <circle id="Ellipse_154" data-name="Ellipse 154" cx="3.424" cy="3.424" r="3.424" transform="translate(238.013 202.288)" fill="#f3f3f3"/>
        <circle id="Ellipse_155" data-name="Ellipse 155" cx="3.424" cy="3.424" r="3.424" transform="translate(252.019 202.288)" fill="#f3f3f3"/>
        <circle id="Ellipse_156" data-name="Ellipse 156" cx="3.424" cy="3.424" r="3.424" transform="translate(333.879 202.288)" fill="#f3f3f3"/>
        <path id="Path_766" data-name="Path 766" d="M364.584,207.005H297.976a4.269,4.269,0,0,1-4.358-1.868c-1.974-2.546-4.669-5.914-4.669-5.914s-2.368-3.424,2.178-3.424h65.986s1.712-.513,3.424,1.556,4.98,6.537,4.98,6.537S367.205,206.909,364.584,207.005Z" transform="translate(-25.159 -9.252)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_767" data-name="Path 767" d="M349.827,167.354H215.054s-2.232-.364-2.179,2.179v9.649H335.82s7.56.28,9.026,4.98c4.482,5.852,9.961,12.761,9.961,12.761s.852,2.179,5.6,2.179h3.113a2.385,2.385,0,0,0,2.8-2.49,55.085,55.085,0,0,0-.623-6.848l-2.178-8.715-4.358-9.96S358.766,167.354,349.827,167.354Z" transform="translate(-1.686 -0.416)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_768" data-name="Path 768" d="M373.8,195.17c.7-.078,2.256.389,2.256,1.478v20.543h-8.871V196.338c0-.467.856-1.323,1.168-1.168Zm-5.136-.7,5.447.078c1.089,0,2.646,1.011,2.646,2.256v20.7c-.078.156-.234.234-.311.311H366.8c-.156-.078-.234-.234-.311-.311V196.648A2.127,2.127,0,0,1,368.662,194.47Z" transform="translate(-49.469 -8.856)" fill="#996dbf" fill-rule="evenodd"/>
      </g>
      <g id="Group_693" data-name="Group 693" transform="translate(1261 1239)">
        <path id="Path_763-2" data-name="Path 763" d="M427.857,174.8l3.113-6.537s1.868.1,0,3.735-3.735,7.782-3.735,7.782-.1,1.348-1.868.934Z" transform="translate(-421.943 -166.705)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_764-2" data-name="Path 764" d="M434.594,195.243l-1.245,8.715,1.556-.934,1.245-6.225,1.556-6.847s-2.178.311-2.178.622S434.594,195.243,434.594,195.243Z" transform="translate(-433.349 -173.451)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_765-2" data-name="Path 765" d="M361.608,166H225.277s-6.748-.193-9.027,1.868a3.147,3.147,0,0,1,.934,1.867l-5.291,10.272h-.934l-1.556,3.423,2.178-.622L209.4,193.4s.426,1.771-1.868,3.112c-.184,2.107,0,7.782,0,7.782s1.177,4.124,3.424,4.358,19.92,0,19.92,0v-4.669a5.943,5.943,0,0,1,6.225-4.98,5.749,5.749,0,0,1,6.225,4.358v5.291h69.41v-3.735c.041-1.787,1.9-5.6,6.847-5.6s6.225,4.98,6.225,4.98h1.245a6.381,6.381,0,0,1,12.451,0v4.669H350.4l13.384-1.245s3.423-.859,3.423-6.225V175.653C366.408,168.685,366.887,166,361.608,166Z" transform="translate(-207.224 -166)" fill="#996dbf" fill-rule="evenodd"/>
        <ellipse id="Ellipse_151-2" data-name="Ellipse 151" cx="5.758" cy="5.758" rx="5.758" ry="5.758" transform="translate(120.245 33.953)" fill="#4a5568" style="isolation: isolate"/>
        <ellipse id="Ellipse_152-2" data-name="Ellipse 152" cx="5.758" cy="5.758" rx="5.758" ry="5.758" transform="translate(106.239 33.953)" fill="#4a5568" style="isolation: isolate"/>
        <ellipse id="Ellipse_153-2" data-name="Ellipse 153" cx="5.758" cy="5.758" rx="5.758" ry="5.758" transform="translate(24.378 33.953)" fill="#4a5568" style="isolation: isolate"/>
        <ellipse id="Ellipse_154-2" data-name="Ellipse 154" cx="3.424" cy="3.424" rx="3.424" ry="3.424" transform="translate(122.58 36.288)" fill="#f3f3f3"/>
        <ellipse id="Ellipse_155-2" data-name="Ellipse 155" cx="3.424" cy="3.424" rx="3.424" ry="3.424" transform="translate(108.573 36.288)" fill="#f3f3f3"/>
        <ellipse id="Ellipse_156-2" data-name="Ellipse 156" cx="3.424" cy="3.424" rx="3.424" ry="3.424" transform="translate(26.712 36.288)" fill="#f3f3f3"/>
        <path id="Path_766-2" data-name="Path 766" d="M289.8,207.005H356.41a4.269,4.269,0,0,0,4.358-1.868c1.974-2.546,4.669-5.914,4.669-5.914s2.369-3.424-2.178-3.424H297.271s-1.712-.513-3.424,1.556-4.98,6.537-4.98,6.537S287.18,206.909,289.8,207.005Z" transform="translate(-261.788 -175.252)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_767-2" data-name="Path 767" d="M229.37,167.354H364.145s2.232-.364,2.179,2.179v9.649H243.377s-7.56.28-9.027,4.98c-4.482,5.852-9.961,12.761-9.961,12.761s-.852,2.179-5.6,2.179h-3.113a2.385,2.385,0,0,1-2.8-2.49,55.084,55.084,0,0,1,.623-6.848l2.178-8.715,4.358-9.96S220.431,167.354,229.37,167.354Z" transform="translate(-210.072 -166.416)" fill="#7c51a1" fill-rule="evenodd" style="isolation: isolate"/>
        <path id="Path_768-2" data-name="Path 768" d="M369.441,195.17c-.7-.078-2.256.389-2.256,1.478v20.543h8.871V196.338c0-.467-.856-1.323-1.168-1.168Zm5.136-.7-5.448.078c-1.089,0-2.646,1.011-2.646,2.256v20.7c.078.156.234.234.311.311h9.649c.156-.078.234-.234.311-.311V196.648A2.127,2.127,0,0,0,374.577,194.47Z" transform="translate(-326.332 -174.856)" fill="#996dbf" fill-rule="evenodd"/>
      </g>
    </g>
  </svg>                <style type="text/css">
  .st0-c{fill-rule:evenodd;clip-rule:evenodd;fill:#E0F1E9;}
  .st1-c{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
  .st2-c{fill-rule:evenodd;clip-rule:evenodd;fill:#FCFCFD;}
  .st3-c{fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#FFFFFF;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
  .st4-c{fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
  .st5-c{fill-rule:evenodd;clip-rule:evenodd;fill:#C8E9D7;}
  .st6-c{fill-rule:evenodd;clip-rule:evenodd;fill:#B3E3C8;}
  .st7-c{fill-rule:evenodd;clip-rule:evenodd;fill:#F3F3F3;}
  </style>
  <path class="st0-c" d="m1040 39.2v145.2h50.1v-145.2h-50.1zm-57.4 4.3-6-13.5-5.8 13.5-2.2 3.7-0.6 25.9-2.1 68-1 42.9h24l-1-42.9-2.1-68-0.6-24.9-2.6-4.7zm149.4 141 61.5-0.5v0.4h38.5v-64.2l-25.7-10-12.8-5.9v-104.3l-61.5 7.3v177.2zm-775.9-62.3c-8.1 7-38.1 37.5-37.9 37.7l-35.5-29s-27.9-25.8-54.8-9.9c-5.2 3.1-38 18.2-38 18.2l-13.3-6.6s-21.7-16.2-54.2 13.9c-38.1 31.2-75.9 26.3-92.1 14.5-16.2-11.7-30.3-28.3-30.3-28.3v51.7l651.3 0.1-68.4-77.3s-17.7-25.4-36.7-5.2l-55 47.8-55.2-31.6s-38.1-32-79.9 4z"/><rect class="st1-c" x="1052" y="38.7" width="1" height="144"/><rect class="st1-c" x="1065" y="38.7" width="1" height="144"/><rect class="st1-c" x="1076" y="38.7" width="1" height="144"/><path class="st2-c" d="m375.4 76.6h57s-1.1-6.3-7-5.2c-2.5-2.3-3.1-4.4-7.6-3.5-1.5-3.2-0.8-4.6-7-7.6-5.1-2.2-10.1 3-9.9 4.7-1.6-1.8-6.4-0.5-7 2.9-2.9-0.6-7.2 0.8-9 3.2-4.5-0.6-8.1-1-9.5 5.5zm-192.1-7.3c-9.8 0-17.8 8-17.8 17.8 0 0.9 0.1 1.7 0.2 2.5h70.8c-0.9-6.5-6.5-11.4-13.2-11.4-2.1 0-4.1 0.5-5.9 1.4-3.2-3.2-7.6-5.2-12.5-5.2-2.7 0-5.3 0.6-7.6 1.7-3.2-4.1-8.3-6.8-14-6.8zm740.3 0.6h57s-1.1-6.3-7-5.2c-2.5-2.3-3.1-4.4-7.6-3.5-1.5-3.2-0.8-4.6-7-7.6-5.1-2.2-10.1 3-9.9 4.6-1.6-1.8-6.4-0.5-7 2.9-2.9-0.6-7.2 0.8-9 3.2-4.5-0.5-8.1-1-9.5 5.6zm281 10h57s-1.1-6.3-7-5.2c-2.5-2.3-3.1-4.4-7.6-3.5-1.5-3.2-0.8-4.6-7-7.6-5.2-2.2-10.1 3-9.9 4.6-1.6-1.8-6.4-0.5-7 2.9-2.9-0.6-7.2 0.8-9 3.2-4.5-0.5-8.1-1-9.5 5.6zm-341.6 14.8c-2.1-10.1-7.6-9.4-14.6-8.5-2.8-3.8-9.4-5.8-13.8-5-0.8-5.3-8.3-7.3-10.7-4.5 0.4-2.6-7.2-10.6-15.1-7.2-9.4 4.6-8.4 6.8-10.7 11.7-6.9-1.4-7.8 1.9-11.5 5.4-8.9-1.7-10.7 8.1-10.7 8.1h87.1zm483 0h87s-1.7-9.8-10.7-8.1c-3.8-3.5-4.7-6.8-11.5-5.4-2.2-4.9-1.2-7.1-10.7-11.7-7.8-3.4-15.5 4.6-15.1 7.2-2.4-2.8-9.8-0.8-10.7 4.5-4.4-0.8-11 1.2-13.8 5-6.9-0.9-12.4-1.6-14.5 8.5zm-254.6 12.3s-0.9-1.8-1.8-1.8-95.6 61.8-95.6 61.8h143v-4.5h-39.2s-36.4 0.4-58.3-7.3c24.2-20.9 51.9-48.2 51.9-48.2z"/><line class="st3-c" x1="1045" x2="1059.6" y1="138.8" y2="162.5"/><line class="st3-c" x1="1052.3" x2="1068.7" y1="137.9" y2="162.5"/><line class="st3-c" x1="1060.5" x2="1076.9" y1="137" y2="162.5"/><line class="st3-c" x1="1063.7" x2="1085.1" y1="129.8" y2="162.5"/><line class="st3-c" x1="1070" x2="1094.2" y1="125.7" y2="162.5"/><line class="st4-c" x1="1074.1" x2="1102.4" y1="119.8" y2="162.5"/><line class="st4-c" x1="1080.1" x2="1111.5" y1="115.7" y2="162.5"/><line class="st4-c" x1="1084.2" x2="1120.6" y1="109.8" y2="162.5"/><path class="st5-c" d="m679.8 167.2c-8.1 0-14.6 6.5-14.6 14.5 0 0.6 0 2.1 0.1 2.7h77.3c-0.5-6.6-6-12.7-12.7-12.7-2 0-3.8 0.4-5.5 1.2-0.5-11.8-10.3-21.2-22.3-21.2-10 0-18.4 6.5-21.3 15.5h-1zm175.2-28.1c0 10 7.7 18.2 17.5 19.2v26.2h5.4v-26.3c9.2-1.6 16.1-9.5 16.1-19.1 0-10.7-8.7-19.3-19.5-19.3h-0.1c-10.7-0.1-19.4 8.6-19.4 19.3z"/><path class="st5-c" d="m1412.7 138.8h-136.9s-6.8-0.2-9.1 1.9c0.9 0.9 0.9 1.9 0.9 1.9l-5.3 10.3h-0.9l-1.6 3.4 2.2-0.6-2.2 10.6s0.4 1.8-1.9 3.1c-0.2 2.1 0 7.8 0 7.8s1.2 4.1 3.4 4.4c2.2 0.2 20 0 20 0v-4.6s1.7-5 7.2-5c5.3-0.3 6.2 4.4 6.2 4.4v5.3h68.7v-3.8c0-1.8 2.3-5.9 7.2-5.9 5 0 6.2 5.3 6.2 5.3h0.9s1.6-5 7.2-5 5.2 5 5.2 5v4.7h10.9l13.4-1.2s3.4-0.9 3.4-6.2v-25.9c-0.3-7.2 0.2-9.9-5.1-9.9z"/><path class="st6-c" d="m1263.6 147-2.5 5.9c1.8 0.4 1.9-0.9 1.9-0.9s1.9-4.2 3.8-7.8 0-3.8 0-3.8l-3.2 6.6zm-4.7 13.7-1.2 8.8 1.6-0.9 1.2-6.2 1.6-6.9s-2.2 0.3-2.2 0.6-1 4.6-1 4.6zm125.7 12.2c-3.2 0-5.8 2.6-5.8 5.8s2.6 5.8 5.8 5.8 5.8-2.6 5.8-5.8-2.6-5.8-5.8-5.8zm-14.1 0c-3.2 0-5.8 2.6-5.8 5.8s2.6 5.8 5.8 5.8 5.8-2.6 5.8-5.8-2.6-5.8-5.8-5.8zm-82.2 0c-3.2 0-5.8 2.6-5.8 5.8s2.6 5.8 5.8 5.8 5.8-2.6 5.8-5.8-2.6-5.8-5.8-5.8zm-11.2-33.1c-9 0-9.4 3.8-9.4 3.8l-4.4 10-2.2 8.8s-0.6 4-0.6 6.9 2.8 2.5 2.8 2.5h3.1c4.8 0 5.6-2.2 5.6-2.2s5.5-6.9 10-12.8c1.5-4.7 9.1-5 9.1-5h123.4v-9.8c0.1-2.6-2.2-2.2-2.2-2.2h-135.2zm82.5 19.7h-50.8c-0.2-2.5-3.8-1.8-5-1.8-1.5 0-4.2-0.5-5.5 1.8v0.1h-4.9s-1.7-0.5-3.4 1.6-5 6.6-5 6.6-1.7 3 0.9 3.1h12.4v10.7h9.8l0.8-0.8v-9.9h29.9 14.1s2.4 0.7 4.4-1.9 4.7-5.9 4.7-5.9 2.1-3.6-2.4-3.6zm-60.4 11.2h8.2v9.4h-8.2v-9.4zm8.3-11.3h-7.9c1-1 3.1-0.8 4.2-0.8h3.8v0.8z"/><path class="st7-c" d="m1384.6 175.2c-1.9 0-3.4 1.5-3.4 3.4s1.5 3.4 3.4 3.4 3.4-1.5 3.4-3.4c0-1.8-1.5-3.4-3.4-3.4zm-14.1 0c-1.9 0-3.4 1.5-3.4 3.4s1.5 3.4 3.4 3.4 3.4-1.5 3.4-3.4c0.1-1.8-1.5-3.4-3.4-3.4zm-82.2 0c-1.9 0-3.4 1.5-3.4 3.4s1.5 3.4 3.4 3.4 3.4-1.5 3.4-3.4c0.1-1.8-1.5-3.4-3.4-3.4z"/><path class="st6-c" d="m33.2 167.1v17.3h12.2v-17.3h16.6l-23.3-105.6-22.1 105.6h16.6zm104.7 3.3v14.1h10v-14.1h13.6l-19.1-86.9-18.1 86.9h13.6zm1305.4-16.4c0 6.8 5.2 12.4 11.8 13.1v17.3h3.7v-17.4c6.2-1.1 10.9-6.5 10.9-13 0-7.3-5.9-13.2-13.2-13.2h-0.1c-7.2 0-13.1 5.9-13.1 13.2zm-996.5 12.6v17.9h12.4v-17.9h16.8l-23.6-105.9-22.4 105.9h16.8zm59.3 2.8v15.1h10v-15.1h13.6l-19.1-86.9-18.1 86.9h13.6zm708.9-105.7h-67l0.6 120.6h67l-0.6-120.6zm-107 90c0 6.7 4.9 12.3 11.2 12.9v17.8h3.5v-17.9c5.9-1.1 10.3-6.4 10.3-12.8 0-7.2-5.6-13-12.5-13s-12.5 5.9-12.5 13zm-132.7 13.6c-7.9 0-14.3 6.6-14.3 14.6 0 0.6 0 1.9 0.1 2.6h75.9c-0.5-6.6-5.9-12.6-12.5-12.6-1.9 0-3.8 0.4-5.4 1.2-0.5-11.9-10.1-21.4-21.9-21.4-9.8 0-18.1 6.5-20.9 15.6h-1zm-305.6 3.5h-0.8c-2.2-7-8.8-12.1-16.5-12.1-9.3 0-16.8 7.4-17.3 16.6-1.3-0.6-2.7-1-4.2-1-5.2 0-9.5 4.9-9.8 10.1h59.8c0.1-0.5 0.1-1.8 0.1-2.2-0.1-6.3-5.1-11.4-11.3-11.4zm138.4-17.8c0 6.8 5.2 12.4 11.8 13.1v18.4h3.6v-18.5c6.2-1.1 10.9-6.5 10.9-12.9 0-7.3-5.9-13.1-13.2-13.1-7.2-0.1-13.1 5.8-13.1 13zm105.9 0c0 6.8 5.2 12.4 11.8 13.1v18.4h3.6v-18.5c6.2-1.1 10.9-6.5 10.9-12.9 0-7.3-5.9-13.1-13.2-13.1-7.2-0.1-13.1 5.8-13.1 13z"/><path class="st5-c" d="m226.3 138.8h136.9s6.8-0.2 9.1 1.9c-0.9 0.9-0.9 1.9-0.9 1.9l5.3 10.3h0.9l1.6 3.4-2.2-0.6 2.2 10.6s-0.4 1.8 1.9 3.1c0.2 2.1 0 7.8 0 7.8s-1.2 4.1-3.4 4.4c-2.2 0.2-20 0-20 0v-4.6s-1.6-5-7.2-5c-5.3-0.3-6.2 4.4-6.2 4.4v5.3h-68.7v-3.8c-0.1-1.8-2.3-5.9-7.2-5.9-5 0-6.2 5.3-6.2 5.3h-1.2s-1.6-5-7.2-5-5.2 5-5.2 5v4.7h-10.9l-13.4-1.2s-3.4-0.9-3.4-6.2v-25.9c0.6-7.2 0.1-9.9 5.4-9.9z"/><path class="st6-c" d="m375.4 147 2.5 5.9c-1.8 0.4-1.9-0.9-1.9-0.9s-1.9-4.2-3.8-7.8 0-3.8 0-3.8l3.2 6.6zm4.7 13.7 1.2 8.8-1.6-0.9-1.2-6.2-1.6-6.9s2.2 0.3 2.2 0.6 1 4.6 1 4.6zm-125.7 12.2c3.2 0 5.8 2.6 5.8 5.8s-2.6 5.8-5.8 5.8-5.8-2.6-5.8-5.8 2.6-5.8 5.8-5.8zm14.1 0c3.2 0 5.8 2.6 5.8 5.8s-2.6 5.8-5.8 5.8-5.8-2.6-5.8-5.8 2.6-5.8 5.8-5.8zm82.2 0c3.2 0 5.8 2.6 5.8 5.8s-2.6 5.8-5.8 5.8-5.8-2.6-5.8-5.8 2.6-5.8 5.8-5.8zm11.2-33.1c9 0 9.4 3.8 9.4 3.8l4.4 10 2.2 8.8s0.6 4 0.6 6.9-2.8 2.5-2.8 2.5h-3.1c-4.8 0-5.6-2.2-5.6-2.2s-5.5-6.9-10-12.8c-1.5-4.7-9.1-5-9.1-5h-123.4v-9.8c-0.1-2.6 2.2-2.2 2.2-2.2h135.2zm-82.5 19.7h50.8c0.2-2.5 3.8-1.8 5-1.8 1.5 0 4.2-0.5 5.5 1.8v0.1h4.9s1.7-0.5 3.4 1.6 5 6.6 5 6.6 1.7 3-0.9 3.1h-12.4v10.7h-9.7l-0.8-0.8v-9.9h-29.9-14.1s-2.4 0.7-4.4-1.9-4.7-5.9-4.7-5.9-2.2-3.6 2.3-3.6zm60.4 11.2h-8.2v9.4h8.2v-9.4zm-8.3-11.3h8c-1-1-3.1-0.8-4.2-0.8h-3.8v0.8z"/><path class="st7-c" d="m254.4 175.2c1.9 0 3.4 1.5 3.4 3.4s-1.5 3.4-3.4 3.4-3.4-1.5-3.4-3.4c0-1.8 1.5-3.4 3.4-3.4zm14.1 0c1.9 0 3.4 1.5 3.4 3.4s-1.5 3.4-3.4 3.4-3.4-1.5-3.4-3.4c-0.1-1.8 1.5-3.4 3.4-3.4zm82.2 0c1.9 0 3.4 1.5 3.4 3.4s-1.5 3.4-3.4 3.4-3.4-1.5-3.4-3.4c-0.1-1.8 1.5-3.4 3.4-3.4z"/></svg>
</section>
<!-- Financiacion -->
<section class="section" id="financiacion">
  <div class="container">
    <?php if (have_rows('financiacion')): ?>
      <?php while (have_rows('financiacion')) : the_row(); ?>
        <?php if (get_row_layout() == 'logos_de_financiacion') : ?>
          <h2><?php the_sub_field('titulo'); ?></h2>
          <h3><?php the_sub_field('subtitulo'); ?></h3>
          <?php if (have_rows('logos')) : ?>
            <div class="content-promos">
              <?php while (have_rows('logos')) : the_row(); ?>
                <?php $logo = get_sub_field('logo'); ?>
                <?php if ($logo) : ?>
                  <div class="sect-promo">
                    <div class="cont-img-fin">
  <img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" /></div>
                    <p class="mt-3 text-center"><?php echo esc_attr($logo['alt']); ?></p>
                  </div>
                <?php endif; ?>
              <?php endwhile; ?>
            </div>
          <?php else : ?>
            <?php // no rows found?>
          <?php endif; ?>
          <p class="mt-5">Mirá todas las <a href="<?php the_sub_field('link_promos_bancarias'); ?>" title="Ver promociones bancarias">promociones bancarias</a></p>
        <?php endif; ?>
      <?php endwhile; ?>
    <?php else: ?>
      <?php // no layouts found?>
    <?php endif; ?>
  </div>
</section>
<!-- Empresas asociadas -->
<section class="section" id="empresas-asociadas">
  <div class="container">
    <?php if (have_rows('empresas_asociadas')): ?>
      <?php while (have_rows('empresas_asociadas')) : the_row(); ?>
        <?php if (get_row_layout() == 'logos_de_empresas_asociadas') : ?>
          <h2><?php the_sub_field('titulo'); ?></h2>
          <h3><?php the_sub_field('subtitulo'); ?></h3>
          <?php $logos_images = get_sub_field('logos'); ?>
          <?php if ($logos_images) :  ?>
            <div class="content-asoc">
              <?php foreach ($logos_images as $logos_image): ?>
                <div class="cont-img-asoc">
                    <img src="<?php echo esc_url($logos_image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($logos_image['alt']); ?>" />
                </div>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      <?php endwhile; ?>
    <?php else: ?>
      <?php // no layouts found?>
    <?php endif; ?>
  </div>
</section>
</div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
