<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WP_Bootstrap_Starter
*/
require 'conexion.php';
$result = new WebService();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/dist/css/app_style.min.css?v=<?php echo rand(); ?>">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/dist/css/app.min.css?v=<?php echo rand(); ?>">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css?v=<?php echo rand(); ?>">
  <?php 
	
	$url= $_SERVER["REQUEST_URI"];
	$pos = strpos($url, 'localiza-micro');
	$viaje = strpos($url, 'estado-viaje');
	if($pos or $viaje){
	?>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>
  
	<?php 
	}
	?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-9XH5Q56ZKQ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-9XH5Q56ZKQ');

</script>
<script type="text/javascript">var s=document.createElement("script");s.src="https://api.wcx.cloud/widget/?id=31b5993421744ddd9e512cf0c61248f0";document.head.appendChild(s);</script>
</head>
<body <?php body_class(); ?>>
  <?php
  // WordPress 5.2 wp_body_open implementation
  if (function_exists('wp_body_open')) {
      wp_body_open();
  } else {
      do_action('wp_body_open');
  }
  ?>
  <div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'wp-bootstrap-starter'); ?></a>
    <?php if (!is_page_template('blank-page.php') && !is_page_template('blank-page-with-container.php')): ?>
      <div id="warning">
        <div class="container">
          <i class="fas fa-exclamation-triangle"></i>
          <?php the_field('topbar', 'option'); ?>
        </div>
      </div>
      <header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
        <nav class="navbar navbar-expand-xl p-0 px-5">
          <div class="container">
            <div class="navbar-brand">
              <?php if (get_theme_mod('wp_bootstrap_starter_logo')): ?>
                <a href="<?php echo esc_url(home_url('/')); ?>">
                  <img src="<?php echo esc_url(get_theme_mod('wp_bootstrap_starter_logo')); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>">
                </a>
              <?php else : ?>
                <a class="site-title" href="<?php echo esc_url(home_url('/')); ?>"><?php esc_url(bloginfo('name')); ?></a>
              <?php endif; ?>
            </div>
            <div class="menu-toggle-wrapper">
              <div class="menu-wrapper" id="menu">
                <?php
                wp_nav_menu(array(
                  'theme_location'    => 'primary',
                  'container'       => 'div',
                  'container_id'    => 'main-nav',
                  'container_class' => 'collapse navbar-collapse justify-content-end',
                  'menu_id'         => false,
                  'menu_class'      => 'navbar-nav',
                  'depth'           => 3,
                  'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                  'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>
                <div class="lang">
                  <div class="dropdown">
                    <a href="#" class="lang-link" style="color:black;">Español <i class="fa fa-chevron-down"></i></a>
                    <ul class="lang-dropdown-list">
                      <li class="first-li">Español</li>
                      <li class="dropdown-li">English</li>
                      <li class="last-dropdown-li">Portugues</li>
                    </ul>
                  </div>
                </div>
              </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
              </button>
            </div>
          </div>
        </nav>
      </header><!-- #masthead -->
      <div id="content" class="site-content">
        <div class="">
          <div class="">
          <?php endif; ?>
