<?php
/**
* Template Name: Via Club
*/
get_header();
?>
<section id="primary" class="content-area inner-section">
  <div id="main" class="site-main" role="main">
    <section class="hero mb-5">
      <div class="container">
        <div class="row">
          <div class="col-6"></div>
          <div class="col-6 py-5">
            <?php if (have_rows('que_es')): ?>
              <?php while (have_rows('que_es')) : the_row(); ?>
                <?php if (get_row_layout() == 'que_es_via_club') : ?>
                  <h2><?php the_sub_field('titulo'); ?></h2>
                  <p><?php the_sub_field('texto'); ?></p>
                  <div class="btn">
                    <a href="<?php the_sub_field('boton'); ?>" target="_blank">
                      Registrate
                    </a>
                  </div>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else: ?>
              <?php // no layouts found?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="svg-hero">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1492" height="441" viewBox="0 0 1492 441">
          <defs>
            <clipPath id="clip-path">
              <rect id="Rectangle_206" data-name="Rectangle 206" width="1492" height="441" transform="translate(0 136)" fill="#f2fbcc" opacity="0.999"/>
            </clipPath>
          </defs>
          <g id="Mask_Group_9" data-name="Mask Group 9" transform="translate(0 -136)" clip-path="url(#clip-path)">
            <g id="Group_665" data-name="Group 665">
              <rect id="Rectangle_1" data-name="Rectangle 1" width="1492" height="441" transform="translate(0 136)" fill="#f2fbcc" opacity="0.999"/>
              <path id="Path" d="M134,673.783,168.587,654S276.339,810.944,422.67,797.756c-1.33,22.421,1.33,46.16,1.33,46.16C318.908,846.553,193.862,787.2,134,673.783Z" transform="translate(-51 -286)" fill="#7c51a1" opacity="0.078"/>
              <path id="Path-2" data-name="Path" d="M-60,624.532-17.422,649S115.225,454.885,295.362,471.2C293.725,443.466,297,414.1,297,414.1,167.628,410.842,13.693,484.247-60,624.532Z" transform="translate(-36 -240)" fill="#dc272d" opacity="0.078"/>
              <path id="Path-3" data-name="Path" d="M1138,952.531,1180.578,977s132.647-194.115,312.784-177.8C1491.725,771.466,1495,742.1,1495,742.1,1365.628,738.842,1211.693,812.247,1138,952.531Z" transform="translate(128 -435)" fill="#c0bc00" opacity="0.078"/>
              <path id="Path-4" data-name="Path" d="M-60,624.532-17.422,649S115.225,454.885,295.362,471.2C293.725,443.466,297,414.1,297,414.1,167.628,410.842,13.693,484.247-60,624.532Z" transform="translate(-29 -252)" fill="#fff" opacity="0.078"/>
              <path id="Path-5" data-name="Path" d="M1138,952.531,1180.578,977s132.647-194.115,312.784-177.8C1491.725,771.466,1495,742.1,1495,742.1,1365.628,738.842,1211.693,812.247,1138,952.531Z" transform="translate(176 -435)" fill="#fff" opacity="0.078"/>
              <path id="Path-6" data-name="Path" d="M250.137,1214.843c-5.674,4.9-26.786,26.312-26.633,26.486l.023.018a.07.07,0,0,1-.023-.018l-24.95-20.37s-19.587-18.158-38.483-6.932c-3.682,2.187-25.99,12.8-25.99,12.8l-10.036-4.641s-15.242-11.369-38.073,9.786c-26.772,21.94-53.331,18.468-64.684,10.194S0,1222.182,0,1222.182v36.291H458.172l-48.782-54.233s-12.434-17.865-25.792-3.67L345,1234.114l-38.773-22.125S279.488,1189.516,250.137,1214.843Z" transform="translate(474.828 -682.09)" fill="#8fe164"/>
              <g id="Group_320" data-name="Group 320" transform="translate(101.182 124.512)">
                <g id="Group_320-2" data-name="Group 320" transform="translate(387.766 65.87)">
                  <rect id="Rectangle_155" data-name="Rectangle 155" width="248.247" height="95.48" rx="5" transform="translate(0 175.537) rotate(-45)" fill="#fff"/>
                  <path id="Rectangle_156" data-name="Rectangle 156" d="M5,0H243.247a5,5,0,0,1,5,5V17.627a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V5A5,5,0,0,1,5,0Z" transform="translate(0 175.537) rotate(-45)" fill="#123f43" style="isolation: isolate"/>
                  <line id="Line_138" data-name="Line 138" x2="67.514" y2="67.514" transform="translate(127.758 47.779)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4 4" style="isolation: isolate"/>
                  <rect id="Rectangle_157" data-name="Rectangle 157" width="17.627" height="54.35" transform="translate(128.797 86.211) rotate(-45)" fill="#009576" style="isolation: isolate"/>
                  <line id="Line_139" data-name="Line 139" x1="88.288" y2="88.288" transform="translate(35.315 97.636)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_140" data-name="Line 140" x1="88.288" y2="88.288" transform="translate(46.221 108.542)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_141" data-name="Line 141" x1="88.288" y2="88.288" transform="translate(57.128 119.449)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_142" data-name="Line 142" x1="88.288" y2="88.288" transform="translate(68.034 130.355)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_143" data-name="Line 143" x1="36.354" y2="36.354" transform="translate(155.803 29.083)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_144" data-name="Line 144" x1="36.354" y2="36.354" transform="translate(166.709 39.989)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_145" data-name="Line 145" x1="36.354" y2="36.354" transform="translate(177.615 50.895)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_146" data-name="Line 146" x1="36.354" y2="36.354" transform="translate(188.521 61.802)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                </g>
                <path id="Composite_Path" data-name="Composite Path" d="M279.185,262.176v25.458h5.351V261.992a19.4,19.4,0,0,1-3.312.281h-.07A19.147,19.147,0,0,1,279.185,262.176Zm-17.392-19.258a19.325,19.325,0,0,0,19.363,19.354h.071a19.354,19.354,0,1,0,0-38.709h-.071a19.36,19.36,0,0,0-19.365,19.353h0Z" transform="translate(86.565 75.508)" fill="#b3e3c8"/>
                <g id="Group_122" data-name="Group 122" transform="translate(382.433 93.354)">
                  <path id="Path-23" d="M284.99,353.143V94.195l89.639-10.678V353.143Z" transform="translate(-284.99 -83.517)" fill="#99d5c8"/>
                  <path id="Path-24" d="M365.176,195.429l37.457,14.689v93.435H345.1V186.093Z" transform="translate(-256.802 -35.417)" fill="#f2fbcc"/>
                </g>
                <path id="Path-25" d="M397.845,146.216s1.322,176.191-1.469,176.191H301.385V146.216Z" transform="translate(105.131 39.238)" fill="#e6f699"/>
                <path id="Path-6-2" data-name="Path-6" d="M159.28,106.719a26.158,26.158,0,0,0-25.853,29.847h104A19.6,19.6,0,0,0,209.4,121.783a26.126,26.126,0,0,0-29.594-5.093A26.071,26.071,0,0,0,159.28,106.719Z" transform="translate(26.25 20.717)" fill="#fcfcfd"/>
                <path id="Path-7" d="M147.02,224.9H273.1s-2.515-13.981-15.438-11.553c-5.467-5.022-6.769-9.657-16.727-7.7-3.233-6.96-1.747-10.111-15.438-16.688-11.386-4.914-22.4,6.547-21.872,10.269-3.446-4.063-14.219-1.175-15.438,6.412-6.4-1.212-15.875,1.754-19.961,7.129C158.012,211.475,150.031,210.409,147.02,224.9Z" transform="translate(32.746 58.714)" fill="#fcfcfd"/>
                <g id="Group_321" data-name="Group 321" transform="translate(246.355 62.538)">
                  <path id="Path_783" data-name="Path 783" d="M239.852,128.536V244.587a63.175,63.175,0,0,0,0-116.051Z" transform="translate(-170.078 -31.59)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_784" data-name="Path 784" d="M192.352,198.345v161.13h69.774V211.852a63.183,63.183,0,0,1-69.774-13.507Z" transform="translate(-192.352 1.144)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_158" data-name="Rectangle 158" width="56.089" height="17.627" rx="6" transform="translate(91.818 72.722) rotate(-45)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_159" data-name="Rectangle 159" width="85.906" height="165.988" rx="3.289" transform="translate(34.899)" fill="#123f43"/>
                  <rect id="Rectangle_160" data-name="Rectangle 160" width="78.304" height="151.299" rx="3.289" transform="translate(38.701 7.345)" fill="#fff" style="isolation: isolate"/>
                  <path id="Rectangle_161" data-name="Rectangle 161" d="M0,0H19.6A20.793,20.793,0,0,1,40.4,20.793v102.6a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V0A0,0,0,0,1,0,0Z" transform="translate(11.751 119.717)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_162" data-name="Rectangle 162" width="24.972" height="17.627" rx="5.086" transform="translate(104.293 85.197)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_163" data-name="Rectangle 163" width="24.972" height="17.627" rx="5.086" transform="translate(104.293 107.231)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_164" data-name="Rectangle 164" width="17.627" height="17.627" rx="5.086" transform="translate(111.638 129.265)" fill="#dbd4d8" style="isolation: isolate"/>
                  <path id="Path_785" data-name="Path 785" d="M270.852,123.038V133.32a5.141,5.141,0,1,0,0-10.282Z" transform="translate(-155.542 -34.169)" fill="#fff" style="isolation: isolate"/>
                  <path id="Path_786" data-name="Path 786" d="M271.352,138.038V148.32a5.141,5.141,0,0,0,0-10.282Z" transform="translate(-155.307 -27.135)" fill="#fff" style="isolation: isolate"/>
                  <path id="Path_787" data-name="Path 787" d="M275.352,153.038V163.32a5.141,5.141,0,0,0,0-10.282Z" transform="translate(-153.432 -20.101)" fill="#fff" style="isolation: isolate"/>
                  <text id="_5" data-name="+5" transform="translate(45.223 66.784)" fill="#009576" font-size="46" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">+5</tspan></text>
                  <path id="Path_788" data-name="Path 788" d="M231.7,209.35a4.83,4.83,0,0,1-4.831-4.831v-58.3l11.744-11.745,2.195-2.195a24.9,24.9,0,0,0,6.8-22.59A6.06,6.06,0,0,0,238.335,106c-13.33,8.637-25.167,19.951-33.49,32.986q-2.173,3.4-4.013,6.954a72.23,72.23,0,0,0-8.48,33.3v62.88a63.183,63.183,0,0,0,69.774,13.507V209.35Z" transform="translate(-192.352 -42.628)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_165" data-name="Rectangle 165" width="52.881" height="19.096" rx="6.5" transform="translate(52.146 128.53)" fill="#c0e900"/>
                </g>
                <path id="Path_789" data-name="Path 789" d="M269.106,155v4.407H233.852V155" transform="translate(73.463 43.357)" fill="#009576"/>
                <rect id="Rectangle_166" data-name="Rectangle 166" width="48.474" height="5.876" transform="translate(299.97 169.035)" fill="#b2a9ac" style="isolation: isolate"/>
                <rect id="Rectangle_167" data-name="Rectangle 167" width="48.474" height="5.876" transform="translate(299.97 158.752)" fill="#b2a9ac" style="isolation: isolate"/>
                <rect id="Rectangle_168" data-name="Rectangle 168" width="48.474" height="5.876" transform="translate(299.97 148.47)" fill="#b2a9ac" style="isolation: isolate"/>
                <g id="Group_322" data-name="Group 322" transform="translate(246.355 124.907)">
                  <path id="Path_790" data-name="Path 790" d="M231.7,209.35a4.83,4.83,0,0,1-4.831-4.831v-58.3l11.744-11.745,2.195-2.195a24.9,24.9,0,0,0,6.8-22.59A6.06,6.06,0,0,0,238.335,106c4.12,9.755-4.414,16.662-4.414,16.662l-7.68-7.68a106.711,106.711,0,0,0-21.4,24q-2.173,3.4-4.013,6.954a72.23,72.23,0,0,0-8.48,33.3v62.88a63.183,63.183,0,0,0,69.774,13.507V209.35Z" transform="translate(-192.352 -104.997)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_791" data-name="Path 791" d="M227.517,105.679a121.965,121.965,0,0,0-12.094,8.982l7.68,7.68S231.637,115.434,227.517,105.679Z" transform="translate(-181.534 -104.677)" fill="#fff" style="isolation: isolate"/>
                </g>
                <path id="Path-12" d="M123.7,307.2A46.415,46.415,0,0,0,77.55,359.406H324.208a40.743,40.743,0,0,0-58.118-33.762,71.275,71.275,0,0,0-139.029-18.332Q125.385,307.194,123.7,307.2Z" transform="translate(0 91.605)" fill="#c0e900"/>
                <g id="Group_323" data-name="Group 323" transform="translate(214.211 353.739)">
                  <path id="Path_792" data-name="Path 792" d="M400.99,277.284l-6.76-14.2s-4.056.223,0,8.111,8.111,16.9,8.111,16.9.223,2.926,4.056,2.027Z" transform="translate(-66.388 -259.698)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_793" data-name="Path 793" d="M407.316,296.668l2.7,18.927-3.38-2.029-2.7-13.52-3.378-14.87s4.731.676,4.731,1.351S407.316,296.668,407.316,296.668Z" transform="translate(-62.576 -249.34)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_794" data-name="Path 794" d="M182.636,260.79H478.709s14.654-.419,19.6,4.056a6.844,6.844,0,0,0-2.029,4.056l11.491,22.307H509.8l3.38,7.436-4.731-1.351,4.731,22.983s-.927,3.846,4.056,6.76c.4,4.576,0,16.9,0,16.9s-2.556,8.956-7.436,9.463-43.261,0-43.261,0V343.258s-1.407-10.808-13.52-10.816c-11.431-.621-13.52,9.463-13.52,9.463V353.4H288.763v-8.111c-.088-3.879-4.136-12.167-14.871-12.167s-13.52,10.816-13.52,10.816h-2.7s-1.379-10.816-13.52-10.816-13.518,10.816-13.518,10.816v10.14H206.97l-29.065-2.7s-7.436-1.866-7.436-13.52v-56.1C172.213,266.61,171.171,260.777,182.636,260.79Z" transform="translate(-170.469 -260.78)" fill="#009576" fill-rule="evenodd"/>
                  <circle id="Ellipse_163" data-name="Ellipse 163" cx="12.505" cy="12.505" r="12.505" transform="translate(61.296 73.738)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_164" data-name="Ellipse 164" cx="12.505" cy="12.505" r="12.505" transform="translate(91.715 73.738)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_165" data-name="Ellipse 165" cx="12.505" cy="12.505" r="12.505" transform="translate(269.493 73.738)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_166" data-name="Ellipse 166" cx="7.436" cy="7.436" r="7.436" transform="translate(66.366 78.808)" fill="#f3f3f3"/>
                  <circle id="Ellipse_167" data-name="Ellipse 167" cx="7.436" cy="7.436" r="7.436" transform="translate(96.784 78.808)" fill="#f3f3f3"/>
                  <circle id="Ellipse_168" data-name="Ellipse 168" cx="7.436" cy="7.436" r="7.436" transform="translate(274.563 78.808)" fill="#f3f3f3"/>
                  <path id="Path_795" data-name="Path 795" d="M418.448,315.532H273.791s-5.178,1.473-9.463-4.056-10.14-12.843-10.14-12.843-5.144-7.436,4.731-7.436h143.3s3.718-1.115,7.436,3.38,10.816,14.194,10.816,14.194S424.14,315.324,418.448,315.532Z" transform="translate(-131.839 -246.574)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_796" data-name="Path 796" d="M473.416,262.181H180.723s-4.846-.792-4.731,4.731v20.954H443s16.42.61,19.6,10.816c9.735,12.709,21.631,27.716,21.631,27.716s1.852,4.731,12.167,4.731h6.76s6.083.8,6.083-5.409a119.743,119.743,0,0,0-1.351-14.871l-4.731-18.926-9.464-21.631S492.829,262.181,473.416,262.181Z" transform="translate(-167.88 -260.142)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_797" data-name="Path 797" d="M348.336,291.3c1.522-.169,4.9.845,4.9,3.211v44.613H333.973v-45.29c0-1.014,1.858-2.872,2.534-2.534Zm-11.152-1.522,11.829.169c2.365,0,5.745,2.2,5.745,4.9V339.8c-.169.339-.507.508-.676.677H333.127c-.338-.169-.507-.508-.676-.677V294.511A4.622,4.622,0,0,1,337.184,289.778Z" transform="translate(-94.512 -247.182)" fill="#009576" fill-rule="evenodd"/>
                </g>
              </g>
            </g>
          </g>
        </svg>
      </div>
      <div class="svg-hero-mobile" style="display: none;">
        <svg id="Group_693" data-name="Group 693" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="1359.006" viewBox="0 0 768 1359.006">
          <defs>
            <clipPath id="clip-path">
              <rect id="Rectangle_214" data-name="Rectangle 214" width="768" height="1359" fill="#f2fbcc" opacity="0.999"/>
            </clipPath>
          </defs>
          <rect id="Rectangle_215" data-name="Rectangle 215" width="768" height="1359" fill="#f2fbcc" opacity="0.999"/>
          <g id="Mask_Group_17" data-name="Mask Group 17" clip-path="url(#clip-path)">
            <g id="Group_692" data-name="Group 692">
              <path id="Path" d="M134,674.723,170.231,654S283.1,818.4,436.388,804.587c-1.393,23.486,1.394,48.354,1.394,48.354C327.7,855.7,196.707,793.535,134,674.723Z" transform="translate(-153.518 486.074)" fill="#7c51a1" opacity="0.078"/>
              <path id="Path-2" data-name="Path" d="M-60,634.536l44.6,25.631s138.95-203.34,327.648-186.253c-1.715-29.048,1.715-59.806,1.715-59.806C178.446,410.692,17.195,487.585-60,634.536Z" transform="translate(-147.023 522.855)" fill="#dc272d" opacity="0.078"/>
              <path id="Path-3" data-name="Path" d="M1138,971.165l46.346,26.634s144.387-211.3,340.468-193.54c-1.783-30.185,1.782-62.146,1.782-62.146C1385.775,738.562,1218.215,818.464,1138,971.165Z" transform="translate(-526.162 238.047)" fill="#c0bc00" opacity="0.078"/>
            </g>
          </g>
          <g id="Group_674" data-name="Group 674" transform="translate(-109.396)">
            <g id="Group_488" data-name="Group 488" transform="translate(-97.629 936.855)">
              <path id="Path-4" data-name="Path" d="M262.024,1215.782c-5.943,5.128-28.059,27.563-27.9,27.745l.024.019a.071.071,0,0,1-.024-.019l-26.136-21.338s-20.517-19.021-40.311-7.261c-3.857,2.291-27.225,13.4-27.225,13.4l-10.513-4.861s-15.967-11.909-39.883,10.251C62.013,1256.7,34.192,1253.067,22.3,1244.4S0,1223.47,0,1223.47v38.016H479.945l-51.1-56.81s-13.025-18.714-27.017-3.844l-40.436,35.137-40.616-23.177S292.77,1189.251,262.024,1215.782Z" transform="translate(420.923 -839.981)" fill="#8fe164"/>
              <g id="Group_485" data-name="Group 485" transform="translate(287.408 13.67)">
                <g id="Group_320" data-name="Group 320" transform="translate(325.338 3.491)">
                  <rect id="Rectangle_155" data-name="Rectangle 155" width="260.044" height="100.017" rx="5" transform="translate(0 183.879) rotate(-45)" fill="#fff"/>
                  <path id="Rectangle_156" data-name="Rectangle 156" d="M5,0H255.044a5,5,0,0,1,5,5V18.465a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V5A5,5,0,0,1,5,0Z" transform="translate(0 183.879) rotate(-45)" fill="#123f43" style="isolation: isolate"/>
                  <line id="Line_138" data-name="Line 138" x2="70.723" y2="70.723" transform="translate(133.829 50.05)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" stroke-dasharray="4 4" style="isolation: isolate"/>
                  <rect id="Rectangle_157" data-name="Rectangle 157" width="18.465" height="56.933" transform="translate(134.917 90.308) rotate(-45)" fill="#009576" style="isolation: isolate"/>
                  <line id="Line_139" data-name="Line 139" x1="92.484" y2="92.484" transform="translate(36.993 102.276)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_140" data-name="Line 140" x1="92.484" y2="92.484" transform="translate(48.418 113.7)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_141" data-name="Line 141" x1="92.484" y2="92.484" transform="translate(59.842 125.125)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_142" data-name="Line 142" x1="92.484" y2="92.484" transform="translate(71.267 136.549)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_143" data-name="Line 143" x1="38.081" y2="38.081" transform="translate(163.207 30.465)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_144" data-name="Line 144" x1="38.081" y2="38.081" transform="translate(174.631 41.89)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_145" data-name="Line 145" x1="38.081" y2="38.081" transform="translate(186.056 53.314)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                  <line id="Line_146" data-name="Line 146" x1="38.081" y2="38.081" transform="translate(197.48 64.739)" fill="none" stroke="#009576" stroke-miterlimit="10" stroke-width="1" style="isolation: isolate"/>
                </g>
                <path id="Composite_Path" data-name="Composite Path" d="M280.012,264.011v26.668h5.606v-26.86a20.319,20.319,0,0,1-3.47.294h-.074A20.067,20.067,0,0,1,280.012,264.011Zm-18.219-20.173a20.243,20.243,0,0,0,20.283,20.274h.074a20.274,20.274,0,1,0,0-40.548h-.074a20.28,20.28,0,0,0-20.285,20.273v0Z" transform="translate(22.264 24.21)" fill="#b3e3c8"/>
                <g id="Group_122" data-name="Group 122" transform="translate(319.752 32.281)">
                  <path id="Path-23" d="M284.99,365.956V94.7l93.9-11.185V365.956Z" transform="translate(-284.99 -83.517)" fill="#99d5c8"/>
                  <path id="Path-24" d="M366.13,195.873l39.238,15.387v97.875H345.1V186.093Z" transform="translate(-252.606 -28.257)" fill="#f2fbcc"/>
                </g>
                <path id="Path-25" d="M402.428,146.216s1.385,184.564-1.539,184.564h-99.5V146.216Z" transform="translate(43.594 -17.459)" fill="#e6f699"/>
                <path id="Path-6" d="M160.521,106.719a27.4,27.4,0,0,0-27.082,31.265H242.382A20.527,20.527,0,0,0,213.023,122.5a27.368,27.368,0,0,0-31-5.335A27.31,27.31,0,0,0,160.521,106.719Z" transform="translate(-47.03 -38.737)" fill="#fcfcfd"/>
                <path id="Path-7" d="M147.02,226.662H279.092s-2.634-14.646-16.172-12.1c-5.727-5.261-7.09-10.116-17.521-8.068-3.387-7.29-1.829-10.591-16.172-17.481-11.927-5.147-23.467,6.858-22.912,10.757-3.61-4.256-14.895-1.231-16.172,6.717-6.7-1.269-16.629,1.837-20.91,7.467C158.534,212.6,150.174,211.486,147.02,226.662Z" transform="translate(-39.566 4.916)" fill="#fcfcfd"/>
                <g id="Group_321" data-name="Group 321" transform="translate(177.207 0)">
                  <path id="Path_783" data-name="Path 783" d="M239.852,128.536V250.1a66.177,66.177,0,0,0,0-121.565Z" transform="translate(-166.763 -26.983)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_784" data-name="Path 784" d="M192.352,198.345V367.132h73.089V212.494a66.185,66.185,0,0,1-73.089-14.149Z" transform="translate(-192.352 10.624)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_158" data-name="Rectangle 158" width="58.754" height="18.465" rx="6" transform="translate(96.181 76.178) rotate(-45)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_159" data-name="Rectangle 159" width="89.988" height="173.876" rx="3.289" transform="translate(36.558)" fill="#123f43"/>
                  <rect id="Rectangle_160" data-name="Rectangle 160" width="82.025" height="158.489" rx="3.289" transform="translate(40.54 7.694)" fill="#fff" style="isolation: isolate"/>
                  <path id="Rectangle_161" data-name="Rectangle 161" d="M0,0H21.522A20.793,20.793,0,0,1,42.315,20.793v108.46a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V0A0,0,0,0,1,0,0Z" transform="translate(12.309 125.406)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_162" data-name="Rectangle 162" width="26.158" height="18.465" rx="5.086" transform="translate(109.249 89.246)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_163" data-name="Rectangle 163" width="26.158" height="18.465" rx="5.086" transform="translate(109.249 112.327)" fill="#dbd4d8" style="isolation: isolate"/>
                  <rect id="Rectangle_164" data-name="Rectangle 164" width="18.465" height="18.465" rx="5.086" transform="translate(116.943 135.408)" fill="#dbd4d8" style="isolation: isolate"/>
                  <path id="Path_785" data-name="Path 785" d="M270.852,123.038v10.771a5.386,5.386,0,1,0,0-10.771Z" transform="translate(-150.062 -29.945)" fill="#fff" style="isolation: isolate"/>
                  <path id="Path_786" data-name="Path 786" d="M271.352,138.038v10.771a5.386,5.386,0,1,0,0-10.771Z" transform="translate(-149.793 -21.864)" fill="#fff" style="isolation: isolate"/>
                  <path id="Path_787" data-name="Path 787" d="M275.352,153.038v10.771a5.386,5.386,0,1,0,0-10.771Z" transform="translate(-147.638 -13.784)" fill="#fff" style="isolation: isolate"/>
                  <text id="_5" data-name="+5" transform="translate(47.373 62.819)" fill="#009576" font-size="41" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">+5</tspan></text>
                  <path id="Path_788" data-name="Path 788" d="M233.573,214.309a5.06,5.06,0,0,1-5.061-5.061V148.177l12.3-12.3,2.3-2.3a26.078,26.078,0,0,0,7.123-23.664,6.348,6.348,0,0,0-9.715-3.864c-13.964,9.048-26.363,20.9-35.081,34.554q-2.276,3.564-4.2,7.284a75.663,75.663,0,0,0-8.883,34.881v65.868a66.185,66.185,0,0,0,73.089,14.149V214.309Z" transform="translate(-192.352 -39.664)" fill="#dad3d7" style="isolation: isolate"/>
                  <rect id="Rectangle_165" data-name="Rectangle 165" width="55.394" height="20.003" rx="6.5" transform="translate(54.624 134.638)" fill="#c0e900"/>
                </g>
                <path id="Path_789" data-name="Path 789" d="M270.781,155v4.616H233.852V155" transform="translate(7.212 -12.727)" fill="#009576"/>
                <rect id="Rectangle_166" data-name="Rectangle 166" width="50.778" height="6.155" transform="translate(233.371 111.558)" fill="#b2a9ac" style="isolation: isolate"/>
                <rect id="Rectangle_167" data-name="Rectangle 167" width="50.778" height="6.155" transform="translate(233.371 100.786)" fill="#b2a9ac" style="isolation: isolate"/>
                <rect id="Rectangle_168" data-name="Rectangle 168" width="50.778" height="6.155" transform="translate(233.371 90.015)" fill="#b2a9ac" style="isolation: isolate"/>
                <g id="Group_322" data-name="Group 322" transform="translate(177.207 65.333)">
                  <path id="Path_790" data-name="Path 790" d="M233.573,214.309a5.06,5.06,0,0,1-5.061-5.061V148.177l12.3-12.3,2.3-2.3a26.078,26.078,0,0,0,7.123-23.664,6.348,6.348,0,0,0-9.715-3.864c4.316,10.219-4.624,17.454-4.624,17.454l-8.044-8.044A111.781,111.781,0,0,0,205.439,140.6q-2.276,3.564-4.2,7.284a75.663,75.663,0,0,0-8.883,34.881v65.868a66.185,66.185,0,0,0,73.089,14.149V214.309Z" transform="translate(-192.352 -104.997)" fill="#dad3d7" style="isolation: isolate"/>
                  <path id="Path_791" data-name="Path 791" d="M228.091,105.679a127.759,127.759,0,0,0-12.668,9.409l8.044,8.044S232.407,115.9,228.091,105.679Z" transform="translate(-179.923 -104.63)" fill="#fff" style="isolation: isolate"/>
                </g>
                <path id="Path-12" d="M125.914,309.541A48.621,48.621,0,0,0,77.567,364.23h258.38a42.679,42.679,0,0,0-60.88-35.366,74.662,74.662,0,0,0-145.636-19.2Q127.675,309.536,125.914,309.541Z" transform="translate(-77.187 44.25)" fill="#c0e900"/>
                <g id="Group_323" data-name="Group 323" transform="translate(143.536 305.039)">
                  <path id="Path_792" data-name="Path 792" d="M401.4,277.958l-7.081-14.87s-4.248.234,0,8.5,8.5,17.7,8.5,17.7.234,3.065,4.248,2.123Z" transform="translate(-50.894 -259.537)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_793" data-name="Path 793" d="M407.638,297.214l2.833,19.827-3.541-2.125L404.1,300.754l-3.539-15.577s4.956.708,4.956,1.416S407.638,297.214,407.638,297.214Z" transform="translate(-46.514 -247.637)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_794" data-name="Path 794" d="M183.214,260.791H493.357s15.35-.438,20.534,4.248a7.169,7.169,0,0,0-2.125,4.248L523.8,292.655h2.125l3.541,7.789-4.956-1.416,4.956,24.075s-.971,4.028,4.249,7.081c.417,4.793,0,17.7,0,17.7s-2.677,9.382-7.789,9.913-45.317,0-45.317,0v-10.62s-1.474-11.322-14.162-11.329c-11.974-.651-14.162,9.912-14.162,9.912V357.8h-157.9v-8.5c-.092-4.064-4.333-12.745-15.578-12.745s-14.162,11.33-14.162,11.33h-2.831s-1.445-11.33-14.162-11.33-14.161,11.33-14.161,11.33v10.622H208.7l-30.447-2.833s-7.789-1.954-7.789-14.162v-58.77C172.3,266.887,171.2,260.777,183.214,260.791Z" transform="translate(-170.469 -260.78)" fill="#009576" fill-rule="evenodd"/>
                  <circle id="Ellipse_163" data-name="Ellipse 163" cx="13.1" cy="13.1" r="13.1" transform="translate(64.209 77.242)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_164" data-name="Ellipse 164" cx="13.1" cy="13.1" r="13.1" transform="translate(96.073 77.242)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_165" data-name="Ellipse 165" cx="13.1" cy="13.1" r="13.1" transform="translate(282.3 77.242)" fill="#001835" style="isolation: isolate"/>
                  <circle id="Ellipse_166" data-name="Ellipse 166" cx="7.789" cy="7.789" r="7.789" transform="translate(69.52 82.553)" fill="#f3f3f3"/>
                  <circle id="Ellipse_167" data-name="Ellipse 167" cx="7.789" cy="7.789" r="7.789" transform="translate(101.384 82.553)" fill="#f3f3f3"/>
                  <circle id="Ellipse_168" data-name="Ellipse 168" cx="7.789" cy="7.789" r="7.789" transform="translate(287.61 82.553)" fill="#f3f3f3"/>
                  <path id="Path_795" data-name="Path 795" d="M426.317,316.695H274.787s-5.424,1.543-9.913-4.248-10.622-13.453-10.622-13.453-5.389-7.789,4.956-7.789H409.323s3.895-1.168,7.789,3.541,11.33,14.869,11.33,14.869S432.28,316.476,426.317,316.695Z" transform="translate(-126.089 -244.459)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_796" data-name="Path 796" d="M487.55,262.182h-306.6s-5.076-.829-4.956,4.956v21.95H455.686s17.2.639,20.534,11.33c10.2,13.313,22.659,29.033,22.659,29.033s1.94,4.956,12.745,4.956h7.081s6.372.836,6.372-5.665a125.438,125.438,0,0,0-1.416-15.578l-4.956-19.825-9.914-22.659S507.885,262.182,487.55,262.182Z" transform="translate(-167.495 -260.047)" fill="#001835" fill-rule="evenodd" style="isolation: isolate"/>
                  <path id="Path_797" data-name="Path 797" d="M349.091,291.372c1.594-.177,5.135.885,5.135,3.364v46.733h-20.18V294.027c0-1.062,1.946-3.008,2.654-2.654Zm-11.682-1.594,12.391.177c2.477,0,6.018,2.3,6.018,5.135v47.086c-.177.355-.531.532-.708.709H333.159c-.354-.177-.531-.533-.708-.709v-47.44A4.841,4.841,0,0,1,337.409,289.778Z" transform="translate(-83.205 -245.158)" fill="#009576" fill-rule="evenodd"/>
                </g>
              </g>
            </g>
          </g>
        </svg>
      </div>
    </section>
    <section class="section-block">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php if (have_rows('consigo_puntos')): ?>
              <?php while (have_rows('consigo_puntos')) : the_row(); ?>
                <?php if (get_row_layout() == 'logos_puntos') : ?>
                  <h3><?php the_sub_field('titulo'); ?></h3>
                  <p><?php the_sub_field('subtitulo'); ?></p>
                  <?php $logos_images = get_sub_field('logos'); ?>
                  <div class="logo-gal">
                    <?php if ($logos_images) :  ?>
                      <?php foreach ($logos_images as $logos_image): ?>
                        <div class="cont-logo-gal ">
                        <img src="<?php echo esc_url($logos_image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($logos_image['alt']); ?>" />
                      </div>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else: ?>
              <?php // no layouts found?>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <p><?php the_field('texto_medio'); ?></p>
          </div>
        </div>
      </div>
    </section>
    <section class="section-block">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php if (have_rows('como_uso_mis_puntos')): ?>
              <?php while (have_rows('como_uso_mis_puntos')) : the_row(); ?>
                <?php if (get_row_layout() == 'bloque_uso_mis_puntos') : ?>
                  <h3><?php the_sub_field('titulo'); ?></h3>
                  <?php if (have_rows('box_boton')) : ?>
                    <div class="box-block">
                      <?php while (have_rows('box_boton')) : the_row(); ?>
                        <div class="box-wrapper box-shadow">
                          <p><?php the_sub_field('texto'); ?></p>
                          <div class="btn mt-3">
                            <a href="<?php the_sub_field('boton'); ?>" target="_blank">Ver puntos</a>
                          </div>
                        </div>
                      <?php endwhile; ?>
                    </div>
                  <?php else : ?>
                    <?php // no rows found?>
                  <?php endif; ?>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else: ?>
              <?php // no layouts found?>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <p><?php the_field('texto_abajo'); ?></p>
          </div>
        </div>
      </div>
    </section>
  </div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
