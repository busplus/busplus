<?php
/**
* Template Name: Protocolo Covid
*/
get_header();
?>
<section id="primary" class="content-area inner-section">
  <div id="main" class="site-main" role="main">
    <section class="hero hero-small hero-protocolo">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2><?php the_title(); ?></h2>
          </div>
        </div>
      </div>
    </section>
    <section class="section-block">
      <div class="container">
        <div class="row">
          <?php if (have_rows('box_preguntas')) : ?>
            <?php while (have_rows('box_preguntas')) : the_row(); ?>
              <div class="col-xs-12 col-sm-6 text-center box-preguntas px-5">
                <?php $icono = get_sub_field('icono'); ?>
                <?php if ($icono) : ?>
                  <img src="<?php echo esc_url($icono['url']); ?>" alt="<?php echo esc_attr($icono['alt']); ?>" />
                <?php endif; ?>
                <h3 class="mt-5"><?php the_sub_field('pregunta'); ?></h3>
                <?php the_sub_field('texto'); ?>
                <?php
                $link = get_sub_field('link_boton');
                $link_url = $link['url'];
                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                <div class="btn mt-5"><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">Más información</a></div>
              </div>
            <?php endwhile; ?>
          <?php else : ?>
            <?php // no rows found?>
          <?php endif; ?>
        </div>
      </div>
    </section>
  </div><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
