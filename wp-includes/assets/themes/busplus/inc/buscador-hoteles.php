<div id="buscador" class="hoteles">
  <a href="#" class="c-search"><i class="fa fa-times"></i></a>
  <form id="do_search">
    <div class="b-bot">
      <div class="b-group">
        <label for="">Ciudad de destino</label>
        <div class="sel-hotel-destino">
          <i class="fa fa-map-marker-alt"></i>
          <input type="text" name="hotel-destino" id="hotel-destino" placeholder="Ingrese una ciudad">
          <input type="hidden" name="hotel-destino-id" id="hotel-destino-id"  value="0">
        </div>
      </div>
      <div class="b-group bg3 h">
        <div class="iv">
          <label for="">Fecha Check in</label>
          <div class="sel-fecha">
            <i class="fa fa-calendar-alt"></i>
            <input type="text" readonly="true" placeholder="Entrada" id="hotel-check-in" name="shotel-check-in" autocomplete="off" class="f-desde">
            <input type="hidden" name="hotel-check-in-formatted" id="hotel-check-in-formatted"  value="0">
          </div>
        </div>
        <div class="iv">
          <label for="">Fecha Check out</label>
          <div class="sel-fecha">
            <input type="text" readonly="true" placeholder="Salida" id="hotel-check-out" name="hotel-check-out" autocomplete="off" class="f-hasta">
            <input type="hidden" name="hotel-check-out-formatted" id="hotel-check-out-formatted"  value="0">
          </div>
        </div>
      </div>
      <div class="b-group">
        <label for="">Pasajeros</label>
        <div class="dropdown hab">
          <!-- <i class="fas fa-bed"></i> -->
          <i class="fas fa-user"></i>
          <a href="#" class="show-hotel-pass">
             <span id="hotel-num-pass">1</span>
            <!-- <span id="hotel-num-hab">1</span> -->
             <i class="fa fa-chevron-down"></i></a>
          <div class="modal-hotel-pass">
            <div class="pass-inner">
              <a href="#0" class="close-hotel-pass"><i class="fa fa-times"></i></a>
              <div class="habitacion" id="hab1">
          <h4> Habitación</h4>
              <div class="p-l">
                <div class="p-c"> <span> <strong>Adultos</strong></span> </div>
                <div class="p-c"> <div class="counter c-adultos">
                  <div class="value-button" onclick="decreaseHotelValue('number-hotel-ad-1')" value="Menos"><i class="fa fa-minus"></i></div>
                  <input type="number" id="number-hotel-ad-1" value="1" class="number" max="9" />
                  <div class="value-button" onclick="increaseHotelValue('number-hotel-ad-1')" value="Más"><i class="fa fa-plus"></i></div>
                </div></div>
              </div>
              <div class="p-l">
                <div class="p-c"> <span><strong>Niños</strong></span><span>Entre 0 y 12 años</span> </div>
                <div class="p-c"><div class="counter c-ninos">
                  <div class="value-button"  onclick="decreaseHotelValue('number-hotel-ch-1')" value="Menos"><i class="fa fa-minus"></i></div>
                  <input type="number" id="number-hotel-ch-1" value="0" class="number" max="4" />
                  <div class="value-button" onclick="increaseHotelValue('number-hotel-ch-1')" value="Más"><i class="fa fa-plus"></i></div>
                </div></div>
              </div>
              <!-- <div class="p-l">
                <div class="p-c"><label for="">Edad niño 1</label> <input type="text" name="" value=""> </div>
                <div class="p-c"><label for="">Edad niño 2</label> <input type="text" name="" value=""> </div>
              </div>
              <div class="p-l">
                <div class="p-c"><label for="">Edad niño 3</label> <input type="text" name="" value=""> </div>
                <div class="p-c"><label for="">Edad niño 4</label> <input type="text" name="" value=""> </div>
              </div> -->
              <div class="p-actions">
                <!-- <a href="#0" class="new-hotel-hab">Agregar Habitación</a> -->
                <a href="#0" class="close-hotel-pass">Cancelar</a>
                <a href="#0" class="save-hotel-pass">Listo</a>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
      <!-- <div class="b-group">
        <label for="">Pasajeros</label>
          <div class="dropdown">
            <i class="fa fa-user"></i>
            <a href="#" class="show-hotel-pass"> <span id="hotel-num-pass">1</span> <i class="fa fa-chevron-down"></i></a>
          </div>
          </div> -->
      <div class="btn">
        <a href="#" onclick="buscarHotel();" title="Buscar">Buscar</a>
      </div>
    </div>
  </div>
