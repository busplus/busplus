<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', WP_ENVIRONMENT === 'production' ? 'db_busplus' : 'db_busplus1');


/** MySQL database username */
define('DB_USER', WP_ENVIRONMENT === 'production' ? 'db_busplus' : 'db_busplus1');


/** MySQL database password */
define('DB_PASSWORD', WP_ENVIRONMENT === 'production' ? 'dWT2T8KdSSezxpb7' : 'PfK5YtNLwyThwXWS');


/** MySQL hostname */
define('DB_HOST', 'localhost');


/** Database charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');


/** The database collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'G 2qPL)Tbm2|lJG9##E=Zi;GZBBuqDw*#;>9wY)T&ahZ_09xOeyi?wHa2(Idx4#]');

define('SECURE_AUTH_KEY', ' JF^:W]OzNHt8a[2Ff#LjH>;t1TU]dhXt *-4u`95|v)#rD2}NB>pAbOPmKbzp0-');

define('LOGGED_IN_KEY', ',`#$CfT?*97#@SwRPPE|rk7&rzsQG!N~N.y84D5y8mMLeQ6;[|#3fRG#&DuQ<X@:');

define('NONCE_KEY', 'N}Wj!NDK4eF=9A`(CE:V,]O0L$~kN!?bbuGq*fd<a,OMLy-`37{sb0Y*i9~o.w2$');

define('AUTH_SALT', ']xr%Xb ZmCBXF{.+E5(AO8XL,dgh`d`|(B|01Q8i44~{I^zT$?7r*/-rDW,r5u$J');

define('SECURE_AUTH_SALT', ' XpNi7815X;ObOCgPwU]s#MRtgvIqE^7I!@-zV$cN#V4u6hV.Qk)UEstkm~Q-(:3');

define('LOGGED_IN_SALT', 'Q|v;*729v+gQ;St~G{]<ThWoM4G^PWy~3;CSH.X//cb3>Ml4Q9r>%C>TXa:*SfFV');

define('NONCE_SALT', '!v|,IQ!8}:U1@~>Qr;xef.6@aN$8jM~+Tv6(4e~|F&:_&K-[:HoAOi/;e1-fw?P`');


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpbspls_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Add any custom values between this line and the "stop editing" line. */


if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/* Sets custom - begin */
define('WP_CONTENT_FOLDERNAME', 'assets');

if (WP_ENVIRONMENT === 'production') {
    define('WP_SITEURL', 'https://busplus.com.ar/');
    define('API_URL', 'https://ws.busplus.com.ar/');
} else {
    define('WP_SITEURL', 'https://web-busplus.viatesting.com.ar/');
    define('API_URL', 'https://api.viatesting.com.ar/');
}
define('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);
define('WP_PLUGIN_FOLDERNAME', 'scripts');
define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/' . WP_PLUGIN_FOLDERNAME);
define('WP_PLUGIN_URL', WP_CONTENT_URL . '/' . WP_PLUGIN_FOLDERNAME);

define('UPLOADS', 'assets/media');
/* Sets custom - end */

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//define('WPCF7_VALIDATE_CONFIGURATION', false);

define('FORCE_SSL_ADMIN', true);
